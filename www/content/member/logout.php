<?php
require_once '../../if-config.php';

unset($_SESSION['user']);

$home_url = HOME_URL;

if_redirect($home_url);

?>