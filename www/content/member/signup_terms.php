<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-user.php';
$user_id = if_get_current_user_id();
if(!empty($user_id)){
    if_js_alert_back('이미 학회 회원입니다.');
}

$json = if_get_option('site_terms');
$option_value = json_decode($json, true);

$json = if_get_option('privacy_stmt');
$option_value2 = json_decode($json, true);

$on2 = 'on';
$left = '회원공간';
$title = '회원가입';
$regist_page = true;

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<ul class="sub-tabmenu">
					<li class="on"><span><img src="<?php echo INC_URL ?>/img/sub/ic-join1.png" alt=""> 약관동의</span></li>
					<li><span><img src="<?php echo INC_URL ?>/img/sub/ic-join2.png" alt=""> 회원정보 입력</span></li>
					<li><span><img src="<?php echo INC_URL ?>/img/sub/ic-join3.png" alt=""> 회원가입 완료</span></li>
				</ul>
				<div class="join-term">
					<form id="form-item-new">
						<span class="tit dot">이용약관</span>
						<div class="term-con">
							<?php echo $option_value ?>
						</div>
						<span class="tit dot">개인정보취급방침</span>
						<div class="term-con">
							<?php echo $option_value2 ?>
						</div>
						<div class="radio">
							<p>인포랑학회 이용약관 및 개인정보 보호정책에 동의하십니까?</p>
							<label class="radio-inline">
								<label><input type="radio" name="agree_term" value="Y"> 동의합니다.</label>
							</label>							
							<label class="radio-inline">
								<label><input type="radio" name="agree_term" value="N"> 동의하지 않음</label>
							</label>	
						</div>
						<div class="btn-wrap text-center">
							<button type="submit" class="btn btn-primary bg-blue btn-big">가입하기</button>
							<button type="button" class="btn btn-default bg-grey btn-big">취소</button>
						</div>
					</form>
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function() {
		if ($('input[name="agree_term"]:checked').val() != "Y") {
			alert("개인정보 취급방침에 대해 동의해 주십시오.");
			return false;
		} else {
			$("#form-item-new").attr({
				"method" : "post",
				"action" : "signup.php"
			}).submit();
		}
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>