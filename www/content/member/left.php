<div class="left-menu-top">
	<h3><?php if(isset($regist_page) && $regist_page){echo "회원가입";} else {echo "회원공간";}?></h3>
</div>
<ul class="left-menu">
<?php if(isset($regist_page) && $regist_page) {?>
	<li class="<?php echo (isset($on1))?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/member/login.php">로그인 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on2))?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/member/signup_terms.php">회원가입 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on3))?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/member/find_user.php">아이디/비밀번호 찾기 <i class="xi-angle-right"></i></a></li>
<?php } else { ?>
	<!-- <li class="<?php echo (isset($on4))?$on4:""?>"><a href="<?php //echo CONTENT_URL ?>/member/mypage.php">My page <i class="xi-angle-right"></i></a></li> -->
	<li class="<?php echo (isset($on5))?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">공지사항 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on6))?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=2">Q&A <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on7))?$on7:""?>"><a href="<?php echo CONTENT_URL ?>/member/subscribe.php">학회지 구독신청 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on8))?$on8:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=3">채용공고 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on9))?$on9:""?>"><a href="<?php echo CONTENT_URL ?>/member/sponsor.php">후원금 신청 <i class="xi-angle-right"></i></a></li>
<?php }?>
</ul>