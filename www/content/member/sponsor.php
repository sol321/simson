<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_user();

$on9 = 'on';
$left = '회원공간';
$title = '후원금 신청';
$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

$sparam = [];

$query = "
    SELECT
        *
    FROM
        " . $GLOBALS['if_tbl_sponsors'] . "
    WHERE
        user_id = ".if_get_current_user_id()."
    ORDER BY
		seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents sponsor">
				<div class="info-box">
					후원금 신청 안내문
				</div>
				<div class="btn-wrap text-center">
					<a href="<?php echo CONTENT_URL ?>/member/application.hwp" class="btn btn-primary bg-blue btn-big" target="_blank">신청서 양식 다운로드</a>
					<a href="<?php echo CONTENT_URL ?>/member/sponsor_add.php" class="btn btn-default bg-grey btn-big">신청하러 가기</a>
				</div>
				<div class="table-wrap n-border mt30">
					<table class="cst-table">
						<caption>후원금 신청</caption>
						<colgroup>
							<col style="width: 25%;">
							<col style="width: 25%;">
							<col style="width: 25%;">
							<col style="width: 25%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">연도</th>
								<th scope="col">신청일</th>
								<th scope="col">금액</th>
								<th scope="col">후원일</th>
							</tr>
						</thead>
						<tbody>
						<?php if (!empty($item_results)) {
						    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						    foreach ($item_results as $key => $val) {
						        $reg_year = substr($val['create_dt'],0,4);
						        $reg_date = substr($val['create_dt'],0,10);
						        $sponsorship_amount = $val['sponsorship_amount'];
						        $apply_dt = $val['apply_dt'];
						        
						        ?>
          						<tr>
        							<td><?php echo $reg_year;?></td>
        							<td><?php echo $reg_date;?></td>
        							<td><?php echo number_format($sponsorship_amount);?>원</td>
        							<td><?php echo $apply_dt;?></td>
        						</tr>  									
    					<?php
    							$list_no--;
    						}
    					?>
						
						<?php } else {?>
						<tr><td colspan="4">등록된 데이터가 없습니다.</td></tr>
						<?php }?>

						</tbody>
					</table>
				</div>
    			<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
    			</div>					
			</div>
		</article>
	</div>
</section>		
<?php 
require_once INC_PATH . '/front-footer.php';
?>