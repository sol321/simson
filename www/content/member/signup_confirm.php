<?php
require_once '../../if-config.php';
$on2 = 'on';
$left = '회원공간';
$title = '가입이력 확인';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="info-box">
					회원가입 이력이 있는 경우,  추가 회원가입 시 입회비는 면제됩니다.			
				</div>
				<div class="join-confirm">
					<form id="form-item-new" class="form-horizontal">
						<fieldset>
							<div class="join-confirm-box">
								<span class="tit">가입이력 찾기</span>
								<ul class="join-confirm-input">
									<li>
										<label for="name">이름</label>
										<input type="text" id="name" name="" class="form-control">
									</li>
									<li>
										<label for="email">E-mail</label>
										<input type="text" id="email" name="" class="form-control">
									</li>
								</ul>
								<div class="btn-wrap text-right">
									<button type="submit" class="btn btn-primary bg-blue regi-confirm">확인</button>
								</div>
							</div>
							<div class="join-confirm-box" id="result-box">
								<span class="tit">가입이력 결과</span>
								<div class="table-wrap n-border">
									<table class="cst-table">
										<caption>가입이력 결과</caption>
										<colgroup>
											<col style="width: 33%;">
											<col style="width: 33%;">
											<col style="width: 33%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">아이디</th>	
												<th scope="col">학회명</th>
												<th scope="col">회원구분</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-left">아이디1</td>
												<td>한국여성간호학회</td>
												<td>연회원</td>
											</tr>
											<tr>
												<td class="text-left">아이디2</td>
												<td>한국여성간호학회</td>
												<td>연회원</td>
											</tr>
											<tr>
												<td class="text-left">아이디3</td>
												<td>한국여성간호학회</td>
												<td>연회원</td>
											</tr>
											<tr>
												<td class="text-left">아이디4</td>
												<td>한국여성간호학회</td>
												<td>연회원</td>
											</tr>
											<tr>
												<td class="text-left">아이디5</td>
												<td>한국여성간호학회</td>
												<td>연회원</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="btn-wrap text-right">
					<a href="signup_terms.php" class="btn btn-default bg-grey btn-big">회원가입</a>
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();
		$.ajax({
			type : "POST",
			url : "./ajax/find-user.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					$("#result-box").html(res.result_msg).removeClass("hide");
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>