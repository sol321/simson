<?php
require_once '../../if-config.php';
$on2 = 'on';
$left = '회원공간';
$title = '회원가입';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<ul class="sub-tabmenu">
					<li><span><img src="<?php echo INC_URL ?>/img/sub/ic-join1.png" alt=""> 약관동의</span></li>
					<li><span><img src="<?php echo INC_URL ?>/img/sub/ic-join2.png" alt=""> 회원정보 입력</span></li>
					<li class="on"><span><img src="<?php echo INC_URL ?>/img/sub/ic-join3.png" alt=""> 회원가입 완료</span></li>
				</ul>
				<div class="info-box">
					<div class="join-complete">
						<img src="<?php echo INC_URL ?>/img/sub/ic-check.png" alt="">
						<span class="tit">
							<strong>회원가입</strong>이 정상적으로 완료되었습니다.
						</span>
						<!-- <div class="fee">
							입회비 : <strong>￦10,000</strong><br />
							회비 : <strong>￦70,000</strong>
							<span class="d-block color-blue">총 <strong>￦80,000</strong>원을 납부하시면 정식회원으로 활동하실 수 있습니다.</span>
						</div>
						<p>
							입회비 및 회비 입금 확인이 완료되면 등록된 이메일로 회원증명서를 발급받으실 수 있습니다.
						</p> -->
					</div>
				</div>
				<div class="btn-wrap text-center mt40">
					<a href="login.php" class="btn btn-primary bg-blue btn-big">로그인</a>
					<a href="/" class="btn btn-default bg-grey btn-big">메인</a>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>