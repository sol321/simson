<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (empty($_POST['user_login'])) {
    $code = 101;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_name2'])) {
    $code = 103;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email2'])) {
    $code = 102;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_login = trim($_POST['user_login']);
$user_email = trim($_POST['user_email2']);
$user_name = trim($_POST['user_name2']);

$user_id = if_get_userid_by_login($user_login);

if (empty($user_id)) {
    $code = 501;
    $msg = '해당하는 회원을 찾을 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_info = if_get_user_by_id($user_id);

if (empty($user_info)) {
    $code = 508;
    $msg = '해당하는 회원을 찾을 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}


if (empty($user_info['name_ko']) || empty($user_info['user_email'])) {
    $code = 510;
    $msg = '해당하는 회원을 찾을 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if($user_email != $user_info['user_email'] || $user_name != $user_info['name_ko']) {
    $code = 502;
    $msg = '회원정보가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$temp_passwd = if_update_user_passwd_by_temp($user_id);

if ($temp_passwd) {
    // 메일 발송
    
    $email_subject = '[' . json_decode(if_get_option('site_name')). '] 임시 비밀번호를 안내해 드립니다. ';

    $content =<<<EOD
            <h2 style="color:#000;font-size:25px;font-weight:bold;margin:0;letter-spacing:-1px;">임시 비밀번호 안내</h2>
			<p style="padding:30px;margin:25px 0;display:block;background:#eee;font-size:16px;line-height:24px;border-radius:5px;">
				$user_login 님의 임시 비밀번호는 아래와 같습니다.<br>
				임시 비밀번호 : $temp_passwd<br>
				비밀번호 재분실 방지를 위해, 임시 비밀번호로 로그인 하신 후 새로운 비밀번호로 변경하여 이용하시기 바랍니다.<br>
				본 메일은 발신전용 메일이므로 문의 및 회신하실 경우 답변되지 않습니다. <br>
                                    메일내용에 대해 궁금한 사항이 있으시면 고객센터로 문의하여 주십시오.<br>
				이용해주셔서 감사합니다.<br>
			</p>
EOD;
				$email_body = if_get_default_mail_template($content);
				
				$result = if_send_mail($user_email, $email_subject, $email_body);
				if(!$result) {
				    $code = 512;
				    $msg = '메일발송에 실패하였습니다.';
				    $json = compact('code', 'msg');
				    exit(json_encode($json));
				}
} else {
    $code = 502;
    $msg = '임시 비밀번호를 등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result_msg =<<<EOE
	<p><i class="fa fa-envelope"></i> $user_login 님의 임시 비밀번호를  [<strong>$user_email</strong>] <br>메일주소로 발송했습니다.</p>
	<p>이메일을 확인해 주시기 바랍니다.</p>
EOE;

$json = compact('code', 'msg', 'result_msg');
echo json_encode($json);
?>
