<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if ( empty($_POST['user_name']) ) {
    $code = 101;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['user_email']) ) {
    $code = 102;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$user_name = trim($_POST['user_name']);
$user_email = trim($_POST['user_email']);

$user_login = if_find_user_login($user_name, $user_email);

if (empty($user_login)) {
    $code = 501;
    $msg = '이름과 이메일 주소에 해당하는 회원을 찾을 수 없습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$result_msg =<<<EOD
	<i class="fa fa-star" aria-hidden="true"></i>
	$user_name 님의 아이디는 [<strong>$user_login</strong>] 입니다.
EOD;
	
$json = compact( 'code', 'msg', 'result_msg' );
echo json_encode( $json );

?>
