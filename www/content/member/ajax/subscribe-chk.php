<?php
/*
 * Desc:학회지 신청
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (empty($_POST['user_id'])) {
    $code = 101;
    $msg = '회원정보를  알 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['journal_item'])) {
    $code = 102;
    $msg = '신청학술지 정보를  알 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$journal_data = if_get_journal_item($_POST['journal_item']);
$journal_chk_data = if_get_journal_user_chkeck($journal_data['journal_name']);
if(!empty($journal_chk_data) && is_array($journal_chk_data)){
    $meta_data = $journal_chk_data['meta_data'];
    $jdec = json_decode($meta_data, true);
    if(!empty($jdec['create_dt'])){
        if(time() < strtotime("+1 years",strtotime($jdec['create_dt']))){
            $code = 9;
            $msg = $journal_data['journal_name']."는 이미 신청기록이 있습니다.\n선택시 중복 신청이 됩니다.";
        }
    }
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>