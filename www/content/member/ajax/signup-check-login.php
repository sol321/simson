<?php
/*
 * Desc : 아이디 중복확인
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if ( empty($_POST['data']) ) {
    $code = 403;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$user_login = $_POST['data'];

if (strlen($user_login) < 6) {
    $code = 504;
    $msg = '아이디는 6자 이상으로 생성해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if (strlen($user_login) > 20) {
    $code = 504;
    $msg = '아이디는 20자 이하로 생성해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if (!ctype_alnum($user_login)) {
    $code = 501;
    $msg = '아이디는 영문과 숫자만 사용할 수 있습니다. 새로운 아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if (if_exist_user_login($user_login)) {
    $code = 502;
    $msg = $user_login . ' 아이디는 이미 사용중입니다. 다른 아이디를 입력해 주십시오';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$json = compact('code', 'msg');
echo json_encode($json);

?>