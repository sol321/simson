<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (empty($_POST['sponsor_user_id'])) {
    $code = 103;
    $msg = '회원정보를 알수없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['name'])) {
    $code = 103;
    $msg = '성명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['birth_dt'])) {
    $code = 107;
    $msg = '생년월일 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['org_name'])) {
    $code = 108;
    $msg = '소속기관명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mobile'])) {
    $code = 105;
    $msg = '휴대전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['email'])) {
    $code = 104;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$email = empty($_POST['email']) ? '' : trim($_POST['email']);

if (!empty($email)) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

if (empty($_POST['sponsorship_type'])) {
    $code = 107;
    $msg = '후원방식을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$sps_type = $_POST['sponsorship_type'];

if (!strcmp($sps_type, 'temp')) {   // 일시 후원
    if (empty($_POST['temp_amount'])) {
        $code = 111;
        $msg = '후원금액을 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
} else {    // 정기 후원
    if (!empty($_POST['prd_manual_input'])) {   // 직접 입력
        if (empty($_POST['prd_amount'])) {
            $code = 121;
            $msg = '후원금액을 입력해 주십시오.';
            $json = compact('code', 'msg');
            exit(json_encode($json));
        }
    } else {
        if (empty($_POST['prd_amt_account'])) {
            $code = 126;
            $msg = '정기 후원 구좌수를 입력해 주십시오.';
            $json = compact('code', 'msg');
            exit(json_encode($json));
        }
    }
    
    if (empty($_POST['prd_bank_name'])) {
        $code = 122;
        $msg = '은행을 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    if (empty($_POST['prd_account_no'])) {
        $code = 123;
        $msg = '계좌번호를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    if (empty($_POST['prd_account_holder'])) {
        $code = 124;
        $msg = '예금주를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    if (empty($_POST['prd_start_ym'])) {
        $code = 125;
        $msg = '시작 납부 연월을 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    if($_POST['prd_usage'] == "etc") {
        if(empty($_POST['prd_etc_content'])){
            $code = 126;
            $msg = '기타 정보를 입력해 주십시오.';
            $json = compact('code', 'msg');
            exit(json_encode($json));
        }
    }
}

/*if (empty($_POST['file_path'])) {
    $code = 106;
    $msg = '신청서를 업로드해주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}*/

$result = if_add_sponsor();

if (empty($result)) {
    $code = 201;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>