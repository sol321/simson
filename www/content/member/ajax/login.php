<?php
/*
 * Desc: 회원 로그인
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (empty($_POST['user_login'])) {
    $code = 101;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_pw'])) {
    $code = 102;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_login = trim($_POST['user_login']);
$input_user_pw  = $_POST['user_pw'];

// 아이디로 회원 정보 조회
$user_row = if_get_user_by_login($user_login);

if (empty($user_row)) {
    $code = 201;
    $msg = '존재하지 않는 아이디입니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
$seq_id = $user_row['seq_id'];
$user_pw = $user_row['user_pw'];
$name_ko = $user_row['name_ko'];
$user_state = $user_row['user_state'];
$show_hide = $user_row['show_hide'];

// 비밀번호 비교
$check_passwd = if_verify_passwd($input_user_pw , $user_pw);

if (!$check_passwd) {
    $code = 202;
    $msg = '비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// 접근권한을 확인
if (strcmp($show_hide, 'show')) {
    $code = 210;
    $msg = '이용할 수 없는 상태입니다. 관리자에게 문의해 주시기 바랍니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

/*
if ($user_state == '10') {
    $code = 213;
    $msg = '미승인 상태입니다. 협회의 승인 안내 후 로그인을 할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
*/

if ($user_state > '40') {
    $code = 214;
    $msg = '현재 이용할 수 없는 상태입니다. 협회로 문의 부탁드립니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// Redirect URI
if (empty($_POST['redirect'])) {
    $redirect = CONTENT_URL . '/mypage/';
} else {
    $redirect = base64_decode($_POST['redirect']);
}

/*
 * Desc: 결제해야 할 내역이 있으면 회비 결제화면으로 이동
 *      회비의 경우 usermeta 테이블의 expiration date를 체크한다. 
 */
$expiry_dt = if_get_user_meta($seq_id, 'membership_expiry_date');

if ($expiry_dt < date('Ymd')) {
    $redirect = CONTENT_URL . '/mypage/payment_dues.php';
}

// 인증세션 저장
$_SESSION['user']['sid'] = $seq_id;
$_SESSION['user']['user_login'] = $user_login;
$_SESSION['user']['user_name'] = $name_ko;

// 로그인 기록 저장
if_add_user_login_history($seq_id);

// 최종 로그인 데이터 회원 정보에 업데이트
if_update_user_login_history($seq_id);


$json = compact('code', 'msg', 'redirect');
echo json_encode($json);

?>