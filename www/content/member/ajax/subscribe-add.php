<?php
/*
 * Desc:학회지 신청
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (empty($_POST['user_id'])) {
    $code = 101;
    $msg = '회원정보를 알 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_name'])) {
    $code = 103;
    $msg = '신청자 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (count($_POST['journal_item']) < 1) {
    $code = 104;
    $msg = '신청학술지를 한가지 이상 선택해주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 105;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}


if (!empty($_POST['user_email'])) {
    if (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
if (empty($_POST['org_phone'])) {
    $code = 106;
    $msg = '직장 전화번호를  입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['zipcode']) || empty($_POST['address1'])) {
    $code = 107;
    $msg = '배송지 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['address2'])) {
    $code = 107;
    $msg = '배송지 상세주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// journal_user 등록
$journal_user_id = if_add_journal_user();

if (empty($journal_user_id)) {
    $code = 201;
    $msg = '구독신청을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

//journal_subscription 등록
for($j=0; $j<count($_POST['journal_item']); $j++) {
    $journal_id = $_POST['journal_item'][$j];
    $journal_data = if_get_journal_item($journal_id);
    $subscription_date = array();
    $subscription_date['journal_user_id'] = $journal_user_id;
    $subscription_date['journal_name'] = $journal_data['journal_name'];
    $subscription_date['journal_issue_month'] = $journal_data['journal_issue_month'];
    $subscription_date['journal_fee'] = $journal_data['journal_fee'];
    $subscription_id = if_add_journal_subscription($subscription_date);
    if (empty($subscription_id)) {
        $code = 201;
        $msg = '구독신청시 오류가 발생하였습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
$result = $journal_user_id;
$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>