<?php
/*
 * Desc:회원 등록
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

// var_dump($_POST); exit;

if (empty($_POST['user_login'])) {
    $code = 101;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password1'])) {
    $code = 102;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_login = trim($_POST['user_login']);
$user_email = empty($_POST['user_email']) ? '' : trim($_POST['user_email']);
$user_pw1 = $_POST['password1'];
$user_pw2 = $_POST['password2'];

if (!ctype_alnum($user_login)) {
    $code = 111;
    $msg = '아이디는 알파벳과 숫자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if ( strcmp($user_pw1, $user_pw2) ) {
    $code = 115;
    $msg = '비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if (empty($_POST['name_ko'])) {
    $code = 103;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 104;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!empty($user_email)) {
    if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
if (empty($_POST['user_mobile'])) {
    $code = 105;
    $msg = '휴대전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['home_zipcode']) || empty($_POST['home_address1'])) {
    $code = 106;
    $msg = '자택 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['home_address2'])) {
    $code = 106;
    $msg = '자택 상세주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
/*
if (empty($_POST['org_name'])) {
    $code = 106;
    $msg = '근무처를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['org_position'])) {
    $code = 107;
    $msg = '직위를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['org_phone'])) {
    $code = 108;
    $msg = '직장전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['email_agree'])) {
    $code = 109;
    $msg = '이메일 수신여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
*/

if (if_exist_user_login($user_login)) {
    $code = 116;
    $msg = $user_login . ' 아이디는 이미 사용중입니다. 다른 아이디를 입력해 주십시오';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

// 회원 등록
$user_id = if_add_user();

if (empty($user_id)) {
    $code = 201;
    $msg = '회원 등록을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if_update_user_meta($user_id, 'membership_expiry_date', 0);

// 관리자에게 메일 발송
$json = if_get_option('site_admin_email');  // 관리자 이메일
$to_email = json_decode($json, true);
$name_ko        = empty($_POST['name_ko']) ? '' : trim($_POST['name_ko']);
$user_mobile    = empty($_POST['user_mobile']) ? '' : $_POST['user_mobile'];

$email_subject = $user_login . ' 회원이 가입했습니다.';

$content =<<<EOD

        <h2 style="color:#000;font-size:25px;font-weight:bold;margin:0;letter-spacing:-1px;">회원가입 안내 메일</h2>
		<p style="padding:30px;margin:25px 0;display:block;background:#eee;font-size:16px;line-height:24px;border-radius:5px;">
			이름 : $name_ko <br>
                          아이디: $user_login <br>
                          이메일: $user_email <br>
                          휴대전화번호: $user_mobile <br>
		</p>
EOD;

$email_body = if_get_default_mail_template($content);

if (filter_var($to_email, FILTER_VALIDATE_EMAIL)) {
    if_send_mail($to_email, $email_subject, $email_body, $user_email);
}
// 메일 발송 끝

$result = $user_id;

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>