<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_user();

$on7 = 'on';
$left = '회원공간';
$title = '학회지 구독신청';
$journal_item = if_get_journal_item_list();
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="table-wrap n-border">
					<table class="cst-table">
						<caption>학회지 구독신청</caption>
						<colgroup>
							<col style="width: 25%">
							<col style="width: 25%"> 
							<col style="width: 25%">
							<col style="width: 25%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">학회지명</th>
								<th scope="col">구독신청</th>
								<th scope="col">발간월</th>
								<th scope="col">연구독료</th>
							</tr>
						</thead>
						<tbody>
							<?php if(!empty($journal_item)){
							    foreach($journal_item as $key => $val){
							        $journal_id = $val['seq_id'];
							        $journal_name = $val['journal_name'];
							        $journal_issue_month = $val['journal_issue_month'];
							        $journal_fee = $val['journal_fee'];
							 ?>
							<tr>
								<td><span class="color-grey"><?php echo $journal_name;?></span></td>
								<td>
									<a href="<?php echo CONTENT_URL ?>/member/subscribe_write.php?journal_id=<?php echo $journal_id;?>" class="btn-link">구독하기 <i class="xi-angle-right"></i></a>
								</td>
								<td>
									<?php echo $journal_issue_month;?>월
								</td>
								<td>
									￦<?php echo number_format($journal_fee);?>
								</td>
							</tr>							 
							 <?php
							    }
							 ?>
							<?php } else {?>
							<tr>
								<td colspan="4">No data.</td>
							</tr>								
							<?php }?>
						</tbody>
					</table>
				</div>
				<div class="bg-box">
					<span class="account-info">
						<img src="<?php echo INC_URL ?>/img/sub/ic-dollar.png" alt=""> 수협은행 : 1130-0051-1147  / <br class="m-br"/>예금주 : 사)인포랑학회
					</span>
				</div>
				<div class="border-box">
					<ul class="list-type1">
						<li>각 학회지 구독을 원하시는 분들은 회원가입과는 별도로 학회지 구독하기를 신청하여 주시기 바랍니다.</li>
						<li>학회지는 입금일 이후의 발행분부터 발송됩니다.</li>
					</ul>
				</div>
			</div>
		</article>
	</div>
</section>		
<?php 
require_once INC_PATH . '/front-footer.php';
?>