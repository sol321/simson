<?php
require_once '../../if-config.php';
$regist_page = true;

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>회원가입</h1>
			</div>
			
			<div class="jumbotron">
				<h2>회원가입이 정상적으로 완료되었습니다.</h2>
				
				<p>
					로그인 후, 학회를 선택해 주십시오.
				</p>
				
				<h3>입회비 및 연회비 입금 확인이 완료되면 정회원으로 활동하실 수 있습니다.</h3>
				
			</div>
		</div> <!-- /container -->
		
		<script>
		$(function() {
		});
		</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>