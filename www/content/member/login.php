<?php
require_once '../../if-config.php';
$redirect = empty($_REQUEST['redirect']) ? '' : $_REQUEST['redirect'];  // $_REQUEST['redirect'] : base64_encode된 내용임.
$on1 = 'on';
$left = '회원공간';
$title = '로그인';
$regist_page = true;

$result = if_get_current_user_login();
if($result){
    if_redirect('/');
}

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="login-form">
					<form id="form-item-new" class="form-horizontal">
					<input type="hidden" name="redirect" value="<?php echo $redirect?>">
						<fieldset>
							<span class="tit"><img src="<?php echo INC_URL ?>/img/sub/ic-secret.png" alt=""> 로그인</span>
							<div class="login-contop">
								<div class="login-input">
									<input type="text" id="user_login" name="user_login" class="form-control" placeholder="아이디">
									<input type="password" id="user_pw" name="user_pw" class="form-control" placeholder="비밀번호" autocomplete="new-password">
								</div>
								<button type="submit" class="btn btn-primary bg-blue">로그인</button>
							</div>
							<ul class="login-conb">
								<li><a href="signup_terms.php">회원가입</a></li>
								<li><a href="find_user.php">아이디/비밀번호 찾기</a></li>
							</ul>
						</fieldset>
					</form>
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			type : "POST",
			url : "./ajax/login.php",
			data : $("#form-item-new").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					location.href = res.redirect;
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});
});
</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>