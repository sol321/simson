<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

if_authenticate_user();

$on9 = 'on';
$left = '회원공간';
$title = '후원금 신청';
$max_file_size = 104857600;
$tpl_max_filesize = 100;
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<form id="form-item-new">
					<input type="hidden" name="sponsor_user_id" id="sponsor_user_id" value="<?php echo if_get_current_user_id();?>">
    				<input type="hidden" name="max_file_size" id="max-file-size" value="<?php echo $max_file_size ?>">
					<fieldset>
						<div class="table-wrap border-table">
							<table class="cst-table write-table">
								<caption>후원금 신청</caption>
								<colgroup>
									<col style="width: 20%;"> 
									<col style="width: 80%;">
								</colgroup> 
								<tbody>
									<tr>
										<th scope="row">성명</th>
										<td class="text-left">
											<input type="text" name="name" id="name" class="form-control">
										</td>
									</tr>
									<tr>
										<th scope="row">생년월일</th>
										<td class="text-left">
    										<div class='input-group date dateTimePicker' id="dateRangePicker" class="form-control">
                                                <input type='text' class="form-control" name="birth_dt" class="form-control" placeholder="예) 1970-06-01" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
										</td>
									</tr>
									<tr>
										<th scope="row">소속기관명</th>
										<td class="text-left">
											<input type="text" name="org_name" id="org_name" class="form-control" required>
										</td>
									</tr>
									<tr>
										<th scope="row">이메일</th>
										<td class="text-left">
											<input type="email" name="email" id="email" class="form-control" required>
										</td>
									</tr>
									<tr>
										<th scope="row">휴대전화번호</th>
										<td class="text-left">
											<input type="text" name="mobile" id="mobile" class="form-control" required>
										</td>
									</tr>
									<tr>
										<th scope="row">후원 방식</th>
										<td class="text-left">
                                			<?php 
                                			foreach ($if_sponsorship_type as $key => $val) {
                                			    $chekced = strcmp($key, 'temp') ? '' : 'checked'; 
                                			?>
                    						<div class="radio">
                    							<label>
                    								<input type="radio" name="sponsorship_type" class="sps-type" value="<?php echo $key ?>" <?php echo $chekced ?> required> <?php echo $val ?>
                    							</label>
                    							<div class="text-left help-block"><?php echo $if_sponsorship_type_txt[$key];?></div>
                    						</div>
                                			<?php 
                                			}
                                			?>
										</td>
									</tr>
									<tr id="wrap_temp">
										<th scope="row">후원금액</th>
										<td>
                    						<div class="input-group">
        										<input type="text" class="form-control numeric" name="temp_amount" id="temp_amount">
        										<span class="input-group-addon">원</span>
        									</div>										
										</td>
									</tr>
									<tr id="wrap_period">
										<th scope="row">후원금액</th>
										<td class="text-left">
                        					<div class="input-group">
                        						<input type="text" class="form-control numeric" name="prd_amt_account" id="prd_amt_account">
                        						<span class="input-group-addon">구좌</span>
                        					</div>
                        					
                        					<div class="checkbox pull-left">
                        						<label>
                        							<input type="checkbox" name="prd_manual_input" id="prd_manual_input" value="1"> 직접 입력
                        						</label>
                        					</div>
                        					<div class="text-right help-block">(1구좌 = 10,000원)</div>
                        					
                        					<div class="input-group hide" id="wrap-prd-amount2">
                        						<input type="text" class="form-control numeric" name="prd_amount" id="prd_amount">
                        						<span class="input-group-addon">원</span>
                        					</div>
										</td>
									</tr>									
									<tr id="wrap_period2">
										<th scope="row">자동이체계좌</th>
										<td class="text-left">
                                			<div class="input-group">
                        						<span class="input-group-addon" style="padding:6px 25px;">은행</span>
                        						<input type="text" class="form-control" name="prd_bank_name" id="prd_bank_name">
                        					</div>
                                			<div class="input-group">
                        						<span class="input-group-addon">계좌번호</span>
                        						<input type="text" class="form-control" name="prd_account_no" id="prd_account_no">
                        					</div>
                                			<div class="input-group">
                        						<span class="input-group-addon" style="padding:6px 18.5px;">예금주</span>
                        						<input type="text" class="form-control" name="prd_account_holder" id="prd_account_holder">
                                			</div>
										</td>
									</tr>									
									<tr id="wrap_period3">
										<th scope="row">납부 시작 연월</th>
										<td class="text-left">
    										<div class='input-group date dateTimePicker' id="prd_start_ym" class="form-control">
                                                <input type="text" class="form-control" name="prd_start_ym" id="prd_start_ym" readonly>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>										
										</td>
									</tr>									
									<tr id="wrap_period4">
										<th scope="row">용도</th>
										<td class="text-left">
                                    	<?php 
                                    	foreach ($if_sponsorship_usage as $key => $val) {
                                    	?>
                        				<div class="radio">
                        					<label>
                        						<input type="radio" name="prd_usage" class="prd_usage" value="<?php echo $key ?>"> <?php echo $val ?>
                        					</label>
                        				</div>
                                    	<?php 
                                    	}
                                    	?>
                        				<input type="text" name="prd_etc_content" id="prd_etc_content" class="form-control hide">
										</td>
									</tr>									
									<!-- tr>
										<th scope="row">신청서</th>
										<td class="text-left">
        									<div class="btn btn-success btn-upload-attach">
        										<i class="fa fa-upload" aria-hidden="true"></i> 파일 첨부
        									</div>
        									<span>최대 <?php echo $tpl_max_filesize ?> MB (<?php echo number_format($max_file_size) ?> bytes)</span>
        									<div style="margin-top: 5px;">
        										<input type="file" name="attachment[]" id="file-attach" class="hide" multiple>
        									</div>        									
        									<ul class="list-group" id="preview-attachment"></ul>								
										</td>
									</tr>	 -->
									<tr>
										<th scope="row" colspan="2">기부금 영수증 정보</th>
									</tr>
									<tr>
										<td class="text-left" colspan="2">
											※기부금 영수증을 원하시는 경우에는 다음 사항을 작성하여 주시기 바랍니다.
										</td>
									</tr>
									<tr>
										<th scope="row">성명</th>
										<td class="text-left">
											<input type="text" name="don_name" id="don_name" class="form-control">
										</td>
									</tr>
									<tr>
										<th scope="row">주민등록번호</th>
										<td class="text-left">
											<input type="text" class="form-control" name="don_reg_no" id="don_reg_no" data-inputmask='"mask": "999999-9999999"' data-mask>
										</td>
									</tr>
									<tr>
										<th scope="row">주소</th>
										<td class="text-left">
											<input type="text" class="form-control" name="don_address" id="don_address">
										</td>
									</tr>
									<tr>
										<td class="text-left" colspan="2">
											* 후원금은 기부금 세액 공제(공제율 15%, 3천만원 초과분은 25%)에 해당됩니다.
										</td>
									</tr>																		
									<tr>
										<td class="text-left" colspan="2">
											[문의] 후원안내 전화 : 02-567-7236, 567-2529 &nbsp;&nbsp;&nbsp; E-mail : <a href="mailto:info@inforang.com">info@inforang.com</a>
										</td>
									</tr>																		
								</tbody>
							</table>
						</div>
						<br>
						<div class="btn-wrap text-center p30">
    						<button type="button" id="btn-cancel" class="btn btn-default">취소</button>
							<button type="submit" id="btn-submit" class="btn btn-primary">신청</button>
						</div>
					</fieldset>
				</form>
			</div>
		</article>
	</div>
</section>	

<!-- Form -->
<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.inputmask.js"></script>
<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
<!-- Numeric -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
<!-- jQuery Validate Plugin -->
<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
<script>
$(function () {

	$('[data-mask]').inputmask();
	$('#dateRangePicker').datepicker({
		 format: "yyyy-mm-dd",
		 language: "kr",
		 autoclose: true
	});	

	// 연월 선택
	$('#prd_start_ym').datepicker({
		format: "yyyy-mm",
		viewMode: "months", 
		minViewMode: "months",
		language: "kr",
		autoclose: true
    });
    
	$(".numeric").numeric({ negative: false });
	
	// 초기화
	if ($(".sps-type:checked").val() == "period") {
		$("#wrap_temp").addClass("hide");
	} else {
		$("#wrap_period").addClass("hide");
		$("#wrap_period2").addClass("hide");
		$("#wrap_period3").addClass("hide");
		$("#wrap_period4").addClass("hide");
	}

	// 후원 방식
	$(".sps-type").click(function() {
		var val = $(this).val();
		if (val == "period") {
			$("#temp_amount").val("");
			$("#wrap_period").removeClass("hide");
			$("#wrap_period2").removeClass("hide");
			$("#wrap_period3").removeClass("hide");
			$("#wrap_period4").removeClass("hide");			
			$("#wrap_temp").addClass("hide");
		} else {
			$("#wrap_period").addClass("hide");
			$("#wrap_period2").addClass("hide");
			$("#wrap_period3").addClass("hide");
			$("#wrap_period4").addClass("hide");			
			$("#wrap_temp").removeClass("hide");
			$("#prd_amt_account").val("");
			$("#prd_amount").val("");
			$("#prd_bank_name").val("");
			$("#prd_account_no").val("");
			$("#prd_account_holder").val("");
			$("#prd_start_ym").val("");
			$("#prd_etc_content").val("");
			$("#prd_manual_input").prop("checked",false);		
			$("input:radio[name='prd_usage']").each(function(i) {
			       this.checked = false;
			});	
		}
	});

	// 용도
	$(".prd_usage").click(function() {
		var val = $(this).val();
		if (val == "etc") {
			$("#prd_etc_content").removeClass("hide");
		} else {
			$("#prd_etc_content").addClass("hide");
			$("#prd_etc_content").val('');

		}		
	});

	// 직접 입력
	$("#prd_manual_input").click(function() {
		var checked = $(this).prop("checked");

		if (checked) {
			$("#wrap-prd-amount2").removeClass("hide");
			$("#prd_amt_account").val("");
		} else {
			$("#wrap-prd-amount2").addClass("hide");
			$("#prd_amount").val("");
		}
	});
	
	//-------------------- 첨부파일 시작
	// 첨부파일 trigger
	$(".btn-upload-attach").click(function() {
		$("#file-attach").click();
	});

	// 첨부파일 Upload
	$("#file-attach").change(function() {
		var fsize = this.files[0].size;
		var fname = this.files[0].name;
		var fext = fname.split('.').pop().toLowerCase();
		var max_file_size = $("#max-file-size").val();

		if (fsize > max_file_size) {
			alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
			return;
		}

    	
		$("#form-item-new").ajaxSubmit({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
			dataType : "json",
			success: function(xhr) {
				if (xhr.code == "0") {
					var fileLists = '';
					for (var i = 0; i < xhr.file_url.length; i++) {
						fileLists += '<li class="list-group-item list-group-item-success">' +
											'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
											'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
											'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
											'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
											xhr.file_name[i] +
										'</li>';
					}
					$("#preview-attachment").append(fileLists);
				} else {
					alert(xhr.msg);
				}
				$('input[name="attachment[]"]').val("");
			}
		});
	});
	//-- 첨부파일 Upload 시작

	// 첨부파일 삭제
	$(document).on("click", ".list-group-item .delete-file-attach", function() {
		var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

		$.ajax({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
			data : {
				"filePath" : file
			},
			dataType : "json",
			success : function(res) {
				$("#preview-attachment li").each(function(idx) {
					var file = $(this).find('input[name="file_path[]"]').val();
					if (file == res.file_path) {
						$(this).fadeOut("slow", function() { $(this).remove(); });
					}
				});
				if (res.code != "0") {
					alert(res.msg);
				}
			}
		});
	});
	//-- 첨부파일 삭제
	//-------------------- 첨부파일 끝

	$("#form-item-new").validate({
		rules: {
			name: {
				required: true
			},
			email: {
				required: true
			},
			mobile: {
				required: true
			}
		},
		messages: {
			name: {
				required: "성명을 입력해 주십시오."
			},
			email: {
				required: "이메일을 입력해 주십시오."
			},
			mobile: {
				required: "연락처를 입력해 주십시오."
			}
		},
		submitHandler: function(form) {
			$.ajax({
				type : "POST",
				url : "./ajax/sponsor-add.php",
				data : $("#form-item-new").serialize(),
				dataType : "json",
				beforeSend : function() {
					$("#btn-submit").prop("disabled", true);
				},
				success : function(res) {
					if (res.code == "0") {
						alert("등록되었습니다.");
						location.href = "sponsor.php";
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax
		}
	});

	$("#btn-cancel").click(function() {
		history.back();
	});	
});	
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>