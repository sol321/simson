<?php
require_once '../../if-config.php';

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>비밀번호 찾기</h1>
			</div>

			<div class="col-sm-6">
    			<form id="form-item-new" class="form-horizontal">
                	<div class="form-group">
                		<label for="user_login" class="col-sm-2 control-label">아이디</label>
                		<div class="col-sm-10">
                			<input type="text" class="form-control" name="user_login" id="user_login" placeholder="아이디">
                		</div>
                	</div>
                	<div class="form-group">
                		<label for="user_email" class="col-sm-2 control-label">이메일</label>
                		<div class="col-sm-10">
                			<input type="email" class="form-control" name="user_email" id="user_email" placeholder="이메일">
                		</div>
                	</div>
                	
                	<div class="alert alert-success hide col-sm-offset-2" id="result-box" role="alert"></div>
                	
                	<div class="form-group">
                		<div class="col-sm-offset-2 col-sm-10">
               				<button type="submit" id="btn-submit" class="btn btn-primary form-control">확 인</button>
               			</div>
                	</div>
                	
                	<div class="form-group">
                		<div class="col-sm-offset-2 col-sm-4">
                			<a href="find_id.php">아이디 찾기</a>
                		</div>
                	</div>
                </form>
                
            </div>
		</div> <!-- /container -->
		
		<script>
        $(function() {
        	$("#form-item-new").submit(function(e) {
        		e.preventDefault();
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/find-pw.php",
    				data : $(this).serialize(),
    				dataType : "json",
    				beforeSend : function() {
    					$("#btn-submit").prop("disabled", true);
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						$("#result-box").html(res.result_msg).removeClass("hide");
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    				$("#btn-submit").prop("disabled", false);
    			}); // ajax
			});
		});
        </script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>