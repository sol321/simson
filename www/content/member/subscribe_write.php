<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_user();

$user_id = if_get_current_user_id();
$user_row = if_get_user_by_id($user_id);

$user_name = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);

$zipcode = $jdec['home_zipcode'];
$address1 = $jdec['home_address1'];
$address2 = $jdec['home_address2'];
$org_phone = $jdec['org_phone'];
$on7 = 'on';
$left = '회원공간';
$title = '학회지 구독신청';
$journal_item = if_get_journal_item_list();
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="border-box">
					학회지 구독은 입금일 이후 발행되는 학회지부터 발송됩니다.
				</div>
				<form id="form-item-new" class="form-inline">
				<input type="hidden" name="user_id" id="user_id"  value="<?php echo $user_id;?>">
					<fieldset>
						<div class="table-wrap border-table">
							<table class="cst-table subscribe-table write-table">
								<caption>구독신청</caption>
								<colgroup>
									<col style="width: 20%;"> 
									<col style="width: 80%;">
								</colgroup> 
								<tbody>
									<tr>
										<th scope="row"><strong class="required">*</strong> 신청자</th>
										<td class="text-left">
											<input type="text" name="user_name" id="user_name" class="form-control w40" value="<?php echo @$user_name;?>">
										</td>
									</tr>
									<tr>
										<th scope="row"><strong class="required">*</strong> 신청학술지</th>
										<td class="text-left">
											
								<?php 
								if(!empty($journal_item)){
									foreach($journal_item as $key => $val){
										$journal_id = $val['seq_id'];
										$journal_name = $val['journal_name'];
										$journal_issue_month = $val['journal_issue_month'];
										$journal_fee = $val['journal_fee'];
										$checked = (!empty($_GET['journal_id']) && $_GET['journal_id'] == $journal_id)? " checked":"";
								?>
    										<div class="checkbox" style="display: block;">
    											<label>
    												<input type="checkbox" name="journal_item[]" class="journal_item" id="journal_<?php echo $journal_id;?>" value="<?php echo $journal_id;?>" <?php echo $checked;?>> <?php echo  $journal_name;?>(1년)
    											</label>
    										</div>												
								<?php 
									}
								}
								?>
										</td>
									</tr>
									<tr>
										<th scope="row"><strong class="required">*</strong> 이메일</th>
										<td class="text-left">
											<input type="email" name="user_email" id="user_email" class="form-control w40" value="<?php echo @$user_email;?>">
										</td>
									</tr>
									<tr>
										<th scope="row">휴대전화</th>
										<td class="text-left">
											<input type="text" name="user_mobile" id="user_mobile" class="form-control w40" value="<?php echo @$user_mobile;?>">
										</td>
									</tr>
									<tr>
										<th scope="row"><strong class="required">*</strong> 직장 전화번호</th>
										<td class="text-left">
											<input type="text" name="org_phone" id="org_phone" class="form-control w40" value="<?php echo @$org_phone;?>">
										</td>
									</tr>
									<tr>
										<th scope="row"><strong class="required">*</strong> 배송지 주소</th>
										<td class="text-left address">
											<input type="text" name="zipcode" id="zipcode" class="form-control w30 has-btn" readonly value="<?php echo @$zipcode;?>"> <button type="button" class="btn btn-warning" onclick="execDaumPostcode('journal');">주소검색</button><br>
											<input type="text" name="address1" id="address1" class="form-control w100p" readonly value="<?php echo @$address1;?>">
											<input type="text" name="address2" id="address2" class="form-control w100p" value="<?php echo @$address2;?>">
										</td>
									</tr>
								</tbody>
							</table>
							
						</div>
						<div class="btn-wrap text-center mt50">
							<a href="<?php echo CONTENT_URL ?>/member/subscribe.php" class="btn btn-danger">취소</a> &nbsp;
							<button type="submit" class="btn btn-primary">확인</button>
						</div>											
					</fieldset>
				</form>

			</div>
		</article>
	</div>
</section>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
$(function() {
	// 우편번호 찾기
	$("#zipcode, #address1").click(function() {
		execDaumPostcode('journal');
	});

	$("#form-item-new").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/subscribe-add.php",
			data : $("#form-item-new").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("신청되었습니다.");
						location.href = "./subscribe.php";
					} else {
						alert("신청하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});

	$(".journal_item").click(function(){
		var chk = $(this).is(":checked");
		var chk_journal_item = $(this).val();
		if(chk) {
			$.ajax({
				type : "POST",
				url : "./ajax/subscribe-chk.php",
				data : {
					"journal_item" : chk_journal_item
					, "user_id" : $("#user_id").val() 
					},
				dataType : "json",
				beforeSend : function() {
				},
				success : function(res) {
					if (res.code == "0") {
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
			}); // ajax
		}
	});	

	<?php if($_GET['journal_id']){?>
	$.ajax({
		type : "POST",
		url : "./ajax/subscribe-chk.php",
		data : {
			"journal_item" : <?php echo $_GET['journal_id'];?>
			, "user_id" : $("#user_id").val() 
			},
		dataType : "json",
		beforeSend : function() {
		},
		success : function(res) {
			if (res.code == "0") {
			} else {
				alert(res.msg);
			}
		}
	}).done(function() {
	}).fail(function() {
	}).always(function() {
	}); // ajax
	<?php }?>			
});

// Daum 우편번호
function execDaumPostcode(spot) {
	new daum.Postcode({
		oncomplete: function(data) {
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
			var extraRoadAddr = ''; // 도로명 조합형 주소 변수

			// 법정동명이 있을 경우 추가한다. (법정리는 제외)
			// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
			if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
				extraRoadAddr += data.bname;
			}
			// 건물명이 있고, 공동주택일 경우 추가한다.
			if(data.buildingName !== '' && data.apartment === 'Y'){
			   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
			}
			// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
			if(extraRoadAddr !== ''){
				extraRoadAddr = ' (' + extraRoadAddr + ')';
			}
			// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
			if(fullRoadAddr !== ''){
				fullRoadAddr += extraRoadAddr;
			}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			if (spot == "journal") {
				document.getElementById('zipcode').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('address1').value = fullRoadAddr + " ";		// 도로명 주소
				document.getElementById('address2').focus();
			}
		}
	}).open();
}
</script>	

<?php 
require_once INC_PATH . '/front-footer.php';
?>