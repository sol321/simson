<?php
require_once '../../if-config.php';
$on3 = 'on';
$left = '회원공간';
$title = '아이디/비밀번호 찾기';
$regist_page = true;
require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>

<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="find-form">
					<fieldset>
						<form id="form-item-new" class="form-horizontal">
							<div class="find-id">
								<span class="tit">아이디 찾기</span>
								<div class="find-contop">
									<p>
									개인정보보호를 위해 이름과 E-MAIL 주소가 회원가입시
									입력한 내용과 일치할 경우 아이디를 확인 하실수 있습니다.								
									</p>
									<div class="find-input">
										<input type="text" id="user_name" name="user_name" class="form-control" placeholder="이름">
										<input type="text" id="user_email" name="user_email" class="form-control" placeholder="이메일">
									</div>
									<div class="alert alert-success hide" id="result-box" role="alert"></div>
									<button type="submit" id="btn-submit" class="btn btn-primary bg-blue">확인</button>
								</div>
							</div>
						</form>
						<form id="form-item-new2" class="form-horizontal">
						<div class="find-pw">
							<span class="tit">비밀번호 찾기</span>
							<div class="find-contop">
								<p>
								개인정보보호를 위해 이름과 E-MAIL 주소가 회원가입시
								입력한 내용과 일치할 경우 비밀번호를 확인 하실수 있습니다.								
								</p>
								<div class="find-input">
									<input type="text" id="user_login" name="user_login" class="form-control" placeholder="아이디">
									<input type="text" id="user_name2" name="user_name2" class="form-control" placeholder="이름">
									<input type="text" id="user_email2" name="user_email2" class="form-control" placeholder="이메일">
								</div>
								<div class="alert alert-success hide" id="result-box2" role="alert"></div>
								<button type="submit" id="btn-submit2" class="btn btn-primary bg-blue">확인</button>
							</div>
						</div>
						</form>	
					</fieldset>
				</div>
			</div>
		</article>
	</div>
</section>
		
		<script>
        $(function() {
        	$("#form-item-new").submit(function(e) {
        		e.preventDefault();
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/find-id.php",
    				data : $(this).serialize(),
    				dataType : "json",
    				beforeSend : function() {
    					$("#btn-submit").prop("disabled", true);
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						$("#result-box").html(res.result_msg).removeClass("hide");
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    				$("#btn-submit").prop("disabled", false);
    			}); // ajax
			});

    	$("#form-item-new2").submit(function(e) {
    		e.preventDefault();

			$.ajax({
				type : "POST",
				url : "./ajax/find-pw.php",
				data : $(this).serialize(),
				dataType : "json",
				beforeSend : function() {
					$("#btn-submit2").prop("disabled", true);
				},
				success : function(res) {
					if (res.code == "0") {
						$("#result-box2").html(res.result_msg).removeClass("hide");
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit2").prop("disabled", false);
			}); // ajax
		});
   	});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>