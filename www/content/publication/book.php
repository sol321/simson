<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
$json = if_get_option('book');
$option_value = json_decode($json, true);

$on1 = 'on';
$left = '출판';
$title = '국가시험 대비 문제집 ';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3">
						<li class="on"><a href="book.php">구입</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=4">공지사항</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=5">질문게시판</a></li>
					</ul>
				</div>
				<div class="info-box book">
					<img src="<?php echo INC_URL ?>/img/sub/ic-book.png" alt="">
					<span class="tit"><strong>구입문의</strong>는 <a href="tel:02-322-4466" class="color-blue">02-322-4466</a>으로 연락주세요.</span>
				</div>
				<div class="info-box info-box-bg">
					<?php echo $option_value;?>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>