<div class="left-menu-top">
	<h3>출판</h3>
</div>
<ul class="left-menu">
	<li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/publication/book.php">국가시험 대비 문제집 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/publication/book2.php">문제집2 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on3)?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/publication/exam_regist.php">학습역량평가 모의고사 <i class="xi-angle-right"></i></a></li>
</ul>