<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$on3 = 'on';
$left = '출판';
$title = '학습역량평가 모의고사';

if (empty($_SESSION['mock_exam_apply_checkout'])) {
    if_js_alert_move('조회 권한이 없습니다.', 'exam_checkout.php');
}

$apply_id = $_SESSION['mock_exam_apply_checkout'];
$apply_row = if_get_mock_exam_application($apply_id);
$univ_name = $apply_row['univ_name'];
$apply_name = $apply_row['apply_name'];
$apply_phone = $apply_row['apply_phone'];
$apply_email = $apply_row['apply_email'];
$apply_dt = $apply_row['apply_dt'];

$meta_data = $apply_row['meta_data'];
$jdec = json_decode($meta_data, TRUE);
$apply_fax = $jdec['apply_fax'];
$org_zipcode = $jdec['org_zipcode'];
$org_address1 = $jdec['org_address1'];
$org_address2 = $jdec['org_address2'];
$year = $jdec['mock_year'];
$file = $jdec['file_attachment'];
$file_path = $file[0]['file_path'];
$file_url = $file[0]['file_url'];
$file_name = $file[0]['file_name'];

// 신청한 차수 정보 조회
$exam_orders = if_get_mock_exam_orders_of_application($apply_id);

// 응시자 수
$total_applicant = if_get_exam_taker_count($apply_id);

// 신청한 연도에 해당하는 모의고사 차수 정보 조회
$mock_exam = if_get_mock_exam_by_year($year);
$row_count = count($mock_exam) + 2;

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<fieldset>
					<div class="table-wrap exam">
						<table class="cst-table write-table exam">
							<caption>구독신청</caption>
							<colgroup>
								<col style="width: 15%;"> 
								<col style="width: 10%;">
								<col style="width: 15%;">
								<col style="width: 10%;">
								<col>
								<col style="width: 10%;">
							</colgroup>
							<thead>
								<tr>
									<td colspan="6">
										신청내역 확인
									</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row" rowspan="<?php echo $row_count ?>">간호대학생<br>학습역량평가<br/>모의고사</th>
									<th scope="col" rowspan="2"></th>
									<th scope="col" rowspan="2">시행일자</th>
								</tr>
								<tr>
									<th scope="col">신청 구분</th>
									<th scope="col" colspan="2">응시일</th>
								</tr>
					<?php 
					if (!empty($mock_exam)) {
						foreach ($mock_exam as $key => $val) {
							$exam_id = $val['seq_id'];
							$exam_order = $val['exam_order'];
							$exam_date = $val['exam_date'];
							
							$apply_label = '';
							$exam_date_label = '';
							
							foreach ($exam_orders as $k => $v) {
							    $e_order = $v['exam_order'];
							    $e_date = $v['exam_date'];
							    $e_state = $v['exam_state'];
							    $p_state = $v['pay_state'];
							    $a_type = $v['apply_type'];
							    
							    if ($exam_order == $e_order) {
							        if (!strcmp($a_type, 'R')) {
							            $apply_label = '정규';
							            $exam_date_label = $e_date;
							        } else {
							            $apply_label = '추가';
							            $exam_date_label = $e_date . ' (희망 응시일)';
							        }
							        
							    } else {
							        
							    }
							    
							}
					?>
								<tr>
									<td>
										<?php echo $exam_order ?>차시
										<input type="hidden" name="exam_order_<?php echo $exam_id ?>" id="exam_order_<?php echo $exam_id ?>" value="<?php echo $exam_order ?>">
									</td>
									<td>
										<?php echo if_date_format_kor_day($exam_date) ?>
									</td>
									<td>
										<?php echo $apply_label ?>
									</td>
									<td class="text-left" colspan="2">
										<?php echo $exam_date_label ?> 
										
									</td>
								</tr>
					<?php 
						}
					}
					?>	
								<tr>
									<th scope="row">신청 대학교</th>
									<td colspan="5" class="text-left">
										<?php echo $univ_name ?>
									</td>
								</tr>
								<tr>
									<th scope="row">받을 주소</th>
									<td colspan="5" class="text-left">
										(<?php echo $org_zipcode ?>) <?php echo $org_address1 ?> <?php echo $org_address2 ?>
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="4">신청자 정보</th>
									<td colspan="5" class="text-left">
										<span class="label label-default">신청자명</span> &nbsp;<?php echo $apply_name ?>
									</td>
								</tr>
								<tr>
									<td colspan="5" class="text-left">
										<span class="label label-default">전화번호</span> &nbsp;<?php echo $apply_phone ?>
									</td>
								</tr>
								<tr>
									<td colspan="5" class="text-left">
										<span class="label label-default">팩스번호</span> &nbsp;<?php echo $apply_fax ?>
									</td>
								</tr>
								<tr>
									<td colspan="5" class="text-left">
										<span class="label label-default">E-mail</span> &nbsp;<?php echo $apply_email ?>
									</td>
								</tr>
								<tr>
									<th scope="row">학생코드표</th>
									<td colspan="5" class="text-left">
										<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $file_name ?>">
											<?php echo $file_name ?>
										</a>
										(<?php echo number_format($total_applicant) ?>명)
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
										※문의사항 - E-mail : info@inforang.com &nbsp; / 
										Tel : 02-337-3337  &nbsp; / Fax : 02-564-0249
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<br>
					<!-- <a href="#n" class="btn btn-primary">접수확인</a> 견적서, 통장 사본, 사업자등록증 확인 가능 -->
					<div class="btn-wrap text-center p30">
						<a href="<?php echo ADMIN_URL ?>/content/publishing/print_estimate.php?id=<?php echo $apply_id ?>" class="btn btn-primary print-estimate">견적서 보기</a> &nbsp;
						<div class="pull-right">
    						<a href="<?php echo CONTENT_URL ?>/download/kan_bank_account.pdf" target="_blank" class="btn btn-default">통장사본 다운로드</a>
    						<a href="<?php echo CONTENT_URL ?>/download/kan_biz_reg_no.pdf" target="_blank" class="btn btn-default">사업자등록증사본 다운로드</a>
						</div>
					</div>	
				</fieldset>
			</div>
		</article>
	</div>
</section>

	<script>
	$(function() {
		// 견적서 출력
		$(".print-estimate").click(function(e) {
			e.preventDefault();
			var url = $(this).attr("href");
			window.open(url, "print", "width=850px,height=1000px,scrollbars=1");
		});
	});

	</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>