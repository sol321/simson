<?php
/*
 * Desc: 접수 확인을 위한 인증 화면
 */
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$on3 = 'on';
$left = '출판';
$title = '학습역량평가 모의고사';

$year = empty($_GET['year']) ? date('Y') : $_GET['year'];

$mock_exam = if_get_mock_exam_by_year($year);

if (empty($mock_exam)) {
    if_js_alert_back('해당 연도의 모의고사가 준비되지 않았습니다.');
}

$row_count = count($mock_exam) + 2;

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<form id="form-item-new" class="form-inline">
					<fieldset>
						<div class="table-wrap exam">
							<table class="cst-table write-table exam">
								<caption>구독신청 확인</caption>
								<colgroup>
									<col style="width: 15%;"> 
									<col>
								</colgroup>
								<thead>
									<tr>
										<td colspan="2" class="text-left">접수 확인</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">신청 대학교</th>
										<td class="text-left">
											<div class="form-group">
												<input type="hidden" name="univ_code" id="univ_code">
												<input type="text" name="univ_name" id="univ_name" class="form-control has-btn" readonly>
												<input type="button" value="검색" class="btn btn-warning" data-toggle="modal" data-target="#myModal"> &nbsp;
												<a href="<?php echo CONTENT_URL ?>/download/univ_code.xlsx" class="btn btn-info btn-sm">학교코드 다운로드</a>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="4">신청자 정보</th>
										<td class="text-left">
											신청자명 &nbsp;<input type="text" name="apply_name" id="apply_name" class="form-control">
										</td>
									</tr>
									<tr>
										<td class="text-left">
											E-mail &nbsp;<input type="email" name="apply_email" id="apply_email" class="form-control"><br>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<br>
						<!-- <a href="#n" class="btn btn-primary">접수확인</a> 견적서, 통장 사본, 사업자등록증 확인 가능 -->
						<div class="btn-wrap text-center p30">
							<button type="submit" id="btn-submit" class="btn btn-primary btn-lg">확인</button>
						</div>	
					</fieldset>
				</form>
			</div>
		</article>
	</div>
</section>

<!-- Modal -->
<div class="modal fade cst-modal" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
				<h4 class="modal-title">대학교 검색</h4>
			</div>
			<div class="modal-body">
				<form id="form-item-search" class="form-inline">
					<div class="pop-form">
						<div class="form-group">
							<input type="text" id="university" class="form-control" placeholder="학교명을 입력해주세요.">
							<button type="submit" class="btn btn-primary">조회</button>
						</div>
					</div>
				</form>
				<div class="table-wrap n-border">
					<table class="cst-table" id="univ-wrap-table">
						<caption>검색결과</caption>
						<colgroup>
							<col style="width: 80%;">
							<col style="width: 20%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">학교명</th>
								<th scope="col">학교코드</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

	<script>
	$(function() {
		$("#form-item-new").submit(function(e) {
			e.preventDefault();

			$.ajax({
				type : "POST",
				url : "./ajax/exam-checkout.php",
				data : $(this).serialize(),
				dataType : "json",
				beforeSend : function() {
					$("#btn-submit").prop("disabled", true);
				},
				success : function(res) {
					if (res.code == "0") {
						location.href = "exam_view.php";
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax
		});

		// 대학교 검색
		$("#form-item-search").submit(function(e) {
			e.preventDefault();

			var university = $.trim($("#university").val());
			searchUniversity(university);
		});

		// 모달 출력 시 바로 검색
		$('#myModal').on('shown.bs.modal', function (e) {
			searchUniversity("");
		});

		// 대학교 클릭 시 값 복사
		$(document).on("click", "#univ-wrap-table tbody tr", function(e) {
			var uname = $(this).find("td:first").text();
			var ucode = $(this).find("td:last").text();
			$("#univ_name").val(uname);
			$("#univ_code").val(ucode);
			$('#myModal').modal("hide");
		});
	});

	function searchUniversity(univ) {
		if (univ == "") {
			univ = "가";
		}
		$.ajax({
			type : "POST",
			url : "./ajax/search-university.php",
			data : {
				"university" : univ
			},
			dataType : "json",
			beforeSend : function() {
			},
			success : function(res) {
				if (res.code == "0") {
					$("#university").val(univ)
					$("#univ-wrap-table tbody").html(res.univ_html);
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
		}); // ajax
	}
	
	</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>