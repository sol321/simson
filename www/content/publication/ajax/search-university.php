<?php
/*
 * Desc : 대학교 검색
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (empty($_POST['university'])) {
    $code = 101;
    $msg = '대학교 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$uname = trim($_POST['university']);

$result = if_get_univ_code_by_name($uname);

$univ_html = '';

if (empty($result)) {
    $univ_html =<<<EOD
        <tr>
            <td colspan="2">검색결과가 없습니다. 대학교명을 확인하시고 다시 입력해 주십시오.</td> 
        </tr>
EOD;
} else {
    foreach ($result as $key => $val) {
        
        $univ_code = $val['univ_code'];
        $univ_name = $val['univ_name']; 
        
        $univ_html .=<<<EOD
                <tr style="cursor: pointer;">
    				<td class="text-left">$univ_name</td>
    				<td>$univ_code</td>
    			</tr>
EOD;
    }
}

$json = compact('code', 'msg', 'univ_html');
echo json_encode($json);

?>