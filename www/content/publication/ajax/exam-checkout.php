<?php
/*
 * Desc : 모의고사 신청
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (empty($_POST['univ_code']) || empty($_POST['univ_name'])) {
    $code = 101;
    $msg = '대학교를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_name'])) {
    $code = 103;
    $msg = '신청자명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_email'])) {
    $code = 105;
    $msg = '이메일 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['apply_email'], FILTER_VALIDATE_EMAIL)) {
    $code = 117;
    $msg = '유효한 이메일주소가 아닙니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$univ_name = $_POST['univ_name'];
$apply_name = $_POST['apply_name'];
$apply_email = $_POST['apply_email'];

$seq_id = if_check_mock_exam_application($apply_name, $apply_email, $univ_name);

$_SESSION['mock_exam_apply_checkout'] = $seq_id;


if (empty($seq_id)) {
    $code = 501;
    $msg = '접수하신  내역을 찾을 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'seq_id');
echo json_encode($json);

?>