<?php
/*
 * Desc : 모의고사 신청
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (empty($_POST['univ_code']) || empty($_POST['univ_name'])) {
    $code = 101;
    $msg = '대학교를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['org_address1']) || empty($_POST['org_address2'])) {
    $code = 102;
    $msg = '주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_name'])) {
    $code = 103;
    $msg = '신청자명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_phone'])) {
    $code = 104;
    $msg = '전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_email'])) {
    $code = 105;
    $msg = '이메일 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['apply_email'], FILTER_VALIDATE_EMAIL)) {
    $code = 117;
    $msg = '유효한 이메일주소가 아닙니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['file_path'][0])) {
    $code = 106;
    $msg = '학생코드표를 업로드해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$app_id = if_add_mock_exam_application();

if (empty($app_id)) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$file_path = if_get_mock_exam_excel_file($app_id);

// 학생코드표의 학생을 if_mock_exam_taker에 등록
$number = if_add_exam_taker_from_excel($file_path, $app_id);

$json = compact('code', 'msg', 'app_id');
echo json_encode($json);

?>