<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
$json = if_get_option('book2');
$option_value = json_decode($json, true);

$on3 = 'on';
$left = '출판';
$title = '학습역량평가 모의고사';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="info-box">
					학습역량평가 모의고사 신청 접수가 완료되었습니다. <br>
					아래의 내용을 확인해 주십시오. 
				</div>
				<div class="info-box info-box-bg" style="text-align: left;">
					<h3 style="font-weight: 500;">모의고사 구독료 납부</h3>
					모의고사 구독신청 후 구독료는 자동으로 합산되어 계산되며, 접수확인 페이지에서 모의고사 견적서를 출력하실 수 있습니다. <br>
					필요시 인포랑학회 사업자등록증과 통장 사본 파일의 다운로드가 가능하오니 참고하시어 견적서에 기재된 입금 계좌로 구독료를 납부해주시기 바랍니다.<br><br> 
					전자계산서 발급이 필요하신 경우 사업자등록증 사본을 메일(info@inforang.com) 또는 팩스(02-564-0249)로 전달해주시면 확인 후 발급해드리도록 하겠습니다.<br><br>
					
					<a href="exam_checkout.php" class="btn btn-info" style="color: #fff;">접수 확인</a>
					
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>