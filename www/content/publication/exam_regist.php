<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$on3 = 'on';
$left = '출판';
$title = '학습역량평가 모의고사';

$year = empty($_GET['year']) ? date('Y') : $_GET['year'];

$mock_exam = if_get_mock_exam_by_year($year);

if (empty($mock_exam)) {
	if_js_alert_back('해당 연도의 모의고사가 준비되지 않았습니다.');
}

$row_count = count($mock_exam) + 2;

require_once INC_PATH . '/front-header.php';
?>

		<style>
		.notice-panel { line-height: 23px; }
		.notice-panel dt { font-size: 18px; margin: 10px 0; }
		.notice-panel dd { margin-left: 10px; }
		</style>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n4">
						<li class="on"><a href="exam.php">단체 <br class="m-tab-br"/>구독신청</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=8">공지사항</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=9">질문게시판</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=10">FAQ</a></li>
					</ul>
				</div>
				<div class="info-box">
					모의고사 시험지 <span class="color-blue">배송작업은 2주가량 소요</span>됩니다.<br>
					희망 응시일이 신청일로부터 2주 이내인 경우, 아래 연락처로 배송 가능여부 확인 후 신청해주시기 바랍니다.<br>
					E-mail: <a href="mailto:info@inforang.com" class="color-blue">info@inforang.com</a>&nbsp;&nbsp;&nbsp;Tel: <a href="tel:02-337-3337" class="color-blue">02-337-3337</a>&nbsp;&nbsp;&nbsp;Fax: <span class="color-blue">02-564-0249</span>
				</div>
				<form id="form-item-new" class="form-inline">
					<input type="hidden" name="year" value="<?php echo $year ?>">
					<fieldset>
						<div class="table-wrap exam">
							<table class="cst-table write-table exam">
								<caption>구독신청</caption>
								<colgroup>
									<col style="width: 15%;"> 
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col>
									<col style="width: 10%;">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" rowspan="<?php echo $row_count ?>">간호대학생<br>학습역량평가<br/>모의고사</th>
										<th scope="col" rowspan="2"></th>
										<th scope="col" rowspan="2">시행일자</th>
										<th scope="col" colspan="3">신청구분</th>
									</tr>
									<tr>
										<th scope="col">정규</th>
										<th scope="col" colspan="2">추가(희망응시일)</th>
									</tr>
						<?php 
						if (!empty($mock_exam)) {
							foreach ($mock_exam as $key => $val) {
								$exam_id = $val['seq_id'];
								$exam_order = $val['exam_order'];
								$exam_date = $val['exam_date'];
						?>
									<tr>
										<td>
											<?php echo $exam_order ?>차시
											<input type="hidden" name="exam_order_<?php echo $exam_id ?>" id="exam_order_<?php echo $exam_id ?>" value="<?php echo $exam_order ?>">
										</td>
										<td>
											<?php echo if_date_format_kor_day($exam_date) ?>
											<input type="hidden" name="exam_date_<?php echo $exam_id ?>" id="exam_date_<?php echo $exam_id ?>" value="<?php echo $exam_date ?>">
										</td>
										<td>
											<input type="hidden" name="exam_id[]" value="<?php echo $exam_id ?>">
											<label class="radio-inline"><input type="radio" name="apply_type_<?php echo $exam_id ?>" value="R"> 정규</label>
										</td>
										<td class="text-left" colspan="2">
											<label class="radio-inline"><input type="radio" name="apply_type_<?php echo $exam_id ?>" value="A"> 추가</label>&nbsp;&nbsp; 
											( <input type="text" name="desired_date_<?php echo $exam_id ?>" class="form-control st-date" readonly> )
										</td>
									</tr>
						<?php 
							}
						}
						?>	
									<tr>
										<th scope="row">신청 대학교</th>
										<td colspan="5" class="text-left">
											<div class="form-group">
												<input type="hidden" name="univ_code" id="univ_code">
												<input type="text" name="univ_name" id="univ_name" class="form-control has-btn" readonly>
												<input type="button" value="검색" class="btn btn-warning" data-toggle="modal" data-target="#myModal"> &nbsp;
												<a href="<?php echo CONTENT_URL ?>/download/univ_code.xlsx" class="btn btn-info btn-sm">학교코드 다운로드</a>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">받을 주소</th>
										<td colspan="5" class="text-left">
											<div class="form-group">
												<input type="text" name="org_zipcode" id="org_zipcode" class="form-control" style="width: 100px;"> 
												<button type="button" id="btn-address-dept" class="btn btn-info btn-sm">주소검색</button>
												<div style="margin-top: 10px;">
												<input type="text" name="org_address1" id="org_address1" class="form-control" style="width: 300px;">
												<input type="text" name="org_address2" id="org_address2" class="form-control" style="width: 300px;">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row" rowspan="4">신청자 정보</th>
										<td colspan="5" class="text-left">
											신청자명 &nbsp;<input type="text" name="apply_name" id="apply_name" class="form-control">
										</td>
									</tr>
									<tr>
										<td colspan="5" class="text-left">
											전화번호 &nbsp;<input type="text" name="apply_phone" id="apply_phone" class="form-control"><br>
										</td>
									</tr>
									<tr>
										<td colspan="5" class="text-left">
											팩스번호 &nbsp;<input type="text" name="apply_fax" id="apply_fax" class="form-control"><br>
										</td>
									</tr>
									<tr>
										<td colspan="5" class="text-left">
											E-mail &nbsp;<input type="email" name="apply_email" id="apply_email" class="form-control"><br>
											<span class="color-red">※ 입력하실 이메일 주소로 답안지와 성적표를 송부하오니 신중히 작성해주세요.</span>
										</td>
									</tr>
									<tr>
										<th scope="row">학생코드표<br/>첨부</th>
										<td colspan="5" class="text-left">
											<span class="color-red">ex) OO대학교 학생코드표.xls</span><br>
											
											<div class="d-block form-group">
												<input type="file" name="attachment[]" id="file-attach" class="form-control">
												
												<a href="<?php echo CONTENT_URL ?>/download/applicant_sample.xls" class="btn btn-info btn-sm">샘플파일 다운로드</a>
												
												<ul class="list-group" id="preview-attachment"></ul>
												
												<span class="d-block">
													* 파일의 형식 또는 확장자가 다른 경우 다음 단계로 진행이 되지 않습니다.<br>
													* 다운로드 받은 샘플파일에 학번과 이름을 기재해 주십시오.
												</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<br>
						<!-- <a href="#n" class="btn btn-primary">접수확인</a> 견적서, 통장 사본, 사업자등록증 확인 가능 -->
						<div class="btn-wrap text-center p30">
							<button type="submit" id="btn-submit" class="btn btn-primary">구독신청</button>
						</div>	
					</fieldset>
				</form>
			</div>
		</article>
	</div>
</section>

<!-- Modal -->
<div class="modal fade cst-modal" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
				<h4 class="modal-title">대학교 검색</h4>
			</div>
			<div class="modal-body">
				<form id="form-item-search" class="form-inline">
					<div class="pop-form">
						<div class="form-group">
							<input type="text" id="university" class="form-control" placeholder="학교명을 입력해주세요.">
							<button type="submit" class="btn btn-primary">조회</button>
						</div>
					</div>
				</form>
				<div class="table-wrap n-border">
					<table class="cst-table" id="univ-wrap-table">
						<caption>검색결과</caption>
						<colgroup>
							<col style="width: 80%;">
							<col style="width: 20%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">학교명</th>
								<th scope="col">학교코드</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>


<!-- Modal 유의사항 -->
<div class="modal fade" id="noticeModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
				<h4 class="modal-title">★ 모의고사 구독신청 유의사항</h4>
			</div>
			<div class="modal-body">
				<dl class="notice-panel">
					<dt>1. 신청 구분</dt>
					<dd>
						인포랑학회 학습역량평가 모의고사는 5회분의 시험을 지정된 날짜에 맞추어 순차적으로 시행합니다. 해당일자에 응시가 불가능한 경우 공개된 시험 유형에 한하여 원하는 일자에 추가적으로 시행할 수 있습니다.
						<div class="alert alert-warning">
    						<ul>
        						<li>● 정규신청: 차시 별로 지정된 시행 일자에 맞추어 응시하는 방법으로 노출되지 않은 새로운 유형의 모의고사를 실시함으로써 정확한 실력 평가가 가능합니다.</li>
        						<li>● 추가신청: 정해진 시행 일자에 응시가 불가능한 경우 이미 공개된 시험 유형을 희망 일자에 추가적으로 시행하는 방법입니다. 선택 시 희망응시일을 필수적으로 지정해야하며 해당 차시의 시행일 이후 날짜만 선택 가능합니다.<br>
        						<p>예) 9월 15일(금) 시행 예정인 1차시 추가신청 시 9월 16일(토) 이후 날짜만 지정 가능</p></li>
        					</ul>
        				</div>
        				<div>
        					모의고사 다회 신청 시, <strong class="alert-danger">시행 차시 별로 응시 인원 또는 응시 학생이 상이한 경우에는 각각의 구독신청서를 별도로 작성해주셔야 합니다.</strong>
        				</div>
					</dd>
					
					<dt>2. 학교 코드
						<a href="<?php echo CONTENT_URL ?>/download/univ_code.xlsx" class="btn btn-info btn-xs">학교코드 다운로드</a>
					</dt>
					<dd>
						구독신청 화면에서 대학교명을 입력한 후 검색 버튼을 누르면 학교코드 확인이 가능합니다. 학교코드는 시험 응시 시 OMR 카드에 기재함으로써 학생의 학교 정보를 구분하는 목적뿐만 아니라 향후 성적 분석 및 통계를 위한 학생코드표를 작성하는 데에도 활용되므로 귀 교의 학교코드를 잘 숙지하여 주시기 바랍니다.
					</dd>
					<dt>3. 학생 코드표
						<a href="<?php echo CONTENT_URL ?>/download/applicant_sample.xls" class="btn btn-info btn-xs">샘플파일 다운로드</a>
					</dt>
					<dd>
						모의고사에 응시한 학생의 정보를 정확하게 확인하고 성적 분석 및 통계를 수행하기 위해서는 학생코드표 작성이 필요합니다. 
						학생코드표란 학생의 학교코드와 학번, 이름을 연결하는 정보파일입니다. 코드표가 누락되는 경우 모의고사 성적표와 통계파일에 이름 등의 정보가 연동되지 않고 학생이 표기한 학번으로만 식별이 가능합니다. 
						OMR 카드에 학번을 잘못 표기하는 경우 코드표 대조를 통한 오류의 발견이 불가능하기 때문에 개인뿐만 아니라 학교의 성적 분석에 혼선을 줄 수 있으며 누적 통계서비스 또한 지원받을 수 없습니다. 
						<span class="alert-danger">코드표 누락으로 인한 성적 재분석 요청은 불가</span>하오니 다음의 학생코드표 작성방법을 참고하여 <span class="alert-danger">사전에 업로드해주시기 바랍니다.</span>
						<div class="alert alert-warning">
							● 학생코드표 작성방법<br>
							구독신청 화면의 샘플파일 다운로드를 클릭하여 첨부된 파일을 내려 받은 후 시험에 응시할 학생들의 학번 및 이름을 순서대로 입력합니다. 
							<span class="alert-danger">학번은 중간 바(-)나 띄어쓰기 없이 숫자로만 기입</span>해야하며 최대 열자리까지 입력할 수 있습니다. 
							완성된 파일은 ‘OO대학교 학생코드표’라는 이름으로 저장하여 첨부해주십시오. 
							모의고사 다회 신청 시, <span class="alert-danger">시행 차시 별로 응시 인원 또는 응시 학생이 상이한 경우에는 각각의 신청서에 맞는 학생코드표를 업로드해주시기 바랍니다.</span>
						</div>
					</dd>
					<dt>4. 모의고사 신청 조회</dt>
					<dd>
						모의고사 구독신청 시 입력한 대학명, 신청자명과 E-mail 주소를 이용하여 신청내역을 조회할 수 있습니다. <span class="alert-danger">신청 내역은 누적되어 나타나며 수정은 불가하오니 신중하게 작성해주시기 바랍니다.</span>
					</dd>
					<dt>5. 모의고사 구독료 납부
						<a href="<?php echo CONTENT_URL ?>/download/kan_bank_account.pdf" target="_blank" class="btn btn-info btn-xs">통장사본 다운로드</a>
    					<a href="<?php echo CONTENT_URL ?>/download/kan_biz_reg_no.pdf" target="_blank" class="btn btn-info btn-xs">사업자등록증사본 다운로드</a>
					</dt>
					<dd>
						모의고사 구독신청 후 구독료는 자동으로 합산되어 계산되며, 접수확인 페이지에서 모의고사 견적서를 출력하실 수 있습니다. 필요시 인포랑학회 사업자등록증과 통장 사본 파일의 다운로드가 가능하오니 참고하시어 견적서에 기재된 입금 계좌로 구독료를 납부해주시기 바랍니다. 전자계산서 발급이 필요하신 경우 사업자등록증 사본을 메일(info@inforang.com) 또는 팩스(02-564-0249)로 전달해주시면 확인 후 발급해드리도록 하겠습니다.
					</dd>
				</dl>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="btn-close">닫기</button>
			</div>
		</div>
		
	</div>
</div>


	<!-- Form -->
	<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
	<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
	
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	$(function() {
		// 유의사항 modal open
		$("#noticeModal").modal("show");
		$("#btn-close").click(function() {
			$("#noticeModal").modal("hide");
		});
		
		$("#form-item-new").submit(function(e) {
			e.preventDefault();

			$.ajax({
				type : "POST",
				url : "./ajax/exam-regist.php",
				data : $(this).serialize(),
				dataType : "json",
				beforeSend : function() {
					$("#btn-submit").prop("disabled", true);
				},
				success : function(res) {
					if (res.code == "0") {
						location.href = "exam_finish.php";
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax
		});

		// radio button toggle
		$("input:radio:checked").data("chk", true);
		$("input:radio").click(function() {
			$("input[name='" + $(this).attr("name") + "']:radio").not(this).removeData("chk");
			$(this).data("chk", !$(this).data("chk"));
			$(this).prop("checked", $(this).data("chk"));
		});

		// 근무처 우편번호 찾기
		$("#btn-address-dept").click(function() {
			execDaumPostcode('dept');
		});

		// bootstrap datepicker
		// https://bootstrap-datepicker.readthedocs.io/en/latest/i18n.html
		$.fn.datepicker.dates['en'] = {
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			daysMin: ["일", "월", "화", "수", "목", "금", "토"],
			months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
			monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
			today: "Today",
			clear: "Clear",
			format: "mm/dd/yyyy",
			titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
			weekStart: 0
		};

		// 추가 희망 응시일 선택 
		$(".st-date").datepicker({
			format: "yyyy-mm-dd",
			autoclose: true
		}).on("changeDate", function(e) {
			var id = $(this).closest("tr").find('input[name="exam_id[]"]').val();
			var exdate = $("#exam_date_" + id).val().replace(/\D/g, "");
			var ddate = $(this).val().replace(/\D/g, "");

			if (exdate >= ddate) {
				alert("희망응시날짜는 정규 시험날짜보다 이후여야 합니다.");
				$(this).val("");
			}
		});

		// 대학교 검색
		$("#form-item-search").submit(function(e) {
			e.preventDefault();

			var university = $.trim($("#university").val());
			searchUniversity(university);
		});

		// 모달 출력 시 바로 검색
		$('#myModal').on('shown.bs.modal', function (e) {
			searchUniversity("");
		});

		// 대학교 클릭 시 값 복사
		$(document).on("click", "#univ-wrap-table tbody tr", function(e) {
			var uname = $(this).find("td:first").text();
			var ucode = $(this).find("td:last").text();
			$("#univ_name").val(uname);
			$("#univ_code").val(ucode);
			$('#myModal').modal("hide");
		});

		// 첨부파일 Upload
		$("#file-attach").change(function() {
			var fsize = this.files[0].size;
			var fname = this.files[0].name;
			var fext = fname.split('.').pop().toLowerCase();
//			var max_file_size = $("#max-file-size").val();
		
			if (fext != "xls" && fext != "xlsx") {
				alert("파일의 확장자는 xls(x)만 가능합니다. 파일을 확인해 주십시오.");
				$(this).val("");
				return;
			}

			$("#form-item-new").ajaxSubmit({
				type : "POST",
				url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
				dataType : "json",
				success: function(xhr) { 
					if (xhr.code == "0") { 
						var fileLists = '';
						for (var i = 0; i < xhr.file_url.length; i++) {
							fileLists = '<li class="list-group-item list-group-item-success">' +
            								'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
            								'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
            								'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
            								'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
											xhr.file_name[i] +
										'</li>';
						}
						$("#preview-attachment").html(fileLists);
					} else {
						alert(xhr.msg);
					}
					$("#file-attach").val("");
				}
			});
		});
		//-- 첨부파일 Upload 시작
		
		// 파일 삭제 
		$(document).on("click", ".delete-file-attach", function() {
			var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

			$.ajax({
				type : "POST",
				url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
				data : {
					"filePath" : file
				},
				dataType : "json",
				success : function(res) {
					$("#preview-attachment").html("");
					if (res.code != "0") {
						alert(res.msg);
					}
				}
			});
		});
		// -- 첨부 파일 끝
		
	});

	function searchUniversity(univ) {
		if (univ == "") {
			univ = "가";
		}
		$.ajax({
			type : "POST",
			url : "./ajax/search-university.php",
			data : {
				"university" : univ
			},
			dataType : "json",
			beforeSend : function() {
			},
			success : function(res) {
				if (res.code == "0") {
					$("#university").val(univ)
					$("#univ-wrap-table tbody").html(res.univ_html);
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
		}); // ajax
	}
	
	function execDaumPostcode(spot) {
		new daum.Postcode({
			oncomplete: function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
				var extraRoadAddr = ''; // 도로명 조합형 주소 변수

				// 법정동명이 있을 경우 추가한다. (법정리는 제외)
				// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
				if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
					extraRoadAddr += data.bname;
				}
				// 건물명이 있고, 공동주택일 경우 추가한다.
				if(data.buildingName !== '' && data.apartment === 'Y'){
				   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
				if(extraRoadAddr !== ''){
					extraRoadAddr = ' (' + extraRoadAddr + ')';
				}
				// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
				if(fullRoadAddr !== ''){
					fullRoadAddr += extraRoadAddr;
				}

				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				if (spot == "dept") {
					document.getElementById('org_zipcode').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('org_address1').value = fullRoadAddr + " ";		// 도로명 주소
					document.getElementById('org_address2').focus();
				} else if (spot == "mail") {
					document.getElementById('mail_zipcode').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('mail_address1').value = fullRoadAddr + " ";		// 도로명 주소
					document.getElementById('mail_address2').focus();
				}
// 				document.getElementById('sample4_jibunAddress').value = data.jibunAddress;		// 지번 주소
			}
		}).open();
	}
	</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>