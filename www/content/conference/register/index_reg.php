<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';

// 회원일 경우 정보 조회
$user_id = if_get_current_user_id();
if ($user_id) {
    //회원은 바로 사전등록으로 이동
    //if_redirect('./regist_add.php?memb=1');
}
$on2 = 'on';
$left = '학술·교육·행사';
$title = '사전등록';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="info-box info-box-bg">
					<?php echo $event_meta['pre_reg_content'];?>
				</div>
				<div class="regi-wrap contents-box-wrap">
					<div class="contents-box n2">
						<span class="tit">회원전용 사전등록</span>
						<span class="icon-wrap">
							<img src="<?php echo INC_URL ?>/img/sub/ic-regi1.png" alt="">
						</span>
						<div class="btn-wrap text-center">
							<a href="<?php echo CONTENT_URL ?>/conference/register/regist_add.php?memb=1" class="btn btn-primary bg-blue">회원사전등록</a>
							<a href="<?php echo CONTENT_URL ?>/member/login.php" class="btn btn-default bg-grey">로그인</a>
						</div>
					</div>
					<div class="contents-box n2">
						<span class="tit">비회원 사전등록</span>
						<span class="icon-wrap">
							<img src="<?php echo INC_URL ?>/img/sub/ic-regi2.png" alt="">
						</span>
						<div class="btn-wrap text-center">
							<a href="<?php echo CONTENT_URL ?>/conference/register/regist_add.php" class="btn btn-primary bg-blue">비회원사전등록</a>
							<a href="<?php echo CONTENT_URL ?>/member/signup_terms.php" class="btn btn-default bg-grey">회원가입</a>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>