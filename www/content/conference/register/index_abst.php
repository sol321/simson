<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
$on3 = 'on';
$left = '학술·교육·행사';
$title = '초록접수';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="info-box info-box-bg">
					<?php echo $event_meta['abstract_content'];?>
				</div>
				<div class="regi-wrap contents-box-wrap">
					<div class="contents-box n2">
						<span class="tit">회원전용 초록접수</span>
						<span class="icon-wrap">
							<img src="<?php echo INC_URL ?>/img/sub/ic-regi1.png" alt="">
						</span>
						<div class="btn-wrap text-center">
							<a href="<?php echo CONTENT_URL ?>/conference/register/abst_add.php?memb=1" class="btn btn-primary bg-blue">회원초록접수</a>
							<a href="<?php echo CONTENT_URL ?>/member/login.php" class="btn btn-default bg-grey">로그인</a>
						</div>
					</div>
					<div class="contents-box n2">
						<span class="tit">비회원전용 초록접수</span>
						<span class="icon-wrap">
							<img src="<?php echo INC_URL ?>/img/sub/ic-regi2.png" alt="">
						</span>
						<div class="btn-wrap text-center">
							<a href="<?php echo CONTENT_URL ?>/conference/register/abst_add.php" class="btn btn-primary bg-blue">비회원초록접수</a>
							<a href="<?php echo CONTENT_URL ?>/member/signup_terms.php" class="btn btn-default bg-grey">회원가입</a>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>