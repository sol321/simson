<?php
/*
 * Desc : 비회원 사전등록 조회
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if (empty($_SESSION['conference']['pre_reg']['id'])) {
    if_js_alert_back('사전 등록 정보가 존재하지 않습니다.');
}

$cr_id = $_SESSION['conference']['pre_reg']['id'];
$cr_row = if_get_pre_registration($cr_id);
$conference_id = $cr_row['conference_id'];
$is_member = $cr_row['is_member'];
$is_graduate = $cr_row['is_graduate'];
$user_name = $cr_row['user_name'];
$org_name = $cr_row['org_name'];
$user_mobile = $cr_row['user_mobile'];
$user_email = $cr_row['user_email'];
$fee_amount = $cr_row['fee_amount'];
$register_dt = $cr_row['register_dt'];
$cr_meta_data = json_decode($cr_row['meta_data'], true);
$bank_name = $cr_meta_data['bank_name'];
$org_phone = @$cr_meta_data['org_phone'];

// 행사
$row = if_get_event($conference_id);

$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];
$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>

			<h3><span class="label label-success">사전등록 조회</span></h3>
			
			<table class="table">
				<tr>
					<td>분류</td>
					<td>
						<?php echo $if_event_type[$event_type] ?>
					</td>
				</tr>
				<tr>
					<td>회원 여부</td>
					<td>
						<?php echo $if_is_member[$is_member] ?>
					</td>
				</tr>
				<tr>
					<td>대학원 재학 여부</td>
					<td>
						<?php echo $if_is_graduate[$is_graduate] ?>
					</td>
				</tr>
				<tr>
					<td>행사명</td>
					<td>
						<?php echo $event_name ?>
					</td>
				</tr>
				<tr>
					<td>이름</td>
					<td>
						<?php echo $user_name ?>
					</td>
				</tr>
				<tr>
					<td>소속(직장)</td>
					<td>
						<?php echo $org_name ?>
					</td>
				</tr>
				<tr>
					<td>직장전화</td>
					<td>
						<?php echo $org_phone ?>
					</td>
				</tr>
				<tr>
					<td>휴대전화</td>
					<td>
						<?php echo $user_mobile ?>
					</td>
				</tr>
				<tr>
					<td>이메일</td>
					<td>
						<?php echo $user_email ?>
					</td>
				</tr>
				<tr>
					<td>등록비(환불불가)</td>
					<td>
						<?php echo number_format($fee_amount) ?>원
					</td>
				</tr>
				<tr>
					<td>계좌정보</td>
					<td>
						<?php echo $bank_name ?>
					</td>
				</tr>
				<tr>
					<td>사전등록 기간</td>
					<td>
						<?php echo substr($pre_reg_from, 0, 16) ?> ~ <?php echo substr($pre_reg_to, 0, 16) ?>
					</td>
				</tr>
			</table>
			
			<div>
				<div class="col-sm-offset-2 col-sm-4">
					<a href="nm_pre_register_edit.php" class="btn btn-primary">수정</a> &nbsp;
					<a href="signin.php" class="btn btn-default">취소</a>
				</div>
			</div>
			
		</div> <!-- /container -->
		
<?php 
require_once INC_PATH . '/front-footer.php';
?>