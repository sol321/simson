<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';
$on4 = 'on';
$left = '학술·교육·행사';
$title = '등록 / 접수 확인(초록접수)';
if(!empty($_GET['memb']) && $_GET['memb'] == 1) {
    $user_id = if_get_current_user_id();
    if(!empty($user_id)){
        $ca_row = if_get_abstract_by_id($event_data['seq_id'],$user_id);
    }
    if(empty($ca_row['ca_id'])){
        $msg = "초록 등록 정보가 없습니다.";
        $url = "../conference.php";
        //if_js_alert_move($msg, $url);exit;
        if_js_alert_back($msg);exit;
    }
    $modify_url = "abst_edit.php?memb=1";
} else {
    if (empty($_SESSION['conference']['abstract']['id'])) {
        if_js_alert_back('초록 등록 정보가 존재하지 않습니다.');
    }
    $ca_id = $_SESSION['conference']['abstract']['id'];
    $ca_row = if_get_abstract($ca_id);
    
    if(empty($ca_row['ca_id'])){
        $msg = "초록등록 정보가 없습니다.";
        $url = "../conference.php";
        //if_js_alert_move($msg, $url);exit;
        if_js_alert_back($msg);exit;
    }
    $modify_url = "abst_edit.php";
}

$is_member = $ca_row['is_member'];
$is_graduate = $ca_row['is_graduate'];
$academy = $ca_row['academy'];
$academy_arr = explode(',', $academy);
$author_name = $ca_row['author_name'];
$author_mobile = $ca_row['author_mobile'];
$author_email = $ca_row['author_email'];
$org_name = $ca_row['org_name'];
$co_author_name = $ca_row['co_author_name'];
$pt_type = $ca_row['pt_type'];
$abstract_title = $ca_row['abstract_title'];
$abstract_class = $ca_row['abstract_class'];
$abstract_class_arr = explode(',', $ca_row['abstract_class']);
$apply_dt = $ca_row['apply_dt'];
$update_dt = $ca_row['update_dt'];
$user_id = $ca_row['user_id'];
$conference_id = $ca_row['conference_id'];

$meta_data = $ca_row['meta_data'];
$jdec = json_decode($meta_data, true);
$org_phone = $jdec['org_phone'];
$passwd = $jdec['passwd'];
$file_attachment = $jdec['file_attachment'];

// 회원학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
    			<div class="table-wrap">
        			<table class="cst-table border-table regi-table">
        				<caption>초록접수 확인</caption>
						<colgroup>
							<col style="width: 20%;">
							<col style="width: 80%;">
						</colgroup>        			
        				<tr>
        					<td>회원 여부</td>
        					<td>
        						<?php echo $if_is_member[$is_member] ?>
        					</td>
        				</tr>
        				<tr>
        					<td>대학원 재학 여부</td>
        					<td>
        						<?php echo $if_is_graduate[$is_graduate] ?>
        					</td>
        				</tr>
        				<tr>
        					<td>행사명</td>
        					<td>
        						<?php echo $event_name ?>
        					</td>
        				</tr>
        				<tr>
        					<td>회원학회명</td>
        					<td>
        						<?php echo implode(", ",$academy_arr); ?>
        					</td>
        				</tr>
        				<tr>
        					<td>이름</td>
        					<td>
        						<?php echo $author_name ?>
        					</td>
        				</tr>
        				<tr>
        					<td>소속(직장)</td>
        					<td>
        						<?php echo $org_name ?>
        					</td>
        				</tr>
        				<tr>
        					<td>발표형태</td>
        					<td>
        						<?php echo $if_pt_type[$pt_type] ?>
        					</td>
        				</tr>
        				<tr>
        					<td rowspan="3">연락처</td>
        					<td>
        						직장전화 : <?php echo $org_phone ?>
        					</td>
        				</tr>
        				<tr>
        					<td>핸드폰 : <?php echo $author_mobile ?></td>
        				</tr>
        				<tr>
        					<td>이메일 : <?php echo $author_email ?></td>
        				</tr>
        				<tr>
        					<td>공저자명</td>
        					<td>
        						<?php echo $co_author_name ?>
        					</td>
        				</tr>
        				<tr>
        					<td>논문제목</td>
        					<td>
        						<?php echo $abstract_title ?>
        					</td>
        				</tr>
        				<tr>
        					<td>초록분류</td>
        					<td>
        						<?php echo implode(", ",$abstract_class_arr) ?>
        					</td>
        				</tr>
        				<tr>
        					<td>논문 파일</td>
        					<td>
            				<?php
    						if (!empty($file_attachment)) {
    							foreach ($file_attachment as $key => $val) {
    								$attach_file_path = $val['file_path'];
    								$attach_file_url = $val['file_url'];
    								$attach_file_name = $val['file_name'];
    								if (is_file($attach_file_path)) {
    						?>
									<a href="<?php echo INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($attach_file_path) ?>&fn=<?php echo base64_encode($attach_file_name) ?>"  title="<?php echo $attach_file_name ?>">
										<?php echo $attach_file_name ?>
									</a><br>
    						<?php
    								}
    							}
    						}
    						?>
        					</td>
        				</tr>
        			</table>
    			</div>
				<div class="btn-wrap text-center mt50">
					<a href="<?php echo $modify_url;?>" class="btn btn-primary">수정</a> &nbsp;
					<a href="confirm.php" class="btn btn-default">취소</a>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>