<?php
/*
 * Desc : 비회원 사전등록 편집
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if (empty($_SESSION['conference']['pre_reg']['id'])) {
    if_js_alert_back('사전 등록 정보가 존재하지 않습니다.');
}

$cr_id = $_SESSION['conference']['pre_reg']['id'];
$cr_row = if_get_pre_registration($cr_id);
$conference_id = $cr_row['conference_id'];
$is_member = $cr_row['is_member'];
$is_graduate = $cr_row['is_graduate'];
$user_name = $cr_row['user_name'];
$org_name = $cr_row['org_name'];
$user_mobile = $cr_row['user_mobile'];
$user_email = $cr_row['user_email'];
$fee_amount = $cr_row['fee_amount'];
$register_dt = $cr_row['register_dt'];
$cr_meta_data = json_decode($cr_row['meta_data'], true);
$bank_name = $cr_meta_data['bank_name'];
$org_phone = @$cr_meta_data['org_phone'];

// 행사
$row = if_get_event($conference_id);

$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];
$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>

			<form id="form-item-new" class="form-horizontal">
				<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $seq_id ?>">
				<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
				
				<h3><span class="label label-success">사전등록 수정</span></h3>
				
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label">분류</label>
						<div class="col-sm-10">
							<?php echo $if_event_type[$event_type] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">회원 여부</label>
						<div class="col-sm-10">
							<?php echo $if_is_member[$is_member] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">대학원 재학 여부</label>
						<div class="col-sm-10">
							<?php echo $if_is_graduate[$is_graduate] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">행사명</label>
						<div class="col-sm-4">
							<?php echo $event_name ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">이름</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_name" id="user_name" value="<?php echo $user_name ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">소속(직장)</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">직장전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo $org_phone ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">휴대전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_mobile" id="user_mobile" value="<?php echo $user_mobile ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">이메일</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_email" id="user_email" value="<?php echo $user_email ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">등록비(환불불가)</label>
						<div class="col-sm-4">
							<?php echo number_format($fee_amount) ?>원
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">입금 계좌정보</label>
						<div class="col-sm-4">
							<?php echo $bank_name ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-4">
						<button type="submit" id="btn-submit" class="btn btn-primary">저장</button> &nbsp;
						<a href="index.php" class="btn btn-default">취소</a>
					</div>
				</div>
			</form>
		</div> <!-- /container -->
		
		<script>
		$(function() {
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/nm-pre-register-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "signin.php";
							} else {
								alert("등록하지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 등록비 계산
			$(".is_member, .is_graduate").click(function() {
				var mval = $(".is_member:checked").val();
				var gval = $(".is_graduate:checked").val();

				if (mval && gval) {
					var fkey = mval + "-" + gval;

					$.ajax({
						type : "POST",
						url : "./ajax/get-user-class-fee.php",
						data : {
							"id" : $("#seq_id").val(),
							"key" : fkey
						},
						dataType : "json",
						success : function(res) {
							if (res.code == "0") {
								$("#pre_reg_fee").val(res.amount);
							} else {
								alert("등록비 정보를 가져오지 못했습니다.");
							}
						}
					});	
				}
			});
		});
		</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>