<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';
$on4 = 'on';
$left = '학술·교육·행사';
$title = '등록 / 접수 확인';
$user_id = if_get_current_user_id();
if(!empty($user_id)){
    $regist_row = if_get_pre_registration_by_id($event_data['seq_id'],$user_id);
} 

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="regi-wrap confirm contents-box-wrap">
					<div class="contents-box n2">
						<form id="form-signin-pre-reg">
						<input type="hidden" name="user_id" id="user_id1" value="<?php echo $user_id;?>">
						<input type="hidden" name="conference_id" id="conference_id1" value="<?php echo $event_data['seq_id'];?>">
						<input type="hidden" name="form_type" id="form_type1" value="pre_registration">
							<span class="tit">사전등록 확인</span>
							<div class="radio">
								<label class="radio-inline chk-mem1">
									<input type="radio" name="user_type" id="user_type_reg1" value="member" class="utype_reg"> 회원
								</label>
								<label class="radio-inline chk-mem1">
									<input type="radio" name="user_type" id="user_type_reg2" value="non_member" class="utype_reg" checked> 비회원
								</label>
							</div>
							<ul class="regi-input">
								<li class="chk-mem-email">
									<label for="">E-mail</label>
									<input type="email" id="user_email1" name="user_email" class="form-control">									
								</li>							
								<li class="chk-mem-pw">
									<label for="">비밀번호</label>
									<input type="password" name="passwd" id="passwd1" class="form-control" placeholder="비밀번호" maxlength="20" autocomplete="new-password">
								</li>
							</ul>
							<div class="btn-wrap text-right">
								<button type="submit" class="btn btn-primary">확인</button>
							</div>
						</form>				
					</div>
					<div class="contents-box n2">
						<form id="form-signin-abst">
    						<input type="hidden" name="user_id" id="user_id2" value="<?php echo if_get_current_user_id();?>">
    						<input type="hidden" name="conference_id" id="conference_id2" value="<?php echo $event_data['seq_id'];?>">
    						<input type="hidden" name="form_type" id="form_type2" value="abstract">						
							<span class="tit">초록접수 확인</span>
							<div class="radio">
								<label class="radio-inline chk-mem2">
									<input type="radio" name="user_type" id="user_type_abst1" value="member" class="utype_reg2"> 회원
								</label>
								<label class="radio-inline chk-mem2">
									<input type="radio" name="user_type" id="user_type_abst2" value="non_member" class="utype_reg2" checked> 비회원
								</label>
							</div>		
							<ul class="regi-input">
								<li class="chk-mem-email">
									<label for="">E-mail</label>
									<input type="email" id="user_email2" name="user_email" class="form-control">									
								</li>							
								<li class="chk-mem-pw">
									<label for="">비밀번호</label>
									<input type="password" id="passwd2" name="passwd" class="form-control" autocomplete="new-password">
								</li>
							</ul>	
							<div class="btn-wrap text-right">
								<button type="submit" class="btn btn-primary">확인</button>
							</div>
						</form>				
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$(".utype_reg").click(function() {
		var user_chk = $("#user_id1").val();
		if ($(this).val() == "member") {
			if(user_chk) {
				location.href = "regist_view.php?memb=1";
			} else {
    			var encReferer = btoa("/content/conference/pre_register_list.php");
    			location.href = "../../member/login.php?redirect=" + encReferer;
			}
		}
	});
	$(".utype_reg2").click(function() {
		var user_chk = $("#user_id1").val();
		if ($(this).val() == "member") {
			if(user_chk) {
				location.href = "abst_view.php?memb=1";
			} else {
    			var encReferer = btoa("/content/conference/pre_register_list.php");
    			location.href = "../../member/login.php?redirect=" + encReferer;
			}
		}
	});

	$("#form-signin-pre-reg, #form-signin-abst").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/signin.php",
			data : $(this).serialize(),
			dataType : "json",
			success : function(res) {
				if (res.code == "0") {
					location.href = res.redirect;
					//location.href = "pre_register_list.php";
				} else {
					alert(res.msg);
				}
			}
		});
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>