<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-user.php';

if (empty($_GET['eid'])) {
	if_js_alert_back('행사 정보가 필요합니다.');
}

$conference_id = $_GET['eid'];
$row = if_get_event($conference_id);

$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];

$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$pre_reg_max_attendees = $jdec['pre_reg_max_attendees'];
// $pre_reg_content = $jdec['pre_reg_content'];
// $user_class_fee = $jdec['user_class_fee'];

// 무통장입금 계좌 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec_bank = json_decode($if_bank_account_data, true);

// 회원일 경우 정보 조회
$user_id = if_get_current_user_id();
if ($user_id) {
	$user_row = if_get_user_by_id($user_id);
	
	$name_ko = $user_row['name_ko'];
	$user_email = $user_row['user_email'];
	$user_mobile = $user_row['user_mobile'];
	$org_name = $user_row['org_name'];
	$meta_data = $user_row['meta_data'];
	$jdec = json_decode($meta_data, true);
	$org_phone = $jdec['org_phone'];
}

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>

			<form id="form-item-new" class="form-horizontal">
				<input type="hidden" name="conference_id" id="conference_id" value="<?php echo $conference_id ?>">
				<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
				
				<h3><span class="label label-success">사전등록 신청</span></h3>
				
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label">분류</label>
						<div class="col-sm-10">
							<?php echo $if_event_type[$event_type] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">회원 여부</label>
						<div class="col-sm-10">
							<div class="radio">
				<?php 
				foreach ($if_is_member as $key => $val) {
					$checked = !empty($user_id) && $key == '30' ? 'checked' : ''; 
				?>
								<label>
									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
									&nbsp; &nbsp;
								</label>
				<?php 
				}
				?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">대학원 재학 여부</label>
						<div class="col-sm-10">
							<div class="radio">
            				<?php 
            				foreach ($if_is_graduate as $key => $val) {
            				?>
							<label>
								<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>"><?php echo $val ?>
								&nbsp; &nbsp;
							</label>
            				<?php 
            				}
            				?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">행사명</label>
						<div class="col-sm-4">
							<?php echo $event_name ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">이름</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_name" id="user_name" value="<?php echo @$name_ko ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">소속(직장)</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo @$org_name ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">직장전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo @$org_phone ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">휴대전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_mobile" id="user_mobile" value="<?php echo @$user_mobile ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">이메일</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="user_email" id="user_email" value="<?php echo @$user_email ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">등록비(환불불가)</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="pre_reg_fee" id="pre_reg_fee" readonly>
						</div>
					</div>
            		<?php 
            		if (empty($user_id)) {    // 비회원
            		?>
					<div class="form-group">
						<label class="col-sm-2 control-label">비밀번호</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" name="passwd" id="passwd" maxlength="20">
							<div class="help-block">비회원으로 신청 시, 이메일과 비밀번호로 신청내용을 조회할 수 있습니다.</div>
						</div>
					</div>
            		<?php 
            		}
            		?>
					<div class="alert alert-success">
						<p>사전등록 기간 : <?php echo substr($pre_reg_from, 0, 16) ?> ~ <?php echo substr($pre_reg_to, 0, 16) ?></p>
						<p>참석자를 <b><?php echo number_format($pre_reg_max_attendees) ?></b>명으로 제한하오니, ... </p>
						<div>송금계좌 :
						
        			   <?php 
        				if (!empty($jdec_bank)) {
        					foreach ($jdec_bank as $key => $val) {
        						foreach ($val as $k => $v) {
        							$exp = explode("\t", $v);
        							$opt_text = $exp[0] . ' ' . $exp[1] . ' ' . $exp[2];
        				?>
        							<div class="radio">
        								<label>
        									<input type="radio" name="bank_name" class="bank_name" value="<?php echo $opt_text ?>"> <?php echo $opt_text ?>
        								</label>
        							</div> 
        				<?php 
        						}
        					}
        				}
        				?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-4">
						<button type="submit" id="btn-submit" class="btn btn-primary">등록</button> &nbsp;
						<a href="index.php" class="btn btn-default">취소</a>
					</div>
				</div>
			</form>
		</div> <!-- /container -->
		
		<script>
		$(function() {
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/pre-register-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "signin.php";
							} else {
								alert("등록하지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 등록비 계산
			$(".is_member, .is_graduate").click(function() {
				var mval = $(".is_member:checked").val();
				var gval = $(".is_graduate:checked").val();

				if (mval && gval) {
					var fkey = mval + "-" + gval;

					$.ajax({
						type : "POST",
						url : "./ajax/get-user-class-fee.php",
						data : {
							"id" : $("#conference_id").val(),
							"key" : fkey
						},
						dataType : "json",
						success : function(res) {
							if (res.code == "0") {
								$("#pre_reg_fee").val(res.amount);
							} else {
								alert("등록비 정보를 가져오지 못했습니다.");
							}
						}
					});	
				}
			});
		});
		</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>