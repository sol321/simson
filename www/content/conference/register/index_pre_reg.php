<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if (empty($_GET['id'])) {
    $seq_id = if_get_latest_event_id();
} else {
    $seq_id = $_GET['id'];
}

$row = if_get_event($seq_id);
$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$pre_reg_content = $jdec['pre_reg_content'];

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>
			
			<div class="jumbotron">
				<?php echo $pre_reg_content ?>
			</div>

			<div class="col-sm-6">
				<div class="col-sm-6">
					<a href="javascript:;" class="btn btn-success">회원 사전 등록</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="col-sm-6">
					<a href="pre_register_add.php?eid=<?php echo $seq_id ?>" class="btn btn-info">비회원 사전 등록</a>
				</div>
			</div>
        		
		</div> <!-- /container -->
		
<?php 
require_once INC_PATH . '/front-footer.php';
?>