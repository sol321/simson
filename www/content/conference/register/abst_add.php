<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';

if($now < $event_data['abstract_from'] || $now > $event_data['abstract_to']) {
    $msg = "초록접수 기간이 아닙니다.";
    $url = "index_reg.php";
    //if_js_alert_move($msg, $url);exit;
    if_js_alert_back($msg);exit;
}

// 회원학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);

$abstract_classification = $event_meta['abstract_classification'];

if(!empty($_GET['memb']) && $_GET['memb'] == "1") {
    // 회원일 경우 정보 조회
    $user_id = if_get_current_user_id();
    if ($user_id) {
        $user_row = if_get_user_by_id($user_id);
        
        $user_name = $user_row['name_ko'];
        $user_email = $user_row['user_email'];
        $user_mobile = $user_row['user_mobile'];
        $org_name = $user_row['org_name'];
        $meta_data = $user_row['meta_data'];
        $jdec = json_decode($meta_data, true);
        $org_phone = $jdec['org_phone'];
        
        //회원의 경우 이미 등록했으면 등록/접수 확인 페이지로 이동
        $regist_row = if_get_abstract_by_id($event_data['seq_id'],$user_id);
        if(!empty($regist_row['ca_id'])) {
            $msg = "이미  초록접수를 완료하였습니다.";
            if_js_alert_move($msg,"confirm.php");exit;
        }
    } else {
        if_authenticate_user();
    }
} else{
    $user_id = if_get_current_user_id();
    if(!empty($user_id)) {
        $msg = "회원초록 등록을 이용해주세요.";
        if_js_alert_back($msg);exit;
    }
}
if(empty($user_name)) $user_name = "";
if(empty($user_email)) $user_email = "";
if(empty($user_mobile)) $user_mobile = "";
if(empty($org_name)) $org_name = "";
if(empty($meta_data)) $meta_data = "";
if(empty($org_phone)) $org_phone = "";


$on3 = 'on';
$left = '학술·교육·행사';
$title = '초록접수';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<form id="form-item-new" class="form-inline">
					<input type="hidden" name="conference_id" id="conference_id" value="<?php echo $event_data['seq_id'] ?>">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
					<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $event_data['seq_id'] ?>">				
					<span class="tit dot">초록접수 신청</span>
					<div class="table-wrap">
						<table class="cst-table border-table regi-table">
							<caption>초록접수 신청</caption>
							<colgroup>
								<col style="width: 20%;">
								<col style="width: 80%;">
							</colgroup>
							<tbody>
								<tr >
									<th scope="row" class="text-left">학회회원 여부</th>
									<td class="text-left">
										<div class="radio" style="display:none;">
                            				<?php 
                            				foreach ($if_is_member as $key => $val) {
                            				    $checked = !empty($user_id) && $key == '30' ? 'checked' : ( (empty($user_id) && $key == '10')? 'checked':''); 
                            				?>
            								<label class="radio-inline">
            									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
                            				<?php 
                            				}
                            				?>											
										</div>
										<div>
											<?php echo ($user_id)?"회원":"비회원"?>
										</div>																							
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">대학원 재학 여부</th>
									<td class="text-left">
										<div class="radio">
                        				<?php 
                        				foreach ($if_is_graduate as $key => $val) {
                        				?>
        								<label class="radio-inline">
        									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>"><?php echo $val ?>
        									&nbsp; &nbsp;
        								</label>
                        				<?php 
                        				}
                        				?>										
										</div>											
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">학술·교육·행사 명</th>
									<td class="text-left"><?php echo $event_name ?></td>
								</tr>
								<tr>
									<th scope="row" class="text-left">회원학회명</th>
									<td class="text-left">
										<div class="checkbox">
                            				<?php 
                                            if ( !empty($unit_items) ) {
                                                $unit_cnt = 1;
                                                foreach ($unit_items as $key => $val ) {
                                            ?>
                                        	<label class="checkbox-inline">
                                        		<input type="checkbox" name="member_academy[]" value="<?php echo $val ?>">
                                        		<?php echo $val ?>
                                        	</label><?php if($unit_cnt % 4 == 0){ echo "<br>";}?>
                            				<?php 
                            				    $unit_cnt++;
                                                }
                                            }
                            				?>																							
										</div>		
									</td>
								</tr>

								<tr>
									<th scope="row" class="text-left">성명</th>
									<td class="text-left">
										<input type="text" class="form-control" name="author_name" id="author_name" value="<?php echo @$name_ko ?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">소속(직장)</th>
									<td class="text-left">
											<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo @$org_name ?>">
									</td>
								</tr>
								
								<tr>
									<th scope="row" class="text-left">발표형태</th>
									<td class="text-left">
										<div class="radio">
                        				<?php 
                        				foreach ($if_pt_type as $key => $val) {
                        				?>
        								<label class="radio-inline">
        									<input type="radio" name="pt_type" class="pt_type" value="<?php echo $key ?>"> <?php echo $val ?>
        								</label>
                        				<?php 
                        				}
                        				?>										
										</div>		
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="3" class="text-left">연락처</th>
									<td class="text-left">
										직장전화 :&nbsp; <input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo @$org_phone ?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										핸드폰 :&nbsp; <input type="text" class="form-control" name="author_mobile" id="author_mobile" value="<?php echo @$user_mobile ?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										이메일 :&nbsp; <input type="text" class="form-control" name="author_email" id="author_email" value="<?php echo @$user_email ?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">공저자명</th>
									<td class="text-left">
										<input type="text" class="form-control" name="co_author_name" id="co_author_name">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">논문제목</th>
									<td class="text-left">
										<input type="text" class="form-control" name="abstract_title" id="abstract_title">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">초록분류</th>
									<td class="text-left">
										* 가장 가까운 분야를 선택하세요.<br>
										<div class="checkbox">
            							<?php 
            							if (!empty($abstract_classification)) {
                        				    foreach ($abstract_classification as $key => $val) {
                        				?>
                          					<label class="checkbox-inline">
        										<input type="checkbox" name="abstract_class[]" value="<?php echo $val ?>">
        										<?php echo $val ?>
        									</label>
                        				<?php
                        				    }
                        				}
                        				?>							
										</div>		
									</td>
								</tr>
                        		<?php 
                        		if (empty($user_id)) {    // 비회원
                        		?>								
								<tr>
									<th scope="row" class="text-left">비밀번호</th>
									<td class="text-left">								

        							<input type="password" class="form-control" name="passwd" id="passwd" maxlength="20" autocomplete="new-password">
        							<div class="help-block">비회원으로 신청 시, 이메일과 비밀번호로 신청내용을 조회할 수 있습니다.</div>
 									</td>
								</tr>    
                        		<?php 
                        		}
                        		?>									                   									
								<tr>
									<th scope="row" class="text-left">논문 파일</th>
									<td class="text-left">
    									<div class="btn btn-success btn-upload-attach">
    										<i class="fa fa-upload" aria-hidden="true"></i> 파일 첨부
    									</div>									
    									<input type="file" name="attachment[]" id="abstract_file" class="hide" multiple>	
    									<ul class="list-group" id="preview-attachment"></ul>							
 									</td>
								</tr>                       									
							</tbody>
						</table>
					</div>
					<div class="border-box mt30">
						<span class="tit">[초록 작성시 주의사항]</span>
						<ul class="list-type1">
							<li>초록 본문의 글자체는 신명조, 글자크기 10, 줄간격 160, 상하좌우 여백 30mm, 양쪽 정렬, A4 용지 1장 분량입니다.(1장 이상의 논문은 접수를 받지 않습니다.)</li>
							<li>초록은 국문 초록으로 연구의 필요성 및 목적, 연구방법, 연구결과, 결론의 순으로 위의 양식과 같이 단락을 구분하여 기재하며, 초록 내에 번호나 참고문헌의 표시는 하지 마십시오.</li>
						</ul>
					</div>
					<div class="btn-wrap text-center mt50">
						<button type="submit" id="btn-submit" class="btn btn-primary bg-blue btn-big">등록</button> &nbsp;
						<a href="<?php echo CONTENT_URL ?>/conference/conference.php"  class="btn btn-default bg-grey btn-big">취소</a>
					</div>					
				</form>
			</div>
		</article>
	</div>
</section>
<!-- Form -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
<script>
$(function() {
	// 첨부파일 trigger
	$(".btn-upload-attach").click(function() {
		$("#abstract_file").click();
	});
	
	$("#form-item-new").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/abstract-add.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("초록접수가 완료되었습니다.");
						location.href = "../conference.php";
					} else {
						alert("등록하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});

	// 첨부파일 Upload
	$("#abstract_file").change(function() {
		var fsize = this.files[0].size;
		var fname = this.files[0].name;
		var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

		$("#form-item-new").ajaxSubmit({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
			dataType : "json",
			success: function(xhr) {
				if (xhr.code == "0") {
					var fileLists = '';
					for (var i = 0; i < xhr.file_url.length; i++) {
						fileLists += '<li class="list-group-item list-group-item-success">' +
											'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
											'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
											'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
											'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
											xhr.file_name[i] +
										'</li>';
					}
					$("#preview-attachment").append(fileLists);
				} else {
					alert(xhr.msg);
				}
				$('input[name="attachment[]"]').val("");
			}
		});
	});
	//-- 첨부파일 Upload 시작

	// 첨부파일 삭제
	$(document).on("click", ".list-group-item .delete-file-attach", function() {
		var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

		$.ajax({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
			data : {
				"filePath" : file
			},
			dataType : "json",
			success : function(res) {
				$("#preview-attachment li").each(function(idx) {
					var file = $(this).find('input[name="file_path[]"]').val();
					if (file == res.file_path) {
						$(this).fadeOut("slow", function() { $(this).remove(); });
					}
				});
				if (res.code != "0") {
					alert(res.msg);
				}
			}
		});
	});
	//-- 첨부파일 삭제
	//-------------------- 첨부파일 끝
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>