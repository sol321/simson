<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if (empty($_GET['id'])) {
    $seq_id = if_get_latest_event_id();
} else {
    $seq_id = $_GET['id'];
}

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>
			
			<div class="col-sm-6">
				
				<div class="panel panel-default">
					<div class="panel-header">
						<h3>사전등록 확인</h3>
					</div>
					<div class="panel-body">
    					<form class="form-horizontal" id="form-signin-pre-reg">
    						<input type="hidden" name="conference_id" value="<?php echo $seq_id ?>">
    						<input type="hidden" name="form_type" value="pre_registration">
    						<div class="form-group">
    							<div class="col-sm-offset-2 col-sm-10">
        							<div class="radio">
        								<label>
        									<input type="radio" name="user_type" value="member" class="utype_reg"> 회원
        								</label>
        								&nbsp; &nbsp; &nbsp;
        								<label>
        									<input type="radio" name="user_type" value="non_member" class="utype_reg" checked> 비회원
        								</label>
        							</div>
        						</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label">이메일(Email)</label>
    							<div class="col-sm-8">
    								<input type="email" name="user_email" class="form-control" placeholder="이메일">
    							</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label">비밀번호</label>
    							<div class="col-sm-8">
    								<input type="password" name="passwd" class="form-control" placeholder="비밀번호" maxlength="20" autocomplete="new-password">
    							</div>
    						</div>
    						<div class="form-group">
    							<label class="col-sm-4 control-label"></label>
    							<div class="col-sm-8">
    								<button type="submit" class="btn btn-primary">확인</button>
    							</div>
    						</div>
    					</form>
    				</div>
				</div>
			
			</div>
			<div class="col-sm-6">
				<div class="col-sm-6">
					<a href="pre_register_add.php?eid=<?php echo $seq_id ?>" class="btn btn-info">비회원 사전 등록</a>
				</div>
			</div>
        		
		</div> <!-- /container -->
		
		<script>
		$(function() {
			$(".utype_reg").click(function() {
				if ($(this).val() == "member") {
					var encReferer = btoa("/content/conference/pre_register_list.php");
					location.href = "../../member/login.php?referer=" + encReferer;
				}
			});

			$("#form-signin-pre-reg").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/signin.php",
					data : $(this).serialize(),
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							location.href = res.redirect;
// 							location.href = "pre_register_list.php";
						} else {
							alert(res.msg);
						}
					}
				});
			});
		});
		</script>
		
<?php 
require_once INC_PATH . '/front-footer.php';
?>