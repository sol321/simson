<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';

if($now < $event_data['pre_reg_from'] || $now > $event_data['pre_reg_to']) {
    $msg = "사전등록 기간이 아닙니다.";
    $url = "index_reg.php";
    //if_js_alert_move($msg, $url);exit;
    if_js_alert_back($msg);exit;
}

if(!empty($_GET['memb']) && $_GET['memb'] == "1") {
    // 회원일 경우 정보 조회
    $user_id = if_get_current_user_id();
    if ($user_id) {
        $user_row = if_get_user_by_id($user_id);
        
        $user_name = $user_row['name_ko'];
        $user_email = $user_row['user_email'];
        $user_mobile = $user_row['user_mobile'];
        $org_name = $user_row['org_name'];
        $meta_data = $user_row['meta_data'];
        $jdec = json_decode($meta_data, true);
        $org_phone = $jdec['org_phone'];
        
        //회원의 경우 이미 등록했으면 등록/접수 확인 페이지로 이동
        $regist_row = if_get_pre_registration_by_id($event_data['seq_id'],$user_id);
        if(!empty($regist_row['cr_id'])) {
            $msg = "이미 사전등록 완료하였습니다.";
            if_js_alert_move($msg,"confirm.php");exit;
        }
    } else {
        if_authenticate_user();
    }
} else{
    $user_id = if_get_current_user_id();
    if(!empty($user_id)) {
        $msg = "회원사전등록을 이용해주세요.";
        if_js_alert_back($msg);exit;
    }
}

// 무통장입금 계좌 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec_bank = json_decode($if_bank_account_data, true);

$on2 = 'on';
$left = '학술·교육·행사';
$title = '사전등록';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<form class="form-inline" id="form-item-new">
					<input type="hidden" name="conference_id" id="conference_id" value="<?php echo $event_data['seq_id'] ?>">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
					<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $event_data['seq_id'] ?>">
					<span class="tit dot">사전등록 신청</span>
					<div class="table-wrap">
						<table class="cst-table border-table regi-table">
							<caption>사전등록 신청</caption>
							<colgroup>
								<col style="width: 20%;">
								<col style="width: 80%;">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" class="text-left">분류</th>
									<td class="text-left"><?php echo $if_event_type[$event_type] ?></td>
								</tr>
								<tr>
									<th scope="row" class="text-left">회원 여부</th>
									<td class="text-left">
										<div class="radio" style='display:none;'>
                        				<?php 
                        				foreach ($if_is_member as $key => $val) {
                        				    $checked = !empty($user_id) && $key == '30' ? 'checked' : ( (empty($user_id) && $key == '10')? 'checked':''); 
                        				?>
        								<label>
        									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
        									&nbsp; &nbsp;
        								</label>
                        				<?php 
                        				}
                        				?>
										</div>
										<div>
											<?php echo ($user_id)?"회원":"비회원"?>
										</div>											
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">대학원 재학 여부</th>
									<td class="text-left">
										<div class="radio">
                        				<?php 
                        				foreach ($if_is_graduate as $key => $val) {
                        				?>
            								<label>
            									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>"><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
                        				<?php 
                        				}
                        				?>
										</div>											
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">학술·교육·행사 명</th>
									<td class="text-left"><?php echo $event_name ?></td>
								</tr>
								<tr>
									<th scope="row" class="text-left">성명</th>
									<td class="text-left">
											<input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo @$user_name;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">소속(직장)</th>
									<td class="text-left">
											<input type="text" name="org_name" id="org_name" class="form-control" value="<?php echo @$org_name;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="3" class="text-left">연락처</th>
									<td class="text-left">
										직장전화 :&nbsp; <input type="text" name="org_phone" id="org_phone" class="form-control" value="<?php echo @$org_phone;?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										핸드폰 :&nbsp; <input type="text" name="user_mobile" id="user_mobile" class="form-control" value="<?php echo @$user_mobile;?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										이메일 :&nbsp; <input type="email" name="user_email" id="user_email" class="form-control" value="<?php echo @$user_email;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">등록비(환불불가)</th>
									<td class="text-left">
										<input type="text" class="form-control" name="pre_reg_fee" id="pre_reg_fee" readonly> 원
									</td>
								</tr>
                        		<?php 
                        		if (empty($user_id)) {    // 비회원
                        		?>								
								<tr>
									<th scope="row" class="text-left">비밀번호</th>
									<td class="text-left">
										<input type="password" class="form-control" name="passwd" id="passwd" autocomplete="new-password"> 
										<br>비회원으로 신청 시, 이메일과 비밀번호로 신청내용을 조회할 수 있습니다.
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
					<div class="border-box mt30">
						<ul class="list-type1">
							<li>사전등록 기간: <?php echo date_format(date_create($event_data['pre_reg_from']) ,'Y년 m년 d월'); ?> ~ <?php echo date_format(date_create($event_data['pre_reg_to']) ,'Y년 m년 d월'); ?></li>
							<li>세미나의 원활한 진행을 위해 <span style="text-decoration:underline;">참석자를 <strong style="font-weight:bold;"><?php echo number_format($pre_reg_max_attendees) ?>명</strong> 으로 제한</span> 하오니 양해하여 주시기 바랍니다.</li>
							<li>송금계좌:</li>
						</ul>	
                			   <?php 
                				if (!empty($jdec_bank)) {
                					foreach ($jdec_bank as $key => $val) {
                						foreach ($val as $k => $v) {
                							$exp = explode("\t", $v);
                							$opt_text = $exp[0] . ' ' . $exp[1] . ' ' . $exp[2];
                				?>
                							<div class="radio">
                								<label>
                									<input type="radio" name="bank_name" class="bank_name" value="<?php echo $opt_text ?>"> <?php echo $opt_text ?>
                								</label>
                							</div> 
                				<?php 
                						}
                					}
                				}
                				?>							
							
						
					</div>
					<div class="btn-wrap text-center mt50">
						<button type="submit" id="btn-submit" class="btn btn-primary bg-blue btn-big">등록</button> &nbsp;
						<a href="<?php echo CONTENT_URL ?>/conference/conference.php"  class="btn btn-default bg-grey btn-big">취소</a>
					</div>						
				</form>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/pre-register-add.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("등록되었습니다.");
						location.href = "../conference.php";
					} else {
						alert("등록하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});

	// 등록비 계산
	$(".is_member, .is_graduate").click(function() {
		var mval = $(".is_member:checked").val();
		var gval = $(".is_graduate:checked").val();
		var user_chk = $("#user_id").val();
		//회원 여부 고정
		if(user_chk != 0) {
			$(".is_member")[0].checked = true;
			$(".is_member")[1].checked = false;
		} else {
			$(".is_member")[0].checked = false;
			$(".is_member")[1].checked = true;
		}

		if (mval && gval) {
			var fkey = mval + "-" + gval;

			$.ajax({
				type : "POST",
				url : "./ajax/get-user-class-fee.php",
				data : {
					"id" : $("#seq_id").val(),
					"key" : fkey
				},
				dataType : "json",
				success : function(res) {
					if (res.code == "0") {
						$("#pre_reg_fee").val(res.amount);
					} else {
						alert("등록비 정보를 가져오지 못했습니다.");
					}
				}
			});	
		}
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>