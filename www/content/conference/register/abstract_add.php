<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-user.php';

if (empty($_GET['eid'])) {
    if_js_alert_back('행사 정보가 필요합니다.');
}

$seq_id = $_GET['eid'];
$row = if_get_event($seq_id);

$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];

$abstract_from = $row['abstract_from'];
$abstract_to = $row['abstract_to'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$abstract_classification = $jdec['abstract_classification'];

// 회원학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);

// 회원일 경우 정보 조회
$user_id = if_get_current_user_id();
if ($user_id) {
    $user_row = if_get_user_by_id($user_id);
    
    $name_ko = $user_row['name_ko'];
    $user_email = $user_row['user_email'];
    $user_mobile = $user_row['user_mobile'];
    $org_name = $user_row['org_name'];
    $meta_data = $user_row['meta_data'];
    $jdec = json_decode($meta_data, true);
    $org_phone = $jdec['org_phone'];
}

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>

		<div class="container">
			<div class="page-header">
				<h1>학술 교육 행사</h1>
			</div>

			<form id="form-item-new" class="form-horizontal">
				<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $seq_id ?>">
				<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
				
				<h3><span class="label label-warning">초록접수 신청</span></h3>
				
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label">분류</label>
						<div class="col-sm-10">
							<?php echo $if_event_type[$event_type] ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">회원 여부</label>
						<div class="col-sm-10">
							<div class="radio">
				<?php 
				foreach ($if_is_member as $key => $val) {
					$checked = !empty($user_id) && $key == '30' ? 'checked' : ''; 
				?>
								<label>
									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
									&nbsp; &nbsp;
								</label>
				<?php 
				}
				?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">대학원 재학 여부</label>
						<div class="col-sm-10">
							<div class="radio">
				<?php 
				foreach ($if_is_graduate as $key => $val) {
				?>
								<label>
									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>"><?php echo $val ?>
									&nbsp; &nbsp;
								</label>
				<?php 
				}
				?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">행사명</label>
						<div class="col-sm-4">
							<?php echo $event_name ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">회원학회명</label>
						<div class="col-sm-4">
				<?php 
                if ( !empty($unit_items) ) {
                    foreach ($unit_items as $key => $val ) {
                ?>
                			<div class="checkbox">
                            	<label>
                            		<input type="checkbox" name="member_academy[]" value="<?php echo $val ?>">
                            		<?php echo $val ?>
                            	</label>
                            </div>
				<?php 
                    }
                }
				?>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">이름</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="author_name" id="author_name" value="<?php echo @$name_ko ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">소속(직장)</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo @$org_name ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">발표 형태</label>
						<div class="col-sm-4">
							<div class="radio">
				<?php 
				foreach ($if_pt_type as $key => $val) {
				?>
								<label>
									<input type="radio" name="pt_type" class="pt_type" value="<?php echo $key ?>"><?php echo $val ?>
									&nbsp; &nbsp;
								</label>
				<?php 
				}
				?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">직장전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo @$org_phone ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">휴대전화</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="author_mobile" id="author_mobile" value="<?php echo @$user_mobile ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">이메일</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="author_email" id="author_email" value="<?php echo @$user_email ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">공저자명</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="co_author_name" id="co_author_name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">논문 제목</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="abstract_title" id="abstract_title">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">초록 분류</label>
						<div class="col-sm-4">
							<div class="help-block">* 가장 가까운 분야를 선택하세요.</div>
				<?php 
				if (!empty($abstract_classification)) {
				    foreach ($abstract_classification as $key => $val) {
				?>
								<div class="checkbox">
                  					<label>
										<input type="checkbox" name="abstract_class[]" value="<?php echo $val ?>">
										<?php echo $val ?>
									</label>
								</div>
				<?php
				    }
				}
				?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">논문 파일</label>
						<div class="col-sm-4">
							<input type="file" class="form-control" name="attachment[]" id="abstract_file">
							
							<ul class="list-group" id="preview-attachment"></ul>
							
						</div>
					</div>
		<?php 
		if (empty($user_id)) {    // 비회원
		?>
					<div class="form-group">
						<label class="col-sm-2 control-label">비밀번호</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" name="passwd" id="passwd" maxlength="20">
							<div class="help-block">비회원으로 신청 시, 이메일과 비밀번호로 신청내용을 조회할 수 있습니다.</div>
						</div>
					</div>
		<?php 
		}
		?>
					<div class="alert alert-info">
						<p>초록접수 기간 : <?php echo substr($abstract_from, 0, 16) ?> ~ <?php echo substr($abstract_to, 0, 16) ?></p>
						<p>&lt;&lt;초록 작성시 주의사항 &gt;&gt;</p>
						<p>초록 본문의 글자체는 신명조, 글자크기 10, 줄간격 160, 상하좌우 여백 30mm, 양쪽 정렬, A4 용지 1장 분량입니다.(1장 이상의 논문은 접수를 받지 않습니다.)</p>
						<p>초록은 국문 초록으로 연구의 필요성 및 목적, 연구방법, 연구결과, 결론의 순으로 위의 양식과 같이 단락을 구분하여 기재하며, 초록 내에 번호나 참고문헌의 표시는 하지 마십시오.</p>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-4">
						<button type="submit" id="btn-submit" class="btn btn-primary">등록</button> &nbsp;
						<a href="index.php" class="btn btn-default">취소</a>
					</div>
				</div>
			</form>
		</div> <!-- /container -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		
		<script>
		$(function() {
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "signin.php";
							} else {
								alert("등록하지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 첨부파일 Upload
			$("#abstract_file").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
    												'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
    												'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
    												'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
    												'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
    												xhr.file_name[i] +
    											'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작

			// 첨부파일 삭제
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 첨부파일 삭제
			//-------------------- 첨부파일 끝
		});
		</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>