<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['key'])) {
    $code = 102;
    $msg = '회원 여부 및 대학원생 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['id'];
$key = $_POST['key'];

$row = if_get_event($seq_id);

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$max_attendees = $jdec['pre_reg_max_attendees'];
$user_class_fee = $jdec['user_class_fee'];

$amount = $user_class_fee[$key];

$json = compact('code', 'msg', 'amount', 'max_attendees');
echo json_encode($json);

?>
