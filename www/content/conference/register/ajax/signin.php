<?php
/*
 * Desc: 사전등록/초록접수 비회원 로그인
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (empty($_POST['conference_id'])) {
    $code = 1010;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['form_type'])) {
    $code = 101;
    $msg = '유형을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_type'])) {
    $code = 102;
    $msg = '회원 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 103;
    $msg = '이메일 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['passwd'])) {
    $code = 104;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$conference_id = $_POST['conference_id'];
$form_type = $_POST['form_type'];
$user_email = trim($_POST['user_email']);
$passwd  = $_POST['passwd'];

if (!strcmp($form_type, 'pre_registration')) {  // 사전 등록
    $result = if_signin_pre_registration($conference_id, $user_email, $passwd);
    
    // 인증 회원 세션 생성
    $_SESSION['conference']['pre_reg']['id'] = $result;
    
    //$redirect = CONTENT_URL . '/conference/register/nm_pre_register_view.php';
    $redirect = CONTENT_URL . '/conference/register/regist_view.php';
    
} else if (!strcmp($form_type, 'abstract')) {   // 초록 등록
    $result = if_signin_abstract($conference_id, $user_email, $passwd);
    
    // 인증 회원 세션 생성
    $_SESSION['conference']['abstract']['id'] = $result;
    
    $redirect = CONTENT_URL . '/conference/register/abst_view.php';
}

$json = compact('code', 'msg', 'result', 'redirect');
echo json_encode($json);

?>
