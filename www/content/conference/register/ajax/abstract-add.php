<?php
/*
 * Desc: 초록 등록 
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (empty($_POST['seq_id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_member'])) {
    $code = 102;
    $msg = '회원 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_graduate'])) {
    $code = 103;
    $msg = '대학원 재학 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['member_academy'])) {
    $code = 125;
    $msg = '회원학회를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_name'])) {
    $code = 104;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_mobile'])) {
    $code = 105;
    $msg = '핸드폰번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_email'])) {
    $code = 106;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['pt_type'])) {
    $code = 107;
    $msg = '발표 형태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if (empty($_POST['co_author_name'])) {
    $code = 118;
    $msg = '공저자명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['abstract_title'])) {
    $code = 117;
    $msg = '논문제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}


if (empty($_POST['abstract_class'])) {
    $code = 119;
    $msg = '초록분류를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if($_POST['is_member'] == 10) {
    if (empty($_POST['passwd'])) {
        $code = 200;
        $msg = '비밀번호를 입력해주세요.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

if (!isset($_POST['file_path'])) {
    $code = 108;
    $msg = '파일을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];

$row = if_get_event($seq_id);
$abstract_from = $row['abstract_from'];
$abstract_to = $row['abstract_to'];

/*
 * Desc: 초록 접수 기간 유효성 확인
 */
$time_check = if_validate_time_period($abstract_from, $abstract_to);

if ($time_check) {
    $code = 205;
    $msg = $time_check == '1' ? '초록 접수 기간이 아닙니다.' : '초록 접수 기간이 마감되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_abstract();

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
