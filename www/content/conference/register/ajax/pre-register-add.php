<?php
/*
 * Desc: 사전등록  처리
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (empty($_POST['seq_id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_member'])) {
    $code = 102;
    $msg = '회원 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_graduate'])) {
    $code = 103;
    $msg = '대학원 재학 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_name'])) {
    $code = 104;
    $msg = '성명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_mobile'])) {
    $code = 105;
    $msg = '핸드폰 정보를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 106;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['pre_reg_fee'])) {
    $code = 107;
    $msg = '등록비 정보가 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if($_POST['is_member'] == 10) {
    if (empty($_POST['passwd'])) {
        $code = 200;
        $msg = '비밀번호를 입력해주세요.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

if (empty($_POST['bank_name'])) {
    $code = 108;
    $msg = '송금계좌를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];
$key = $_POST['is_member'] . '-' . $_POST['is_graduate'];

$row = if_get_event($seq_id);
$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$max_attendees = $jdec['pre_reg_max_attendees'];
$user_class_fee = $jdec['user_class_fee'];
$user_class_pre_reg_fee = $user_class_fee[$key];

$pre_reg_fee = $_POST['pre_reg_fee'];

if ($pre_reg_fee != $user_class_pre_reg_fee) {
    $code = 201;
    $msg = '등록비가 잘못 계산되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

/*
 * Desc: 사전등록 기간 유효성 확인
 */
$time_check = if_validate_time_period($pre_reg_from, $pre_reg_to);

if ($time_check) {
    $code = 205;
    $msg = $time_check == '1' ? '사전등록 기간이 아닙니다.' : '사전등록 기간이 마감되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

/*
 * Desc: 사전등록 신청자 수 조회
 */
$attendees = if_get_pre_registration_attendees($seq_id);

if (empty($attendees)) {
    $attendees = 0;
}

// 정원 초과 여부 확인
if($max_attendees > 0) {
    if ($max_attendees <= $attendees) {
        $code = 202;
        $msg = '죄송합니다. 정원을 초과했습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_add_pre_registration();

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
