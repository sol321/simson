<?php
require_once '../../../if-config.php';
require_once CONTENT_PATH . '/conference/header.php';
require_once FUNC_PATH . '/functions-user.php';

if($now < $event_data['pre_reg_from'] || $now > $event_data['pre_reg_to']) {
    $msg = "사전등록 기간이 아닙니다.";
    $url = "/";
    //if_js_alert_move($msg, $url);exit;
    if_js_alert_back($msg);exit;
}

if(!empty($_GET['memb']) && $_GET['memb'] == 1) {
    $user_id = if_get_current_user_id();
    if(!empty($user_id)){
        $cr_row = if_get_pre_registration_by_id($event_data['seq_id'],$user_id);
    }
    if(empty($cr_row['cr_id'])){
        $msg = "사전등록 정보가 없습니다.";
        $url = "../conference.php";
        //if_js_alert_move($msg, $url);exit;
        if_js_alert_back($msg);exit;
    }
    $view_url = "regist_view.php?memb=1";
} else {
    if (empty($_SESSION['conference']['pre_reg']['id'])) {
        if_js_alert_back('사전 등록 정보가 존재하지 않습니다.');
    }
    $cr_id = $_SESSION['conference']['pre_reg']['id'];
    $cr_row = if_get_pre_registration($cr_id);
    
    if(empty($cr_row['cr_id'])){
        $msg = "사전등록 정보가 없습니다.";
        $url = "../conference.php";
        //if_js_alert_move($msg, $url);exit;
        if_js_alert_back($msg);exit;
    }
    $view_url = "regist_view.php";
}

if($cr_row['pay_state'] == "9000") {
    $msg = "납부가 완료되어 수정하실 수 없습니다.";
    if_js_alert_back($msg);exit;
}
$conference_id = $cr_row['conference_id'];
$is_member = $cr_row['is_member'];
$is_graduate = $cr_row['is_graduate'];
$user_name = $cr_row['user_name'];
$org_name = $cr_row['org_name'];
$user_mobile = $cr_row['user_mobile'];
$user_email = $cr_row['user_email'];
$fee_amount = $cr_row['fee_amount'];
$register_dt = $cr_row['register_dt'];
$cr_meta_data = json_decode($cr_row['meta_data'], true);
$bank_name = $cr_meta_data['bank_name'];
$org_phone = @$cr_meta_data['org_phone'];

// 무통장입금 계좌 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec_bank = json_decode($if_bank_account_data, true);

$on4 = 'on';
$left = '학술·교육·행사';
$title = '등록/접수 확인(사전등록수정)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once '../left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<form class="form-inline" id="form-item-new">
					<input type="hidden" name="cr_id" id="cr_id" value="<?php echo $cr_row['cr_id'] ?>">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo !empty($user_id)?$user_id:'' ?>">
					<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $event_data['seq_id'] ?>">
					<input type="hidden" name="is_member" id="is_member" value="<?php echo $is_member ?>">
					<span class="tit dot">사전등록 신청</span>
					<div class="table-wrap">
						<table class="cst-table border-table regi-table">
							<caption>사전등록 신청</caption>
							<colgroup>
								<col style="width: 20%;">
								<col style="width: 80%;">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" class="text-left">분류</th>
									<td class="text-left"><?php echo $if_event_type[$event_type] ?></td>
								</tr>
								<tr>
									<th scope="row" class="text-left">회원 여부</th>
									<td class="text-left">
                        				<?php echo $if_is_member[$is_member] ?>
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">대학원 재학 여부</th>
									<td class="text-left">
										<div class="radio">
                        				<?php echo $if_is_graduate[$is_graduate] ?>
										</div>											
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">학술·교육·행사 명</th>
									<td class="text-left"><?php echo $event_name ?></td>
								</tr>
								<tr>
									<th scope="row" class="text-left">성명</th>
									<td class="text-left">
											<input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo $user_name;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">소속(직장)</th>
									<td class="text-left">
											<input type="text" name="org_name" id="org_name" class="form-control" value="<?php echo $org_name;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="3" class="text-left">연락처</th>
									<td class="text-left">
										직장전화 :&nbsp; <input type="text" name="org_phone" id="org_phone" class="form-control" value="<?php echo $org_phone;?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										핸드폰 :&nbsp; <input type="text" name="user_mobile" id="user_mobile" class="form-control" value="<?php echo $user_mobile;?>">
									</td>
								</tr>
								<tr>
									<td class="text-left">
										이메일 :&nbsp; <input type="email" name="user_email" id="user_email" class="form-control" value="<?php echo $user_email;?>">
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">등록비(환불불가)</th>
									<td class="text-left">
										<?php echo number_format($fee_amount) ?>원
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-left">입금계좌</th>
									<td class="text-left">
                                    <?php echo $bank_name ?>
									</td>
								</tr>
                        		<?php 
                        		if (empty($user_id)) {    // 비회원
                        		?>								
								<tr>
									<th scope="row" class="text-left">비밀번호</th>
									<td class="text-left">
										<input type="password" class="form-control" name="passwd" id="passwd" autocomplete="new-password"> 
										<br>비회원으로 신청 시, 이메일과 비밀번호로 신청내용을 조회할 수 있습니다.
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
					<div class="btn-wrap text-center mt50">
						<button type="submit" id="btn-submit" class="btn btn-primary bg-blue btn-big">수정</button> &nbsp;
						<a href="<?php echo CONTENT_URL ?>/conference/register/<?php echo $view_url;?>" class="btn btn-default bg-grey btn-big">취소</a>
					</div>
				</form>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();
		var user_chk = $("#user_id").val();
		$.ajax({
			type : "POST",
			url : "./ajax/pre-register-edit.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("수정되었습니다.");
						if(user_chk) {
							location.href = "regist_view.php?memb=1";
						} else {
							location.href = "regist_view.php";
						}
					} else {
						alert("등록하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>