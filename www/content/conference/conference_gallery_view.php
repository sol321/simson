<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-post.php';

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

$evt_type = empty($_GET['evt_type']) ? 'ACD' : $_GET['evt_type'];  // 분류
if(empty($_GET['conference_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}
if(empty($_GET['gallery_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}
$conference_id = $_GET['conference_id'];
$gallery_id = $_GET['gallery_id'];

$event_data = if_get_event($conference_id);

if(empty($event_data['seq_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}

// 연도 검색
$c_years = if_get_conference_years_by_year();

$gallery_data = if_get_post_gallery($conference_id, $gallery_id);

if(empty($gallery_data['seq_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}

$post_title = htmlspecialchars($gallery_data['post_title']);
$post_content = $gallery_data['post_content'];
$file_attach = if_get_val_from_json($gallery_data['meta_data'], 'file_attachment'); 

/* 게시글 이전글 다음글 */
$pre_post_row = if_get_pre_gallery($conference_id, $gallery_id);
$next_post_row = if_get_next_gallery($conference_id, $gallery_id);

$on5 = 'on';
$left = '학술·교육·행사';
$title = '역대 학술·교육·행사';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3 mb0">
						<li <?php echo ($evt_type == "ACD")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">학술</a></li>
						<li <?php echo ($evt_type == "TRN")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=TRN">교육</a></li>
						<li <?php echo ($evt_type == "EVT")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=EVT">행사</a></li>
					</ul>
				</div>
				<div class="form-inline text-right sel-year">
					<div class="form-group">
						<form id="form-item-new" action="./conference_list.php"  method="GET">
						<input type="hidden" name="evt_type" value="<?php echo $evt_type;?>">
						<select name="qa1" id="qa1" class="form-control" onchange="javascript:submit();">
							<option value="">- 연도 선택 -</option>
            				<?php 
            				if (!empty($c_years)) {
            				    foreach ($c_years as $key => $val) {
            				        $year = $val['event_year'];
            				        $cnt = $val['cnt'];
            				        $selected = $qa1 == $year ? 'selected' : '';
            				?>
							<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?></option>
            				<?php 
            				    }
            				}
            				?>
						</select>
						</form>	
					</div>
				</div>
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type3">
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">프로그램 표</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/conference/conference_gallery.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">갤러리</a></li>
					</ul>
				</div>
				<div class="table-wrap">
					<table class="cst-table gall-view-table">
						<caption>글보기</caption>
						<thead>
							<tr>
								<th scope="col" class="text-left"><?php echo $post_title;?></th>	
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-left p20">
									<?php echo $post_content;?>
								</td>
							</tr>
							<tr>
								<td class="text-left">
									<div class="view-con">
										<?php for($attach=0;$attach<count($file_attach);$attach++) {?>
										<img src="<?php echo $file_attach[$attach]['file_url'];?>" alt="">
										<?php }?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btn-wrap text-right p30">
					<a href="<?php echo CONTENT_URL ?>/conference/conference_gallery.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>" class="btn btn-default">목록</a>
				</div>

				<div class="prevnext table-wrap n-border mt30">
					<table class="cst-table border-table">
						<caption>이전글/다음글</caption>
						<colgroup>
							<col style="width: 20%;">
							<col style="width: 80%;">
						</colgroup>
						<tbody>
							<?php if(!empty($next_post_row)){?>
							<tr>
								<td class="text-left"><i class="xi-angle-up"></i> &nbsp;&nbsp;다음글</td>	
								<td class="text-left">
									<a href="<?php echo CONTENT_URL ?>/conference/conference_gallery_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>&gallery_id=<?php echo $next_post_row['seq_id'];?>" class="btn-link nowrap"><?php echo $next_post_row['post_title'];?></a>
								</td>
							</tr>
							<?php }?>
							<?php if(!empty($pre_post_row)){?>
							<tr>
								<td class="text-left"><i class="xi-angle-down"></i> &nbsp;&nbsp;이전글</td>	
								<td class="text-left">
									<a href="<?php echo CONTENT_URL ?>/conference/conference_gallery_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>&gallery_id=<?php echo $pre_post_row['seq_id'];?>" class="btn-link nowrap"><?php echo $pre_post_row['post_title'];?></a>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>