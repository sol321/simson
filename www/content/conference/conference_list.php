<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';
//require_once CONTENT_PATH . '/conference/header.php';

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

$evt_type = empty($_GET['evt_type']) ? 'ACD' : $_GET['evt_type'];  // 분류
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);

$sql = '';
$pph = '';
$sparam = [];

// 분류
if (!empty($evt_type)) {
    $sql .= " AND event_type = ?";
    array_push($sparam, $evt_type);
}

// 연도별 조회
if (!empty($qa1)) {
    $sql .= " AND event_year = '$qa1'";
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_conference'] . "
		WHERE
			show_hide = 'show'
            AND event_state = 'N'
            $sql
		ORDER BY
			event_year DESC, event_period_to DESC
";

$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

// 연도 검색
$c_years = if_get_conference_years_by_year();
			
			
$on5 = 'on';
$left = '학술·교육·행사';
$title = '역대 학술·교육·행사';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3 mb0">
						<li <?php echo ($evt_type == "ACD")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">학술</a></li>
						<li <?php echo ($evt_type == "TRN")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=TRN">교육</a></li>
						<li <?php echo ($evt_type == "EVT")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=EVT">행사</a></li>
					</ul>
				</div>
				<div class="form-inline text-right sel-year">
					<div class="form-group">
						<form id="form-item-new" action="./conference_list.php"  method="GET">
						<input type="hidden" name="evt_type" value="<?php echo $evt_type;?>">
						<select name="qa1" id="qa1" class="form-control" onchange="javascript:submit();">
							<option value="">- 연도 선택 -</option>
            				<?php 
            				if (!empty($c_years)) {
            				    foreach ($c_years as $key => $val) {
            				        $year = $val['event_year'];
            				        $cnt = $val['cnt'];
            				        $selected = $qa1 == $year ? 'selected' : '';
            				?>
							<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?></option>
            				<?php 
            				    }
            				}
            				?>
						</select>
						</form>						
					</div>
				</div>
				<div class="table-wrap mt20">
					<table class="cst-table m-board">
						<caption>역대 학술·교육·행사</caption>
						<colgroup>
							<col style="width: 15%;">
							<col style="width: 15%;">
							<col>
							<col style="width: 25%;">
							<col style="width: 15%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">연도</th>
								<th scope="col">행사구분</th>
								<th scope="col">행사 명</th>
								<th scope="col">일시</th>
								<th scope="col">장소</th>
							</tr>
						</thead>
						<tbody>
        				<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$seq_id = $val['seq_id'];
    							
    							$event_type = $val['event_type'];
    							$event_name = htmlspecialchars($val['event_name']);
    							$event_year = $val['event_year'];
    							
    							$event_period_from = $val['event_period_from'];
    							$event_period_to = $val['event_period_to'];
    							$event_place = $val['event_place'];
    						?>
							<tr>
								<td><?php echo $event_year ?></td>
								<td><?php echo $if_event_type[$event_type] ?></td>
								<td><a href="<?php echo CONTENT_URL ?>/conference/conference_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $seq_id;?>"><?php echo $event_name ?></a></td>
								<td><?php echo $event_period_from ?> ~ <?php echo $event_period_to ?></td>
								<td><?php echo $event_place ?></td>
							</tr>
    					<?php
    							$list_no--;
    						}
    					} else {
    					?>
    						<tr>
    							<td colspan="5">No data.</td>
    						</tr>
    					<?php 
    					}
    					?>						
						</tbody>
					</table>
				</div>
				<br>
				<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>