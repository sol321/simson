<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

$evt_type = empty($_GET['evt_type']) ? 'ACD' : $_GET['evt_type'];  // 분류
if(empty($_GET['conference_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}
$conference_id = $_GET['conference_id'];
$event_data = if_get_event($conference_id);
if(empty($event_data['seq_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}

// 연도 검색
$c_years = if_get_conference_years_by_year();
$sparam = [];
$sql = " AND post_type_secondary = ? ";
array_push($sparam, $conference_id);
array_unshift($sparam, 'i');
$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_posts'] . "
		WHERE
			post_state = 'open'
            AND post_type = 'conf_gallery'
            $sql
		ORDER BY
			post_order DESC,
			seq_id DESC
";
			
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
			
$on5 = 'on';
$left = '학술·교육·행사';
$title = '역대 학술·교육·행사';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3 mb0">
						<li <?php echo ($evt_type == "ACD")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">학술</a></li>
						<li <?php echo ($evt_type == "TRN")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=TRN">교육</a></li>
						<li <?php echo ($evt_type == "EVT")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=EVT">행사</a></li>
					</ul>
				</div>
				<div class="form-inline text-right sel-year">
					<div class="form-group">
						<form id="form-item-new" action="./conference_list.php"  method="GET">
						<input type="hidden" name="evt_type" value="<?php echo $evt_type;?>">
						<select name="qa1" id="qa1" class="form-control" onchange="javascript:submit();">
							<option value="">- 연도 선택 -</option>
            				<?php 
            				if (!empty($c_years)) {
            				    foreach ($c_years as $key => $val) {
            				        $year = $val['event_year'];
            				        $cnt = $val['cnt'];
            				        $selected = $qa1 == $year ? 'selected' : '';
            				?>
							<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?></option>
            				<?php 
            				    }
            				}
            				?>
						</select>
						</form>	
					</div>
				</div>
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type3">
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">프로그램 표</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/conference/conference_gallery.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">갤러리</a></li>
					</ul>
				</div>
				<div class="gall-wrap">
				<?php
				if (!empty($item_results)) {
				    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
					foreach ($item_results as $key => $val) {
						$post_id = $val['seq_id'];
						$post_title = htmlspecialchars($val['post_title']);
						$meta_data = $val['meta_data'];
						$thumb = if_get_val_from_json($meta_data, 'thumb_attachment');
						$file_attach = if_get_val_from_json($meta_data, 'file_attachment');  // 대표이미지 없는 경우, 첫 사진을 썸네일로 사용하기 위함.
						$thumb_url = empty($thumb[0]) ? $file_attach[0]['file_url'] : $thumb[0]['file_url'];
						$file_size = @getimagesize($thumb_url); 
						if (isset($file_size[0]) && !empty($file_size[0])) {
						    $fileWidth = ($file_size[0] > 260)? 260:$file_size[0];
						    $fileHeight = ($file_size[1] > 150)? 150:$file_size[1];
						}
					?>
					<div class="gall-box n3">
						<a href="<?php echo CONTENT_URL ?>/conference/conference_gallery_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>&gallery_id=<?php echo $post_id;?>">
							<span class="img-wrap">
								<img src="<?php echo $thumb_url ?>" alt="대표 이미지" width=<?php echo $fileWidth;?> height=<?php echo $fileHeight;?> align="center">
							</span>
							<span class="tit"><?php echo $post_title ?></span>
						</a>
					</div>
				<?php
						$list_no--;
					}
				} else {?>
    				No data.
				<?php 
				}
				?>	
				</div>
				<div class="btn-wrap text-right">
					<!-- a href="conference_gall_write.php" class="btn btn-primary">글쓰기</a> -->
				</div>
    			<div class="text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        		</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>