<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-post.php';


$evt_type = empty($_GET['evt_type']) ? 'ACD' : $_GET['evt_type'];  // 분류
if(empty($_GET['conference_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}
$conference_id = $_GET['conference_id'];
$event_data = if_get_event($conference_id);
if(empty($event_data['seq_id'])){
    if_js_alert_back("정보를 확인할 수 없습니다.");
}
$event_meta = json_decode($event_data['meta_data'],true);

// 연도 검색
$c_years = if_get_conference_years_by_year();
			
/*갤러리 게시판 등록확인*/
$gallery_total = if_get_post_gallery_total($conference_id);

$on5 = 'on';
$left = '학술·교육·행사';
$title = '역대 학술·교육·행사';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3 mb0">
						<li <?php echo ($evt_type == "ACD")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">학술</a></li>
						<li <?php echo ($evt_type == "TRN")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=TRN">교육</a></li>
						<li <?php echo ($evt_type == "EVT")?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php?evt_type=EVT">행사</a></li>
					</ul>
				</div>
				<div class="form-inline text-right sel-year">
					<div class="form-group">
						<form id="form-item-new" action="./conference_list.php" method="GET">
						<input type="hidden" name="evt_type" value="<?php echo $evt_type;?>">
						<select name="qa1" id="qa1" class="form-control" onchange="javascript:submit();">
							<option value="">- 연도 선택 -</option>
            				<?php 
            				if (!empty($c_years)) {
            				    foreach ($c_years as $key => $val) {
            				        $year = $val['event_year'];
            				        $cnt = $val['cnt'];
            				        $selected = $qa1 == $year ? 'selected' : '';
            				?>
							<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?></option>
            				<?php 
            				    }
            				}
            				?>
						</select>
						</form>	
					</div>
				</div>
				<div class="sub-tabmenu-wrap">
					<ul class="sub-tabmenu type3">
						<li class="on"><a href="<?php echo CONTENT_URL ?>/conference/conference_view.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">프로그램 표</a></li>
						<?php if($gallery_total>0){?>
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_gallery.php?evt_type=<?php echo $evt_type;?>&conference_id=<?php echo $conference_id;?>">갤러리</a></li>
						<?php }?>
					</ul>
				</div>
				<div class="info-box info-box-bg">
					<?php if(!empty($event_meta['program_table'])){echo $event_meta['program_table'];}?>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>