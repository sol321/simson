<div class="left-menu-top">
	<h3>학술·교육·행사</h3>
</div>
<ul class="left-menu">
	<li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술·교육·행사 안내 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/index_reg.php">사전등록 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on3)?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/index_abst.php">초록접수 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on4)?$on4:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/confirm.php">등록 / 접수 확인 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on5)?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">역대 학술·교육·행사 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on6)?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">자료실 <i class="xi-angle-right"></i></a></li>
</ul>