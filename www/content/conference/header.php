<?php
require_once FUNC_PATH . '/functions-conference.php';
$event_id = if_get_latest_event_id();

if(empty($event_id)) {
    $msg = "진행중인 학술/교육행사가 없습니다.";
    $url = "/";
    if_js_alert_move($msg, $url);
}

$event_data = if_get_latest_event_data();
$event_type = $event_data['event_type'];
$event_name = htmlspecialchars($event_data['event_name']);

//echo "<pre>";
//print_R($event_data);
$event_meta = json_decode($event_data['meta_data'], true);
$pre_reg_max_attendees = $event_meta['pre_reg_max_attendees'];

$now = date("Y-m-d H:i:s");
?>