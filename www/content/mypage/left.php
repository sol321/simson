<div class="left-menu-top">
	<h3>MYPAGE</h3>
</div>
<ul class="left-menu">
	<li class="<?php echo (isset($on1))?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on2))?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on3))?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on4))?$on4:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on5))?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/subscription.php">구독정보 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo (isset($on6))?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사 <i class="xi-angle-right"></i></a></li>
</ul>