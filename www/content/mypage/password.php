<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
$on3 = 'on';
$left = 'MYPAGE';
$title = '비밀번호변경';
if_authenticate_user();
require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
                <ul class="sub-tabmenu type2 m-mypage-tab">
                	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
                	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                </ul>				
				<div class="join-regi">
					<form id="form-item-new" class="form-horizontal">
						<div class="table-wrap">
							<table class="cst-table border-table regi-table">
								<caption>비밀번호변경</caption>
								<colgroup>
									<col style="width: 20%;">
									<col style="width: 80%;">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" class="text-left"><strong class="required"></strong> 현재 비밀번호</th>
										<td class="text-left">
											<input type="password" class="form-control" name="password_current" id="password_current" maxlength="20" autocomplete="new-password">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required"></strong> 새 비밀번호</th>
										<td class="text-left">
											<input type="password" class="form-control" name="password_new" id="password_new" maxlength="20" autocomplete="new-password">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required"></strong> 비밀번호 확인</th>
										<td class="text-left">
											<input type="password" class="form-control" name="password_repeat" id="password_repeat" maxlength="20" autocomplete="new-password">
										</td>
									</tr>
								</tbody>
							</table>
						</div>	
						<div class="btn-wrap text-center">
							<button type="submit" id="btn-submit" class="btn btn-primary">변경하기</button>
						</div>
					</form>								
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();

		/*
		var chkNumeric = $("#password_new").val().search(/[0-9]/g);
	    var chkAlphabet = $("#password_new").val().search(/[a-z]/ig);
	    var chkSpecial = $("#password_new").val().search(/[^0-9a-zA-Z]/g);

	    if ( chkNumeric > -1 ) {
	    	if ( chkAlphabet < 0 && chkSpecial < 0 ) {
	    		alert('비밀번호는 영문소문자, 숫자, 특수문자 중 2가지 이상을 혼용해서 사용해 주십시오.');
	    		$("#password").focus();
		    	return false;
	    	}
		} else if ( chkAlphabet > -1 ) {
			if ( chkNumeric < 0 && chkSpecial < 0 ) {
				alert('비밀번호는 영문소문자, 숫자, 특수문자 중 2가지 이상을 혼용해서 사용해 주십시오.');
				$("#password").focus();
		    	return false;
	    	}
		} else if ( chkSpecial > -1 ) {
			if ( chkNumeric < 0 && chkAlphabet < 0 ) {
				alert('비밀번호는 영문소문자, 숫자, 특수문자 중 2가지 이상을 혼용해서 사용해 주십시오.');
				$("#password").focus();
		    	return false;
	    	}
		}
		*/
		if ($.trim($("#password_current").val()).length < 1) {
			alert("현재 비밀번호를 입력해 주십시오.");
			$("#password_current").focus();
			return false;
		}
		if ($.trim($("#password_new").val()).length < 6) {
			alert("비밀번호를 6자 이상으로 입력해 주십시오.");
			$("#password_new").focus();
			return false;
		}
		if ($.trim($("#password_repeat").val()).length < 6) {
			alert("비밀번호를 6자 이상으로 입력해 주십시오.");
			$("#password_repeat").focus();
			return false;
		}
		if ($("#password_repeat").val() != $("#password_new").val()) {
			alert("새로운 비밀번호가 일치하지 않습니다.");
			$("#password_new").focus();
			return false;
		}
		
		$.ajax({
			type : "POST",
			url : "./ajax/password.php",
			data : $("#form-item-new").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("변경되었습니다.");
						location.href = "index.php";
					} else {
						alert("변경하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>