<?php
/*
 * Desc: 회원 비밀번호 변경.
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_user_id()) {
    $code = 510;
    $msg = '로그인 후 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password_current'])) {
    $code = 101;
    $msg = '현재 비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password_new'])) {
    $code = 102;
    $msg = '새로운 비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password_repeat'])) {
    $code = 103;
    $msg = '새로운 비밀번호를 한번 더 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (strcmp($_POST['password_repeat'], $_POST['password_new'])) {
    $code = 104;
    $msg = '새 비밀번호가 서로 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_id = if_get_current_user_id();
$password_current = $_POST['password_current'];
$password_new = $_POST['password_new'];

// 현재 비밀번호 검증
$user_row = if_get_user_by_id($user_id);
$user_pw = $user_row['user_pw'];

$check_passwd = if_verify_passwd($password_current, $user_pw);

if (!$check_passwd) {
    $code = 202;
    $msg = '현재 비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// update password
$result = if_update_user_password($user_id, $password_new);

if (empty($result)) {
    $code = 201;
    $msg = '비밀번호를 변경할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>