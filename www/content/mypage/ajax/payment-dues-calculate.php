<?php
/*
 * Desc: 선택한 학회 수와 기간에 대한 총 비용을 계산합니다.
 *  최소 1개의 회원 학회가 기본입니다. 2개 이상부터 복수 학회회원입니다.
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';
require_once FUNC_PATH . '/functions-payment.php';

$code = 0;
$msg = '';

if (!if_get_current_user_id()) {
    $code = 510;
    $msg = '로그인 후 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['member_academy'])) {
    $code = 101;
    $msg = '가입하실 회원학회를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['membership_period'])) {
    $code = 102;
    $msg = '기간을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_id = if_get_current_user_id();
$member_academy = $_POST['member_academy'];
$membership_period = $_POST['membership_period'];

$academy_count = count($member_academy);

/*
if ($academy_count < 1) {
    $code = 104;
    $msg = '회원학회를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
*/

$base_year = date('Y');

$products = if_get_products_by_year($base_year);

// 입회비 납부 내역
$entrance_fee_flag = if_check_user_entance_fee($user_id);

$entrance_fee = 0;
$annual_fee_single_1year = 0;    // 단수 1 year
$annual_fee_single_5year = 0;  // 단수 5 year
$annual_fee_plural_1year = 0;    // 복수 1 year
$annual_fee_plural_5year = 0;  // 복수 5 year

if (!empty($products)) {
    foreach ($products as $key => $val) {
        if (!strcmp($val['product_type'], 'ENT')) {
            $entrance_fee = $val['product_price'];
        }
        if (!strcmp($val['product_type'], 'ANF_SGL') && $val['product_period'] == '1') {
            $annual_fee_single_1year = $val['product_price'];    // 단수 1 year
        }
        if (!strcmp($val['product_type'], 'ANF_SGL') && $val['product_period'] == '5') {
            $annual_fee_single_5year = $val['product_price'];    // 단수 5 year
        }
        if (!strcmp($val['product_type'], 'ANF_PLR') && $val['product_period'] == '1') {
            $annual_fee_plural_1year = $val['product_price'];    // 복수 1 year
        }
        if (!strcmp($val['product_type'], 'ANF_PLR') && $val['product_period'] == '5') {
            $annual_fee_plural_5year = $val['product_price'];    // 복수 5 year
        }
    }
}

// 회원의 기 회비 납입 내역 조회, 장기회원(추가 협회를 장기로 선택 시, 단수장기회원 이슈 발생 - 연회비가 15만인데 30만으로 인식)
$dues_log = if_get_count_user_payment($user_id, 'ANF_PLR');

// 요금표
// $entrance_fee = 10000;
// $annual_fee_single_1year = 70000;    // 단수 1 year
// $annual_fee_single_5year = 300000;  // 단수 5 year
// $annual_fee_plural_1year = 70000;    // 복수 1 year
// $annual_fee_plural_5year = 150000;  // 복수 5 year

$total_fee = 0;

if ($membership_period > 1) {   // 5년 장기
    if ($dues_log > 0) {
        $total_fee += $annual_fee_plural_5year;     // 기존 회비 납입 이력이 있으면 복수 장기회원이 된다.
    } else {
        $total_fee += $annual_fee_single_5year;     // 장기 회원
    }
} else {    // 1년
    $total_fee += $annual_fee_single_1year;
}

if ($academy_count > 1) {  // 복수 학회 선택 : 입회비 면제
    $addition_count = $academy_count - 1;    // 1개를 제외한 추가 학회 수
    
    if ($membership_period > 1) {   // 5년 장기
        $total_fee += $addition_count * $annual_fee_plural_5year;
    } else {    // 1년
        $total_fee += $addition_count * $annual_fee_plural_1year;
    }
    $entrance_fee = 0;  // 연회비 면제    
}

// 연회비 납부 이력이 있으면 0원
if ($entrance_fee_flag) {
    $entrance_fee = 0;
}

$total_fee += $entrance_fee;

$amount = number_format($total_fee) . '원';

$json = compact('code', 'msg', 'amount', 'entrance_fee');
echo json_encode($json);

?>