<?php
/*
 * Desc:회원 정보 수정
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_user_id()) {
    $code = 510;
    $msg = '로그인 후 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// if (empty($_POST['password'])) {
//     $code = 102;
//     $msg = '비밀번호를 입력해 주십시오.';
//     $json = compact('code', 'msg');
//     exit(json_encode($json));
// }

if (empty($_POST['name_ko'])) {
    $code = 103;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 104;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!empty($user_email)) {
    if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
if (empty($_POST['user_mobile'])) {
    $code = 105;
    $msg = '휴대전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['home_zipcode']) || empty($_POST['home_address1'])) {
    $code = 106;
    $msg = '자택주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['home_address2'])) {
    $code = 106;
    $msg = '자택상세주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

//회원 정보조회
/*
$user_data = if_get_user_by_id(if_get_current_user_id());

// 비밀번호 비교
$check_passwd = if_verify_passwd($_POST['password'] , $user_data['user_pw']);

if (!$check_passwd) {
    $code = 202;
    $msg = '비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
*/

$user_id = if_get_current_user_id();

// 변경 전 회원정보 저장 : json
$user_row = if_get_user_by_id($user_id);
$meta_key = 'update_' . time();
$meta_value = json_encode($user_row);
if_update_user_meta($user_id, $meta_key, $meta_value);

$result = if_update_user($user_id);

if (empty($result)) {
    $code = 201;
    $msg = '정보 수정을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// 관리자에게 메일 발송
/*
$json = if_get_option('site_email_from');
$to_email = json_decode($json, true);
$name_ko        = empty($_POST['name_ko']) ? '' : trim($_POST['name_ko']);
$user_mobile    = empty($_POST['user_mobile']) ? '' : $_POST['user_mobile'];

$email_subject = $user_login . ' 회원이 정보를 수정했습니다.';

$content =<<<EOD

        <h2 style="color:#000;font-size:25px;font-weight:bold;margin:0;letter-spacing:-1px;">회원가입 안내 메일</h2>
		<p style="padding:30px;margin:25px 0;display:block;background:#eee;font-size:16px;line-height:24px;border-radius:5px;">
			이름 : $name_ko <br>
                          아이디: $user_login <br>
                          이메일: $user_email <br>
                          휴대전화번호: $user_mobile <br>
		</p>
EOD;

$email_body = if_get_default_mail_template($content);

if (filter_var($to_email, FILTER_VALIDATE_EMAIL)) {
    if_send_mail($to_email, $email_subject, $email_body, $user_email);
}
// 메일 발송 끝
*/

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>