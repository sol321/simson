<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_user_id()) {
    $code = 510;
    $msg = '로그인 후 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['product_id'])) {
    $code = 101;
    $msg = '결제할 회비 내역이 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['due_date'])) {
    $code = 102;
    $msg = '입금 예정일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['remitter'])) {
    $code = 103;
    $msg = '입금인을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['bank_name'])) {
    $code = 104;
    $msg = '입금은행을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_id = if_get_current_user_id();

$result = if_add_item_order_due($user_id);

if (empty($result)) {
    $code = 201;
    $msg = '입금 신청이 이뤄지지 않았습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);


?>