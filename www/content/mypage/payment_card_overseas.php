<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-payment.php';

$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

if (empty($_POST['product_id'])) {
    if_js_alert_back('납입할 회비 내역을 선택해 주십시오.');
}

$user_id = if_get_current_user_id();
// $expiry_dt = if_get_user_meta($user_id, 'membership_expiry_date');

$user_row = if_get_user_by_id($user_id);
$user_name = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];
$remote_ip = $_SERVER['REMOTE_ADDR'];

$product_id = $_POST['product_id']; // 선택한 회비 정보
$prod_results = if_get_products($product_id);
$order_code = if_make_order_no(16); // 주문번호 생성
$order_name = $prod_results[0]['product_name'];
$product_count = count($prod_results);
if ($product_count > 1) {
    $order_name .= ' 외 ' . intval($product_count - 1) . '건';
}

// if_item_order에 저장
$order_id = if_add_item_order_due_by_card($user_id, $order_code, $product_id, 'CARD_OC');

// PG 설정정보
$json = if_get_option('if_api_pg_eximbay');
$pg_data = json_decode($json, true);

if (empty($pg_data)) {
    if_js_alert_back('신용카드 결제 설정정보가 완료되지 않았습니다.');
}

// Eximbay Data
$mid = $pg_data['mid'];
$store_name = $pg_data['store_name'];
// $secret_key = $pg_data['secret_key'];

require_once INC_PATH . '/front-header.php';
?>
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';

?>
        <section id="subcontainer">
        	<div class="layer1120">
        		<aside class="left">
        			<?php include_once 'left.php'; ?>
        		</aside>
        		<article class="subcon">
        			<div class="path">
        				<ul>
        					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
        					<li><?php echo $left?></li>
        					<li><?php echo $title?></li>
        				</ul>
        			</div>
        			<h4><?php echo $title?></h4>
        			<div class="subcontents">
        				<div class="tabmenu-wrap">
        					<ul class="sub-tabmenu type2 n4">
            					<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
            					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
            					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
        					</ul>
        				</div>			
                		<div class="join-regi">
                			<form id="form-item-new">
        						<div class="table-wrap">
            						<table class="cst-table border-table regi-table">
                    					<thead>
        									<tr>
                								<th class="active">내용</th>
                								<th class="active">납부할 금액</th>
                							</tr>
        								</thead>
                						<tbody>
                				<?php
                				$total_amount = 0;  // 결제할 금액 합계
        
                				foreach ($prod_results as $key => $val) {
                				    $prod_id = $val['product_id'];
                				    $prod_name = $val['product_name'];
                				    $product_price = $val['product_price'];
                				    $total_amount += $product_price;
                				?>
                							<tr>
                								<td>
                									<?php echo $prod_name ?>
                								</td>
                								<td>
                									<?php echo number_format($product_price) ?>
                									<input type="hidden" name="product_id[]" value="<?php echo $prod_id ?>">
                								</td>
                							</tr>
                				<?php
                				}
                				?>
                						</tbody>
                        				<tfoot>
                        					<tr>
                        						<th class="active">결제금액 합계</th>
                        						<th class="active"><?php echo number_format($total_amount) ?></th>
                        					</tr>
                        				</tfoot>
                    				</table>
                				</div>
        
                				<div class="text-center mt30">
                    				<button type="button" id="do-payment" class="btn btn-primary btn-lg">결제하기</button>
                    			</div>
                    		</form>
                		</div>			
        			</div>
        		</article>
        	</div>
        </section>
        
        <form class="form-horizontal" name="regForm" method="post" action="<?php echo EXT_URL ?>/eximbay/request.php">
        	<!-- 결제에 필요 한 필수 파라미터 -->
        	<input type="hidden" name="ver" value="230"><!-- 연동 버전 -->
        	<input type="hidden" name="txntype" value="PAYMENT"><!-- 거래 타입 -->
        	<input type="hidden" name="charset" value="UTF-8"><!-- 고정 : UTF-8 -->
        
        	<!-- statusurl(필수 값) : 결제 완료 시 Back-end 방식으로 Eximbay 서버에서 statusurl에 지정된 가맹점 페이지를 Back-end로 호출하여 파라미터를 전송 -->
        	<!-- 스크립트, 쿠키, 세션 사용 불가 -->
        	<input type="hidden" name="statusurl" value="<?php echo EXT_URL ?>/eximbay/status.php">
        	<input type="hidden" name="returnurl" value="<?php echo EXT_URL ?>/eximbay/return.php">
        	<!--결제 완료 시 Front-end 방식으로 사용자 브라우저 상에 호출되어 보여질 가맹점 페이지 -->
        
        	<!-- 배송지 파라미터(선택) -->
        	<input type="hidden" name="shipTo_country" value="KR">
        	<input type="hidden" name="shipTo_city" value="Seoul">
        	<input type="hidden" name="shipTo_state" value="">
        	<input type="hidden" name="shipTo_street1" value="">
        	<input type="hidden" name="shipTo_postalCode" value="">
        	<input type="hidden" name="shipTo_phoneNumber" value="<?php echo $user_mobile ?>">
        	<input type="hidden" name="shipTo_firstName" value="<?php echo $user_name ?>">
        	<input type="hidden" name="shipTo_lastName" value="<?php echo $user_name ?>">
        
        	<!-- 청구지 파라미터 (선택) -->
        	<input type="hidden" name="billTo_city">
        	<input type="hidden" name="billTo_country">
        	<input type="hidden" name="billTo_firstName">
        	<input type="hidden" name="billTo_lastName">
        	<input type="hidden" name="billTo_phoneNumber">
        	<input type="hidden" name="billTo_postalCode">
        	<input type="hidden" name="billTo_state">
        	<input type="hidden" name="billTo_street1">
        
        	<!-- 결제 응답 값 처리 파라미터 -->
        	<input type="hidden" name="rescode">
        	<input type="hidden" name="resmsg">

			<input type="hidden" name="item_0_product" value="<?php echo $order_name ?>">
        	<input type="hidden" name="item_0_unitPrice" value="<?php echo $total_amount ?>">
        	<input type="hidden" name="issuercountry" value="KR">
        	        
        	<!-- 결제에 필요한 필수 파라미터 -->
        	<input type="hidden" name="mid" value="<?php echo $mid ?>">
        	<input type="hidden" name="ref" value="<?php echo $order_code ?>">
        	<input type="hidden" name="ostype" value="P">
        	<input type="hidden" name="displaytype" value="P">
        	<input type="hidden" name="cur" value="KRW">
        	<input type="hidden" name="amt" value="<?php echo $total_amount ?>">
        	<input type="hidden" name="shop" value="<?php echo $store_name ?>">
        	<input type="hidden" name="buyer" value="<?php echo $user_name ?>">
        	<input type="hidden" name="email" value="<?php echo $user_email ?>">
        	<input type="hidden" name="tel" value="<?php echo $user_mobile ?>">
        	<input type="hidden" name="lang" value="KR">
        	<input type="hidden" name="paymethod" value="P000">
        	<input type="hidden" name="autoclose" value="N">
        	<input type="hidden" name="param1" value="<?php echo $order_id ?>">
        	<input type="hidden" name="param2" value="<?php echo $user_id ?>">
        	<input type="hidden" name="param3" value="">
        </form>
        
        <script>
        <!--
        function eximbayPayment(){
        	var frm = document.regForm;
        	window.open("", "payment2", "resizable=yes,scrollbars=yes,width=415,height=600");
        	frm.target = "payment2";
        	frm.submit();
        }
        //-->
        </script>
        <script>
        $(function() {
        	$("#do-payment").click(function() {
        		eximbayPayment();
        	});
        });
        </script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>