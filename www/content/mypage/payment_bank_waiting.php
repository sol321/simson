<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once INC_PATH . '/classes/Paginator.php';
$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

$user_id = if_get_current_user_id();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

$sql = '';
$pph = '';
$sparam = [];


// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			order_code,
            item_year,
            create_dt,
            COUNT(*) AS tcnt,
            SUM(order_amount) AS tsum
		FROM
			" . $GLOBALS['if_tbl_item_order'] . "
		WHERE
            user_id = '$user_id' AND
            order_state = '1000' AND
            JSON_UNQUOTE(meta_data->'$.payment_method') = 'BANK_TR'
			$sql
        GROUP BY
            order_code,
            item_year,
            create_dt
        ORDER BY
			create_dt DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
			
require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
                    <ul class="sub-tabmenu type2 m-mypage-tab">
                    	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                    	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                    	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                    	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                    	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
                    	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                    </ul>						
					<ul class="sub-tabmenu type2 n3">
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
    					<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
					</ul>
				</div>			
				<div class="join-regi">
					<div class="table-wrap">
					<form id="form-item-list">
						<table class="cst-table border-table regi-table">
							<thead>
								<tr class="active">
									<th>#</th>
									<th>신청날짜</th>
									<th>연도</th>
									<th>내용</th>
									<th>금액</th>
									<th>입금인</th>
									<th>입금은행</th>
									<th>입금예정일</th>
								</tr>
							</thead>
							<tbody>
					<?php
					if (!empty($item_results)) {
					    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);

						foreach ($item_results as $key => $val) {
							$order_code = $val['order_code'];
							$item_year = $val['item_year'];
							$create_dt = $val['create_dt'];
							
							$tsum = $val['tsum'];
							$tcnt = intval($val['tcnt']);
							
							$pmt_results = if_get_user_payment_by_code($order_code);
							$order_name = $pmt_results[0]['order_name'];
							$order_name_label = $tcnt > 1 ? $order_name . ' 외 ' . ($tcnt - 1) . '건' : $order_name;
							
							$meta_data = $pmt_results[0]['meta_data'];
							$jdec = json_decode($meta_data, true);
							$remitter = $jdec['remitter'];
							$bank_name = $jdec['bank_name'];
							$due_date = $jdec['due_date'];
					?>
								<tr>
									<td><?php echo $list_no ?></td>
									<td><?php echo substr($create_dt, 0, 10) ?></td>
									<td><?php echo $item_year ?></td>
									<td><?php echo $order_name_label ?></td>
									<td><b><?php echo number_format($tsum) ?></b></td>
									<td><?php echo $remitter ?></td>
									<td><?php echo $bank_name ?></td>
									<td><?php echo $due_date ?></td>
								</tr>
								<tr>
									<td></td>
									<td colspan="7">
										<ul class="list-group">
								<?php
							    foreach ($pmt_results as $k => $v) {
// 							        $i_type_secondary = $v['item_type_secondary'];
							        $i_order_name = $v['order_name'];
							        $i_order_amount = $v['order_amount'];
								?>
											<li class="list-group-item">
												<table>
												<colgroup>
													<col width="70%">
													<col width="*">
												</colgroup>
													<tr>
                    									<th>
                    										<?php echo $i_order_name ?> 
                    									</th>
                    									<td>
                    										<?php echo number_format($i_order_amount) ?>원
                    									</td>
                    								</tr>
												</table>
											</li>
								<?php 
								}
								?>
										</ul>
									</td>
								</tr>
					<?php
					      $list_no--;
						}
					} else {
					?>
					<tr>
						<td colspan="8">무통장입금 대기 회비내역이 없습니다.</td>
					</tr>						
					<?php 
					}
					?>
							</tbody>
						</table>
					</form>
				</div><!-- /.box-body -->
				<div class="box-footer text-center">
            		<?php
            		echo $paginator->if_paginator();
            		?>
				</div>
				<!-- /.box-footer -->
			</div>
		</div>
		</article>
	</div>        		
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>