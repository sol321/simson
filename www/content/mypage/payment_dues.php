<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-payment.php';
$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

$cur_year = date('Y');

$user_id = if_get_current_user_id();
$user_row = if_get_user_by_id($user_id);
$user_create_dt = $user_row['create_dt'];
$user_reg_time = strtotime($user_create_dt);
$user_reg_year = date('Y', $user_reg_time);    // 회원 가입연도
$user_reg_md = date('Ymd', $user_reg_time);    // 회원 등록(승인) 월일

$ann_fee_year_to = $cur_year;   // 연회비 마지막 부과 연도 기준은 오늘의 연도

// 회계 기준날짜와 회원의 가입날짜를 비교한다.
$fiscal_data = if_get_option('if_fiscal_data');

if (!empty($fiscal_data)) {
    $jdec = json_decode($fiscal_data, true);
    
    $fiscal_type = $jdec['fiscal_type'];
    $fiscal_md = $jdec['fiscal_md'];
    
    if (!strcmp($fiscal_type, 'A')) {   // 회계연도 기준 날짜
        $ref_md = $cur_year . $fiscal_md;
        
        if ($user_reg_md < $ref_md) {
            $ann_fee_year_from = $cur_year - 1;
        } else {
            $ann_fee_year_from = $cur_year;
        }
        
    } else {    // 회원 가입(등록) 월일
        $ann_fee_year_from = $user_reg_year;
    }
}

// 회원의 class에 해당하는 모든 item product를 가져온다.
$member_fee = if_check_member_fee($user_id);

// 로그인 회원이 결제 완료한 내역
$paid_list = if_get_paid_product_ids($user_id);

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
                <ul class="sub-tabmenu type2 m-mypage-tab">
                	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
                	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                </ul>						
					<ul class="sub-tabmenu type2 n3">
    					<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
					</ul>
				</div>			
				
				<form id="form-item-new">
    				<div class="table-wrap n-border scroll-wrap">
    					<table class="cst-table" id="wrap-dues">
    						<caption>회원가입 안내</caption>
    						<thead>
    							<tr>
    								<th class="text-center">결제항목 선택</th>
    								<th class="text-center">내용</th>
    								<th class="text-center">회원구분</th>
    								<th class="text-center">납부액</th>
    							</tr>
    						</thead>
    						<tbody>
    			<?php
    			if (!empty($member_fee)) {
    			    foreach ($member_fee as $key => $val) {
    			        $prod_id = $val['product_id'];
    			        $prod_name = $val['product_name'];
    			        $prod_type = $val['product_type'];
    			        $prod_year = $val['product_year'];
    			        $product_price = $val['product_price'];
    			        $class_name = $val['class_name'];
    			        /*
    			         * Desc: 납부 완료내역이 없어야 결제항목으로 노출된다.
    			         */
    			        if (!in_array($prod_id, $paid_list)) {
    				        // 입회비, 1회성
    			            if ($ann_fee_year_from == $prod_year && !strcmp($prod_type, 'ENT')) {
    		   ?>
    				            <tr>
    								<td>
    									<input type="checkbox" name="product_id[]" class="pay_item" value="<?php echo $prod_id ?>" class="form-control" checked>
    								</td>
    								<td><?php echo $prod_name ?></td>
    								<td><?php echo $class_name ?></td>
    								<td><?php echo number_format($product_price) ?></td>
    							</tr>
    			<?php
    			            }
    			        
    				        // 연회비: 각 연도별
    				        if (!strcmp($prod_type, 'ANN')) {
    				            if ($ann_fee_year_from <= $prod_year && $prod_year <= $ann_fee_year_to) {
    			?>
    							<tr>
    								<td>
    									<input type="checkbox" name="product_id[]" class="pay_item" value="<?php echo $prod_id ?>" class="form-control" checked>
    								</td>
    								<td><?php echo $prod_name ?></td>
    								<td><?php echo $class_name ?></td>
    								<td><?php echo number_format($product_price) ?></td>
    							</tr>
    			<?php
    				            }
    				        }
    			        }
    			    }
    			}
    			?>

    						</tbody>
    					</table>
    				</div>
    				
    				<p>&nbsp;</p>
    				
    				<div class="table-wrap n-border scroll-wrap" id="wrap-payment-method">
    					<table class="cst-table">
    						<thead>
    							<tr>
    								<th>
    									결제수단 선택<br><br>
    									<span class="text-warning">납부액이 0원인 경우, 무통장입금을 선택하신 후 진행해 주시기 바랍니다.</span>
    								</th>
    							</tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td>
    				<?php
    				foreach ($if_payment_method as $key => $val) {
    				    if (!empty($key)) {
    				?>
    									<label>
    										<input type="radio" name="pay_method" value="<?php echo $key ?>"> <?php echo $val ?>
    									</label> &nbsp; &nbsp;
    				<?php
    				    }
    				}
    				?>
    								</td>
    							</tr>
    							<tr>
    								<td>
    									<p class="next_btn">
    										<button type="button" class="btn btn-primary" id="btn-payment">다음단계 &gt;</button>
    									</p>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
				</form>					
			
			</div>
		</article>
	</div>
</section>
<script>
$(function() {
	$("#btn-payment").click(function() {
		var clen = $(".pay_item:checked").length;
		var payMethod = $('input[name="pay_method"]:checked').val();

		var actionUrl = "";

		if (clen == 0) {
			alert("납부하실 결제항목을 선택해 주십시오.");
			return;
		}

		if (payMethod == "BANK_TR") {
			actionUrl = "payment_bank_transfer.php";
		} else if (payMethod == "CARD_LC") {
			actionUrl = "payment_card_local.php";
		} else if (payMethod == "CARD_OC") {
			actionUrl = "payment_card_overseas.php";
		} else {
			alert("결제수단을 선택해 주십시오.");
			$("#pay_method").focus();
			return;
		}

		$("#form-item-new").attr({
			"action" : actionUrl,
			"method" : "post",
			"target" : "_self"
		}).submit();
	});

	// 납부 내역이 없은면 결제수단 감추기
	if ($("#wrap-dues tbody tr").length == 0) {
		$("#wrap-payment-method").html('<h3 class="text-center">납부하실 내역이 없습니다.</h3>');
	}
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>