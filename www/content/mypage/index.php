<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$on1 = 'on';
$left = 'MYPAGE';
$title = '개인정보';

if_authenticate_user();

$user_id = if_get_current_user_id();

$user_row = if_get_user_by_id($user_id);

$user_login = $user_row['user_login'];
$name_ko = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];
$user_birth = $user_row['user_birth'];
$org_name = $user_row['org_name'];
$show_hide = $user_row['show_hide'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);
$gender = $jdec['gender'];
$home_zipcode = $jdec['home_zipcode'];
$home_address1 = $jdec['home_address1'];
$home_address2 = $jdec['home_address2'];
$org_zipcode = $jdec['org_zipcode'];
$org_address1 = $jdec['org_address1'];
$org_address2 = $jdec['org_address2'];
$org_phone = $jdec['org_phone'];
$org_position = $jdec['org_position'];
$org_category = $jdec['org_category'];
$license_no = $jdec['license_no'];
$email_agree = empty($jdec['email_agree']) ? '' : $jdec['email_agree'];
$sms_agree = empty($jdec['sms_agree']) ? '' : $jdec['sms_agree'];
$user_qualification = $jdec['user_qualification'];
$user_qualification_etc = $jdec['user_qualification_etc'];

// 삭제된 회원 접근금지
if (strcmp($show_hide, 'show')) {
    if_js_alert_back('이용 불가능한 상태입니다. 관리자에게 문의해 주시기 바랍니다.');
}

require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>

<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
            <ul class="sub-tabmenu type2 m-mypage-tab">
            	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
            	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
            	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
            	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
            	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
            	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
            </ul>	
            
            <!-- 개인정보 -->
            <div class="join-regi">
				<div class="table-wrap">
					<table class="cst-table border-table regi-table">
						<caption>회원가입</caption>
						<colgroup>
							<col style="width: 20%;">
							<col style="width: 80%;">
						</colgroup>
						<tbody>
							<tr>
								<td class="text-left" colspan="2">기본 정보</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">아이디</th>
								<td class="text-left">
									<?php echo $user_login ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">성명</th>
								<td class="text-left">
									<?php echo $name_ko ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">이메일</th>
								<td class="text-left">
									<?php echo $user_email ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">휴대전화</th>
								<td class="text-left">
									<?php echo $user_mobile ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">자택 주소</th>
								<td class="text-left address">
									<?php echo $home_address1 ?>
									<?php echo $home_address2 ?>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">상세 정보</td>
							</tr>
						
							<tr>
								<th scope="row" class="text-left">자격 구분</th>
								<td class="text-left">
									<?php echo $if_user_qualification[$user_qualification] ?>
									<?php 
									if ($user_qualification == '1001') {
									    echo '';
									} else if ($user_qualification == '1999') {
									    echo '('. $user_qualification_etc .')';
									} else {
									    echo '(의사면허번호: '. $license_no .')';
									}
									?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">소속 분류</th>
								<td class="text-left">
									<?php echo $if_org_category[$org_category] ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">근무처</th>
								<td class="text-left">
									<?php echo $org_name ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">직위</th>
								<td class="text-left">
									<?php echo $org_position ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">근무처 전화번호</th>
								<td class="text-left">
									<?php echo $org_phone ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">근무처 주소</th>
								<td class="text-left address">
									<?php echo $org_address1 ?>
									<?php echo $org_address2 ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">성별</th>
								<td class="text-left">
        							<?php
        							if (empty($gender)) {
        							    echo '-';
        							} else {
        							    echo strcmp($gender, 'M') ? '여' : '남';
        							}
        							?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">생년월일</th>
								<td class="text-left">
									<?php echo $user_birth ?>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">부가 정보</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">이메일 수신</th>
								<td class="text-left">
									<?php echo $if_agree_yn[$email_agree] ?>
								</td>
							</tr>
							<tr>
								<th scope="row" class="text-left">문자메시지 수신</th>
								<td class="text-left">
									<?php echo $if_agree_yn[$sms_agree] ?>	
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btn-wrap text-center mt30">
					<a href="modify.php" class="btn btn-info btn-big">정보 변경</a>
				</div>
			</div>
            <!-- /.개인정보 -->
            
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>