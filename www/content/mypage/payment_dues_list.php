<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once INC_PATH . '/classes/Paginator.php';
$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

$user_id = if_get_current_user_id();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

$sql = '';
$pph = '';
$sparam = [];

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
        SELECT
			p.*
		FROM
			" . $GLOBALS['if_tbl_user_payment'] . " AS p
		INNER JOIN
			" . $GLOBALS['if_tbl_users'] . " AS u
		WHERE
			p.user_id = '$user_id' AND
            p.user_id = u.seq_id
			$sql
		ORDER BY
			p.pay_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
require_once INC_PATH . '/front-header.php';
?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
                    <ul class="sub-tabmenu type2 m-mypage-tab">
                    	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                    	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                    	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                    	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                    	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
                    	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                    </ul>						
					<ul class="sub-tabmenu type2 n3">
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
    					<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
    					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
					</ul>
				</div>			
				<div class="join-regi">
					<div class="table-wrap">
					<form id="form-item-list">
						<table class="cst-table border-table regi-table">
							<thead>
								<tr class="active">
									<th>#</th>
    								<th>연도</th>
    								<th>내용</th>
    								<th>금액</th>
									<th>결제날짜</th>
    								<th>상태</th>
								</tr>
							</thead>
							<tbody>
					<?php
					if (!empty($item_results)) {
					    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);

						foreach ($item_results as $key => $val) {
						    $pay_id = $val['pay_id'];
						    $pay_state = $val['pay_state'];
						    $pay_content = $val['pay_content'];
						    $pay_amount = $val['pay_amount'];
						    $pay_date = $val['pay_date'];
// 						    $expiry_date = $val['expiry_date'];
						    $item_year = $val['item_year'];
					?>
								<tr>
									<td><?php echo $list_no ?></td>
									<td><?php echo $item_year ?></td>
									<td><?php echo $pay_content ?></td>
									<td><?php echo number_format($pay_amount) ?></td>
									<td><?php echo str_replace('-', '.', $pay_date) ?></td>
									<td><?php echo $if_payment_state[$pay_state] ?></td>
								</tr>
					<?php
					      $list_no--;
						}
					} else {
					?>
								<tr>
									<td colspan="6">납부완료된 회비내역이 없습니다.</td>
								</tr>					
					<?php 
					}
					?>
							</tbody>
						</table>
					</form>
				</div><!-- /.box-body -->
				<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
				</div>
				<!-- /.box-footer -->
			</div>
		</div>				
		</article>
	</div>        		
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>