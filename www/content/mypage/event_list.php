<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';
$on6 = 'on';
$left = 'MYPAGE';
$title = '참여행사';

if_authenticate_user();

$user_id = if_get_current_user_id();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

$sql = '';
$pph = '';
$sparam = [];

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
        SELECT
			c.*, r.fee_amount , r.pay_state
		FROM
			" . $GLOBALS['if_tbl_conference'] . " AS c
		INNER JOIN
			" . $GLOBALS['if_tbl_conference_register'] . " AS r
		WHERE
			c.seq_id =  r.conference_id AND
            r.user_id = '$user_id'
			$sql
		ORDER BY
			c.event_period_from desc, c.event_period_to desc, c.seq_id desc
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
require_once INC_PATH . '/front-header.php';
			?>

	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
                <ul class="sub-tabmenu type2 m-mypage-tab">
                	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscription.php">구독정보</a></li>
                	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                </ul>				
					<div class="table-wrap scroll-x">
					<form id="form-item-list">
						<table class="cst-table border-table">
						<colgroup>
							<col>
							<col style="width: 13%"> 
							<col style="width: 10%">
							<col style="width: 10%">
							<col style="width: 10%">
							<col style="width: 10%">
						</colgroup>
						<thead>						
							<thead>
								<tr class="active">
									<th>행사명</th>
									<th>참가비</th>
									<th>납부일</th>
									<th>납부현황</th>
									<th>참가<br>확인증</th>
									<th>영수증</th>
								</tr>
							</thead>
							<tbody>
					<?php
					if (!empty($item_results)) {
					    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);

						foreach ($item_results as $key => $val) {
					?>
							<tr>
								<td><?php echo $val['event_name'] ?></td>
								<td>￦<?php echo number_format($val['fee_amount']); ?></td>
								<td></td>
								<td><?php echo $if_payment_state[$val['pay_state']]; ?></td>
								<td>
								<?php if($if_payment_state[$val['pay_state']] == "결제완료") {?>
									<a href="#n" class="btn-print"><img src="<?php echo INC_URL ?>/img/sub/ic-print.png" alt="인쇄"></a>
								<?php }?>
								</td>
								<td>
								<?php if($if_payment_state[$val['pay_state']] == "결제완료") {?>
									<a href="#n" class="btn-print"><img src="<?php echo INC_URL ?>/img/sub/ic-print.png" alt="인쇄"></a>
								<?php }?>
								</td>
							</tr>
					<?php
					      $list_no--;
						}
					} else {
					?>
								<tr>
									<td colspan="6">신청한 학회지가 없습니다.</td>
								</tr>					
					<?php 
					}
					?>
							</tbody>
						</table>
					</form>
				</div><!-- /.box-body -->
				<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
				</div>
				<!-- /.box-footer -->
		</div>				
		</article>
	</div>        		
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>