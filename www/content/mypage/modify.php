<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$on2 = 'on';
$left = 'MYPAGE';
$title = '회원정보변경';
$regist_page = true;

if_authenticate_user();

$user_id = if_get_current_user_id();

$user_row = if_get_user_by_id($user_id);

$user_login = $user_row['user_login'];
$name_ko = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];
$user_birth = $user_row['user_birth'];
$org_name = $user_row['org_name'];
$show_hide = $user_row['show_hide'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);
$gender = $jdec['gender'];
$home_zipcode = $jdec['home_zipcode'];
$home_address1 = $jdec['home_address1'];
$home_address2 = $jdec['home_address2'];
$org_zipcode = $jdec['org_zipcode'];
$org_address1 = $jdec['org_address1'];
$org_address2 = $jdec['org_address2'];
$org_phone = $jdec['org_phone'];
$org_position = $jdec['org_position'];
$org_category = $jdec['org_category'];
$license_no = $jdec['license_no'];
$email_agree = empty($jdec['email_agree']) ? '' : $jdec['email_agree'];
$sms_agree = empty($jdec['sms_agree']) ? '' : $jdec['sms_agree'];
$user_qualification = $jdec['user_qualification'];
$user_qualification_etc = $jdec['user_qualification_etc'];

// 삭제된 회원 접근금지
if (strcmp($show_hide, 'show')) {
    if_js_alert_back('이용 불가능한 상태입니다. 관리자에게 문의해 주시기 바랍니다.');
}

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
	
<?php 
require_once INC_PATH . '/front-gnb.php';

?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
                <ul class="sub-tabmenu type2 m-mypage-tab">
                	<li <?php echo isset($on1)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/">개인정보</a></li>
                	<li <?php echo isset($on2)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/modify.php">회원정보변경</a></li>
                	<li <?php echo isset($on3)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/password.php">비밀번호변경</a></li>
                	<li <?php echo isset($on4)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비납부</a></li>
                	<li <?php echo isset($on5)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/subscribe.php">구독정보</a></li>
                	<li <?php echo isset($on6)?" class='on'":""?>><a href="<?php echo CONTENT_URL ?>/mypage/event_list.php">참여행사</a></li>
                </ul>				
				<div class="join-regi">
					<form id="form-item-new" class="form-inline"> <!-- class="form-horizontal" -->
						<span class="tit dot">기본 정보</span>
						<div class="table-wrap">
							<table class="cst-table border-table regi-table">
								<caption>회원가입</caption>
								<colgroup>
									<col style="width: 20%;">
									<col style="width: 80%;">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" class="text-left"><strong class="required">*</strong> 아이디</th>
										<td class="text-left">
											<?php echo $user_login ?>
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required">*</strong> 성명</th>
										<td class="text-left">
											<input type="text" name="name_ko" id="name_ko" class="form-control w40" value="<?php echo $name_ko ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required">*</strong> 이메일</th>
										<td class="text-left">
											<input type="text" name="user_email" id="user_email" class="form-control w40" value="<?php echo $user_email ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required">*</strong> 휴대전화</th>
										<td class="text-left">
											<input type="text" name="user_mobile" id="user_mobile" class="form-control w40" placeholder="010-0000-0000" value="<?php echo $user_mobile ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"><strong class="required">*</strong> 자택 주소</th>
										<td class="text-left address">
											<input type="text" name="home_zipcode" id="home_zipcode" class="form-control w30 has-btn" readonly value="<?php echo $home_zipcode ?>"> 
											<button type="button" class="btn btn-warning" onclick="execDaumPostcode('home');">주소검색</button><br />
											<input type="text" name="home_address1" id="home_address1" class="form-control w100p" readonly value="<?php echo $home_address1 ?>">
											<input type="text" name="home_address2" id="home_address2" class="form-control w100p" placeholder="상세주소" value="<?php echo $home_address2 ?>">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<span class="tit dot mt30">상세 정보</span>
						<div class="table-wrap">
							<table class="cst-table border-table regi-table">
								<caption>회원가입</caption>
								<colgroup>
									<col style="width: 20%;">
									<col style="width: 80%;">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" class="text-left"> 자격 구분</th>
										<td class="text-left">
    										<div class="radio_group">
        							<?php 
        							foreach($if_user_qualification as $key => $val) {
        							    $checked = $user_qualification == $key ? 'checked' : '';
    								?>
    											<label><input type="radio" name="user_qualification" class="user_qualification" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php
    								}
    								?>
    										</div>
    										<input type="text" name="user_qualification_etc" id="user_qualification_etc" class="form-control w40 hide" placeholder="기타 선택 시 입력해 주세요" value="<?php echo $user_qualification_etc ?>">
    										<input type="text" name="license_no" id="license_no" class="form-control w40 hide" placeholder="의사 면허번호" value="<?php echo $license_no ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 소속 분류</th>
										<td class="text-left">
    										<div class="radio_group">
        							<?php 
    								foreach( $if_org_category as $key => $val) {
    								    $checked = $key == $org_category ? 'checked' : '';
    								?>
    											<label><input type="radio" name="org_category" class="org_category" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php
    								}
    								?>
    										</div>
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 근무처</th>
										<td class="text-left">
											<input type="text" name="org_name" id="org_name" class="form-control w40" value="<?php echo $org_name ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 직위</th>
										<td class="text-left">
											<input type="text" name="org_position" id="org_position" class="form-control w40" value="<?php echo $org_position ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 근무처 전화번호</th>
										<td class="text-left">
											<input type="text" name="org_phone" id="org_phone" class="form-control w40" placeholder="02-0000-0000" value="<?php echo $org_phone ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 근무처 주소</th>
										<td class="text-left address">
											<input type="text" name="org_zipcode" id="org_zipcode" class="form-control w30 has-btn" readonly value="<?php echo $org_zipcode ?>"> 
											<button type="button" class="btn btn-warning">주소검색</button><br />
											<input type="text" name="org_address1" id="org_address1" class="form-control w100p" readonly value="<?php echo $org_address1 ?>">
											<input type="text" name="org_address2" id="org_address2" class="form-control w100p" placeholder="상세주소" value="<?php echo $org_address2 ?>">
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 성별</th>
										<td class="text-left">
    										<div class="radio_group">
    											<label><input type="radio" name="gender" class="gender" value="M" <?php echo strcmp($gender, 'M') ? '' : 'checked'; ?>> 남</label> &nbsp; &nbsp; 
    											<label><input type="radio" name="gender" class="gender" value="F" <?php echo strcmp($gender, 'F') ? '' : 'checked'; ?>> 여</label>
    										</div>
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 생년월일</th>
										<td class="text-left">
    										<input type="text" name="user_birth" id="user_birth" class="form-control w40" placeholder="1975-04-22" value="<?php echo $user_birth ?>">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<span class="tit dot mt30">부가 정보</span>
						<div class="table-wrap">
							<table class="cst-table border-table regi-table">
								<caption>회원가입</caption>
								<colgroup>
									<col style="width: 20%;">
									<col style="width: 80%;">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" class="text-left"> 이메일 수신</th>
										<td class="text-left">
											<div class="radio">
									<?php 
    								foreach ($if_agree_yn as $key => $val) {
    								    $checked = strcmp($key, $email_agree) ? '' : 'checked';
    								?>
												<label><input type="radio" name="email_agree" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php 
    								}
    								?>
											</div>	
										</td>
									</tr>
									<tr>
										<th scope="row" class="text-left"> 문자메시지 수신</th>
										<td class="text-left">
											<div class="radio">
									<?php 
    								foreach ($if_agree_yn as $key => $val) {
    								    $checked = strcmp($key, $sms_agree) ? '' : 'checked';
    								?>
												<label><input type="radio" name="sms_agree" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php 
    								}
    								?>
											</div>	
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="btn-wrap text-center mt30">
							<button type="submit" class="btn btn-primary bg-blue btn-big">저장</button>
						</div>
					</form>
				</div>
			</div>
		</article>
	</div>
</section>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
$(function() {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/modify.php",
			data : $("#form-item-new").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					if (res.result) {
						alert("수정되었습니다.");
						location.href = "index.php";
					} else {
						alert("저장하지 못했습니다.");
					}
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});

	// 우편번호 찾기
	$("#home_zipcode, #home_address1").click(function() {
		execDaumPostcode('home');
	});
	// 우편번호 찾기
	$("#org_zipcode, #org_address1").click(function() {
		execDaumPostcode('dept');
	});

	// 생년월일
	$("#user_birth").datepicker({
		 dateFormat : "yy-mm-dd",
		 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
	     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
//	     monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
		 changeMonth : true,
		 changeYear : true,
		 yearRange : "-100:+19",
		 maxDate : "-19y"
	});

	// 자격구분 선택에 따른 면허번호 입력란
	$(".user_qualification").click(function() {
		var val = $(this).val();
		if (val > "3000") {
			$("#license_no").removeClass("hide");
		} else {
			$("#license_no").addClass("hide");
		}
		if (val == "1999") {
			$("#user_qualification_etc").removeClass("hide");
		} else {
			$("#user_qualification_etc").addClass("hide");
		}
	});

	if ($(".user_qualification:checked").val() == "1001") {	// 간호사
	} else if ($(".user_qualification:checked").val() == "1999") {	// 기타
		$("#user_qualification_etc").removeClass("hide");
	} else {	// 의사
		$("#license_no").removeClass("hide");
	}
});

// Daum 우편번호
function execDaumPostcode(spot) {
	new daum.Postcode({
		oncomplete: function(data) {
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
			var extraRoadAddr = ''; // 도로명 조합형 주소 변수

			// 법정동명이 있을 경우 추가한다. (법정리는 제외)
			// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
			if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
				extraRoadAddr += data.bname;
			}
			// 건물명이 있고, 공동주택일 경우 추가한다.
			if(data.buildingName !== '' && data.apartment === 'Y'){
			   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
			}
			// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
			if(extraRoadAddr !== ''){
				extraRoadAddr = ' (' + extraRoadAddr + ')';
			}
			// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
			if(fullRoadAddr !== ''){
				fullRoadAddr += extraRoadAddr;
			}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			if (spot == "home") {
				document.getElementById('home_zipcode').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('home_address1').value = fullRoadAddr + " ";		// 도로명 주소
				document.getElementById('home_address2').focus();
			} else if (spot == "dept") {
				document.getElementById('org_zipcode').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('org_address1').value = fullRoadAddr + " ";		// 도로명 주소
				document.getElementById('org_address2').focus();
			}
		}
	}).open();
}
</script>

<?php 
require_once INC_PATH . '/front-footer.php';
?>