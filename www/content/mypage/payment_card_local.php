<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-payment.php';

$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

if (empty($_POST['product_id'])) {
    if_js_alert_back('납입할 회비 내역을 선택해 주십시오.');
}

$user_id = if_get_current_user_id();
// $expiry_dt = if_get_user_meta($user_id, 'membership_expiry_date');

$user_row = if_get_user_by_id($user_id);
$user_name = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];
$remote_ip = $_SERVER['REMOTE_ADDR'];

$product_id = $_POST['product_id']; // 선택한 회비 정보
$prod_results = if_get_products($product_id);
$order_code = if_make_order_no(16); // 주문번호 생성
$order_name = $prod_results[0]['product_name'];
$site_phone = '';
$product_count = count($prod_results);
if ($product_count > 1) {
    $order_name .= ' 외 ' . intval($product_count - 1) . '건';
}

// if_item_order에 저장
$order_id = if_add_item_order_due_by_card($user_id, $order_code, $product_id, 'CARD_LC');


// PG 설정정보
$json = if_get_option('if_api_pg_paynuri');
$pg_data = json_decode($json, true);

if (empty($pg_data)) {
    if_js_alert_back('신용카드 결제 설정정보가 완료되지 않았습니다.');
}

// Paynuri Data
$store_id = $pg_data['store_id'];
$store_name = $pg_data['store_name'];
$biz_no = $pg_data['biz_no'];
$crypto_key = $pg_data['crypto_key'];

?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=0,maximum-scale=10,user-scalable=yes">
        <meta name="Author" content="">
        <meta name="Keywords" content="">
        <meta name="Description" content="">
        <title><?php echo json_decode(if_get_option('site_name')) ?></title>
    
        <!-- Inforang custom -->
        <link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/reset-paynuri.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/common.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/sub.css">
        <!-- Paynuri -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
		<style>
		.ui-dialog-titlebar-close {visibility: hidden;}
		</style>
    	
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    	<script src="<?php echo INC_URL ?>/js/jquery.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		<script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/common.js"></script>
		
		<script src="https://pg.paynuri.com/js/payment/lib.js"></script>
		
		<script>
		var dialog ;
		
		$(function(){
			dialog = $( "#dialog_payment" ).dialog({
				autoOpen: false,
				title: '페이누리 결제',
				height: 650,
				width: 570,
				modal: true,
				closeOnEscape: false,
				close: function() {
		
				}
			});
		});
		
		function openDialog(){
			var form = document.frmPayment;
			dialog.dialog( "open" );
			document.charset = form.acceptCharset;
			form.submit();
		}
		</script>
		
		
	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';

?>
        <section id="subcontainer">
        	<div class="layer1120">
        		<aside class="left">
        			<?php include_once 'left.php'; ?>
        		</aside>
        		<article class="subcon">
        			<div class="path">
        				<ul>
        					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
        					<li><?php echo $left?></li>
        					<li><?php echo $title?></li>
        				</ul>
        			</div>
        			<h4><?php echo $title?></h4>
        			<div class="subcontents">
        				<div class="tabmenu-wrap">
        					<ul class="sub-tabmenu type2 n4">
        						<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
        					</ul>
        				</div>			
        				<div class="join-regi">
        					<form id="form-item-new">
        						<input type="hidden" name="order_code" value="<?php echo $order_code ?>">
        						
        						<div class="table-wrap">
        							<table class="cst-table border-table regi-table">
        								<thead>
        									<tr>
        										<th class="active">내용</th>
        										<th class="active">납부할 금액</th>
        									</tr>
        								</thead>
        								<tbody>
        						<?php
        						$total_amount = 0;  // 결제할 금액 합계
        
        						foreach ($prod_results as $key => $val) {
        							$prod_id = $val['product_id'];
        							$prod_name = $val['product_name'];
        							$product_price = $val['product_price'];
        							$total_amount += $product_price;
        						?>
        									<tr>
        										<td>
        											<?php echo $prod_name ?>
        										</td>
        										<td>
        											<?php echo number_format($product_price) ?>
        											<input type="hidden" name="product_id[]" value="<?php echo $prod_id ?>">
        										</td>
        									</tr>
        						<?php
        						}
        						?>
        								</tbody>
        								<tfoot>
        									<tr>
        										<th class="active">결제금액 합계</th>
        										<th class="active"><?php echo number_format($total_amount) ?></th>
        									</tr>
        								</tfoot>
        							</table>
        						</div>
        						<br>
        						<div class="text-center mt30">
        							<button type="button" onclick="openDialog()" id="btn-submit" class="btn btn-primary btn-lg">결제하기</button>
        						</div>
        					</form>
        				</div>			
        			</div>
        		</article>
        	</div>
        </section>
        
        
        <?php 
        // debug
//         $total_amount = 1004;
        ?>
        
        <!-- Paynuri Form -->
        <form action="https://pg.paynuri.com/paymentgateway/pc/reqPay.do" method="post" name="frmPayment" id="frmPayment" target="iframe_payment" accept-charset="EUC-KR">
        	<input type="hidden" name="STOREID" value="<?php echo $store_id ?>">
        	<input type="hidden" name="CRYPTO_KEY" value="<?php echo $crypto_key ?>">
        	<input type="hidden" name="STORE_NAME" value="<?php echo $store_name ?>">
        	<input type="hidden" name="STORE_LOGO" value="https://demo.paynuri.com/images/logo_shop.png">
        	<input type="hidden" name="BUSINESS_NO" value="<?php echo $biz_no ?>">
        	<input type="hidden" name="TX_USER_DEFINE" value="<?php echo $store_name ?>^<?php echo $biz_no ?>^<?php echo $store_name ?>^<?php echo $site_phone ?>">
        	<!-- // 고객통장인자내용(10Byte이내)^쇼핑몰사업자번호(10Byte이내)^쇼핑몰명(30Byte이내)^쇼핑몰연락처(20Byte이내) -->
        	<input type="hidden" name="TRAN_NO" value="<?php echo $order_code ?>"><!-- 가맹점주문번호 -->
        	<input type="hidden" name="PRODUCTTYPE" value="2"><!-- 상품구분, 디지털 : 1, 실물 : 2 -->
        	<input type="hidden" name="TAX_FREE_CD" value="00"><!-- 과세/비과세  과세 : 00, 비과세 : 01 -->
        	<input type="hidden" name="GOODS_NAME" value="<?php echo $order_name ?>"><!-- 상품명 -->
        	<input type="hidden" name="AMT" value="<?php echo $total_amount ?>"><!-- 결제금액, 테스트시 결제금액은 항상 1004원으로 해주시기 바랍니다.-->
        	<input type="hidden" name="QUANTITY" value="1"><!-- 수량 -->
        	<input type="hidden" name="SALE_DATE" value="<?php echo date('ymd') ?>"><!-- 거래일자 yymmdd-->
        	<input type="hidden" name="CUSTOMER_NAME" value="<?php echo $user_name ?>"><!-- 고객명 -->
        	<input type="hidden" name="NOTICE_URL" value=""><!-- 입금통보URL(가상계좌) -->
        	<input type="hidden" name="RETURN_URL" value="<?php echo EXT_URL ?>/paynuri/result.php"><!-- 리턴URL -->
        	<input type="hidden" name="COMPLETE_URL" value="<?php echo EXT_URL ?>/paynuri/complete.php"><!--  결제성공 URL -->
        	<input type="hidden" name="CANCEL_URL" value="<?php echo EXT_URL ?>/paynuri/cancel.php?order_code=<?php echo $order_code ?>"><!-- 결제취소 URL -->
        	<input type="hidden" name="STORE_URL" value="<?php echo FQDN ?>"><!-- 가맹점 URL -->
        	<input type="hidden" name="CUSTOMER_EMAIL" value="<?php echo $user_email ?>"><!-- 고객 이메일 -->
        	<input type="hidden" name="CUSTOMER_TEL" value="<?php echo $user_mobile ?>"><!-- 고객전화번호 -->
        	<input type="hidden" name="CUSTOMER_IP" value="<?php echo $remote_ip ?>"><!-- 고객 IP -->
        	<input type="hidden" name="TRAN_TYPE" value="CARD"><!-- 결제수단,  -->
        	<input type="hidden" name="ETC_DATA1" value="<?php echo $order_id ?>"><!-- 커스텀 필드1 -->
        	<input type="hidden" name="ETC_DATA2" value="<?php echo $user_id ?>"><!-- 커스텀 필드2 -->
        	<input type="hidden" name="ETC_DATA3"><!-- 커스텀 필드3 -->
        </form>
        <!-- /.Paynuri Form -->
        
        
        
        <!-- Paynuri 결제용 필수 .. -->
        <div id="dialog_payment" style="margin: 0; padding: 0;overflow:hidden" >
        	<iframe name="iframe_payment" id="iframe_payment" src="about:blank" width="570" height="600" scrolling="no" frameborder="0" ></iframe>
        </div>
        <!-- /.Paynuri 결제용 필수 .. -->

<?php 
require_once INC_PATH . '/front-footer.php';
?>