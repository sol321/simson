<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-payment.php';

$on4 = 'on';
$left = 'MYPAGE';
$title = '회비납부';

if_authenticate_user();

if (empty($_POST['product_id'])) {
    if_js_alert_back('납입할 회비 내역을 선택해 주십시오.');
}

$user_id = if_get_current_user_id();
// $expiry_dt = if_get_user_meta($user_id, 'membership_expiry_date');

$user_row = if_get_user_by_id($user_id);
$user_name = $user_row['name_ko'];

$product_id = $_POST['product_id'];
$prod_results = if_get_products($product_id);

// 무통장입금 계좌 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec_bank = json_decode($if_bank_account_data, true);

// 주문번호 생성
$order_code = if_make_order_no(16);

require_once INC_PATH . '/front-header.php';
?>
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>
	<body>

<?php 
require_once INC_PATH . '/front-gnb.php';

?>
        <section id="subcontainer">
        	<div class="layer1120">
        		<aside class="left">
        			<?php include_once 'left.php'; ?>
        		</aside>
        		<article class="subcon">
        			<div class="path">
        				<ul>
        					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
        					<li><?php echo $left?></li>
        					<li><?php echo $title?></li>
        				</ul>
        			</div>
        			<h4><?php echo $title?></h4>
        			<div class="subcontents">
        				<div class="tabmenu-wrap">
        					<ul class="sub-tabmenu type2 n4">
            					<li class="on"><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues.php">회비 납부</a></li>
            					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_dues_list.php">납부 완료 목록</a></li>
            					<li><a href="<?php echo CONTENT_URL ?>/mypage/payment_bank_waiting.php">무통장입금 대기</a></li>
        					</ul>
        				</div>			
                		<div class="join-regi">
                			<form id="form-item-new">
                				<input type="hidden" name="order_code" value="<?php echo $order_code ?>">
                				
        						<div class="table-wrap">
            						<table class="cst-table border-table regi-table">
                    					<thead>
        									<tr>
                								<th class="active">내용</th>
                								<th class="active">납부할 금액</th>
                							</tr>
        								</thead>
                						<tbody>
                				<?php
                				$total_amount = 0;  // 결제할 금액 합계
        
                				foreach ($prod_results as $key => $val) {
                				    $prod_id = $val['product_id'];
                				    $prod_name = $val['product_name'];
                				    $product_price = $val['product_price'];
                				    $total_amount += $product_price;
                				?>
                							<tr>
                								<td>
                									<?php echo $prod_name ?>
                								</td>
                								<td>
                									<?php echo number_format($product_price) ?>
                									<input type="hidden" name="product_id[]" value="<?php echo $prod_id ?>">
                								</td>
                							</tr>
                				<?php
                				}
                				?>
                						</tbody>
                        				<tfoot>
                        					<tr>
                        						<th class="active">입금금액 합계</th>
                        						<th class="active"><?php echo number_format($total_amount) ?></th>
                        					</tr>
                        				</tfoot>
                    				</table>
                    				
                    				<p>&nbsp;</p>
                    				
                    				<table class="cst-table border-table regi-table">
                						<colgroup>
                							<col style="width: 20%;">
                							<col style="width: 80%;">
                						</colgroup>       				
                    					<tbody>
                    						<tr>
                    							<th class="active">입금인</th>
                    							<td>
                    								<div class="col-md-4">
                    									<input type="text" name="remitter" id="remitter" class="form-control" value="<?php echo $user_name ?>">
                    								</div>
                    							</td>
                    						</tr>
                    						<tr>
                    							<th class="active">입금 예정일</th>
                    							<td>
                    								<div class="col-md-4">
                    									<input type="text" name="due_date" id="due_date" class="form-control" value="<?php echo date('Y-m-d') ?>">
                    								</div>
                    							</td>
                    						</tr>
                    						<tr>
                    							<th class="active">입금 은행</th>
                    							<td>
                    								<div class="col-md-12">
                        					<?php 
                        					if (!empty($jdec_bank)) {
                        					    foreach ($jdec_bank as $key => $val) {
                        					        foreach ($val as $k => $v) {
                        					            $exp = explode("\t", $v);
                        					            $opt_text = $exp[0] . ' ' . $exp[1] . ' ' . $exp[2];
                        					?>
                        								<div class="radio">
                        									<label>
                        										<input type="radio" name="bank_name" class="bank_name" value="<?php echo $opt_text ?>"> <?php echo $opt_text ?>
                        									</label>
                        								</div> 
                        					<?php 
                        					        }
                        					    }
                        					}
                        					?>
                    								</div>
                    							</td>
                    						</tr>
                    					</tbody>
                					</table>
                				</div>
                				<br>
                				<div class="text-center">
                    				<button type="submit" id="btn-submit" class="btn btn-primary btn-lg">입금신청</button>
                    			</div>
                    		</form>
                		</div>			
        			</div>
        		</article>
        	</div>
        </section>
        
        <!-- jQueryUI -->
        <script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
        <script>
        $(function() {
        	$("#form-item-new").submit(function(e) {
            	e.preventDefault();
        
            	var chk = $(".bank_name:checked").length;
        
            	if (chk == 0) {
                	alert("입금은행을 선택해 주십시오.");
                	return false;
                }
            	
        		$.ajax({
        			type : "POST",
        			url : "./ajax/payment-bank-transfer.php",
        			data : $(this).serialize(),
        			dataType : "json",
        			beforeSend : function() {
        				$("#btn-submit").prop("disabled", true);
        			},
        			success : function(res) {
        				if (res.code == "0") {
        					if (res.result) {
        						alert("무통장입금 신청이 등록되었습니다.");
        						location.href = "payment_bank_waiting.php";
        					} else {
        						alert("등록하지 못했습니다.");
        					}
        				} else {
        					alert(res.msg);
        				}
        			}
        		}).done(function() {
        		}).fail(function() {
        		}).always(function() {
        			$("#btn-submit").prop("disabled", false);
        		}); // ajax
            });
        
        	$("#due_date").datepicker({
        		 dateFormat : "yy-mm-dd",
        		 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
        	     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
        		 changeMonth : true,
        		 changeYear : true,
        		 minDate: 0,
        		 maxDate: "+10D"
        	});
        });
        </script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>