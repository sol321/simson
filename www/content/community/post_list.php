<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-user.php';
require_once INC_PATH . '/classes/Paginator.php';

$is_list = true;
if (empty($_GET['bt'])) {
    if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}

/* 게시판에 대한 정보 */
$tpl_id = $_GET['bt'];
$board_row = if_get_board($tpl_id);

if (empty($board_row)) {
    if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

/*권한확인*/
$meta_b=$board_row['meta_data'];
$meta_b = json_decode($meta_b, true);
$chk_read_level = !empty($meta_b['read_level'])?$meta_b['read_level']:"0";
$chk_write_level = !empty($meta_b['write_level'])?$meta_b['write_level']:"0";
switch($chk_read_level) {
    case "1000" : #회원
        if_authenticate_user();
        break;
    case "3000" : #관리자    
        if_js_alert_back('관리자만 접근 가능한 게시판입니다.');
        break;
    default:
        #비회원
        break;
}

$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];
/* --게시판에 대한 정보 */
$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if (!empty($q)) {
    $sql = " AND template_id = ? AND (post_content LIKE ? OR post_title LIKE ?) ";
    array_push($sparam, $tpl_id, '%' . $q . '%', '%' . $q . '%');
} else {
    $sql = " AND template_id = ? ";
    array_push($sparam, $tpl_id);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_posts'] . "
		WHERE
			post_state = 'open' AND 
            post_type_secondary <> 'answer'
			$sql
		ORDER BY
			post_order DESC,
			seq_id DESC
";
			
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
			
require_once INC_PATH . '/front-header.php';

?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<?php include_once 'post_tap.php'; ?>
    			<?php if(!strcmp($tpl_skin,"GAL")){
    				include_once CONTENT_PATH."/community/post_gallery_skin.php";
    			} else {?>
 				<div class="table-wrap">
					<table class="cst-table m-board">
						<caption>게시판</caption>
						<colgroup>
							<col style="width: 7%;">
							<col>
							<col style="width: 15%;">
							<col style="width: 15%;">
							<col style="width: 10%;">
							<!-- col style="width: 12%;"> -->
						</colgroup> 
						<thead>
							<tr>
								<th scope="col">번호</th>
								<th scope="col">제목</th>
								<th scope="col">작성자</th>
								<th scope="col">등록일</th>
								<th scope="col">조회</th>
								<!-- <th scope="col">첨부파일</th>-->
							</tr>
						</thead>
						<tbody>
        				<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$post_id = $val['seq_id'];
    							$post_title = htmlspecialchars($val['post_title']);
    							$post_date = substr($val['post_date'],0,10);
    							$post_order = $val['post_order'];
    							$post_name = htmlspecialchars($val['post_name']);
    							$post_view_count = $val['post_view_count'];
    							// 필독(Top) 여부
    							$notice_label = empty($post_order) ? '' : '<span class="label label-danger">필독</span>';
    							if(!strcmp($tpl_skin,'QNA')){
    							    $reply_row = if_get_answer_post($post_id);
    							}
    						?>
						<tr id="post-id-<?php echo $post_id ?>">
							<td><?php echo $list_no ?></td>
							<td class="text-left m-board-show">
								<a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=<?php echo $_GET['bt'];?>&post_id=<?php echo $post_id;?>&page=<?php echo $page;?>"><?php echo $notice_label ?><?php echo $post_title ?></a>
								<div class="m-show">
									<span class="d-block">
										<span class="d-inline"><?php echo $post_name ?></span> <span class="d-inline"><?php echo $post_date ?></span> <span class="d-inline"><?php echo $post_view_count;?></span>
									</span>
								</div>  											
							</td>
							<td><?php echo $post_name ?></td>
							<td><?php echo $post_date ?></td>
							<td>
								<?php echo $post_view_count;?>
							</td>
						</tr>
						<?php 
						if (!empty($reply_row)) {
						?>
							<tr id="post-id-<?php echo $reply_row['seq_id'] ?>" class="active">
								<td></td>
								<td class="text-left m-board-show">
									<a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=<?php echo $_GET['bt'];?>&post_id=<?php echo$reply_row['seq_id'];?>&page=<?php echo $page;?>" class="nowrap">└ &nbsp;<?php echo $notice_label ?><?php echo $reply_row['post_title']?></a>
    								<div class="m-show">
    									<span class="d-block">
    										<span class="d-inline"><?php echo $reply_row['post_name'] ?></span> <span class="d-inline"><?php echo substr($reply_row['post_date'],0,10) ?></span> <span class="d-inline"><?php echo $reply_row['post_view_count'];?></span>
    									</span>
    								</div>  	        											
								</td>
								<td><?php echo $reply_row['post_name'] ?></td>
								<td><?php echo substr($reply_row['post_date'],0,10) ?></td>
								<td><?php echo $reply_row['post_view_count'] ?></td>
							</tr>
						<?php 
						}
						?>  									
    					<?php
    							$list_no--;
    						}
    					} else {?>
    					<tr><td colspan="5">등록된 게시글이 없습니다.</td></tr>
                        <?php 
    					}
    					?>						
						</tbody>
					</table>
				</div>   			
    			<?php }?>
				<?php if(!strcmp($chk_write_level,"0")) {?>
				<div class="btn-wrap text-right">
					<a href="<?php echo CONTENT_URL ?>/community/post_add.php?bt=<?php echo $tpl_id ?>" class="btn btn-primary">글쓰기</a>
				</div>
				<?php } else if(!strcmp($chk_write_level,"1000") && if_get_current_user_id() ){?>
				<div class="btn-wrap text-right">
					<a href="<?php echo CONTENT_URL ?>/community/post_add.php?bt=<?php echo $tpl_id ?>" class="btn btn-primary">글쓰기</a>
				</div>
				<?php }?>
				<br>				
    			<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        		</div>	    							
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>