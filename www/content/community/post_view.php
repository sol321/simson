<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-user.php';

if (empty($_GET['bt'])) {
	if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}
if (empty($_GET['post_id'])) {
	if_js_alert_back('게시글에 대한 정보가 필요합니다.');
}

/* 게시판에 대한 정보 */
$tpl_id = $_GET['bt'];
$board_row = if_get_board($tpl_id);
$tpl_skin = $board_row['tpl_skin'];
if (empty($board_row)) {
	if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

/*권한확인*/
$meta_b=$board_row['meta_data'];
$meta_b = json_decode($meta_b, true);
$chk_read_level = !empty($meta_b['read_level'])?$meta_b['read_level']:"0";
$chk_write_level = !empty($meta_b['write_level'])?$meta_b['write_level']:"0";

switch($chk_read_level) {
	case "1000" : #회원
		if_authenticate_user();
		break;
	case "3000" : #관리자
		if_js_alert_back('관리자만 접근 가능한 게시판입니다.');
		break;
	default:
		#비회원
		break;
}


/* 게시글에 대한 정보*/
$post_id = $_GET['post_id'];
$post_row = if_get_post($post_id);
if(empty($post_row['seq_id'])){
    if_js_alert_back('게시글이 존재하지 않습니다.');
}

if($post_row['template_id'] != $_GET['bt']){
    if_js_alert_back('게시글이 존재하지 않습니다.');
}
/* 게시글 이전글 다음글 */
$pre_post_row = if_get_pre_post($tpl_id, $post_id, $post_row['post_order']);
$next_post_row = if_get_next_post($tpl_id, $post_id, $post_row['post_order']);

$user_id = if_get_current_user_id();
$name_ko = '';
if(!empty($user_id)){
    $user_data = if_get_user_by_id($user_id);
    $name_ko = $user_data['name_ko'];
}

// $post_row -> variable 변수 할당
foreach ($post_row as $key => $val) {
	${"col_$key"} = $val;
}

$col_meta_data = json_decode($col_meta_data, true);
$file_attachment = empty($col_meta_data['file_attachment']) ? '' : $col_meta_data['file_attachment'];
if_update_post_view_count($_GET['post_id']); //뷰 카운트

$tpl_max_filesize = $meta_b['tpl_max_filesize'];   // MB
$max_file_size = $tpl_max_filesize * 1048576;

// 댓글 조회
//$comment_results = if_get_post_comments($post_id, 'DESC');
//댓글 사용여부
$use_comment = !empty($meta_b['use_comment']) ? $meta_b['use_comment'] : 'N';
if(!strcmp($use_comment,'Y')) {
    // 댓글 조회
    $comment_results = if_get_post_comments($post_id, 'DESC');
}
require_once INC_PATH . '/front-header.php';

?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<?php include_once 'post_tap.php'; ?>
				</div>
				<div class="table-wrap">
					<table class="cst-table m-board">
						<caption>글보기</caption>
						<colgroup>
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;">
							<col style="width: 16.67%;">
							<col style="width: 16.67%;">
							<col style="width: 16.67%;">
							<col style="width: 16.67%;">
						</colgroup> 
						<tbody>
							<tr>
								<th scope="row">제목</th>
								<td class="text-left m-board-show" colspan="5">
									<?php echo $col_post_title;?>
									<div class="m-show">
										<span class="d-block">
											<span class="d-inline">작성자 : <?php echo $col_post_name;?></span> <span class="d-inline">등록일 : <?php echo substr($col_post_date,0,10);?></span> <span class="d-inline">조회수 :<?php echo $col_post_view_count;?></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row">작성자</th>
								<td><?php echo $col_post_name;?></td>
								<th scope="row">등록일</th>
								<td><?php echo substr($col_post_date,0,10);?></td>
								<th scope="row">조회수</th>
								<td><?php echo $col_post_view_count;?></td>
							</tr>
							<?php if(strcmp($tpl_skin,"GAL")){?>
							<tr>
								<th scope="row">첨부파일</th>
								<td class="text-left m-board-show" colspan="5">
									<?php
									if ( !empty($file_attachment) ) {
										foreach ( $file_attachment as $key => $val ) {
											$attach_file_path = $val['file_path'];
											$attach_file_url = $val['file_url'];
											$attach_file_name = $val['file_name'];
											if (is_file($attach_file_path)) {
									?>
										<a href="<?php echo INC_URL ?>/lib/download_post_attachment.php?id=<?php echo $post_id;?>&idx=<?php echo $key;?>" class="btn-down">
											<?php echo $attach_file_name ?>
										</a>
									<?php
											}
										}
									}
									?>
								</td>
							</tr>
							<?php }?>
							<tr>
								<td class="text-left m-board-show" colspan="6">
									<div class="view-con">
										<?php echo $col_post_content;?>
										<?php if(!strcmp($tpl_skin,"GAL")){
											if ( !empty($file_attachment) ) {
												foreach ( $file_attachment as $key => $val ) {
													$attach_file_path = $val['file_path'];
													$attach_file_url = $val['file_url'];
													$file_chk_array = explode(".",$attach_file_url);
													$file_chk = $file_chk_array[count($file_chk_array)-1];
													$file_array = array('png','jpg','jpeg','gif','bmp');
													if(in_array($file_chk,$file_array)) {
													$attach_file_name = $val['file_name'];
														if (is_file($attach_file_path)) {
														?>
														<img src="<?php echo $attach_file_url;?>" alt=""><br>
												<?php }
													}
												}
											}
										}?>											
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btn-wrap text-right p30">
				<?php if(!empty($post_row['post_user_id'])){?>
					<?php if($post_row['post_user_id'] == if_get_current_user_id()){?>
						<a href="<?php echo CONTENT_URL ?>/community/post_edit.php?bt=<?php echo $tpl_id;?>&post_id=<?php echo $post_id;?>" class="btn btn-primary">수정하기</a>
						<a href="#n" class="btn btn-danger" data-toggle="modal" data-target="#myModal">삭제하기</a>									
					<?php }?>
				<?php } else{?>
					<?php if(!empty($post_row['post_passwd'])){?>
 					<a href="#n" data-toggle="modal" data-target="#myModal2"  class="btn btn-primary">수정하기</a>
					<a href="#n" class="btn btn-danger" data-toggle="modal" data-target="#myModal">삭제하기</a>   				
					<?php }?>
				<?php }?>				
					<a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=<?php echo $tpl_id;?>&page=<?php echo @$_GET['page'];?>" class="btn btn-default">목록</a>
				</div>
				<?php if(!strcmp($use_comment,'Y')) {?>
				<div class="prevnext table-wrap n-border mt30">
					<table class="cst-table m-board">
						<tr>
							<td class="text-left">
								<form id="form-new-comment" class="form article-view-add-comment">
									<input type="hidden" name="MAX_FILE_SIZE" id="max-file-size" value="<?php echo $max_file_size ?>">
									<input type="hidden" name="post_id" value="<?php echo $post_id ?>">
									<textarea id="comment_content" name="comment_content" class="form-control" rows="4" placeholder="댓글을 입력해 주세요."></textarea>
									<div class="clearfix form-inline" style="margin-top: 15px;">
										<span class="field name">
											<input type="text" id="comment_author" name="comment_author" class="form-control" maxlength="30" placeholder="이름" value="<?php echo $name_ko?>"> &nbsp; &nbsp;
										</span> 
        								<?php if(!$user_id){?>	
        									<span class="field password">
        										<input type="password" id="comment_pw" name="comment_pw" class="form-control" maxlength="20" placeholder="비밀번호"> &nbsp; &nbsp;
        									</span>
        								<?php }?>
										<span class="submit">
											<button type="submit" class="btn btn-primary btn-sm btn-submit">등록</button>
										</span>
									</div>
									<div class="clearfix form-inline" style="margin-top: 15px;">
										<span class="field name">
											<button type="button" class="btn btn-info btn-sm" id="btn-upload-attach">파일 첨부</button>
											<span>최대 <?php echo $tpl_max_filesize ?> MB (<?php echo number_format($max_file_size) ?> bytes)</span>
										</span> 
										<div style="margin-top: 5px;">
											<input type="file" name="attachment[]" id="file-attach" class="hide" multiple>
										</div>											
									</div>
									<div class="clearfix form-inline" style="margin-top: 15px;">
										<ul class="list-group" id="preview-attachment"></ul>											
									</div>
								</form>
							</td>
						</tr>
            			<?php 
            			if (!empty($comment_results)) {
            			?>
						<tr>
							<td>
            					<!-- 글의 댓글 목록 -->
                                <div class="article-view-comment text-left">
            			<?php
        				foreach ($comment_results as $key => $val) {
        				    $comment_id = $val['comment_id'];
        				    $comment_user_id = $val['comment_user_id'];
        					$comment_author = $val['comment_author'];
        					$comment_content = htmlspecialchars($val['comment_content']);
        					$comment_dt = $val['comment_dt'];
        					$meta_data = $val['meta_data'];
        					$time_ago = if_time_ago(strtotime($comment_dt));
        					
        					$jdec = json_decode($meta_data, true);
        					$comment_file_attachment = $jdec['file_attachment'];
            			?>
                                    <div class="article-comment" id="comment-<?php echo $comment_id ?>" style="margin-bottom: 7px; padding-bottom: 9px; border-bottom: 1px solid #eee;">
                                    	<div class="pull-right" style="color: #777;">
                                    		<?php if($comment_user_id){?>
                                    			<?php if($comment_user_id == $user_id){?>
                                    			<a href="#" class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#myModal4">삭제</a>
                                    			<?php }?>
                                    		<?php } else {?>
                                    			<a href="#" class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#myModal3">삭제</a>
                                    		<?php }?>
                                    	</div>
                                        <div class="article-comment-head" style="color: #aaa;">
                                            <span class="article-comment-name"><?php echo $comment_author ?></span> &nbsp;
                                            <span><?php echo $time_ago ?></span>
                                        </div>
                                        <div class="article-comment-body">
                                            <?php echo nl2br($comment_content) ?>
                                        </div>
                                        <div class="article-comment-attach">
                                <?php 
                                if (!empty($comment_file_attachment)) {
                                    foreach ($comment_file_attachment as $k => $v) {
                                        $file_name = $v['file_name'];
                                ?>
                                        	<a href="<?php echo INC_URL ?>/lib/download_comment_attachment.php?id=<?php echo $comment_id ?>&idx=<?php echo $k ?>" class="label label-info" style="color: white;">
                                        		<?php echo $file_name ?>
                                        	</a> &nbsp;
                                <?php 
                                    }
                                }
                                ?>
                                        </div>
                                    </div>
            			<?php
            			}
            			?>
                                </div>	
							</td>
						</tr>
            			<?php 
            			}
            			?>
					</table>
				</div>
				<?php }?>				
				<div class="prevnext table-wrap n-border mt30">
					<table class="cst-table border-table">
						<caption>이전글/다음글</caption>
						<colgroup>
							<col style="width: 20%;">
							<col style="width: 80%;">
						</colgroup>
						<tbody>
							<?php if(!empty($next_post_row)){?>
							<tr>
								<td class="text-left"><i class="xi-angle-up"></i> &nbsp;&nbsp;다음글</td>	
								<td class="text-left">
									<a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=<?php echo $_GET['bt'];?>&post_id=<?php echo $next_post_row['seq_id'];?>" class="btn-link nowrap"><?php echo $next_post_row['post_title'];?></a>
								</td>
							</tr>
							<?php }?>
							<?php if(!empty($pre_post_row)){?>
							<tr>
								<td class="text-left"><i class="xi-angle-down"></i> &nbsp;&nbsp;이전글</td>	
								<td class="text-left">
									<a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=<?php echo $_GET['bt'];?>&post_id=<?php echo $pre_post_row['seq_id'];?>" class="btn-link nowrap"><?php echo $pre_post_row['post_title'];?></a>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>

			</div>
		</article>
	</div>
</section>

<!-- Modal -->
<div class="modal fade cst-modal pop-password" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
			</div>
			<div class="modal-body">
				<form id="form-item-new">
					<input type="hidden" name="tpl_id" id="tpl_id" value="<?php echo $tpl_id;?>">
					<input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id;?>">
					<?php if(if_get_current_user_id() && $post_row['post_user_id'] == if_get_current_user_id()){?>
					<input type="hidden" name="user_id" id="user_id" value="<?php echo if_get_current_user_id();?>">
					<p class="text-center">
					해당 게시글을 삭제하시겠습니까?
					</p>
					<div class="pop-form">
						<div class="form-group" align="center">
							<button type="submit" class="btn btn-danger">삭제</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
						</div>
					</div>
					<?php } else {?>
					<p class="text-center">
					글쓴이와 관리자나 접근할 수 있습니다.<br />
					글 등록 시 입력한 비밀번호를 입력해 주세요.
					</p>
					<div class="pop-form">
						<div class="form-group">
							<input type="password" id="post_passwd" name="post_passwd" class="form-control" autocomplete="new-password">
							<button type="submit" class="btn btn-primary">확인</button>
						</div>
					</div>
					<?php }?>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade cst-modal pop-password" id="myModal2" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
			</div>
			<div class="modal-body">
				<p class="text-center">
				글쓴이와 관리자나 접근할 수 있습니다.<br />
				글 등록 시 입력한 비밀번호를 입력해 주세요.
				</p>
				<form id="form-item-new2">
					<input type="hidden" name="tpl_id" id="tpl_id" value="<?php echo $tpl_id;?>">
					<input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id;?>">
					<div class="pop-form">
						<div class="form-group">
							<input type="password" id="post_passwd" name="post_passwd" class="form-control" autocomplete="new-password">
							<button type="submit" class="btn btn-primary">확인</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal 댓글 삭제 -->
<div class="modal fade cst-modal" id="myModal3" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
			</div>
			<div class="modal-body">
				<p class="text-center">
				댓글 등록 시 입력한 비밀번호를 입력해 주세요.
				</p>
				<form id="form-comment-delete">
					<input type="hidden" name="comment_id" id="comment_id" value="0">
					<div class="pop-form">
						<div class="form-group">
							<input type="password" id="comment_pw" name="comment_pw" class="form-control" autocomplete="new-password">
							<button type="submit" class="btn btn-primary">확인</button>
						</div>
					</div>
				</form>	
			</div>
		</div>
	</div>
</div>
<!-- Modal 댓글 삭제 -->
<div class="modal fade cst-modal" id="myModal4" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="xi-close"></i></button>
			</div>
			<div class="modal-body">
				<p class="text-center">
				댓글을 삭제하시겠습니까?
				</p>
				<form id="form-comment-delete2">
					<input type="hidden" name="comment_id" id="comment_id2" value="0">
					<div class="pop-form">
						<div class="form-group">
							<button type="submit" class="btn btn-primary">확인</button>
						</div>
					</div>
				</form>	
			</div>
		</div>
	</div>
</div>

<!-- Form -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
<!-- Numeric -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
<!-- jQuery Validate Plugin -->
<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
<script>
$(function () {
	$("#form-item-new").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			type : "POST",
			url : "./ajax/post-del.php",
			data : $("#form-item-new").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					alert("삭제되었습니다.");
					location.href = "post_list.php?bt=" + $("#tpl_id").val();
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});

	$("#form-item-new2").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			type : "POST",
			url : "./ajax/post-login.php",
			data : $("#form-item-new2").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					location.href = "./post_edit.php?bt=" + $("#tpl_id").val()+"&post_id="+ $("#post_id").val();
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});	

	// 댓글 등록
	$("#form-new-comment").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/comment-add.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-comment-new").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					alert("등록했습니다.");
					location.reload();
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
		}); // ajax
	});

	//-------------------- 첨부파일 시작
	// 첨부파일 trigger
	$("#btn-upload-attach").click(function() {
		$("#file-attach").click();
	});

	// 첨부파일 Upload
	$("#file-attach").change(function() {
		var fsize = this.files[0].size;
		var fname = this.files[0].name;
		var fext = fname.split('.').pop().toLowerCase();
		var max_file_size = $("#max-file-size").val();

		if (fsize > max_file_size) {
			alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
			return;
		}
		
		$("#form-new-comment").ajaxSubmit({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
			dataType : "json",
			success: function(xhr) {
				if (xhr.code == "0") {
					var fileLists = '';
					for (var i = 0; i < xhr.file_url.length; i++) {
						fileLists += '<li class="list-group-item list-group-item-success">' +
											'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
											'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
											'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
											'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
											xhr.file_name[i] +
										'</li>';
					}
					$("#preview-attachment").append(fileLists);
				} else {
					alert(xhr.msg);
				}
				$('input[name="attachment[]"]').val("");
			}
		});
	});
	//-- 첨부파일 Upload 시작

	// 첨부파일 삭제
	$(document).on("click", ".list-group-item .delete-file-attach", function() {
		var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

		$.ajax({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
			data : {
				"filePath" : file
			},
			dataType : "json",
			success : function(res) {
				$("#preview-attachment li").each(function(idx) {
					var file = $(this).find('input[name="file_path[]"]').val();
					if (file == res.file_path) {
						$(this).fadeOut("slow", function() { $(this).remove(); });
					}
				});
				if (res.code != "0") {
					alert(res.msg);
				}
			}
		});
	});
	//-- 첨부파일 삭제
	//-------------------- 첨부파일 끝

	$(".btn-delete").click(function(e) {
		e.preventDefault();
		var id = $(this).closest(".article-comment").attr("id").replace(/\D/g, "");
		$("#comment_id").val(id);
		$("#comment_id2").val(id);
	});


	$("#form-comment-delete, #form-comment-delete2").submit(function(e) {
		e.preventDefault();

		$.ajax({
			type : "POST",
			url : "./ajax/comment-delete.php",
			data : $(this).serialize(),
			dataType : "json",
			beforeSend : function() {
			},
			success : function(res) {
				if (res.code == "0") {
					location.reload();
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
		}); // ajax
	});
	
});
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>