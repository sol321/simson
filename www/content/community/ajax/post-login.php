<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (empty($_POST['tpl_id'])) {
    $code = 102;
    $msg = '게시판에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_id'])) {
    $code = 102;
    $msg = '게시글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_passwd'])) {
    $code = 105;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

# 비밀번호 일치 하는지 확인
$result_post_passwd = if_get_passwd_post($_POST['post_id']);

if(empty($result_post_passwd) || $result_post_passwd != $_POST['post_passwd']){
    $code = 202;
    $msg = '비밀번호를 확인해주세요.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// 인증세션 저장
$_SESSION['post']['bt'] = $_POST['post_id'];

$json = compact('code', 'msg');
echo json_encode($json);


?>