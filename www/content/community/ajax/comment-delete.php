<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';
if (empty($_POST['comment_id'])) {
    $code = 102;
    $msg = '댓글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if(!if_get_current_user_id()) {
    if (empty($_POST['comment_pw'])) {
        $code = 105;
        $msg = '비밀번호를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$comment_id = $_POST['comment_id'];
$input_pw = !empty($_POST['comment_pw'])?$_POST['comment_pw']:'';

// 첨부파일 조횐
$row = if_get_post_comment_by_id($comment_id);
$comment_pw = $row['comment_pw'];
$comment_user_id = $row['comment_user_id'];

if(!if_get_current_user_id() || !$comment_user_id) {
    // 비밀번호 비교
    if (strcmp($input_pw, $comment_pw)) {
        $code = 103;
        $msg = '비밀번호가 일치하지 않습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
} else {
    // 작성자 비교
    if (strcmp(if_get_current_user_id(), $comment_user_id)) {
        $code = 103;
        $msg = '작성자가 일치하지 않습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$file_attachment = $jdec['file_attachment'];

if (!empty($file_attachment)) {
    foreach ($file_attachment as $key => $val) {
        $file_path = $val['file_path'];
        @unlink($file_path);
    }
}

$result = if_delete_post_comment($comment_id);

if (empty($result)) {
    $code = 201;
    $msg = '삭제하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>