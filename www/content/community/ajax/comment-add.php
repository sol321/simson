<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (empty($_POST['post_id'])) {
    $code = 102;
    $msg = '게시글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['comment_content'])) {
    $code = 103;
    $msg = '댓글 내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
if(!if_get_current_user_id()) {
    if (empty($_POST['comment_author'])) {
        $code = 104;
        $msg = '이름을 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    
    if (empty($_POST['comment_pw'])) {
        $code = 105;
        $msg = '비밀번호를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
$result = if_add_post_comemnt();

if (empty($result)) {
    $code = 201;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>