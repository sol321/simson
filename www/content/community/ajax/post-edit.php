<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (empty($_POST['tpl_id'])) {
    $code = 102;
    $msg = '게시판에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_id'])) {
    $code = 102;
    $msg = '게시글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_title'])) {
    $code = 103;
    $msg = '제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_name'])) {
    $code = 104;
    $msg = '작성자를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_user_id'])) {
    if (empty($_POST['post_passwd'])) {
        $code = 105;
        $msg = '비밀번호를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    # 비밀번호 일치 하는지 확인
    $result_post_passwd = if_get_passwd_post($_POST['post_id']);
    
    if(empty($result_post_passwd) || $result_post_passwd != $_POST['post_passwd']){
        $code = 202;
        $msg = '비밀번호를 확인해주세요.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
} else {
    #글쓴이가 맞는지
    $post_row = if_get_post($_POST['post_id']);
    if($post_row['post_user_id'] != $_POST['post_user_id']){
        $code = 202;
        $msg = '본인이 작성한 게시글만 수정 하실수 있습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}
$result = if_update_post();

if (empty($result)) {
    $code = 201;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

unset($_SESSION['post']['bt']);
$json = compact('code', 'msg');
echo json_encode($json);


?>