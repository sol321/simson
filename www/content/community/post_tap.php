<div class="tabmenu-wrap">
<?php if(isset($_GET['bt'])){?>
	<?php if($_GET['bt'] == 4 || $_GET['bt'] == 5) {?>
    <ul class="sub-tabmenu type2 n3">
    	<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">구입</a></li>
    	<li <?php if($_GET['bt'] == 4) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=4">공지사항</a></li>
    	<li <?php if($_GET['bt'] == 5) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=5">질문게시판</a></li>
    </ul>
    <?php } else if($_GET['bt'] == 6 || $_GET['bt'] == 7) {?>
    <ul class="sub-tabmenu type2 n3">
    	<li><a href="<?php echo CONTENT_URL ?>/publication/book2.php">구입</a></li>
    	<li <?php if($_GET['bt'] == 6) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=6">공지사항</a></li>
    	<li <?php if($_GET['bt'] == 7) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=7">질문게시판</a></li>
    </ul>
	<?php } else if($_GET['bt'] == 8 || $_GET['bt'] == 9 || $_GET['bt'] == 10) {?>
    <ul class="sub-tabmenu type2 n4">
    	<li><a href="<?php echo CONTENT_URL ?>/publication/exam_regist.php">단체 <br class="m-tab-br"/>구독신청</a></li>
    	<li <?php if($_GET['bt'] == 8) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=8">공지사항</a></li>
    	<li <?php if($_GET['bt'] == 9) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=9">질문게시판</a></li>
    	<li <?php if($_GET['bt'] == 10) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=10">FAQ</a></li>
    </ul>
	<?php } else if($_GET['bt'] == 1 || $_GET['bt'] == 11) {?>
	<ul class="sub-tabmenu type2 n2">
		<li <?php if($_GET['bt'] == 1) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">한국간호학회</a></li>
		<li <?php if($_GET['bt'] == 11) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=11">관련단체</a></li>
	</ul>
	<?php } else if($_GET['bt'] == 12 || $_GET['bt'] == 13) {?>
	<ul class="sub-tabmenu type2 n2">
		<li <?php if($_GET['bt'] == 12) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">학술∙교육∙행사</a></li>
		<li <?php if($_GET['bt'] == 13) echo "class='on'";?>><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=13">간호과학회 관련</a></li>
	</ul>	
	<?php }?>
<?php }?>
</div>
<?php if(isset($is_list) && $is_list) {?>
<div class="sch-wrap">
	<form class="form-inline">
		<fieldset>
			<div class="sch-box">
				<div class="form-group">
					<label for="key">검색어</label>
					<div class="sch-input">
						<input type="hidden" name="bt" value="<?php echo $tpl_id ?>">
						<input type="text" name="q" value="<?php echo $q ?>" class="form-control" placeholder="최대 50자 검색어 입력">
						<button type="submit" class="btn btn-primary bg-blue2">검색</button>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<?php }?>