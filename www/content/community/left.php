<?php if(isset($_GET['bt'])) {
    $menu_post_array1 = array();
    $menu_post_array2 = array();
    #국가시험대비문제집 / 기초간호학 문제집/ 학습역량평가 모의고사 게시판
    $menu_post_array1 = array(4, 5, 6, 7, 8, 9, 10);
    #회원공간 게시판
    $menu_post_array2 = array(1, 2 ,3, 11);
    #학술교육행사 게시판
    $menu_post_array3 = array(12,13);
    
?>
    <?php if(in_array($_GET['bt'],$menu_post_array1)) { 
        $left = '출판';
        if($_GET['bt'] == 4 || $_GET['bt'] == 5) {
                $on1 = 'on';
                $title = '국가시험 핵심 문제집 ';
            } else if($_GET['bt'] == 6 || $_GET['bt'] == 7){
                $on2 = 'on';
                $title = '기초간호학 문제집';
            } else if($_GET['bt'] == 8 || $_GET['bt'] == 9 || $_GET['bt'] == 10) {
                $on3 = 'on';
                $title = '학습역량평가 모의고사';
            } 
        ?>
        <div class="left-menu-top">
        	<h3>출판</h3>
        </div>
        <ul class="left-menu">
        	<li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/publication/book.php">국가시험 핵심 문제집 <i class="xi-angle-right"></i></a></li>
        	<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/publication/book2.php">기초간호학 문제집 <i class="xi-angle-right"></i></a></li>
        	<li class="<?php echo isset($on3)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/publication/exam_regist.php">학습역량평가 모의고사 <i class="xi-angle-right"></i></a></li>
        </ul>
    <?php } else if(in_array($_GET['bt'],$menu_post_array2)) {
        $left = '회원공간';
        if($_GET['bt'] == 1){
            $on5 = 'on';
            $title = '공지사항';
        } else if($_GET['bt'] == 2) {
            $on6 = 'on';
            $title = 'Q&A';
        } else if($_GET['bt'] == 3){
            $on8 = 'on';
            $title = '채용공고';
        } else if($_GET['bt'] == 11) {
            $on5 = 'on';
            $title = '공지사항';
        } 
    ?>
    	<div class="left-menu-top">
    		<h3>회원공간</h3>
    	</div>
    	<ul class="left-menu">
    		<!-- <li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php //echo CONTENT_URL ?>/member/login.php">로그인 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php //echo CONTENT_URL ?>/member/join.php">회원가입 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on3)?$on3:""?>"><a href="<?php //echo CONTENT_URL ?>/member/find.php">아이디/비밀번호 찾기 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on4)?$on4:""?>"><a href="<?php //echo CONTENT_URL ?>/member/mypage.php">My page <i class="xi-angle-right"></i></a></li> -->
    		<li class="<?php echo isset($on5)?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">공지사항 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on6)?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=2">Q&A <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on7)?$on7:""?>"><a href="<?php echo CONTENT_URL ?>/member/subscribe.php">학회지 구독신청 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on8)?$on8:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=3">채용공고 <i class="xi-angle-right"></i></a></li>
    		<li class="<?php echo isset($on9)?$on9:""?>"><a href="<?php echo CONTENT_URL ?>/member/sponsor.php">후원금 신청 <i class="xi-angle-right"></i></a></li>
    	</ul>
    <?php } else if(in_array($_GET['bt'],$menu_post_array3)) {
        $left = '학술∙교육∙행사';
        $on6 = 'on';
        $title = '자료실';        
        ?>
	<div class="left-menu-top">
		<h3>학술·교육·행사</h3>
	</div>
	<ul class="left-menu">
    	<li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술·교육·행사 안내 <i class="xi-angle-right"></i></a></li>
    	<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/index_reg.php">사전등록 <i class="xi-angle-right"></i></a></li>
    	<li class="<?php echo isset($on3)?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/index_abst.php">초록접수 <i class="xi-angle-right"></i></a></li>
    	<li class="<?php echo isset($on4)?$on4:""?>"><a href="<?php echo CONTENT_URL ?>/conference/register/confirm.php">등록 / 접수 확인 <i class="xi-angle-right"></i></a></li>
    	<li class="<?php echo isset($on5)?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">역대 학술·교육·행사 <i class="xi-angle-right"></i></a></li>
    	<li class="<?php echo isset($on6)?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">자료실 <i class="xi-angle-right"></i></a></li>
	</ul>        
    <?php }?>
<?php }?>