<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';
require_once FUNC_PATH . '/functions-post.php';

if (empty($_GET['bt'])) {
    if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}

/* 게시판에 대한 정보 */
$tpl_id = $_GET['bt'];
$board_row = if_get_board($tpl_id);

if (empty($board_row)) {
    if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];
$meta_data = $board_row['meta_data'];
$tpl_max_filesize = if_get_val_from_json($meta_data, 'tpl_max_filesize');   // MB
$max_file_size = $tpl_max_filesize * 1048576;


/* 게시판에 대한 정보 */
$tpl_id = $_GET['bt'];
$board_row = if_get_board($tpl_id);

if (empty($board_row)) {
    if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

/* 게시글에 대한 정보*/
$post_id = $_GET['post_id'];
$post_row = if_get_post($post_id);

if(!empty($post_row['post_user_id'])){
    if($post_row['post_user_id'] != if_get_current_user_id()){
        if_js_alert_back('작성자만 글 수정이 가능합니다.');
    }
} else {
    if(empty($_SESSION['post']['bt']) || $post_id != $_SESSION['post']['bt']){
        if_redirect(CONTENT_URL.'/community/post_list.php?bt='.$tpl_id);
    }
}

// $post_row -> variable 변수 할당
foreach ($post_row as $key => $val) {
    ${"col_$key"} = $val;
}

$col_meta_data = json_decode($col_meta_data, true);
$file_attachment = empty($col_meta_data['file_attachment']) ? '' : $col_meta_data['file_attachment'];
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<?php include_once 'post_tap.php'; ?>
				</div>
				<form id="form-item-new">
				<input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id;?>">
				<input type="hidden" name="MAX_FILE_SIZE" id="max-file-size" value="<?php echo $max_file_size ?>">
				<input type="hidden" name="tpl_id" id="tpl_id" value="<?php echo $tpl_id ?>">
				<input type="hidden" name="post_user_id" id="post_user_id" value="<?php echo $post_row['post_user_id'] ?>">
				<?php if(!strcmp($tpl_skin,'QNA')){?>
				<input type="hidden" name="post_type" id="post_type" value="qna_new">	
				<input type="hidden" name="post_type_secondary" id="post_type_secondary" value="question">
				<?php }?>									
					<fieldset>
						<div class="table-wrap border-table write-table">
							<table class="cst-table">
								<caption>글쓰기</caption>
								<colgroup>
									<col style="width: 20%;"> 
									<col style="width: 80%;">
								</colgroup> 
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td class="text-left">
											<input type="text" name="post_title" id="post_title" class="form-control" value="<?php echo $col_post_title;?>">
										</td>
									</tr>
									<tr>
										<th scope="row">작성자</th>
										<td class="text-left">
											<input type="text" name="post_name" id="post_name" class="form-control" value="<?php echo $col_post_name;?>">
										</td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td class="text-left">
											<textarea name="post_content" id="post_content" cols="30" rows="10" class="form-control"><?php echo $col_post_content;?></textarea>
										</td>
									</tr>
									<tr>
										<th scope="row">첨부파일</th>
										<td class="text-left">
        									<div class="btn btn-success btn-upload-attach">
        										<i class="fa fa-upload" aria-hidden="true"></i> 파일 첨부
        									</div>
        									<span>최대 <?php echo $tpl_max_filesize ?> MB (<?php echo number_format($max_file_size) ?> bytes)</span>
        									<div style="margin-top: 5px;">
        										<input type="file" name="attachment[]" id="file-attach" class="hide" multiple>
        									</div>        									
        									<ul class="list-group" id="preview-attachment">
                    						<?php
                    						if ( !empty($file_attachment) ) {
                    							foreach ( $file_attachment as $key => $val ) {
                    								$attach_file_path = $val['file_path'];
                    								$attach_file_url = $val['file_url'];
                    								$attach_file_name = $val['file_name'];
                    								if (is_file($attach_file_path)) {
                    						?>
                    										<li class="list-group-item list-group-item-success">
                    											<input name="file_path[]" value="<?php echo $attach_file_path ?>" type="hidden">
                    											<input name="file_url[]" value="<?php echo $attach_file_url ?>" type="hidden">
                    											<input name="file_name[]" value="<?php echo $attach_file_name ?>" type="hidden">
                    											<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>
                    											<a href="<?php echo INC_URL ?>/lib/download_post_attachment.php?id=<?php echo $post_id ?>&idx=<?php echo $key ?>" class="btn btn-success btn-xs">
                    												<?php echo $attach_file_name ?>
                    											</a>
                    										</li>
                    						<?php
                    								}
                    							}
                    						}
                    						?>
        									</ul>									
										</td>
									</tr>
									<?php if(empty($post_row['post_user_id'])){?>
									<tr>
										<th scope="row">비밀번호</th>
										<td class="text-left">
											<input type="password" name="post_passwd" id="post_passwd" class="form-control" autocomplete="new-password">
										</td>
									</tr>
									<?php }?>																		
								</tbody>
							</table>
							
						</div>
						<div class="btn-wrap text-center p30">
							<button type="submit" id="btn-submit" class="btn btn-primary">수정</button>	
    						<button type="button" id="btn-cancel" class="btn btn-danger">목록</button>
						</div>					
					</fieldset>
				</form>

			</div>
		</article>
	</div>
</section>

<!-- Form -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
<!-- Numeric -->
<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
<!-- jQuery Validate Plugin -->
<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
<!-- CKEditor -->
<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
<script>
$(function () {
	// CKEditor file upload
	CKEDITOR.replace("post_content", {
		toolbar :[['Font','FontSize','-','TextColor','BGColor','-','Bold','Italic','Underline','StrikeThrough','-','Image','Maximize']],
    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
    });

	//-------------------- 첨부파일 시작
	// 첨부파일 trigger
	$(".btn-upload-attach").click(function() {
		$("#file-attach").click();
	});

	// 첨부파일 Upload
	$("#file-attach").change(function() {
		var fsize = this.files[0].size;
		var fname = this.files[0].name;
		var fext = fname.split('.').pop().toLowerCase();
		var max_file_size = $("#max-file-size").val();

		if (fsize > max_file_size) {
			alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
			return;
		}

    	
		$("#form-item-new").ajaxSubmit({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
			dataType : "json",
			success: function(xhr) {
				if (xhr.code == "0") {
					var fileLists = '';
					for (var i = 0; i < xhr.file_url.length; i++) {
						fileLists += '<li class="list-group-item list-group-item-success">' +
											'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
											'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
											'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
											'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
											xhr.file_name[i] +
										'</li>';
					}
					$("#preview-attachment").append(fileLists);
				} else {
					alert(xhr.msg);
				}
				$('input[name="attachment[]"]').val("");
			}
		});
	});
	//-- 첨부파일 Upload 시작

	// 첨부파일 삭제
	$(document).on("click", ".list-group-item .delete-file-attach", function() {
		var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

		$.ajax({
			type : "POST",
			url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
			data : {
				"filePath" : file
			},
			dataType : "json",
			success : function(res) {
				$("#preview-attachment li").each(function(idx) {
					var file = $(this).find('input[name="file_path[]"]').val();
					if (file == res.file_path) {
						$(this).fadeOut("slow", function() { $(this).remove(); });
					}
				});
				if (res.code != "0") {
					alert(res.msg);
				}
			}
		});
	});
	//-- 첨부파일 삭제
	//-------------------- 첨부파일 끝

	// 폼 전송
	$("#form-item-new").submit(function(e) {
		e.preventDefault();
		CKEDITOR.instances.post_content.updateElement();
	});	

	$("#form-item-new").validate({
		rules: {
			post_title: {
				required: true
			},
			post_name: {
				required: true
			},
			post_passwd: {
				required: true
			}
		},
		messages: {
			post_title: {
				required: "제목을 입력해 주십시오."
			},
			post_name: {
				required: "작성자를 입력해 주십시오."
			},
			post_passwd: {
				required: "비밀번호를 입력해 주십시오."
			}
		},
		submitHandler: function(form) {
			$.ajax({
				type : "POST",
				url : "./ajax/post-edit.php",
				data : $("#form-item-new").serialize(),
				dataType : "json",
				beforeSend : function() {
					$("#btn-submit").prop("disabled", true);
				},
				success : function(res) {
					if (res.code == "0") {
						alert("수정되었습니다.");
						location.href = "post_list.php?bt=" + $("#tpl_id").val();
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax
		}
	});

	$("#btn-cancel").click(function() {
		history.back();
	});	
});	
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>