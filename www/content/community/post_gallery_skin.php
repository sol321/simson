<div class="gall-wrap">
<?php
if (!empty($item_results)) {
	$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
	foreach ($item_results as $key => $val) {
		$post_id = $val['seq_id'];
		$post_title = htmlspecialchars($val['post_title']);
		$meta_data = $val['meta_data'];
		$thumb = if_get_val_from_json($meta_data, 'thumb_attachment');
		$file_attach = if_get_val_from_json($meta_data, 'file_attachment');  // 대표이미지 없는 경우, 첫 사진을 썸네일로 사용하기 위함.
		$thumb_url = empty($thumb[0]) ? (empty($file_attach[0]['file_url'])?'': $file_attach[0]['file_url']): $thumb[0]['file_url'];
		
		$ext_chk = explode(".",$thumb_url);
		$ext_type = $ext_chk[count($ext_chk)-1];
		$file_size = @getimagesize($thumb_url);
		$image_stype = array('png','jpg','jpeg','gif');
		$fileWidth = $fileHeight = '';
		if(@in_array($ext_type,$image_stype)){
			if (isset($file_size[0]) && !empty($file_size[0])) {
				$fileWidth = ($file_size[0] > 260)? 260:$file_size[0];
				$fileHeight = ($file_size[1] > 150)? 150:$file_size[1];
			}
		} else {
		    $thumb_url = "";
		    $fileWidth = 260;
		    $fileHeight = 150;
		}
	?>
	<div class="gall-box n3">
		<a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=<?php echo $_GET['bt'];?>&post_id=<?php echo $post_id;?>&page=<?php echo $page;?>">
			<span class="img-wrap">
				<img src="<?php echo $thumb_url ?>" alt="대표 이미지" width=<?php echo $fileWidth;?> height=<?php echo $fileHeight;?> align="center">
			</span>
			<span class="tit"><?php echo $post_title ?></span>
		</a>
	</div>
<?php
		$list_no--;
	}
} else {?>
	No data.
<?php 
}
?>	
</div>