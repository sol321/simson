<div class="left-menu-top">
	<h3>학회소개</h3>
</div>
<ul class="left-menu">
	<li class="<?php echo isset($on1)?$on1:""?>"><a href="<?php echo CONTENT_URL ?>/about/intro.php">학회소개 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on2)?$on2:""?>"><a href="<?php echo CONTENT_URL ?>/about/greeting.php">회장인사말 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on3)?$on3:""?>"><a href="<?php echo CONTENT_URL ?>/about/history.php">연혁 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on4)?$on4:""?>"><a href="<?php echo CONTENT_URL ?>/about/organ.php">조직도 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on5)?$on5:""?>"><a href="<?php echo CONTENT_URL ?>/about/president.php">역대 회장 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on6)?$on6:""?>"><a href="<?php echo CONTENT_URL ?>/about/membership.php">회원학회 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on7)?$on7:""?>"><a href="<?php echo CONTENT_URL ?>/about/term.php">정관 <i class="xi-angle-right"></i></a></li>
	<li class="<?php echo isset($on8)?$on8:""?>"><a href="<?php echo CONTENT_URL ?>/about/contact.php">오시는 길 <i class="xi-angle-right"></i></a></li>
</ul>