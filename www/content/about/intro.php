<?php
require_once '../../if-config.php';
$on1 = 'on';
$left = '학회소개';
$title = '학회소개';

$content_type = 'intro';
$json = if_get_option($content_type);
$option_value = json_decode($json, true);

require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<?php echo $option_value ?>
			</div>
		</article>
	</div>
</section>		
<?php 
require_once INC_PATH . '/front-footer.php';
?>