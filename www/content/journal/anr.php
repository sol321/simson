<?php
require_once '../../if-config.php';
$on2 = 'on';
$left = '학술지';
$title = 'ANR (Asian Nursing Research)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
				<a href="https://www.asian-nursingresearch.com/" class="sub-link" target="_blank">홈페이지</a>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n4">
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/anr.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_rule.php">투고규정</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=anr">검색</a></li>
					</ul>
				</div>
				<div class="journal-wrap">
					<p>
						<strong>Asian Nursing Research</strong> is the official peer-reviewed research journal of the Korean Society of Nursing Science, 
						and is devoted to publication of a wide range of research that will contribute to the body of nursing science and 
						inform the practice of nursing, nursing education, administration, and history, on health issues relevant to
						nursing, and on the testing of research findings in practice.<br /><br />

						The ISO abbreviated title of the journal is Asian Nurs Res. It is published quarterly by Elsevier beginning in 
						June 2007, and indexed/abstracted in CINAHL, Embase, Science Direct, SCOPUS, Science Citation Index 
						Expanded(SCIE), Current Contents/ Social and Behavioral Sciences (SBS), Social Sciences Citation Index (SSCI)
						and Journal Citation Reports/Science Edition and Social Science es Edition. It has been indexed in Medline 
						since 2014.<br /><br />

						Free full-texts both in XML and PDF are available in the journal's homepage at<br />  
						<a href="http://www.asian-nursingresearch.com/" class="btn-link underline" target="_blank">http://www.asian-nursingresearch.com/</a><br /><br />

						Manuscripts should be submitted electronically via <a href="http://ees.elsevier.com/asiannursingresearch/" class="btn-link underline" target="_blank">http://ees.elsevier.com/asiannursingresearch/</a>. <br />
						The manuscript should be written in English and a minimum standard of the proficiency in the English language 
						should be met before submission to the editorial office. <br />
						(Tel: <a href="tel:+82-2-337-3337">+82-2-337-3337</a>, Fax: +82-2-564-0249, E-mail: <a href="mailto:info@inforang.com">info@inforang.com</a>).
					</p>
					<p class="text-right color-blue">
						PISSN (Print) 1976-1317<br />
						ISSN (Online) 2093-7482					
					</p>
					<p class="journal-copy">
						© Korean Society of Nursing Science, <br />
						Published by Elsevier Korea LLC. All rights reserved.
					</p>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>
