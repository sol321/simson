<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';
require_once INC_PATH . '/classes/Paginator.php';

if (empty($_GET['jt'])) {
    if_js_alert_back('학술지 대한 정보가 필요합니다.');
}

/* 학술지 종류에 대한 정보 */
$jn_type = $_GET['jt'];

if (empty($_GET['journal_id'])) {
    if_js_alert_back('학술지 대한 정보가 필요합니다.');
}

$journal_id = $_GET['journal_id'];

if($jn_type == "jkan"){
    $on1 = 'on';
    $left = '학술지';
    $title = 'JKAN(대한간호학회지)';
} else if($jn_type == "anr") {
    $on2 = 'on';
    $left = '학술지';
    $title = 'ANR (Asian Nursing Research)';
}

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$s_jn_year = !isset($_GET['s_jn_year']) ? '' : trim($_GET['s_jn_year']);
$s_jn_vol = !isset($_GET['s_jn_vol']) ? '' : trim($_GET['s_jn_vol']);
$s_jn_no = !isset($_GET['s_jn_no']) ? '' : trim($_GET['s_jn_no']);
$jn_year_vol = !isset($_GET['jn_year_vol']) ? '' : trim($_GET['jn_year_vol']);

if($jn_year_vol) {
    $in_data = explode("||",$jn_year_vol);
    $s_jn_year = $in_data[0];
    $s_jn_vol = $in_data[1];
}

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
    $sql .= "AND (author like ? OR j_title like ? OR j_keyword like ? OR j_abstract like ?)";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%', '%' . $qs1 . '%', '%' . $qs1 . '%');
}
if (!empty($s_jn_year) && !empty($s_jn_vol)) {
    $sql .= "AND jn_year = ? AND jn_vol = ? ";
    array_push($sparam, $s_jn_year, $s_jn_vol);
}
if (!empty($s_jn_no)) {
    $sql .= "AND jn_no = ? ";
    array_push($sparam, $s_jn_no);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$journal_data = if_get_journal_by_id($journal_id);
$journal_id = $journal_data['seq_id'];
$jn_vol = $journal_data['jn_vol'];
$jn_no = $journal_data['jn_no'];
$jn_year = $journal_data['jn_year'];
$jn_month = $journal_data['jn_month'];
$author = $journal_data['author'];
$j_title = $journal_data['j_title'];
$j_abstract = $journal_data['j_abstract'];
$j_keyword = $journal_data['j_keyword'];

$meta_data = $journal_data['meta_data'];
$jdec = json_decode($meta_data, true);
$page_from = $jdec['page_from'];
$page_to = $jdec['page_to'];
$file_attachment = $jdec['file_attachment'];

$journal_data_year = if_get_journal_list_by_year($jn_type);
$journal_data_no = array();
if($s_jn_year && $s_jn_vol) {
    $journal_data_no = if_get_journal_list_by_year_vol($jn_type, $s_jn_year, $s_jn_vol);
} 

require_once INC_PATH . '/front-header.php';

?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
			<?php if($jn_type == "jkan") {?>
				<a href="https://jkan.or.kr/index.php?body=instructions" class="sub-link" target="_blank">투고지침</a>
				<a href="https://www.jkan.or.kr/" class="sub-link" target="_blank">홈페이지</a>
			<?php } else if($jn_type == "anr") {?>
				<a href="https://www.asian-nursingresearch.com/" class="sub-link" target="_blank">홈페이지</a>
			<?php }?>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<?php if($jn_type == "jkan") {?>
					<ul class="sub-tabmenu type2 n3">
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan_comm.php">편집위원회</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=jkan">검색</a></li>
					</ul>					
					<?php } else if($jn_type == "anr") {?>
					<ul class="sub-tabmenu type2 n4">
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_rule.php">투고규정</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=anr">검색</a></li>
					</ul>					
					<?php }?>

				</div>
				<div class="sch-wrap">
					<form class="form-inline" id="search-form" action="journal_list.php">
					<input type="hidden" name="jt" id="jt" value="<?php echo $jn_type ?>">
						<fieldset>
							<div class="sch-box">
								<div class="form-group">
									<label for="key">검색어</label>
									<div class="sch-input">
										<input type="text" id="qs1" name="qs1" class="form-control" placeholder="최대 50자 검색어 입력" value="<?php echo $qs1;?>">
										<button type="submit" class="btn btn-primary bg-blue2">검색</button>
									</div>
								</div>
							</div>
							<div class="form-inline journal-sch">
								<p>년도별 ‘권’과 ‘호’를 선택해 주세요  (SSN 1598-2874(38권 4호까지), ISSN 2005-3673(38권 5호부터)</p>
								<div class="form-group">
									<select name="jn_year_vol" id="jn_year_vol" class="form-control">
										<option value="">년도별 권</option>
										<?php for($jn=0;$jn<count($journal_data_year); $jn++) {
										    ?>
											<option value="<?php echo $journal_data_year[$jn]['jn_year']."||".$journal_data_year[$jn]['jn_vol']?>" <?php echo ($s_jn_year == $journal_data_year[$jn]['jn_year'] && $s_jn_vol == $journal_data_year[$jn]['jn_vol'])?" selected":""?>><?php echo $journal_data_year[$jn]['journal_list']?></option>
										<?php
										  }?>
									</select>
									<select name="s_jn_no" id="s_jn_no" class="form-control">
										<option value="">년도별 호</option>
										<?php for($jn2=0;$jn2<count($journal_data_no);$jn2++) {?>
											<option value="<?php echo $journal_data_no[$jn2]['jn_no']?>" <?php echo ($s_jn_no == $journal_data_no[$jn2]['jn_no'])?" selected":""?>><?php echo $journal_data_no[$jn2]['jn_no']?></option>
										<?php }?>
									</select>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="table-wrap">
					<table class="cst-table view-table m-board">
						<caption>본문</caption>
						<colgroup>
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;"> 
							<col style="width: 16.67%;"> 
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">제목</th>
								<td class="text-left m-board-show" colspan="5">
									<?php echo $j_title;?>
									<div class="m-show">
										<span class="d-block">
											<span class="d-inline">작성자 : <?php echo $author;?></span> <span class="d-inline">게시일 : <?php echo $jn_year ?>년 <?php echo $jn_month ?>월</span> <span class="d-inline">페이지 : p<?php echo $page_from;?>~p<?php echo $page_to;?></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<th scope="row">작성자</th>
								<td><?php echo $author;?></td>
								<th scope="row">게시일</th>
								<td><?php echo $jn_year ?>년 <?php echo $jn_month ?>월</td>
								<th scope="row">페이지</th>
								<td>p<?php echo $page_from;?>~p<?php echo $page_to;?> <a href="#n" class="btn-print"><img src="/include/img/sub/ic-print.png" alt="인쇄"></a></td>
							</tr>
							<tr>
								<th scope="row">파일</th>
								<td class="text-left m-board-show" colspan="5">
		                    	<?php
        						if (!empty($file_attachment)) {
        							foreach ($file_attachment as $key => $val) {
        								$file_path = $val['file_path'];
        								$file_url = $val['file_url'];
        								$file_name = $val['file_name'];
        								if (is_file($file_path)) {
        						?>
										<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn-link" title="<?php echo $file_name ?>">
											<?php echo $file_name ?>
										</a>
        						<?php
        								}
        							}
        						}
        						?>					
								
									
								</td>
							</tr>
							
							
							<tr>
								<th scope="row">키워드</th>
								<td class="text-left m-board-show" colspan="5">
									<?php echo $j_keyword;?>
								</td>
							</tr>
							<tr>
								<td class="text-left m-board-show" colspan="6">
									<div class="view-con">
										<?php echo $j_abstract;?>									
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btn-wrap text-right p30">
					<a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=<?php echo $jn_type;?>" class="btn btn-default">목록</a>
				</div>		
			</div>
		</article>
	</div>
</section>
<script>
    $("#jn_year_vol").change(function() {
    	var data = $.trim($("#jn_year_vol").val());
    	var jt = $.trim($("#jt").val());
    	var data_array = data.split("||");
    	var jn_year = data_array[0];
    	var jn_vol = data_array[1];
    	
    	if (!data || !jt) {
    		location.href = "journal_list.php?jt="+jt;
    		return;
    	}
        	$.ajax({
    			type : "POST",
    			url : "./ajax/journal_year_vol.php",
    			data : {
    				"data" : data
    				,"jt" : jt
    			},
    			dataType : "json",
    			success : function(res) {
        			console.log(res);
    				if (res.code != "0") {
    					alert(res.msg);
    				} else {
    					$("select#jn_no option").remove();
    					$("select#jn_no").append("<option value=''>년도별 호</option>");
    					$("select#jn_no").append(res.result_msg);
    					location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol;
    				}
    			}
    		});   
    });

    $("#s_jn_no").change(function() {
    	var data = $.trim($("#jn_year_vol").val());
    	var s_jn_no = $.trim($("#s_jn_no").val());
    	var jt = $.trim($("#jt").val());
    	var data_array = data.split("||");
    	var jn_year = data_array[0];
    	var jn_vol = data_array[1];
    	
    	if (!data || !jt || !s_jn_no) {
    		location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol;
    		return;
    	}
        	$.ajax({
    			type : "POST",
    			url : "./ajax/journal_no.php",
    			data : {
    				"data" : data
    				,"jn_no" : s_jn_no
    				,"jt" : jt
    			},
    			dataType : "json",
    			success : function(res) {
        			console.log(res);
    				if (res.code != "0") {
    					alert(res.msg);
    				} else {
    					location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol+"&s_jn_no="+s_jn_no;
    				}
    			}
    		});   
    });   
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>