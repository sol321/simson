<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';

$code = 0;
$msg = '';

if ( empty($_POST['jt']) ) {
    $code = 100;
    $msg = '저널정보를 알수없습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['data']) ) {
    $code = 101;
    $msg = '년도별  권 정보를 선택해주세요.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$journal_data = explode("||" ,$_POST['data']);
$journal_year = $journal_data[0];
$journal_vol = $journal_data[1];

if ( empty($journal_year) ||  empty($journal_vol)) {
    $code = 102;
    $msg = '년도별  권 정보를 선택해주세요.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}


$journal_data_array = if_get_journal_list_by_year_vol($_POST['jt'], $journal_year, $journal_vol);
$msg = count($journal_data_array);
$result_msg = "";
for($jn=0;$jn<count($journal_data_array);$jn++) {
    $journal_data = $journal_data_array[$jn]['jn_no'];
    if(!empty($result_msg)) {
        $result_msg .= "<option value='".$journal_data."'>".$journal_data."호</option>";
    } else {
        $result_msg = "<option value='".$journal_data."'>".$journal_data."호</option>";
    }
}

$json = compact( 'code', 'msg' );
echo json_encode( $json );
?>