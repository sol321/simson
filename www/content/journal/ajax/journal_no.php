<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';

$code = 0;
$msg = '';

if ( empty($_POST['jt']) ) {
    $code = 100;
    $msg = '저널정보를 알수없습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['data']) ) {
    $code = 101;
    $msg = '년도별  권 정보를 선택해주세요.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$journal_data = explode("||" ,$_POST['data']);
$journal_year = $journal_data[0];
$journal_vol = $journal_data[1];

if ( empty($journal_year) ||  empty($journal_vol)) {
    $code = 102;
    $msg = '년도별  권 정보를 선택해주세요.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}
if ( empty($_POST['jn_no'])) {
    $code = 103;
    $msg = '년도별  호 정보를 선택해주세요.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}


$json = compact( 'code', 'msg' );
echo json_encode( $json );
?>