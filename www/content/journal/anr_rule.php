<?php
require_once '../../if-config.php';
$on2 = 'on';
$left = '학술지';
$title = 'ANR (Asian Nursing Research)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
				<a href="https://www.asian-nursingresearch.com/" class="sub-link" target="_blank">홈페이지</a>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n4">
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_comm.php">편집위원회</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/anr_rule.php">투고규정</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=anr">검색</a></li>
					</ul>
				</div>
				<div class="journal-wrap">
					<div class="btn-wrap text-right">
						<a href="<?php echo CONTENT_URL ?>/journal/GFA.pdf" class="btn-pdf" target="_blank">PDF Download</a>
					</div>
					<div class="journal-rule-img">
						<img src="<?php echo INC_URL ?>/img/sub/img-anr-rule.png" alt="">
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>