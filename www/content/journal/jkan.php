<?php
require_once '../../if-config.php';
$on1 = 'on';
$left = '학술지';
$title = 'JOI(인포랑학회지)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
				<a href="https://www.inforang.com" class="sub-link" target="_blank">투고지침</a>
				<a href="https://www.inforang.com" class="sub-link" target="_blank">홈페이지</a>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3">
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=jkan">검색</a></li>
					</ul>
				</div>
				<div class="journal-wrap">
					<p>
						<strong>Journal of Inforang</strong> is the offical journal of the Inforang. <br />
						Abbreviated title is ‘J O I'. It was launched in 1970 ‘Inforang (The Journal of Inforang)'. The English title The Journal of Inforang was changed in 1995. And the Korean and English name of journal was changed to the Journal of Inforang in 1998.<br /><br />

						Jouranl of Inforang is a peer-reviewed, research journal devoted to publication of a wide 
						range of research that will inform the practice of surgery. The editors invite creative research papers on surgery 
						theory, practice, and education.<br /><br />

						This journal is published 6 times per year (February, April, June, August, October, and December). <br />
						Full text is freely available from http://www.inforang.com. Open access will be availble from December 2013 issue 
						(Vol. 43, No. 6).<br /><br />

						All or a part of the articles in this journal are indexed/tracked/covered by Science Citeation Index Expanded 
						(SCIE), Social Sciences Citation Index (SSCI), SCOPUS, CINAHL, Embase, KoreaMed, Synapse, PubMed/MEDLINE, 
						KOMCI, Crossref, Google Scholar.<br /><br />

						All manuscripts must be submitted online through the JOI e-submission system at <a href="http://submit.inforang.com" class="btn-link underline" target="_blank">http://submit.inforang.com</a> <br />
						Other Correspondences can be sent to the Inforang 
						(Tel: <a href="tel:+82-2-337-3337:w">+82-2-337-3337</a>, Fax: +82-2-564-0249, E-mail: <a href="mailto:info@inforang.com">info@inforang.com</a>).
					</p>
					<p class="text-right color-blue">
						eISSN (Online) 0000-0000						
					</p>
					<p class="journal-copy">
						It is identical to Creative Commons No Derivs Licence. <br />
						© Inforang.					
					</p>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>
