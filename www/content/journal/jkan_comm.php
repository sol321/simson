<?php
require_once '../../if-config.php';
$on1 = 'on';
$left = '학술지';
$title = 'JOI(인포랑학회지)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
				<a href="https://jkan.or.kr/index.php?body=instructions" class="sub-link" target="_blank">투고지침</a>
				<a href="https://www.jkan.or.kr/" class="sub-link" target="_blank">홈페이지</a>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n3">
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">소개</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/jkan_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=jkan">검색</a></li>
					</ul>
				</div>
				<div class="journal-wrap">
					<div class="journal-comm">
						<div class="comm-box-wrap">
							<div class="comm-box w50">
								<span class="tit">Editor in Chief</span>
								<div class="text-wrap">
									<ul class="comm-list list-type3">
										<li><span class="name">Gil Dong Hong</span><p>The Catholic University, Korea</p></li>
									</ul>
								</div>
							</div>
							<div class="comm-box w50">
								<span class="tit">Associate Editor</span>
								<div class="text-wrap">
									<ul class="comm-list list-type3">
										<li><span class="name">Gil Dong Hong</span><p>Wonju College of Medicine, <br class="no-br"/>Yonsei University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Yonsei University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Kongju National university, Korea </p></li>
										<li><span class="name">Gil Dong Hong</span><p>Kongju National university, Korea</p></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="comm-box w100">
							<span class="tit">Editor</span>
							<div class="text-wrap">
								<div class="w50">
									<ul class="comm-list list-type3">
										<li><span class="name">Gil Dong Hong</span><p>Sungshin Women's University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Chungbuk National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Ajou University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Seoul National University, Korea </p></li>
										<li><span class="name">Gil Dong Hong</span><p>Cheongju University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>The Catholic University, Korea </p></li>
										<li><span class="name">Gil Dong Hong</span><p>Ulsan University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Seoul National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Chosun University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Pusan National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Baekseok University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Chungbuk National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>The Catholic University, Korea </p></li>
										<li><span class="name">Gil Dong Hong</span><p>Virginia Commonwealth University, U.S.A</p></li>
									</ul>
								</div>
								<div class="w50">
									<ul class="comm-list list-type3">
										<li><span class="name">Gil Dong Hong</span><p>Gyeongsang National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>The Catholic University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Chungnam National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Semyung University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Konkuk University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Sungshin Women's University, Korea </p></li>
										<li><span class="name">Gil Dong Hong</span><p>Keimyung University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Yonsei University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Hallym University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Yonsei University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Korea University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Kangwon National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Kunsan National University, Korea</p></li>
										<li><span class="name">Gil Dong Hong</span><p>Chonnam National University, Korea</p></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>
