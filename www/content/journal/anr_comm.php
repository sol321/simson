<?php
require_once '../../if-config.php';
$on2 = 'on';
$left = '학술지';
$title = 'ANR (Asian Nursing Research)';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="<?php echo INC_URL ?>/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
				<a href="https://www.asian-nursingresearch.com/" class="sub-link" target="_blank">홈페이지</a>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<ul class="sub-tabmenu type2 n4">
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr.php">소개</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/anr_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_rule.php">투고규정</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=anr">검색</a></li>
					</ul>
				</div>
				<div class="journal-wrap">
					<div class="journal-comm">
						<div class="comm-box-wrap">
							<div class="comm-box w50">
								<span class="tit">Editor in Chief</span>
								<div class="text-wrap">
									<ul class="comm-list list-type3">
										<li><span class="name">Eui Geum Oh, <b>PhD, RN, FAAN</b></span><p>Yonsei University, Korea</p></li>
									</ul>
								</div>
							</div>
							<div class="comm-box w50">
								<span class="tit">Associate Editor</span>
								<div class="text-wrap">
									<ul class="comm-list list-type3">
										<li><span class="name">Yeon-Hwan Park, <b>RN, PhD</b></span><p>WSeoul National University, Korea</p></li>
										<li><span class="name">Kyeong Hwa Kang, <b>RN, PhD</b></span><p>Hallym University, Korea</p></li>
										<li><span class="name">Soo Hyun Kim, <b>RN, PhD</b></span><p>Inha University, Korea</p></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="comm-box w100">
							<span class="tit">Editorial Board</span>
							<div class="text-wrap">
								<ul class="comm-list list-type3">
									<li><span class="name">Carol Estwing Ferrans, <b>PhD, RN, FAAN</b></span><p>University of Illinois, USA</p></li>
									<li><span class="name">Chia-Chin Lin, <b>RN, PhD, FAAN</b></span><p>University of Hong Kong, Hong Kong</p></li>
									<li><span class="name">Craig Lockwood, <b>RN, PhD</b></span><p>The University of Adelaide, Australia</p></li>
									<li><span class="name">Diana Tze Fan Lee, <b>PhD, RN, FAAN, JP</b></span><p>Chinese University of Hong Kong, Hong Kong</p></li>
									<li><span class="name">Domingo Palacios-Cena, <b>RN, PhD</b></span><p>Rey Juan Carlos University, Spain</p></li>
									<li><span class="name">Eun Kyeung Song, <b>RN, PhD</b></span><p>University of Ulsan, Korea</p></li>
									<li><span class="name">Eun Ok Im, <b>PhD, MPH, RN, CNS, FAAN</b></span><p>Duke University, USA</p></li>
									<li><span class="name">Gwi-Ryung Son Hong, <b>RN, PhD</b></span><p>Hanyang University, Korea</p></li>
									<li><span class="name">Hee Sun Kang, <b>RN, PhD</b></span><p>Chung-Ang University, Korea</p></li>
									<li><span class="name">Heejung Choi, <b>RN, PhD</b></span><p>Konkok University, Korea</p></li>
									<li><span class="name">Hyeonkyeong Lee, <b>RN, PhD</b></span><p>Yonsei University, Korea</p></li>
									<li><span class="name">Ilknur Aydin Avci, <b>RN, PhD</b></span><p>Ondokuz Mayis University, Turkey</p></li>
									<li><span class="name">Jane Hsiao-Chen Tang, <b>RN, PhD</b></span><p>Immaculata University, USA</p></li>
									<li><span class="name">Janet L. Larson, <b>PhD, RN, FAAN</b></span><p>University of Michigan, USA</p></li>
									<li><span class="name">Jiyeon Lee, <b>RN, PhD</b></span><p>Chungnam National University, Korea</p></li>
									<li><span class="name">Kerada Krainuwat,<b> RN, PhD, APN</b></span><p>Mahidol University, Thailand</p></li>
									<li><span class="name">Jeung Im Kim, <b>RN, PhD</b></span><p>Soonchunhyang University, Korea</p></li>
									<li><span class="name">Kyung-Ah Kang, <b>RN, PhD</b></span><p>Sahmyook University, Korea</p></li>
									<li><span class="name">Mio Ozawa, <b>RN, PhD</b></span><p>Hiroshima University, Japan</p></li>
									<li><span class="name">Mo-Kyung Sin, <b>RN, PhD</b></span><p>Seattle University, USA</p></li>
									<li><span class="name">Pamela Gail Hawranik, <b>RN, PhD</b></span><p>Athabasca University, Canada</p></li>
									<li><span class="name">Patraporn Tungpunkom, <b>RN, PhD</b></span><p>Chiang Mai University, Thailand</p></li>
									<li><span class="name">Sally Wai-chi Chan, <b>RN, PhD</b></span><p>University of Newcastle, Australia</p></li>
									<li><span class="name">Sek Ying Chair, <b>RN, PhD</b></span><p>The Chinese University of Hong Kong, Hong Kong</p></li>
									<li><span class="name">Soong-nang Jang, <b>RN, PhD</b></span><p>Chung-Ang University, Korea</p></li>
									<li><span class="name">Sun-Mi Chae, <b>RN, PhD</b></span><p>Seoul National University, Korea</p></li>
									<li><span class="name">Sun-Mi Lee, <b>RN, PhD</b></span><p>The Catholic Unoiversity of Korea, Korea</p></li>
									<li><span class="name">Tae Wha Lee, <b>RN, PhD</b></span><p>Yonsei University, Korea</p></li>
									<li><span class="name">Wai-Tong Chien, <b>RMN, RTN, PhD</b></span><p>The Hong Kong Polytechnic University, Hong Kong</p></li>
									<li><span class="name">Won-Oak Oh, <b>RN, PhD</b></span><p>Korea University, Korea</p></li>
									<li><span class="name">Yoonju Lee, <b>RN, PhD</b></span><p>Pusan National University, Korea</p></li>
								</ul>
							</div>
						</div>
						<p>
							This journal was supported by the Korean Federation of Science and Technology Societies Grant funded Korean Government(Ministry of Education).							
						</p>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>