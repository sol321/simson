<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';
require_once INC_PATH . '/classes/Paginator.php';

if (empty($_GET['jt'])) {
    if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}

/* 학술지 종류에 대한 정보 */
$jn_type = $_GET['jt'];

if($jn_type == "jkan"){
    $on1 = 'on';
    $left = '학술지';
    $title = 'JOI(인포랑학회지)';
    $journal_title = json_decode(if_get_option('site_name'));
} else if($jn_type == "anr") {
    $on2 = 'on';
    $left = '학술지';
    $title = 'ANR (Asian Nursing Research)';
    $journal_title = "Asian Nursing Research";
}

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$s_jn_year = !isset($_GET['s_jn_year']) ? '' : trim($_GET['s_jn_year']);
$s_jn_vol = !isset($_GET['s_jn_vol']) ? '' : trim($_GET['s_jn_vol']);
$s_jn_no = !isset($_GET['s_jn_no']) ? '' : trim($_GET['s_jn_no']);
$jn_year_vol = !isset($_GET['jn_year_vol']) ? '' : trim($_GET['jn_year_vol']);

if($jn_year_vol) {
    $in_data = explode("||",$jn_year_vol);
    $s_jn_year = $in_data[0];
    $s_jn_vol = $in_data[1];
}

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
    $sql .= "AND (author like ? OR j_title like ? OR j_keyword like ? OR j_abstract like ?)";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%', '%' . $qs1 . '%', '%' . $qs1 . '%');
}
if (!empty($s_jn_year) && !empty($s_jn_vol)) {
    $sql .= "AND jn_year = ? AND jn_vol = ? ";
    array_push($sparam, $s_jn_year, $s_jn_vol);
}
if (!empty($s_jn_no)) {
    $sql .= "AND jn_no = ? ";
    array_push($sparam, $s_jn_no);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_journal'] . "
		WHERE
			jn_type = '$jn_type'
			$sql
		ORDER BY
			seq_id DESC
";
			
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

$journal_data_year = if_get_journal_list_by_year($jn_type);
$journal_data_no = array();
if($s_jn_year && $s_jn_vol) {
    $journal_data_no = if_get_journal_list_by_year_vol($jn_type, $s_jn_year, $s_jn_vol);
} 
require_once INC_PATH . '/front-header.php';

?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
					<li><?php echo $title?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="btn-wrap text-right sub-link-wrap">
			<?php if($jn_type == "jkan") {?>
				<a href="https://jkan.or.kr/index.php?body=instructions" class="sub-link" target="_blank">투고지침</a>
				<a href="https://www.jkan.or.kr/" class="sub-link" target="_blank">홈페이지</a>
			<?php } else if($jn_type == "anr") {?>
				<a href="https://www.asian-nursingresearch.com/" class="sub-link" target="_blank">홈페이지</a>
			<?php }?>
			</div>
			<div class="subcontents">
				<div class="tabmenu-wrap">
					<?php if($jn_type == "jkan") {?>
					<ul class="sub-tabmenu type2 n3">
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan_comm.php">편집위원회</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=jkan">검색</a></li>
					</ul>					
					<?php } else if($jn_type == "anr") {?>
					<ul class="sub-tabmenu type2 n4">
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr.php">소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_comm.php">편집위원회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/journal/anr_rule.php">투고규정</a></li>
						<li class="on"><a href="<?php echo CONTENT_URL ?>/journal/journal_list.php?jt=anr">검색</a></li>
					</ul>					
					<?php }?>

				</div>
				<div class="sch-wrap">
					<form class="form-inline" id="search-form">
					<input type="hidden" name="jt" id="jt" value="<?php echo $jn_type ?>">
						<fieldset>
							<div class="sch-box">
								<div class="form-group">
									<label for="key">검색어</label>
									<div class="sch-input">
										<input type="text" id="qs1" name="qs1" class="form-control" placeholder="최대 50자 검색어 입력" value="<?php echo $qs1;?>">
										<button type="submit" class="btn btn-primary bg-blue2">검색</button>
									</div>
								</div>
							</div>
							<div class="form-inline journal-sch">
								<p>년도별 ‘권’과 ‘호’를 선택해 주세요  (SSN 0000-0000(00권 0호까지), ISSN 0000-0000(00권 0호부터)</p>
								<div class="form-group">
									<select name="jn_year_vol" id="jn_year_vol" class="form-control">
										<option value="">년도별 권</option>
										<?php for($jn=0;$jn<count($journal_data_year); $jn++) {
										    ?>
											<option value="<?php echo $journal_data_year[$jn]['jn_year']."||".$journal_data_year[$jn]['jn_vol']?>" <?php echo ($s_jn_year == $journal_data_year[$jn]['jn_year'] && $s_jn_vol == $journal_data_year[$jn]['jn_vol'])?" selected":""?>><?php echo $journal_data_year[$jn]['journal_list']?></option>
										<?php
										  }?>
									</select>
									<select name="s_jn_no" id="s_jn_no" class="form-control">
										<option value="">년도별 호</option>
										<?php for($jn2=0;$jn2<count($journal_data_no);$jn2++) {?>
											<option value="<?php echo $journal_data_no[$jn2]['jn_no']?>" <?php echo ($s_jn_no == $journal_data_no[$jn2]['jn_no'])?" selected":""?>><?php echo $journal_data_no[$jn2]['jn_no']?></option>
										<?php }?>
									</select>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="jkan-sch-list">	
				<?php
				if (!empty($item_results)) {
					$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
					foreach ($item_results as $key => $val) {
						$journal_id = $val['seq_id'];
						$jn_vol = $val['jn_vol'];
						$jn_no = $val['jn_no'];
						$jn_year = $val['jn_year'];
						$jn_month = $val['jn_month'];
						$author = $val['author'];
						$j_title = $val['j_title'];
						$j_abstract = $val['j_abstract'];
						$j_keyword = $val['j_keyword'];
						
						$meta_data = $val['meta_data'];
						$jdec = json_decode($meta_data, true);
						$page_from = $jdec['page_from'];
						$page_to = $jdec['page_to'];
						$file_attachment = $jdec['file_attachment'];
						
					?>
					<div class="jkan-sch-box">
						<span class="tit"><a href="<?php echo CONTENT_URL ?>/journal/journal_view.php?jt=<?php echo $jn_type;?>&journal_id=<?php echo $journal_id;?>"><?php echo $journal_title; ?>지 <?php echo $jn_year ?>년 <?php echo $jn_vol ?>권 <?php echo $jn_no ?>호</a></span>
						<span class="tit2"><a href="<?php echo CONTENT_URL ?>/journal/journal_view.php?jt=<?php echo $jn_type;?>&journal_id=<?php echo $journal_id;?>"><?php echo if_cut_str($j_title, 60) ?></a></span>
						<ul class="jkan-sch-info">
                    		<li>작성자 : <?php echo $author ?></li>
                    		<li>게시글 : <?php echo $jn_year ?>년 <?php echo $jn_month ?>월</li>
                    		<li>Page : <?php echo $page_from ?>-<?php echo $page_to ?></li>
                    	</ul>
                    	<div class="jkan-sch-con">
                    		<a href="<?php echo CONTENT_URL ?>/journal/journal_view.php?jt=<?php echo $jn_type;?>&journal_id=<?php echo $journal_id;?>"><?php echo if_cut_str($j_abstract, 370);?></a> 									
                    	</div> 
                    	<?php if($j_keyword) {?>
                    	<span class="jkan-key">키워드 : <?php echo $j_keyword ?></span>
                    	<?php }?>     
                     	<div class="btn-wrap text-right">
                    		<a href="<?php echo CONTENT_URL ?>/journal/journal_view.php?jt=<?php echo $jn_type;?>&journal_id=<?php echo $journal_id;?>">전문보기 <i class="xi-angle-down"></i></a>
            				<?php
    						if (!empty($file_attachment)) {
    							foreach ($file_attachment as $key => $val) {
    								$file_path = $val['file_path'];
    								$file_url = $val['file_url'];
    								$file_name = $val['file_name'];
    								if (is_file($file_path)) {
    						?>
									<a href="<?php echo INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" title="<?php echo $file_name ?>">
										PDF <i class="xi-angle-down"></i>
									</a>    								
    						<?php
    								}
    							}
    						}
    						?>                    		
                    		
                    	</div>                   	              		
					</div>
				<?php
						$list_no--;
					}
				} else {
				?>
				<div class="jkan-sch-box">
					데이터가 없습니다.
				</div>
				<?php 
				}
				?>		
			</div>
			<div class="box-footer text-center">
    		<?php
    		echo $paginator->if_paginator();
    		?>
			</div>				
			</div>
		</article>
	</div>
</section>
<script>
    $("#jn_year_vol").change(function() {
    	var data = $.trim($("#jn_year_vol").val());
    	var jt = $.trim($("#jt").val());
    	var data_array = data.split("||");
    	var jn_year = data_array[0];
    	var jn_vol = data_array[1];
    	
    	if (!data || !jt) {
    		location.href = "journal_list.php?jt="+jt;
    		return;
    	}
        	$.ajax({
    			type : "POST",
    			url : "./ajax/journal_year_vol.php",
    			data : {
    				"data" : data
    				,"jt" : jt
    			},
    			dataType : "json",
    			success : function(res) {
    				if (res.code != "0") {
    					alert(res.msg);
    				} else {
    					$("select#jn_no option").remove();
    					$("select#jn_no").append("<option value=''>년도별 호</option>");
    					$("select#jn_no").append(res.result_msg);
    					location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol;
    				}
    			}
    		});   
    });

    $("#s_jn_no").change(function() {
    	var data = $.trim($("#jn_year_vol").val());
    	var s_jn_no = $.trim($("#s_jn_no").val());
    	var jt = $.trim($("#jt").val());
    	var data_array = data.split("||");
    	var jn_year = data_array[0];
    	var jn_vol = data_array[1];
    	
    	if (!data || !jt || !s_jn_no) {
    		location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol;
    		return;
    	}
        	$.ajax({
    			type : "POST",
    			url : "./ajax/journal_no.php",
    			data : {
    				"data" : data
    				,"jn_no" : s_jn_no
    				,"jt" : jt
    			},
    			dataType : "json",
    			success : function(res) {
    				if (res.code != "0") {
    					alert(res.msg);
    				} else {
    					location.href = "journal_list.php?jt="+jt+"&s_jn_year="+jn_year+"&s_jn_vol="+jn_vol+"&s_jn_no="+s_jn_no;
    				}
    			}
    		});   
    });   
</script>
<?php 
require_once INC_PATH . '/front-footer.php';
?>
