<?php
require_once '../../if-config.php';
$on1 = 'on';
$left = '사이트맵';
$title = '사이트맵';
require_once INC_PATH . '/front-header.php';
?>
	</head>
	<body>
<?php 
require_once INC_PATH . '/front-gnb.php';
?>
<section id="subcontainer">
	<div class="layer1120">
		<aside class="left">
			<?php include_once 'left.php'; ?>
		</aside>
		<article class="subcon">
			<div class="path">
				<ul>
					<li><img src="/include/img/sub/ic-home.png" alt="홈"></li>
					<li><?php echo $left?></li>
				</ul>
			</div>
			<h4><?php echo $title?></h4>
			<div class="subcontents">
				<div class="sitemap-wrap">
					<ul class="sitemap-list">
						<li>
							<span class="tit">학회소개</span>
							<ul>
        						<li><a href="<?php echo CONTENT_URL ?>/about/intro.php">학회소개</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/greeting.php">회장인사말</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/history.php">연혁</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/organ.php">조직도</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/president.php">역대 회장</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/membership.php">회원학회</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/term.php">정관</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/about/contact.php">오시는 길</a></li>
							</ul>
						</li>
						<li>
							<span class="tit">학술∙교육∙행사</span>
							<ul>
        						<li><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술·교육·행사 안내</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_regi.php">사전등록 </a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/conference/extract.php">초록접수</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/conference/extract_confirm.php">등록 / 접수 확인</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_history.php">역대 학술·교육·행사</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">자료실</a></li>
							</ul>
						</li>
						<li>
							<span class="tit">학술지</span>
							<ul>
        						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">JOI</a></li>
							</ul>
						</li>
						<li>
							<span class="tit">출판</span>
							<ul>
        						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">국가시험 대비 문제집</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">문제집2</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/publication/exam.php">학습역량평가 모의고사</a></li>
							</ul>
						</li>
						<li>
							<span class="tit">회원공간</span>
							<ul>
        						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">공지사항</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=2">Q&A</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/member/subscribe.php">학회지 구독신청</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=3">채용공고</a></li>
        						<li><a href="<?php echo CONTENT_URL ?>/member/sponsor.php">후원금 신청</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</article>
	</div>
</section>
<?php 
require_once INC_PATH . '/front-footer.php';
?>
