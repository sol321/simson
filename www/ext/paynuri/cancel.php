<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

$order_code = $_GET['order_code'];
if_delete_item_order_by_code($order_code);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>결제취소</title>
<script>
function close() {
	window.parent.location.href = '<?php echo CONTENT_URL ?>/mypage/payment_dues.php';
	parent.dialog.dialog( "close" );
}
</script>
</head>
<body onload="close();">
</body>
</html>