<?php
/*
 * Desc : 페이누리로부터 결제결과 리턴, 해당 페이지 호출은 http request 방식은 아닌 듯 함.
 *
 */
header("Content-Type:text/html;charset=UTF-8");
header("Pragma: no-cache");
header("Cache-Control: no-cache,must-revalidate");

require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

$STOREID        = $_POST['STOREID'];
$STORE_NAME     = $_POST['STORE_NAME'];
$STORE_URL      = $_POST['STORE_URL'];
$BUSINESS_NO    = $_POST['BUSINESS_NO'];
$TRAN_NO        = $_POST['TRAN_NO'];
$GOODS_NAME     = $_POST['GOODS_NAME'];
$AMT            = $_POST['AMT'];
$QUANTITY       = $_POST['QUANTITY'];
$SALE_DATE      = $_POST['SALE_DATE'];
$CUSTOMER_NAME  = $_POST['CUSTOMER_NAME'];
$CUSTOMER_EMAIL = $_POST['CUSTOMER_EMAIL'];
$CUSTOMER_TEL   = $_POST['CUSTOMER_TEL'];
$CUSTOMER_IP    = $_POST['CUSTOMER_IP'];
$NOTICE_URL     = $_POST['NOTICE_URL'];
$TRAN_TYPE      = $_POST['TRAN_TYPE'];

$REP_CODE       = $_POST['REP_CODE'];
$REP_MSG        = $_POST['REP_MSG'];
$REP_AUTH_NO    = $_POST['REP_AUTH_NO'];
$REP_BANK       = $_POST['REP_BANK'];
$TID            = $_POST['TID'];

$order_id       = $_POST['ETC_DATA1'];  // if_item_order의 마지막 seq_id 
$user_id        = $_POST['ETC_DATA2'];  // user_id
$ETC_DATA3      = $_POST['ETC_DATA3'];

$compact = compact('TRAN_NO', 'AMT', 'SALE_DATE', 'CUSTOMER_IP', 'REP_AUTH_NO', 'TID', 'order_id', 'user_id');
// $json = json_encode($compact);

if ($REP_CODE == "0000") {   
    $result = if_add_user_payment_by_code($TRAN_NO, $compact);
    
    if ($result > 0) {
        if_delete_item_order_by_code($TRAN_NO); // 카트에서 삭제
    }
    
    echo "SUCCESS";
} else {
    // 결제 실패시 상점 처리로직 구현
    echo 'Error';
}

// Logging
$logged_data .= "\n" . ' POST : ' . print_r($_POST, true);
$log_msg = $logged_data . "\n";
$log_msg .= "------------------------------------------------------------------------\n";
$log_dir = UPLOAD_PATH . '/pg/logs';
$log_file = 'paynuri_' . date('Ymd') . '.log';

if_record_log($log_msg, $log_dir . '/' . $log_file);
// Logging

?>
