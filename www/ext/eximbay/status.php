<?php
/*
 * Desc : DB 처리용
 */
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once FUNC_PATH . '/functions-user.php';

$rescode = $_POST['rescode'];
$order_id = $_POST['param1'];   // if_item_order의 마지막 seq_id 
$user_id = $_POST['param2'];    // user_id

$ref = $_POST['ref'];   // order code
$transid = $_POST['transid'];
$authcode = $_POST['authcode'];
$amt = $_POST['amt'];

$compact = compact('ref', 'transid', 'authcode', 'amt', 'order_id', 'user_id');
// $json = json_encode($compact);

if ($rescode == '0000') {   // 성공
    $result = if_add_user_payment_by_code($ref, $compact);
    
    if ($result > 0) {
        if_delete_item_order_by_code($ref); // 카트에서 삭제
    }
} else {    // 실패
    // 
}

// Logging
$logged_data = '';
$logged_data .= "\n" . ' POST : ' . print_r($_POST, true);
$log_msg = $logged_data . "\n";
$log_msg .= "------------------------------------------------------------------------\n";
$log_dir = UPLOAD_PATH . '/pg/logs';
$log_file = 'eximbay_' . date('Ymd') . '.log';

if_record_log($log_msg, $log_dir . '/' . $log_file);
// Logging

?>