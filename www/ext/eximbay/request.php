<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$option_name = 'if_api_pg_eximbay';
$json = if_get_option($option_name);
$jdec = json_decode($json, true);

$secretKey = $jdec['secret_key'];
$reqURL = $jdec['request_url'];

$fgkey = '';            //fgkey검증키 관련
$sortingParams = '';    //파라미터 정렬 관련

foreach($_POST as $Key => $value) {
    $hashMap[$Key]  = $value;
}
$size = count($hashMap);
ksort($hashMap);
$counter = 0;
foreach ($hashMap as $key => $val) {
    if ($counter == $size-1){
        $sortingParams .= $key .'=' . $val;
    }else{
        $sortingParams .= $key . '=' . $val . '&';
    }
    ++$counter;
}
$linkBuf = $secretKey . '?'. $sortingParams;
$fgkey = hash('sha256', $linkBuf);

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body leftmargin="0" topmargin="0" align="center" onload="javascript:document.regForm.submit();">
<form name="regForm" method="post" action="<?php echo $reqURL; ?>">
<input type="hidden" name="fgkey" value="<?php echo $fgkey; ?>" />	<!--필수 값-->

<?php
foreach($_POST as $Key=>$value) {
?>
<input type="hidden" name="<?php echo $Key;?>" value="<?php echo $value;?>">
<?php } ?>
</form>
</body>
</html>