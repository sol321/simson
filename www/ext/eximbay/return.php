<?php
/*
 * Desc: Eximbay로부터 전달받는 응답 파라미터(rescode)에 따라 처리.
 *      실제 DB처리는 status.php 에서 처리함.
 */
require_once '../../if-config.php';

$rescode = $_POST['rescode'];

$suc_url = CONTENT_URL . '/mypage/payment_dues_list.php';

if ($rescode == '0000') {   // 결제 성공
    echo <<<EOD
        <script>
        alert("Payment was successful.");
        window.opener.location = "$suc_url";
        self.close();
        </script>
EOD;
} else {    // 결제실패
    echo <<<EOD
        <script>
        alert("Your payment was not successful.");
        self.close();
        </script>
EOD;
}

?>