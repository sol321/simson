<?php 
require_once './if-config.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-service-post.php';
require_once INC_PATH . '/front-header.php';
if_get_live_popup_main();
$banner_result = if_get_live_banner_main();
?>
	</head>

	<body> 	
<?php 
require_once INC_PATH . '/front-gnb.php';
?>	
	
<section id="container">
	<article class="conbox layer1120">
		<div class="visual">
			<a href="#n"><img src="<?php echo INC_URL ?>/img/190918.jpg" alt="INFORANG Since 2013~ 고객과 함께하는 성장 (주)인포랑의 끊임없는 도전 역사입니다."></a>
			<a href="#n"><img src="<?php echo INC_URL ?>/img/190918.jpg" alt="INFORANG Since 2013~ 고객과 함께하는 성장 (주)인포랑의 끊임없는 도전 역사입니다."></a>
			<a href="#n"><img src="<?php echo INC_URL ?>/img/190918.jpg" alt="INFORANG Since 2013~ 고객과 함께하는 성장 (주)인포랑의 끊임없는 도전 역사입니다."></a>
<!--
			<a href="#n"><img src="<?php echo INC_URL ?>/img/visual.png" alt="인포랑학회"></a>
			<a href="#n"><img src="<?php echo INC_URL ?>/img/visual.png" alt="인포랑학회"></a>
			<a href="#n"><img src="<?php echo INC_URL ?>/img/visual.png" alt="인포랑학회"></a>
-->
		</div>
		<div class="quick-link">
				<a href="<?php echo CONTENT_URL ?>/about/intro.php" class="quick-link-box">
					<span class="tit">학회소개</span>
					<span class="txt">
						1980년에 창설된 <br class="no-br"/>
						인포랑학회를 소개합니다.
					</span>
				</a>
				<a href="<?php echo CONTENT_URL ?>/conference/conference.php" class="quick-link-box">
					<span class="tit">학술행사</span>
					<span class="txt">
						인포랑학회 <br class="no-br"/>
						각종 학술대회를 소개합니다.
					</span>
				</a>
				<a href="<?php echo CONTENT_URL ?>/journal/jkan.php" class="quick-link-box">
					<span class="tit">JOI</span>
					<span class="txt">
						Journal of <br class="no-br"/>
						Inforang
					</span>
				</a>
				<a href="<?php echo CONTENT_URL ?>/journal/jkan.php" class="quick-link-box">
					<span class="tit">AJOI</span>
					<span class="txt">
						Asian Journal of<br class="no-br"/>
						Inforang
					</span>
				</a>
		</div>
	</article>
	<article class="conbox layer1120">
		<div class="icon-link">
			<a href="<?php echo CONTENT_URL ?>/member/signup_info.php" class="icon-link-box">
				<span class="tit">학회<br class="no-br">등록</span>
				<span class="icon-wrap"></span>
				<span class="btn-link"></span>
			</a>
			<a href="<?php echo CONTENT_URL ?>/publication/book.php" class="icon-link-box">
				<span class="tit">출간물<br class="no-br"></span>
				<span class="icon-wrap"></span>
				<span class="btn-link"></span>
			</a>
			<a href="<?php echo CONTENT_URL ?>/about/membership.php" class="icon-link-box">
				<span class="tit">회원<br class="no-br">학회</span>
				<span class="icon-wrap"></span>
				<span class="btn-link"></span>
			</a>
			<a href="<?php echo CONTENT_URL ?>/member/sponsor.php" class="icon-link-box">
				<span class="tit">축하 후원금<br class="no-br"></span>
				<span class="icon-wrap"></span>
				<span class="btn-link"></span>
			</a>
		</div>
		<div class="board-box">
			<ul class="tabmenu">
				<li class="on"><a href="#n">인포랑학회</a></li>
				<li><a href="#n">관련단체</a></li>
			</ul>
			<div class="tabcon">
				<span class="board-tit">※ 인포랑학회 공지사항</span>
				<div class="board-list">
					<ul>
						<?php
						      $posts = if_get_main_post(1);
						      if(!empty($posts) && is_array($posts)){
						          foreach($posts as $post_key => $post_val){
						              $post_id = $post_val['seq_id'];
						              $post_title = htmlspecialchars($post_val['post_title']);
						              $post_date = date("y.m.d",strtotime($post_val['post_date']));
						          ?>
						          	<li><a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=1&post_id=<?php echo $post_id;?>"><span class="subject"><?php echo $post_title;?></span><span class="date"><?php echo $post_date;?></span></a></li>
						          <?php 
						          }
						      } else {
						          echo "<li>등록된 게시글이 없습니다.</li>";
						      }
						?>
					</ul>
				</div>
				<a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1" class="btn-more">더보기 <i class="xi-plus"></i></a>
			</div>
			<div class="tabcon">
				<span class="board-tit">※ 관련단체 공지사항</span>
				<div class="board-list">
					<ul>
						<?php
						      $posts2 = if_get_main_post(11);
						      if(!empty($posts2) && is_array($posts2)){
						          foreach($posts2 as $post_key2 => $post_val2){
						              $post_id = $post_val2['seq_id'];
						              $post_title = htmlspecialchars($post_val2['post_title']);
						              $post_date = date("y.m.d",strtotime($post_val2['post_date']));
						          ?>
						          	<li><a href="<?php echo CONTENT_URL ?>/community/post_view.php?bt=11&post_id=<?php echo $post_id;?>"><span class="subject"><?php echo $post_title;?></span><span class="date"><?php echo $post_date;?></span></a></li>
						          <?php 
						          }
						      } else {
						          echo "<li>등록된 게시글이 없습니다.</li>";
						      }
						?>
					</ul>
				</div>
				<a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=11" class="btn-more">더보기 <i class="xi-plus"></i></a>
			</div>
		</div>
	</article>
	<?php if(!empty($banner_result) && is_array($banner_result)) {?>
	<article class="conbox layer1120 banner-con">
		<div class="banner-wrap">
			<div class="banner-rolling">
				<?php foreach($banner_result as $banner_key => $banner_val){
				    $jdec = json_decode($banner_val['meta_data'],true);
				    $banner_url = $jdec['banner_url'];
				    $file_attachment = $jdec['file_attachment'];
				    $file_url = $file_attachment[0]['file_url'];
				    $img_url = empty($file_url) ? '' : '<img src="' . $file_url . '"  alt = "'.$banner_val['post_title'].'">';
				 ?>
				<a href="<?php echo $banner_url;?>" target="_blank"><?php echo $img_url;?></a>
				<?php }?>
			</div>
		</div>
	</article>
	<?php }?>
</section>

<?php 
require_once INC_PATH . '/front-footer.php';
?>
