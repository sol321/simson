<?php
define('DB_HOST', '127.0.0.1');
define('DB_USER', 'simson_user');
define('DB_PASS', '12345678');
define('DB_NAME', 'simson_db');
define('DB_PORT', 3306);
define('DB_SOCK', '');
define('DB_CHARSET', 'utf8mb4');

$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
$fqdn = $_SERVER['HTTP_HOST'];
$site_url = $protocol . $fqdn;

define('FQDN', $fqdn);
define('SITE_URL', $site_url);
define('SETUP_DIR', '');    // slash(/)로 시작합니다.
define('ADMIN_DIR', 'admin');
define('HOME_URL', SITE_URL . SETUP_DIR);

/*
 * URL
 */
define('ADMIN_URL',         HOME_URL . '/' . ADMIN_DIR);
define('ADMIN_INC_URL',     ADMIN_URL . '/include');
define('CONTENT_URL',       HOME_URL . '/content');
define('INC_URL',           HOME_URL . '/include');
define('EXT_URL',           HOME_URL . '/ext');
define('IMG_URL',           INC_URL . '/images');
define('UPLOAD_ABS_URL',    SETUP_DIR . '/upload');
define('UPLOAD_URL',        HOME_URL . UPLOAD_ABS_URL);

/*
 * Path
 */
define('ABSOLUTE_PATH', dirname(__FILE__));

define('ADMIN_PATH',    ABSOLUTE_PATH . '/' . ADMIN_DIR);
define('CONTENT_PATH',  ABSOLUTE_PATH . '/content');
define('INC_PATH',      ABSOLUTE_PATH . '/include');
define('UPLOAD_PATH',   ABSOLUTE_PATH . UPLOAD_ABS_URL);		// php_flag engine off
define('FUNC_PATH',     INC_PATH . '/functions');

require_once ABSOLUTE_PATH . '/if-env.php';
   
?>
