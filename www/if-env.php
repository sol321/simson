<?php
// Include necessary files
require_once INC_PATH . '/classes/Database.php';
require_once INC_PATH . '/classes/Session.php';
require_once FUNC_PATH . '/functions.php';
require_once FUNC_PATH . '/functions-auth.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-mail.php';



header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('Asia/Seoul');
// ini_set('display_errors', '1');

// define('SITE_NAME', '인포랑학회');
// define('SITE_NOTI_EMAIL', 'john@softsyw.com');   // 관리자 이메일
// define('SITE_FROM_EMAIL', 'office@kspsm.or.kr');	// 발신전용 이메일
// define('SITE_PHONE', '041-529-6190');
// define('BIZ_NO', '2178261260');    // 사업자등록번호

define('ALTE_SKIN', 'skin-blue');		// Admin LTE skin

// DB Table name mapping
$GLOBALS['if_tbl_admin']            = 'if_admin';
$GLOBALS['if_tbl_session']          = 'if_session';
$GLOBALS['if_tbl_options']          = 'if_options';
$GLOBALS['if_tbl_post_template']    = 'if_post_template';
$GLOBALS['if_tbl_posts']            = 'if_posts';
$GLOBALS['if_tbl_post_comments']    = 'if_post_comments';
$GLOBALS['if_tbl_terms']            = 'if_terms';
$GLOBALS['if_tbl_users']            = 'if_users';
$GLOBALS['if_tbl_usermeta']         = 'if_usermeta';
$GLOBALS['if_tbl_user_class']       = 'if_user_class';
$GLOBALS['if_tbl_user_login_history']       = 'if_user_login_history';
$GLOBALS['if_tbl_executives']       = 'if_executives';
$GLOBALS['if_tbl_sponsors']         = 'if_sponsors';
$GLOBALS['if_tbl_items']            = 'if_items';
$GLOBALS['if_tbl_item_product']     = 'if_item_product';
$GLOBALS['if_tbl_user_payment']     = 'if_user_payment';
$GLOBALS['if_tbl_item_order']       = 'if_item_order';
$GLOBALS['if_tbl_conference']       = 'if_conference';
$GLOBALS['if_tbl_conference_abstract']       = 'if_conference_abstract';
$GLOBALS['if_tbl_conference_register']       = 'if_conference_register';
$GLOBALS['if_tbl_mail_content']     = 'if_mail_content';
$GLOBALS['if_tbl_mail_log']         = 'if_mail_log';
$GLOBALS['if_tbl_journal']          = 'if_journal';
$GLOBALS['if_tbl_service_posts']    = 'if_service_posts';
$GLOBALS['if_tbl_address_group']    = 'if_address_group';
$GLOBALS['if_tbl_address_contact']  = 'if_address_contact';
$GLOBALS['if_tbl_univ_code']        = 'if_univ_code';
$GLOBALS['if_tbl_mock_exam']        = 'if_mock_exam';           // 모의고사
$GLOBALS['if_tbl_mock_exam_apply']  = 'if_mock_exam_apply';     // 모의고사 신청
$GLOBALS['if_tbl_mock_exam_order']  = 'if_mock_exam_order';     // 차시
$GLOBALS['if_tbl_mock_exam_taker']  = 'if_mock_exam_taker';     // 응시자
$GLOBALS['if_tbl_journal_item']     = 'if_journal_item';
$GLOBALS['if_tbl_journal_user']     = 'if_journal_user';
$GLOBALS['if_tbl_journal_subscription']     = 'if_journal_subscription';
$GLOBALS['if_sales_info']     = 'sales_info';

/*
$GLOBALS['if_tbl_post_template']    = 'if_post_template';
$GLOBALS['if_tbl_posts']            = 'if_posts';
$GLOBALS['if_tbl_item_order']       = 'if_item_order';
$GLOBALS['if_tbl_conference']       = 'if_conference';
$GLOBALS['if_tbl_conference_payment']     = 'if_conference_payment';
$GLOBALS['if_tbl_archive_medical']  = 'if_archive_medical';
*/
$ifdb = new Database();
$Session = new Session($ifdb);

// Code
// 게시판 Code
$if_board_template = array(
    'STD' => '일반 게시판',
    'GAL' => '갤러리 게시판',
    'ARC' => '자료실',
    'QNA' => 'Q&amp;A',
);

/*
 * 결제 관련
 */
// 회비 종류
$if_fee_type = array(
    'ANN' => '연회비 (매년 청구)',
    'ENT' => '입회비 (1회 청구)',
    'PER' => '평생회비 (입회비, 연회비 면제)',
    'ETC' => '기타',
);

// 결제 수단
$if_payment_method = array(
    'BANK_TR'  => '무통장입금',
    'CARD_LC'  => '국내 신용카드',
    'CARD_OC'  => '해외 신용카드',
);

// 결제 상태
$if_payment_state = array(
    '1000'  => '미납',
    '9000'  => '결제완료',
    '4000'  => '결제취소',
);

/*
 * 회원 코드 관련
 */
// 상태
$if_user_state = array(
    '10' => '미승인',    
    '30' => '승인',    
    '41' => '차단',    
    '91' => '탈퇴',    
);

// 자격
$if_user_qualification = array(
    '1001' => '간호사',
    '3001' => '전공의', // resident
    '6001' => '전임의', // fellow
    '4001' => '전문의', // specialist
    '1999' => '기타',
);

// 소속 분류
$if_org_category = array(
    '50' => '종합병원',
    '40' => '병원',
    '30' => '개인병원',
    '20' => '기타',
);

$if_agree_yn = array(
    'Y' => '동의',
    'N' => '미동의',
);

// 주문관련
// 상태
// $if_order_state = array(
//     '1000' => '무통장입금 대기중',
//     '3000' => '신용카드 처리중',
// );

/*
 * 학술 교육 행사 관련
 */
// 행사 분류
$if_event_type = array(
    'ACD' => '학술',
    'TRN' => '교육',
    'EVT' => '행사',
);

$if_progress_state = array(
    'P' => '진행중',
    'N' => 'No',
);

// 회원 여부
$if_is_member = array(
    '30' => '회원',
    '10' => '비회원',
);

// 대학원생 여부
$if_is_graduate = array(
    '55' => '일반',
    '66' => '대학원 재학중(석사과정, 박사과정)',
);

// 발표 형태
$if_pt_type = array(
    'ORAL' => '구두',
    'POSTER' => '포스터',
);

// 초록 채택 코드
$if_abstract_selection = array(
    '1000' => '대기중',
    '3000' => '채택',
    '4000' => '미채택',
);

// 행사 사전 or 현장 등록, registration_type
$if_event_reg_type = array(
    '1000' => '사전등록',
    '2000' => '현장등록',
);

// 후원관리
$if_sponsorship_type = array(
    'period' => '정기 후원',
    'temp'  => '일시 후원',
);

// 후원방식 안내문구
$if_sponsorship_type_txt = array(
    'period' => '후원자님께서 지정하신 금액이 자동적으로 매월 출금되는 방식으로 계좌이체가 가능합니다.',
    'temp'  => '후원자님께서 원하시는 금액을 일시 후원하는 방식으로 계좌이체가 가능합니다.',
);

$if_sponsorship_usage = array(
    'fund' => '학회발전기금: 50주년 기념행사',
    'etc'  => '기타',
);

/*
 * 모의고사
 */
$if_exam_type = array(
    'R' => '정규',
    'A' => '추가',
);

// 시험 상태 (모의고사 신청-발송 상태)
$if_exam_state = array(
    '1001'  => '시험접수',
    '2000'  => '문제지 발송',
    '3100'  => 'OMR 회수',
    '3200'  => 'OMR 판독',
    '4100'  => '성적 분석',
    '4900'  => '성적표 발송',
);

require_once INC_PATH . '/classes/Database.php';
require_once INC_PATH . '/classes/Session.php';
require_once FUNC_PATH . '/functions.php';
require_once FUNC_PATH . '/functions-option.php';

// $session = new Session($db);

?>