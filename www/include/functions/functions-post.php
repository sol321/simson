<?php
function if_add_post() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $tpl_id = empty($_POST['tpl_id']) ? NULL : $_POST['tpl_id'];
    $post_name = empty($_POST['post_name']) ? if_get_current_user_display_name() : $_POST['post_name'];
    $post_content = empty($_POST['post_content']) ? '' : $_POST['post_content'];
    $post_date = empty($_POST['post_date']) ? date('Y-m-d H:i:s') : $_POST['post_date'];
    $post_title = empty($_POST['post_title']) ? '' : $_POST['post_title'];
    $post_parent = empty($_POST['post_parent']) ? 0 : $_POST['post_parent'];
    $post_state = empty($_POST['post_state']) ? 'open' : $_POST['post_state'];
    $post_passwd = empty($_POST['post_passwd']) ? '' : $_POST['post_passwd'];
    $post_user_id = empty($_POST['post_user_id']) ? 0 : $_POST['post_user_id'];
    $post_email = empty($_POST['post_email']) ? '' : $_POST['post_email'];
    $post_order = empty($_POST['post_order']) ? 0: $_POST['post_order'];
    $post_type = empty($_POST['post_type']) ? 'post_new' : $_POST['post_type'];
    $post_type_secondary = empty($_POST['post_type_secondary']) ? '' : $_POST['post_type_secondary'];
    $post_mime_type = empty($_POST['post_mime_type']) ? 'html' : $_POST['post_mime_type'];
    $post_ip_addr = $_SERVER['REMOTE_ADDR'];
    
    $ckeditor_images = '';
    $file_attachment = [];
    $thumb_attachment = [];
    
    // CKEditor 에서 사용한 이미지
    $ck_images = [];
    preg_match_all('/(src)=("[^"]*")/i', $post_content, $ck_images, PREG_SET_ORDER);
    
    
    if (!empty($ck_images)) {
        $images_tmp = [];		// Temporary directory path
        $images_real = [];		// Real directory path
        
        foreach ($ck_images as $key => $val) {
            if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false) {
                $images_tmp[] = $val[2];
            }
        }
        
        if (!empty($images_tmp)) {
            $ym = date('Ym');
            $tmp_mark   = '/tmp';
            $ck_mark    = '/ckeditor/' . $ym;
            
            $new_dir_path = UPLOAD_PATH . $ck_mark;    // 디렉토리 생성을 위해
            
            if (!is_dir($new_dir_path)) {
                mkdir($new_dir_path, 0777, true);
            }
            foreach ($images_tmp as $key => $val) {
                $img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
                $img_new_url = str_replace($tmp_mark, $ck_mark, $img_url);  // format: /upload/ckeditor/201903/1553048848_71241100_1082390327.jpg
                // 파일 이동을 위해 실제 물리 경로를 만든다.
                $real_tmp_path = ABSOLUTE_PATH . $img_url;
                $real_new_path = ABSOLUTE_PATH . $img_new_url;
                if (@rename($real_tmp_path, $real_new_path)) {
                    $post_content = str_replace($img_url, $img_new_url, $post_content);
                    $images_real[] = $img_new_url;
                }
            }
        }
        if (!empty($images_real)) {
            $ckeditor_images = $images_real;
        }
    }
    //-- CKEditor

    // 대표이미지
    if (!empty($_POST['thumb_path'])) {
        $year = date('Y');
        $month = date('m');
        $upload_dir = UPLOAD_PATH . '/posts_thumb/' . $year . '/' . $month;
        $upload_url = UPLOAD_URL . '/posts_thumb/' . $year . '/' . $month;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['thumb_path'] as $key => $val) {
            $basename = basename($val);
            $atc_val['file_path'] = $upload_dir . '/' . $basename;
            $atc_val['file_url'] = $upload_url . '/' . $basename;
            $atc_val['file_name'] = $_POST['thumb_name'][$key];
            $result = rename($val, $atc_val['file_path']);
            if (!empty($result)) {
                array_push($thumb_attachment, $atc_val);
            }
        }
        if (!empty($thumb_attachment)) {
	        $json = json_encode(compact('thumb_attachment'));
	    }
    }
    
    // 첨부파일
    if (!empty($_POST['file_path'])) {
        $year = date('Y');
        $month = date('m');
        $upload_dir = UPLOAD_PATH . '/posts/' . $year . '/' . $month;
        $upload_url = UPLOAD_URL . '/posts/' . $year . '/' . $month;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) {
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['file_url'][$key];
                $atc_val['file_name'] = $_POST['file_name'][$key];
                array_push($file_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['file_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($file_attachment, $atc_val);
                }
            }
        }
	    if (!empty($file_attachment)) {
	        $json = json_encode(compact('file_attachment'));
	    }
    }

    $compact = compact('post_ip_addr', 'post_mime_type', 'ckeditor_images', 'thumb_attachment', 'file_attachment');
    $json = json_encode($compact);
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					post_name,
					post_content,
					post_title,
					post_parent,
					post_state,
					post_passwd,
					post_user_id,
					post_email,
					post_order,
					post_type,
					post_type_secondary,
					meta_data,
					template_id
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, ?,
					?, ?, ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssissisissss',
                	    $post_name,
                	    $post_content,
                	    $post_title,
                	    $post_parent,
                	    $post_state,
                	    $post_passwd,
                	    $post_user_id,
                	    $post_email,
                	    $post_order,
                	    $post_type,
                	    $post_type_secondary,
                	    $json,
	                    $tpl_id
   );
    $stmt->execute();
	$new_post_id = $ifdb->insert_id;
	
	// 메일 발송
	/*
    $to_email = if_get_option('pps-bbsmanager-to-email');
    $email_subject = '새로운 게시글이 등록되었습니다.';
    $mail_content = nl2br($post_content);
    
    $email_body =<<<EOD
        <div style="padding: 20px; border: 1px solid #dcdcdc; font-size: 12px;">
            <p>이름 : $post_name </p>
            <p>제목 : $post_title</p>
            <p>내용 : $mail_content</p>
            <p></p>
        </div>
EOD;
    
    $result = if_send_mail($to_email, $email_subject, $email_body);
	 */
	
	
	return $new_post_id;
}


function if_get_post($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ? AND
                post_state = 'open'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $post_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

function if_update_post() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $post_id = $_POST['post_id'];
    $post_name = empty($_POST['post_name']) ? if_get_current_user_display_name() : $_POST['post_name'];
    $post_content = empty($_POST['post_content']) ? '' : $_POST['post_content'];
    $post_title = empty($_POST['post_title']) ? '' : $_POST['post_title'];
    $post_order = empty($_POST['post_order']) ? 0: $_POST['post_order'];
    $post_ip_addr = $_SERVER['REMOTE_ADDR'];
    $post_type_secondary = empty($_POST['post_type_secondary']) ? '' : $_POST['post_type_secondary'];
    
    $ckeditor_images = '';
    $file_attachment = [];
    $thumb_attachment = [];
    
    // CKEditor 에서 사용한 이미지
    $ck_images = [];
    preg_match_all('/(src)=("[^"]*")/i', $post_content, $ck_images, PREG_SET_ORDER);
    
    if (!empty($ck_images)) {
        $images_tmp = [];		// Temporary directory path
        $images_real = [];		// Real directory path

        foreach ($ck_images as $key => $val) {
            if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false || stripos($val[2], UPLOAD_ABS_URL . '/ckeditor') !== false) {
                $images_tmp[] = $val[2];
            }
        }
        
        if (!empty($images_tmp)) {
            $ym = date('Ym');
            $tmp_mark   = '/tmp';
            $ck_mark    = '/ckeditor/' . $ym;
            
            $new_dir_path = UPLOAD_PATH . $ck_mark;    // 디렉토리 생성을 위해
            
            if (!is_dir($new_dir_path)) {
                mkdir($new_dir_path, 0777, true);
            }
            foreach ($images_tmp as $key => $val) {
                $img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
                $img_new_url = str_replace($tmp_mark, $ck_mark, $img_url, $ch_count);  // format: /upload/ckeditor/201903/1553048848_71241100_1082390327.jpg
                // 파일 이동을 위해 실제 물리 경로를 만든다.
                $real_tmp_path = ABSOLUTE_PATH . $img_url;
                $real_new_path = ABSOLUTE_PATH . $img_new_url;
                if ($ch_count > 0) {    // tmp내의 파일만 이동
                    if (@rename($real_tmp_path, $real_new_path)) {
                        $post_content = str_replace($img_url, $img_new_url, $post_content);
                        $images_real[] = $img_new_url;
                    }
                } else {
                    $images_real[] = $img_url;
                }
            }
        }
        if (!empty($images_real)) {
            $ckeditor_images = $images_real;
        }
    }
    //-- CKEditor
    
    // 대표이미지
    if (!empty($_POST['thumb_path'])) {
        $year = date('Y');
        $month = date('m');
        $upload_dir = UPLOAD_PATH . '/posts_thumb/' . $year . '/' . $month;
        $upload_url = UPLOAD_URL . '/posts_thumb/' . $year . '/' . $month;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['thumb_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) { 
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['thumb_url'][$key];
                $atc_val['file_name'] = $_POST['thumb_name'][$key];
                array_push($thumb_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['thumb_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($thumb_attachment, $atc_val);
                }
            }
        }
        if (!empty($thumb_attachment)) {
            $json = json_encode(compact('thumb_attachment'));
        }
    }
    //--- 대표이미지
    
    // 첨부파일
    if (!empty($_POST['file_path'])) {
        $year = date('Y');
        $month = date('m');
        $upload_dir = UPLOAD_PATH . '/posts/' . $year . '/' . $month;
        $upload_url = UPLOAD_URL . '/posts/' . $year . '/' . $month;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) {
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['file_url'][$key];
                $atc_val['file_name'] = $_POST['file_name'][$key];
                array_push($file_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['file_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($file_attachment, $atc_val);
                }
            }
        }
        
        if (!empty($file_attachment)) {
            $json = json_encode(compact('file_attachment'));
        }
    }
    //--- 첨부파일
    
    $compact = compact('post_ip_addr', 'ckeditor_images', 'thumb_attachment', 'file_attachment');
    $json = json_encode($compact);
    
    $query = "
    		UPDATE
    			$if_table1
    		SET
    			post_name = ?,
    			post_content = ?,
    			post_title = ?,
    			post_order = ?,
    			post_type_secondary = ?,
    			meta_data = ?
    		WHERE
    			seq_id = ?
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssissi',
	    $post_name,
	    $post_content,
	    $post_title,
	    $post_order,
	    $post_type_secondary,
	    $json,
	    $post_id
  );
	$result = $stmt->execute();
	return $result;
}

function if_delete_post($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $post_id);
	$stmt->execute();
	$result = $ifdb->affected_rows;
	return $result;
}

function if_update_post_view_count($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                post_view_count = post_view_count + 1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $post_id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 답변 글 조회
 */
function if_get_answer_post($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				post_parent = ? AND
                post_type_secondary = 'answer' AND
                post_state = 'open'
	";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $post_id);
    $stmt->execute();
    return $ifdb->get_row($stmt);
}

/*
 * Desc : post의 post_state를 close로 변경
 */
function if_close_post($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                post_state = 'close'
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $post_id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc : 게시글 설정된 비밀번호 가져오기
 */
function if_get_passwd_post($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
            SELECT post_passwd
            FROM $if_table1
            WHERE seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i',$post_id);
    $stmt->execute();
    $post_passwd = $ifdb->get_var($stmt);
    return $post_passwd;
}


/*
 * Desc : 이전글  
 */
function if_get_pre_post($tpl_id, $post_id, $post_order) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    $query = "
		SELECT
			*
		FROM
			$if_table1
		WHERE
			post_state = 'open'
            AND template_id  = ?
            AND seq_id < ?
            AND post_order = ?
            AND post_type_secondary <> 'answer'
		ORDER BY
			post_order DESC,
			seq_id DESC
        limit 0, 1
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sii', $tpl_id,$post_id,$post_order);
	$stmt->execute();
	$results = $ifdb->get_row($stmt);
	if(empty($results) && $post_order != 0){
	    $post_order_re = ( $post_order == 0 ) ? 1 : 0 ;
	    $query = "
        	SELECT
        		*
        	FROM
        		$if_table1
        	WHERE
        		post_state = 'open'
                AND template_id  = ?
                AND post_order = ?
                AND post_type_secondary <> 'answer'
        	ORDER BY
        		post_order DESC,
        		seq_id DESC
            limit 0, 1
        ";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('si', $tpl_id,$post_order_re);
		$stmt->execute();
		$results = $ifdb->get_row($stmt);
	}
	return $results;
}

/*
 * Desc : 다음글
 */
function if_get_next_post($tpl_id, $post_id, $post_order) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
		SELECT
			*
		FROM
			$if_table1
		WHERE
			post_state = 'open'
            AND template_id  = ?
            AND seq_id > ?
            AND post_type_secondary <> 'answer'
            AND post_order = ?
		ORDER BY
			post_order ASC,
			seq_id ASC
        limit 0, 1
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sii', $tpl_id,$post_id,$post_order);
	$stmt->execute();
	$results = $ifdb->get_row($stmt);
	
	if(empty($results) && $post_order != 1){
	    $post_order_re = ( $post_order == 0 ) ? 1 : 0 ;
	    $query = "
        	SELECT
        		*
        	FROM
        		$if_table1
        	WHERE
        		post_state = 'open'
                AND template_id  = ?
                AND post_type_secondary <> 'answer'
                AND post_order = ?
        	ORDER BY
        		post_order ASC,
        		seq_id ASC
            limit 0, 1
        ";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('si', $tpl_id,$post_order_re);
		$stmt->execute();
		$results = $ifdb->get_row($stmt);
	}
	return $results;
}

/*내부형 학술대회용 갤러리 등록 건수 확인*/
function if_get_post_gallery_total($conference_id){
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    
    $query = "
			SELECT
				count(*) as total
			FROM
				$if_table1
			WHERE
				post_type = 'conf_gallery' AND
                post_state = 'open' AND
                post_type_secondary = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $conference_id);
	$stmt->execute();
	$total = $ifdb->get_var($stmt);
	return $total;
}

/*내부형 학술대회용 갤러리 게시판 내용 가져오기*/

function if_get_post_gallery($conference_id, $post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				post_type = 'conf_gallery' AND
                post_state = 'open' AND
                post_type_secondary = ? AND 
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ii', $conference_id,$post_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}


/*
 * Desc : 이전글(내부형 갤러리)
 */
function if_get_pre_gallery($conference_id, $post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    $query = "
		SELECT
			*
		FROM
			$if_table1
		WHERE
			post_type = 'conf_gallery' AND
            post_state = 'open' AND
            post_type_secondary = ?
            AND seq_id < ?
		ORDER BY
			post_order DESC,
			seq_id DESC
        limit 0, 1
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ii', $conference_id,$post_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

/*
 * Desc : 다음글(내부형 갤러리)
 */
function if_get_next_gallery($conference_id, $post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    $query = "
		SELECT
			*
		FROM
			$if_table1
		WHERE
			post_type = 'conf_gallery' AND
            post_state = 'open' AND
            post_type_secondary = ?
            AND seq_id > ?
		ORDER BY
			post_order ASC,
			seq_id ASC
        limit 0, 1
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ii', $conference_id,$post_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

/*
 * Desc : 메인 게시글 가져오기
 */
function if_get_main_post($tpl_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_posts'];
    $query = "
		SELECT
			*
		FROM
			$if_table1
		WHERE
			post_state = 'open'
            AND template_id  = ?
		ORDER BY
            post_order DESC,
			seq_id desc
        limit 0, 4
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $tpl_id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
	
}

function if_time_ago($time) {
    $now	= time();
    $diff   = $now - $time;
    
    if ($diff < 60) {
        return '방금 전';
    } else if ($diff < 3600) {
        return round($diff / 60) . '분 전';
    } else if ($diff < 3600 * 24) {
        return round($diff / 3600) . '시간 전';
    } else if ($diff < 3600 * 24 * 2) {
        return '어제';
    } else if ($diff < 3600 * 24 * 7) {
        return round($diff / 86400) . '일 전';
    } else if ($diff < 3600 * 24 * 7 * 7) {
        return round($diff / 604800) . '주 전';
    } else if ($diff < 3600 * 24 * 7 * 30) {
        return round($diff / 2592000) . '개월 전';
    } else {
        return date('n월 j일 A G:i ', $time);
    }
}

function if_add_post_comemnt() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_comments'];
    
	$post_id = $_POST['post_id'];
	$user_name = $_POST['comment_author'];
	$content = $_POST['comment_content'];
	$user_id = if_get_current_user_id();
	$state = 'new';
	$passwd = empty($_POST['comment_pw']) ? '' : $_POST['comment_pw'];
	
	$file_attachment = [];
	$thumb_attachment = [];
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
	    $upload_dir = UPLOAD_PATH . '/posts/comment/' . $post_id;
	    $upload_url = UPLOAD_URL . '/posts/comment/' . $post_id;
	    $atc_val = [];
	    
	    if (!is_dir($upload_dir)) {
	        mkdir($upload_dir, 0777, true);
	    }
	    foreach ($_POST['file_path'] as $key => $val) {
	        // 기존 파일
	        if (stripos($val, '/tmp/') === false) {
	            $atc_val['file_path'] = $val;
	            $atc_val['file_url'] = $_POST['file_url'][$key];
	            $atc_val['file_name'] = $_POST['file_name'][$key];
	            array_push($file_attachment, $atc_val);
	        } else {    // 신규 업로드 파일
	            $basename = basename($val);
	            $atc_val['file_path'] = $upload_dir . '/' . $basename;
	            $atc_val['file_url'] = $upload_url . '/' . $basename;
	            $atc_val['file_name'] = $_POST['file_name'][$key];
	            $result = rename($val, $atc_val['file_path']);
	            if (!empty($result)) {
	                array_push($file_attachment, $atc_val);
	            }
	        }
	    }
	}
	
	$compact = compact('thumb_attachment', 'file_attachment');
	$json = json_encode($compact);
	
	$query = "
		INSERT INTO
			$if_table1
			(
				comment_id,
				post_id,
				comment_author,
				comment_content,
				comment_user_id,
				comment_state,
				comment_pw,
				meta_data
			)
		VALUES
			(
				NULL, ?, ?, ?, ?,
				?, ?, ?
			)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ississs', $post_id, $user_name, $content, $user_id, $state, $passwd, $json);
	$stmt->execute();
	$comment_id = $ifdb->insert_id;
    return $comment_id;
}

function if_get_post_comment_by_id($comment_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_post_comments'];

	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				comment_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $comment_id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_post_comments($post_id, $order = 'ASC') {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_post_comments'];

	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				post_id = ?
			ORDER BY
				1 $order
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $post_id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_delete_post_comment($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_comments'];
    
    $query = "
			DELETE FROM
				$if_table1
			WHERE
				comment_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->affected_rows;
	return $result;
}

// 댓글 수
function if_get_post_comments_count($post_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_comments'];

	$query = "
			SELECT
				COUNT(*) AS cnt
			FROM
				$if_table1
			WHERE
				post_id = ?
	";
	$stmt = $ifdb->prepare( $query );
	$stmt->bind_param('i', $post_id);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}


?>