<?php
/*
 * Desc: 회원(user_class)에 해당하는 item products 
 */
function if_check_member_fee($user_id) {
    $user_row = if_get_user_by_id($user_id);
    $user_class = $user_row['user_class'];
    
    $items = if_get_items_of_class($user_class);    // 회원 class에 해당하는 item(product)
    
    return $items;
}

/*
 * Desc: class_id 에 해당하는 item products
 */
function if_get_items_of_class($class_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_product'];
    $if_table2 = $GLOBALS['if_tbl_user_class'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1 AS p
            LEFT JOIN
                $if_table2 AS c
            ON
                p.user_class_id = c.seq_id
			WHERE
                p.user_class_id = ? AND
                p.show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $class_id);
	$stmt->execute();
	$result =  $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc : 주문번호 생성
 */
function if_make_order_no($len) {
    $id = date('Ymd') . mt_rand();
    return substr($id, 0, $len);
}

/*
 * Desc: 회원이 회비를 무통장입금 신청한다.
 */
function if_add_item_order_due($user_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $order_code = $_POST['order_code'];
    $product_id = $_POST['product_id'];   // array
    
    $payment_method = 'BANK_TR';
    $remitter = $_POST['remitter'];
    $due_date = $_POST['due_date'];
    $bank_name = $_POST['bank_name'];
    $order_state = '1000';
    $ip_addr = $_SERVER['REMOTE_ADDR'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    seq_id,
                    order_code,
                    order_name,
                    order_amount,
                    order_state,
                    item_type,
                    item_type_secondary,
                    item_year,
                    item_period,
                    user_id,
                    meta_data,
                    create_dt,
                    product_id
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, NOW(), ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    
    foreach ($product_id as $key => $val) {
        $prod_row = if_get_product($val);
        
        $order_name = $prod_row['product_name'];
        $order_amount = $prod_row['product_price'];
        $item_year = $prod_row['product_year'];
        $item_type = $prod_row['product_type'];
        $item_type_secondary = 'member_dues';
        $item_period = $prod_row['product_period'];
        
        $compact = compact('remitter', 'bank_name', 'due_date', 'payment_method', 'ip_addr', 'user_agent', 'order_code');
        $meta_data = json_encode($compact);
        $stmt->bind_param('sssssssiiss', $order_code, $order_name, $order_amount, $order_state, $item_type, 
            $item_type_secondary, $item_year, $item_period, $user_id, $meta_data, $val);
        $stmt->execute();
        $order_id = $stmt->insert_id;
    }
    return $order_id;
}

/*
 * Desc: 신용카드 결제를 위한 임시 정보 저장
 */
function if_add_item_order_due_by_card($user_id, $order_code, $items, $pm) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $payment_method = $pm;
    $order_state = '1000';
    $ip_addr = $_SERVER['REMOTE_ADDR'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    seq_id,
                    order_code,
                    order_name,
                    order_amount,
                    order_state,
                    item_type,
                    item_type_secondary,
                    item_year,
                    item_period,
                    user_id,
                    meta_data,
                    create_dt,
                    product_id
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, NOW(), ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    
    foreach ($items as $key => $val) {
        $prod_row = if_get_product($val);
        
        $order_name = $prod_row['product_name'];
        $order_amount = $prod_row['product_price'];
        $item_year = $prod_row['product_year'];
        $item_type = $prod_row['product_type'];
        $item_type_secondary = 'member_dues';
        $item_period = $prod_row['product_period'];
        
        $compact = compact('payment_method', 'ip_addr', 'user_agent', 'order_code');
        $meta_data = json_encode($compact);
        $stmt->bind_param('sssssssiiss', $order_code, $order_name, $order_amount, $order_state, $item_type, 
            $item_type_secondary, $item_year, $item_period, $user_id, $meta_data, $val);
        $stmt->execute();
        $order_id = $stmt->insert_id;
    }
    return $order_id;
}

function if_get_item_order($seq_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $seq_id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_item_order_by_code($order_code) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
			WHERE
				order_code = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $order_code);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
	
}

function if_delete_item_order($order_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $order_id);
	$stmt->execute();
	$result = $ifdb->affected_rows;
	return $result;
}

function if_delete_item_order_by_code($order_code) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $query = "
			DELETE FROM
				$if_table1
			WHERE
				order_code = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $order_code);
	$stmt->execute();
	$result = $ifdb->affected_rows;
	return $result;
}

/*
 * Desc: 회원의 입회비 납부 여부를 조회
 */
function if_check_user_entance_fee($user_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
        SELECT
            pay_id
        FROM
            $if_table1
        WHERE
            user_id = ? AND
            item_type = 'ENT' AND
            pay_state = '9000'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $pay_id = $ifdb->get_var($stmt);
    return $pay_id;
}

function if_get_user_payment($pay_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
			WHERE
				pay_id = ?
	";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $pay_id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

function if_update_user_payment() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $pay_id = $_POST['pay_id'];
    $pay_content = $_POST['pay_content'];
    $pay_amount = $_POST['pay_amount'];
    $pay_date = $_POST['pay_date'];
    $pay_method = $_POST['pay_method'];
    $pay_state = $_POST['pay_state'];
//     $item_type_secondary = $_POST['item_type_secondary'];
    $item_year = $_POST['item_year'];
    $expiry_date = $_POST['expiry_date'];
    $admin_memo = empty($_POST['admin_memo']) ? '' : $_POST['admin_memo'];
    
    $update_dt = date('Y-m-d H:i:s');
    $compact = compact('update_dt', 'admin_memo');
    $meta_data = json_encode($compact);
    
    $query = "
			UPDATE
				$if_table1
            SET
                pay_content = ?,
                pay_amount = ?,
                pay_date = ?,
                pay_method = ?,
                pay_state = ?,
                item_year = ?,
                expiry_date = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				pay_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sisssssss', $pay_content, $pay_amount, $pay_date, $pay_method, $pay_state, 
	    $item_year, $expiry_date, $meta_data, $pay_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc : 변경내역을 meta_data에 기록한다.
 */
function if_record_user_payment($pay_id, $history) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $meta_data = json_encode(compact('history'));
    
    $query = "
			UPDATE
				$if_table1
            SET
                meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				pay_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $meta_data, $pay_id);
	$result = $stmt->execute();
	return $result;
}

function if_add_user_payment_by_code($order_code, $extra = NULL) {
    $pay_date = empty($_POST['pay_date']) ? date('Y-m-d') : $_POST['pay_date'];
    $result = '';
    
    $order_result = if_get_item_order_by_code($order_code);
    
    if (!empty($order_result)) {
        foreach ($order_result as $key => $val) {
            $seq_id = $val['seq_id'];
            $result = if_add_payment_from_cart($seq_id, $pay_date, $extra);
//             $result = if_move_item_order($seq_id, $pay_date);
        }
    }
    return $result;
}

function if_add_payment_from_cart($order_id, $pay_date, $extra = NULL) {   // if_item_order
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    // Get item_order data
    $order_row = if_get_item_order($order_id);
    
    $order_name = $order_row['order_name'];
    $order_amount = $order_row['order_amount'];
    $item_type = $order_row['item_type'];
    $item_type_secondary = $order_row['item_type_secondary'];
    $item_year = $order_row['item_year'];
    $user_id = $order_row['user_id'];
    $product_id = $order_row['product_id'];
    $ometa = $order_row['meta_data'];
    $jdec = json_decode($ometa, true);
    $payment_method = $jdec['payment_method'];
    
    $order_code = $order_row['order_code'];
    $item_period = $order_row['item_period'];
    $compact = compact('order_code', 'item_period', 'extra');
    $meta_data = json_encode($compact);
    $new_date = strtotime('+'.$item_period.'year', strtotime($pay_date));
    $expiry_date = date('Y-m-d', $new_date);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    pay_id,
                    pay_content,
                    pay_amount,
                    pay_date,
                    pay_method,
                    pay_state,
                    item_type,
                    item_type_secondary,
                    item_year,
                    create_dt,
                    expiry_date,
                    meta_data,
                    user_id,
                    product_id
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    '9000', ?, ?, ?, NOW(),
                    ?, ?, ?, ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('sssssssssss', $order_name, $order_amount, $pay_date, $payment_method, 
        $item_type, $item_type_secondary, $item_year, $expiry_date, 
        $meta_data, $user_id, $product_id);
    $stmt->execute();
    $pay_id = $stmt->insert_id;
    
    if ($pay_id && strcmp($item_type, 'ENT')) {     // 연회비가 아니면 회원의 회원 유지 만료기간을 업데이트
        $exp_date = if_get_user_meta($user_id, 'membership_expiry_date');
        if ($exp_date < $expiry_date) {
            if_update_user_meta($user_id, 'membership_expiry_date', $expiry_date);
        }
    }
    
    // 정회원으로 승격
//     if_update_user_class_of_user($user_id, '3000');
    
    return $pay_id;
}


/*
 * Desc : 회원의 학회 회비 납부 내역
 */
function if_get_user_payment_by_userid($user_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
			WHERE
				user_id = ? AND
                pay_state = '9000'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $user_id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc : 회원의 연회비 납부 여부 체크
 */
function if_check_payment_of_entrance_fee($user_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			SELECT
                pay_id
            FROM
				$if_table1
			WHERE
				user_id = ? AND
                pay_state = '9000' AND
                item_type = 'ENT'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $user_id);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

/*
 * Desc : order_code로 입금대기 내역 조회
 */
function if_get_user_payment_by_code($order_code) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_item_order'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
			WHERE
				order_code = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $order_code);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc : 회원의 복수 장기 회비 납부 내역 (유효기간이 지나지 않은)
 */
function if_get_count_user_payment($user_id, $item_type) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			SELECT
                COUNT(*) AS cnt
            FROM
				$if_table1
			WHERE
				user_id = ? AND
                pay_state = '9000' AND
                item_type = ? AND
                expiry_date >= CURDATE() AND
                JSON_UNQUOTE(meta_data->'$.item_period') = '5'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('is', $user_id, $item_type);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_delete_user_payment($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			DELETE FROM 
				$if_table1
			WHERE
				pay_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 결제한 학회 수
 */
function if_get_number_by_academy($academy) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
			SELECT
                COUNT(*) AS cnt
            FROM
				$if_table1
			WHERE
                pay_state = '9000' AND
                item_type_secondary = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $academy);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

/*
 * Desc: 회원이 결제 완료한 내역의 product_id를 조회한 후, 배열로 리턴
 */
function if_get_paid_product_ids($user_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_user_payment'];
    
    $query = "
            SELECT
                product_id
            FROM
                $if_table1
            WHERE
                user_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    
    $array = [];
    
    if (!empty($result)) {
        foreach ($result as $key => $val) {
            array_push($array, $val['product_id']);
        };
    }
    return $array;
}
?>