<?php
/*
 * Desc: 회원 등록
 */
function if_add_user() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$user_login	    = trim($_POST['user_login']);
	$user_pw	    = if_encrypt_passwd($_POST['password1']);
	$name_ko	    = empty($_POST['name_ko']) ? '' : trim($_POST['name_ko']);
	$user_email	    = empty($_POST['user_email']) ? '' : trim($_POST['user_email']);
	$user_mobile    = empty($_POST['user_mobile']) ? '' : $_POST['user_mobile'];
	$org_name	    = empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	$dept_name	    = empty($_POST['dept_name']) ? '' : trim($_POST['dept_name']);
	$user_birth	    = empty($_POST['user_birth']) ? '' : trim($_POST['user_birth']);
	$user_state	    = empty($_POST['user_state']) ? '10' : $_POST['user_state'];
	$user_class	    = if_get_user_default_class_id();
	$show_hide	    = empty($_POST['show_hide']) ? 'show' : $_POST['show_hide'];
	
	// meta_data
	$org_phone	    = empty($_POST['org_phone']) ? '' : trim($_POST['org_phone']);
	$org_position   = empty($_POST['org_position']) ? '' : trim($_POST['org_position']);
	$email_agree    = empty($_POST['email_agree']) ? '' : trim($_POST['email_agree']);
	$sms_agree	    = empty($_POST['sms_agree']) ? '' : trim($_POST['sms_agree']);
	$home_zipcode   = empty($_POST['home_zipcode']) ? '' : trim($_POST['home_zipcode']);
	$home_address1  = empty($_POST['home_address1']) ? '' : trim($_POST['home_address1']);
	$home_address2  = empty($_POST['home_address2']) ? '' : trim($_POST['home_address2']);
	$org_zipcode	= empty($_POST['org_zipcode']) ? '' : trim($_POST['org_zipcode']);
	$org_address1	= empty($_POST['org_address1']) ? '' : trim($_POST['org_address1']);
	$org_address2	= empty($_POST['org_address2']) ? '' : trim($_POST['org_address2']);
	$license_no	    = empty($_POST['license_no']) ? '' : trim($_POST['license_no']);
	$org_category	= empty($_POST['org_category']) ? '' : trim($_POST['org_category']);
	$user_qualification	    = empty($_POST['user_qualification']) ? '' : trim($_POST['user_qualification']);
	$user_qualification_etc = empty($_POST['user_qualification_etc']) ? '' : trim($_POST['user_qualification_etc']);
	$gender	= empty($_POST['gender']) ? '' : trim($_POST['gender']);
	
	$compact = compact('org_phone', 'org_position', 'email_agree', 'sms_agree',
		'home_zipcode', 'home_address1', 'home_address2','org_zipcode', 'org_address1', 'org_address2',
		'user_qualification', 'user_qualification_etc', 'license_no', 'org_category', 'gender');
	$meta_data = json_encode($compact);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					user_login,
					user_pw,
					name_ko,
					user_email,
					user_mobile,
					user_birth,
					license_no,
					org_name,
					dept_name,
					create_dt,
					update_dt,
					show_hide,
					user_state,
					user_class,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, ?,
					NOW(), NOW(), ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssssssssss', 
		$user_login, $user_pw, $name_ko, $user_email, $user_mobile,
		$user_birth, $license_no, $org_name, $dept_name, $show_hide, 
		$user_state, $user_class, $meta_data
	);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_user() {
	
}

/*
 * Desc: 회원 Mypage > 정보 수정
 */
function if_update_user($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$name_ko	    = empty($_POST['name_ko']) ? '' : trim($_POST['name_ko']);
	$user_email	    = empty($_POST['user_email']) ? '' : trim($_POST['user_email']);
	$user_mobile    = empty($_POST['user_mobile']) ? '' : $_POST['user_mobile'];
	$org_name	    = empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	$dept_name	    = empty($_POST['dept_name']) ? '' : trim($_POST['dept_name']);
	$user_birth	    = empty($_POST['user_birth']) ? '' : trim($_POST['user_birth']);
	
	// meta_data
	$org_phone	    = empty($_POST['org_phone']) ? '' : trim($_POST['org_phone']);
	$org_position   = empty($_POST['org_position']) ? '' : trim($_POST['org_position']);
	$email_agree    = empty($_POST['email_agree']) ? '' : trim($_POST['email_agree']);
	$sms_agree	    = empty($_POST['sms_agree']) ? '' : trim($_POST['sms_agree']);
	$home_zipcode   = empty($_POST['home_zipcode']) ? '' : trim($_POST['home_zipcode']);
	$home_address1  = empty($_POST['home_address1']) ? '' : trim($_POST['home_address1']);
	$home_address2  = empty($_POST['home_address2']) ? '' : trim($_POST['home_address2']);
	$org_zipcode	= empty($_POST['org_zipcode']) ? '' : trim($_POST['org_zipcode']);
	$org_address1	= empty($_POST['org_address1']) ? '' : trim($_POST['org_address1']);
	$org_address2	= empty($_POST['org_address2']) ? '' : trim($_POST['org_address2']);
	$license_no	    = empty($_POST['license_no']) ? '' : trim($_POST['license_no']);
	$org_category	= empty($_POST['org_category']) ? '' : trim($_POST['org_category']);
	$user_qualification	    = empty($_POST['user_qualification']) ? '' : trim($_POST['user_qualification']);
	$user_qualification_etc = empty($_POST['user_qualification_etc']) ? '' : trim($_POST['user_qualification_etc']);
	$gender	= empty($_POST['gender']) ? '' : trim($_POST['gender']);
	
	$update_dt	  = date('Y-m-d H:i:s');
	$remote_ip	  = $_SERVER['REMOTE_ADDR'];
	
	$compact = compact('org_phone', 'org_position', 'email_agree', 'sms_agree',
	    'home_zipcode', 'home_address1', 'home_address2','org_zipcode', 'org_address1', 'org_address2',
	    'user_qualification', 'user_qualification_etc', 'license_no', 'org_category', 'gender',
	    'update_dt', 'remote_ip');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				name_ko = ?,
				user_email = ?,
				user_mobile = ?,
				user_birth = ?,
				license_no = ?,
				org_name = ?,
				dept_name = ?,
				update_dt = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssssssi',
		$name_ko, $user_email, $user_mobile, $user_birth, $license_no, 
		$org_name, $dept_name, $update_dt, $meta_data, $user_id
	);
	$result = $stmt->execute();
	
	// 회원 정보 변경 완료 후 이메일 발송. 누구에게? 본인 or 학회 관리자
	$subject = '회원 정보가 변경되었습니다.';
	$body =<<<EOD
		$name_ko 님의 정보가 변경되었습니다.
EOD;
//	 if_send_mail($user_login, $subject, $body);	// send email

	return $result;
}

/*
 * Desc: CMS > 정보 수정
 */
function if_update_user_by_cms($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$user_class     = $_POST['user_class'];
	$user_state     = $_POST['user_state'];
	
	$name_ko	    = empty($_POST['name_ko']) ? '' : trim($_POST['name_ko']);
	$user_email	    = empty($_POST['user_email']) ? '' : trim($_POST['user_email']);
	$user_mobile    = empty($_POST['user_mobile']) ? '' : $_POST['user_mobile'];
	$org_name	    = empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	$dept_name	    = empty($_POST['dept_name']) ? '' : trim($_POST['dept_name']);
	$user_birth	    = empty($_POST['user_birth']) ? '' : trim($_POST['user_birth']);
	
	// meta_data
	$org_phone	    = empty($_POST['org_phone']) ? '' : trim($_POST['org_phone']);
	$org_position   = empty($_POST['org_position']) ? '' : trim($_POST['org_position']);
	$email_agree    = empty($_POST['email_agree']) ? '' : trim($_POST['email_agree']);
	$sms_agree	    = empty($_POST['sms_agree']) ? '' : trim($_POST['sms_agree']);
	$home_zipcode   = empty($_POST['home_zipcode']) ? '' : trim($_POST['home_zipcode']);
	$home_address1  = empty($_POST['home_address1']) ? '' : trim($_POST['home_address1']);
	$home_address2  = empty($_POST['home_address2']) ? '' : trim($_POST['home_address2']);
	$org_zipcode	= empty($_POST['org_zipcode']) ? '' : trim($_POST['org_zipcode']);
	$org_address1	= empty($_POST['org_address1']) ? '' : trim($_POST['org_address1']);
	$org_address2	= empty($_POST['org_address2']) ? '' : trim($_POST['org_address2']);
	$license_no	    = empty($_POST['license_no']) ? '' : trim($_POST['license_no']);
	$org_category	= empty($_POST['org_category']) ? '' : trim($_POST['org_category']);
	$user_qualification	    = empty($_POST['user_qualification']) ? '' : trim($_POST['user_qualification']);
	$user_qualification_etc = empty($_POST['user_qualification_etc']) ? '' : trim($_POST['user_qualification_etc']);
	$gender	= empty($_POST['gender']) ? '' : trim($_POST['gender']);
	
	$update_dt	  = date('Y-m-d H:i:s');
	$remote_ip	  = $_SERVER['REMOTE_ADDR'];
	$admin_memo	 = empty($_POST['admin_memo']) ? '' : $_POST['admin_memo'];
	
	$compact = compact('org_phone', 'org_position', 'email_agree', 'sms_agree',
	    'home_zipcode', 'home_address1', 'home_address2','org_zipcode', 'org_address1', 'org_address2',
	    'user_qualification', 'user_qualification_etc', 'license_no', 'org_category', 'gender',
	    'update_dt', 'remote_ip', 'admin_memo');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				user_state = ?,
				user_class = ?,
				name_ko = ?,
				user_email = ?,
				user_mobile = ?,
				user_birth = ?,
				license_no = ?,
				org_name = ?,
				dept_name = ?,
				update_dt = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssssssssi',
		$user_state, $user_class, $name_ko, $user_email, $user_mobile, 
	    $user_birth, $license_no, $org_name, $dept_name, $update_dt, 
	    $meta_data, $user_id
	);
	$result = $stmt->execute();
	
	if ($result) {
		if (!empty($_POST['user_pass'])) { // 비밀번호 변경
			$user_pass = $_POST['user_pass'];
			if_update_user_password($user_id, $user_pass);
		};
	}

	return $result;
}

/*
 * Desc: 회원 삭제
 */
function if_delete_user($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	// FK 삭제
	if_delete_user_login_history($user_id);
	if_delete_user_meta($user_id);
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $user_id);
	$result = $stmt->execute();
	return $result;
}

function if_update_user_password($id, $password) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$enc_password = if_encrypt_passwd($password);
	
	$query = "
			UPDATE
				$if_table1
			SET
				user_pw = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $enc_password, $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회원 차단
 */
function if_block_user($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$block_dt = date('Y-m-d H:i:s');
	$compact = compact('block_dt');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				user_state = '41',
				meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $meta_data, $user_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회원 삭제(hide)
 */
function if_hide_user($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$hide_dt = date('Y-m-d H:i:s');
	$compact = compact('hide_dt');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				show_hide = 'hide',
				meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $meta_data, $user_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회원 복구(show)
 */
function if_show_user($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$show_dt = date('Y-m-d H:i:s');
	$compact = compact('show_dt');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				show_hide = 'show',
				meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $meta_data, $user_id);
	$result = $stmt->execute();
	return $result;
}

function if_get_user_by_id($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

function if_get_user_by_login($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				user_login = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

/*
 * Desc: meta_data의 data를 조회
 */
function if_get_user_meta_data_by_key($id, $jkey) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				JSON_UNQUOTE(JSON_EXTRACT(meta_data, '$." . $jkey . "'))
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

/*
 * Desc: 회원 테이블의 meta_data 컬럼의 내용을 key로 조회
 */
function if_get_user_by_tempid($temp_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				JSON_EXTRACT(meta_data, '$.user_temp_id') = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $temp_id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

// 중복 아이디 존재 여부 확인
function if_exist_user_login($user_login) {
	global $ifdb;
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				user_login = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $user_login);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc : 아이디 찾기
 */
function if_find_user_login($name, $email) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				user_login
			FROM
				$if_table1
			WHERE
				name_ko = ? AND
				user_email = ? AND
				user_state < '40'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ss', $name, $email);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}

/*
 * Desc : 아이디로 회원 id 조회
 */
function if_get_userid_by_login($user_login) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];		// user table
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				user_login = ? AND
				user_state < '40'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $user_login);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}

/*
 * Desc: 회원 수 show_hide > show
 */
function if_get_user_total_count() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
			WHERE
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}

/*
 * Desc : 임시 비밀번호로 업데이트
 */
function if_update_user_passwd_by_temp($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$pp_table1 = $GLOBALS['if_tbl_users'];
	
	$tmp_pw = mt_rand(1005, 9945) . '#' . if_make_random_str(6);
	$passwd = if_encrypt_passwd($tmp_pw);
	
	$query = "
			UPDATE
				$pp_table1
			SET
				user_pw = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $passwd, $user_id);
	$result = $stmt->execute();
	if (!empty($result)) {
		return $tmp_pw;
	} else {
		return false;
	}
}

function if_update_user_meta($user_id, $meta_key, $meta_value) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_usermeta'];
	
	$query = "
			SELECT
				meta_id
			FROM
				$if_table1
			WHERE
				user_id = ? AND
				meta_key = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('is', $user_id, $meta_key);
	$stmt->execute();
	$meta_id = $ifdb->get_var($stmt);
		
	if (empty($meta_id)) {		// INSERT
		$query = "
				INSERT INTO
					$if_table1
					(
						meta_id,
						user_id,
						meta_key,
						meta_value
					)
				VALUES
					(
						0, ?, ?, ?
					)
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('iss', $user_id, $meta_key, $meta_value);
		$stmt->execute();
		
		return $stmt->insert_id;
			
	} else {		// UPDATE
		$query = "
				UPDATE
					$if_table1
				SET
					meta_value = ?
				WHERE
					user_id = ? AND
					meta_key = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('sis', $meta_value, $user_id, $meta_key);
		return $stmt->execute();
	}
}

function if_get_user_meta($user_id, $meta_key = NULL) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_usermeta'];
	
	if ($meta_key) {
		$query = "
				SELECT
					meta_value
				FROM
					$if_table1
				WHERE
					user_id = ? AND
					meta_key = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('is', $user_id, $meta_key);
		$stmt->execute();
		return $ifdb->get_var($stmt);
	} else {
		$query = "
				SELECT
					meta_key,
					meta_value
				FROM
					$if_table1
				WHERE
					user_id = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('i', $user_id);
		$stmt->execute();
		
		$data = $ifdb->get_results($stmt);
		$array = array();
		
		if (!empty($data)) {
			foreach ($data as $key => $val) {
				$array[$val['meta_key']] = $val['meta_value'];
			}
		}
		return $array;
	}
}

function if_delete_user_meta($user_id, $meta_key = NULL) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_usermeta'];
	
	if ($meta_key) {
		$query = "
				DELETE FROM
					$if_table1
				WHERE
					user_id = ? AND
					meta_key = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('is', $user_id, $meta_key);
		$result = $stmt->execute();
	} else {
		$query = "
				DELETE FROM
					$if_table1
				WHERE
					user_id = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('i', $user_id);
		$result = $stmt->execute();
	}
}

/*
 * Desc: 회원 로그인 정보 저장
 */
function if_add_user_login_history($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_login_history'];
	
	$ip_addr = $_SERVER['REMOTE_ADDR'];
	$country_code = '';
	$c_date = date('Y-m-d');
	$c_time = date('H:i:s');
	$browser = $_SERVER['HTTP_USER_AGENT'];
	$json = json_encode(compact('browser'));
	
	// IP to Location
	// 	$check_url = 'http://ip-api.com/json/' . $ip_addr;
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_URL, $check_url);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_HEADER, 0);
	// 	$curl_result =  curl_exec($ch);		// return json
	// 	curl_close($ch);
	
	// 	$curl_array = json_decode($curl_result, true);
	// 	$status = $curl_array['status'];
	// 	if (!strcmp($status, 'success')) {
	// 		$country_code = $curl_array['countryCode'];
	// 	} else {
	// 		$country_code = '';
	// 	}
	// /.IP to Location
	
	$query = "
			INSERT INTO
				$if_table1
				(
					log_id,
					login_date,
					login_time,
					ip_addr,
					country_code,
					meta_data,
					user_id
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $c_date, $c_time, $ip_addr, $country_code, $json, $user_id);
	$stmt->execute();
	$log_id = $stmt->insert_id;
	return $log_id;
}

/*
 * Desc: 로그인 날짜/시각을 if_users의 meta_data 업데이트
 */
function if_update_user_login_history($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
  
	$last_login_dt = date('Y-m-d H:i:s');
	$json = json_encode(compact('last_login_dt'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $json, $user_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 로그인 히스토리 삭제
 */
function if_delete_user_login_history($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_login_history'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				user_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $user_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: serialize data 조회하여 삭제
 *	   https://stackoverflow.com/questions/24508164/how-do-i-search-from-serialize-field-in-mysql-database
 */
function if_delete_user_session($user_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_session'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				sess_data REGEXP '.*\"sid\";i:$user_id.*'
	";
	$stmt = $ifdb->prepare($query);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회원등급 변경
 */
function if_update_user_class_of_user($user_id, $class) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_users'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				user_class = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $class, $user_id);
	$result = $stmt->execute();
	return $result;
}

//---------- 임원
/*
 * Desc: 임원 등록
 */
function if_add_executives() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_executives'];
	
	$position	= empty($_POST['position']) ? '' : trim($_POST['position']);
	$name		= empty($_POST['name']) ? '' : trim($_POST['name']);
	$email	   = empty($_POST['email']) ? '' : trim($_POST['email']);
	$org_name	= empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	
	$create_dt = date('Y-m-d H:i:s');
	$file_attachment = [];
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {

		$thumb_instance = new Thumbnail();
		
		// Thumbnail
		$thumb_suffix = '-thumb';
		$thumb_width = 350;
		
		$upload_dir = UPLOAD_PATH . '/executives';
		$upload_url = UPLOAD_URL . '/executives';
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			$basename = basename($val);
			$atc_val['file_path'] = $upload_dir . '/' . $basename;
			$atc_val['file_url'] = $upload_url . '/' . $basename;
			$atc_val['file_name'] = $_POST['file_name'][$key];
			$result = rename($val, $atc_val['file_path']);
			if (!empty($result)) {
				// 썸네일 생성
				$atc_val['thumb_name'] = $thumb_instance->resize_image($atc_val['file_path'], $thumb_suffix, $thumb_width);
				$atc_val['thumb_path'] = UPLOAD_PATH . '/executives/' . $atc_val['thumb_name'];
				$atc_val['thumb_url'] = UPLOAD_URL . '/executives/' . $atc_val['thumb_name'];
				
				array_push($file_attachment, $atc_val);
			}
		}
	}
	
	$compact = compact('create_dt', 'file_attachment');
	$meta_data = json_encode($compact);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					position,
					name,
					org_name,
					email,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssss', $position, $name, $org_name, $email, $meta_data);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
	
}

function if_get_executives_by_id($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_executives'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $seq_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

function if_update_executives($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_executives'];
	
	$position	= empty($_POST['position']) ? '' : trim($_POST['position']);
	$name		= empty($_POST['name']) ? '' : trim($_POST['name']);
	$email	   = empty($_POST['email']) ? '' : trim($_POST['email']);
	$org_name	= empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	
	$update_dt	  = date('Y-m-d H:i:s');
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		
		$thumb_instance = new Thumbnail();
		
		$file_attachment = [];
		// Thumbnail
		$thumb_suffix = '-thumb';
		$thumb_width = 350;
		
		$upload_dir = UPLOAD_PATH . '/executives';
		$upload_url = UPLOAD_URL . '/executives';
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			$basename = basename($val);
			$atc_val['file_path'] = $upload_dir . '/' . $basename;
			$atc_val['file_url'] = $upload_url . '/' . $basename;
			$atc_val['file_name'] = $_POST['file_name'][$key];
			$result = rename($val, $atc_val['file_path']);
			if (!empty($result)) {
				// 썸네일 생성
				$atc_val['thumb_name'] = $thumb_instance->resize_image($atc_val['file_path'], $thumb_suffix, $thumb_width);
				$atc_val['thumb_path'] = UPLOAD_PATH . '/executives/' . $atc_val['thumb_name'];
				$atc_val['thumb_url'] = UPLOAD_URL . '/executives/' . $atc_val['thumb_name'];
				
				array_push($file_attachment, $atc_val);
			}
		}
		$compact = compact('update_dt', 'file_attachment');
	} else {
		$compact = compact('update_dt');
	}
	
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				position = ?,
				name = ?,
				org_name = ?,
				email = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $position, $name, $org_name, $email, $meta_data, $seq_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 임원 삭제
 */
function if_delete_executives($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_executives'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $seq_id);
	$result = $stmt->execute();
	return $result;
}

function if_get_total_executives() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_executives'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}

//-- 후원자
/*
 * Desc: 후원자 등록
 */
function if_add_sponsor() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_sponsors'];
	$sponsor_user_id	= empty($_POST['sponsor_user_id']) ? 0 : trim($_POST['sponsor_user_id']); //회원 seq_id 추가
	$apply_dt	= empty($_POST['apply_dt']) ? '' : trim($_POST['apply_dt']);
	$name		= empty($_POST['name']) ? '' : trim($_POST['name']);
	$birth_dt	= empty($_POST['birth_dt']) ? '' : trim($_POST['birth_dt']);
	$org_name	= empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	$mobile	  = empty($_POST['mobile']) ? '' : trim($_POST['mobile']);
	$email	   = empty($_POST['email']) ? '' : trim($_POST['email']);
	$sponsorship_type	   = empty($_POST['sponsorship_type']) ? '' : trim($_POST['sponsorship_type']);

	$temp_amount = empty($_POST['temp_amount']) ? '' : $_POST['temp_amount'];   // 일시 후원금
	$man_input = empty($_POST['prd_manual_input']) ? '' : $_POST['prd_manual_input'];   // 직접 입력
	$prd_amt_account = empty($_POST['prd_amt_account']) ? '' : $_POST['prd_amt_account'];   // 구좌 수
	$prd_amount = empty($_POST['prd_amount']) ? '' : $_POST['prd_amount'];  // 직접 입력한 후원 금액
	
	$prd_bank_name = empty($_POST['prd_bank_name']) ? '' : $_POST['prd_bank_name'];
	$prd_account_no = empty($_POST['prd_account_no']) ? '' : $_POST['prd_account_no'];
	$prd_account_holder = empty($_POST['prd_account_holder']) ? '' : $_POST['prd_account_holder'];
	$prd_start_ym = empty($_POST['prd_start_ym']) ? '' : $_POST['prd_start_ym'];
	$prd_usage = empty($_POST['prd_usage']) ? '' : $_POST['prd_usage'];
	$prd_etc_content = empty($_POST['prd_etc_content']) ? '' : $_POST['prd_etc_content'];
	$don_name = empty($_POST['don_name']) ? '' : $_POST['don_name'];
	$don_reg_no = empty($_POST['don_reg_no']) ? '' : $_POST['don_reg_no'];
	$don_address = empty($_POST['don_address']) ? '' : $_POST['don_address'];
	
	/*임시로 넣음 시작 */
	if(empty($apply_dt)){
		$apply_dt = date('Y-m-d');
	}
	/*임시로 넣음 끝 */
	
	if (!strcmp($sponsorship_type, 'temp')) {   // 일시 후원
		$sponsorship_amount = $_POST['temp_amount']; 

	} else {	// 정기 후원
		if ($man_input) {   // 직접 입력
			$sponsorship_amount = $prd_amount;
		} else {	// 구좌
			$sponsorship_amount = $prd_amt_account * 10000;
		}
	}
	/* 파일첨부 부분 추가  시작*/
	$file_attachment = [];
 
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		$year = date('Y');
		$month = date('m');
		$upload_dir = UPLOAD_PATH . '/sponsor/' . $year . '/' . $month;
		$upload_url = UPLOAD_URL . '/sponsor/' . $year . '/' . $month;
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			// 기존 파일
			if (stripos($val, '/tmp/') === false) {
				$atc_val['file_path'] = $val;
				$atc_val['file_url'] = $_POST['file_url'][$key];
				$atc_val['file_name'] = $_POST['file_name'][$key];
				array_push($file_attachment, $atc_val);
			} else {	// 신규 업로드 파일
				$basename = basename($val);
				$atc_val['file_path'] = $upload_dir . '/' . $basename;
				$atc_val['file_url'] = $upload_url . '/' . $basename;
				$atc_val['file_name'] = $_POST['file_name'][$key];
				$result = rename($val, $atc_val['file_path']);
				if (!empty($result)) {
					array_push($file_attachment, $atc_val);
				}
			}
		}
		if (!empty($file_attachment)) {
			$meta_data = json_encode(compact('file_attachment'));
		}
	}
	
	/* 파일첨부 부분 추가 끝*/

	$compact = compact('temp_amount', 'man_input', 'prd_amt_account', 'prd_amount', 'prd_bank_name', 
		'prd_account_no', 'prd_account_holder', 'prd_start_ym', 'prd_usage', 'prd_etc_content', 
		'don_name', 'don_reg_no', 'don_address', 'file_attachment');
	
	$meta_data = json_encode($compact);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					name,
					birth_dt,
					org_name,
					mobile,
					email,
					sponsorship_type,
					sponsorship_amount,
					apply_dt,
					create_dt,
					user_id,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, NOW(), ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssssssis', $name, $birth_dt, $org_name, $mobile, $email, $sponsorship_type, $sponsorship_amount, $apply_dt, $sponsor_user_id, $meta_data);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

/*
 * Desc: 후원자 조회
 */
function if_get_sponsor_by_id($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_sponsors'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $seq_id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

/*
 * Desc: 후원자 편집
 */
function if_update_sponsor($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_sponsors'];
	
	$apply_dt	= empty($_POST['apply_dt']) ? '' : trim($_POST['apply_dt']);
	$name		= empty($_POST['name']) ? '' : trim($_POST['name']);
	$birth_dt	= empty($_POST['birth_dt']) ? '' : trim($_POST['birth_dt']);
	$org_name	= empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
	$mobile	  = empty($_POST['mobile']) ? '' : trim($_POST['mobile']);
	$email	   = empty($_POST['email']) ? '' : trim($_POST['email']);
	$sponsorship_type	   = empty($_POST['sponsorship_type']) ? '' : trim($_POST['sponsorship_type']);
	
	$temp_amount = empty($_POST['temp_amount']) ? '' : $_POST['temp_amount'];   // 일시 후원금
	$man_input = empty($_POST['prd_manual_input']) ? '' : $_POST['prd_manual_input'];   // 직접 입력
	$prd_amt_account = empty($_POST['prd_amt_account']) ? '' : $_POST['prd_amt_account'];   // 구좌 수
	$prd_amount = empty($_POST['prd_amount']) ? '' : $_POST['prd_amount'];  // 직접 입력한 후원 금액
	
	$prd_bank_name = empty($_POST['prd_bank_name']) ? '' : $_POST['prd_bank_name'];
	$prd_account_no = empty($_POST['prd_account_no']) ? '' : $_POST['prd_account_no'];
	$prd_account_holder = empty($_POST['prd_account_holder']) ? '' : $_POST['prd_account_holder'];
	$prd_start_ym = empty($_POST['prd_start_ym']) ? '' : $_POST['prd_start_ym'];
	$prd_usage = empty($_POST['prd_usage']) ? '' : $_POST['prd_usage'];
	$prd_etc_content = empty($_POST['prd_etc_content']) ? '' : $_POST['prd_etc_content'];
	$don_name = empty($_POST['don_name']) ? '' : $_POST['don_name'];
	$don_reg_no = empty($_POST['don_reg_no']) ? '' : $_POST['don_reg_no'];
	$don_address = empty($_POST['don_address']) ? '' : $_POST['don_address'];
	
	if (!strcmp($sponsorship_type, 'temp')) {   // 일시 후원
		$sponsorship_amount = $_POST['temp_amount'];
	} else {	// 정기 후원
		if ($man_input) {   // 직접 입력
			$sponsorship_amount = $prd_amount;
		} else {	// 구좌
			$sponsorship_amount = $prd_amt_account * 10000;
		}
	}
	
	$compact = compact('temp_amount', 'man_input', 'prd_amt_account', 'prd_amount', 'prd_bank_name',
		'prd_account_no', 'prd_account_holder', 'prd_start_ym', 'prd_usage', 'prd_etc_content',
		'don_name', 'don_reg_no', 'don_address');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				name = ?,
				birth_dt = ?,
				org_name = ?,
				mobile = ?,
				email = ?,
				sponsorship_type = ?,
				sponsorship_amount = ?,
				apply_dt = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssssssi', $name, $birth_dt, $org_name, $mobile, $email, $sponsorship_type, $sponsorship_amount, $apply_dt, $meta_data, $seq_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 후원자 삭제
 */
function if_delete_sponsor($seq_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_sponsors'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $seq_id);
	$result = $stmt->execute();
	return $result;
}

function if_get_total_sponsors() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_sponsors'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}
?>