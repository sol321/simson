<?php
function if_add_term() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$parent_id = $_POST['parent_id'];
	$term_group = $_POST['term_group'];
	$term_name = trim($_POST['term_name']);
	$menu_url = empty($_POST['menu_url']) ? '' : trim($_POST['menu_url']);
	$term_order = 0;
	$create_dt = date('Y-m-d H:i:s');
	$is_hidden = empty($_POST['is_hidden']) ? '' : $_POST['is_hidden'];
	
	$meta_data = compact('menu_url', 'create_dt', 'is_hidden');
	$json = json_encode($meta_data);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					term_name,
					term_group,
					term_order,
					parent_id,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssiis', $term_name, $term_group, $term_order, $parent_id, $json);
	$stmt->execute();
	$seq_id = $ifdb->insert_id;
	return $seq_id;
}

function if_get_term_by_id($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_child_term_count($pid) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
			WHERE
				parent_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $pid);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

/*
 * Desc: fancytree 메뉴
 */
function if_get_terms($pid, $group) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			SELECT
				seq_id, 
				term_name, 
				JSON_EXTRACT(meta_data, '$.menu_url') AS menu_url,
				JSON_EXTRACT(meta_data, '$.is_hidden') AS is_hidden
			FROM
				$if_table1
			WHERE
				parent_id = ? AND
				term_group = ?
			ORDER BY
				term_order ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('is', $pid, $group);
	$stmt->execute();
	$tree_item = $ifdb->get_results($stmt);

	$json = [];
	
	if (!empty($tree_item)) {
		foreach ($tree_item as $key => $val) {
			$seq_id = $val['seq_id'];
			$term_name = $val['term_name'];
			$menu_url = trim($val['menu_url'], '"');	// JSON_UNQUOTE
			$is_hidden = trim($val['is_hidden'], '"');
			
			$selected = false;
			$url = '';
			
			if (if_get_child_term_count($seq_id) > 0) {
				$children = json_decode(if_get_terms($seq_id, $group));
				$folder = true;
			} else {
				$children = [];
				$folder = false;
			}
			
			if (!strcmp($is_hidden, 'Y')) {
				$title = '<span style="text-decoration: line-through">' . $term_name . ' ' . $menu_url . '</span>';
				$tooltip = 'hide';
			} else {
				$title = $term_name . ' <span style="color: gray;">' . $menu_url . '</span>';
				$tooltip = 'show';
			}
			
			$json[] = array(
				'key'	   => $seq_id,
				'title'	 => $title,
				'expanded'  => false,
				'extraClasses'  => '',
				'folder'	=> $folder,
				'children'  => $children,
				'selected'  => $selected,
				'url'	   => $menu_url,
				'titleOnly'	=> $term_name,
				'tooltip'   => $tooltip
		   );
		}
	}
	return json_encode($json);
}

/*
 * Desc: AdminLTE Sidebar 출력
 *  All menu open 처리 : menu-open, style="display:blcok;" 추가
 */
function if_print_terms($pid, $group, $active = 0) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_terms'];
    $root_id = '';
    
    $query = "
			SELECT
				seq_id,
				term_name,
				JSON_EXTRACT(meta_data, '$.menu_url') AS menu_url
			FROM
				$if_table1
			WHERE
				parent_id = ? AND
				term_group = ? AND
				JSON_SEARCH(meta_data, 'one', 'Y', NULL, '$.is_hidden') IS NULL
			ORDER BY
				term_order ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('is', $pid, $group);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	
	if (!empty($result)) {
	    foreach ($result as $key => $val) {
	        $seq_id = $val['seq_id'];
	        $term_name = $val['term_name'];
	        $menu_url = trim($val['menu_url'], '"');	// JSON_UNQUOTE
	        $req_uri = $_SERVER['REQUEST_URI'];
	        
	        $root_node = if_get_root_parent_id($seq_id);
	        $root_id = $root_node['seq_id'];     // 최상위 item ID (자신을 포함)
	        $active_id = 0;
	        
	        if (if_get_child_term_count($seq_id) > 0) {
	            // 				echo '<li class="active treeview menu-open">';
	            echo '<li class="treeview pid-' . $active_id . '">';
	            //				 echo '<li class="treeview menu-open">';	  // All menu open
	            echo '<a href="#">';
	            echo '<i class="fa fa-th"></i><span>';
	            echo $term_name;
	            echo '</span>';
	            echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
	            echo '</a>';
	            echo '<ul class="treeview-menu pid-' . $root_id . '">';
	            //				 echo '<ul class="treeview-menu" style="display: block;">';   // All menu open
	            if_print_terms($seq_id, $group, $active_id);
	            echo '</ul>';
	        } else {
	            if (!empty($menu_url)) {
	                if (strpos($req_uri, $menu_url) !== false) {
	                    $active_id = $root_id;
	                    echo '<li class="active" id="pid-' . $active_id . '">';
	                } else {
	                    echo '<li>';
	                }
	            } else {
	                echo '<li>';
	            }
	            echo '<a href="'. $menu_url . '">';
	            echo '<i class="fa fa-circle-o"></i><span>';
	            echo $term_name;
	            echo '</span>';
	            echo '</a>';
	            echo '<ul class="treeview-menu">';
	            if_print_terms($seq_id, $group, $active_id);
	            echo '</ul>';
	        }
	        echo '</li>';
	    }
	}
}

function if_edit_term() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$seq_id = $_POST['seq_id'];
	$term_name = trim($_POST['term_name']);
	$menu_url = empty($_POST['menu_url']) ? '' : trim($_POST['menu_url']);
	$update_dt = date('Y-m-d H:i:s');
	$is_hidden = empty($_POST['is_hidden']) ? '' : $_POST['is_hidden'];
	
	$meta_data = compact('menu_url', 'update_dt', 'is_hidden');
	$json = json_encode($meta_data);
	
	$query = "
			UPDATE
				$if_table1
			SET
				term_name = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssi', $term_name, $json, $seq_id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_term($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE 
				parent_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	
	if (!empty($result)) {
		foreach ($result as $key => $val) {
			$seq_id = $val['seq_id'];
			if_delete_term($seq_id);
		}
	}
}

/*
 * Desc: 하위 node로 이동 시
 */
function if_move_term_to_parent($action_id, $target_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				parent_id = ?
			WHERE 
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ii', $target_id, $action_id);
	$stmt->execute();
	$result = $ifdb->affected_rows;
	return $result;
}

/*
 * Desc: 동일 node 아래에서 순서만 변경 시
 */
function if_move_term_to_sibling($action_id, $target_id, $nodes) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$sibling = if_get_term_by_id($target_id);
	$sibling_pid = $sibling['parent_id'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				parent_id = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ii', $sibling_pid, $action_id);
	$stmt->execute();
				
	$query = "
			UPDATE
				$if_table1
			SET
				term_order = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	
	foreach ($nodes as $key => $val) {
		$stmt->bind_param('ii', $key, $val);
		$stmt->execute();
	}
	return true;
}

/*
 * Desc: if_terms의 meta_data > menu_url로 term_name 조회
 */
function if_get_term_name_by_url($url = NULL) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	if (empty($url)) {
		$url = $_SERVER['REQUEST_URI'];
	}
	
	$query = "
			SELECT
				term_name
			FROM
				$if_table1
			WHERE
				JSON_EXTRACT(meta_data, '$.menu_url') = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $url);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_get_term_group_count($group) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
			WHERE 
				term_group = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $group);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_add_sample_term_data($sql) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_terms'];
	
	$query = file_get_contents($sql);

	$stmt = $ifdb->prepare($query);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 가장 상위의 seq_id를 조회
 */
function if_get_root_parent_id($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_terms'];
    
    $query = "
			WITH RECURSIVE cte AS
			(
                SELECT
                    seq_id, term_name, parent_id
                FROM
                    $if_table1
                WHERE seq_id = ?
                UNION ALL
                SELECT
                    c.seq_id, c.term_name, c.parent_id
                FROM
                    $if_table1 c
                JOIN
                    cte
                ON
                    c.seq_id = cte.parent_id
			)
			SELECT * FROM cte WHERE parent_id = 0
			
	";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}
?>