<?php
function if_add_address_group() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_group'];
    
    $group_name = trim($_POST['group_name']);
    $meta_data = json_encode('');
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					group_name,
                    create_dt,
                    meta_data
				)
			VALUES
				(
					NULL, ?, NOW(), ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ss', $group_name, $meta_data);
	$stmt->execute();
	$group_id = $stmt->insert_id;
	return $group_id;
}

function if_get_address_group_by_id($id) {
    
}

function if_update_address_group($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_group'];
    
    $group_name = trim($_POST['data']);
    
    $query = "
			UPDATE
				$if_table1
            SET
                group_name = ?
            WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $group_name, $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc : 그룹 삭제
 */
function if_delete_address_group($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_group'];
    
    $query = "
			DELETE FROM
				$if_table1
            WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

function if_get_address_groupname($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_group'];
    
    $query = "
			SELECT
                group_name
            FROM
				$if_table1
            WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$gname = $ifdb->get_var($stmt);
	return $gname;
}

/*
 * Desc: 모든 그룹 정보 조회
 */
function if_get_address_groups() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_group'];
    
    $query = "
			SELECT
                *
            FROM
				$if_table1
            ORDER BY
                group_name ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc: 그룹에 속한 주소록의 개수를 조회
 */
function if_get_address_contact_count($group_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $query = "
			SELECT
                COUNT(*) AS count
            FROM
				$if_table1
            WHERE
                group_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $group_id);
	$stmt->execute();
	$count = $ifdb->get_var($stmt);
	return $count;
}

/*
 * Desc: 주소록의 group_id 를 0으로 변경
 */
function if_initialize_group_of_contact($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $query = "
			UPDATE
				$if_table1
            SET
                group_id = 0
            WHERE
                group_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 그룹 ID에 해당하는 주소 삭제
 */
function if_delete_address_contact_by_group($group_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $query = "
			DELETE FROM
				$if_table1
            WHERE
                group_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $group_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 주소 추가
 */
function if_add_address_contact() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $group_id = $_POST['group_id'];
    $contact_name = trim($_POST['contact_name']);
    $contact_email = trim($_POST['contact_email']);
    $contact_mobile = empty($_POST['contact_mobile']) ? '' : trim($_POST['contact_mobile']);
    $org_name = empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
    
    $meta_data = json_encode('');
    
    $query = "
			INSERT INTO
				$if_table1
				(
					contact_id,
                    contact_name,
                    contact_email,
                    contact_mobile,
                    org_name,
                    create_dt,
                    meta_data,
                    group_id
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					NOW(), ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $contact_name, $contact_email, $contact_mobile, $org_name, $meta_data, $group_id);
	$stmt->execute();
	$contact_id = $stmt->insert_id;
	return $contact_id;
}

function if_get_address_contact_by_id($id) {
    
}

function if_update_address_contact($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $contact_name = trim($_POST['contact_name']);
    $contact_email = trim($_POST['contact_email']);
    $contact_mobile = empty($_POST['contact_mobile']) ? '' : trim($_POST['contact_mobile']);
    $org_name = empty($_POST['org_name']) ? '' : trim($_POST['org_name']);
    
    $update_dt = date('Y-m-d H:i:s');
    $compact = compact('update_dt');
    $meta_data = json_encode($compact);
    
    $query = "
			UPDATE
				$if_table1
		    SET
                contact_name = ?,
                contact_email = ?,
                contact_mobile = ?,
                org_name = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				contact_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $contact_name, $contact_email, $contact_mobile, $org_name, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_address_contact($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $query = "
			DELETE FROM
				$if_table1
            WHERE
                contact_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc : 주소록 대량 업로드, excel 파일
 */
function if_add_excel_file_to_address($upload_file) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_address_contact'];
    
    $affected_rows = 0;	// 등록된 개수
    $array_data = array();
    $query_body = '';
    $file_ext = if_get_file_extension($upload_file);
    $group_id = empty($_POST['group_id']) ? 0 : $_POST['group_id'];
    
    if (!strcmp($file_ext, 'xls')) {
        require_once INC_PATH . '/classes/PHPExcel.php';
        
        $objPHPExcel = new PHPExcel();
        $objReader = new PHPExcel_Reader_Excel5();
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($upload_file);
        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        $sheet = $objPHPExcel->getActiveSheet();
        
        foreach ($rowIterator as $row) {
            $rowIndex = $row->getRowIndex();
            $array_data[$rowIndex] = array('A'=>'', 'B'=>'', 'C'=>'', 'D'=>'');
            
            $cell = $sheet->getCell('A' . $rowIndex);
            $array_data[$rowIndex]['A'] = $cell->getValue();
            $cell = $sheet->getCell('B' . $rowIndex);
            $array_data[$rowIndex]['B'] = $cell->getValue();
            $cell = $sheet->getCell('C' . $rowIndex);
            $array_data[$rowIndex]['C'] = $cell->getValue();
            $cell = $sheet->getCell('D' . $rowIndex);
            $array_data[$rowIndex]['D'] = $cell->getValue();
        }
        
        if (!empty($array_data)) {
            foreach ($array_data as $key => $val) {
                $name = $val['A'];
                $email = $val['B'];
                $mobile = $val['C'];
                $corp = $val['D'];
                
                if ($key > 1 && !empty($name)) {
                    $query_body .= ", (
    						NULL,
    						'$name',
    						'$email',
    						'$mobile',
    						'$corp',
    						'$group_id'
    					)
    				";
                }
            } // foreach
        }
    } else if (!strcmp($file_ext, 'xlsx')) {
        require_once INC_PATH . '/classes/SimpleXLSX.php';
        
        $xlsx = new SimpleXLSX($upload_file);
        $array_data = $xlsx->rows();
        
        if (!empty($array_data)) {
            foreach ($array_data as $key => $val) {
                $name = $val[0];
                $email = $val[1];
                $mobile = $val[2];
                $corp = $val[3];
                
                if ($key > 0 && !empty($name)) {
                    $query_body .= ",(
    						NULL,
    						'$name',
    						'$email',
    						'$mobile',
    						'$corp',
    						'$group_id'
    					)
    				";
                }
            } // foreach
        }
    }
    
    $query_head = "
			INSERT INTO
				$if_table1
				(
					contact_id,
					contact_name,
					contact_email,
					contact_mobile,
					org_name,
					group_id
				)
			VALUES
	";
    if (!empty($query_body)) {
        $query = $query_head . ltrim($query_body, ',');
        $stmt = $ifdb->prepare($query);
        $stmt->execute();
        $affected_rows = $ifdb->affected_rows;
    }
    return $affected_rows;
}


?>