<?php
/*
 * Desc : 암호화
 */
function if_encrypt_passwd($plain_text) {
    return password_hash($plain_text, PASSWORD_DEFAULT);
}

function if_verify_passwd($plain_text, $hash) {
    return password_verify($plain_text, $hash);
}

/*
 * Desc : Admin
 */
function if_is_admin() {
    if (!empty($_SESSION['cms'] )) {
        return true;
    } else {
        return false;
    }
}

function if_get_current_admin_id() {
    if (!empty($_SESSION['cms'] )) {
        return $_SESSION['cms']['admin_id'];
    }
    return false;
}

function if_get_current_admin_login() {
    if (!empty($_SESSION['cms'] )) {
        return $_SESSION['cms']['admin_login'];
    }
    return false;
}

function if_get_current_admin_name() {
    if (!empty($_SESSION['cms'] )) {
        return $_SESSION['cms']['admin_name'];
    }
    return false;
}

function if_get_current_admin_level() {
    if (!empty($_SESSION['cms'] )) {
        return $_SESSION['cms']['admin_level'];
    }
    return false;
}

function if_authenticate_admin() {
    if (!if_is_admin()) {
        if_redirect(ADMIN_URL . '/login.php');
    }
}


/*
 * Desc : User
 *
 */
function if_get_current_user_id() {
    if (!empty($_SESSION['user'] )) {
        return $_SESSION['user']['sid'];
    }
    return 0;
}

function if_get_current_user_login() {
    if (!empty($_SESSION['user'] )) {
        return $_SESSION['user']['user_login'];
    }
    return '';
}

function if_get_current_user_name() {
    if (!empty($_SESSION['user'] )) {
        return $_SESSION['user']['user_name'];
    }
    return '';
}

function if_authenticate_user($redirect = NULL) {
    if (!if_get_current_user_id()) {
        if (empty($redirect)) {
            $redirect = base64_encode($_SERVER['REQUEST_URI']);
        }
        if_redirect(CONTENT_URL . '/member/login.php?redirect=' . $redirect );
    }
}

?>