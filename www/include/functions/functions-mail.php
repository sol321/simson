<?php
/*
 * Desc: 메일 전송
 */
function if_send_mail($to, $subject, $body, $from = NULL) {
	if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
		return false;
	} else {
		$charset = 'utf-8';
		$to_subject ='=?utf-8?B?' . base64_encode($subject) . '?=';
		$to_subject = iconv('utf-8', 'euc-kr', $to_subject);
		$mail_msg = base64_encode($body);
		if (empty($from)) {
			$from = json_decode(if_get_option('site_email_from'), true);
		}
		$return_path = json_decode(if_get_option('site_email_return'), true);
		if (empty($return_path)) {
		    $return_path = 'null@inforang.com';       // $from
		}
		
		$headers	= array();
		$headers[]	= 'MIME-Version: 1.0';
		$headers[]	= 'Content-type: text/html; charset=' . $charset;
		$headers[]	= 'Content-Transfer-Encoding: base64';
		$headers[]	= 'From: ' . $from;
		$headers[]	= 'Return-Path: ' . $return_path;
		$headers[]	= 'X-Mailer: PHP/' . phpversion();
		
		$mail_result = mail($to, $to_subject, $mail_msg, implode("\r\n", $headers));
		return $mail_result;
	}
}

/*
 * Desc: 메일 내용 설정. 메일 헤더, 푸터 포함
 */
function if_get_default_mail_template($content) {
	
	$mail_img_top = INC_URL . '/img/mail/mail_top.jpg';
	$mail_img_bottom = INC_URL . '/img/mail/mail_bottom.jpg';
	
	$tmpl =<<<EOD

<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td>
				<img src="$mail_img_top" width="750" height="100" alt="인포랑학회. 국민의 건강을 지키는 것이 사명이라는 마음가짐으로 여러분의 의견을 수렴하겠습니다.">
			</td>
		</tr>
		<tr>
			<td style="padding-top: 20px;padding-bottom: 20px;border-left: 1px solid #ccc;border-right: 1px solid #ccc;">
				<div class="mail-con" style="padding: 20px;color: #333;">
					$content
				</div>
			</td>		
		</tr>
		<tr>
			<td>
				<img src="$mail_img_bottom" width="750" height="100" alt="서울특별시 광진구 광나루로 56길 85, 테크노-마트21 31층 (우05116) / FAX : 02-325-7667 출판/회원관리 : 02-337-3337 / E-Mail : info@inforang.com Copyright ⓒ 2011 by KMEDrang.INFOrang LTD.">
			</td>
		</tr>
	</tbody>
</table>

EOD;
	
	return $tmpl;
}

/*
 * Desc: 메일 내용 등록
 */
function if_add_mail_content() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$conference_id = $_POST['conference_id'];
	$sender_email = $_POST['sender_email'];
	$receiver_email = $_POST['receiver_email'];
	$mail_subject = $_POST['mail_subject'];
	$mail_content = $_POST['mail_content'];
	$group_id = mt_rand(100000000, 999999999); // 동보 발송시 그룹 번호
	
	$file_attachment = [];
	$download_link = '';	// 첨부파일의 경우 서버 업로드 후, 링크를 생성한다.
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		$ym = date('Ym');
		$upload_dir = UPLOAD_PATH . '/mail/' . $ym . '/' . $group_id;
		$upload_url = UPLOAD_URL . '/mail/' . $ym . '/' . $group_id;
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			$basename = basename($val);
			$atc_val['file_path'] = $upload_dir . '/' . $basename;
			$atc_val['file_url'] = $upload_url . '/' . $basename;
			$atc_val['file_name'] = $_POST['file_name'][$key];
			$result = rename($val, $atc_val['file_path']);
			if (!empty($result)) {
				array_push($file_attachment, $atc_val);
				// download link
				$download_link .= '<br><a href="' . $atc_val['file_url'] . '" target="_blank">' . $atc_val['file_name'] . '</a><br>';
			}
		}
	}
	
	if (!empty($download_link)) {
		$mail_content .= $download_link;
	}
	
	$json = json_encode(compact('file_attachment'));
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					mail_subject,
					mail_content,
					create_dt,
					show_hide,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, NOW(), 'show', 
                    ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $mail_subject, $mail_content, $json);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_mail_content_by_id($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

/*
 * Desc: 초록 접수자에게 메일 발송
 */
function if_send_mail_abstract_attendees($cid, $mail_id, $stage_group) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_log'];
	
	// 메일 내용 조회
	$m_row = if_get_mail_content_by_id($mail_id);
	$mail_subject = $m_row['mail_subject'];
	$mail_content = $m_row['mail_content'];
	$group_id = mt_rand(100000000, 999999999); // 동보 발송시 그룹 번호
	
	$from = $_POST['sender_email'];
	
	// 받는 그룹
	$receiver_group = if_get_abstract_attendees_by_adoption($cid, $stage_group);
	
	$query = "
			INSERT INTO
				$if_table1			
				(
					log_id,
					content_id,
					group_id,
					rcv_email,
					rcv_name,
					show_hide,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					'show', ?
				)
	";
	$stmt = $ifdb->prepare($query);
	
	foreach ($receiver_group as $key => $val) {
		$rcv_email = $val['author_email'];
		$rcv_name = $val['author_name'];
		
		$body = str_replace(array('{:name:}'), array($rcv_name), $mail_content);
		$subject = str_replace(array('{:name:}'), array($rcv_name), $mail_subject);
		
		$meta_data = json_encode(compact('body', 'from', 'subject'));
		
		$stmt->bind_param('issss', $mail_id, $group_id, $rcv_email, $rcv_name, $meta_data);
		$stmt->execute();
		$seq_id = $stmt->insert_id;
	}
	return $seq_id;
}

/*
 * Desc: 테스트 메일 발송  (sendmail 발송)
 */
function if_send_mail_abstract_test($cid, $mail_id, $to_email) {
	// 메일 내용 조회
	$m_row = if_get_mail_content_by_id($mail_id);
	$mail_subject = $m_row['mail_subject'];
	$mail_content = $m_row['mail_content'];
	
	$rcv_name = '테스트';
	
	$from = $_POST['sender_email'];
	$email_body = str_replace(array('{:name:}'), array($rcv_name), $mail_content);
	$email_subject = str_replace(array('{:name:}'), array($rcv_name), $mail_subject);
	
	return if_send_mail($to_email, $email_subject, $email_body, $from);
	
}

// Mail Svr -------------------------
/*
 * Desc: 메일 컨텐트 등록
 */
function if_add_mailing_content() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$mail_subject = $_POST['mail_subject'];
	$mail_content = $_POST['mail_content'];
	
	$file_attachment = [];
	
	// CKEditor 에서 사용한 이미지
	$ck_images = [];
	preg_match_all('/(src)=("[^"]*")/i', $mail_content, $ck_images, PREG_SET_ORDER);
	
	if (!empty($ck_images)) {
		$images_tmp = [];		// Temporary directory path
		
		foreach ($ck_images as $key => $val) {
			if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false) {
				$images_tmp[] = $val[2];
			}
		}
		
		if (!empty($images_tmp)) {
			$ym = date('Ym');
			$tmp_mark   = '/tmp';
			$ck_mark	= '/mail/' . $ym;
			
			$new_dir_path = UPLOAD_PATH . $ck_mark;	// 디렉토리 생성을 위해
			
			if (!is_dir($new_dir_path)) {
				mkdir($new_dir_path, 0777, true);
			}
			foreach ($images_tmp as $key => $val) {
				$img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
				$img_new_url = str_replace($tmp_mark, $ck_mark, $img_url);  // format: /upload/popup/201903/1553048848_71241100_1082390327.jpg
				// 파일 이동을 위해 실제 물리 경로를 만든다.
				$real_tmp_path = ABSOLUTE_PATH . $img_url;
				$real_new_path = ABSOLUTE_PATH . $img_new_url;
				if (@rename($real_tmp_path, $real_new_path)) {
					$mail_content = str_replace($img_url, $img_new_url, $mail_content);
				}
			}
		}
	}
	//-- CKEditor
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		$ym = date('Ym');
		$upload_dir = UPLOAD_PATH . '/mail/' . $ym;
		$upload_url = UPLOAD_URL . '/mail/' . $ym;
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			$basename = basename($val);
			$atc_val['file_path'] = $upload_dir . '/' . $basename;
			$atc_val['file_url'] = $upload_url . '/' . $basename;
			$atc_val['file_name'] = $_POST['file_name'][$key];
			$result = rename($val, $atc_val['file_path']);
			if (!empty($result)) {
				array_push($file_attachment, $atc_val);
			}
		}
	}
	
	$json = json_encode(compact('file_attachment'));
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					mail_subject,
					mail_content,
					create_dt,
					show_hide,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, NOW(), 'show',
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $mail_subject, $mail_content, $json);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

/*
 * Desc: 메일 컨텐트 조회
 */
function if_get_mailing_content_by_id($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

/*
 * Desc: 메일 컨텐트 감추기
 */
function if_update_mailing_content($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$mail_subject = $_POST['mail_subject'];
	$mail_content = $_POST['mail_content'];
	
	$file_attachment = [];
	
	// CKEditor 에서 사용한 이미지
	$ck_images = [];
	preg_match_all('/(src)=("[^"]*")/i', $mail_content, $ck_images, PREG_SET_ORDER);
	
	if (!empty($ck_images)) {
		$images_tmp = [];		// Temporary directory path
		
		foreach ($ck_images as $key => $val) {
			if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false) {
				$images_tmp[] = $val[2];
			}
		}
		
		if (!empty($images_tmp)) {
			$ym = date('Ym');
			$tmp_mark   = '/tmp';
			$ck_mark	= '/mail/' . $ym;
			
			$new_dir_path = UPLOAD_PATH . $ck_mark;	// 디렉토리 생성을 위해
			
			if (!is_dir($new_dir_path)) {
				mkdir($new_dir_path, 0777, true);
			}
			foreach ($images_tmp as $key => $val) {
				$img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
				$img_new_url = str_replace($tmp_mark, $ck_mark, $img_url);  // format: /upload/popup/201903/1553048848_71241100_1082390327.jpg
				// 파일 이동을 위해 실제 물리 경로를 만든다.
				$real_tmp_path = ABSOLUTE_PATH . $img_url;
				$real_new_path = ABSOLUTE_PATH . $img_new_url;
				if (@rename($real_tmp_path, $real_new_path)) {
					$mail_content = str_replace($img_url, $img_new_url, $mail_content);
				}
			}
		}
	}
	//-- CKEditor
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		$ym = date('Ym');
		$upload_dir = UPLOAD_PATH . '/mail/' . $ym;
		$upload_url = UPLOAD_URL . '/mail/' . $ym;
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			// 기존 파일
			if (stripos($val, '/tmp/') === false) {
				$atc_val['file_path'] = $val;
				$atc_val['file_url'] = $_POST['file_url'][$key];
				$atc_val['file_name'] = $_POST['file_name'][$key];
				array_push($file_attachment, $atc_val);
			} else {	// 신규 업로드 파일
				$basename = basename($val);
				$atc_val['file_path'] = $upload_dir . '/' . $basename;
				$atc_val['file_url'] = $upload_url . '/' . $basename;
				$atc_val['file_name'] = $_POST['file_name'][$key];
				$result = rename($val, $atc_val['file_path']);
				if (!empty($result)) {
					array_push($file_attachment, $atc_val);
				}
			}
		}
	}
	
	$json = json_encode(compact('file_attachment'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				mail_subject = ?,
				mail_content = ?,
				meta_data = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssi', $mail_subject, $mail_content, $json, $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 메일 컨텐트 감춤
 */
function if_hide_mailing_content($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_content'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				show_hide = 'hide'
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 메일 컨텐트 삭제
 */
function if_delete_mailing_content($id) {
	
}

function if_get_mail_qry($id) {}


/*
 * Desc: 메일 서버 경유 발송
 */
function if_send_mail_by_svr($to_emails) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_log'];
	
	$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
	$fqdn = $_SERVER['HTTP_HOST'];
	$site_url = $protocol . $fqdn;
	
	$group_id = mt_rand(100000000, 999999999); // 동보 발송시 그룹 번호
	$content_id = $_POST['content_id'];	 // 메일 컨텐트 ID
	$mail_subject = $_POST['mail_subject'];
	$from = $_POST['sender_email'];
	$success = 0;   // 메일 발송 성고 건수 (서버에 등록 건수)
	
	// 메일 본문
	$mail_row = if_get_mailing_content_by_id($content_id);
	
	$mail_body = $mail_row['mail_content'];
	// 첨부파일
	$meta_data = $mail_row['meta_data'];
	$jdec = json_decode($meta_data, true);
	$file_attachment = $jdec['file_attachment'];
	$attach = '';
	
	if (!empty($file_attachment)) {
		foreach ($file_attachment as $key => $val) {
			$file_path = $val['file_path'];
			$file_name = $val['file_name'];
			if (is_file($file_path)) {
				$attach .= '<a href="' . ADMIN_INC_URL .'/lib/download_filepath.php?fp=' . base64_encode($file_path) . '&fn=' . base64_encode($file_name) .'" style="padding: 5px; background-color: #00c0ef; border-radius: 3px; color: #fff;">' . $file_name . '</a> &nbsp;';
			}
		}
	}
	$mail_body .= '<p>' . $attach . '</p>';	 // 본문에 첨부파일 정보 추가함.
	
	// 메일 서버 정보
	$mail_svr = json_decode(if_get_option('if_mail_svr'), true);
	
	$mail_host = @$mail_svr['mail_host'];
	$mail_userid = @$mail_svr['mail_userid'];
	$mail_passwd = @$mail_svr['mail_passwd'];
	$mail_db_name = @$mail_svr['mail_db_name'];
	$hcode = @$mail_svr['hcode'];
	$fname = json_decode(if_get_option('site_name'), true);
	
	//$sent_cnt = count($to_emails);
	$sent_cnt = 0;
	
	// Mail DB : mail_trunk 저장
	$mysqli = new mysqli($mail_host, $mail_userid, $mail_passwd, $mail_db_name);
//	 $mysqli = new mysqli("mailing2.inforang.com", "mailing", "info44813", "mailing_DB");

	//테스트 메일발송은 10분전 시간으로 등록처리
	$reg_dt = date('Y-m-d H:i:s');
	if(!empty($_POST['test_email_address'])) {
	    $reg_dt = date('Y-m-d H:i:s',strtotime("-10 minutes"));
	} 
	$query = "
			INSERT INTO
				mail_trunk
				(
					num,
					HCode,
					mail_num,
					mail_uid,
					sum_cnt,
					err_cnt,
					addr_cnt,
					send_cnt,
					process,
					dataChk,
					reg_dt,
					mod_dt,
					encodingType,
					logTableName,
					send_dt,
					end_time,
					php_logs
				)
			VALUES
				(
					NULL, '$hcode', '$content_id', '$group_id', '$sent_cnt',
					'0', '0', '$sent_cnt', 'N', 'Y',
					'$reg_dt', '0000-00-00 00:00:00', 'base64', '$if_table1', '0000-00-00 00:00:00',
					0, ''
				)
	";
	if ($mysqli->query($query)) {
		$trunk_id = $mysqli->insert_id;
	} else {
		return false;
	}

	// Local mail log Table, Prepared statement
	$query = "
			INSERT INTO
				$if_table1
				(
					log_id,
					content_id,
					group_id,
					rcv_email,
					rcv_name,
					create_dt,
					update_dt,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					NOW(), NOW(), ?
				)
	";
	$stmt = $ifdb->prepare($query);
				
	// Mail DB: mail_send, Prepared statement
	$rquery = "
			INSERT INTO
				mail_send
				(
					num,
					trunk_num,
					HCode,
					mid,
					kname,
					email,
					cc_mail,
					subject,
					body,
					fname,
					femail,
					return_email,
					reply_email,
					reg_dt
				)
			VALUES
				(
					NULL, '$trunk_id', '$hcode', ?, ?,
					?, '', ?, ?, ?,
					?, 'null@inforang.com', 'null@inforang.com', NOW()
				)
	";
	$rstmt = $mysqli->prepare($rquery);
	
// 	var_dump($to_emails); exit;
				
	foreach ($to_emails as $key => $val) {
		$email = $val['to_email'];
		$name = $val['to_name'];
		$user_login = $val['to_login'];
		
		$subject = str_replace('{:name:}', $name, $mail_subject);
		$body = str_replace('{:name:}', $name, $mail_body);
		$body = str_replace('{:login:}', $user_login, $body);
		$body = preg_replace('/\t+/', '', $body);  // tab 제거
		
		$compact = compact('from', 'subject', 'body');
		$json = json_encode($compact);
		
		$stmt->bind_param('iisss', $content_id, $group_id, $email, $name, $json);
		$stmt->execute();
		$log_id = $ifdb->insert_id;
		
		if ($log_id) {
			$success++;
			$mail_confirm = '<div style="display:none;"><img src="' . $site_url . '/admin/content/email/check_read_mail.php?log_id=' . $log_id . '"></div>';
			$body .= $mail_confirm;
			
			$rstmt->bind_param('sssssss', $log_id, $name, $email, $subject, $body, $fname, $from);
			$rstmt->execute();
		}
		
		// Test send mail
// 		if_send_mail($email, $subject, $body);
	}
	
	return $success;
}

/*
 * Desc: 메일 수신 확인
 */
function if_check_read_mail($log_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mail_log'];
	
	$query = "
			UPDATE 
				$if_table1
			SET 
				rec_dt = NOW()
			WHERE
				log_id = ? AND
				rec_dt IS NULL
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $log_id);
	$result = $stmt->execute();   
	return $result;
}


?>
