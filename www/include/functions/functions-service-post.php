<?php
/*
 * Desc: Popup 등록
 */
function if_add_popup_window() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $post_title     = empty($_POST['post_title']) ? '' : trim($_POST['post_title']);
    $popup_width    = $_POST['popup_width'];
    $popup_height   = $_POST['popup_height'];
    $popup_top      = empty($_POST['popup_top']) ? 0 : $_POST['popup_top'];
    $popup_left     = empty($_POST['popup_left']) ? 0 : $_POST['popup_left'];
    $period_from    = empty($_POST['period_from']) ? '' : trim($_POST['period_from']);
    $period_to      = empty($_POST['period_to']) ? '' : trim($_POST['period_to']);
    $post_state     = empty($_POST['post_state']) ? 'open' : trim($_POST['post_state']);
    $post_content   = $_POST['post_content'];
    
    $post_order     = 0;
    $post_type      = 'popup_new';
    $post_type_secondary = '';
    
    $compact = compact('popup_width', 'popup_height', 'popup_top', 'popup_left');
    $meta_data = json_encode($compact);
    
    // CKEditor 에서 사용한 이미지
    $ckeditor_images = '';
    $ck_images = [];
    preg_match_all('/(src)=("[^"]*")/i', $post_content, $ck_images, PREG_SET_ORDER);
    
    if (!empty($ck_images)) {
        $images_tmp = [];		// Temporary directory path
        $images_real = [];		// Real directory path
        
        foreach ($ck_images as $key => $val) {
            if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false) {
                $images_tmp[] = $val[2];
            }
        }
        
        if (!empty($images_tmp)) {
            $ym = date('Ym');
            $tmp_mark   = '/tmp';
            $ck_mark    = '/popup/' . $ym;
            
            $new_dir_path = UPLOAD_PATH . $ck_mark;    // 디렉토리 생성을 위해
            
            if (!is_dir($new_dir_path)) {
                mkdir($new_dir_path, 0777, true);
            }
            foreach ($images_tmp as $key => $val) {
                $img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
                $img_new_url = str_replace($tmp_mark, $ck_mark, $img_url);  // format: /upload/popup/201903/1553048848_71241100_1082390327.jpg
                // 파일 이동을 위해 실제 물리 경로를 만든다.
                $real_tmp_path = ABSOLUTE_PATH . $img_url;
                $real_new_path = ABSOLUTE_PATH . $img_new_url;
                if (@rename($real_tmp_path, $real_new_path)) {
                    $post_content = str_replace($img_url, $img_new_url, $post_content);
                    $images_real[] = $img_new_url;
                }
            }
        }
        if (!empty($images_real)) {
            $ckeditor_images = $images_real;
        }
    }
    //-- CKEditor
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
                    post_title,
                    post_content,
                    post_order,
                    post_type,
                    post_type_secondary,
                    post_state,
                    meta_data,
                    period_from,
                    period_to,
                    create_dt
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, ?,
					NOW()
				)
	";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('sssssssss', $post_title, $post_content, $post_order, $post_type,
        $post_type_secondary, $post_state, $meta_data, $period_from, $period_to);
    $stmt->execute();
    $seq_id = $ifdb->insert_id;
    return $seq_id;
}

function if_get_popup_window($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

function if_update_popup_window($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $post_title     = empty($_POST['post_title']) ? '' : trim($_POST['post_title']);
    $popup_width    = $_POST['popup_width'];
    $popup_height   = $_POST['popup_height'];
    $popup_top      = empty($_POST['popup_top']) ? 0 : $_POST['popup_top'];
    $popup_left     = empty($_POST['popup_left']) ? 0 : $_POST['popup_left'];
    $period_from    = empty($_POST['period_from']) ? '' : trim($_POST['period_from']);
    $period_to      = empty($_POST['period_to']) ? '' : trim($_POST['period_to']);
    $post_state     = empty($_POST['post_state']) ? 'open' : trim($_POST['post_state']);
    $post_content   = $_POST['post_content'];
    
    $post_order     = 0;
    $post_type      = 'popup_new';
    $post_type_secondary = '';
    
    $compact = compact('popup_width', 'popup_height', 'popup_top', 'popup_left');
    $meta_data = json_encode($compact);
    
    // CKEditor 에서 사용한 이미지
    $ckeditor_images = '';
    $ck_images = [];
    preg_match_all('/(src)=("[^"]*")/i', $post_content, $ck_images, PREG_SET_ORDER);
    
    if (!empty($ck_images)) {
        $images_tmp = [];		// Temporary directory path
        $images_real = [];		// Real directory path
        
        foreach ($ck_images as $key => $val) {
            if (stripos($val[2], UPLOAD_ABS_URL . '/tmp') !== false) {
                $images_tmp[] = $val[2];
            }
        }
        
        if (!empty($images_tmp)) {
            $ym = date('Ym');
            $tmp_mark   = '/tmp';
            $ck_mark    = '/popup/' . $ym;
            
            $new_dir_path = UPLOAD_PATH . $ck_mark;    // 디렉토리 생성을 위해
            
            if (!is_dir($new_dir_path)) {
                mkdir($new_dir_path, 0777, true);
            }
            foreach ($images_tmp as $key => $val) {
                $img_url = str_replace('"', '', $val);	// 큰 따옴표를 제거합니다. format: /upload/tmp/1553048848_71241100_1082390327.jpg
                $img_new_url = str_replace($tmp_mark, $ck_mark, $img_url);  // format: /upload/popup/201903/1553048848_71241100_1082390327.jpg
                // 파일 이동을 위해 실제 물리 경로를 만든다.
                $real_tmp_path = ABSOLUTE_PATH . $img_url;
                $real_new_path = ABSOLUTE_PATH . $img_new_url;
                if (@rename($real_tmp_path, $real_new_path)) {
                    $post_content = str_replace($img_url, $img_new_url, $post_content);
                    $images_real[] = $img_new_url;
                }
            }
        }
        if (!empty($images_real)) {
            $ckeditor_images = $images_real;
        }
    }
    //-- CKEditor
    
    $query = "
			UPDATE
				$if_table1
		    SET
                post_title = ?,
                post_content = ?,
                post_state = ?,
                meta_data = ?,
                period_from = ?,
                period_to = ?
			WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssssi', $post_title, $post_content, $post_state, $meta_data, $period_from, $period_to, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_popup_window($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $query = "
            DELETE FROM
                $if_table1
            WHERE
                seq_id = ? AND
                post_type = 'popup_new'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 오늘 노출이 허용된 팝업 조회
 */
function if_get_live_popup_windows() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];

    $ctime = date('Y-m-d H:i:s');
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                post_state = 'open' AND
                post_type = 'popup_new' AND
                '$ctime' BETWEEN period_from AND period_to
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

/*
 * Desc: 배너 등록
 */
function if_add_banner() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $post_title     = empty($_POST['post_title']) ? '' : trim($_POST['post_title']);
    $banner_url     = empty($_POST['banner_url']) ? '#' : $_POST['banner_url'];
    $period_from    = empty($_POST['period_from']) ? '' : trim($_POST['period_from']);
    $period_to      = empty($_POST['period_to']) ? '' : trim($_POST['period_to']);
    $post_state     = empty($_POST['post_state']) ? 'open' : trim($_POST['post_state']);
    $post_content   = $banner_url;  // 검색용
    $post_order     = 0;
    $post_type      = 'banner_new';
    $post_type_secondary = empty($_POST['post_type_secondary']) ? 'main' : $_POST['post_type_secondary'];
    
    $file_attachment = [];
    
    // 배너 이미지
    if (!empty($_POST['file_path'])) {
        $upload_dir = UPLOAD_PATH . '/banner';
        $upload_url = UPLOAD_URL . '/banner';
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            $basename = basename($val);
            $atc_val['file_path'] = $upload_dir . '/' . $basename;
            $atc_val['file_url'] = $upload_url . '/' . $basename;
            $atc_val['file_name'] = $_POST['file_name'][$key];
            $result = rename($val, $atc_val['file_path']);
            if (!empty($result)) {
                array_push($file_attachment, $atc_val);
            }
        }
    }
    
    $compact = compact('banner_url', 'file_attachment');
    $meta_data = json_encode($compact);
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
                    post_title,
                    post_content,
                    post_order,
                    post_type,
                    post_type_secondary,
                    post_state,
                    meta_data,
                    period_from,
                    period_to,
                    create_dt
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, ?,
					NOW()
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssssss', $post_title, $post_content, $post_order, $post_type,
	    $post_type_secondary, $post_state, $meta_data, $period_from, $period_to);
	$stmt->execute();
	$seq_id = $ifdb->insert_id;
	return $seq_id;
}

function if_get_banner_by_id($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_update_banner($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $post_title     = empty($_POST['post_title']) ? '' : trim($_POST['post_title']);
    $banner_url     = empty($_POST['banner_url']) ? '#' : $_POST['banner_url'];
    $period_from    = empty($_POST['period_from']) ? '' : trim($_POST['period_from']);
    $period_to      = empty($_POST['period_to']) ? '' : trim($_POST['period_to']);
    $post_state     = empty($_POST['post_state']) ? 'open' : trim($_POST['post_state']);
    $post_content   = $banner_url;  // 검색용
    
    $file_attachment = [];
    
    // 배너 이미지
    if (!empty($_POST['file_path'])) {
        $upload_dir = UPLOAD_PATH . '/banner';
        $upload_url = UPLOAD_URL . '/banner';
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) {
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['file_url'][$key];
                $atc_val['file_name'] = $_POST['file_name'][$key];
                array_push($file_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['file_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($file_attachment, $atc_val);
                }
            }
        }
    }
    
    $compact = compact('banner_url', 'file_attachment');
    $meta_data = json_encode($compact);
    
    $query = "
			UPDATE
				$if_table1
			SET
                post_title = ?,
                post_content = ?,
                post_state = ?,
                meta_data = ?,
                period_from = ?,
                period_to = ?
			WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssssi', $post_title, $post_content, $post_state, $meta_data, $period_from, $period_to, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_banner($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $query = "
            DELETE FROM
                $if_table1
            WHERE
                seq_id = ? AND
                post_type = 'banner_new'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc : 배너 순서 재정렬
 */
function if_reorder_banner() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $ids = $_POST['banner_id'];
    
    $query = "
		UPDATE
			$if_table1
		SET
			post_order = ?
		WHERE
			seq_id = ?
	";
    $stmt = $ifdb->prepare($query);

    foreach ($ids as $key => $val) {
        $stmt->bind_param('ii', $key, $val);
        $stmt->execute();
    }
    return 1;
}

/*
 * Desc: 오늘 노출이 허용된 팝업 띄우기
 */
function if_get_live_popup_main() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $ctime = date('Y-m-d H:i:s');
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                post_state = 'open' AND
                post_type = 'popup_new' AND
                '$ctime' BETWEEN period_from AND period_to
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    $skin_dir = ABSOLUTE_PATH.'/skin/popup';
    require_once $skin_dir.'/popup_skin.php';
}

/*
 * Desc: 노출이 허용된 배너 띄우기
 */
function if_get_live_banner_main() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_service_posts'];
    
    $ctime = date('Y-m-d H:i:s');
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                post_state = 'open' AND
                post_type = 'banner_new' AND
                '$ctime' BETWEEN period_from AND period_to
            ORDER BY post_order desc
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

?>