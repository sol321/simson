<?php
function if_add_journal() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    
    $jn_type = $_POST['jt'];
    $jn_year_month = explode('-', $_POST['jn_year_month']);
    $jn_year = $jn_year_month[0];
    $jn_month = $jn_year_month[1];
    
    $jn_vol = empty($_POST['jn_vol']) ? '' : trim($_POST['jn_vol']);
    $jn_no = empty($_POST['jn_no']) ? '' : trim($_POST['jn_no']);
    $page_from = empty($_POST['page_from']) ? '' : trim($_POST['page_from']);
    $page_to = empty($_POST['page_to']) ? '' : trim($_POST['page_to']);
    $author = empty($_POST['author']) ? '' : trim($_POST['author']);
    $j_title = empty($_POST['j_title']) ? '' : trim($_POST['j_title']);
    $j_keyword = empty($_POST['j_keyword']) ? '' : trim($_POST['j_keyword']);
    $j_abstract = empty($_POST['j_abstract']) ? '' : trim($_POST['j_abstract']);
    
    $file_attachment = [];
    
    // 첨부파일
    if (!empty($_POST['file_path'])) {
        $upload_dir = UPLOAD_PATH . '/journal/' . $jn_vol;
        $upload_url = UPLOAD_URL . '/journal/' . $jn_vol;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            $basename = basename($val);
            $atc_val['file_path'] = $upload_dir . '/' . $basename;
            $atc_val['file_url'] = $upload_url . '/' . $basename;
            $atc_val['file_name'] = $_POST['file_name'][$key];
            $result = rename($val, $atc_val['file_path']);
            if (!empty($result)) {
                array_push($file_attachment, $atc_val);
            }
        }
    }
    
    $compact = compact('page_from', 'page_to', 'file_attachment');
    $meta_data = json_encode($compact);
    
    $query = "
			INSERT INTO
				$if_table1
				(
                    seq_id,
                    jn_vol,
                    jn_no,
                    jn_year,
                    jn_month,
                    jn_type,
                    author,
                    j_title,
                    j_keyword,
                    j_abstract,
                    create_dt,
                    meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ?, ?, ?,
                    NOW(), ?
				)
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssssssss', $jn_vol, $jn_no, $jn_year, $jn_month, $jn_type, $author, $j_title, $j_keyword, $j_abstract, $meta_data);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_journal_by_id($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

function if_update_journal($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    
    $jn_type = $_POST['jt'];
    $jn_year_month = explode('-', $_POST['jn_year_month']);
    $jn_year = $jn_year_month[0];
    $jn_month = $jn_year_month[1];
    
    $jn_vol = empty($_POST['jn_vol']) ? '' : trim($_POST['jn_vol']);
    $jn_no = empty($_POST['jn_no']) ? '' : trim($_POST['jn_no']);
    $page_from = empty($_POST['page_from']) ? '' : trim($_POST['page_from']);
    $page_to = empty($_POST['page_to']) ? '' : trim($_POST['page_to']);
    $author = empty($_POST['author']) ? '' : trim($_POST['author']);
    $j_title = empty($_POST['j_title']) ? '' : trim($_POST['j_title']);
    $j_keyword = empty($_POST['j_keyword']) ? '' : trim($_POST['j_keyword']);
    $j_abstract = empty($_POST['j_abstract']) ? '' : trim($_POST['j_abstract']);
    
    $update_dt = date('Y-m-d H:i:s');
    $file_attachment = [];
    
    // 첨부파일
    if (!empty($_POST['file_path'])) {
        $upload_dir = UPLOAD_PATH . '/journal/' . $jn_vol;
        $upload_url = UPLOAD_URL . '/journal/' . $jn_vol;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) {
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['file_url'][$key];
                $atc_val['file_name'] = $_POST['file_name'][$key];
                array_push($file_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['file_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($file_attachment, $atc_val);
                }
            }
        }
    }
    
    $compact = compact('page_from', 'page_to', 'file_attachment', 'update_dt');
    $meta_data = json_encode($compact);
    
    $query = "
			UPDATE
				$if_table1
			SET
                jn_vol = ?,
                jn_no = ?,
                jn_year = ?,
                jn_month = ?,
                jn_type = ?,
                author = ?,
                j_title = ?,
                j_keyword = ?,
                j_abstract = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
                seq_id = ?
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssssssssi', 
	    $jn_vol, $jn_no, $jn_year, $jn_month, $jn_type, 
	    $author, $j_title, $j_keyword, $j_abstract, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_journal($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    
    $query = "
            DELETE FROM
                $if_table1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $result = $stmt->execute();
    return $result;
}

/*
 *  년도별 권 정보
 */
function if_get_journal_list_by_year($jn_type) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    $query = "
    	SELECT
            concat(jn_year,'년 ', jn_vol, '권') as journal_list, jn_year, jn_vol
        FROM (
            SELECT
        		jn_year, jn_vol
        	FROM
        		$if_table1
        	WHERE
        		jn_type = ?
        	ORDER BY
        		jn_year DESC,
                jn_vol DESC,
                jn_no ASC
        ) journal
        GROUP BY jn_year, jn_vol
        ORDER BY jn_year DESC, jn_vol
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $jn_type);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	
	return $result;
}
/*
 * 년도별 호 정보
 */
function if_get_journal_list_by_year_vol($jn_type, $jn_year, $jn_vol) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal'];
    $query = "
        SELECT
    		jn_no
    	FROM
    		$if_table1
    	WHERE
    		jn_type = ?
            AND jn_year = ?
            AND jn_vol = ?
        GROUP BY jn_no
    	ORDER BY
            jn_no ASC
   
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $jn_type, $jn_year, $jn_vol);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}
?>