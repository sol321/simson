<?php
function if_redirect($url = NULL) {
    if (empty($url)) {
        $url = SITE_URL;
    }
    header('Location: ' . $url);
    exit;
}

function if_js_alert_back($msg) {
    echo <<<EOD
			<script>
			alert("$msg");
			history.back();
			</script>
EOD;
    exit;
}

function if_js_alert_move($msg, $url) {
    echo <<<EOD
			<script>
			alert("$msg");
			location.href = "$url";
			</script>
EOD;
    exit;
}

function if_js_alert_close($msg, $url) {
    echo <<<EOD
			<script>
			alert("$msg");
			self.close();
			</script>
EOD;
    exit;
}

/*
 * Desc : 난수 생성, ex) 1418891543_68353500_488364154
 */
function if_make_rand() {
    list($usec, $sec) = explode(' ', microtime());
    return $sec . '_' . substr($usec, 2) . '_' . mt_rand();
}

/*
 * Desc : 랜덤 String
 */
function if_make_random_str($length = 45) {
    $str = '';
    $chars = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
    $max = count($chars) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $chars[$rand];
    }
    return $str;
}

/*
 * Desc : 파일 확장자
 */
function if_get_file_extension($file) {
    return pathinfo($file, PATHINFO_EXTENSION);
}

/*
 * Desc : 숫자만 리턴
 */
function if_sanitize_numeric($data) {
    return preg_replace('/\D/', '', $data);
}

function if_record_log($log_msg, $log_file) {
    $log_dir = dirname($log_file);
    if (!is_dir($log_dir)) {
        mkdir($log_dir, 0777, true);
    }
    return file_put_contents($log_file, $log_msg, FILE_APPEND | LOCK_EX);
}

# 문자열 자르기
function if_cut_str($str, $size, $add = "...", $charset = "utf-8") {
    if (mb_strlen($str, $charset) > $size) {
        return mb_substr($str, 0, $size, $charset) . $add;
    } else {
        return $str;
    }
}

/*
 * Desc: 1st, 2nd, 3rd, Xth
 */
function if_get_ordinal_suffix($number) {
    $suffix = ['th', 'st', 'nd', 'rd'];
    
    $s = $number % 10;
    if ($s > 3) {
        $s = 0;
    }
    return $suffix[$s];
}

/*
 * Desc: 1024 -> 1KB
 */
function if_convert_bytes($size, $precision = 2) {
    $base = log($size, 1024);
    $suffixes = array('B', 'KB', 'MB', 'GB', 'TB');
    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

/*
 * Desc: 1K -> 1024,
 *      php.ini의 upload_max_filesize의 설정에서 byte값을 반환하기 위한 용도
 */
function if_return_bytes_from_ini($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    $number = intval($val);

    switch($last) {
        case 'g':
            $number *= 1024;
        case 'm':
            $number *= 1024;
        case 'k':
            $number *= 1024;
    }
    return $number;
}

/*
 * Desc: JSON형식의 값으로부터 해당 key에 해당하는 값을 반환 
 */
function if_get_val_from_json($json, $key) {
    $jdec = json_decode($json, TRUE);
    $val = $jdec[$key];
    return $val; 
}

/*
 * Desc: timestamp로 현재의 시간이 from ~ to에 있는 지 검사.
 *  true : from ~ to 기간내에 있음.
 *  false : 없음.
 */
function if_validate_period_by_time($from, $to) {
    $curtime = time();
    $time_from = is_int($from) ? $from : strtotime($from);
    $time_to = is_int($to) ? $to : strtotime($to);
    
    if ($curtime < $time_from || $curtime > $time_to) {
        return false;
    } else {
        return true;
    }
}

/*
 * Desc: 날짜 포멧 (한글 요일 포함)
 *      2019. 08. 26(월)
 */
function if_date_format_kor_day($date) {
    $time = strtotime($date);
    $kor_day = array('일', '월', '화', '수', '목', '금', '토');
    
    $fm_date = date('Y. m. d', $time);
    $fm_day = date('w', $time);
    
    return $fm_date . '(' . $kor_day[$fm_day] . ')';
}

/*
 * Desc: 유효한 휴대전화번호 포멧인지 검증
 */
function if_validate_mobile_number($str) {
    $number = if_sanitize_numeric($str);
    $len = strlen($number);
    
    if (substr($number, 0, 2) == '01' && $len > 9 && $len < 12) {
        return true;
    } else {
        return false;
    }
}
?>