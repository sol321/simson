<?php
/*
 * Desc: 행사 등록
 */
function if_add_event() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $event_type = $_POST['event_type'];
    $event_state = $_POST['event_state'];
    $event_name = $_POST['event_name'];
    $event_year = $_POST['event_year'];
    $event_content = $_POST['event_content'];
    $program_table = $_POST['program_table'];
    $event_period_from = $_POST['event_period_from'];
    $event_period_to = $_POST['event_period_to'];
    $pre_reg_content = $_POST['pre_reg_content'];
    $pre_reg_from = $_POST['pre_reg_from'];
    $pre_reg_to = $_POST['pre_reg_to'];
    $abstract_content = $_POST['abstract_content'];
    $abstract_from = $_POST['abstract_from'];
    $abstract_to = $_POST['abstract_to'];
    $event_place = $_POST['event_place'];
    $pre_reg_max_attendees = empty($_POST['pre_reg_max_attendees']) ? 0 : $_POST['pre_reg_max_attendees'];
    
    $fee_classification = $_POST['fee_classification'];     // 사전등록 비용, array
    $fee_amount = $_POST['fee_amount'];
    $user_class_fee = [];   // 각 회원 자격별 사전등록 비용
    
    foreach ($fee_classification as $key => $val) {
        $user_class_fee[$val] = $fee_amount[$key];
    }
    
    // 초록 분류명, array
    $abstract_classification = empty($_POST['abstract_classification']) ? '' : $_POST['abstract_classification'];

    $compact = compact('user_class_fee', 'event_content', 'program_table', 'pre_reg_content', 'abstract_content', 'pre_reg_max_attendees', 'abstract_classification');
    $json = json_encode($compact);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    seq_id,
                    event_name,
                    event_type,
                    event_state,
                    event_year,
                    event_period_from,
                    event_period_to,
                    pre_reg_from,
                    pre_reg_to,
                    abstract_from,
                    abstract_to,
                    event_place,
                    show_hide,
                    create_dt,
                    meta_data
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, 'show', NOW(), ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssssssss', $event_name, $event_type, $event_state, $event_year,
        $event_period_from, $event_period_to, $pre_reg_from, $pre_reg_to, $abstract_from, $abstract_to,
        $event_place, $json
    );
    
    $stmt->execute();
    $seq_id = $stmt->insert_id;
    return $seq_id;
}

function if_get_event($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

function if_update_event() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $seq_id = $_POST['seq_id'];
    $event_type = $_POST['event_type'];
    $event_state = $_POST['event_state'];
    $event_name = $_POST['event_name'];
    $event_year = $_POST['event_year'];
    $event_content = $_POST['event_content'];
    $program_table = $_POST['program_table'];
    $event_period_from = $_POST['event_period_from'];
    $event_period_to = $_POST['event_period_to'];
    $pre_reg_content = $_POST['pre_reg_content'];
    $pre_reg_from = $_POST['pre_reg_from'];
    $pre_reg_to = $_POST['pre_reg_to'];
    $abstract_content = $_POST['abstract_content'];
    $abstract_from = $_POST['abstract_from'];
    $abstract_to = $_POST['abstract_to'];
    $event_place = $_POST['event_place'];
    $pre_reg_max_attendees = empty($_POST['pre_reg_max_attendees']) ? 0 : $_POST['pre_reg_max_attendees'];
    
    $fee_classification = $_POST['fee_classification'];     // 사전등록 비용, array
    $fee_amount = $_POST['fee_amount'];
    $user_class_fee = [];   // 각 회원 자격별 사전등록 비용
    
    foreach ($fee_classification as $key => $val) {
        $user_class_fee[$val] = $fee_amount[$key];
    }
    
    // 초록 분류명, array
    $abstract_classification = empty($_POST['abstract_classification']) ? '' : $_POST['abstract_classification'];
    
    $compact = compact('user_class_fee', 'event_content', 'program_table', 'pre_reg_content', 'abstract_content', 'pre_reg_max_attendees', 'abstract_classification');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                event_name = ?,
                event_type = ?,
                event_state = ?,
                event_year = ?,
                event_period_from = ?,
                event_period_to = ?,
                pre_reg_from = ?,
                pre_reg_to = ?,
                abstract_from = ?,
                abstract_to = ?,
                event_place = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssssssssi', $event_name, $event_type, $event_state, $event_year,
        $event_period_from, $event_period_to, $pre_reg_from, $pre_reg_to, $abstract_from, $abstract_to,
        $event_place, $json, $seq_id
    );
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: event 삭제
 */
function if_hide_event($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $hide_dt = date('Y-m-d H:i:s');
    
    $compact = compact('hide_dt');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                show_hide = 'hide',
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 행사의 변경 내용을 기록한다.
 */
function if_preserve_event_history($seq_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $event_history = if_get_event($seq_id);
    $update_dt = date('Y-m-d H:i:s'); 
    
    $compact = compact('event_history', 'update_dt');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $seq_id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 최근 행사 조회
 */
function if_get_latest_event_id() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                seq_id
            FROM
                $if_table1
            WHERE
                show_hide = 'show' AND
                event_state = 'P' 
            ORDER BY
                1 DESC
            LIMIT
                0, 1
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $seq_id = $ifdb->get_var($stmt);
    return $seq_id;
}


/*
 * Desc: 최근 행사 조회
 */
function if_get_latest_event_data() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                show_hide = 'show' AND
                event_state = 'P'
            ORDER BY
                1 DESC
            LIMIT
                0, 1
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}
/*
 * Desc: 행사 연도별로 조회
 */
function if_get_conference_years_by_year() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                event_year,
                COUNT(*) AS cnt
            FROM
                $if_table1
            WHERE
                show_hide = 'show'
            GROUP BY
                1
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

/*
 * Desc: 모든 행사 내역 조회
 *  행사용 게시판에서 사용함. 
 */
function if_get_all_conference() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            ORDER BY
                1 DESC
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

/*
 * Desc: 행사명 조회
 *  행사용 게시판에서 사용함. 
 */
function if_get_conference_name($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                event_name
            FROM
                $if_table1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $event_name = $ifdb->get_var($stmt);
    return $event_name;
}

/*
 * Desc: F/C 사전등록, C 현장등록 포함
 */
function if_add_pre_registration() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $conference_id = $_POST['conference_id'];
    $is_member = $_POST['is_member'];
    $is_graduate = $_POST['is_graduate'];
    $user_name = $_POST['user_name'];
    $org_name = empty($_POST['org_name']) ? '' : $_POST['org_name'];
    $org_phone = empty($_POST['org_phone']) ? '' : $_POST['org_phone'];
    $user_mobile = $_POST['user_mobile'];
    $user_email = $_POST['user_email'];
    $fee_amount = empty($_POST['pre_reg_fee']) ? 0 : $_POST['pre_reg_fee'];
    $pay_state = empty($_POST['pay_state']) ? '1000' : $_POST['pay_state'];
    $user_id = empty($_POST['user_id']) ? 0 : $_POST['user_id'];
    $bank_name = empty($_POST['bank_name']) ? '' : $_POST['bank_name'];
    $passwd = empty($_POST['passwd']) ? '' : $_POST['passwd'];
    $registration_type = empty($_POST['registration_type']) ? '1000' : $_POST['registration_type'];
    
    $compact = compact('bank_name', 'passwd', 'org_phone');
    $json = json_encode($compact);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    cr_id,
                    is_member,
                    is_graduate,
                    user_name,
                    org_name,
                    user_mobile,
                    user_email,
                    fee_amount,
                    pay_state,
                    register_dt,
                    meta_data,
                    user_id,
                    registration_type,
                    conference_id
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?, ?, ?, NOW(),
                    ?, ?, ?, ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('sssssssissss', $is_member, $is_graduate, $user_name, $org_name, $user_mobile, 
        $user_email, $fee_amount, $pay_state, $json, $user_id, $registration_type, $conference_id
    );
    $stmt->execute();
    $cr_id = $stmt->insert_id;
    return $cr_id;
}

/*
 * Desc: 사전/현장 등록 조회
 */
function if_get_pre_registration($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                cr_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}


/*
 * Desc: 사전/현장등록 편집
 */
function if_update_pre_registration() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $cr_id = $_POST['cr_id'];
    $is_member = $_POST['is_member'];
    $is_graduate = $_POST['is_graduate'];
    $user_name = $_POST['user_name'];
    $org_name = empty($_POST['org_name']) ? '' : $_POST['org_name'];
    $org_phone = empty($_POST['org_phone']) ? '' : $_POST['org_phone'];
    $user_mobile = $_POST['user_mobile'];
    $user_email = $_POST['user_email'];
    $fee_amount = $_POST['pre_reg_fee'];
    $pay_state = $_POST['pay_state'];
    $bank_name = empty($_POST['bank_name']) ? '' : $_POST['bank_name'];
    $passwd = empty($_POST['passwd']) ? '' : $_POST['passwd'];
    $admin_memo = empty($_POST['admin_memo']) ? '' : $_POST['admin_memo'];
    
    $compact = compact('bank_name', 'passwd', 'org_phone', 'admin_memo');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                is_member = ?,
                is_graduate = ?,
                user_name = ?,
                org_name = ?,
                user_mobile = ?,
                user_email = ?,
                fee_amount = ?,
                pay_state = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssissi', $is_member, $is_graduate, $user_name, $org_name, $user_mobile,
        $user_email, $fee_amount, $pay_state, $json, $cr_id
    );
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 사전등록 삭제
 */
function if_delete_registration() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $ids = $_POST['ids'];
    
    $query = "
            DELETE FROM
                $if_table1
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $cr_id);
    
    foreach ($ids as $key => $val) {
        $cr_id = $val;
        $result = $stmt->execute();
    }
    return $result;
}

/*
 * Desc: 사전등록 신청자 수 조회
 */
function if_get_pre_registration_attendees($conference_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            SELECT
                COUNT(*)
            FROM
                $if_table1
            WHERE
                conference_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $conference_id);
    $stmt->execute();
    $count = $ifdb->get_var($stmt);
    return $count;
}

/*
 * Desc : 사전/현장등록 삭제/복구(hide/show)
 *  $ids : array
 */
function if_switch_state_registration($ids, $state) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                show_hide = '$state'
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $cr_id);
    
    foreach ($ids as $key => $val) {
        $cr_id = $val;
        $result = $stmt->execute();
    }
    return $result;
}

/*
 * Desc : 사전/현장등록 입금 여부를 전환합니다.
 */
function if_switch_reg_payment_state($id, $pay_state) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                pay_state = ?
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $pay_state, $id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 결제상태별 납부 건수와 금액 조회, 
 *  parameter : 사전등록, 현장등록
 */
function if_get_payment_by_pay_state($reg_type, $conference_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            SELECT
                pay_state, 
                COUNT(*) AS cnt, 
                SUM(fee_amount) AS total 
            FROM
                $if_table1
            WHERE
                registration_type = ? 
                AND conference_id = ? 
                AND show_hide = 'show'
            GROUP BY
                pay_state
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $reg_type, $conference_id);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

/*
 * Desc: 사전등록 입금완료 안내 메일 내용을 기록한다.
 */
function if_preserve_payment_mail_history($cr_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $to_email = $_POST['receiver_email'];
    $mail_subject = $_POST['mail_subject'];
    $mail_content = $_POST['mail_content'];
    $from_email = $_POST['sender_email'];
    
    $payment_confirm_dt = date('Y-m-d H:i:s');
    
    $compact = compact('payment_confirm_dt', 'to_email', 'mail_subject', 'mail_content', 'from_email');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $cr_id);
    $result = $stmt->execute();
    return $result;
}


/*
 * ----------------------------
 * Desc: 초록 접수 등록
 */
function if_add_abstract() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $conference_id = $_POST['seq_id'];
    $is_member = $_POST['is_member'];
    $is_graduate = $_POST['is_graduate'];
    $author_name = $_POST['author_name'];
    $org_name = empty($_POST['org_name']) ? '' : $_POST['org_name'];
    $org_phone = empty($_POST['org_phone']) ? '' : $_POST['org_phone'];
    $author_mobile = $_POST['author_mobile'];
    $author_email = $_POST['author_email'];
    $co_author_name = empty($_POST['co_author_name']) ? '' : $_POST['co_author_name'];
    $member_academy = empty($_POST['member_academy']) ? '' : implode(',', $_POST['member_academy']);
    $pt_type = $_POST['pt_type'];
    $abstract_title = $_POST['abstract_title'];
    $abstract_class = empty($_POST['abstract_class']) ? '' : implode(',', $_POST['abstract_class']);
    
    $user_id = empty($_POST['user_id']) ? 0 : $_POST['user_id'];
    $passwd = empty($_POST['passwd']) ? '' : $_POST['passwd'];
    $file_attachment = [];
    
    // 논문 파일
    if (!empty($_POST['file_path'])) {
        $year = date('Y');
        $upload_dir = UPLOAD_PATH . '/abstract/' . $year . '/' . $conference_id;
        $upload_url = UPLOAD_URL . '/abstract/' . $year . '/' . $conference_id;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            $basename = basename($val);
            $atc_val['file_path'] = $upload_dir . '/' . $basename;
            $atc_val['file_url'] = $upload_url . '/' . $basename;
            $atc_val['file_name'] = $_POST['file_name'][$key];
            $result = rename($val, $atc_val['file_path']);
            if (!empty($result)) {
                array_push($file_attachment, $atc_val);
            }
        }
    }
    
    $compact = compact('passwd', 'org_phone', 'file_attachment');
    $json = json_encode($compact);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    ca_id,
                    is_member,
                    is_graduate,
                    academy,
                    author_name,
                    author_mobile,
                    author_email,
                    org_name,
                    co_author_name,
                    pt_type,
                    abstract_title,
                    abstract_class,
                    meta_data,
                    user_id,
                    conference_id
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssssssssii', $is_member, $is_graduate, $member_academy, $author_name, $author_mobile, 
        $author_email, $org_name, $co_author_name, $pt_type, $abstract_title, 
        $abstract_class, $json, $user_id, $conference_id
    );
    $stmt->execute();
    $cr_id = $stmt->insert_id;
    return $cr_id;
}

function if_get_abstract($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                ca_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

/*
 * Desc: 초록 접수 수정
 */
function if_update_abstract() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $ca_id = $_POST['ca_id'];
    $conference_id = $_POST['conference_id'];
    $is_member = $_POST['is_member'];
    $is_graduate = $_POST['is_graduate'];
    $author_name = $_POST['author_name'];
    $org_name = empty($_POST['org_name']) ? '' : $_POST['org_name'];
    $org_phone = empty($_POST['org_phone']) ? '' : $_POST['org_phone'];
    $author_mobile = $_POST['author_mobile'];
    $author_email = $_POST['author_email'];
    $co_author_name = empty($_POST['co_author_name']) ? '' : $_POST['co_author_name'];
    $member_academy = empty($_POST['member_academy']) ? '' : implode(',', $_POST['member_academy']);
    $pt_type = $_POST['pt_type'];
    $abstract_title = $_POST['abstract_title'];
    $abstract_class = empty($_POST['abstract_class']) ? '' : implode(',', $_POST['abstract_class']);
    
    $user_id = empty($_POST['user_id']) ? 0 : $_POST['user_id'];
    $passwd = empty($_POST['passwd']) ? '' : $_POST['passwd'];
    $file_attachment = [];
    
    $evt_row = if_get_event($conference_id);
    $event_year = $evt_row['event_year'];
    
    // 논문 파일
    if (!empty($_POST['file_path'])) {
        $upload_dir = UPLOAD_PATH . '/abstract/' . $event_year . '/' . $conference_id;
        $upload_url = UPLOAD_URL . '/abstract/' . $event_year . '/' . $conference_id;
        $atc_val = [];
        
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        foreach ($_POST['file_path'] as $key => $val) {
            // 기존 파일
            if (stripos($val, '/tmp/') === false) {
                $atc_val['file_path'] = $val;
                $atc_val['file_url'] = $_POST['file_url'][$key];
                $atc_val['file_name'] = $_POST['file_name'][$key];
                array_push($file_attachment, $atc_val);
            } else {    // 신규 업로드 파일
                $basename = basename($val);
                $atc_val['file_path'] = $upload_dir . '/' . $basename;
                $atc_val['file_url'] = $upload_url . '/' . $basename;
                $atc_val['file_name'] = $_POST['file_name'][$key];
                $result = rename($val, $atc_val['file_path']);
                if (!empty($result)) {
                    array_push($file_attachment, $atc_val);
                }
            }
        }
    }
    
    $compact = compact('passwd', 'org_phone', 'file_attachment');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                is_member = ?,
                is_graduate = ?,
                academy = ?,
                author_name = ?,
                author_mobile = ?,
                author_email = ?,
                org_name = ?,
                co_author_name = ?,
                pt_type = ?,
                abstract_title = ?,
                abstract_class = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE 
                ca_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssssssssi', $is_member, $is_graduate, $member_academy, $author_name, $author_mobile,
        $author_email, $org_name, $co_author_name, $pt_type, $abstract_title,
        $abstract_class, $json, $ca_id
    );
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 초록 삭제
 */
function if_delete_abstract() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $ids = $_POST['ids'];
    
    $query = "
            DELETE FROM
                $if_table1
            WHERE
                ca_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $ca_id);
    
    foreach ($ids as $key => $val) {
        $ca_id = $val;
        $result = $stmt->execute();
    }
    return $result;
}

/*
 * Desc : 초록의 채택/거부를 결정한다.
 */
function if_choose_abstract() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $ids = $_POST['ids'];
    $type = $_POST['type'];
    
    $select_stage = !strcmp($type, 'adopt') ? '3000' : '4000';
    
    $query = "
            UPDATE
                $if_table1
            SET
                select_stage = ?
            WHERE
                ca_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $select_stage, $ca_id);
    
    foreach ($ids as $key => $val) {
        $ca_id = $val;
        $result = $stmt->execute();
    }
    return $result;
}

/*
 * Desc : 초록 삭제/복구(hide/show)
 */
function if_switch_state_abstract($state) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $ids = $_POST['ids'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                show_hide = '$state'
            WHERE
                ca_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $ca_id);
    
    foreach ($ids as $key => $val) {
        $ca_id = $val;
        $result = $stmt->execute();
    }
    return $result;
}

/*
 * Desc: 초록의 변경 내용을 기록한다.
 */
function if_preserve_abstract_history($ca_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $abstract_history = if_get_abstract($ca_id);
    $history_update_dt = date('Y-m-d H:i:s');
    
    $compact = compact('abstract_history', 'history_update_dt');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                ca_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $ca_id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 초록 접수자의 채택 구분별 인원 수 조회
 */
function if_get_count_abstract_selection($conference_id, $stage) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $query = "
            SELECT
                COUNT(*) AS cnt
            FROM
                $if_table1
            WHERE
                conference_id = ? AND
                select_stage = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ss', $conference_id, $stage);
    $stmt->execute();
    $result = $ifdb->get_var($stmt);
    return $result;
}

/*
 * Desc: 현재 시간이 기간 내에 포함되는 지 여부를 확인
 *  미도래 1,마감 시간을 넘기면 2, 유효하면 0
 */
function if_validate_time_period($from, $to) {
    $period_from = strtotime($from);
    $period_to = strtotime($to);
    $time = time();
    
    if ($time < $period_from) {
        return 1;
    } else if ($time > $period_to) {
        return 2;
    } else {
        return 0;
    }
}

function if_signin_pre_registration($cid, $email, $pw) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            SELECT
                cr_id
            FROM
                $if_table1
            WHERE
                conference_id = ? AND
                user_email = ? AND
                show_hide = 'show' AND
                meta_data->'$.passwd' = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('iss', $cid, $email, $pw);
    $stmt->execute();
    $cr_id = $ifdb->get_var($stmt);
    return $cr_id;
}

/*
 * Desc: 사전등록의 변경 내용을 기록한다.
 */
function if_preserve_registration_history($cr_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $reg_history = if_get_pre_registration($cr_id);
    $history_update_dt = date('Y-m-d H:i:s');
    
    $compact = compact('reg_history', 'history_update_dt');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                cr_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $cr_id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 채택 상태에 따른 이름과 이메일주소
 */
function if_get_abstract_attendees_by_adoption($cid, $stage) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $select_stage = implode(',', $stage);
    
    $query = "
            SELECT
                author_name,
                author_email
            FROM
                $if_table1
            WHERE
                conference_id = ? AND
                show_hide = 'show' AND
                select_stage IN ($select_stage)
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $cid);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}


/*
function if_add_conference() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $cfr_year = $_POST['cfr_year'];
    $cfr_title = trim($_POST['cfr_title']);
    $cfr_period_from = $_POST['cfr_period_from'];
    $cfr_period_to = $_POST['cfr_period_to'];
    $cfr_content = empty($_POST['cfr_content']) ? '' : $_POST['cfr_content'];
    $cfr_comment = empty($_POST['cfr_comment']) ? '' : $_POST['cfr_comment'];
    
    $compact = compact('cfr_comment');
    $json = json_encode($compact);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    seq_id,
                    cfr_year,
                    cfr_title,
                    cfr_content,
                    cfr_period_from,
                    cfr_period_to,
                    meta_data
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?,
                    ?, ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssss', $cfr_year, $cfr_title, $cfr_content, $cfr_period_from, $cfr_period_to, $json);
    $stmt->execute();
    $seq_id = $stmt->insert_id;
    return $seq_id;
}

function if_get_conference($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

function if_get_latest_conference_id() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $query = "
            SELECT
                seq_id
            FROM
                $if_table1
            WHERE
                show_hide = 'show'
            ORDER BY
                1 DESC
            LIMIT
                0, 1
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $seq_id = $ifdb->get_var($stmt);
    return $seq_id;
}

function if_update_conference() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $seq_id = $_POST['seq_id'];
    $cfr_year = $_POST['cfr_year'];
    $cfr_title = trim($_POST['cfr_title']);
    $cfr_period_from = $_POST['cfr_period_from'];
    $cfr_period_to = $_POST['cfr_period_to'];
    $cfr_content = empty($_POST['cfr_content']) ? '' : $_POST['cfr_content'];
    $cfr_comment = empty($_POST['cfr_comment']) ? '' : $_POST['cfr_comment'];
    $update_dt = date('Y-m-d H:i:s');
    
    $compact = compact('cfr_comment', 'update_dt');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                cfr_year = ?,
                cfr_title = ?,
                cfr_content = ?,
                cfr_period_from = ?,
                cfr_period_to = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssssssi', $cfr_year, $cfr_title, $cfr_content, $cfr_period_from, $cfr_period_to, $json, $seq_id);
    $result = $stmt->execute();
    return $result;
}

function if_delete_conference($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $seq_id = $_POST['id'];
    
    $delete_dt = date('Y-m-d H:i:s');
    $delete_ip = $_SERVER['REMOTE_ADDR'];
    $compact = compact('delete_dt', 'delete_ip');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                show_hide = 'hide',
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $seq_id);
    $result = $stmt->execute();
    return $result;
}
*/

/*
 * Desc: 학술행사 사전등록 관련 정보 저장
 */
/*
function if_set_event_fee() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference'];
    
    $seq_id = $_POST['seq_id'];
    $event_dt_from = $_POST['event_dt_from'];   // 등록가능기간 시작 날짜,시각 
    $event_dt_to = $_POST['event_dt_to'];       // 등록가능기간 종료 날짜,시각
    $pre_registration_fee = $_POST['pre_registration_fee']; // 자격별 사전등록비
    $update_dt_event_fee = date('Y-m-d H:i:s'); // 사전등록비 설정 변경 시간 기록
    
    $compact = compact('event_dt_from', 'event_dt_to', 'pre_registration_fee', 'update_dt_event_fee');
    $json = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $json, $seq_id);
    $result = $stmt->execute();
    return $result;
}
*/

/*
 * Desc: 사전/현장 등록 조회
 */
function if_get_pre_registration_by_id($conference_id,$id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_register'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                user_id = ? AND
                conference_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ii', $id, $conference_id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}


function if_get_abstract_by_id($conference_id,$id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                user_id = ? AND
                conference_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ii', $id, $conference_id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}


function if_signin_abstract($cid, $email, $pw) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_conference_abstract'];
    
    $query = "
            SELECT
                ca_id
            FROM
                $if_table1
            WHERE
                conference_id = ? AND
                author_email = ? AND
                show_hide = 'show' AND
                meta_data->'$.passwd' = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('iss', $cid, $email, $pw);
    $stmt->execute();
    $ca_id = $ifdb->get_var($stmt);
    return $ca_id;
}
?>