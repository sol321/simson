<?php
function if_add_univ_code() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];

	$univ_code = str_pad($_POST['univ_code'], 3, '0', STR_PAD_LEFT);
	$univ_name = trim($_POST['univ_name']);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					univ_code,
					univ_name
				)
			VALUES
				(
					NULL, ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ss', $univ_code, $univ_name);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_univ_code_by_id($id) {}

/*
 * Desc: 대학교명 검색
 */
function if_get_univ_code_by_name($name) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$uname = '%' . $name . '%';
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				univ_name LIKE ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $uname);
	$stmt->execute();
	$results = $ifdb->get_results($stmt);
	return $results;
}

function if_update_univ_code($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$univ_code = str_pad($_POST['univ_code'], 3, '0', STR_PAD_LEFT);
	$univ_name = trim($_POST['univ_name']);
	
	$query = "
			UPDATE
				$if_table1
			SET
				univ_code = ?,
				univ_name = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssi', $univ_code, $univ_name, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_univ_code($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_univ_code_all() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$query = "
			TRUNCATE TABLE
				$if_table1
	";
	$stmt = $ifdb->prepare($query);
	$result = $stmt->execute();
	return $result;
}

function if_exists_univ_code($code) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				univ_code = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $code);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc : 대학교 코드 업로드, excel 파일
 */
function if_add_excel_file_to_univ_code($upload_file) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_univ_code'];
	
	$affected_rows = 0;	// 등록된 개수
	$array_data = array();
	$query_body = '';
	$file_ext = if_get_file_extension($upload_file);
	
	if (!strcmp($file_ext, 'xls')) {
		require_once INC_PATH . '/classes/PHPExcel.php';
		
		$objPHPExcel = new PHPExcel();
		$objReader = new PHPExcel_Reader_Excel5();
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($upload_file);
		$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
		$sheet = $objPHPExcel->getActiveSheet();
		
		foreach ($rowIterator as $row) {
			$rowIndex = $row->getRowIndex();
			$array_data[$rowIndex] = array('A'=>'', 'B'=>'');
			
			$cell = $sheet->getCell('A' . $rowIndex);
			$array_data[$rowIndex]['A'] = $cell->getValue();
			$cell = $sheet->getCell('B' . $rowIndex);
			$array_data[$rowIndex]['B'] = $cell->getValue();
		}
		
		if (!empty($array_data)) {
			foreach ($array_data as $key => $val) {
				$code = str_pad($val['A'], 3, '0', STR_PAD_LEFT);
				$name = $val['B'];

				if ($key > 1) {	 // 타이틀은 제외
					$query_body .= ", (NULL, '$code', '$name')";
				}
			} // foreach
		}
	} else if (!strcmp($file_ext, 'xlsx')) {
		require_once INC_PATH . '/classes/SimpleXLSX.php';
		
		$xlsx = new SimpleXLSX($upload_file);
		$array_data = $xlsx->rows();
		
		if (!empty($array_data)) {
			foreach ($array_data as $key => $val) {
				$code = $code = str_pad($val[0], 3, '0', STR_PAD_LEFT);
				$name = $val[1];
				
				if ($key > 0) {	 // 타이틀은 제외
					$query_body .= ", (NULL, '$code', '$name')";
				}
			} // foreach
		}
	}
	
	$query_head = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					univ_code,
					univ_name
				)
			VALUES
	";
	if (!empty($query_body)) {
		$query = $query_head . ltrim($query_body, ',');
		$stmt = $ifdb->prepare($query);
		$stmt->execute();
		$affected_rows = $ifdb->affected_rows;
	}
	return $affected_rows;
}

function if_exists_exam_year($year) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				exam_year = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc: 시험 일정 등록
 */
function if_add_mock_exam() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam'];

	$exam_year = trim($_POST['exam_year']);
	$create_dt = date('Y-m-d H:i:s');
	
	$json = json_encode(compact('create_dt')); 
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					exam_year,
					exam_order,
					exam_date,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	
	foreach ($_POST['exam_order'] as $key => $val) {
		$exam_order = $key + 1;
		$stmt->bind_param('ssss', $exam_year, $exam_order, $val, $json);
		$stmt->execute();
	}
	
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_mock_exam_by_year($year) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				exam_year = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_update_mock_exam() {}

function if_delete_mock_exam_by_year($year) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				exam_year = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 모의고사 신청
 */
function if_add_mock_exam_application() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];
	$if_table2 = $GLOBALS['if_tbl_mock_exam_order'];

	$mock_year = $_POST['year'];
	$univ_code = $_POST['univ_code'];
	$univ_name = $_POST['univ_name'];
	$org_zipcode = $_POST['org_zipcode'];
	$org_address1 = $_POST['org_address1'];
	$org_address2 = $_POST['org_address2'];
	$apply_name = trim($_POST['apply_name']);
	$apply_phone = trim($_POST['apply_phone']);
	$apply_email = trim($_POST['apply_email']);
	$apply_fax = empty($_POST['apply_fax']) ? '' : trim($_POST['apply_fax']);
	$pay_state = '1000';
	$exam_state = '1001';
	$exam_ids = $_POST['exam_id'];
	
	$file_attachment = [];
	
	// 첨부파일
	if (!empty($_POST['file_path'])) {
		$upload_dir = UPLOAD_PATH . '/mock_exam/' . $univ_code;
		$upload_url = UPLOAD_URL . '/mock_exam/' . $univ_code;
		$atc_val = [];
		
		if (!is_dir($upload_dir)) {
			mkdir($upload_dir, 0777, true);
		}
		foreach ($_POST['file_path'] as $key => $val) {
			$basename = basename($val);
			$atc_val['file_path'] = $upload_dir . '/' . $basename;
			$atc_val['file_url'] = $upload_url . '/' . $basename;
			$atc_val['file_name'] = $_POST['file_name'][$key];
			$result = rename($val, $atc_val['file_path']);
			if (!empty($result)) {
				array_push($file_attachment, $atc_val);
			}
		}
	}
	
	// 시험 응시 정보
	$exam_seq_id = []; // 모의고사 아이디 저장
	$exam_regular_dt = []; // 모의고사 정규 시행일자
	
	foreach ($exam_ids as $key => $val) {
		if (!empty($_POST['apply_type_'. $val])) {
			array_push($exam_seq_id, $val);
			array_push($exam_regular_dt, $_POST['exam_date_' . $val]);
		}
	}
	
	$compact = compact('file_attachment', 'apply_fax', 'org_zipcode', 'org_address1', 'org_address2', 'exam_seq_id', 'exam_regular_dt', 'mock_year');
	$meta_data = json_encode($compact);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					univ_code,
					univ_name,
					apply_name,
					apply_phone,
					apply_email,
					apply_dt,
					meta_data		
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, NOW(), ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssss', $univ_code, $univ_name, $apply_name, $apply_phone, $apply_email, $meta_data);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	
	if (!empty($seq_id)) {
	    // 차수 저장
		$query = "
			INSERT INTO
				$if_table2
				(
					order_id,
					apply_id,
					exam_order,
					exam_state,
					pay_state,
					apply_type,
					exam_date,
					admin_memo
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, ''
				)
		";
		$stmt = $ifdb->prepare($query);
		
		foreach ($exam_ids as $key => $val) {
		    if (!empty($_POST['apply_type_'. $val])) {
		        
		        $type = $_POST['apply_type_'. $val];
		        $exam_order = $_POST['exam_order_' . $val];
		        
		        if (!strcmp($type, 'R')) {  // 정규
		            $exam_dt = $_POST['exam_date_' . $val];
		        } else {	// A 추가 : 희망 응시일
		            $exam_dt = $_POST['desired_date_' . $val];
		        }

		        $stmt->bind_param('iissss', $seq_id, $exam_order, $exam_state, $pay_state, $type, $exam_dt);
		        $stmt->execute();
		    }
		}
	}
	
	return $seq_id;
}

function if_get_mock_exam_application($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_mock_exam_excel_file($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];
	
	$query = "
			SELECT
				JSON_UNQUOTE(JSON_EXTRACT(meta_data, '$.file_attachment[0].file_path')) AS file_path
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_update_mock_exam_application($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];

	$org_zipcode = $_POST['org_zipcode'];
	$org_address1 = $_POST['org_address1'];
	$org_address2 = $_POST['org_address2'];
	$apply_name = trim($_POST['apply_name']);
	$apply_phone = trim($_POST['apply_phone']);
	$apply_email = trim($_POST['apply_email']);
	$apply_fax = empty($_POST['apply_fax']) ? '' : trim($_POST['apply_fax']);
	$admin_memo = empty($_POST['admin_memo']) ? '' : trim($_POST['admin_memo']);
	$update_dt = date('Y-m-d H:i:s');

	$compact = compact('apply_fax', 'org_zipcode', 'org_address1', 'org_address2', 'admin_memo', 'update_dt');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				apply_name = ?,
				apply_phone = ?,
				apply_email = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)	
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssi', $apply_name, $apply_phone, $apply_email, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 신청 내역 삭제 
 * 
 *      if_mock_exam_taker, if_mock_exam_order CASCADE DELETE
 */
function if_delete_mock_exam_application($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 접수 확인을 위한 정보 확인
 */
function if_check_mock_exam_application($name, $email, $univ_name) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_apply'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				univ_name = ? AND
				apply_name = ? AND
				apply_email = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $univ_name, $name, $email);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc: 학생코드표(xls)의 자료를 if_mock_exam_taker 테이블에 등록.
 */
function if_add_exam_taker_from_excel($upload_file, $apply_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_taker'];
	
	$affected_rows = 0;	// 등록된 개수
	$array_data = array();
	$query_body = '';
	$file_ext = if_get_file_extension($upload_file);

	if (!strcmp($file_ext, 'xls')) {
		require_once INC_PATH . '/classes/PHPExcel.php';
		
		$objPHPExcel = new PHPExcel();
		$objReader = new PHPExcel_Reader_Excel5();
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($upload_file);
		$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
		$sheet = $objPHPExcel->getActiveSheet();
		
		foreach ($rowIterator as $row) {
			$rowIndex = $row->getRowIndex();
			$array_data[$rowIndex] = array('A'=>'', 'B'=>'');
			
			$cell = $sheet->getCell('A' . $rowIndex);
			$array_data[$rowIndex]['A'] = $cell->getValue();
			$cell = $sheet->getCell('B' . $rowIndex);
			$array_data[$rowIndex]['B'] = $cell->getValue();
		}
		
		if (!empty($array_data)) {
			foreach ($array_data as $key => $val) {
				$code = str_pad($val['A'], 3, '0', STR_PAD_LEFT);
				$name = $val['B'];

				if ($key > 1) {	 // 타이틀은 제외
					$query_body .= ", (NULL, $apply_id, '$code', '$name')";
				}
			} // foreach
		}
	} else if (!strcmp($file_ext, 'xlsx')) {
		require_once INC_PATH . '/classes/SimpleXLSX.php';
		
		$xlsx = new SimpleXLSX($upload_file);
		$array_data = $xlsx->rows();
		
		if (!empty($array_data)) {
			foreach ($array_data as $key => $val) {
				$code = $code = str_pad($val[0], 3, '0', STR_PAD_LEFT);
				$name = $val[1];
				
				if ($key > 0) {	 // 타이틀은 제외
					$query_body .= ", (NULL, $apply_id, '$code', '$name')";
				}
			} // foreach
		}
	}
	
	$query_head = "
			INSERT INTO
				$if_table1
				(
					taker_id,
					apply_id,
					student_id,
					student_name
				)
			VALUES
	";
	if (!empty($query_body)) {
		$query = $query_head . ltrim($query_body, ',');
		$stmt = $ifdb->prepare($query);
		$stmt->execute();
		$affected_rows = $ifdb->affected_rows;
	}
	return $affected_rows;
}

function if_add_exam_taker() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_taker'];

	$apply_id = $_POST['apply_id'];
	$student_id = trim($_POST['student_id']);
	$student_name = trim($_POST['student_name']);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					taker_id,
					apply_id,
					student_id,
					student_name
				)
			VALUES
				(
					NULL, ?, ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('iss', $apply_id, $student_id, $student_name);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

/*
 * Desc: 신청 학교의 응시자 수 합계
 */
function if_get_exam_taker_count($app_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_taker'];
	
	$query = "
			SELECT
				COUNT(*) AS cnt
			FROM
				$if_table1
			WHERE
				apply_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $app_id);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_delete_exam_taker($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_taker'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				taker_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 모의고사 신청 내용 중 차수 정보 조회
 */
function if_get_mock_exam_orders_of_application($app_id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_order'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				apply_id = ?
            ORDER BY
                exam_order ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $app_id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_get_mock_exam_order($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_order'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				order_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_update_mock_exam_order($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_mock_exam_order'];

	$exam_state = $_POST['exam_state'];
	$pay_state = $_POST['pay_state'];
	$exam_date = $_POST['exam_date'];
	$apply_type = $_POST['apply_type'];
	$admin_memo = empty($_POST['admin_memo']) ? '' : trim($_POST['admin_memo']);
	
	$query = "
			UPDATE
				$if_table1
			SET
				exam_state = ?,
				pay_state = ?,
				exam_date = ?,
				admin_memo = ?	
			WHERE
				order_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssi', $exam_state, $pay_state, $exam_date, $admin_memo, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_mock_exam_order($id) {}

?>