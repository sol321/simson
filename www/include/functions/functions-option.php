<?php
function if_get_option($option_name = NULL) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_options'];
    
    
    if (empty($option_name)) {
        $query = "
				SELECT
					*
				FROM
					$if_table1
		";
		$stmt = $ifdb->prepare($query);
		$stmt->execute();
		return $ifdb->get_results($stmt);
    } else {
        $query = "
				SELECT
					option_value
				FROM
					$if_table1
				WHERE
					option_name = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('s', $option_name);
		$stmt->execute();
		return $ifdb->get_var($stmt);
    }
}

/*
 * Ref: https://dev.mysql.com/doc/refman/8.0/en/replace.html
 */
function if_update_option_value($option_name, $option_value) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_options'];
    
    $query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				option_name = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $option_name);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	
	if (empty($seq_id)) {
	    $seq_id = 0;
	}
	
	$query = "
			REPLACE INTO
				$if_table1
				(
					seq_id,
					option_name,
					option_value
				)
			VALUES
				(
					?, ?, ?
				)
    ";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('iss', $seq_id, $option_name, $option_value);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 업데이트 정보를 계속 쌓아간다.
 */
function if_preserve_option_value($option_name, $option_value) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_options'];
    
    $query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				option_name = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $option_name);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	
	if (!empty($seq_id)) {
	    $query = "
				UPDATE
					$if_table1
				SET
					option_value = JSON_MERGE_PRESERVE(option_value, ?)
				WHERE
					option_name = ?
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('ss', $option_value, $option_name);
	} else {
	    $query = "
				INSERT INTO
					$if_table1
					(
						seq_id,
						option_name,
						option_value
					)
				VALUES
					(
						NULL, ?, ?
					)
		";
		$stmt = $ifdb->prepare($query);
		$stmt->bind_param('ss', $option_name, $option_value);
		
	}
	$result = $stmt->execute();
	return $result;
}

function if_remove_json_option($option_name, $option_key, $idx) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_options'];
  
    $pos = '$.' . $option_key .'[' . $idx . ']';
    
    $query = "
			UPDATE
				$if_table1
			SET
                option_value = JSON_REMOVE(option_value, '$pos')
			WHERE
				option_name = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $option_name);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc : if_get_xxx_meta($id)에 의해 나온 결과값(배열)에서 meta_key로 meta_value를 찾는다.
 * 			if_get_option() 결과값에서 option_key로 option_value를 찾기 위해 meta_value parameter 추가
 */
/*
function if_get_meta_value_by_key($meta_val, $meta_key, $column = 'meta_value') {
    $value = false;
    
    foreach ($meta_val as $key => $val) {
        if (is_array($val)) {
            foreach ($val as $k => $v) {
                if (!strcmp($v, $meta_key)) {
                    $value = $val[$column];
                    break;
                }
            }
        } else {
            if (!strcmp($meta_key, $key)) {
                $value = $val;
                break;
            }
        }
    }
    return $value;
}
*/
?>