<?php
/*
 * Desc: 비용항목
 */
function if_add_item() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	$item_year = $_POST['item_year'];
	$item_name = trim($_POST['item_name']);
	$item_type = empty($_POST['item_type']) ? '' : $_POST['item_type'];
	$item_period = empty($_POST['item_period']) ? '1' : $_POST['item_period'];
	$item_comment = empty($_POST['item_comment']) ? '' : $_POST['item_comment'];
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					item_year,
					item_name,
					item_type,
					item_period,
					item_comment
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssss', $item_year, $item_name, $item_type, $item_period, $item_comment);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_item($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_item_list($year = NULL) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	if (empty($year)) {
		$year = date('Y');
	}
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				item_year = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc: 비용 목록에 등록된 모든 연도 조회
 */
function if_get_item_all_year() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	if (empty($year)) {
		$year = date('Y');
	}
	
	$query = "
			SELECT
				item_year
			FROM
				$if_table1
    		GROUP BY
                1
            ORDER BY 
                1 DESC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}


function if_update_item() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	$seq_id = $_POST['seq_id'];
	$item_year = $_POST['item_year'];
	$item_name = trim($_POST['item_name']);
	$item_type = empty($_POST['item_type']) ? '' : $_POST['item_type'];
	$item_period = empty($_POST['item_period']) ? '1' : $_POST['item_period'];
	$item_comment = empty($_POST['item_comment']) ? '' : $_POST['item_comment'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				item_year = ?,
				item_name = ?,
				item_type = ?,
				item_period = ?,
				item_comment = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $item_year, $item_name, $item_type, $item_period, $item_comment, $seq_id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_item($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 연도와 종류로 중복 타입 존재 체크. 비용 항목 생성 시 사용함.
 */
function if_check_duplicate_item($year, $type, $period) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_items'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				item_year = ? AND
				item_type = ? AND
				item_period = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $year, $type, $period);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc: 회원등급
 */
function if_add_user_class() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$class_name = trim($_POST['class_name']);
	$is_default = empty($_POST['is_default']) ? '0' : $_POST['is_default'];
	$class_comment = empty($_POST['class_comment']) ? '' : $_POST['class_comment'];
	
	if (!empty($is_default)) {
	    if_update_user_class_zero();
	}
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					class_name,
					is_default,
					class_comment
				)
			VALUES
				(
					NULL, ?, ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $class_name, $is_default, $class_comment);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_user_class($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_user_class_list() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_get_user_class_assoc_array() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				seq_id,
				class_name
			FROM
				$if_table1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	
	$assoc_array = [];
	
	if (!empty($result)) {
		foreach ($result as $key => $val) {
			$seq_id = $val['seq_id'];
			$class_name = $val['class_name'];
			$assoc_array[$seq_id] = $class_name;
		}
	}
	return $assoc_array;
}

/*
 * Desc: 회원 기본 등급 조회
 *      is_default가 1, 없으면 가장 최근에 등록한 등급
 */
function if_get_user_default_class_id() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			ORDER BY
				is_default DESC,
                seq_id DESC
            LIMIT
                0, 1 
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

/*
 * Desc: 회원 등급 모두 조회
 */
function if_get_all_user_class() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			ORDER BY
				class_name ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_get_user_class_array($uc) {
    $array = [];
    if (!empty($uc)) {
        foreach ($uc as $key => $val) {
            $cid = $val['seq_id'];
            $cname = $val['class_name'];
            $array[$cid] = $cname;
        }
    }
    return $array;
}


function if_update_user_class() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$seq_id = $_POST['seq_id'];
	$class_name = trim($_POST['class_name']);
	$is_default = empty($_POST['is_default']) ? '0' : $_POST['is_default'];
	$class_comment = empty($_POST['class_comment']) ? '' : $_POST['class_comment'];
	
	if (!empty($is_default)) {
	    if_update_user_class_zero();
	}
	
	$query = "
			UPDATE
				$if_table1
			SET
				class_name = ?,
				is_default = ?,
				class_comment = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssi', $class_name, $is_default, $class_comment, $seq_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회원 기본 회원 등급의 초기화
 */
function if_update_user_class_zero() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				is_default = 0
	";
	$stmt = $ifdb->prepare($query);
	$result = $stmt->execute();
	return $result;
}

function if_delete_user_class($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

function if_check_duplicate_user_class($name) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_user_class'];
	
	$query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				class_name = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $name);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc: 연회비(상품)
 */
function if_add_product() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$class_id = $_POST['class_sid'];
	$product_name = $_POST['product_name'];
	$product_price = empty($_POST['product_price']) ? 0 : $_POST['product_price'];
	$product_year = empty($_POST['product_year']) ? date('Y') : $_POST['product_year'];
	$item_sid = $_POST['item_sid'];
	$item_row = if_get_item($item_sid);
	$product_type = $item_row['item_type'];
	$product_period = $item_row['item_period'];
	$show_hide = 'show';
	
	$compact = compact('item_sid');
	$meta_data = json_encode($compact);
	
	$query = "
			INSERT INTO
				$if_table1
				(
					product_id,
					product_name,
					product_price,
					product_year,
					product_period,
					product_type,
					show_hide,
					create_dt,
					meta_data,
					user_class_id
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, NOW(), ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sisssssi', $product_name, $product_price, $product_year, $product_period,
		$product_type, $show_hide, $meta_data, $class_id);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_product($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				product_id = ? AND
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

/*
 * Desc : 특정 연도의 모든 회비 상품을 조회
 */
function if_get_products_by_year($year) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				product_year = ? AND
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

/*
 * Desc : 특정 연도의 종류와 기간으로 회비 상품을 조회
 */
function if_get_product_by_condition($year, $type, $period) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				product_year = ? AND
				product_type = ? AND
				product_period = ? AND
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $year, $type, $period);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

/*
 * Desc: product_id[]에 해당하는 product 정보 조회
 */
function if_get_products($array) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$prod_ids = implode(',', $array);
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				product_id IN ($prod_ids) AND
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_update_product() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$product_id = $_POST['product_id'];
	$class_id = empty($_POST['class_id']) ? 0 : $_POST['class_id'];
	$product_name = empty($_POST['product_name']) ? '' : $_POST['product_name'];
	$product_price = empty($_POST['product_price']) ? 0 : $_POST['product_price'];
	$product_year = empty($_POST['product_year']) ? '' : $_POST['product_year'];
	$item_sid = $_POST['item_id'];
	$item_row = if_get_item($item_sid);
	$product_type = $item_row['item_type'];
	$product_period = $item_row['item_period'];
	
	// history를 만들어 저장합니다.
	$product_row = if_get_product($product_id);
	$update_dt = date('Y-m-d H:i:s');
	$product_history = compact('product_row', 'update_dt');
	$meta_data = json_encode($product_history);
	
	$query = "
			UPDATE
				$if_table1
			SET
				product_name = ?,
				product_price = ?,
				product_year = ?,
				product_period = ?,
				product_type = ?,
				meta_data = JSON_MERGE_PRESERVE(meta_data, ?),
				user_class_id = ?
			WHERE
				product_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sissssii', $product_name, $product_price, $product_year, $product_period,
		$product_type, $meta_data, $class_id, $product_id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_product($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				show_hide = 'hide'
			WHERE
				product_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 회비 항목 일괄 등록 시, 기존 항목은 삭제함.
 */
function if_delete_duplicated_product($year, $period, $type, $user_class) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$delete_dt = date('Y-m-d H:i:s');
	$json = json_encode(compact('delete_dt'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				show_hide = 'hide',
                meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				product_year = ? AND
				product_period = ? AND
				product_type = ? AND
				user_class_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssss', $json, $year, $period, $type, $user_class);
	$result = $stmt->execute();
	return $result;
}

// 연도에 해당하는 회비 정보 개수 조회.
function if_get_product_number($year = NULL) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	if (empty($year)) {
		$year = date('Y');
	}
	
	$query = "
			SELECT
				COUNT(*) AS cnt
			FROM
				$if_table1
			WHERE
				product_year = ? AND
				show_hide = 'show'
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $year);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

// 대량 등록
function if_add_product_bulk() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$success = 0;
	
	$query = "
			INSERT INTO
				$if_table1
				(
					product_id,
					product_name,
					product_price,
					product_year,
					product_period,
					product_type,
					show_hide,
					create_dt,
					meta_data,
					user_class_id
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?, ?, NOW(), ?, ?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sisssssi', $product_name, $product_price, $product_year, $product_period,
		$product_type, $show_hide, $meta_data, $user_class_id);
	
	foreach ($_POST['product_price'] as $key => $val) {
	    if ($val != "") {  // 회비 금액이 미입력이면 등록하지 않습니다.
	        $product_year = $_POST['product_year'];
	        $user_class_id = $_POST['class_id'][$key];
	        $item_sid = $_POST['item_id'][$key];
	        $product_price = $val;
	        $product_name = empty($_POST['product_name'][$key]) ? '미입력' : $_POST['product_name'][$key];
	        $show_hide = 'show';
	        $compact = compact('item_sid');
	        $meta_data = json_encode($compact);
	        
	        $item_row = if_get_item($item_sid);
	        $product_type = $item_row['item_type'];
	        $product_period = $item_row['item_period'];
	        
	        if_delete_duplicated_product($product_year, $product_period, $product_type, $user_class_id);
	        
	        $stmt->execute();
	        $prod_id = $stmt->insert_id;
	        
	        if ($prod_id) {
	            $success++;
	        }
	    }
	}
	return $success;
}

/*
 * Desc: 연도, 회원등급, 종류로 중복 타입 존재 체크. 회비 항목 생성 시 사용함.
 */
function if_check_duplicate_product($class, $year, $type, $period) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_item_product'];
	
	$query = "
			SELECT
				product_id
			FROM
				$if_table1
			WHERE
				product_year = ? AND
				product_period = ? AND
				product_type = ? AND
				show_hide = 'show' AND
				user_class_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssss', $year, $period, $type, $class);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

/*
 * Desc: 구독 학회지 등록
 */
function if_add_journal_item() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$journal_name = trim($_POST['journal_name']);
	$journal_fee = intval($_POST['journal_fee']);
	$journal_issue_month = implode(',', $_POST['issue_month']);
	$journal_order = 0;
	
	$create_dt = date('Y-m-d H:i:s');
	$json = json_encode(compact('create_dt'));
	
	$query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					journal_name,
					journal_issue_month,
					journal_fee,
					journal_order,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssiis', $journal_name, $journal_issue_month, $journal_fee, $journal_order, $json);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_journal_item($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_all_journal_items() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			ORDER BY
				journal_order ASC
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_update_journal_item($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$journal_name = trim($_POST['journal_name']);
	$journal_fee = intval($_POST['journal_fee']);
	$journal_issue_month = implode(',', $_POST['issue_month']);
	
	$update_dt = date('Y-m-d H:i:s');
	$json = json_encode(compact('update_dt'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				journal_name = ?,
				journal_issue_month = ?,
				journal_fee = ?,
				meta_data = JSON_MERGE_PRESERVE(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssisi', $journal_name, $journal_issue_month, $journal_fee, $json, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_journal_item($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 학회지 순서 재졍렬
 */
function if_update_journal_item_order($item_ids) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	
	$query = "
			UPDATE
				$if_table1
			SET
				journal_order = ?
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	
	foreach ($item_ids as $key => $val) {
		$stmt->bind_param('ii', $key, $val);
		$result = $stmt->execute();
	}
	
	return $result;
}

/*
 * Desc: 학회지 구독 신청
 */
function if_add_journal_user() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal_user'];
    
    $user_id = trim($_POST['user_id']);
    $org_phone = !empty($_POST['org_phone'])?trim($_POST['org_phone']):"";
    $user_name = !empty($_POST['user_name'])?trim($_POST['user_name']):"";
    $user_email = !empty($_POST['user_email'])?trim($_POST['user_email']):"";
    $org_phone = !empty($_POST['org_phone'])?trim($_POST['org_phone']):"";
    $user_mobile = !empty($_POST['user_mobile'])?trim($_POST['user_mobile']):"";
    $zipcode = !empty($_POST['zipcode'])?trim($_POST['zipcode']):"";
    $address1 = !empty($_POST['address1'])?trim($_POST['address1']):"";
    $address2 = !empty($_POST['address2'])?trim($_POST['address2']):"";
    $create_dt = date('Y-m-d H:i:s');
    
    $json = json_encode(compact('zipcode', 'address1', 'address2'));
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					user_name,
					user_email,
					org_phone,
					user_mobile,
					user_id,
					create_dt,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssiss', $user_name, $user_email, $org_phone, $user_mobile, $user_id, $create_dt, $json);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}

function if_get_journal_subscription_user($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_journal_user'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_update_journal_subscription_user($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_user'];
	
	$user_name = trim($_POST['user_name']);
	$user_email = trim($_POST['user_email']);
	$org_phone = trim($_POST['org_phone']);
	$user_mobile = empty($_POST['user_mobile']) ? '' : trim($_POST['user_mobile']);
	$admin_memo = empty($_POST['admin_memo']) ? '' : trim($_POST['admin_memo']);
	
	$update_dt = date('Y-m-d H:i:s');
	$compact = compact('update_dt', 'admin_memo');
	$meta_data = json_encode($compact);
	
	$query = "
			UPDATE
				$if_table1
			SET
				user_name = ?,
				user_email = ?,
				org_phone = ?,
				user_mobile = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sssssi', $user_name, $user_email, $org_phone, $user_mobile, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}

function if_delete_journal_subscription_user($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_user'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: 구독신청 정보 if_journal_subscription
 */
function if_get_journal_subscription($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				subscription_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}

function if_get_journal_subscription_by_journal_userid($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				journal_user_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}

function if_get_journal_subscription_slibing_count($jn_userid) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$query = "
			SELECT
				COUNT(*)
			FROM
				$if_table1
			WHERE
				journal_user_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $jn_userid);
	$stmt->execute();
	$result = $ifdb->get_var($stmt);
	return $result;
}

function if_update_payment_journal_subscription($id, $pay_state, $pay_date) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	// pay_state : 9000 & pay_date -> expiry_date 자동 계산
	if ($pay_state == '9000' && !empty($pay_date)) {
		$new_date = strtotime('+ 1 year', strtotime($pay_date));
		$expiry_date = date('Y-m-d', $new_date);
	} else {
		$expiry_date = NULL;
	}
	
	$update_dt = date('Y-m-d H:i:s');
	$meta_data = json_encode(compact('update_dt'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				pay_state = ?,
				pay_date = ?,
				expiry_date = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				subscription_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssssi', $pay_state, $pay_date, $expiry_date, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}

/*
function if_update_journal_subscription($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$journal_name = trim($_POST['journal_name']);
	$journal_issue_month = trim($_POST['journal_issue_month']);
	$journal_fee = $_POST['journal_fee'];
	$pay_state = $_POST['pay_state'];
	$pay_date = $_POST['pay_date'];
	
	// pay_state : 9000 & pay_date -> expiry_date 자동 계산
	if ($pay_state == '9000' && !empty($pay_date)) {
		$new_date = strtotime('+ 1 year', strtotime($pay_date));
		$expiry_date = date('Y-m-d', $new_date);
	} else {
		$expiry_date = NULL;
	}
	
	$update_dt = date('Y-m-d H:i:s');
	$meta_data = json_encode(compact('update_dt'));
	
	$query = "
			UPDATE
				$if_table1
			SET
				journal_name = ?,
				journal_issue_month = ?,
				journal_fee = ?,
				pay_state = ?,
				pay_date = ?,
				expiry_date = ?,
				meta_data = JSON_MERGE_PATCH(meta_data, ?)
			WHERE
				subscription_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssissssi', $journal_name, $journal_issue_month, $journal_fee,
		$pay_state, $pay_date, $expiry_date, $meta_data, $id);
	$result = $stmt->execute();
	return $result;
}
*/

function if_delete_journal_subscription($id) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$query = "
			DELETE FROM
				$if_table1
			WHERE
				subscription_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$result = $stmt->execute();
	return $result;
}

/*
 * 학회지 item 가져오기
 */
function if_get_journal_item_list() {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_item'];
	$query = "
		SELECT
			*
		FROM
			$if_table1
		ORDER BY
			journal_order ASC
			
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	$result = $ifdb->get_results($stmt);
	return $result;
}


/*
 * Desc: 구독 학회지 신청 여부 확인
 */
function if_get_journal_user_chkeck($journal_name) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_user'];
	$if_table2 = $GLOBALS['if_tbl_journal_subscription'];
	
	$user_id = trim($_POST['user_id']);
	$query = "
			SELECT
				".$if_table2.".*
			FROM
				$if_table1 
            LEFT JOIN 
                $if_table2
			ON
				".$if_table1.".seq_id = ".$if_table2.".journal_user_id
			WHERE
				".$if_table1.".user_id = ? AND
                ".$if_table2.".journal_name = ?
			ORDER BY 
                ".$if_table2.".subscription_id DESC
            LIMIT
                0, 1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('is', $user_id, $journal_name);
	$stmt->execute();
	$result = $ifdb->get_row($stmt);
	return $result;
}


/*
 * Desc: 구독 학회지 등록
 */
function if_add_journal_subscription($subscription_data) {
	$ifdb = $GLOBALS['ifdb'];
	$if_table1 = $GLOBALS['if_tbl_journal_subscription'];
	
	$journal_user_id = trim($subscription_data['journal_user_id']);
	$journal_name = trim($subscription_data['journal_name']);
	$journal_issue_month = trim($subscription_data['journal_issue_month']);
	$journal_fee = trim($subscription_data['journal_fee']);
	$create_dt = date('Y-m-d H:i:s');
	$pay_state = '1000';
	
	$json = json_encode(compact('create_dt'));
	
	$query = "
			INSERT INTO
				$if_table1
				(
					subscription_id,
					journal_user_id,
					journal_name,
					journal_issue_month,
					journal_fee,
					pay_state,
					meta_data
				)
			VALUES
				(
					NULL, ?, ?, ?, ?, ?,
					?
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ississ', $journal_user_id, $journal_name, $journal_issue_month, $journal_fee, $pay_state, $json);
	$stmt->execute();
	$seq_id = $stmt->insert_id;
	return $seq_id;
}
?>