<?php
function if_add_board() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_template'];
    
    $tpl_name = $_POST['tpl_name'];
    $tpl_skin = $_POST['tpl_skin'];
    $tpl_max_filesize = empty($_POST['tpl_max_filesize']) ? 0 : $_POST['tpl_max_filesize'];
    $show_hide = 'show';
    $read_level = $_POST['read_level'];
    $write_level = $_POST['write_level'];
    $use_comment = !empty($_POST['use_comment'])?$_POST['use_comment']:'';
    
    $update_dt = date('Y-m-d H:i:s');
    $compact = compact('tpl_max_filesize', 'update_dt', 'read_level', 'write_level','use_comment');
    $meta_data = json_encode($compact);
    
    $query = "
            INSERT INTO
                $if_table1
                (
                    seq_id,
                    tpl_name,
                    tpl_skin,
                    show_hide,
                    meta_data
                )
            VALUES
                (
                    NULL, ?, ?, ?, ?
                )
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('ssss', $tpl_name, $tpl_skin, $show_hide, $meta_data);
    $stmt->execute();
    $seq_id = $stmt->insert_id;
    return $seq_id;
}

function if_get_board($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_template'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                seq_id = ? AND
                show_hide = 'show'
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $ifdb->get_row($stmt);
    return $result;
}

function if_update_board() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_template'];
    
    $seq_id = $_POST['seq_id'];
    $tpl_name = $_POST['tpl_name'];
    $tpl_skin = $_POST['tpl_skin'];
    $tpl_max_filesize = empty($_POST['tpl_max_filesize']) ? 0 : $_POST['tpl_max_filesize'];
    $read_level = $_POST['read_level'];
    $write_level = $_POST['write_level'];
    $use_comment = !empty($_POST['use_comment'])?$_POST['use_comment']:'';
    
    $update_dt = date('Y-m-d H:i:s');
    $compact = compact('tpl_max_filesize', 'update_dt', 'read_level', 'write_level','use_comment');
    $meta_data = json_encode($compact);
    
    $query = "
            UPDATE
                $if_table1
            SET
                tpl_name = ?,
                tpl_skin = ?,
                meta_data = JSON_MERGE_PATCH(meta_data, ?)
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('sssi', $tpl_name, $tpl_skin, $meta_data, $seq_id);
    $result = $stmt->execute();
    return $result;
}

function if_delete_board($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_template'];
    
    $query = "
            UPDATE
                $if_table1
            SET
                show_hide = 'hide'
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('i', $id);
    $result = $stmt->execute();
    return $result;
}

/*
 * Desc: 게시판 내역
 */
function if_get_board_lists() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_post_template'];
    
    $query = "
            SELECT
                *
            FROM
                $if_table1
            WHERE
                show_hide = 'show'
            ORDER BY
                tpl_name ASC
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $result = $ifdb->get_results($stmt);
    return $result;
}

?>