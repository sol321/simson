<?php
// 관리자 추가
function if_add_admin() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $login = trim($_POST['admin_login']);
    $user_pass = if_encrypt_passwd($_POST['password']);
    $admin_name = empty($_POST['admin_name']) ? '관리자' : trim($_POST['admin_name']);
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					admin_login,
					admin_pw,
					admin_name,
					create_dt
				)
			VALUES
				(
					NULL, ?, ?, ?, NOW()
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('sss', $login, $user_pass, $admin_name);
	$stmt->execute();
	$user_id = $stmt->insert_id;
	return $user_id;
}

function if_update_admin() {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $admin_id = $_POST['admin_id'];
    $admin_name = empty($_POST['admin_name']) ? '관리자' : trim($_POST['admin_name']);
    
    $query = "
			UPDATE
				$if_table1
            SET
				admin_name = ?
			WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('si', $admin_name, $admin_id);
	$result = $stmt->execute();
	
	if (!empty($_POST['admin_pass'])) {
	    $admin_pass = $_POST['admin_pass'];
	    if_update_admin_password($admin_id, $admin_pass);
	}
	
	return $result;
}

function if_delete_admin($admin_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $query = "
			DELETE FROM
				$if_table1
			WHERE
                seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $admin_id);
	$result = $stmt->execute();
	return $result;
}

/*
 * Desc: serialize data 조회하여 삭제
 *       https://stackoverflow.com/questions/24508164/how-do-i-search-from-serialize-field-in-mysql-database
 */
function if_delete_admin_session($admin_id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_session'];
    
    $query = "
			DELETE FROM
				$if_table1
            WHERE 
                sess_data REGEXP '.*\"admin_id\";i:$admin_id.*'
	";
	$stmt = $ifdb->prepare($query);
	$result = $stmt->execute();
	return $result;
}

// 관리자 수를 조회
function if_get_admin_count() {
    $ifdb = $GLOBALS['ifdb'];
    $pp_table1 = $GLOBALS['if_tbl_admin'];
    
    $query = "
			SELECT
				COUNT(*) AS cnt
			FROM
				$pp_table1
	";
	$stmt = $ifdb->prepare($query);
	$stmt->execute();
	return $ifdb->get_var($stmt);
}

function if_exist_admin($login) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $query = "
			SELECT
				seq_id
			FROM
				$if_table1
			WHERE
				admin_login = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $login);
	$stmt->execute();
	$seq_id = $ifdb->get_var($stmt);
	return $seq_id;
}

function if_get_admin($id) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				seq_id = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('i', $id);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}

function if_get_admin_by_login($login) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $query = "
			SELECT
				*
			FROM
				$if_table1
			WHERE
				admin_login = ?
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('s', $login);
	$stmt->execute();
	return $ifdb->get_row($stmt);
}


function if_update_admin_password($id, $password) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $enc_password = if_encrypt_passwd($password);
    
    $query = "
            UPDATE
                $if_table1
            SET
                admin_pw = ?
            WHERE
                seq_id = ?
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->bind_param('si', $enc_password, $id);
    $result = $stmt->execute();
    return $result;
}

function if_add_sub_admin($level) {
    $ifdb = $GLOBALS['ifdb'];
    $if_table1 = $GLOBALS['if_tbl_admin'];
    
    $login = trim($_POST['admin_login']);
    $admin_pass = if_encrypt_passwd($_POST['admin_pass']);
    $admin_name = empty($_POST['admin_name']) ? '관리자' : trim($_POST['admin_name']);
    
    $query = "
			INSERT INTO
				$if_table1
				(
					seq_id,
					admin_login,
					admin_pw,
					admin_name,
					admin_level,
					create_dt
				)
			VALUES
				(
					NULL, ?, ?, ?, ?, 
                    NOW()
				)
	";
	$stmt = $ifdb->prepare($query);
	$stmt->bind_param('ssss', $login, $admin_pass, $admin_name, $level);
	$stmt->execute();
	$user_id = $stmt->insert_id;
	return $user_id;
}
?>