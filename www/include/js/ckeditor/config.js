/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.image_previewText = CKEDITOR.tools.repeat( '_ ', 10 );
	config.extraPlugins = 'youtube';
	config.removePlugins = 'elementspath';	// remove bottom status bar
	config.resize_enabled = false;			// remove bottom status bar
	config.font_names = '맑은 고딕/Malgun Gothic;굴림/Gulim;돋움/Dotum;바탕/Batang;궁서/Gungsuh;' + config.font_names;
	config.allowedContent = true;
//	config.disableAutoInline = true;
//	config.extraPlugins = 'menubutton';
};
