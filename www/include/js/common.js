$(document).ready(function(e){
	var wWidth = $(window).width();
	if(wWidth < 1201){
		//gnbMobile
		gnbMobile();
		if($('#dim').hasClass('mobile')){
			$('#dim').fadeIn();
		}
	}else{
		//map popup
		mapPopup();
		if($('#dim').hasClass('mobile')){
			$('#dim').fadeOut();
		}
		//gnb
		gnb();	
	}

	//allmenu
	allMenu();
	//visual
	visual();
	//tab
	tab();
	//banner
	banner();
	//term
	term();
	//회원/비회원 체크
	chkMember();
	//select link
	selectLink();
	//faq
	faq();
	//usemap
	mapRes();
	//datepicker
	datepicker();
});
$(window).resize(function(e){
	var wWidth = $(window).width();
	if(wWidth < 1201){
		//gnbMobile
		gnbMobile();
		if($('#dim').hasClass('mobile')){
			$('#dim').fadeIn();
		}
	}else{
		//map popup
		mapPopup();
		if($('#dim').hasClass('mobile')){
			$('#dim').fadeOut();
		}
		//gnb
		gnb();	
		//allmenu
		allMenu();
	}
});
function gnb(){
	$('#gnb > li > a').off('click');
	$('#gnb > li > a').mouseenter(function(e){
		e.stopPropagation();
		$('.sub-gnb-wrap').stop().fadeIn(100);
		$('.sub-gnb').stop().fadeIn(100);
	});
	$('.btn-allmenu').mouseenter(function(e){
		$('.sub-gnb-wrap').stop().fadeOut(100);
		$('.sub-gnb').hide();	
	});
	$('#header, .sub-gnb-wrap').mouseleave(function(e){
		$('.sub-gnb-wrap').stop().fadeOut(100);
		$('.sub-gnb').hide();
	})
}
function gnbMobile(){
	$('.sub-gnb').hide();
	$('#gnb > li > a').off('mouseenter');
	$('#header, .sub-gnb-wrap').off('mouseleave');

	$('#gnb > li > a').off().on('click',function(e){
		e.preventDefault();
		$(this).next('i').toggleClass('on');
		$(this).next().next('.sub-gnb').stop().slideToggle();
		$('#gnb > li > a').not(this).next().next('.sub-gnb').slideUp();
		$('#gnb > li > a').not(this).next().removeClass('on');
	});
	$('.btn-menu').on('click',function(e){
		$('nav, .top').stop().animate({'right':'0'},500);
		$('#dim').fadeIn().addClass('mobile');
	});
	$('.btn-close, #dim').on('click',function(e){
		$('nav, .top').stop().animate({'right':'-100%'},500);
		$('#dim').removeClass('mobile').fadeOut();
	});
}
function allMenu(){
	$('.btn-allmenu').off().on('click',function(e){
		$('.allmenu-wrap').fadeIn(100);
	});
	$('.btn-allmenuclose').on('click',function(e){
		$('.allmenu-wrap').fadeOut(100);
	});
}
function visual(){
	$('.visual').not('.slick-initialized').slick({
		arrows: true,
		dots: true,
		autoplay: true,
		//fade: true,
		infinite: true,
	});
}
function tab(){
	$('.tabcon, .sub-tabcon').hide().first().show();
	$('.tabmenu > li, .sub-tabmenu.has > li').on('click',function(e){
		var cnt = $(this).index();
		$('.tabcon, .sub-tabcon').hide().eq(cnt).show();
		$('.tabmenu > li, .sub-tabmenu.has > li').removeClass('on');
		$(this).addClass('on');
	});
}
function banner(){
	$('.banner-rolling').not('.slick-initialized').slick({
		autoplay: true,
		infinite: true,
		variableWidth: true,
		nextArrow: '<a href="#n" class="btn-arrow btn-prev"><i class="xi-angle-left"></i></a>',
		prevArrow: '<a href="#n" class="btn-arrow btn-next"><i class="xi-angle-right"></i></a>'
	});
}

function term(){
	$('.term-contents').hide().first().show();
	$('#sel-term').on('change',function(e){
		var stat = $('#sel-term option:selected').val();
		$('.term-contents').hide();
		$('.term-contents'+stat).show();
	});
}

function chkMember(){
	$('.chk-mem1, .chk-mem2').on('click',function(e) {
		var thisval = $(this).children().val();
		if(thisval == 'mem'){
			$(this).parent().next().find('.chk-mem-email').addClass('hide').removeClass('show');
			$(this).parent().next().find('.chk-mem-pw').addClass('show').removeClass('hide');
		}
		else if(thisval == 'n-mem'){
			$(this).parent().next().find('.chk-mem-email').addClass('show').removeClass('hide');
			$(this).parent().next().find('.chk-mem-pw').addClass('hide').removeClass('show');
		}
	});
}

function selectLink(){
	$('.sub-link-wrap select').on('change',function(e){
		var value = $(this).val();
		console.log(value);
		window.open(value,'_self');
	});
}

function faq(){
	$('.question-tr').on('click',function(e){
		$(this).children().find('.xi-angle-down').toggleClass('on');
		$('.question-tr').not(this).next('.answer-tr').hide();
		$('.question-tr').not(this).children().find('.xi-angle-down').removeClass('on');
		$(this).next('.answer-tr').toggle();
	})
}

function mapRes(){
	$('img[usemap]').rwdImageMaps();
}

function mapPopup(){
	$('.organ-map').mouseenter(function(e){
		var className = $(this).attr('class');
		var popNum = className.substring(13,15);
		$('.map-pop').hide();
		$('.pop'+popNum).stop().fadeIn(100);
	});
	$('.organ, .map-pop').mouseleave(function(e){
		var className = $(this).attr('class');
		var popNum = className.substring(13,15);
		$('.map-pop').hide();
		$('.pop'+popNum).hide();
	});
}

function datepicker(){
	if($('.datepicker').length){
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			language: 'kr'
		});
	}
}