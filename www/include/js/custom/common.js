//Option Ajax
function getOption(name, target) {
	$.ajax({
		type : "POST",
		url : "/content/ajax/get-option.php",
		data : {
			"name" : name
		},
		dataType : "json",
		success : function(res) {
			$(target).html(res.result);
		}
	});
}