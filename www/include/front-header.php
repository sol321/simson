<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=0,maximum-scale=10,user-scalable=yes">
        <meta name="Author" content="">
        <meta name="Keywords" content="">
        <meta name="Description" content="">
        <title><?php echo json_decode(if_get_option('site_name')) ?></title>
    
        <!-- Inforang custom -->
        <link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/font-awesome/css/font-awesome.min.css">
    	<!-- link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/datepicker3.css"/> -->
    	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css">
    	<link rel="stylesheet" href="<?php echo INC_URL ?>/css/slick.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/reset.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/common.css">
        <link rel="stylesheet" href="<?php echo INC_URL ?>/css/sub.css">
    	
    
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
        <script src="<?php echo INC_URL ?>/js/jquery.min.js"></script>
        <script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap.min.js"></script>
    	<!-- script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap-datepicker.js"></script>
    	<script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap-datepicker.kr.js"></script> -->	
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
    	<script src="<?php echo INC_URL ?>/js/slick.min.js"></script>
        <script src="<?php echo INC_URL ?>/js/common.js"></script>