<div class="header-wrap">
	<div class="top">
		<div class="t-show">
			<h1><a href="/"><img src="/include/img/h1-logo.png" alt="인포랑학회"></a></h1>
		</div>
    <div class="layer1120">
     <form class="top-login-form form-inline" method="post" id="form-item-new_main">
        <fieldset>
        <?php 
		if (if_get_current_user_id()) {
		?>
			<div class="login-txt">
				<b><?php echo if_get_current_user_name();?></b>님 환영합니다.
			</div>		
			<div class="btn-wrap text-right t-show">
				<a href="<?php echo CONTENT_URL ?>/member/logout.php" class="btn btn-default">로그아웃</a>
				<a href="<?php echo CONTENT_URL ?>/mypage/" class="btn btn-default">MYPAGE</a>
			</div>
		<?php   
		} else {
		?>
         <div class="form-group">
            <input type="text" name="user_login" class="form-control" placeholder="ID">
            <input type="password" name="user_pw" class="form-control" placeholder="PW" autocomplete="new-password">
            <button type="submit" class="btn btn-primary">로그인</button>
         </div>		
		<div class="btn-wrap text-right t-show">
			<a href="<?php echo CONTENT_URL ?>/member/signup_terms.php" class="btn btn-default">회원가입</a>
		</div>		
		<?php 
		}
		?>

        </fieldset>
      </form>        
      <ul class="top-menu">
  			<li><a href="/">홈</a></li>
        <?php 
		if (if_get_current_user_id()) {
		?>  			
  			<li><a href="<?php echo CONTENT_URL ?>/member/logout.php">로그아웃</a></li>
  			<li><a href="<?php echo CONTENT_URL ?>/mypage/">MYPAGE</a></li>	
  		<?php } else {?>
   			<li><a href="<?php echo CONTENT_URL ?>/member/login.php">로그인</a></li>
  			<li><a href="<?php echo CONTENT_URL ?>/member/signup_terms.php">회원가입</a></li> 		
  		<?php }?>
				<li><a href="<?php echo CONTENT_URL ?>/sitemap/sitemap.php">사이트맵</a></li>
  			<li><a href="#n">English</a></li>
  		</ul>
    </div>
  </div>
	<header id="header" class="layer1120">
		<h1><a href="/"><img src="/include/img/h1-logo.png" alt="인포랑학회"></a></h1>
		<nav>
			<ul id="gnb">
				<li><a href="<?php echo CONTENT_URL ?>/about/intro.php">학회소개</a> <i class="xi-angle-down t-show"></i>
					<ul class="sub-gnb">
						<li><a href="<?php echo CONTENT_URL ?>/about/intro.php">학회소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/greeting.php">회장인사말</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/history.php">연혁</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/organ.php">조직도</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/president.php">역대 회장</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/membership.php">회원학회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/term.php">정관</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/contact.php">오시는 길</a></li>
					</ul>
				</li>
				<li><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술∙교육∙행사</a> <i class="xi-angle-down t-show"></i>
					<ul class="sub-gnb">
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술·교육·행사 안내</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/register/index_reg.php">사전등록 </a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/register/index_abst.php">초록접수</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/register/confirm.php">등록 / 접수 확인</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_list.php">역대 학술·교육·행사</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">자료실</a></li>
					</ul>
				</li> 
				<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">학술지</a> <i class="xi-angle-down t-show"></i>
					<ul class="sub-gnb">
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">JOI</a></li>
					</ul>
				</li>
				<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">출판</a> <i class="xi-angle-down t-show"></i>
					<ul class="sub-gnb">
						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">국가시험 대비 문제집</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">문제집2</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/publication/exam_regist.php">학습역량평가 모의고사</a></li>
					</ul>
				</li>
				<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">회원공간</a> <i class="xi-angle-down t-show"></i>
					<ul class="sub-gnb">
                    <?php 
            		if (if_get_current_user_id()) {
            		?>  	
						<!-- <li><a href="<?php //echo CONTENT_URL ?>/mypage/">My page</a></li> -->            		
            		<?php } else{?>
						<!-- <li><a href="<?php //echo CONTENT_URL ?>/member/login.php">로그인</a></li>
						<li><a href="<?php //echo CONTENT_URL ?>/member/signup_terms.php">회원가입</a></li>
						<li><a href="<?php //echo CONTENT_URL ?>/member/find.php">아이디/비밀번호 찾기</a></li> -->            		
            		<?php }?>	
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">공지사항</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=2">Q&A</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/member/subscribe.php">학회지 구독신청</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=3">채용공고</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/member/sponsor.php">후원금 신청</a></li>
					</ul>
				</li>
			</ul>
			<a href="#n" class="btn-close t-show"><i class="xi-close"></i></a>
		</nav>
		<a href="#n" class="btn-allmenu">
			<span></span>
			<span></span>
			<span></span>
		</a>
		<a href="#n" class="btn-menu t-show">
			<span></span>
			<span></span>
			<span></span>
		</a>
		<div class="sub-gnb-wrap"></div>
	</header>
	<section class="allmenu-wrap">
		<div class="layer1120">
			<h3>인포랑학회 전체메뉴</h3>
			<ul class="allmenu-list">
				<li><span class="tit">학회소개</span>
					<ul>
						<li><a href="<?php echo CONTENT_URL ?>/about/intro.php">학회소개</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/greeting.php">회장인사말</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/history.php">연혁</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/organ.php">조직도</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/president.php">역대 회장</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/membership.php">회원학회</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/term.php">정관</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/about/contact.php">오시는 길</a></li>
					</ul>
				</li>
				<li><span class="tit">학술∙교육∙행사</span>
					<ul>
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference.php">학술·교육·행사 안내</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_regi.php">사전등록 </a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/extract.php">초록접수</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/extract_confirm.php">등록 / 접수 확인</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/conference/conference_history.php">역대 학술·교육·행사</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=12">자료실</a></li>
					</ul>
				</li>
				<li><span class="tit">학술지</span>
					<ul>
						<li><a href="<?php echo CONTENT_URL ?>/journal/jkan.php">JOI</a></li>
					</ul>
				</li>
				<li><span class="tit">출판</span>
					<ul>
						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">국가시험 대비 문제집</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/publication/book.php">문제집2</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/publication/exam_regist.php">학습역량평가 모의고사</a></li>
					</ul>
				</li>
				<li><span class="tit">회원공간</span>
					<ul>
                    <?php 
            		if (if_get_current_user_id()) {
            		?>  
						<!-- <li><a href="<?php //echo CONTENT_URL ?>/member/mypage.php">My page</a></li> -->            		
            		<?php } else {?>
						<!--<li><a href="<?php //echo CONTENT_URL ?>/member/login.php">로그인</a></li>
						<li><a href="<?php //echo CONTENT_URL ?>/member/signup_terms.php">회원가입</a></li>
						<li><a href="<?php //echo CONTENT_URL ?>/member/find.php">아이디/비밀번호 찾기</a></li>     -->         		
            		<?php }?>					
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=1">공지사항</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=2">Q&A</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/member/subscribe.php">학회지 구독신청</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/community/post_list.php?bt=3">채용공고</a></li>
						<li><a href="<?php echo CONTENT_URL ?>/member/sponsor.php">후원금 신청</a></li>
					</ul>
				</li>
			</ul>
			<a href="#n" class="btn-allmenuclose"><i class="xi-close"></i> 닫기</a>
		</div>
	</section>
	<div id="dim"></div>
</div>
<script>
$(function() {
	$("#form-item-new_main").submit(function(e) {
		e.preventDefault();
		
		$.ajax({
			type : "POST",
			url : "<?php echo CONTENT_URL ?>/member/ajax/login.php",
			data : $("#form-item-new_main").serialize(),
			dataType : "json",
			beforeSend : function() {
				$("#btn-submit").prop("disabled", true);
			},
			success : function(res) {
				if (res.code == "0") {
					location.href = res.redirect;
				} else {
					alert(res.msg);
				}
			}
		}).done(function() {
		}).fail(function() {
		}).always(function() {
			$("#btn-submit").prop("disabled", false);
		}); // ajax
	});
});
</script>  
