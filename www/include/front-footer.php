	<div class="footer-wrap">
		<footer id="footer" class="layer1120">
			<div class="footer-menu">
				<ul class="footer-menu-list">
					<li><a href="#n" data-toggle="modal" data-target="#policy">개인정보취급방침</a></li>
					<li><a href="#n" data-toggle="modal" data-target="#access">이용약관</a></li>
					<li><a href="<?php echo CONTENT_URL ?>/about/contact.php">CONTACT US</a></li>
				</ul>	
			</div>
			<span class="footer-logo"><img src="<?php echo INC_URL ?>/img/footer-logo.png" alt="인포랑학회"></span>
			<div class="footer-con">
				서울특별시 광진구 광나루로 56길 85, 테크노-마트21 31층 (우05116) &nbsp;&nbsp;/&nbsp;&nbsp; FAX : 02-325-7667 <br />
				출판/회원관리 : <a href="tel:02-337-3337">02-337-3337</a> &nbsp;&nbsp;/&nbsp;&nbsp; E-Mail  :  <a href="mailto:info@inforang.com">info@inforang.com</a><br />
				Copyright ⓒ 2011 by MEDrang.INFOrang LTD.
			</div>
		</footer>	
	</div>

<?php 
require_once FUNC_PATH . '/functions-option.php';
?>
  <!-- Modal -->
  <div class="modal fade" id="policy" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">개인정보취급방침</h4>
        </div>
        <div class="modal-body">
    		<?php 
    		$json = if_get_option('privacy_stmt');
    		$option_value = json_decode($json, true);
    		echo $option_value;
    		?>				
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="access" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">이용약관</h4>
        </div>
        <div class="modal-body">
		<?php 
		$json = if_get_option('site_terms');
		$option_value2 = json_decode($json, true);
		echo $option_value2;
		?>			        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

