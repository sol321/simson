<?php
/**
 * Name : Session Class
 * @author softsyw
 *
 */
class Session
{
    private $_sesslife;
    private $_db;
    public $sess_tbl;

    public function __construct($db) {
        $this->_sesslife = 60 * 60 * 24;	// 1 day
        $this->_db = $db;
        $this->sess_tbl = $GLOBALS['if_tbl_session'];

        session_set_save_handler(array(&$this, 'sessOpen'),
            array(&$this, 'sessClose'),
            array(&$this, 'sessRead'),
            array(&$this, 'sessWrite'),
            array(&$this, 'sessDestroy'),
            array(&$this, 'sessGC'));
        register_shutdown_function('session_write_close');
        session_start();
    }

    public function sessOpen($save_path, $session_name) {
        return true;
    }

    public function sessClose() {
        $this->sessGC($this->_sesslife);
        return true;
    }

    public function sessRead($sess_id) {
        $query	= "
			SELECT
				sess_data
			FROM
				$this->sess_tbl
			WHERE
				sess_id = ? AND
				sess_expiry >= ?
		";
		$stmt = $this->_db->prepare($query);
		$stmt->bind_param('si', $sess_id, $time);
		$time = time();
		$stmt->execute();
		$result = $this->_db->get_var($stmt);
		return $result;
    }

    public function sessWrite($sess_id, $sess_data) {
        $expiry = $this->_sesslife + time();

        $query  = "
			SELECT
				sess_id
			FROM
				$this->sess_tbl
			WHERE
				sess_id = ?
		";
		$stmt = $this->_db->prepare($query);
		$stmt->bind_param('s', $sess_id);
		$stmt->execute();
		$stmt->store_result();	// required to fetch $stmt->num_rows

		// UPDATE sess_expiry if the sess_id exists.
		if ($stmt->num_rows > 0 ) {
		    $query = "
        		UPDATE
        			$this->sess_tbl
        		SET
        			sess_expiry = ?,
        			sess_data = ?
        		WHERE
        			sess_id = ?
	        ";
			$stmt = $this->_db->prepare($query);
			$stmt->bind_param('iss', $expiry, $sess_data, $sess_id);
		// create session if a sess_id does not exists.
		} else {
		    $query  = "
        		INSERT INTO
        			$this->sess_tbl
        			(
        				sess_id,
        				sess_created,
        				sess_expiry,
        				sess_data,
        				ip_addr
        			)
        		VALUES
        			(?, ?, ?, ?, ?)
        	";
			$stmt = $this->_db->prepare($query);
			$stmt->bind_param('siiss', $sess_id, $time, $expiry, $sess_data, $ip);
			$time = time();
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$stmt->execute();
		$stmt->free_result();
		$stmt->close();

		return true;
    }

    public function sessDestroy($sess_id) {
        $query  = "
			DELETE FROM
				$this->sess_tbl
			WHERE
				sess_id = ?
		";
		$stmt = $this->_db->prepare($query);
		$stmt->bind_param('s', $sess_id);
		$stmt->execute();
		$stmt->free_result();
		$stmt->close();
		return true;
    }

    public function sessGC($sess_maxlifetime) {
        $query  = "
			DELETE FROM
				$this->sess_tbl
			WHERE
				sess_expiry < ?
		";
		$stmt = $this->_db->prepare($query);
		$stmt->bind_param('i', $time);
		$time = time();
		$stmt->execute();
		$stmt->free_result();
		$stmt->close();
		return true;
    }
}
?>