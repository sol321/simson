<?php
/**
 * Name : Pagination class
 * Usage:
 * $paginator = new PpPaginator($db, $page_no, $rows_count );
 * $result = $paginator->if_init_pagination($query, $sparam );
 */

class Paginator
{
    public $page_id;
    public $rows_per_page;
    public $group_per_page;
    public $total_rows;
    public $total_page;
    public $offset;
    public $db;

    public function __construct($db, $page, $rows_count = null, $group_count = null ) {
        $this->db = $db;
        $this->page_id = empty($page) ? 1 : intval($page );
        $this->rows_per_page = empty($rows_count) ? 10 : intval($rows_count );
        $this->group_per_page = empty($group_count) ? 10 : intval($group_count );
    }

    /*
     * @params : array($type, $pam1, $pam2,...);
     *
     */
    public function if_init_pagination($sql, $params = null ) {
        // get offset of LIMIT clause
        $this->offset = $this->_if_get_offset();

        $query = preg_replace('/^select/i', 'SELECT SQL_CALC_FOUND_ROWS', trim($sql));
        $auto_limit = ' LIMIT ' . $this->offset . ', ' . $this->rows_per_page;

        if (stristr($query, 'LIMIT') === false ) {	// Attach LIMIT clause if a query does not have LIMIT clause.
            $query .= $auto_limit;
        } else {	// Replace LIMIT clause if a query has LIMIT clause.
            $query = preg_replace('/LIMIT .*/i', $auto_limit, $query );
        }

        if ($stmt = $this->db->prepare($query)) {
            if ($params != null ) {
                $refs = array();
                foreach ($params as $key => $val ) {
                    $refs[$key] = &$params[$key];
                }
                call_user_func_array(array($stmt, 'bind_param'), $refs );
            }
            $stmt->execute();
            $rows = $this->db->get_results($stmt);	// result set
        } else {
            $errmsg = '[Query error >>> Failed to prepared the statment. ' . date('Y-m-d H:i:s') . "]\n";
            $errmsg .= 'Query: ' . $query . "\n";
            echo $errmsg;
            // 			error_log($errmsg, 3, LOG_FILE);
            exit('<h4>Error : Cannot execute pagination query</h4>');
        }
        $query = "SELECT FOUND_ROWS() AS total_rows";	// get the count of total total_rows
        $res = $this->db->query($query);
        $row = $res->fetch_assoc();
        $this->total_rows = $row['total_rows'];

        // get total pages from total total_rows
        $this->total_page = ceil($this->total_rows / $this->rows_per_page);

        return $rows;
    }

    public function if_get_total_page() {
        $total_page = $this->total_page == 0 ? 1 : $this->total_page;
        return $total_page;
    }

    public function if_get_total_records() {
        return $this->total_records;
    }

    public function if_get_total_rows() {
        return $this->total_rows;
    }

    private function _if_get_offset() {
        $offset = ($this->page_id - 1) * $this->rows_per_page;
        return $offset;
    }

    /*
     * Desc : CMS에서 사용
     *
     */
    public function if_paginator() {
        $total_group = ceil($this->total_page / $this->group_per_page);
        $cur_group = ceil($this->page_id / $this->group_per_page);

        $first_page_offset = ($cur_group - 1) * $this->group_per_page + 1;
        $last_page_offset = $cur_group * $this->group_per_page;
        if ($cur_group == $total_group) {
            $last_page_offset = $this->total_page;
        }

        // start
        $output = '<ul class="pagination">';
        $parameter = preg_replace('/page=[0-9]{0,}&*/', '', $_SERVER['QUERY_STRING']);
        if (!empty($parameter)) {
            $parameter = '&' . $parameter;
        }

        // 		$output .= '<li class="page-item">';
        // 		$output .= 	'<a class="page-link page-ctrl page-first" href="#" title="첫 페이지로">';
        // 		$output .= 		'<i class="icon icon-chevron-xxs-double-left"></i>';
        // 		$output .= 	'</a>';
        // 		$output .= '</li>';

        if ($this->page_id > 1 ) {
            if ($cur_group > 1 ) {
                $output .= '<li class="page-item">';
                $output .= 	'<a class="page-link page-ctrl page-prev" href="?page=' . ($first_page_offset - 1) . $parameter . '" title="이전 페이지로">';
                $output .= 		'<i class="fa fa-fw fa-angle-double-left"></i>';
                $output .= 	'</a>';
                $output .= '</li>';
            }
        }

        for ($i = $first_page_offset; $i <= $last_page_offset; $i++ ) {

            if ($i == $this->page_id ) {
                $output .= '<li class="page-item active"><a class="page-link" href="?page=' . $i . $parameter . '">' . $i . '</a></li>';
            } else {
                $output .= '<li class="page-item"><a class="page-link" href="?page=' . $i . $parameter . '">' . $i . '</a></li>';
            }
        }

        if ($this->page_id != $this->total_page ) {
            if ($cur_group < $total_group ) {
                $output .= '<li class="page-item">';
                $output .= ' <a class="page-link page-ctrl page-next" href="?page=' . ($last_page_offset + 1) . $parameter . '" title="다음 페이지로">';
                $output .= '	<i class="fa fa-fw fa-angle-double-right"></i>';
                $output .= ' </a>';
                $output .= '</li>';
            }
        }

        // 		$output .= '<li class="page-item">';
        // 		$output .= 	'<a class="page-link page-ctrl page-last" href="#" title="마지막 페이지로">';
        // 		$output .= 		'<i class="icon icon-chevron-xxs-double-right"></i>';
        // 		$output .= 	'</a>';
        // 		$output .= '</li>';

        if ($this->total_page == 0 ) {
            $output = '';
        } else {
            $output .= '</ul>';
        }

        return $output;
    }

    /*
     * Desc : Front에서 사용
     *
     */
    public function if_front_paginator() {
        $total_group = ceil($this->total_page / $this->group_per_page);
        $cur_group = ceil($this->page_id / $this->group_per_page);

        $first_page_offset = ($cur_group - 1) * $this->group_per_page + 1;
        $last_page_offset = $cur_group * $this->group_per_page;
        if ($cur_group == $total_group) {
            $last_page_offset = $this->total_page;
        }

        // start
        $output = '<div class="inner">';
        $parameter = preg_replace('/page=[0-9]{0,}&*/', '', $_SERVER['QUERY_STRING']);
        if (!empty($parameter)) {
            $parameter = '&' . $parameter;
        }

        if ($this->page_id > 1 ) {
            if ($cur_group > 1 ) {
                $output .= 	'<a class="link_pg prev" href="?page=' . ($first_page_offset - 1) . $parameter . '" title="이전 페이지로">';
                $output .= 		'<i class="ico_comm ico_pg_prev">이전</i>';
                $output .= 	'</a>';
            }
        }

        for ($i = $first_page_offset; $i <= $last_page_offset; $i++ ) {

            if ($i == $this->page_id ) {
                $output .= '<a class="link_pg current" href="?page=' . $i . $parameter . '">' . $i . '</a>';
            } else {
                $output .= '<a class="link_pg" href="?page=' . $i . $parameter . '">' . $i . '</a>';
            }
        }

        if ($this->page_id != $this->total_page ) {
            if ($cur_group < $total_group ) {
                $output .= ' <a class="link_pg next" href="?page=' . ($last_page_offset + 1) . $parameter . '" title="다음 페이지로">';
                $output .= '	<i class="ico_comm ico_pg_next">처음</i>';
                $output .= ' </a>';
            }
        }

        if ($this->total_page == 0 ) {
            $output = '';
        } else {
            $output .= '</div>';
        }

        return $output;
    }

    /*
     * Desc : Queue contact ajax pagination
     */
    public function if_print_ajax_pagination($class = '' ) {
        $total_group = ceil($this->total_page / $this->group_per_page);
        $cur_group = ceil($this->page_id / $this->group_per_page);

        $first_page_offset = ($cur_group - 1) * $this->group_per_page + 1;
        $last_page_offset = $cur_group * $this->group_per_page;
        if ($cur_group == $total_group) {
            $last_page_offset = $this->total_page;
        }

        // start
        $output = '<ul class="pagination ' . $class . '">';

        if ($this->page_id > 1 ) {
            if ($cur_group > 1 ) {
                $output .= '<li><a title="' . ($first_page_offset - 1) . '"  aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
            }
        }

        for ($i = $first_page_offset; $i <= $last_page_offset; $i++ ) {

            if ($i == $this->page_id ) {
                $output .= '<li class="active"><a title="' . $i . '">' . $i . '</a></li>';
            } else {
                $output .= '<li><a href="#" title="' . $i . '">' . $i . '</a></li>';
            }
        }

        if ($this->page_id != $this->total_page ) {
            if ($cur_group < $total_group ) {
                $output .= '<li><a href="#" title="' . ($last_page_offset + 1) . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
            }
        }

        if ($this->total_page == 0 ) {
            $output = '';
        } else {
            $output .= '</ul>';
        }

        return $output;
    }
}
?>
