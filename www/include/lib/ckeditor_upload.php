<?php
require_once '../../if-config.php';

// define('UPLOAD_PATH', dirname(__FILE__) . '/upload');
// define('UPLOAD_URL', '/upload');

$tmp_up_dir = UPLOAD_PATH . '/tmp';
$tmp_up_url = UPLOAD_URL . '/tmp';
$use_extension = 1;		// 확장자 사용 여부

$file_ext = strtolower(pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION));  // 확장자
$available_img_ext = array('gif', 'jpg', 'jpeg', 'png');
if (!in_array($file_ext, $available_img_ext)) {
	//     exit('Not available');
	$use_extension = 0;
}

if (!is_dir($tmp_up_dir)) {
	mkdir($tmp_up_dir, 0777, true);
}

if ($use_extension) {
	$new_file_name = if_make_rand() . '.' . $file_ext;
} else {
	$new_file_name = if_make_rand();
}

$upload_path = $tmp_up_dir . '/' . $new_file_name;
$result = move_uploaded_file($_FILES['upload']['tmp_name'], $upload_path );

if ($result) {
	$file_path = $upload_path;
	$file_url = $tmp_up_url . '/' . $new_file_name;
	$file_name = $_FILES['upload']['name'];

	// Callback
	$uploaded = '1';
	$fileName = $file_name;
	$url = $file_url;
} else {
	// Callback
	$uploaded = '0';
	$fileName = '';
	$url = '';
}

$json = compact('uploaded', 'fileName', 'url');
echo json_encode($json);

?>