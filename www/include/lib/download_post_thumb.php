<?php
/*
 * Desc : 파일 다운로드 JSON column(meta_data)
 * 		if_posts > meta_data > thumb_attachment
 */
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

if ( empty($_GET['id']) ) {
    if_js_alert_back('다운로드 파일에 대한 정보가 필요합니다.');
}

if ( !isset($_GET['idx']) ) {
    if_js_alert_back('다운로드 파일에 대한 정보가 필요합니다.');
}

$post_id = $_GET['id'];
$idx = $_GET['idx'];

$post_row = if_get_post($post_id);

if (empty($post_row)) {
    if_js_alert_back('게시글이 존재하지 않습니다.');
}

$meta_data = $post_row['meta_data'];

// JSON
$jdec = json_decode($meta_data, true);

$file_path = $jdec['thumb_attachment'][$idx]['file_path'];
$file_name = $jdec['thumb_attachment'][$idx]['file_name'];

if ( !is_file($file_path) ) {
    if_js_alert_back('파일이 존재하지 않습니다.');
}

$length = filesize($file_path);
$file_name = rawurlencode($file_name);		// firefox에서는 한글 인코딩됨.

// Start Download
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="'. $file_name . '"');
header('Content-Type: application/zip');
header('Content-Transfer-Encoding: binary');
if ( $length > 0 ) {
    header('Content-Length: ' . $length);
}

set_time_limit(0);

$file = fopen($file_path, "rb");

if ( $file ) {
    while( !feof($file) ) {
        print( fread($file, 1024 * 8) );
        flush();
        usleep(20000);		// *** 남은 다운로드 시간을 보여준다.
    }
    @fclose( $file );
}

?>