<?php
/*
 * Desc : 파일 업로드
 * 		tmp 디렉토리에 업로드
 */
require_once '../../if-config.php';

$code = 0;
$msg = '';

$tmp_up_dir = UPLOAD_PATH . '/tmp';
$tmp_up_url = UPLOAD_URL . '/tmp';

if (!is_dir($tmp_up_dir)) {
    mkdir($tmp_up_dir, 0777, true);
}

foreach ($_FILES['attachment']['name'] as $key => $val) {
    $file_ext = strtolower(pathinfo($_FILES['attachment']['name'][$key], PATHINFO_EXTENSION));
    $new_file_name = if_make_rand() . '.' . $file_ext;
    
    $upload_path = $tmp_up_dir . '/' . $new_file_name;
    $result = move_uploaded_file($_FILES['attachment']['tmp_name'][$key], $upload_path);
    
    if ($result) {
        $file_url[$key] = $tmp_up_url . '/' . $new_file_name;
        $file_path[$key] = UPLOAD_PATH . '/tmp/' . $new_file_name;
        $file_name[$key] = $_FILES['attachment']['name'][$key];
    } else {
        $code = 505;
        $msg = '파일을 업로드할 수 없습니다. 관리자에게 문의해 주십시오.';
    }
}

$json = compact('code', 'msg', 'file_url', 'file_path', 'file_name', 'file_url');
echo json_encode($json);

?>