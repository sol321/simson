<?php
/*
 * Desc : 파일 업로드 후 썸네일 생성 (대표이미지)
 * 		tmp 디렉토리에 업로드
 */
require_once '../../if-config.php';
require_once INC_PATH . '/classes/Thumbnail.php';

$code = 0;
$msg = '';

if ( empty($_FILES['attachment']['tmp_name'][0]) ) {
    $code = 101;
    $msg = '이미지 파일을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$file_ext = strtolower(pathinfo( $_FILES['attachment']['name'][0], PATHINFO_EXTENSION ));

if (strcmp($file_ext, 'jpg') && strcmp($file_ext, 'jpeg') && strcmp($file_ext, 'gif') && strcmp($file_ext, 'png')) {
    $code = 102;
    $msg = '이미지 파일 확장자는 jpg, jpeg, png, gif만 가능합니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}


$tmp_up_dir = UPLOAD_PATH . '/tmp';
$tmp_up_url = UPLOAD_URL . '/tmp';

if ( !is_dir($tmp_up_dir) ) {
    mkdir($tmp_up_dir, 0777, true);
}

$thumbnail = new Thumbnail();

foreach ( $_FILES['attachment']['name'] as $key => $val ) {
    
    $new_file_name = if_make_rand() . '.' . $file_ext;
    $upload_path = $tmp_up_dir . '/' . $new_file_name;
    $result = move_uploaded_file( $_FILES['attachment']['tmp_name'][$key], $upload_path );
    
    if ( $result ) {
        $file_url[$key] = $tmp_up_url . '/' . $new_file_name;
        $file_path[$key] = $tmp_up_dir . '/' . $new_file_name;
        $file_name[$key] = $_FILES['attachment']['name'][$key];
        
        $thumb_suffix = '-thumb';
        $thumb_width = 500;
        $thumb_name = $thumbnail->resize_image($file_path[$key], $thumb_suffix, $thumb_width);
        $thumb_path = $tmp_up_dir . '/' . $thumb_name;
        $thumb_url = $tmp_up_url . '/' . $thumb_name;
        
        if (stripos($thumb_name, $thumb_suffix) !== false) {	// thumbnail을 생성한 경우
            @unlink($file_path[$key]);	// 업로드한 원본 이미지 삭제
        }
        
        $file_url[$key] = $thumb_url;
        $file_path[$key] = $thumb_path;
        $file_name[$key] = $thumb_name;
        
    } else {
        $code = 505;
        $msg = '파일을 업로드할 수 없습니다. 관리자에게 문의해 주십시오.';
    }
}

$json = compact('code', 'msg', 'file_url', 'file_path', 'file_name', 'file_url');
echo json_encode( $json );

?>