<?php
require_once '../../pps-config.php';
require_once FUNC_PATH . '/functions-user.php';

pps_authenticate_admin();

$id = $_GET['id'];
$member_row = pps_get_member_by_id($id);

$member_id = $member_row['member_id'];
$member_name = $member_row['member_name'];
$member_pw = $member_row['member_pw'];

require_once ADMIN_PATH . '/includes/cms-header.php';

?>

	</head>
	
	<body class="hold-transition <?php echo ALT_SKIN ?> sidebar-mini">
	
		<div class="wrapper">
		
<?php require_once ADMIN_PATH . '/includes/cms-gnb.php'; ?>		

<?php require_once ADMIN_PATH . '/includes/cms-lnb.php'; ?>	
		
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						멤버 편집
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<form id="form-item-new">
							<input type="hidden" name="id" value="<?php echo $id ?>">
							<input type="hidden" name="cur_member_id" value="<?php echo $member_id ?>">
							<input type="hidden" name="cur_member_name" value="<?php echo $member_name ?>">
							
							<div class="col-md-6">
								<div class="box box-primary">
									<div class="box-body">
										<div class="form-group">
											<label>사번</label>
											<input type="text" name="member_id" id="member_id" class="form-control" value="<?php echo $member_id?>" required>
										</div>
										<div class="form-group">
											<label>이름</label>
											<input type="text" name="member_name" id="member_name" class="form-control" value="<?php echo $member_name?>" required>
										</div>
										<div class="form-group">
											<label>비밀번호</label>
											<input type="password" name="member_pw" id="member_pw" class="form-control" value="">
										</div>
									</div>
									</div><!-- /.box-body -->
									<div class="box-footer">
										<button type="button" class="btn btn-default" id="btn-cancel"><i class="fa fa-mail-reply"></i> 뒤로가기</button>
										<div class="pull-right">
											<button type="submit" class="btn btn-primary" id="submit-btn">
												저장합니다 <span class="hide"><i class="fa fa-refresh fa-spin"></i></span>
											</button>
										</div>

									</div><!-- /.box-footer -->
								</div><!-- /. box -->
						</form>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		</div>
		<!-- ./wrapper -->
		
		<script>
		$(function() {
			$("#btn-cancel").click(function() {
				history.back();
			});
			
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/member-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#submit-btn").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							alert("저장했습니다.");
							location.href = "member_list.php";
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#submit-btn").prop("disabled", false);
				});
			});
		});
		</script>
		
	</body>
</html>