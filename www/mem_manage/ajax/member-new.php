<?php
require_once '../../../pps-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if ( !pps_is_root_admin() ) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['member_id']) ) {
    $code = 103;
    $msg = '사번을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['member_pw']) ) {
    $code = 104;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['member_name']) ) {
    $code = 105;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$member_id= $_POST['member_id'];

if (strlen($member_id) > 20) {
    $code = 203;
    $msg = '아이디는 20자리 미만 입니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if (pps_exist_member($member_id)) {
    $code = 201;
    $msg = '사용중인 아이디가 존재합니다. 다른 아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$id = pps_add_member();

if ( empty($id) ) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$json = compact('code', 'msg');
echo json_encode( $json );

?>
