<?php
require_once '../../pps-config.php';

pps_authenticate_admin();

require_once ADMIN_PATH . '/includes/cms-header.php';

?>

	</head>
	
	<body class="hold-transition <?php echo ALT_SKIN ?> sidebar-mini">
	
		<div class="wrapper">
		
<?php require_once ADMIN_PATH . '/includes/cms-gnb.php'; ?>		

<?php require_once ADMIN_PATH . '/includes/cms-lnb.php'; ?>	
		
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						회원 추가
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
					
						<div class="col-md-6">
							<form id="form-item-upload">
    							<div class="box box-danger">
    								<div class="box-header">
    									<h4>회원 추가 (CSV 파일 업로드)</h4>
    								</div>
    								<div class="box-body">
    									<div class="form-group">
    										<div class="btn btn-warning btn-file">
                        						CSV 파일 선택
                        						<input type="file" name="attachment[]" id="file-attach">
                        					</div>
                        					<div style="margin-top: 10px;">
                        						<ul class="list-group" id="preview-attachment"></ul>
                        					</div>
                        					 <a href="<?php echo ADMIN_URL ?>/member/upload_sample.csv" class="green2-bg" target="_blank">샘플양식보기</a>
    									</div>
    								</div><!-- /.box-body -->
    								<div class="box-footer">
    									<div class="text-center">
        									<button type="button" class="btn btn-default" id="btn-cancel"><i class="fa fa-mail-reply"></i> 뒤로가기</button> &nbsp;
        									<button type="submit" class="btn btn-primary" id="submit-btn">
    											추가합니다 <span class="hide"><i class="fa fa-refresh fa-spin"></i></span>
    										</button>
    									</div>
    								</div><!-- /.box-footer -->
    							</div><!-- /. box -->
							</form>
						</div>
						
						<div class="col-md-6">
							<form id="form-item-new">
    							<div class="box box-primary">
    								<div class="box-header">
    									<h4>개별 회원 추가</h4>
    								</div>
    								<div class="box-body">
    									<div class="form-group">
    										<label>사번</label>
    										<input type="text" name="member_id" id="member_id" class="form-control" required>
    									</div>
    									<div class="form-group">
    										<label>이름</label>
    										<input type="text" name="member_name" id="member_name" class="form-control" required>
    									</div>
    									<div class="form-group">
    										<label>비밀번호</label>
    										<input type="password" name="member_pw" id="member_pw" class="form-control" required>
    									</div>
    								</div><!-- /.box-body -->
    								<div class="box-footer">
    									<div class="text-center">
        									<button type="button" class="btn btn-default" id="btn-cancel"><i class="fa fa-mail-reply"></i> 뒤로가기</button> &nbsp;
        									<button type="submit" class="btn btn-primary" id="submit-btn">
    											추가합니다 <span class="hide"><i class="fa fa-refresh fa-spin"></i></span>
    										</button>
    									</div>
    								</div><!-- /.box-footer -->
    							</div><!-- /. box -->
							</form>
						</div>
						
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		</div>
		<!-- ./wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		
		<script>
		$(function() {
			$("#btn-cancel").click(function() {
				history.back();
			});

			// Upload files 첨부파일
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();

				if (fext != "csv") {
					alert("CSV 파일만 업로드할 수 있습니다.");
					return;
				}

// 				if ( fsize > 20971520 ) {
// 					alert("최대 20M 파일까지 업로드할 수 있습니다.");
// 					return;
// 				}

				$("#form-item-upload").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/upload-attachment.php",
					dataType : "json",
					success: function(xhr) {
						if ( xhr.code == "0" ) {
							var fileLists = '';
							for ( var i = 0; i < xhr.file_url.length; i++ ) {
								fileLists = '<li class="list-group-item list-group-item-warning">' +
													'<input type="hidden" name="file_path" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="file_name" value="' + xhr.file_name[i] + '">' +
													'<input type="hidden" name="file_url" value="' + xhr.file_url[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove del-post-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-attachment").append( fileLists );
							$("#file-attach").val("");
						} else {
							alert( xhr.msg );
						}
					}
				});
			});

			// Delete File 첨부파일
			$(document).on("click", ".del-post-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/delete-attachment.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path"]').val();
							if ( file == res.file_path ) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if ( res.code != "0" ) {
							alert( res.msg );
						}
					}
				});
			});
			
			// 일괄 회원 추가 (CSV)
			$("#form-item-upload").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/member-new-csv.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#submit-btn").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.last_id) {
								alert("등록했습니다.");
							}
							location.href = "member_list.php";
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#submit-btn").prop("disabled", false);
				});
			});
			
			// 개별 회원 추가
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/member-new.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#submit-btn").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							alert("등록했습니다.");
							location.href = "member_list.php";
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#submit-btn").prop("disabled", false);
				});
			});
		});
		</script>
		
	</body>
</html>