<?php
require_once '../../if-config.php';

if_authenticate_admin();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>행사 등록</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<!-- form start -->
						<form id="form-item-new" class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label class="col-md-2 control-label">진행 상황</label>
									<div class="col-md-10">
										<div class="radio">
							<?php 
							foreach ($if_progress_state as $key => $val) {
							?>
											<label>
												<input type="radio" name="event_state" value="<?php echo $key ?>"><?php echo $val ?>
												&nbsp; &nbsp;
											</label>
							<?php 
							}
							?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">행사 분류</label>
									<div class="col-md-10">
										<div class="radio">
							<?php 
							foreach ($if_event_type as $key => $val) {
							?>
											<label>
												<input type="radio" name="event_type" value="<?php echo $key ?>"><?php echo $val ?>
												&nbsp; &nbsp;
											</label>
							<?php 
							}
							?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">행사명</label>
									<div class="col-md-10">
										<input type="text" name="event_name" id="event_name" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">장소</label>
									<div class="col-md-10">
										<input type="text" name="event_place" id="event_place" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">행사 연도</label>
									<div class="col-md-2">
										<input type="text" name="event_year" id="event_year" class="form-control" maxlength="4">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">
										행사 안내문
									</label>
									<div class="col-md-10">
										<textarea id="event_content" name="event_content"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">
										프로그램 표
									</label>
									<div class="col-md-10">
										<textarea id="program_table" name="program_table"></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">학술대회 기간</label>
									<div class="col-md-6">
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="event-period-range" class="form-control pull-right" readonly>
											
											<input type="hidden" name="event_period_from" id="event_period_from">
											<input type="hidden" name="event_period_to" id="event_period_to">
										</div>
										<!-- /.input group -->
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">
										사전 등록 안내문
									</label>
									<div class="col-md-10">
										<textarea id="pre_reg_content" name="pre_reg_content"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">사전 등록 접수 기간</label>
									<div class="col-md-6">
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="pre-reg-range" class="form-control pull-right" readonly>
											
											<input type="hidden" name="pre_reg_from" id="pre_reg_from">
											<input type="hidden" name="pre_reg_to" id="pre_reg_to">
										</div>
										<!-- /.input group -->
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">사전 등록 최대 인원</label>
									<div class="col-md-3">
										<div class="input-group">
											<input type="text" name="pre_reg_max_attendees" class="form-control numeric" placeholder="최대 인원">
											<span class="input-group-addon">명</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">사전 등록 비용</label>
									<div class="col-md-10">
										<div class="bg-gray" style="padding: 10px 0;">
    							<?php 
            					foreach ($if_is_member as $key => $val) {
            					    foreach ($if_is_graduate as $k => $v) {
            					?>	
            								<div class="form-group">
            									<input type="hidden" name="fee_classification[]" value="<?php echo $key .'-' . $k ?>">
            								
            									<label class="col-md-3 control-label">
            										<span class="label label-success"><?php echo $val ?> - <?php echo $v ?></span>
            									</label>
        										<div class="col-md-3">
        											<div class="input-group">
        												<input type="text" name="fee_amount[]" class="form-control numeric" placeholder="금액">
        												<span class="input-group-addon">원</span>
        											</div>
        										</div>
            								</div>
    							<?php 
            					    }
            					}
            					?>
            							</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">
										초록접수 안내문
									</label>
									<div class="col-md-10">
										<textarea id="abstract_content" name="abstract_content"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">초록 접수 기간</label>
									<div class="col-md-6">
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="abstract-range" class="form-control pull-right" readonly>
											
											<input type="hidden" name="abstract_from" id="abstract_from">
											<input type="hidden" name="abstract_to" id="abstract_to">
										</div>
										<!-- /.input group -->
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">초록 분류</label>
									<div class="col-md-3">
										<div class="input-group">
											<div class="input-group-addon">분류명</div>
											<input type="text" id="abstract_classification" class="form-control pull-right">
											<span class="input-group-btn">
												<button type="button" class="btn btn-info btn-flat" id="btn-abstract-classification">추가</button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-10 col-md-offset-2" id="abstract-category">
									</div>
								</div>
								
							</div>
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<!-- jQuery Validate Plugin
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script> -->
		<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<!-- date-range-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/moment/moment.min.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<!-- bootstrap time picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

		<script>
		$(function () {
			$(".numeric").numeric({ negative: false });
			
			// CKEditor file upload
			CKEDITOR.replace("event_content", {
				filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
			});
			CKEDITOR.replace("program_table", {
				filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
			});
			CKEDITOR.replace("pre_reg_content", {
				filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
			});
			CKEDITOR.replace("abstract_content", {
				filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
			});

			// 행사 연도
			$("#event_year").datepicker({
				format: "yyyy",
				viewMode: "years", 
				minViewMode: "years",
				autoclose: true
			});

			// 행사 안내
			$("#event-period-range").daterangepicker({
				locale: {
					format: "YYYY-MM-DD",
					applyLabel: "적용",
					cancelLabel: "취소",
					daysOfWeek: [
						"<span style='color: red;'>일</span>", 
						"월", "화", "수", "목", "금",
						"<span style='color: blue;'>토</span>"
					],
					monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				}
			}, function(start, end) {
				$("#event_period_from").val(start.format("YYYY-MM-DD"));
				$("#event_period_to").val(end.format("YYYY-MM-DD"));
			});

			// 사전 등록
			$("#pre-reg-range").daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				locale: {
					format: "YYYY-MM-DD HH:mm",
					applyLabel: "적용",
					cancelLabel: "취소",
					daysOfWeek: [
						"<span style='color: red;'>일</span>", 
						"월", "화", "수", "목", "금",
						"<span style='color: blue;'>토</span>"
					],
					monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				}
			}, function(start, end) {
				$("#pre_reg_from").val(start.format("YYYY-MM-DD HH:mm"));
				$("#pre_reg_to").val(end.format("YYYY-MM-DD HH:mm"));
			});

			// 초록 접수
			$("#abstract-range").daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				locale: {
					format: "YYYY-MM-DD HH:mm",
					applyLabel: "적용",
					cancelLabel: "취소",
					daysOfWeek: [
						"<span style='color: red;'>일</span>", 
						"월", "화", "수", "목", "금",
						"<span style='color: blue;'>토</span>"
					],
					monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				}
			}, function(start, end) {
				$("#abstract_from").val(start.format("YYYY-MM-DD HH:mm"));
				$("#abstract_to").val(end.format("YYYY-MM-DD HH:mm"));
			});

			//-------------------- 첨부파일 시작
			// 첨부파일 trigger
			$(".btn-upload-attach").click(function() {
				$("#file-attach").click();
			});

			// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
				var max_file_size = $("#max-file-size").val();

				if (fsize > max_file_size) {
					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
													'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
													'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작

			// 첨부파일 삭제
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 첨부파일 삭제
			//-------------------- 첨부파일 끝
			
			//-------------------- 대표이미지 시작
			// 대표이미지 trigger
			$(".btn-upload-thumb").click(function() {
				$("#thumb-attach").click();
			});

			// 대표이미지 Upload 시작
			$("#thumb-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
				var max_file_size = $("#max-file-size").val();

				if (fsize > max_file_size) {
					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists = '<li class="list-group-item list-group-item-info">' +
													'<input type="hidden" name="thumb_path[]" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="thumb_url[]" value="' + xhr.file_url[i] + '">' +
													'<input type="hidden" name="thumb_name[]" value="' + xhr.file_name[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove delete-thumb-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-thumb").html(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});

			// 대표이미지 삭제
			$(document).on("click", ".list-group-item .delete-thumb-attach", function() {
				var file = $(this).parent().parent().find('input[name="thumb_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-thumb li").each(function(idx) {
							var file = $(this).find('input[name="thumb_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 대표이미지 삭제
			//-------------------- 대표이미지 끝

			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances.event_content.updateElement();
				CKEDITOR.instances.program_table.updateElement();
				CKEDITOR.instances.pre_reg_content.updateElement();
				CKEDITOR.instances.abstract_content.updateElement();
//		 		var postContent = CKEDITOR.instances.event_content.getData();

				$.ajax({
					type : "POST",
					url : "./ajax/event-add.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "event_list.php";
							} else {
								alert("행사를 생성할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 초록 분류명 
			$("#abstract_classification").keypress(function(evt) {
				if (evt.which == 13) {
					evt.preventDefault();
				}
			});

			// 초록 분류 등록
			$("#btn-abstract-classification").click(function() {
				var val = $.trim($("#abstract_classification").val());

				if (!val) {
					alert("분류명을 입력해 주십시오.");
					$("#abstract_classification").focus();
					return;
				}

				var item = '<a class="btn btn-app">'
    					 + 	'<span class="badge bg-red remove-ac">삭제</span>'
    					 + 	'<input type="hidden" name="abstract_classification[]" value="' + val + '">'
    					 + 	val
    					 + '</a>';
				$("#abstract-category").append(item);
				$("#abstract_classification").val("");
			});
			
			// 초록 분류 삭제
			$(document).on("click", ".remove-ac", function() {
				$(this).parent().remove();
			});

			// 초록 분류 sortable
			$("#abstract-category").sortable();

			// Default value 세팅
			var eventPeriod = $("#event-period-range").val().split(" - ");
			$("#event_period_from").val(eventPeriod[0]);
			$("#event_period_to").val(eventPeriod[1]);
			
			var preReg = $("#pre-reg-range").val().split(" - ");
			$("#pre_reg_from").val(preReg[0]);
			$("#pre_reg_to").val(preReg[1]);

			var abstractRange = $("#abstract-range").val().split(" - ");
			$("#abstract_from").val(abstractRange[0]);
			$("#abstract_to").val(abstractRange[1]);
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>