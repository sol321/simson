<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['classification'])) {
    $code = 101;
    $msg = '분류명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$classification = $_POST['classification'];

$result = if_add_abstract_classification($classification);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>