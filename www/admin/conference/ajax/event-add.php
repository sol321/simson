<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['event_state'])) {
    $code = 1011;
    $msg = '진행 상황을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['event_type'])) {
    $code = 1012;
    $msg = '행사 분류를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['event_name'])) {
    $code = 101;
    $msg = '행사명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['event_year'])) {
    $code = 102;
    $msg = '행사 연도를  입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

/*
if (empty($_POST['fee_classification'])) {
    $code = 10;
    $msg = '을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['fee_amount'])) {
    $code = 10;
    $msg = '을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
*/

$result = if_add_event();

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>