<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qa3 = empty($_GET['qa3']) ? '' : trim($_GET['qa3']);

$evt_type = empty($_GET['evt_type']) ? '' : $_GET['evt_type'];  // 분류
$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 분류
if (!empty($evt_type)) {
    $sql .= " AND event_type = ?";
    array_push($sparam, $evt_type);
}

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// 날짜별 검색
if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
    $qa2_from = $qa2 . '_from';
    $qa2_to = $qa2 . '_to';
    
    $sql .= " AND ($qa2_from BETWEEN ? AND ?) OR ($qa2_to BETWEEN ? AND ?)";
//     $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach, $period_from_attach, $period_to_attach);
}

// 연도별 조회
if (!empty($qa3)) {
    $sql .= " AND event_year = '$qa3'";
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_conference'] . "
		WHERE
			show_hide = 'show'
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

// 연도 검색
$c_years = if_get_conference_years_by_year();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						학술˙교육˙행사 관리
						<a href="event_add.php" class="btn btn-warning btn-sm">
							새 행사 등록
						</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">

								<div class="row">
									<div class="col-md-2">
										<select name="evt_type" class="form-control">
    										<option value="">- 분류 -</option>
    							<?php
    							foreach ($if_event_type as $key => $val) {
    								$selected = !strcmp($evt_type, $key) ? 'selected' : '';
    							?>
    										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
    							<?php
    							}
    							?>
    									</select>
    								</div>
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="event_name" <?php echo strcmp($qa1, 'event_name') ? '' : 'selected'; ?>>행사명</option>
											<option value="event_place" <?php echo strcmp($qa1, 'event_place') ? '' : 'selected'; ?>>장소</option>
										</select>
									</div>
									<div class="col-md-6">
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    									<div class="col-md-6">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        								</div>
    								</div>
								</div>
								
								<div class="row add-top">
    								<div class="col-md-2">
    									<select name="qa2" class="form-control">
    										<option value="">- 기간 선택 -</option>
    										<option value="event_period" <?php echo !strcmp($qa2, 'event_period') ? 'selected' : ''; ?>>행사 기간</option>
    										<option value="pre_reg" <?php echo !strcmp($qa2, 'pre_reg') ? 'selected' : ''; ?>>사전등록 기간</option>
    										<option value="abstract" <?php echo !strcmp($qa2, 'abstract') ? 'selected' : ''; ?>>초록접수 기간</option>
    									</select>
    								</div>
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
    									</div>
    								</div>
    								<div class="col-md-2">
    									<select name="qa3" id="qa3" class="form-control">
    										<option value="">- 연도 선택 -</option>
    							<?php 
    							if (!empty($c_years)) {
    							    foreach ($c_years as $key => $val) {
    							        $year = $val['event_year'];
    							        $cnt = $val['cnt'];
    							        $selected = $qa3 == $year ? 'selected' : '';
    							?>
    										<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?>년 (<?php echo $cnt ?>)</option>
    							<?php 
    							    }
    							}
    							?>
    									</select>
    								</div>
    							</div>

							</form>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
        					<div class="box-tools">
        						<form class="form-inline">
									
								</form>
        					</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>진행 상황</th>
										<th>행사 분류</th>
										<th>연도</th>
										<th>행사 기간</th>
										<th>행사명</th>
										<th>등록날짜</th>
        								<th>-</th>
    								</tr>
    				<?php
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$event_type = $val['event_type'];
							$event_state = $val['event_state'];
							$event_name = htmlspecialchars($val['event_name']);
							$event_year = $val['event_year'];
							
							$event_period_from = $val['event_period_from'];
							$event_period_to = $val['event_period_to'];
							
							$create_dt = $val['create_dt'];
						?>
									<tr id="id-<?php echo $seq_id ?>">
										<td><?php echo $list_no ?></td>
										<td><?php echo $if_progress_state[$event_state] ?></td>
										<td><?php echo $if_event_type[$event_type] ?></td>
										<td><?php echo $event_year ?></td>
										<td><?php echo $event_period_from ?> ~ <?php echo $event_period_to ?></td>
										<td>
											<a href="./abstract/abstract_list.php?id=<?php echo $seq_id ?>"><?php echo $event_name ?></a>
										</td>
										<td><?php echo substr($create_dt, 0, 10) ?></td>
										<td>
											<a href="event_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">편집</a>
                							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/event-hide.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>