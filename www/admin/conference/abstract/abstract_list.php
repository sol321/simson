<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 ID Error.');
}

$conference_id = intval($_GET['id']);

$evt_row = if_get_event($conference_id);
$event_name = htmlspecialchars($evt_row['event_name']);

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 9;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];
$st = '';

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// condition : select stage
if (!empty($_GET['st'])) {
    $st = $_GET['st'];
    $sql .= " AND select_stage = ?";
    array_push($sparam, $st);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_conference_abstract'] . "
		WHERE
            conference_id = '$conference_id' AND
            show_hide = 'show'
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h4 class="text-danger pull-right">[<?php echo $event_name ?>] &nbsp; </h4>
					
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="abstract_list.php?id=<?php echo $conference_id ?>"><b>초록 접수 리스트</b></a></li>
							<li><a href="../registration/pre_reg_list.php?id=<?php echo $conference_id ?>"><b>사전 등록 리스트</b></a></li>
							<li><a href="../registration/onsite_reg_list.php?id=<?php echo $conference_id ?>"><b>현장 등록 리스트</b></a></li>
						</ul>
					</div>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<input type="hidden" name="id" value="<?php echo $conference_id ?>">
								<div class="row">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="abstract_title" <?php echo strcmp($qa1, 'abstract_title') ? '' : 'selected'; ?>>제목</option>
											<option value="author_name" <?php echo strcmp($qa1, 'author_name') ? '' : 'selected'; ?>>발표자</option>
											<option value="co_author_name" <?php echo strcmp($qa1, 'co_author_name') ? '' : 'selected'; ?>>공저자</option>
										</select>
									</div>
									<div class="col-md-3">
   										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    								</div>
									<div class="col-md-7">
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
								</div>
							</form>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<div class="pull-right">
								<a href="abstract_adopt_mail.php?id=<?php echo $conference_id ?>" class="btn btn-info btn-sm">채택 메일 미리보기</a>
								<a href="download_abstract_list.php?id=<?php echo $conference_id ?>&st=<?php echo $st ?>" class="btn btn-success btn-sm">엑셀 파일 저장</a>
							</div>
						
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<form id="form-abstract-list">	
    							<table class="table table-hover">
    								<tbody>
    									<tr class="active">
        									<!-- <th><input type="checkbox" id="cb-switch"></th> -->
        									<th>#</th>
    										<th>발표 형태</th>
    										<th>초록 분류</th>
    										<th>초록 제목</th>
    										<th>이름</th>
    										<th>연락처</th>
    										<th>소속</th>
    										<th>공저자명</th>
    										<th>채택</th>
    										<th>첨부 파일</th>
    										<th>최종 수정일</th>
            								<th>-</th>
        								</tr>
        				<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$ca_id = $val['ca_id'];
    							
    							$author_name = $val['author_name'];
    							$author_mobile = $val['author_mobile'];
    							$author_email = $val['author_email'];
    							$org_name = $val['org_name'];
    							$co_author_name = $val['co_author_name'];
    							$pt_type = $val['pt_type'];
    							$abstract_title = $val['abstract_title'];
    							$abstract_class = $val['abstract_class'];
    							$abstract_title = htmlspecialchars($val['abstract_title']);
    							$update_dt = $val['update_dt'];
    							$select_stage = $val['select_stage'];
    							
    							$meta_data = json_decode($val['meta_data'], true);
    							$file_attachment = empty($meta_data['file_attachment']) ? '' : $meta_data['file_attachment'];
    						?>
    									<tr id="id-<?php echo $ca_id ?>">
    										<!-- <td><input type="checkbox" name="ca_id[]" value="<?php echo $ca_id ?>"></td> -->
    										<td><?php echo $list_no ?></td>
    										<td><?php echo $if_pt_type[$pt_type] ?></td>
    										<td><?php echo str_replace(',', '<br>', $abstract_class) ?></td>
    										<td><?php echo $abstract_title ?></td>
    										<td>
    											<?php echo $author_name ?><br>
    											<?php echo $author_email ?>
    										</td>
    										<td><?php echo $author_mobile ?></td>
    										<td><?php echo $org_name ?></td>
    										<td><?php echo $co_author_name ?></td>
    										<td><?php echo $if_abstract_selection[$select_stage] ?></td>
    										<td>
    							<?php 
    							foreach ( $file_attachment as $key => $val ) {
    							    $attach_file_path = $val['file_path'];
    // 							    $attach_file_url = $val['file_url'];
    							    $attach_file_name = $val['file_name'];
    							    if (is_file($attach_file_path)) {
    							?>
    											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($attach_file_path) ?>&fn=<?php echo base64_encode($attach_file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $attach_file_name ?>">
    												<i class="fa fa-fw fa-download"></i>
    											</a>
    							<?php 
    							    }
    							}
    							?>
    										</td>
    										<td><?php echo substr($update_dt, 0, 10) ?></td>
    										<td>
                    							<button class="btn btn-success btn-xs action-adopt">채택</button>
                    							<button class="btn btn-warning btn-xs action-reject">미채택</button>
                    							<div class="xs-top"></div>
                    							<button class="btn btn-danger btn-xs action-hide">삭제</button>
    											<a href="abstract_edit.php?id=<?php echo $ca_id ?>" class="badge bg-aqua">편집</a>
    										</td>
    									</tr>
    					<?php
    							$list_no--;
    						}
    					}
    					?>
    								</tbody>
    								<tfoot>
    									<tr>
    										<td colspan="13">
												<a href="abstract_list.php?id=<?php echo $conference_id ?>&st=3000" class="btn btn-info btn-xs">채택된 초록</a>
    											<a href="abstract_list.php?id=<?php echo $conference_id ?>&st=4000" class="btn btn-info btn-xs">미채택 초록</a>
    											<a href="abstract_list.php?id=<?php echo $conference_id ?>&st=1000" class="btn btn-info btn-xs">채택 대기중</a>
    											<a href="abstract_hide_list.php?id=<?php echo $conference_id ?>" class="btn btn-info btn-xs">삭제된 초록</a>
    											<a href="abstract_statistics.php?id=<?php echo $conference_id ?>" class="btn btn-info btn-xs">초록 통계</a>
    										</td>
    									</tr>
    								</tfoot>
    							</table>
    						</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			$("#cb-switch").click(function() {
				var ck = $(this).prop("checked");
				$('input[name="ca_id[]"]').prop("checked", ck);
			});

			// 채택
			$(".action-adopt").click(function(e) {
				e.preventDefault();

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var ids = [];

				ids.push(id);

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-choose.php",
					data : {
						"ids" : ids,
						"type" : "adopt"
					},
					dataType : "json",
					success : function(res) {
						if ( res.code == "0" ) {
							location.reload();
						} else {
							alert( res.msg );
						}
					}
				});
			});
			
			// 미채택
			$(".action-reject").click(function(e) {
				e.preventDefault();

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var ids = [];

				ids.push(id);

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-choose.php",
					data : {
						"ids" : ids,
						"type" : "reject"
					},
					dataType : "json",
					success : function(res) {
						if ( res.code == "0" ) {
							location.reload();
						} else {
							alert( res.msg );
						}
					}
				});
			});
			
			// 휴지통
			$(".action-hide").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return;
				}

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var ids = [];

				ids.push(id);

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-hide.php",
					data : {
						"ids" : ids
					},
					dataType : "json",
					success : function(res) {
						if ( res.code == "0" ) {
							location.reload();
						} else {
							alert( res.msg );
						}
					}
				});
			});
		
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>