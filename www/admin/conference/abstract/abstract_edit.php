<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('초록을 선택해 주십시오.');
}

$ca_id = $_GET['id'];

$row = if_get_abstract($ca_id);

$is_member = $row['is_member'];
$is_graduate = $row['is_graduate'];
$academy = $row['academy'];
$academy_arr = explode(',', $academy);
$author_name = $row['author_name'];
$author_mobile = $row['author_mobile'];
$author_email = $row['author_email'];
$org_name = $row['org_name'];
$co_author_name = $row['co_author_name'];
$pt_type = $row['pt_type'];
$abstract_title = $row['abstract_title'];
$abstract_class = $row['abstract_class'];
$abstract_class_arr = explode(',', $row['abstract_class']);
$apply_dt = $row['apply_dt'];
$update_dt = $row['update_dt'];
$user_id = $row['user_id'];
$conference_id = $row['conference_id'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$org_phone = $jdec['org_phone'];
$passwd = $jdec['passwd'];
$file_attachment = $jdec['file_attachment'];

// Event 조회
$evt_row = if_get_event($conference_id);

$event_type = $evt_row['event_type'];
$event_state = $evt_row['event_state'];
$event_name = htmlspecialchars($evt_row['event_name']);
$event_year = $evt_row['event_year'];

$abstract_from = $evt_row['abstract_from'];
$abstract_to = $evt_row['abstract_to'];

$evt_meta_data = $evt_row['meta_data'];
$evt_jdec = json_decode($evt_meta_data, true);

$abstract_classification = $evt_jdec['abstract_classification'];

// 회원학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>초록 수정</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<!-- form start -->
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="ca_id" value="<?php echo $ca_id ?>">
							<input type="hidden" name="conference_id" value="<?php echo $conference_id ?>">
							<div class="box-body">
								
								<div class="form-group">
            						<label class="col-md-2 control-label">분류</label>
            						<div class="col-md-10">
            							<?php echo $if_event_type[$event_type] ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">행사명</label>
            						<div class="col-md-4">
            							<?php echo $event_name ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">회원 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_member as $key => $val) {
            					$checked = $is_member == $key ? 'checked' : ''; 
            				?>
            								<label>
            									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">대학원 재학 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_graduate as $key => $val) {
            				    $checked = $is_graduate == $key ? 'checked' : '';
            				?>
            								<label>
            									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">회원학회명</label>
            						<div class="col-md-4">
            				<?php 
                            if (!empty($unit_items)) {
                                foreach ($unit_items as $key => $val) {
                                    $checked = in_array($val, $academy_arr) ? 'checked' : '';
                            ?>
                            			<div class="checkbox">
                                        	<label>
                                        		<input type="checkbox" name="member_academy[]" value="<?php echo $val ?>" <?php echo $checked ?>>
                                        		<?php echo $val ?>
                                        	</label>
                                        </div>
            				<?php 
                                }
                            }
            				?>
            						</div>
            					</div>
            					
            					<div class="form-group">
            						<label class="col-md-2 control-label">이름</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="author_name" id="author_name" value="<?php echo $author_name ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">소속(직장)</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">발표 형태</label>
            						<div class="col-md-4">
            							<div class="radio">
            				<?php 
            				foreach ($if_pt_type as $key => $val) {
            				    $checked = strcmp($pt_type, $key) ? '' : 'checked';
            				    
            				?>
            								<label>
            									<input type="radio" name="pt_type" class="pt_type" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">직장전화</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo $org_phone ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">휴대전화</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="author_mobile" id="author_mobile" value="<?php echo $author_mobile ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">이메일</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="author_email" id="author_email" value="<?php echo $author_email ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">공저자명</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="co_author_name" id="co_author_name" value="<?php echo $co_author_name ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">논문 제목</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="abstract_title" id="abstract_title" value="<?php echo $abstract_title ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">초록 분류</label>
            						<div class="col-md-4">
            				<?php 
            				if (!empty($abstract_classification)) {
            				    foreach ($abstract_classification as $key => $val) {
            				        $checked = in_array($val, $abstract_class_arr) ? 'checked' : '';
            				?>
            								<div class="checkbox">
                              					<label>
            										<input type="checkbox" name="abstract_class[]" value="<?php echo $val ?>" <?php echo $checked ?>>
            										<?php echo $val ?>
            									</label>
            								</div>
            				<?php
            				    }
            				}
            				?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">논문 파일</label>
            						<div class="col-md-4">
            							<input type="file" class="form-control" name="attachment[]" id="abstract_file">
            							
            							<ul class="list-group" id="preview-attachment">
            				<?php
    						if (!empty($file_attachment)) {
    							foreach ($file_attachment as $key => $val) {
    								$attach_file_path = $val['file_path'];
    								$attach_file_url = $val['file_url'];
    								$attach_file_name = $val['file_name'];
    								if (is_file($attach_file_path)) {
    						?>
    										<li class="list-group-item list-group-item-info">
    											<input name="file_path[]" value="<?php echo $attach_file_path ?>" type="hidden">
    											<input name="file_url[]" value="<?php echo $attach_file_url ?>" type="hidden">
    											<input name="file_name[]" value="<?php echo $attach_file_name ?>" type="hidden">
    											<span class="badge"><span class="glyphicon glyphicon-remove hide-file-attach" style="cursor: pointer;"></span></span>
    											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($attach_file_path) ?>&fn=<?php echo base64_encode($attach_file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $attach_file_name ?>">
    												<?php echo $attach_file_name ?>
    											</a>
    										</li>
    						<?php
    								}
    							}
    						}
    						?>
            							</ul>
            							
            						</div>
            					</div>
            		<?php 
            		if (empty($user_id)) {    // 비회원
            		?>
            					<div class="form-group">
            						<label class="col-md-2 control-label">비밀번호</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="passwd" id="passwd" maxlength="20" value="<?php echo $passwd ?>">
            						</div>
            					</div>
            		<?php 
            		}
            		?>
            					
								
							</div>
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<!-- jQuery Validate Plugin
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script> -->
		<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		
		<script>
		$(function () {
			$(".numeric").numeric({ negative: false });
			
			//-------------------- 첨부파일 시작
			// 첨부파일 trigger
			$(".btn-upload-attach").click(function() {
				$("#file-attach").click();
			});

			// 첨부파일 Upload
			$("#abstract_file").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
													'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
													'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작

			// 첨부파일 삭제
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 첨부파일 삭제
			// 첨부파일 숨기기
			$(document).on("click", ".hide-file-attach", function() {
				$(this).closest("li").fadeOut("slow", function() { $(this).remove(); });
			});
			//-- 첨부파일 숨기기
			//-------------------- 첨부파일 끝

			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "abstract_list.php?id=" + res.conference_id;
							} else {
								alert("행사를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>