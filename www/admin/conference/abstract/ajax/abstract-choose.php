<?php
/*
 * Desc: 채택/미채택 처리
 *      파라미터 ids는 배열입니다. 
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['ids'])) {
    $code = 101;
    $msg = '초록을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['type'])) {
    $code = 102;
    $msg = '처리 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_choose_abstract();

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
