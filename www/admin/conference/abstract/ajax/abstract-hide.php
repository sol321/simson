<?php
/*
 * Desc: 초록 삭제 (hide) 
 * 
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['ids'])) {
    $code = 101;
    $msg = '초록을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_switch_state_abstract('hide');

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
