<?php
/*
 * Desc: 메일 발송
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['conference_id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['sender_email'])) {
    $code = 1012;
    $msg = '보내는 사람 이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['receiver_email']) && empty($_POST['receiver_group'])) {
    $code = 102;
    $msg = '받는 사람 이메일을 입력하거나 받는 그룹을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_subject'])) {
    $code = 103;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_content'])) {
    $code = 125;
    $msg = '메일 내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$conference_id = $_POST['conference_id'];

$mail_id = if_add_mail_content();   // 메일 내용 등록

if (!empty($_POST['receiver_email'])) {
    $r_email = $_POST['receiver_email'];
    $result = if_send_mail_abstract_test($conference_id, $mail_id, $r_email);
}

if (!empty($_POST['receiver_group'])) {
    $r_group = $_POST['receiver_group'];
    $result = if_send_mail_abstract_attendees($conference_id, $mail_id, $r_group);
}

$json = compact('code', 'msg', 'result', 'conference_id');
echo json_encode($json);

?>
