<?php
/*
 * Desc: 초록 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['conference_id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['ca_id'])) {
    $code = 1012;
    $msg = '초록을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_member'])) {
    $code = 102;
    $msg = '회원 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_graduate'])) {
    $code = 103;
    $msg = '대학원 재학 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['member_academy'])) {
    $code = 125;
    $msg = '회원학회를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_name'])) {
    $code = 104;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_mobile'])) {
    $code = 105;
    $msg = '휴대전화를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author_email'])) {
    $code = 106;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['pt_type'])) {
    $code = 107;
    $msg = '발표 형태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['file_path'])) {
    $code = 108;
    $msg = '파일을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$ca_id = $_POST['ca_id'];
$conference_id = $_POST['conference_id'];

if_preserve_abstract_history($ca_id);   // 변경 내용 기록

$result = if_update_abstract();

$json = compact('code', 'msg', 'result', 'conference_id');
echo json_encode($json);

?>
