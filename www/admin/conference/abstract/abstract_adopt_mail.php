<?php
/*
 * Desc : 초록 채택자에게 메일 발송 
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 ID Error.');
}

$conference_id = intval($_GET['id']);

$row = if_get_event($conference_id);
$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];

$event_period_from = $row['event_period_from'];
$event_period_to = $row['event_period_to'];
$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];
$abstract_from = $row['abstract_from'];
$abstract_to = $row['abstract_to'];
$event_place = $row['event_place'];
$create_dt = $row['create_dt'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$event_content = $jdec['event_content'];
$pre_reg_content = $jdec['pre_reg_content'];
$pre_reg_max_attendees = $jdec['pre_reg_max_attendees'];
$abstract_content = $jdec['abstract_content'];
$user_class_fee = $jdec['user_class_fee'];
$abstract_classification = $jdec['abstract_classification'];

$default_mail_body = if_get_default_mail_template('');    // 기본 메일 본문
$option_site_name = json_decode(if_get_option('site_name'), true);
$option_site_email_from = json_decode(if_get_option('site_email_from'), true);      // 발신 이메일주소

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						초록 신청자 메일 발송
						<small><?php echo $event_name ?></small>
					</h1>
				</div>
				<div class="content">
					<div class="box box-warning">
						<!-- form start -->
                		<form id="form-item-new" class="form-horizontal">
                			<input type="hidden" id="max-file-size" value="" ><!-- 첨부파일 용량 -->
                			<input type="hidden" name="conference_id" value="<?php echo $conference_id ?>">
					
    						<div class="box-body">
        						<div class="form-group">
        							<label class="col-md-2 control-label">보내는 사람</label>					
        							<div class="col-md-10">
        								<input class="form-control" id="sender_email" name="sender_email" placeholder="보내는 사람 이메일" value="<?php echo $option_site_email_from ?>">
        							</div>
        						</div>
        						<div class="form-group">
        							<label class="col-md-2 control-label">받는 사람</label>
        							<div class="col-md-10">
        								<input class="form-control" id="receiver_email" name="receiver_email" placeholder="test@test.com">
        							</div>
        						</div>
        						<div class="form-group">
        							<label class="col-md-2 control-label">받는 그룹</label>
        							<div class="col-md-10">
        								<div class="checkbox">
        						<?php 
        						foreach ($if_abstract_selection as $key => $val) {
        						    $count = if_get_count_abstract_selection($conference_id, $key);
        						?>
        									<label>
        										<input type="checkbox" name="receiver_group[]" value="<?php echo $key ?>"><?php echo $val ?>(<?php echo number_format($count) ?>)
        									</label> &nbsp; &nbsp;
        						<?php 
        						}
        						?>
        								</div>
        							</div>
        						</div>
        						<div class="form-group">
        							<label class="col-md-2 control-label">메일 제목</label>
        							<div class="col-md-10">
        								<input class="form-control" id="mail_subject" name="mail_subject" placeholder="메일 제목">
        							</div>
        						</div>
        						<div class="form-group">
        							<div class="col-md-offset-2 col-md-10">
            							<textarea id="mail_content" name="mail_content" class="form-control"><?php echo $default_mail_body ?></textarea>
    									<div class="bg-warning" style="padding: 7px;">
    										<strong style="color: #aaa;">치환문자</strong><br>
    										이름   <b>{:name:}</b> &nbsp; &nbsp;
    										<!-- 회원아이디  <b>{:login:}</b> -->
    									</div>
    								</div>
        						</div>
        						<div class="form-group">
        							<div class="col-md-offset-2 col-md-6">
        								<div class="btn btn-default btn-file">
                                        	<i class="fa fa-paperclip"></i> 첨부파일
                                        	<input type="file" name="attachment[]" id="file-attach" multiple>
                                        </div>
                                        <ul class="list-group" id="preview-attachment"></ul>
        							</div>
        						</div>
    						</div>
    						<!-- /.box-body -->
        					<div class="box-footer text-center">
    							<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
    							<button type="submit" id="btn-submit" class="btn btn-primary">발송</button>
        					</div>
        					<!-- /.box-footer -->
        				</form>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		
		<script>
		$(function () {
			CKEDITOR.replace("mail_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?",
    	    	height: '350px'
    	    });

			// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) { 
						if (xhr.code == "0") { 
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
    												'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
    												'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
    												'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
    												'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
    												xhr.file_name[i] +
    											'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$("#file-attach").val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작
			
			// 파일 삭제 
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});			

    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances.mail_content.updateElement();

				$.ajax({
					type : "POST",
					url : "./ajax/abstract-adopt-mail-send.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("메일을 발송했습니다.");
							} else {
								alert("메일을 발송할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

    		$("#btn-cancel").click(function() {
				history.back();
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>