<?php
/*
 * Desc : 초록 접수 리스트 다운로드
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/PHPExcel.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('ID Error.');
}

$conference_id = intval($_GET['id']);
$st = $_GET['st'];

$filename = '초록 접수 리스트.xlsx';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Inforang")
->setLastModifiedBy("Softsyw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Abstract");

$active_sheet = $objPHPExcel->setActiveSheetIndex(0);

$column_array = array(
    '회원 여부', '대학원 재학 여부', '회원학회명', '이름', '소속(직장)', '발표 형태', '직장 전화', '휴대전화', '이메일',
    '공저자명', '논문 제목', '초록 분류', '논문 파일', '등록날짜', '채택여부'
);

$active_sheet->fromArray( $column_array );

$sql = '';

if (!empty($st)) {
    $sql = " AND select_stage = '$st'";
}

$query = "
		SELECT
            ca.is_member,
            ca.is_graduate,
            ca.academy,
            ca.author_name,
            ca.org_name,
            ca.pt_type,
            JSON_UNQUOTE(ca.meta_data->'$.org_phone') AS org_phone,
            ca.author_mobile,
            ca.author_email,
            ca.co_author_name,
            ca.abstract_title,
            ca.abstract_class,
            JSON_UNQUOTE(JSON_EXTRACT(ca.meta_data, '$.file_attachment[0].file_name')) AS file_attach,
            ca.apply_dt,
            ca.select_stage
		FROM
			" . $GLOBALS['if_tbl_conference_abstract'] . " AS ca
		WHERE
			ca.conference_id = '$conference_id'
            $sql
		ORDER BY
			ca.ca_id DESC
";
$stmt = $ifdb->prepare($query);
$stmt->execute();
$item_results = $ifdb->get_results($stmt);

foreach ($item_results as $key => $val) {
    $item_results[$key]['is_member'] = @$if_is_member[$val['is_member']];
    $item_results[$key]['is_graduate'] = @$if_is_graduate[$val['is_graduate']];
    $item_results[$key]['select_stage'] = $if_abstract_selection[$val['select_stage']];
}

$active_sheet->fromArray( $item_results, null, "A2" );

$active_sheet->getDefaultColumnDimension()->setWidth(20);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('초록 접수 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '. gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>