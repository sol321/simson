<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 ID Error.');
}

$conference_id = intval($_GET['id']);

$row = if_get_event($conference_id);
$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];

$event_period_from = $row['event_period_from'];
$event_period_to = $row['event_period_to'];
$pre_reg_from = $row['pre_reg_from'];
$pre_reg_to = $row['pre_reg_to'];
$abstract_from = $row['abstract_from'];
$abstract_to = $row['abstract_to'];
$event_place = $row['event_place'];
$create_dt = $row['create_dt'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);

$event_content = $jdec['event_content'];
$pre_reg_content = $jdec['pre_reg_content'];
$pre_reg_max_attendees = $jdec['pre_reg_max_attendees'];
$abstract_content = $jdec['abstract_content'];
$user_class_fee = $jdec['user_class_fee'];
$abstract_classification = $jdec['abstract_classification'];

// 회원학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);
$unit_items_count = count($unit_items);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						초록 통계
						<small><?php echo $event_name ?></small>
					</h1>
				</div>
				<div class="content">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="abstract_statistics.php?id=<?php echo $conference_id ?>"><b>회원 학회별</b></a></li>
							<li class="active"><a href="abstract_statistics_graduate.php?id=<?php echo $conference_id ?>"><b>대학원 재학 여부</b></a></li>
						</ul>
					</div>
					<div class="box box-warning">
						<div class="box-body">
							<table class="table">
								<tr>
									<th class="bg-orange">분야\</th>
					<?php 
                    foreach ($if_is_graduate as $key => $val) {
                        $total_sum[$key] = 0;
                    ?>
                    				<th class="bg-info text-center"><?php echo $val ?></th>
                    <?php 
                    }
                    ?>
								</tr>
								
					<?php 
    				if (!empty($abstract_classification)) {
    				    foreach ($abstract_classification as $key2 => $val2) {
    				?>
								<tr>
									<td class="bg-info"><?php echo $val2 ?></td>
						<?php 
						foreach ($if_is_graduate as $key => $val) {
						    $query = "
                                    SELECT
                                        COUNT(*)
                                    FROM "
                                        . $GLOBALS['if_tbl_conference_abstract'] . "
                                    WHERE
                                        is_graduate = '$key' AND
                                        abstract_class LIKE '%$val2%'
                            ";
                            $stmt = $ifdb->prepare($query);
                            $stmt->execute();
                            $count = $ifdb->get_var($stmt);
                            
                            $total_sum[$key] += $count;
						?>
									<td class="text-center">
										<h3><?php echo number_format($count) ?></h3>
									</td>
						<?php 
						}
						?>
								</tr>
					<?php 
    				    }
    				}
					?>
								<tr>
									<th class="bg-orange">합계</th>
						<?php 
						foreach ($if_is_graduate as $key => $val) {
						?>
									<th class="bg-info text-center">
										<h3><?php echo number_format($total_sum[$key]) ?></h3>
									</th>
						<?php 
						}
						?>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function () {
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>