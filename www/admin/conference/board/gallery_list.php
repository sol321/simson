<?php
/*
 * Desc: 학술대회용 갤러리 게시판 리스트
 *      template_id: NULL
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 12;		// 리스트 개수

// search
$qa = empty($_GET['qa']) ? '' : trim($_GET['qa']);
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 학회 검색
if (!empty($qa)) {
    $sql .= " AND post_type_secondary = ?";
    array_push($sparam, $qa);
}

// 검색어
if (!empty($q)) {
    $sql .= " AND (post_content LIKE ? OR post_title LIKE ?) ";
    array_push($sparam, '%' . $q . '%', '%' . $q . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_posts'] . "
		WHERE
			post_type = 'conf_gallery' AND
			post_state = 'open'
			$sql
		ORDER BY
			post_order DESC,
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
// $total_page = $paginator->if_get_total_page();

$conf_results = if_get_all_conference();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						갤러리
						<a href="gallery_add.php" class="btn btn-info btn-sm">
							새 글 등록
						</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
        					<div class="box-tools">
        						<form class="form-inline">
									<div class="input-group">
    									<select name="qa" class="form-control input-sm">
    										<option value="">- 선택 -</option>
    						<?php 
    						if (!empty($conf_results)) {
    						    foreach ($conf_results as $key => $val) {
    						        $event_id = $val['seq_id'];
    						        $event_name = $val['event_name'];
    						        $event_year = $val['event_year'];
    						        $event_place = $val['event_place'];
    						        $selected = $event_id == $qa ? 'selected' : '';
    						?>
    										<option value="<?php echo $event_id ?>" <?php echo $selected ?>> (<?php echo $event_year ?>) <?php echo $event_name ?> (<?php echo $event_place ?>)</option>
    						<?php 
    						    }
    						}
    						?>
    									</select>
									</div>
									<div class="input-group">
										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="Search">
										<div class="input-group-btn">
											<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
										</div>
									</div>
								</form>
        					</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<div class="clearfix gallery gallery-media gallery-photo">
								<div class="row">
						<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$post_id = $val['seq_id'];
    							$post_title = htmlspecialchars($val['post_title']);
    							$post_date = $val['post_date'];
    							$post_type_secondary = $val['post_type_secondary'];  // if_conference > seq_id
    							$post_order = $val['post_order'];
    							
    							$meta_data = $val['meta_data'];
    							$thumb = if_get_val_from_json($meta_data, 'thumb_attachment');
    							$file_attach = if_get_val_from_json($meta_data, 'file_attachment');  // 대표이미지 없는 경우, 첫 사진을 썸네일로 사용하기 위함.
    							$thumb_url = empty($thumb[0]) ? $file_attach[0]['file_url'] : $thumb[0]['file_url'];
    							
    							$event_name = if_get_conference_name($post_type_secondary);
    							
    							// 상단(Top) 여부
    							$notice_label = empty($post_order) ? '' : '<span class="label label-danger">필독</span>';
						?>
    								<div class="col-md-4 col-md-3">
    									<div class="thumbnail">
    										<h4 class="label label-success"><?php echo $event_name ?></h4>
    										
    										<img src="<?php echo $thumb_url ?>" alt="대표 이미지">
    										
    										<div class="caption" style="height: 100px;">
    											<h5 class="ellipsis_2"><?php echo $post_title ?></h5>
    											<div>
    												<div style="float: left;"><?php echo substr($post_date, 0, 10) ?></div>
    												<div class="text-right" id="post-id-<?php echo $post_id ?>">
    													<a href="gallery_edit.php?id=<?php echo $post_id ?>" class="badge bg-aqua">편집</a>
                    									<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
    												</div>
    											</div>
    										</div>
    									</div>
    								</div>
						<?php 
    						}
    					}
						?>
								</div>
							</div>

						</div><!-- /.box-body -->
    					<div class="box-footer">
        			<?php 
        			if (!empty($q) || !empty($qa)) {
        			?>
    						<div class="pull-right">
    							<a href="gallery_list.php" class="btn btn-info btn-sm">리스트</a>
    						</div>
    				<?php 
        			}
    				?>
    						<div class="text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        					</div>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
    	$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("div").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/gallery-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>