<?php
/*
 * Desc: 학술대회용 갤러리 등록
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

$conf_results = if_get_all_conference();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>갤러리 등록 <small>학술˙교육˙행사</small></h1>
				</div>
				<div class="content">
    				<div class="box box-info">
        				<div class="box-header with-border">
        					<h3 class="box-title">새 갤러리 등록</h3>
        				</div>
        				<!-- /.box-header -->
        				<!-- form start -->
        				<form id="form-item-new">
        					<input type="hidden" name="post_type" value="conf_gallery">
        					<div class="box-body">
        						<div class="form-group">
									<label>
										<input type="checkbox" id="post_order" name="post_order" value="1"> 상단에 노출합니다.
									</label>
								</div>
								<div class="form-group">
									<label>행사 선택</label>
									<select id="post_type_secondary" name="post_type_secondary" class="form-control">
										<option value="">- 선택 -</option>
						<?php 
						if (!empty($conf_results)) {
						    foreach ($conf_results as $key => $val) {
						        $event_id = $val['seq_id'];
						        $event_name = $val['event_name'];
						        $event_year = $val['event_year'];
						        $event_place = $val['event_place'];
						?>
										<option value="<?php echo $event_id ?>"><?php echo $event_name ?> (<?php echo $event_year ?> - <?php echo $event_place ?>)</option>
						<?php 
						    }
						}
						?>
									</select>
								</div>
								<div class="form-group">
									<label>글쓴이</label>
									<input type="text" name="post_name" id="post_name" required class="form-control" placeholder="글쓴이" value="<?php echo if_get_current_admin_name() ?>">
								</div>
								<div class="form-group">
									<label>제목</label>
									<input type="text" id="post_title" name="post_title" class="form-control" placeholder="제목">
								</div>
								<div class="form-group">
									<label>내용</label>
									<textarea id="post_content" name="post_content"></textarea>
								</div>
								<div class="form-group">
									<div class="btn btn-default btn-file">
                                    	<i class="fa fa-photo"></i> 대표 이미지
                                    	<input type="file" name="attachment[]" id="thumb-attach">
                                    </div>
                                    <ul class="list-group" id="preview-thumb"></ul>
								</div>
								<div class="form-group">
									<div class="btn btn-success btn-file">
                                    	<i class="fa fa-upload"></i> 사진 업로드
                                    	<input type="file" name="attachment[]" id="file-attach" multiple>
                                    </div>
                                    <div class="row" id="preview-attachment"></div>
                                    
								</div>
        					</div>
        					<!-- /.box-body -->
        					<div class="box-footer text-center">
        						<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
        					</div>
        					<!-- /.box-footer -->
        				</form>
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>

    	<script>
    	$(function () {
    		// CKEditor file upload
    		CKEDITOR.replace("post_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
    	    });

    		//-------------------- 첨부파일 시작
    		// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<div class="col-md-6 col-md-4 gallery-thumb-box">' +
                									'<div class="thumbnail">' +
                    									'<img src="' + xhr.file_url[i] + '" alt="갤러리 ' + xhr.file_name[i] + '">' +
                    									'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
            											'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
            											'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
                        								'<div class="caption">' +
                        									'<h3>' + xhr.file_name[i] + '</h3>' +
                        									'<button type="button" class="btn btn-danger btn-xs delete-file-attach">삭제</button>' +
                        								'</div>' +
                        							'</div>' +
                        						'</div>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작

			// 첨부파일 삭제
			$(document).on("click", ".delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment div.gallery-thumb-box").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 첨부파일 삭제
			//-------------------- 첨부파일 끝
			
    	    //-------------------- 대표이미지 시작
    		// 대표이미지 Upload 시작
			$("#thumb-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/thumbnail_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists = '<li class="list-group-item list-group-item-info">' +
												'<img src="' + xhr.file_url[i] + '" alt="' + xhr.file_name[i] + '" style="width: 150px;">' +
												'<input type="hidden" name="thumb_path[]" value="' + xhr.file_path[i] + '">' +
												'<input type="hidden" name="thumb_url[]" value="' + xhr.file_url[i] + '">' +
												'<input type="hidden" name="thumb_name[]" value="' + xhr.file_name[i] + '">' +
												'<span class="badge"><span class="glyphicon glyphicon-remove delete-thumb-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
											'</li>';
							}
							$("#preview-thumb").html(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});

			// 대표이미지 삭제
			$(document).on("click", ".list-group-item .delete-thumb-attach", function() {
				var file = $(this).parent().parent().find('input[name="thumb_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-thumb li").each(function(idx) {
							var file = $(this).find('input[name="thumb_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 대표이미지 삭제
			//-------------------- 대표이미지 끝

			// 폼 전송
    		$("#form-item-new").submit(function(e) {
        		e.preventDefault();
        		CKEDITOR.instances.post_content.updateElement();
        	});

    		$("#form-item-new").validate({
    			rules: {
    				post_type_secondary: {
    					required: true
    				},
    				post_title: {
    					required: true
    				}
    			},
    			messages: {
    				post_type_secondary: {
    					required: "행사를 선택해 주십시오."
    				},
    				post_title: {
    					required: "제목을 입력해 주십시오."
    				}
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/gallery-add.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							location.href = "gallery_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    
    		$("#btn-cancel").click(function() {
    			history.back();
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>