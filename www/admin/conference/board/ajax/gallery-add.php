<?php
/*
 * Desc: 학술대회용 갤러리 등록
 *  post_type: conf_gallery
 *  post_type_secondary: 학술˙교육˙행사 ID
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_type_secondary'])) {
    $code = 102;
    $msg = '행사에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_title'])) {
    $code = 103;
    $msg = '제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['file_path'])) {
    $code = 111;
    $msg = '사진을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_post();

if (empty($result)) {
    $code = 201;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>