<?php
/*
 * Desc: Post 삭제
 *  if_posts
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 102;
    $msg = '게시글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$post_id = $_POST['id'];

$post_row = if_get_post($post_id);
$jdec = json_decode($post_row['meta_data'], true);

// 첨부 파일 삭제
if (!empty($jdec['post_attachment'])) {
    foreach ($jdec['post_attachment'] as $key => $val) {
        @unlink($val['post_attach_path']);
    }
}

// CKEditor에서 첨부한 파일 삭제
if (!empty($jdec['ckeditor_images'])) {
    foreach ($jdec['ckeditor_images'] as $key => $val) {
        $file = ABSOLUTE_PATH . $val;
        @unlink($file);
    }
}

if_delete_post($post_id);

$json = compact('code', 'msg');
echo json_encode($json);
?>