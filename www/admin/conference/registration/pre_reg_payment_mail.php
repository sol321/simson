<?php
/*
 * Desc: 사전등록 입금 완료 메일 양식
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('사전등록자 정보가 필요합니다.');
}

$cr_id = $_GET['id'];

$row = if_get_pre_registration($cr_id);

$is_member = $row['is_member'];
$is_graduate = $row['is_graduate'];
$user_name = $row['user_name'];
$user_mobile = $row['user_mobile'];
$user_email = $row['user_email'];
$org_name = $row['org_name'];
$fee_amount = $row['fee_amount'];
$pay_state = $row['pay_state'];
$registration_type = $row['registration_type'];
$user_id = $row['user_id'];
$conference_id = $row['conference_id'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$org_phone = @$jdec['org_phone'];
$bank_name = $jdec['bank_name'];
$admin_memo = @$jdec['admin_memo'];

$default_mail_body = if_get_default_mail_template('');    // 기본 메일 본문
$option_site_name = json_decode(if_get_option('site_name'), true);
$option_site_email_from = json_decode(if_get_option('site_email_from'), true);      // 발신 이메일주소

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						입금완료 안내 메일 
					</h1>
				</div>
				<div class="content">
					<div class="col-md-10">
    					<div class="box box-warning">
    						<!-- form start -->
                    		<form id="form-item-new" class="form-horizontal">
                    			<input type="hidden" name="cr_id" value="<?php echo $cr_id ?>">
                    			
        						<div class="box-body">
            						<div class="form-group">
            							<label class="col-md-2 control-label">보내는 사람</label>					
            							<div class="col-md-10">
            								<input class="form-control" id="sender_email" name="sender_email" placeholder="보내는 사람 이메일" value="<?php echo $option_site_email_from ?>">
            							</div>
            						</div>
            						<div class="form-group">
            							<label class="col-md-2 control-label">받는 사람</label>
            							<div class="col-md-10">
            								<input class="form-control" id="receiver_email" name="receiver_email" placeholder="test@test.com" value="<?php echo $user_email ?>">
            								
            								<div class="callout callout-info">
            									<p>
            									이름 : <?php echo $user_name ?> / 
            									휴대전화 : <?php echo $user_mobile ?> / 
            									금액 : <?php echo number_format($fee_amount) ?>원 / 
            									계좌정보 : <?php echo $bank_name; ?>
            									</p>
            								</div>
            								
            							</div>
            						</div>
            						<div class="form-group">
            							<label class="col-md-2 control-label">메일 제목</label>
            							<div class="col-md-10">
            								<input class="form-control" id="mail_subject" name="mail_subject" placeholder="메일 제목" value="[<?php echo $option_site_name ?>] 무통장 입금 납입완료 확인 메일입니다. ">
            							</div>
            						</div>
            						<div class="form-group">
            							<div class="col-md-12">
                							<textarea id="mail_content" name="mail_content" class="form-control"><?php echo $default_mail_body ?></textarea>
        								</div>
            						</div>
            						
        						</div>
        						<!-- /.box-body -->
            					<div class="box-footer text-center">
        							<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        							<button type="submit" id="btn-submit" class="btn btn-primary">발송</button>
            					</div>
            					<!-- /.box-footer -->
            				</form>
    					</div>
    				</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		
		<script>
		$(function () {
			CKEDITOR.replace("mail_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?",
    	    	height: '350px'
    	    });

    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances.mail_content.updateElement();

				$.ajax({
					type : "POST",
					url : "./ajax/pre-reg-payment-mail-send.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("메일 발송이 완료되었습니다.");
							} else {
								alert("메일을 발송할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

    		$("#btn-cancel").click(function() {
				history.back();
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>