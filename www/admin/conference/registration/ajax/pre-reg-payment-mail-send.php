<?php
/*
 * Desc: 입금완료 안내 메일 발송
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['cr_id'])) {
    $code = 101;
    $msg = '사전등록자를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['sender_email'])) {
    $code = 1012;
    $msg = '보내는 사람 이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['receiver_email'])) {
    $code = 102;
    $msg = '받는 사람 이메일을 입력하거나 받는 그룹을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_subject'])) {
    $code = 103;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_content'])) {
    $code = 125;
    $msg = '메일 내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$cr_id = $_POST['cr_id'];

// 메일 내용 meta_data에 저장
if_preserve_payment_mail_history($cr_id);   // 메일 내용 등록

$to_email = $_POST['receiver_email'];
$mail_subject = $_POST['mail_subject'];
$mail_content = $_POST['mail_content'];
$from_email = $_POST['sender_email'];

$result = if_send_mail($to_email, $mail_subject, $mail_content, $from_email);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
