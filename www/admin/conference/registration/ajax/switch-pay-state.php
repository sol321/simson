<?php
/*
 * Desc: 입금 여부 switching
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '사전등록을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$cr_id = $_POST['id'];

$row = if_get_pre_registration($cr_id);
$pay_state = $row['pay_state'];

// 현재 state를 결제상태에서 제거 후, 변경할 나머지 key를 조회한다.
unset($if_payment_state[$pay_state]);
$new_state = key($if_payment_state); 

$result = if_switch_reg_payment_state($cr_id, $new_state);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
