<?php
/*
 * Desc: 현장등록 신규 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['conference_id'])) {
    $code = 101;
    $msg = '행사를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_member'])) {
    $code = 102;
    $msg = '회원 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['is_graduate'])) {
    $code = 103;
    $msg = '대학원 재학 여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_name'])) {
    $code = 104;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_mobile'])) {
    $code = 105;
    $msg = '휴대전화를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 106;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['pre_reg_fee'])) {
    $code = 107;
    $msg = '등록비 정보가 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['pay_state'])) {
    $code = 109;
    $msg = '입금여부를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_pre_registration();    // 사전등록과 공유

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>
