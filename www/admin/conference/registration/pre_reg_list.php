<?php
/*
 * Desc: 사전 등록 리스트 메인
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 ID Error.');
}

$conference_id = intval($_GET['id']);

$evt_row = if_get_event($conference_id);
$event_name = htmlspecialchars($evt_row['event_name']);

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 9;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);

$evt_type = empty($_GET['evt_type']) ? '' : $_GET['evt_type'];  // 분류
$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// 날짜별 검색
if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
    $sql .= " AND ($qa2 BETWEEN ? AND ?)";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_conference_register'] . "
		WHERE
            registration_type = '1000' AND
            show_hide = 'show' AND
            conference_id = '$conference_id'
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h4 class="text-danger pull-right">[<?php echo $event_name ?>] &nbsp; </h4>
					
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="../abstract/abstract_list.php?id=<?php echo $conference_id ?>"><b>초록 접수 리스트</b></a></li>
							<li class="active"><a href="pre_reg_list.php?id=<?php echo $conference_id ?>"><b>사전 등록 리스트</b></a></li>
							<li><a href="onsite_reg_list.php?id=<?php echo $conference_id ?>"><b>현장 등록 리스트</b></a></li>
						</ul>
					</div>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<input type="hidden" name="id" value="<?php echo $conference_id ?>">
								<div class="row">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 선택 -</option>
											<option value="user_name" <?php echo strcmp($qa1, 'user_name') ? '' : 'selected'; ?>>이름</option>
											<option value="user_email" <?php echo strcmp($qa1, 'user_email') ? '' : 'selected'; ?>>이메일</option>
											<option value="user_mobile" <?php echo strcmp($qa1, 'user_mobile') ? '' : 'selected'; ?>>휴대전화</option>
											<option value="org_name" <?php echo strcmp($qa1, 'org_name') ? '' : 'selected'; ?>>직장명</option>
										</select>
									</div>
									<div class="col-md-3">
   										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    								</div>
									<div class="col-md-7">
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
								</div>
								
								<div class="row add-top">
    								<div class="col-md-2">
    									<select name="qa2" class="form-control">
    										<option value="">- 기간 선택 -</option>
    										<option value="register_dt" <?php echo !strcmp($qa2, 'register_dt') ? 'selected' : ''; ?>>등록일</option>
    									</select>
    								</div>
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
    									</div>
    								</div>
    							</div>
							</form>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<div class="pull-right">
								<!-- 
								<a href="download_abstract_list.php?id=<?php echo $conference_id ?>" class="btn btn-primary btn-sm">엑셀 파일 저장</a>
								 -->
							</div>
						
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<form id="form-abstract-list">	
    							<table class="table table-hover">
    								<tbody>
    									<tr class="active">
        									<th>#</th>
    										<th>이름</th>
    										<th>회원구분</th>
    										<th>소속(직장)</th>
    										<th>이메일</th>
    										<th>금액</th>
    										<th>입금여부</th>
    										<th>등록일</th>
    										<th>관리</th>
    										<th></th>
            								<th>-</th>
        								</tr>
        				<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$cr_id = $val['cr_id'];
    							$is_member = $val['is_member'];
    							$is_graduate = $val['is_graduate'];
    							$user_name = $val['user_name'];
    							$org_name = $val['org_name'];
    							$user_email = $val['user_email'];
    							$fee_amount = $val['fee_amount'];
    							$pay_state = $val['pay_state'];
    							$register_dt = $val['register_dt'];
    							
    							
    							$meta_data = json_decode($val['meta_data'], true);
    						?>
    									<tr id="id-<?php echo $cr_id ?>">
    										<td><?php echo $list_no ?></td>
    										<td><?php echo $user_name ?></td>
    										<td>
    											<?php echo $if_is_member[$is_member] ?> <br>
    											<?php echo $if_is_graduate[$is_graduate] ?>
    										</td>
    										<td><?php echo $org_name ?></td>
    										<td>
    											<?php echo $user_email ?><br>
    											<a href="pre_reg_payment_mail.php?id=<?php echo $cr_id ?>" class="badge bg-aqua">메일 작성</a>
    										</td>
    										<td><?php echo number_format($fee_amount) ?></td>
    										<td>
    											<?php echo $if_payment_state[$pay_state] ?><br>
    											<a class="badge bg-purple act-change-paid">변경</a>
    										</td>
    										<td><?php echo substr($register_dt, 0, 10) ?></td>
    										<td>
    											<a href="pre_reg_edit.php?id=<?php echo $cr_id ?>" class="btn btn-info btn-xs">편집</a>
    											<button class="btn btn-danger btn-xs action-hide">삭제</button>
    										</td>
    									</tr>
    					<?php
    							$list_no--;
    						}
    					}
    					?>
    								</tbody>
    								<tfoot>
    									<tr>
    										<td colspan="13">
    											<a href="<?php echo CONTENT_URL ?>/conference/register/regist_add.php?eid=<?php echo $conference_id ?>" target="_blank" class="btn btn-info btn-xs">사전등록 (링크)</a>
    											<a href="pre_reg_add.php?id=<?php echo $conference_id ?>" class="btn btn-warning btn-xs">사전등록(관리)</a>
    											<a href="pre_reg_hide_list.php?id=<?php echo $conference_id ?>" class="btn btn-info btn-xs">삭제된 리스트</a>
    											<a href="pre_reg_statistics.php?id=<?php echo $conference_id ?>" class="btn btn-info btn-xs">사전등록 통계</a>
    										</td>
    									</tr>
    								</tfoot>
    							</table>
    						</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			// 휴지통
			$(".action-hide").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return;
				}

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var ids = [];

				ids.push(id);

				$.ajax({
					type : "POST",
					url : "./ajax/registration-hide.php",
					data : {
						"ids" : ids
					},
					dataType : "json",
					success : function(res) {
						if ( res.code == "0" ) {
							location.reload();
						} else {
							alert( res.msg );
						}
					}
				});
			});

			// 입금/미입금 처리
			$(".act-change-paid").click(function(e) {
				e.preventDefault();

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");

				$.ajax({
					type : "POST",
					url : "./ajax/switch-pay-state.php",
					data : {
						"id" : id
					},
					dataType : "json",
					success : function(res) {
						if ( res.code == "0" ) {
							location.reload();
						} else {
							alert( res.msg );
						}
					}
				});
			});
			

			$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});
		
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>