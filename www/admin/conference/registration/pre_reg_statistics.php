<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 ID Error.');
}

$conference_id = intval($_GET['id']);

$row = if_get_event($conference_id);
$event_type = $row['event_type'];
$event_state = $row['event_state'];
$event_name = htmlspecialchars($row['event_name']);
$event_year = $row['event_year'];

// 사전등록 결제상태별 납부 건수, 금액 조회
$pre_result = if_get_payment_by_pay_state('1000',$conference_id);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						사전등록 통계
						<small><?php echo $event_name ?></small>
					</h1>
				</div>
				<div class="content">
					<div class="box box-warning">
						<div class="box-body">
							<table class="table">
								<tr>
									<th class="bg-orange">\</th>
					<?php 
                    foreach ($if_is_graduate as $key => $val) {
                        $total_sum[$key] = 0;
                    ?>
                    				<th class="bg-info text-center"><?php echo $val ?></th>
                    <?php 
                    }
                    ?>
								</tr>
								
					<?php 
					foreach ($if_is_member as $key2 => $val2) {
    				?>
								<tr>
									<td class="bg-info"><?php echo $val2 ?></td>
						<?php 
						foreach ($if_is_graduate as $key => $val) {
						    $query = "
                                    SELECT
                                        COUNT(*)
                                    FROM "
                                        . $GLOBALS['if_tbl_conference_register'] . "
                                    WHERE
                                        is_graduate = '$key' AND
                                        is_member = '$key2' AND
                                        registration_type = '1000'
                                        AND conference_id = '$conference_id'
                                        AND show_hide = 'show'
                            ";
                            $stmt = $ifdb->prepare($query);
                            $stmt->execute();
                            $count = $ifdb->get_var($stmt);
                            
                            $total_sum[$key] += $count;
						?>
									<td class="text-center">
										<h3><?php echo number_format($count) ?></h3>
									</td>
						<?php 
						}
						?>
								</tr>
					<?php 
    				}
					?>
								<tr>
									<th class="bg-orange">합계</th>
						<?php 
						foreach ($if_is_graduate as $key => $val) {
						?>
									<th class="bg-info text-center">
										<h3><?php echo number_format($total_sum[$key]) ?></h3>
									</th>
						<?php 
						}
						?>
								</tr>
							</table>
						</div>
						<div class="box-footer">
							<div class="col-md-4">
    							<table class="table table-bordered">
    					<?php 
    					foreach ($pre_result as $key => $val) {
    					    $p_state = $val['pay_state'];
    					    $cnt = $val['cnt'];
    					    $total = $val['total'];
    					?>
    								<tr>
    									<td class="active"><?php echo $if_payment_state[$p_state] ?></td>
    									<td><?php echo number_format($cnt) ?>건</td>
    									<td><?php echo number_format($total) ?>원</td>
    								</tr>
    					<?php 
    					}
    					?>
    							</table>
    						</div>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function () {
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>