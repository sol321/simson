<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('행사 정보가 필요합니다.');
}

// 무통장입금 계좌 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec_bank = json_decode($if_bank_account_data, true);

$conference_id = $_GET['id'];

// Event 조회
$evt_row = if_get_event($conference_id);

$event_type = $evt_row['event_type'];
$event_state = $evt_row['event_state'];
$event_name = htmlspecialchars($evt_row['event_name']);
$event_year = $evt_row['event_year'];

$pre_reg_from = $evt_row['pre_reg_from'];
$pre_reg_to = $evt_row['pre_reg_to'];

$meta_data = $evt_row['meta_data'];
$evt_jdec = json_decode($meta_data, true);

$pre_reg_max_attendees = $evt_jdec['pre_reg_max_attendees'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>사전등록 신규 추가</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<!-- form start -->
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="conference_id" value="<?php echo $conference_id ?>">
							<div class="box-body">
								
								<div class="form-group">
            						<label class="col-md-2 control-label">분류</label>
            						<div class="col-md-10">
            							<?php echo $if_event_type[$event_type] ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">행사명</label>
            						<div class="col-md-4">
            							<?php echo $event_name ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">사전등록 정보</label>
            						<div class="col-md-4">
            							<p>사전등록 기간 : <b><?php echo substr($pre_reg_from, 0, 16) ?> ~ <?php echo substr($pre_reg_to, 0, 16) ?></b> </p>
            							<p>최대 참석자 : <b><?php echo number_format($pre_reg_max_attendees) ?></b>명</p>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">회원 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_member as $key => $val) {
            				?>
            								<label>
            									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>"><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">대학원 재학 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_graduate as $key => $val) {
            				?>
            								<label>
            									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>"><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					
            					<div class="form-group">
            						<label class="col-md-2 control-label">이름</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_name" id="user_name" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">소속(직장)</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_name" id="org_name" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">직장전화</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_phone" id="org_phone" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">휴대전화</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_mobile" id="user_mobile" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">이메일</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_email" id="user_email" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">등록비</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control numeric" name="pre_reg_fee" id="pre_reg_fee" value="">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">계좌정보</label>
            						<div class="col-md-4">
            				<?php 
            				if (!empty($jdec_bank)) {
            					foreach ($jdec_bank as $key => $val) {
            						foreach ($val as $k => $v) {
            							$exp = explode("\t", $v);
            							$opt_text = $exp[0] . ' ' . $exp[1] . ' ' . $exp[2];
            				?>
            							<div class="radio">
            								<label>
            									<input type="radio" name="bank_name" class="bank_name" value="<?php echo $opt_text ?>"> <?php echo $opt_text ?>
            								</label>
            							</div> 
            				<?php 
            						}
            					}
            				}
            				?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">입금 여부</label>
            						<div class="col-md-10">
            				<?php 
            				foreach ($if_payment_state as $key => $val) {
            				?>
            							<div class="radio">
            								<label>
            									<input type="radio" name="pay_state" class="pay_state" value="<?php echo $key ?>"><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            							</div>
            				<?php 
            				}
            				?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">비밀번호</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="passwd" id="passwd" maxlength="20" value="">
            						</div>
            					</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
		<script>
		$(function () {
			$(".numeric").numeric({ negative: false });
			
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/pre-reg-add.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "pre_reg_list.php?id=<?php echo $conference_id ?>";
							} else {
								alert("행사를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 등록비 계산
			$(".is_member, .is_graduate").click(function() {
				var mval = $(".is_member:checked").val();
				var gval = $(".is_graduate:checked").val();

				if (mval && gval) {
					var fkey = mval + "-" + gval;

					$.ajax({
						type : "POST",
						url : "<?php echo CONTENT_URL ?>/conference/register/ajax/get-user-class-fee.php",
						data : {
							"id" : "<?php echo $conference_id ?>",
							"key" : fkey
						},
						dataType : "json",
						success : function(res) {
							if (res.code == "0") {
								$("#pre_reg_fee").val(res.amount);
							} else {
								alert("등록비 정보를 가져오지 못했습니다.");
							}
						}
					});	
				}
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>