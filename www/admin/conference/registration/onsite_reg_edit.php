<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-conference.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('사전등록 정보를 선택해 주십시오.');
}

$cr_id = $_GET['id'];

$row = if_get_pre_registration($cr_id);

$is_member = $row['is_member'];
$is_graduate = $row['is_graduate'];
$user_name = $row['user_name'];
$user_mobile = $row['user_mobile'];
$user_email = $row['user_email'];
$org_name = $row['org_name'];
$fee_amount = $row['fee_amount'];
$pay_state = $row['pay_state'];
$registration_type = $row['registration_type'];
$user_id = $row['user_id'];
$conference_id = $row['conference_id'];

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$org_phone = @$jdec['org_phone'];
$passwd = @$jdec['passwd'];
$bank_name = $jdec['bank_name'];
$admin_memo = @$jdec['admin_memo'];

// Event 조회
$evt_row = if_get_event($conference_id);

$event_type = $evt_row['event_type'];
$event_state = $evt_row['event_state'];
$event_name = htmlspecialchars($evt_row['event_name']);
$event_year = $evt_row['event_year'];

$event_period_from = $evt_row['event_period_from'];
$event_period_to = $evt_row['event_period_to'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>현장등록 수정</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<!-- form start -->
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="cr_id" value="<?php echo $cr_id ?>">
							<div class="box-body">
								
								<div class="form-group">
            						<label class="col-md-2 control-label">분류</label>
            						<div class="col-md-10">
            							<?php echo $if_event_type[$event_type] ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">행사명</label>
            						<div class="col-md-4">
            							<?php echo $event_name ?>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">행사 정보</label>
            						<div class="col-md-4">
            							<p>학술대회 기간 : <b><?php echo substr($event_period_from, 0, 16) ?> ~ <?php echo substr($event_period_to, 0, 16) ?></b> </p>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">회원 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_member as $key => $val) {
            				    $checked = $is_member == $key ? 'checked' : ''; 
            				?>
            								<label>
            									<input type="radio" name="is_member" class="is_member" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">대학원 재학 여부</label>
            						<div class="col-md-10">
            							<div class="radio">
            				<?php 
            				foreach ($if_is_graduate as $key => $val) {
            				    $checked = $is_graduate == $key ? 'checked' : '';
            				?>
            								<label>
            									<input type="radio" name="is_graduate" class="is_graduate" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            				<?php 
            				}
            				?>
            							</div>
            						</div>
            					</div>
            					
            					<div class="form-group">
            						<label class="col-md-2 control-label">이름 *</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_name" id="user_name" value="<?php echo $user_name ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">소속(직장)</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">직장전화</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo $org_phone ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">휴대전화 *</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_mobile" id="user_mobile" value="<?php echo $user_mobile ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">이메일 *</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control" name="user_email" id="user_email" value="<?php echo $user_email ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">현장등록비 *</label>
            						<div class="col-md-4">
            							<input type="text" class="form-control numeric" name="pre_reg_fee" id="pre_reg_fee" value="<?php echo $fee_amount ?>">
            						</div>
            					</div>
            					<div class="form-group">
            						<label class="col-md-2 control-label">입금 여부</label>
            						<div class="col-md-10">
            				<?php 
            				foreach ($if_payment_state as $key => $val) {
            				    $checked = $pay_state == $key ? 'checked' : '';
            				?>
            							<div class="radio">
            								<label>
            									<input type="radio" name="pay_state" class="pay_state" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
            									&nbsp; &nbsp;
            								</label>
            							</div>
            				<?php 
            				}
            				?>
            						</div>
            					</div>
							</div>
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
							</div>
							<!-- /.box-footer -->
						</form>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
		<script>
		$(function () {
			$(".numeric").numeric({ negative: false });
			
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/onsite-reg-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "onsite_reg_list.php?id=<?php echo $conference_id ?>";
							} else {
								alert("행사를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>