<?php
require_once '../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';
require_once FUNC_PATH . '/functions-auth.php';

if (if_get_admin_count() > 0 && !if_is_admin()) {
    if_redirect( ADMIN_URL . '/login.php' );
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>관리자 생성</title>

		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/bootstrap.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/css/custom.css">

		<!-- jQuery 3 -->
		<script src="<?php echo INC_URL ?>/js/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap.min.js"></script>
		<!-- jQuery Validate Plugin -->
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition register-page">
    	<div class="register-box">
    		<div class="register-logo">
    			<b>새로운 관리자 등록</b>
    		</div>
    		<div class="register-box-body">
    			<form class="validation" id="form-item-new">
    				<div class="form-group">
    					<input type="text" name="admin_login" id="admin_login" class="form-control" placeholder="아이디">
    				</div>
    				<div class="form-group">
    					<input type="password" name="password" id="password" class="form-control" placeholder="비밀번호">
    				</div>
    				<div class="form-group">
    					<input type="password" name="password_repeat" id="password_repeat" class="form-control" placeholder="비밀번호 확인">
    				</div>
    				<div class="form-group">
    					<input type="text" name="admin_name" id="admin_name" class="form-control" placeholder="이름">
    				</div>
    				<div class="row">
    					<div class="col-xs-8">
    					</div>
    					<!-- /.col -->
    					<div class="col-xs-4">
    						<button type="submit" class="btn btn-primary btn-block btn-flat" id="btn-submit">등록</button>
    					</div>
    					<!-- /.col -->
    				</div>
    			</form>
    		</div>
    		<!-- /.form-box -->
    	</div>
    	<!-- /.register-box -->

    	<script>
    		$(function () {
    			$(":input").attr("maxlength", "30");
    
    			$("#form-item-new").validate({
    				rules: {
    					admin_login: {
    						required: true,
    						minlength: 6
    					},
    					password: {
    						required: true,
    						minlength: 6
    					},
    					password_repeat: {
    						required: true,
    						minlength: 6,
    						equalTo: "#password"
    					}
    				},
    				messages: {
    					admin_login: {
    						required: "아이디를 입력해 주십시오.",
    						minlength: "아이디는 최소 6자 이상입니다."
    					},
    					password: {
    						required: "비밀번호를 입력해 주십시오.",
    						minlength: "비밀번호는 최소 6자 이상입니다."
    					},
    					password_repeat: {
    						required: "비밀번호를 한번 더 입력해 주십시오.",
    						minlength: "비밀번호는 최소 6자 이상입니다.",
    						equalTo: "비밀번호가 일치하지 않습니다."
    					}
    				},
    				submitHandler: function(form) {
    					$.ajax({
    						type : "POST",
    						url : "./ajax/create-admin.php",
    						data : $("#form-item-new").serialize(),
    						dataType : "json",
    						beforeSend : function() {
    							$("#btn-submit").prop("disabled", true);
    						},
    						success : function(res) {
    							if (res.code == "0") {
    								location.href = "login.php";
    							} else {
    								alert(res.msg);
    							}
    						}
    					}).done(function() {
    					}).fail(function() {
    					}).always(function() {
    						$("#btn-submit").prop("disabled", false);
    					}); // ajax
    				}
    			});
    		});
    	</script>
	</body>
</html>
