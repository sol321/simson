<?php
/*
 * Desc : 관리자 Index
 */
require_once '../if-config.php';
// require_once FUNC_PATH . '/functions-user.php';

if (if_is_admin()) {
    if_redirect( ADMIN_URL . '/dashboard.php' );
} else {
    if_redirect( ADMIN_URL . '/login.php' );
}

?>