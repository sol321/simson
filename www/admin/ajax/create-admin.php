<?php
/*
 * Desc : 관리자 등록 화면
 * 		관리자가 미등록 상태이거나, 관리자로 로그인 한 경우만 사용 가능
 */
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

$code = 0;
$msg = '';

if ( if_get_admin_count() > 0 && !if_is_admin() ) {
    $code = 201;
    $msg = '사용할 수 없습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['admin_login']) ) {
    $code = 101;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['password']) ) {
    $code = 102;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( empty($_POST['password_repeat']) ) {
    $code = 103;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$admin_login = $_POST['admin_login'];
$passwd = $_POST['password'];
$passwd_confirm = $_POST['password_repeat'];

if ( !ctype_alnum($admin_login) ) {
	$code = 110;
	$msg = '아이디는 알파벳과 숫자만 사용하실 수 있습니다. 아이디를 다시 입력해 주십시오';
	$json = compact('code', 'msg');
	exit( json_encode($json) );
}

if ( if_exist_admin($admin_login) ) {
    $code = 501;
    $msg = $admin_login . ' 아이디는 이미 사용중입니다. 다른 아이디를 입력해 주십시오';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( strcmp($passwd, $passwd_confirm) ) {
    $code = 104;
    $msg = '비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$seq_id  = if_add_admin();

if ( empty($seq_id) ) {
    $code = 500;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$json = compact('code', 'msg', 'seq_id');
echo json_encode( $json );
?>