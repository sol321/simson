<?php
require_once '../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

$code = 0;
$msg = '';

if (empty($_POST['login'])) {
    $code = 403;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password'])) {
    $code = 404;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$input_login = trim($_POST['login']);
$input_passwd = $_POST['password'];
$admin_row = if_get_admin_by_login($input_login);

if (empty($admin_row)) {
    $code = 501;
    $msg = '존재하지 않는 아이디입니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $admin_row['seq_id'];
$admin_login = $admin_row['admin_login'];
$admin_name =$admin_row['admin_name'];
$admin_passwd = $admin_row['admin_pw'];
$admin_level = $admin_row['admin_level'];

$check_passwd = if_verify_passwd($input_passwd, $admin_passwd);

if (!$check_passwd) {
    $code = 502;
    $msg = '비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

/*
if (!empty($_POST['remember_me'])) {
    // 기억하기 체크시에는 아이디를 쿠키로 저장함.
    setcookie('admin_cookie_login', $input_login, time() + 86400 * 30, '/' . ADMIN_DIR . '/');
} else {
    setcookie('admin_cookie_login', '', time() - 86400, '/' . ADMIN_DIR . '/');
}
*/

$redirect = ADMIN_URL . '/admin.php';

$_SESSION['cms']['admin_id'] = $seq_id;
$_SESSION['cms']['admin_login'] = $input_login;
$_SESSION['cms']['admin_name'] = $admin_name;
$_SESSION['cms']['admin_level'] = $admin_level;

$json = compact('code', 'msg', 'redirect');
echo json_encode($json);

?>