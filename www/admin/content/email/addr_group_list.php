<?php
/*
 * Desc: 주소록 > 그룹별 보기
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-address.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if (!empty($q)) {
    $sql = " AND group_name LIKE ? ";
    array_push($sparam, '%' . $q . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_address_group'] . "
		WHERE
            1
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						주소록 <small>그룹별 보기</small>
					</h1>
				</div>
				<div class="content">
					<div class="row">
                    	<div class="col-md-4">
                        	<!-- form start -->
                            <form id="form-item-new" class="form-horizontal">
            					<div class="box box-info">
                					<div class="box-body">
                						<div class="form-group">                							
                							<div class="col-md-12">
                								<input type="text" class="form-control" id="group_name" name="group_name" placeholder="그룹 이름">
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<button type="submit" id="btn-submit" class="btn btn-primary">그룹 추가</button>
                					</div>
                					<!-- /.box-footer -->
            					</div>
            				</form>
            			</div>
            			<div class="col-md-8">
							<div class="box box-success">
        						<div class="box-header">
        							<h3 class="box-title">Total : <?php echo number_format($total_count) ?></h3>
        							<div class="box-tools">
        								<form class="form-inline">
        									<div class="input-group">
        										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="Search">
        										<div class="input-group-btn">
        											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        										</div>
        									</div>
        								</form>
        							</div>
        						</div><!-- /.box-header -->
        						<div class="box-body">
        							<table class="table table-hover">
        								<thead>
        									<tr>
        										<th>#</th>
        										<th>그룹 이름</th>
        										<th>인원 수</th>
        										<th>개인 주소</th>
        										<th></th>
        									</tr>
        								</thead>
        								<tbody>
        					<?php
        					if (!empty($item_results)) {
        						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        						foreach ($item_results as $key => $val) {
        						    $rgid = $val['seq_id'];
        						    $rgname = $val['group_name'];
        						    $rcount = if_get_address_contact_count($rgid);
        						?>
        									<tr id="item-id-<?php echo $rgid ?>">
        										<td><?php echo $list_no ?></td>
        										<td class="has-success">
        											<input type="text" class="form-control item-label-name" value="<?php echo $rgname ?>">
        										</td>
        										<td>
        											<b><?php echo number_format($rcount) ?></b>  &nbsp;
        										</td>
        										<td>
        											<a href="addr_contact_list.php?gid=<?php echo $rgid ?>" class="btn btn-info btn-xs">주소 리스트</a>
        										</td>
        										<td>
        											<button class="btn bg-maroon btn-xs action-delete">삭제(그룹만)</button> &nbsp;
        											<button class="btn btn-danger btn-xs action-cleanup">삭제(주소 포함)</button> &nbsp;
        										</td>
        									</tr>
        					<?php
        							$list_no--;
        						}
        					}
        					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
        						<div class="box-footer">
        							<div class="text-center">
        				<?php 
        				echo $paginator->if_paginator();
        				?>
        							</div>
        						</div>
        					</div><!-- /.box -->
						
						</div>
            		</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
    	$(function() {
    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/addr-group-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("그룹을 등록했습니다.");
								location.reload();
							} else {
								alert("메일 그룹을 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 그룹명 편집
			$(document).on("change", ".item-label-name", function(e) {
				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var val = $(this).val();

				$.ajax({
					type : "POST",
					url : "./ajax/addr-group-edit.php",
					data : {
						"id" : id,
						"data" : val
					},
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							alert("변경했습니다.");
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			});

			// 그룹만 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/addr-group-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
        					alert("삭제했습니다.");
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    		
			// 그룹 삭제, 해당 그룹 ID의 주소도 삭제
    		$(".action-cleanup").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/addr-group-cleanup.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
        					alert("삭제했습니다.");
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>