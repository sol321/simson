<?php
/*
 * Desc: 메일 본문 리스트
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
	$sql .= " AND c.mail_subject LIKE ? OR c.mail_content LIKE ?";
	array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
	$pph_count = substr_count($sql, '?');
	for ($i = 0; $i < $pph_count; $i++) {
		$pph .= 's';
	}
}

if (!empty($pph)) {
	array_unshift($sparam, $pph);
}

$query = "
		SELECT
			SUM(if(l.send_chk='Y',1,0)) AS cnt, 
			c.seq_id, 
			c.mail_subject, 
			c.create_dt
		FROM
			" . $GLOBALS['if_tbl_mail_content'] . " AS c
		LEFT JOIN
			" . $GLOBALS['if_tbl_mail_log'] . " AS l
		ON
			c.seq_id = l.content_id 
		WHERE
			c.show_hide = 'show'
			$sql
		GROUP BY
			c.seq_id, c.mail_subject, c.create_dt
		ORDER BY
			c.seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						메일링 리스트 관리
						<a href="mail_add.php" class="btn btn-info btn-sm">새 메일 등록</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?>
							</h3>
							<form id="search-form">
								<div class="box-tools">
									<div class="pull-right">
										<div class="input-group input-group-sm" style="width: 350px;">
											<input type="text" name="qs1" class="form-control pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
											
											<div class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<!-- /.box-header -->
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
										<th>#</th>
										<th>메일 제목</th>
										<th>등록날짜</th>
										<th></th>
									</tr>
						<?php 
						if (!empty($item_results)) {
							$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
							
							foreach ($item_results as $key => $val) {
								$seq_id = $val['seq_id'];
								$mail_subject = $val['mail_subject'];
								$create_dt = $val['create_dt'];
								$sent_cnt = $val['cnt'];
						?>
								<tr id="item-id-<?php echo $seq_id ?>">
									<td><?php echo $list_no ?></td>
									<td>
										<?php echo $mail_subject ?> &nbsp;
							<?php 
							if ($sent_cnt) {
							?>
										<a href="mail_sent_list.php?id=<?php echo $seq_id ?>" class="btn btn-warning btn-xs">발송 내역(<?php echo number_format($sent_cnt) ?>)</a>
							<?php
							}
							?>
									</td>
									<td><?php echo substr($create_dt, 0, 10) ?></td>								
									<td>
										<a href="mail_preview.php?id=<?php echo $seq_id ?>" class="btn btn-success btn-xs">미리보기 및 메일발송</a> &nbsp; &nbsp; &nbsp;
										<a href="mail_edit.php?id=<?php echo $seq_id ?>" class="btn btn-info btn-xs">수정</a> &nbsp;
										<button class="btn btn-danger btn-xs action-delete">삭제</button>
									</td>
								</tr>
						<?php 
							   $list_no--;
							}
						}
						?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer text-center">
				<?php
				echo $paginator->if_paginator();
				?>
						</div>
						<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
		$(function() {
			// 삭제
			$(".action-delete").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return false;
				}

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
	
				$.ajax({
					type : "POST",
					url : "./ajax/mail-hide.php",
					data : {
						"id" : id
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>