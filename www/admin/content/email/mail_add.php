<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mail.php';

if_authenticate_admin();

// 메일 본문 기본내용
$default_mail_body = if_get_default_mail_template('');

$max_file_size = 50 * 1024 * 1024; // 100M 

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						새 메일 작성
					</h1>
				</div>
				<div class="content">
					<div class="row">
                    	<div class="col-md-12">
                        	<!-- form start -->
                            <form id="form-item-new" class="form-horizontal">
                            	<input type="hidden" id="max-file-size" value="<?php echo $max_file_size ?>">
            					<div class="box box-info">
                					<div class="box-body">
                						<div class="form-group">                							
                							<div class="col-md-12">
                								<input type="text" class="form-control" id="mail_subject" name="mail_subject" placeholder="메일 제목">
                							</div>
                						</div>
                						<div class="form-group">
                							<div class="col-md-12">
                    							<textarea id="mail_content" name="mail_content" class="form-control"><?php echo $default_mail_body ?></textarea>
            								</div>
                						</div>
                						<div class="form-group">
                							<div class="col-md-6">
                								<div class="callout">
                									<h4>치환문자</h4>
            										<div class="input-group">
                    									<div class="input-group-addon">이름</div>
                    									<input type="text" class="form-control replace-word" value="{:name:}">
                    									<div class="input-group-addon">회원 아이디</div>
                    									<input type="text" class="form-control replace-word" value="{:login:}">
                    								</div>
            										
                								</div>
                							</div>
                						</div>
                						<div class="form-group">
        									<div class="col-md-8">
        										<div class="btn btn-default btn-file">
            										<i class="fa fa-paperclip"></i> 첨부파일
            										<input type="file" name="attachment[]" id="file-attach">
            									</div>
            									<ul class="list-group" id="preview-attachment"></ul>
        									</div>
        								</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
            							<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
                					</div>
                					<!-- /.box-footer -->
            					</div>
            				</form>
            			</div>
            		</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		
		<script>
    	$(function() {
        	// 치환 문자 선택
        	$(".replace-word").click(function() {
            	$(this).select();
            });

        	// CKEditor file upload
    		CKEDITOR.replace("mail_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?",
    	    	height: "400px"
    	    });

    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances["mail_content"].updateElement();

				$.ajax({
					type : "POST",
					url : "./ajax/mail-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "mail_list.php";
							} else {
								alert("메일을 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
				var max_file_size = $("#max-file-size").val();
				var max_file_count = $("#preview-attachment li").length;

				if (max_file_count > 4) {
					alert("첨부할 수 있는 파일은 총 5개입니다.");
					return;
				}

				if (fsize > max_file_size) {
					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
					$(this).val("");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) { 
						if (xhr.code == "0") { 
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
    												'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
    												'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
    												'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
    												'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
    												xhr.file_name[i] +
    											'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$("#file-attach").val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작
			
			// 파일 삭제 
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			// -- 첨부 파일 끝
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>