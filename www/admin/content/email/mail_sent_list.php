<?php
/*
 * Desc: 발송  리스트
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('메일 본문에 대한 정보가 필요합니다.');
}

$content_id = $_GET['id'];

$mail_row = if_get_mailing_content_by_id($content_id);
$mail_subject = $mail_row['mail_subject'];


$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
    $sql .= " AND rcv_email LIKE ? OR rcv_name LIKE ?";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
            *
		FROM
			" . $GLOBALS['if_tbl_mail_log'] . "
		WHERE
            content_id = '$content_id' AND
			show_hide = 'show'
             AND msg IS NOT NULL
            $sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						이메일 리스트 <small><?php echo $mail_subject ?></small>
					</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?>
							</h3>
							<form id="search-form">
								<input type="hidden" name="id" value="<?php echo $content_id ?>">
								<div class="box-tools">
									<div class="pull-right">
										<div class="input-group input-group-sm" style="width: 350px;">
											<input type="text" name="qs1" class="form-control pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
											
											<div class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<!-- /.box-header -->
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
										<th>#</th>
										<th>이메일</th>
										<th>이름</th>
										<th>수신 날짜</th>
										<th></th>
									</tr>
						<?php 
						if (!empty($item_results)) {
							$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
							
							foreach ($item_results as $key => $val) {
								$log_id = $val['log_id'];
								$rcv_email = $val['rcv_email'];
								$rcv_name = $val['rcv_name'];
								$rec_dt = $val['rec_dt'];
								$send_dt = $val['send_dt'];
								$msg = $val['msg'];
								if(strcmp($msg,'Sent')){
								    $rec_dt = $msg;
								}
						?>
								<tr id="item-id-<?php echo $log_id ?>">
									<td><?php echo $list_no ?></td>
									<td><?php echo $rcv_email ?></td>
									<td><?php echo $rcv_name ?></td>
									<td><?php echo $rec_dt ?></td>
								</tr>
						<?php 
							   $list_no--;
							}
						}
						?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer text-center">
							<div class="pull-left">
								<a href="mail_list.php" class="btn btn-info btn-sm">리스트</a>
							</div>
				<?php
				echo $paginator->if_paginator();
				?>
						</div>
						<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
		$(function() {
			// 삭제
			$(".action-delete").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return false;
				}

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
	
				$.ajax({
					type : "POST",
					url : "./ajax/mail-hide.php",
					data : {
						"id" : id
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>