<?php
/*
 * Desc: 주소록 > 개별 주소 보기
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-address.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['gid'])) {
    if_js_alert_back('그룹 정보가 필요합니다.');
}

$group_id = intval($_GET['gid']);
$group_name = if_get_address_groupname($group_id);

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if ( !empty($q) ) {
    $sql = " AND (contact_name LIKE ? OR contact_email LIKE ? OR contact_mobile LIKE ? OR org_name LIKE ?) ";
    array_push($sparam, '%'.$q.'%', '%'.$q.'%', '%'.$q.'%', '%'.$q.'%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_address_contact'] . "
		WHERE
            group_id = $group_id
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						[<?php echo $group_name ?>] <small>주소 리스트</small>
					</h1>
				</div>
				<div class="content">
					<div class="row">
                    	<div class="col-md-4">
        					<div class="box box-info" id="box-contact-add">
        						<!-- form start -->
                        		<form id="form-item-new">
                        			<input type="hidden" name="group_id" id="group_id" value="<?php echo $group_id ?>">	
                					<div class="box-body">
										<h4>새 주소 등록</h4>
										<div class="form-group">
											<input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="이름" required>
										</div>
										<div class="form-group">
											<input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="이메일" required>
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="contact_mobile" id="contact_mobile" placeholder="휴대전화번호">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="org_name" id="org_name" placeholder="소속">
										</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<button type="submit" id="btn-submit" class="btn btn-primary">주소 추가</button>
                					</div>
                					<!-- /.box-footer -->
                				</form>
        					</div>
        					
        					<!-- Edit -->
        					<div class="box box-warning hide" id="box-contact-edit">
        						<!-- form start -->
                        		<form id="form-item-edit">
                        			<input type="hidden" name="contact_id" id="contact_id">	
                					<div class="box-body">
										<h4>주소 변경</h4>
										<div class="form-group">
											<input type="text" class="form-control" name="contact_name" id="contact_name_edit" placeholder="이름" required>
										</div>
										<div class="form-group">
											<input type="email" class="form-control" name="contact_email" id="contact_email_edit" placeholder="이메일" required>
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="contact_mobile" id="contact_mobile_edit" placeholder="휴대전화번호">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="org_name" id="org_name_edit" placeholder="소속">
										</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
                						<button type="button" id="btn-contact-edit-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit-edit" class="btn btn-success">저장합니다</button>
                					</div>
                					<!-- /.box-footer -->
                				</form>
        					</div>
        					<!-- /. Edit -->
            			</div>
            			<div class="col-md-8">
							<div class="box box-success">
        						<div class="box-header">
        							<h3 class="box-title">
        								<form id="form-excel-uplod">
            								Total : <?php echo number_format($total_count) ?> &nbsp;
            								<button type="button" class="btn btn-success btn-xs" id="btn-bulk-upload">
            									<span class="glyphicon glyphicon-upload"></span> Excel 업로드
            								</button> &nbsp; &nbsp;
            								<a href="../_files/sample_address.xlsx" class="btn btn-info btn-xs">샘플 주소록 Download</a>
            								<input type="file" name="attachment" id="attachment_file" class="hide">
            							</form>
        							</h3>
        							<div class="box-tools">
        								<form class="form-inline">
        									<div class="input-group">
        										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="Search">
        										<div class="input-group-btn">
        											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        										</div>
        									</div>
        								</form>
        							</div>
        						</div><!-- /.box-header -->
        						<div class="box-body">
        							<table class="table table-hover">
        								<thead>
        									<tr>
        										<th>#</th>
        										<th>이름</th>
        										<th>이메일</th>
        										<th>휴대전화번호</th>
        										<th>소속</th>
        										<th></th>
        									</tr>
        								</thead>
        								<tbody>
        					<?php
        					if (!empty($item_results)) {
        						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        						foreach ($item_results as $key => $val) {
        						    $cid = $val['contact_id'];
        						    $cname = $val['contact_name'];
        						    $cemail = $val['contact_email'];
        						    $cmobile = $val['contact_mobile'];
        						    $corg = $val['org_name'];
        						?>
        									<tr id="item-id-<?php echo $cid ?>">
        										<td><?php echo $list_no ?></td>
        										<td><?php echo $cname ?></td>
        										<td><?php echo $cemail ?></td>
        										<td><?php echo $cmobile ?></td>
        										<td><?php echo $corg ?></td>
        										<td>
        											<button class="btn btn-warning btn-xs action-edit">수정</button> &nbsp;
        											<button class="btn btn-danger btn-xs action-delete">삭제</button> &nbsp;
        										</td>
        									</tr>
        					<?php
        							$list_no--;
        						}
        					}
        					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
        						<div class="box-footer">
        							<div class="pull-left">
        								<a href="addr_group_list.php" class="btn btn-info btn-sm">그룹별 주소록 보기</a>
        							</div>
        							<div class="text-center">
        				<?php 
        				echo $paginator->if_paginator();
        				?>
        							</div>
        						</div>
        					</div><!-- /.box -->
						
						</div>
            		</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		
		<script>
    	$(function() {
    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/addr-contact-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("주소를 등록했습니다.");
								location.reload();
							} else {
								alert("주소를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 수정 클릭 시, 수정 form 노출
			$(".action-edit").click(function() {
				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var cname = $(this).closest("tr").find("td").eq(1).html();
				var cemail = $(this).closest("tr").find("td").eq(2).html();
				var cmobile = $(this).closest("tr").find("td").eq(3).html();
				var ccorp = $(this).closest("tr").find("td").eq(4).html();
				
				$("#contact_id").val(id);
				$("#contact_name_edit").val(cname);
				$("#contact_email_edit").val(cemail);
				$("#contact_mobile_edit").val(cmobile);
				$("#org_name_edit").val(ccorp);

				$("#box-contact-add").addClass("hide");
				$("#box-contact-edit").removeClass("hide");
			});

			// 수정 변경 form 취소
			$("#btn-contact-edit-cancel").click(function() {
				$("#box-contact-edit").addClass("hide");
				$("#box-contact-add").removeClass("hide");
			});

			// 변경 저장
			$("#form-item-edit").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/addr-contact-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit-edit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("주소를 변경했습니다.");
								location.reload();
							} else {
								alert("주소를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit-edit").prop("disabled", false);
				}); // ajax
			});
			
			// 주소 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/addr-contact-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
        					alert("삭제했습니다.");
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		// Excel Upload
			$("#btn-bulk-upload").click(function() {
				$("#attachment_file").click();
			});
			// File Upload
			$("#attachment_file").change(function() {
				var fext = this.files[0].name.split('.').pop().toLowerCase();

				if ( fext !=  'xls' && fext != 'xlsx' ) {
					alert('확장자가 xls, xlsx인 엑셀파일만 업로드해 주십시오.');
					$(this).val("");
					return;
				}

				$("#btn-bulk-upload").html("업로드중입니다...");
				$("#btn-bulk-upload").prop("disabled", true);

				$("#form-excel-uplod").ajaxSubmit({
					type : "POST",
					url : "./ajax/add-upload-excel-file.php",
					data : {
						"group_id" : $("#group_id").val()
					},
					dataType : "json",
					success: function(res) {
						if (res.code == "0") {
							alert(res.result + "건이 등록되었습니다.");
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>