<?php
/*
 * Desc: 메일 미리보기 및 발송
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-address.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('메일에 대한 정보가 필요합니다.');
}

$seq_id = $_GET['id'];

$mail_row = if_get_mailing_content_by_id($seq_id);

$mail_subject = $mail_row['mail_subject'];
$mail_content = $mail_row['mail_content'];
$meta_data = $mail_row['meta_data'];
$jdec = json_decode($meta_data, true);
$file_attachment = $jdec['file_attachment'];

$option_site_email_from = json_decode(if_get_option('site_email_from'), true);

// 회원 구분
$uclasses = if_get_all_user_class();
$if_user_class = if_get_user_class_array($uclasses);

// 주소록 그룹
$address_groups = if_get_address_groups();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						메일 미리보기
					</h1>
				</div>
				<div class="content">
					<div class="row">
						<!-- form start -->
                        <form id="form-item-new">
                        	<input type="hidden" name="content_id" value="<?php echo $seq_id ?>">
					
	                    	<div class="col-md-8">
            					<div class="box box-info">
                					<div class="box-body">
                						<div class="form-group">                							
            								<div class="input-group">
        										<div class="input-group-addon bg-gray">보내는 사람 이메일</div>
        										<input class="form-control" id="sender_email" name="sender_email" value="<?php echo $option_site_email_from ?>">
        										<div class="input-group-addon bg-gray"><i><small>SPF 확인 기능을 위해 협회 도메인의 이메일 주소 사용을 권장합니다.</small></i></div>
        									</div>
                						</div>
                						<div class="form-group">
            								<div class="input-group">
        										<div class="input-group-addon bg-gray">메일 제목</div>
        										<input type="text" class="form-control" id="mail_subject" name="mail_subject" placeholder="메일 제목" value="<?php echo $mail_subject ?>">
        									</div>
                						</div>
            							<div class="form-group">
                							<div class="input-group">
                								<div class="input-group-addon bg-purple">테스트 발송용 수신자 이메일</div>
                								<input type="email" class="form-control" id="test_email_address" name="test_email_address" placeholder="발송 테스트를 위한 수신자 이메일">
                								<span class="input-group-btn">
                									<button type="button" class="btn bg-orange btn-flat" id="btn-send-test">테스트 발송 !</button>
                								</span>
                							</div>
            							</div>
            						</div>
            						
            						<div class="box-footer">	
                						<div class="form-group">
                							<div class="col-md-12">

                    							<?php echo $mail_content ?>

                    							<hr>
                    							
                    							<label>첨부 파일</label>
            									<br>
                    				<?php
            						if (!empty($file_attachment)) {
            							foreach ($file_attachment as $key => $val) {
            								$file_path = $val['file_path'];
            								$file_name = $val['file_name'];
            								
            								if (is_file($file_path)) {
            						?>
    											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" style="padding: 5px; background-color: #00c0ef; border-radius: 3px; color: #fff;" title="<?php echo $file_name ?>">
    												<?php echo $file_name ?>
    											</a> &nbsp;
            						<?php
            								}
            							}
            						}
            						?>
                								
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
            					</div>
                			</div>
                			<div class="col-md-4">
                				<div class="box box-success">
                					<div class="box-header">
                						<h4>받는 사람</h4>
                					</div>
                					<div class="box-body">
        								<input type="checkbox" name="rcv_user_all" id="rcv_user_all" value="Y"> 회원 전체발송(미승인포함)
                					</div>                					
                					<div class="box-body">
                						<div class="form-group">
                							<label>회원 등급</label>
        						<?php 
        						foreach ($if_user_class as $key => $val) {
        						?>
        									<div class="checkbox">
        										<label>
        											<input type="checkbox" name="rcv_user_class[]" value="<?php echo $key ?>"> <?php echo $val ?>
        										</label>
        									</div>
        						<?php 
        						}
        						?>
        								</div>
                						<div class="form-group">
                							<label>그룹 주소록</label>
        						<?php 
        						foreach ($address_groups as $key => $val ) {
        						    $group_id = $val['seq_id'];
        						    $group_name = $val['group_name'];
        						?>
        									<div class="checkbox">
        										<label>
        											<input type="checkbox" name="rcv_group_id[]" value="<?php echo $group_id ?>"> <?php echo $group_name ?>
        										</label>
        									</div>
        						<?php 
        						}
        						?>
        								</div>
                					</div>
									<div class="box-footer text-center send_lading" style="display:none;">
										<div class="input-group-addon bg-gray"><i><small>메일발송 중입니다. 잠시 기다려주세요.</small></i></div>
									</div>                					
                					<div class="box-footer text-center">
            							<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">발송</button>
        								<img src='/include/img/view_loading.gif' class="send_lading" style="display:none;"/>
                					</div>
                					<!-- /.box-footer -->
                				</div>
                			</div>
                		</form>
            		</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
    		$('.send_lading').hide();
    		
        	$("#rcv_user_all").click(function() {
        		$("input[name='rcv_user_class[]']").prop("checked",false);     	
        	});
        	$("input[name='rcv_user_class[]']").click(function() {
            	if($("#rcv_user_all").prop("checked")){
            		$("input[name='rcv_user_class[]']").prop("checked",false); 
            	}
        	});       	
    		// 메일 발송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				$('.send_lading').show();
				$.ajax({
					type : "POST",
					url : "./ajax/send-mail-real.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						$('.send_lading').hide();
						if (res.code == "0") {
							if (res.result) {
								alert("메일을 보냈습니다.");
								location.href = "mail_list.php";
							} else {
								alert("메일을 보내지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 테스트 메일 발송
			$("#btn-send-test").click(function() {
				$.ajax({
					type : "POST",
					url : "./ajax/send-mail-test.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert($("#test_email_address").val() + " 님에게 메일을 보냈습니다.");
							} else {
								alert("메일을 보내지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
// 					$("#btn-send-test").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>