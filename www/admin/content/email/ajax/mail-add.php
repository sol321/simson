<?php
/*
 * Desc : 메일 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_subject'])) {
    $code = 101;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_content']))) {
    $code = 102;
    $msg = '메일 내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_mailing_content();

if (empty($result)) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>