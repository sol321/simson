<?php
/*
 * Desc : 메일 수정
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['seq_id'])) {
    $code = 111;
    $msg = '메일을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_subject'])) {
    $code = 101;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_content']))) {
    $code = 102;
    $msg = '메일 내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];

$result = if_update_mailing_content($seq_id);

if (empty($result)) {
    $code = 501;
    $msg = '저장하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>