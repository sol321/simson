<?php
/*
 * Desc : 그룹 메일 발송
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['content_id'])) {
    $code = 101;
    $msg = '메일 내용을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['sender_email']))) {
    $code = 102;
    $msg = '보내는 사람 이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['sender_email'], FILTER_VALIDATE_EMAIL)) {
    $code = 112;
    $msg = '보내는 사람의 이메일주소 형식이 잘못되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_subject']))) {
    $code = 103;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// 수신자 확인
$rcv_user_all = empty($_POST['rcv_user_all']) ? '' : $_POST['rcv_user_all'];  // 회원전체
$rcv_user_class = empty($_POST['rcv_user_class']) ? '' : $_POST['rcv_user_class'];  // 회원 등급
$rcv_academy = empty($_POST['rcv_academy']) ? '' : $_POST['rcv_academy'];           // 학회
$rcv_group_id = empty($_POST['rcv_group_id']) ? '' : $_POST['rcv_group_id'];        // 그룹 주소록
$to_emails = [];

if (empty($rcv_user_class) && empty($rcv_academy) && empty($rcv_group_id) && empty($rcv_user_all)) {
    $code = 110;
    $msg = '수신자 그룹을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if(!empty($rcv_user_all)){
    $query = "
            SELECT
                user_email  AS to_email,
                name_ko AS to_name,
                user_login  AS to_login
            FROM "
        . $GLOBALS['if_tbl_users'] . "
            WHERE
                show_hide = 'show' AND
                user_state IN ('10','30')
    ";
        $stmt = $ifdb->prepare($query);
        $stmt->execute();
        $contacts_users = $ifdb->get_results($stmt);
        $to_emails = array_merge($contacts_users);
}


if (!empty($rcv_user_class)) {
    $uclass = "'". implode("','", $rcv_user_class) . "'";
    
    $query = "
            SELECT
                user_email  AS to_email,
                name_ko     AS to_name,
                user_login  AS to_login
            FROM "
                . $GLOBALS['if_tbl_users'] . "
            WHERE
                show_hide = 'show' AND
                user_state = '30' AND
                user_class IN ($uclass)
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $contacts_users = $ifdb->get_results($stmt);
    $to_emails = array_merge($contacts_users);
}

if (!empty($rcv_academy)) {
    $sql_academy = '(';
    foreach ($rcv_academy as $key => $val) {
        $sql_academy .= "p.item_type_secondary = '$val' OR ";
    }
    $sql_academy = trim($sql_academy, 'OR ');
    $sql_academy .= ')';
    
    $query = "
            SELECT
                u.user_email  AS to_email,
                u.name_ko     AS to_name,
                u.user_login  AS to_login
            FROM "
                . $GLOBALS['if_tbl_users'] . " AS u
            INNER JOIN "
                . $GLOBALS['if_tbl_user_payment'] . " AS p
            WHERE
                u.show_hide = 'show' AND
                u.user_state = '10' AND
                p.expiry_date >= CURDATE() AND
                $sql_academy AND
                u.seq_id = p.user_id
    ";
    $stmt = $ifdb->prepare($query);
    $stmt->execute();
    $contacts_users = $ifdb->get_results($stmt);
    $to_emails = array_merge($contacts_users);
}

if (!empty($rcv_group_id)) {
    $agroup = "'". implode("','", $rcv_group_id) . "'";
    
    $query = "
            SELECT
                contact_email   AS to_email,
                contact_name    AS to_name,
                ''              AS to_login
            FROM "
                . $GLOBALS['if_tbl_address_contact'] . "
            WHERE
                group_id IN ($agroup)
    ";
        $stmt = $ifdb->prepare($query);
        $stmt->execute();
        $contacts_group = $ifdb->get_results($stmt);
        $to_emails = array_merge($to_emails, $contacts_group);
}

if (count($to_emails) == 0) {
    $code = 511;
    $msg = '메일 수신자가  없습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

/*
 * Desc: 중복 이메일 제거 
 * 
 * $to_emails = array_unique($to_emails, SORT_REGULAR); 은 정확히 3개의 element가 모두 같은 지를 비교하므로 사용하기 어려움.
 * 
 */
$arr_email = [];

foreach ($to_emails as $key => $val) {
    if (!in_array($val['to_email'], $arr_email)) {
        $arr_email[$key] = $val['to_email'];
    } else {
        unset($to_emails[$key]);
    }
}
unset($_POST['test_email_address']);
$result = if_send_mail_by_svr($to_emails);

if (empty($result)) {
    $code = 501;
    $msg = '발송하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>