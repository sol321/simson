<?php
/*
 * Desc : 주소 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-address.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['group_id'])) {
    $code = 101;
    $msg = '그룹을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['contact_name'])) {
    $code = 102;
    $msg = '이믈을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['contact_email'])) {
    $code = 103;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$email = trim($_POST['contact_email']);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $code = 105;
    $msg = '이메일 형식이 잘못되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_address_contact();

if (empty($result)) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>