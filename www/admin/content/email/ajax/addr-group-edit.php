<?php
/*
 * Desc : 그룹명 수정
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-address.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 111;
    $msg = '그룹을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['data'])) {
    $code = 101;
    $msg = '그룹 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['id'];

$result = if_update_address_group($seq_id);

if (empty($result)) {
    $code = 501;
    $msg = '저장하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>