<?php
/*
 * Desc : 테스트 메일 발송
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['content_id'])) {
    $code = 101;
    $msg = '메일 내용을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['sender_email']))) {
    $code = 102;
    $msg = '보내는 사람 이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['sender_email'], FILTER_VALIDATE_EMAIL)) {
    $code = 112;
    $msg = '보내는 사람의 이메일주소 형식이 잘못되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_subject']))) {
    $code = 103;
    $msg = '메일 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['test_email_address']))) {
    $code = 104;
    $msg = '수신자 이메일주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['test_email_address'], FILTER_VALIDATE_EMAIL)) {
    $code = 105;
    $msg = '수신자의 이메일주소 형식이 잘못되었습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$test_email = array(
    'to_email' => $_POST['test_email_address'],
    'to_name'  => '테스트',
    'to_login' => ''
);
$to_emails = array($test_email);

$result = if_send_mail_by_svr($to_emails);

if (empty($result)) {
    $code = 501;
    $msg = '발송하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>