<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-term.php';

if_authenticate_admin();

$taxonomy = empty($_GET['tkey']) ? 'cms_lnb_menu' : $_GET['tkey'];

$term_count = if_get_term_group_count($taxonomy);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<!-- fancytree -->
		<link href="<?php echo ADMIN_INC_URL ?>/lib/fancytree/skin-lion/ui.fancytree.css" rel="stylesheet">
		<script src="<?php echo ADMIN_INC_URL ?>/lib/fancytree/jquery.fancytree.min.js"></script>
		<!-- contextMenu -->
		<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/jquery-contextmenu@2.6.4/dist/jquery.contextMenu.min.css">
		<script src="//cdn.jsdelivr.net/npm/jquery-contextmenu@2.6.4/dist/jquery.contextMenu.min.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/lib/fancytree/jquery.fancytree.contextMenu.js"></script>
		<!-- dnd -->
		<script src="<?php echo ADMIN_INC_URL ?>/lib/fancytree/jquery.fancytree.dnd.js"></script>
		
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<section class="content-header">
					<h1>관리자 메뉴 관리</h1>
				</section>
				<section class="content">
					<div class="col-md-4">
						<form id="form-item-new">
							<input type="hidden" name="parent_id" id="parent_seq_id" value="0">
							<input type="hidden" name="term_group" value="<?php echo $taxonomy ?>">
							<div class="box box-success">
								<div class="box-body">
									<div class="form-group">
										<label>메뉴 이름</label>
										<input type="text" name="term_name" id="term_name" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Relative File Path</label>
										<input type="text" name="menu_url" id="menu_url" class="form-control" value="">
										<div class="help-block">
											<b>사용 예)</b> /<?php echo ADMIN_DIR ?>/content/conf/setup.php?type=abc
										</div>
									</div>
									<div class="form-group">
										<label>메뉴 노출 여부</label>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="is_hidden" value="Y"> 메뉴를 숨깁니다
											</label>
										</div>
									</div>
									<button type="submit" class="btn btn-success" id="btn-submit">
										<i class="fa fa-check"></i> 등록합니다 
									</button>
								</div>
							</div><!-- /. box -->
						</form>
						
						<form id="form-item-edit" class="hide">
							<input type="hidden" name="seq_id" id="term_seq_id">
							<div class="box box-warning">
								<div class="box-body">
									<div class="form-group">
										<label>메뉴 이름</label>
										<input type="text" name="term_name" id="term_name_edit" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Relative File Path</label>
										<input type="text" name="menu_url" id="menu_url_edit" class="form-control">
									</div>
									<div class="form-group">
										<label>메뉴 노출 여부</label>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="is_hidden" id="is_hidden" value="Y"> 메뉴를 숨깁니다
											</label>
										</div>
									</div>
									<button type="submit" class="btn btn-warning">
										<i class="fa fa-check"></i> 저장합니다 
									</button>
								</div>
							</div><!-- /. box -->
						</form>
					</div>
					<div class="col-md-8">
						<div class="box box-info">
							<div class="box box-header">
					<?php 
					if ($term_count == 0) {
					?>
								<div class="pull-left">
									<button type="button" id="insert-sample-data" class="btn btn-warning btn-xs">샘플 메뉴 등록</button>
								</div>
					<?php 
					}
					?>
								<div class="pull-right">
									<button type="button" id="tree-expand" class="btn btn-success btn-sm">모두 확장</button>
									<button type="button" id="tree-reduce" class="btn btn-info btn-sm">모두 축소</button>
								</div>
							</div>
							<div class="box-body">
								<div id="tree"></div>
							</div>
						</div><!-- /. box -->
					</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- jQuery Validate Plugin -->
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
		$(function () {
			$("#form-item-new").validate({
				rules: {
				},
				messages: {
				},
				submitHandler: function(form) {
					$.ajax({
						type : "POST",
						url : "./ajax/menu-add.php",
						data : $("#form-item-new").serialize(),
						dataType : "json",
						beforeSend : function() {
							$("#btn-submit").prop("disabled", true);
						},
						success : function(res) {
							if (res.code == "0") {
								if (res.result) {
									location.reload();
								} else {
									alert("등록하지 못했습니다.");
								}
							} else {
								alert(res.msg);
							}
						}
					}).done(function() {
					}).fail(function() {
					}).always(function() {
						$("#btn-submit").prop("disabled", false);
					}); // ajax
				}
			});
			
			$("#form-item-edit").validate({
				rules: {
				},
				messages: {
				},
				submitHandler: function(form) {
					$.ajax({
						type : "POST",
						url : "./ajax/menu-edit.php",
						data : $("#form-item-edit").serialize(),
						dataType : "json",
						beforeSend : function() {
						},
						success : function(res) {
							if (res.code == "0") {
								if (res.result) {
									location.reload();
								} else {
									alert("등록하지 못했습니다.");
								}
							} else {
								alert(res.msg);
							}
						}
					}).done(function() {
					}).fail(function() {
					}).always(function() {
					}); // ajax
				}
			});

			
			// fancytree
			$("#tree").fancytree({
				extensions: ["contextMenu", "dnd"],
				source: {
					url: "./ajax/get_json_tree_item.php?tkey=<?php echo $taxonomy ?>"
				},
				debugLevel: 0,
				contextMenu: {
					menu: {
						'add': { 'name': '추가(Add)', 'icon': 'add' },
						'edit': { 'name': '편집(Edit)', 'icon': 'edit' },
						'sep1': '---------',
						'delete': { 'name': '삭제(Delete)', 'icon': 'delete' },
						'sep2': '---------',
						'quit': { 'name': '닫기(Quit)', 'icon': 'quit' }
					},
					actions: function(node, action, options) {
						if (action == "add") {
							addNode(node);
						} else if (action == "edit") {
							editNode(node);
						} else if (action == "delete") {
							deleteNode(node);
						}
					}
				},
				dnd: {
					autoExpandMS: 800,
					focusOnClick: true,
					preventVoidMoves: true,
					preventRecursiveMoves: true,
					dragStart: function(node, data) {
					  return true;
					},
					dragEnter: function(node, data) {
					   return true;
					},
					dragDrop: function(node, data) {
					  data.otherNode.moveTo(node, data.hitMode);
					  moveNode(data.otherNode, node);
					}
				},
				click: function(event, data) {
					document.getElementById("form-item-new").reset();
					document.getElementById("form-item-edit").reset();
				}
			});

			function addNode(node) {
				$("#parent_seq_id").val(node.key);
				$("#form-item-new").removeClass("hide");
				$("#form-item-edit").addClass("hide");
				$("#term_name").focus();
			}
			
			function editNode(node) {
				$("#term_seq_id").val(node.key);
				$("#term_name_edit").val(node.data.titleOnly);
				$("#menu_url_edit").val(node.data.url);
				$("#is_hidden").prop("checked", node.tooltip == "hide" ? true : false);

				$("#form-item-new").addClass("hide");
				$("#form-item-edit").removeClass("hide");
				$("#term_name").focus();
			}

			function deleteNode(node) {
				if (!confirm("삭제하시겠습니까?")) {
					return;
				}
				
				$.ajax({
					type : "POST",
					url : "./ajax/menu-delete.php",
					data : {
						"id" : node.key
					},
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			}
			
			function moveNode(moved, node) {
				if (moved.key == node.key) {
					return;
				}

				var nodeKeys = [];
				for ( var i = 0, len = node.parent.children.length; i < len; i++ ) {
					nodeKeys.push( node.parent.children[i].key );
				}

				$.ajax({
					type : "POST",
					url : "./ajax/menu-move.php",
					data : {
						"actionID" : moved.key,
						"targetID" : node.key,
						"relatedNodes" : nodeKeys
					},
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			}

			// 모두 확장
			$("#tree-expand").click(function() {
				$("#tree").fancytree("getRootNode").visit(function(node){
					node.setExpanded(true);
				});
			});
			// 모두 취소
			$("#tree-reduce").click(function() {
				$("#tree").fancytree("getRootNode").visit(function(node){
					node.setExpanded(false);
				});
			});

			// 샘플 메뉴 등록
			$("#insert-sample-data").click(function() {
				$.ajax({
					type : "POST",
					url : "./ajax/insert-sample-data.php",
					data : {
						"act": "insert"
					},
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
		</div>
		<!-- ./wrapper -->

	</body>
</html>