<?php
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-term.php';

$code = 0;
$msg = '';

if (if_get_current_admin_level() < '9000') {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['term_group'])) {
    $code = 101;
    $msg = '분류를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['term_name'])) {
    $code = 102;
    $msg = '메뉴 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_term();

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>