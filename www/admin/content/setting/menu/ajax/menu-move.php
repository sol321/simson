<?php
/*
 * Desc: 메뉴(노드) 이동하기.
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-term.php';

$code = 0;
$msg = '';

if (if_get_current_admin_level() < '9000') {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['actionID'])) {
    $code = 103;
    $msg = '메뉴를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['targetID'])) {
    $code = 102;
    $msg = '메뉴를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$action_id = $_POST['actionID'];
$target_id = $_POST['targetID'];
$related_nodes = $_POST['relatedNodes'];

if (in_array( $action_id, $related_nodes)) {		// siblings
    $result = if_move_term_to_sibling($action_id, $target_id, $related_nodes);
} else {	// child
    $result = if_move_term_to_parent($action_id, $target_id);
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>