<?php
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-term.php';

$code = 0;
$msg = '';

if (if_get_current_admin_level() < '9000') {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['act'])) {
    $code = 101;
    $msg = '실행 중 에러가 발생했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$sql_file = ADMIN_PATH . '/content/_files/if_terms_sample.sql';

$result = if_add_sample_term_data($sql_file);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>