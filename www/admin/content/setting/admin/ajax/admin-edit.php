<?php
/*
 * Desc: 관리자 정보 변경
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['admin_id'])) {
    $code = 105;
    $msg = '관리자을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$admin_id = $_POST['admin_id'];

$result = if_update_admin($admin_id);

if (empty($result)) {
    $code = 201;
    $msg = '정보 변경을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>