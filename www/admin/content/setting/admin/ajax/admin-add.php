<?php
/*
 * Desc: 관리자 추가
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['admin_login'])) {
    $code = 105;
    $msg = '아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['admin_name'])) {
    $code = 106;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['admin_pass'])) {
    $code = 107;
    $msg = '비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$admin_login = trim($_POST['admin_login']);

if (!ctype_alnum($admin_login)) {
    $code = 110;
    $msg = '아이디는 알파벳과 숫자만 사용하실 수 있습니다. 아이디를 다시 입력해 주십시오';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

if ( if_exist_admin($admin_login) ) {
    $code = 501;
    $msg = $admin_login . ' 아이디는 이미 사용중입니다. 다른 아이디를 입력해 주십시오';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$seq_id = if_add_sub_admin('1000');

if ( empty($seq_id) ) {
    $code = 500;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit( json_encode($json) );
}

$json = compact('code', 'msg');
echo json_encode($json);

?>