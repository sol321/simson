<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

if_authenticate_admin();

require_once ADMIN_PATH . '/include/cms-header.php';
?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						관리자 추가
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<form id="form-item-edit">
							<div class="col-md-5">
								<div class="box box-warning">
									<div class="box-body">
										<div class="form-group">
											<label>아이디</label>
											<input type="text" name="admin_login" id="admin_login" class="form-control">
										</div>
										<div class="form-group">
											<label>이름</label>
											<input type="text" name="admin_name" id="admin_name" class="form-control">
										</div>
										<div class="form-group">
											<label>비밀번호</label>
											<input type="password" name="admin_pass" id="admin_pass" class="form-control" autocomplete="new-password">
										</div>
									</div><!-- /.box-body -->
									<div class="box-footer">
										<button type="button" class="btn btn-default" id="btn-cancel"><i class="fa fa-mail-reply"></i> 뒤로가기</button>
										<div class="pull-right">
											<button type="submit" class="btn btn-primary" id="btn-user-edit">
												추가합니다
											</button>
										</div>

									</div><!-- /.box-footer -->
								</div><!-- /. box -->
							</div><!-- /.col -->
						</form>
						<div class="col-md-4">
						<!-- Contents -->
						</div>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		

	<!-- jQueryUI -->
	<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
	<script>
	$(function () {
		$("#form-item-edit").submit(function(e) {
			e.preventDefault();

			if ($.trim($("#admin_login").val()) == "") {
				alert("아이디를 입력해 주십시오.");
				$("#admin_login").focus();
				return false;
			}

			if ($.trim($("#admin_name").val()) == "") {
				alert("이름을 입력해 주십시오.");
				$("#admin_name").focus();
				return false;
			}

			$.ajax({
				type : "POST",
				url : "./ajax/admin-add.php",
				data : $(this).serialize(),
				dataType : "json",
				beforeSend : function() {

				},
				success : function(res) {
					if (res.code == "0") {
						location.href = "admin_list.php";
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax

		});
		

		$("#btn-cancel").click(function() {
			history.back();
		});

	});
	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
