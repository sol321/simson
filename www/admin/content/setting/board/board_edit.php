<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('게시판을 선택해 주십시오.');
}

$seq_id = $_GET['id'];

$row = if_get_board($seq_id);

if (empty($row)) {
    if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

$tpl_name = $row['tpl_name'];
$tpl_skin = $row['tpl_skin'];
$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, TRUE);
$tpl_max_filesize = $jdec['tpl_max_filesize'];

// php.ini
$upload_max_size = ini_get('upload_max_filesize');
$max_mbytes = if_sanitize_numeric($upload_max_size);
$read_level = !empty($jdec['read_level'])?$jdec['read_level']:0;
$write_level = !empty($jdec['write_level'])?$jdec['write_level']:0;
$use_comment = !empty($jdec['use_comment'])?$jdec['use_comment']:'N';
require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>게시판 편집</h1>
				</div>
				<div class="content">
					<div class="box box-info">
        				<div class="box-header with-border">
        					<h3 class="box-title"></h3>
        				</div>
        				<!-- /.box-header -->
        				<!-- form start -->
        				<form id="form-item-edit" class="form-horizontal">
        					<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
        					<div class="box-body">
        						<div class="form-group">
        							<label class="col-md-3 control-label">게시판 이름</label>
        							<div class="col-md-5">
        								<input type="text" name="tpl_name" class="form-control" id="tpl_name" placeholder="게시판 이름" value="<?php echo $tpl_name ?>">
        							</div>
        						</div>
        						<div class="form-group">
        							<label class="col-md-3 control-label">게시판 종류</label>
        							<div class="col-md-5">
        					<?php 
        					foreach ($if_board_template as $key => $val) {
        					    $checked = strcmp($key, $tpl_skin) ? '' : 'checked';
        					?>
        								<div class="radio">
            								<label>
            									<input type="radio" name="tpl_skin" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
            								</label>
            							</div>
        					<?php 
        					}
        					?>
        								<label id="nation-error" class="error" for="nation"></label>
        							</div>
        						</div>      						
        						<div class="form-group">
        							<label class="col-md-3 control-label">업로드 파일 최대 크기</label>
        							<div class="col-md-5">
        								<div class="input-group">
            								<input type="text" name="tpl_max_filesize" class="form-control numeric" id="tpl_max_filesize" placeholder="숫자만 입력하십시오." value="<?php echo $tpl_max_filesize ?>" min="0" max="<?php echo $max_mbytes ?>">
            								<span class="input-group-addon">MB</span>
            							</div>
        							</div>
        						</div>
         						<div class="form-group">
        							<label class="col-md-3 control-label">권한설정</label>
        							<div class="col-md-5">
                       					<div class="box-body table-responsive no-padding">
                    						<table class="table table-hover">
                        						<colgroup>
                        							<col style="width: 20%;">
                        							<col style="width: 80%;">
                        						</colgroup>
                        						<tbody>
                        							<tr>
                        								<th>읽기</th>
                        								<td>
                        									<input type="radio" name="read_level" id="read_level1" value="0" <?php if($read_level=="0") echo "checked";?>>비회원&nbsp;&nbsp;&nbsp;
                        									<input type="radio" name="read_level" id="read_level2" value="1000" <?php if($read_level=="1000") echo "checked";?>>회원&nbsp;&nbsp;&nbsp;
                        									<input type="radio" name="read_level" id="read_level3" value="3000" <?php if($read_level=="3000") echo "checked";?>>관리자
                        								</td>
                        							</tr>
                        							<tr>
                        								<th>쓰기</th>
                        								<td>
                         									<input type="radio" name="write_level" id="write_level1" value="0" <?php if($write_level=="0") echo "checked";?>>비회원&nbsp;&nbsp;&nbsp;
                        									<input type="radio" name="write_level" id="write_level2" value="1000" <?php if($write_level=="1000") echo "checked";?>>회원&nbsp;&nbsp;&nbsp;
                        									<input type="radio" name="write_level" id="write_level3" value="3000" <?php if($write_level=="3000") echo "checked";?>>관리자                       								
                        								</td>
                        							</tr>
                        						</tbody>
                        					</table>       								
            							</div>
        							</div>
        						</div> 
        						<div class="form-group">
        							<label class="col-md-3 control-label">댓글사용</label>
        							<div class="col-md-5">
        								<label class="radio-inline"><input type="radio" name="use_comment" id="use_comment1" value="Y" <?php echo (!strcmp($use_comment,'Y'))?' checked':''?>> &nbsp;사용</label>
        								<label class="radio-inline"><input type="radio" name="use_comment" id="use_comment2" value="N" <?php echo (!strcmp($use_comment,'N'))?' checked':''?>> &nbsp;미사용</label>
        							</div>
        						</div>        						        						
        						<!-- 
        						<div class="form-group">
        							<label class="col-md-3 control-label">사용 권한</label>
        							<div class="col-md-5">
        								<input type="text" name="" class="form-control numeric" id="" placeholder="">
        							</div>
        						</div>
        						 -->
        					</div>
        					<!-- /.box-body -->
        					<div class="box-footer text-center">
        						<a href="board_list.php" id="btn-cancel" class="btn btn-default">리스트로</a> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
        					</div>
        					<!-- /.box-footer -->
        				</form>
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		<script>
		$("input").prop("required", true);	// All fields required
		$(".numeric").numeric({ negative: false });
		$("#form-item-edit").validate({
			rules: {
				physician: {
					required: true
				}
			},
			messages: {
			},
			submitHandler: function(form) {
				$.ajax({
					type : "POST",
					url : "./ajax/board-edit.php",
					data : $("#form-item-edit").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							location.href = "board_list.php";
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			}
		});

		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>