<?php
/*
 * Desc: 게시판 삭제 (hide)
 *  if_post_template
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '게시판을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$id = $_POST['id'];

$result = if_delete_board($id);

if (empty($result)) {
    $code = 201;
    $msg = '게시판을 삭제할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>