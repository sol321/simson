<?php
/*
 * Desc: 게시판 편집
 *  if_post_template
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['seq_id'])) {
    $code = 101;
    $msg = '게시판을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['tpl_name'])) {
    $code = 102;
    $msg = '게시판 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_update_board();

if (empty($result)) {
    $code = 201;
    $msg = '게시판을 생성할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>