<?php
require_once '../../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
    $sql .= " AND tpl_name LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_post_template'] . " AS t
		WHERE
			show_hide = 'show'
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>게시판 리스트</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<form id="search-form">
        						<div class="box-tools">
        							<a href="board_add.php" class="btn btn-info btn-sm">
        								<i class="fa fa-plus-square-o"></i> 게시판 생성
        							</a>
        							<div class="pull-right">
            							<div class="input-group input-group-sm" style="width: 350px;">
            								<input type="text" name="qs1" class="form-control pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
            								
            								<div class="input-group-btn">
            									<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            								</div>
            							</div>
            						</div>
        						</div>
        					</form>
						</div>
						<!-- /.box-header -->
    					<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th style="width: 10px">#</th>
        								<th>게시판 이름</th>
        								<th>URL</th>
        								<th>게시판 형식</th>
        								<th>업로드 파일 최대 크기</th>
        								<th>-</th>
    								</tr>
    					<?php 
        				if (!empty($item_results)) {
        				    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        				    
        					foreach ($item_results as $key => $val) {
        						$seq_id = $val['seq_id'];
        						$tpl_name = $val['tpl_name'];
        						$tpl_skin = $val['tpl_skin'];
        						$meta_data = $val['meta_data'];
        						$jdec = json_decode($meta_data, TRUE);
        						$tpl_max_filesize = $jdec['tpl_max_filesize'];
        				?>
    								<tr id="item-<?php echo $seq_id ?>">
    									<td><?php echo $list_no ?></td>
    									<td><?php echo $tpl_name ?></td>
    									<td>
    							<?php 
    							if (strcmp($tpl_skin, 'QNA')) {
    							?>
    										<a href="<?php echo ADMIN_URL ?>/content/post/post_list.php?bt=<?php echo $seq_id ?>" target="_blank" class="label label-primary">Link</a>
    							<?php 
    							} else {
    							?>
    										<a href="<?php echo ADMIN_URL ?>/content/post/qna_list.php?bt=<?php echo $seq_id ?>" target="_blank" class="label label-warning">Link</a>
    							<?php 
    							}
    							?>
    									</td>
    									<td><?php echo $tpl_skin ?></td>
    									<td><?php echo $tpl_max_filesize ?></td>
    									<td>
        									<a href="board_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">Modify</a>
        									<a href="javascript:;" class="badge bg-red action-delete">Delete</a>
        								</td>
    								</tr>
    					<?php 
    					       $list_no--;
        					}
        				}
    					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
    	$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/board-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>