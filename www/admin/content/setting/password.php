<?php
require_once '../../../if-config.php';

if_authenticate_admin();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<!-- <h1>Change Password</h1> -->
				</div>
				<div class="content">
					<div class="col-md-6">
    					<div class="box box-info">
    						<!-- form start -->
            				<form id="form-item-new" class="form-horizontal">
            					<div class="box-header with-border">
            						<h1 class="box-title text-info">비밀번호 변경</h1>
            					</div>
            					<div class="box-body">
            						<div class="form-group">
            							<label for="name" class="col-md-3 control-label">현재 비밀번호</label>
            							<div class="col-md-8">
            								<input type="password" name="password" id="password" class="form-control" placeholder="">
            							</div>
            						</div>
            						
            						<div class="form-group">
            							<label for="name" class="col-md-3 control-label">새 비밀번호</label>
            							<div class="col-md-8">
            								<input type="password" name="password_new" id="password_new" class="form-control" placeholder="6자 이상">
            							</div>
            						</div>
            						<div class="form-group">
            							<label for="name" class="col-md-3 control-label">새 비밀번호 확인</label>
            							<div class="col-md-8">
            								<input type="password" name="password_new_repeat" id="password_new_repeat" class="form-control" placeholder="">
            							</div>
            						</div>
            					</div>
            					<!-- /.box-body -->
            					
            					<div class="box-footer text-center">
            						<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
            					</div>
            					<!-- /.box-footer -->
            				</form>
    					</div>
    				</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<script>
    	$(function () {
    		$("input").prop("required", true);	// All fields required
    		
    		$("#form-item-new").validate({
    			rules: {
    				password: {
						required: true,
						minlength: 6
					},
    				password_new: {
    					required: true,
    					minlength: 6
    				},
    				password_new_repeat: {
    					required: true,
    					minlength: 6,
    					equalTo: "#password_new"
    				}
    			},
    			messages: {
    				password: {
    					required: "현재 비밀번호를 입력해 주십시오.",
    					minlength: "최소 6자 이상의 글자를 입력해 주십시오."
					},
    				password_new: {
    					required: "필수 입력 항목입니다.",
    					minlength: "최소 6자 이상의 글자를 입력해 주십시오."
    				},
    				password_new_repeat: {
    					required: "필수 입력 항목입니다.",
    					minlength: "최소 6자 이상의 글자를 입력해 주십시오.",
    					equalTo: "비밀번호가 일치하지 않습니다."
    				}
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/password.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							alert("변경했습니다.");
    							location.reload();
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>