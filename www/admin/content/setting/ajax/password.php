<?php
/*
 * Desc: 관리자 비밀번호 변경
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-admin.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password'])) {
    $code = 101;
    $msg = '현재 비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password_new'])) {
    $code = 102;
    $msg = '새로운 비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['password_new_repeat'])) {
    $code = 103;
    $msg = '새로운 비밀번호 확인을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (strcmp($_POST['password_new_repeat'], $_POST['password_new'])) {
    $code = 104;
    $msg = '새로운 비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$admin_id = if_get_current_admin_id();
$password = $_POST['password'];
$password_new = $_POST['password_new'];

// 현재 비밀번호 검증
$user_row = if_get_admin($admin_id);
$admin_pw = $user_row['admin_pw'];

$check_passwd = if_verify_passwd($password, $admin_pw);

if (!$check_passwd) {
    $code = 202;
    $msg = '현재 비밀번호가 일치하지 않습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// update password
$result = if_update_admin_password($admin_id, $password_new);

if (empty($result)) {
    $code = 201;
    $msg = '비밀번호를 변경하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>