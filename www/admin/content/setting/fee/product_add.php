<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

$class_lists = if_get_user_class_list();	// 회원 등급

$item_years = if_get_item_all_year(); // 연도별

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회비 항목 관리</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- form start -->
							<form id="form-item-new" class="form-horizontal">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">회비 항목 생성</h3>
									</div>
									<div class="box-body">
										<div class="form-group">
											<label class="col-md-4 control-label">연도</label>
											<div class="col-md-8">
												<select name="product_year" id="product_year" class="form-control">
													<option value="">- 선 택 -</option>
									<?php 
									if (!empty($item_years)) {
										foreach ($item_years as $key => $val) {
											$item_year = $val['item_year'];
									?>
													<option value="<?php echo $item_year ?>"><?php echo $item_year ?></option>
									<?php 
										}
									}
									?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">회원 등급</label>
											<div class="col-md-8">
									<?php 
									if (!empty($class_lists)) {
										foreach ($class_lists as $k => $v) {
											$class_sid = $v['seq_id'];
											$class_name = $v['class_name'];
									?>
												<div class="radio">
													<label>
														<input type="radio" name="class_sid" class="class_sid" value="<?php echo $class_sid ?>">
														<span><?php echo $class_name ?></span>
													</label>
												</div>
									<?php
										}
									}
									?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">비용 목록</label>
											<div class="col-md-8" id="product-type-wrap">
												<small>연도 선택 시, 등록된 비용 항목을 불러옵니다.</small>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">회비 이름</label>
											<div class="col-md-8">
												<input type="text" name="product_name" class="form-control" id="product_name">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">금액</label>
											<div class="col-md-8">
												<input type="text" name="product_price" class="form-control numeric" id="product_price">
											</div>
										</div>
									</div>
									<!-- /.box-body -->
									<div class="box-footer text-center">
										<a href="product_list.php" class="btn btn-default">리스트</a> &nbsp; &nbsp;
										<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
									</div>
									<!-- /.box-footer -->
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
		$(function() {
			$("input, select").prop("required", true);	// All fields required
			$(".numeric").numeric({ negative: false });

			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				
				$.ajax({
					type : "POST",
					url : "./ajax/product-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							location.href = "product_list.php";
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});
			
			// 연도 선택 > 해당 연도 비용 목록 조회
			$("#product_year").change(function() {
				$('input[type="text"]').val("");
				$('input[type="radio"]').prop("checked", false);
				
				var year = $(this).val();

				$.ajax({
					type : "POST",
					url : "./ajax/get-item-list.php",
					data : {
						"year": year
					},
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							$("#product-type-wrap").html(res.item_element);
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 회비 이름 자동 입력
			$(document).on("click", ".class_sid, .item_name", function() {
				var classLabel = $(".class_sid:checked").parent().find("span").text();
				var itemLabel = $(".item_sid:checked").parent().find("span").text();
//			 	$("#product_name").val(itemLabel + " - " + classLabel);
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>