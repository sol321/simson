<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('등급을 선택해 주십시오.');
}

$seq_id = $_GET['id'];

$row = if_get_user_class($seq_id);

$class_name = $row['class_name'];
$class_comment = $row['class_comment'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회원 등급 관리</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
                				<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
    							<div class="box box-info">
    								<div class="box-header with-border">
                    					<h3 class="box-title">회원 등급 편집</h3>
                    				</div>
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">등급 이름</label>
                							<div class="col-md-8">
                								<input type="text" name="class_name" class="form-control" id="class_name" placeholder="준회원" value="<?php echo $class_name ?>">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">&nbsp;</label>
                							<div class="col-md-8">
                								<label>
               										<input type="checkbox" name="is_default" value="1"> <small>회원 가입 시, 기본 등급입니다.</small>
               									</label>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="class_comment" class="form-control"><?php echo $class_comment ?></textarea>
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<a href="user_class_list.php" class="btn btn-default">리스트</a> &nbsp; &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
                					</div>
                					<!-- /.box-footer -->
        						</div>
        					</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
    		$("input").prop("required", true);	// All fields required

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/user-class-edit.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.href = "user_class_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>