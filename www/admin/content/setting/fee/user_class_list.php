<?php
require_once '../../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 50;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qs1)) {
    $sql .= " AND class_name LIKE ? OR class_comment LIKE ?";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_user_class'] . "
		WHERE
			1
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$user_class_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회원 등급 관리</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-8">
        					<div class="box box-info">
        						<div class="box-header">
        							<h4>회원 등급 목록</h4>
        						</div>
        						<!-- /.box-header -->
            					<div class="box-body table-responsive no-padding">
        							<table class="table table-hover">
        								<tbody>
        									<tr class="active">
            									<th style="width: 10px">#</th>
                								<th>회원 등급 이름</th>
                								<th>기본 등급</th>
                								<th>메모</th>
                								<th>-</th>
            								</tr>
            					<?php 
                				if (!empty($user_class_results)) {
                				    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
                				    
                					foreach ($user_class_results as $key => $val) {
                						$seq_id = $val['seq_id'];
                						$class_name = $val['class_name'];
                						$is_default = $val['is_default'];
                						$class_comment = $val['class_comment'];
                						$default_label = $is_default ? '<span class="label label-success">기본</span>' : '';
                				?>
            								<tr id="item-<?php echo $seq_id ?>">
            									<td><?php echo $list_no ?></td>
            									<td><?php echo $class_name ?></td>
            									<td><?php echo $default_label ?></td>
            									<td><?php echo $class_comment ?></td>
            									<td>
                									<a href="user_class_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">수정</a>
                									<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
                								</td>
            								</tr>
            					<?php 
            					       $list_no--;
                					}
                				}
            					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
            					<div class="box-footer text-center">
                		<?php
                		echo $paginator->if_paginator();
                		?>
                				</div>
            					<!-- /.box-footer -->
        					</div>
        				</div>
        				<div class="col-md-4">
        					<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
    							<div class="box box-primary">
    								<div class="box-header with-border">
                    					<h3 class="box-title">회원 등급 만들기</h3>
                    				</div>
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">등급 이름</label>
                							<div class="col-md-8">
                								<input type="text" name="class_name" class="form-control" id="class_name" placeholder="준회원">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">&nbsp;</label>
                							<div class="col-md-8">
                								<label>
               										<input type="checkbox" name="is_default" value="1"> <small>회원 가입 시, 기본 등급입니다.</small>
               									</label>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="class_comment" class="form-control"></textarea>
                							</div>
                						</div>
                						<div class="text-center">
            								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
                    					</div>
                					</div>
                					<!-- /.box-body -->
        						</div>
        					</form>
        				</div>
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
    		$("#class_name").prop("required", true);	// All fields required

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/user-class-add.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.href = "user_class_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    		
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/user-class-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>