<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$prod_number = if_get_product_number();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 50;		// 리스트 개수

// search
$qa1 = !isset($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 연도 검색
if (!empty($qa1)) {
    $sql .= " AND product_year = ?";
    array_push($sparam, $qa1);
}
// 키워드 검색
if (!empty($qs1)) {
    $sql .= " AND product_name LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_item_product'] . " AS p
		WHERE
			p.show_hide = 'show'
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

$item_years = if_get_item_all_year(); // 연도별

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회비 항목 관리</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<form id="search-form">
        						<div class="box-tools">
        							<a href="product_add_bulk.php" class="btn btn-success btn-sm">
        								회비 항목 일괄 등록
        							</a> &nbsp;
        							<a href="product_add.php" class="btn btn-info btn-sm">
        								새 항목 등록
        							</a> 
        							
        							<div class="pull-right">
        								<div class="input-group input-group-sm" style="width: 250px;">
            								<input type="text" name="qs1" class="form-control pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
            								<div class="input-group-btn">
            									<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            								</div>
            							</div>
            						</div>
        							<div class="pull-right">
        								<div class="input-group input-group-sm" style="width: 150px;">
            								<select name="qa1" id="qa1" class="form-control">
    											<option value="">- 선 택 -</option>
    							<?php 
    							if (!empty($item_years)) {
    								foreach ($item_years as $key => $val) {
    									$item_year = $val['item_year'];
    									$selected = $qa1 == $item_year ? 'selected' : '';
    							?>
    											<option value="<?php echo $item_year ?>" <?php echo $selected ?>><?php echo $item_year ?></option>
    							<?php 
    								}
    							}
    							?>
    										</select>
            							</div>
            						</div>
        						</div>
        					</form>
						</div>
						<!-- /.box-header -->
    					<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th style="width: 10px">#</th>
        								<th>연도</th>
        								<th>회비 종류</th>
        								<th>회비 이름</th>
        								<th>금액</th>
        								<th>-</th>
    								</tr>
    					<?php 
        				if (!empty($item_results)) {
        				    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        				    
        					foreach ($item_results as $key => $val) {
        						$product_id = $val['product_id'];
        						$product_name = $val['product_name'];
        						$product_price = $val['product_price'];
        						$product_year = $val['product_year'];
        						$product_period = $val['product_period'];
        						$product_type = $val['product_type'];
        						$product_period_label = empty($product_period) ? '' : '(' . $product_period .'년)';
        				?>
    								<tr id="item-<?php echo $product_id ?>">
    									<td><?php echo $list_no ?></td>
    									<td><?php echo $product_year ?></td>
    									<td>
    										<?php echo $if_fee_type[$product_type] ?>
    										<?php echo $product_period_label ?>
    									</td>
    									<td><?php echo $product_name ?></td>
    									<td><?php echo number_format($product_price) ?></td>
    									<td>
        									<a href="product_edit.php?id=<?php echo $product_id ?>" class="badge bg-aqua">수정</a>
        									<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
        								</td>
    								</tr>
    					<?php 
    					       $list_no--;
        					}
        				}
    					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
    	$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/product-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		// 연도 선택
			$("#qa1").change(function() {
				var year = $(this).val();
				location.href = "?qa1=" + year;
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>