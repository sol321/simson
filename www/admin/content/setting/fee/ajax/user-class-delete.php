<?php
/*
 * Desc: 회원등급 삭제 (delete)
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '회원등급을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$id = $_POST['id'];

$result = if_delete_user_class($id);

if (empty($result)) {
    $code = 201;
    $msg = '회원등급을 삭제할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>