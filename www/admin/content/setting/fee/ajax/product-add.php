<?php
/*
 * Desc: 회비 항목 생성
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['product_year'])) {
    $code = 101;
    $msg = '연도를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['class_sid'])) {
    $code = 112;
    $msg = '회원등급을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['item_sid'])) {
    $code = 103;
    $msg = '비용 목록을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['product_name'])) {
    $code = 102;
    $msg = '회비 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['product_price'])) {
    $code = 105;
    $msg = '회비를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$class_sid = $_POST['class_sid'];   // 회원 등급
$year = $_POST['product_year'];

$item_sid = $_POST['item_sid'];   // item seq_id
$item_row = if_get_item($item_sid);

$type = $item_row['item_type'];
$period = $item_row['item_period'];

if (strcmp($type, 'ETC')) {
    $id = if_check_duplicate_product($class_sid, $year, $type, $period);
    
    if (!empty($id)) {
        $code = 201;
        $msg = '이미 동일한 회비 항목이 등록되어 있습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_add_product();

if (empty($result)) {
    $code = 201;
    $msg = '회비 항목을 생성할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>