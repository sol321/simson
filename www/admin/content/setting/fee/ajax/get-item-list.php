<?php
/*
 * Desc: 연도에 해당하는 비용 목록 조회
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$item_element = '';

if (empty($_POST['year'])) {
    $json = compact('code', 'msg', 'item_element');
    exit(json_encode($json));
}

$year = $_POST['year'];
$result = if_get_item_list($year);

if (!empty($result)) {
    foreach ($result as $key => $val) {
        $seq_id = $val['seq_id'];
        $item_name = $val['item_name'];
        
        $item_element .=<<<EOD
            <div class="radio">
                <label>
                    <input type="radio" name="item_sid" class="item_sid" value="$seq_id"><span>$item_name</span>
                </label>
            </div>
EOD;
    }
}

$json = compact('code', 'msg', 'item_element');
echo json_encode($json);

?>