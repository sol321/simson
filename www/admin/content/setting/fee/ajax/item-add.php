<?php
/*
 * Desc: 비용 항목 생성
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['item_year'])) {
    $code = 101;
    $msg = '연도를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['item_name'])) {
    $code = 102;
    $msg = '비용 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['item_type'])) {
    $code = 103;
    $msg = '비용 종류를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$year = $_POST['item_year'];
$type = $_POST['item_type'];
$period = empty($_POST['item_period']) ? 1 : $_POST['item_period'];

if (strcmp($type, 'ETC')) {
    $id = if_check_duplicate_item($year, $type, $period);
    
    if (!empty($id)) {
        $code = 201;
        $msg = '이미 동일한 비용 항목(연도/종류 동일)이 등록되어 있습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_add_item();

if (empty($result)) {
    $code = 201;
    $msg = '비용 항목을 생성할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>