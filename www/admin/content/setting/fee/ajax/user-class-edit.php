<?php
/*
 * Desc: 회원 등급 업데이트
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['seq_id'])) {
    $code = 100;
    $msg = '등급을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['class_name'])) {
    $code = 102;
    $msg = '회원 등급 이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];
$class_name = trim($_POST['class_name']);

$id = if_check_duplicate_user_class($class_name);

if (!empty($id)) {
    if ($id != $seq_id) {
        $code = 201;
        $msg = '이미 동일한 등급 이름이 등록되어 있습니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_update_user_class();

if (empty($result)) {
    $code = 201;
    $msg = '회원 등급 이름을 수정할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>