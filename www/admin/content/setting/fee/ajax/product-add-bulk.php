<?php
/*
 * Desc: 회비 대량 등록
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_product_bulk();

if (empty($result)) {
    $code = 201;
    $msg = '회비 항목을 생성할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>