<?php
/*
 * Desc: 비용목록, 회원등급 테이블의 내용을 조합하여 해당연도의 회비 목록을 한번에 등록하기 위함.
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

$year = empty($_GET['year']) ? date('Y') : $_GET['year'];

$item_lists = if_get_item_list($year);  // 비용 목록
$class_lists = if_get_user_class_list();    // 회원 등급
$item_years = if_get_item_all_year(); // 연도별

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회비 항목 관리</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-10">
							<!-- form start -->
							<form id="form-item-new" class="form-horizontal">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">
											회비 항목 한번에 등록
										</h3>
										<small>비용목록과 회원 등급 내용을 조합하여 자동으로 목록을 구성합니다.</small>
									</div>
									<div class="box-body">
									
										<div class="form-group">
											<div class="col-md-3">
												<select name="product_year" id="product_year" class="form-control">
													<option value="">- 연도 선택 -</option>
									<?php 
									if (!empty($item_years)) {
										foreach ($item_years as $key => $val) {
											$item_year = $val['item_year'];
											$selected = $item_year == $year ? 'selected' : '';
									?>
													<option value="<?php echo $item_year ?>" <?php echo $selected ?>><?php echo $item_year ?></option>
									<?php 
										}
									}
									?>
												</select>
											</div>
											<div class="col-md-2"></div>
											<div class="col-md-7">
												<small>
													금액을 입력하지 않으면 등록되지 않습니다.
												</small>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-2">회원 등급</div>
											<div class="col-md-3">비용 이름</div>
											<div class="col-md-4">회비 이름</div>
											<div class="col-md-3">금액</div>
										</div>
							<?php
							if (!empty($class_lists)) {
							    foreach ($class_lists as $k => $v) {
							        $class_seq_id = $v['seq_id'];
							        $class_name = $v['class_name'];
							        
        							if (!empty($item_lists)) {
        								foreach ($item_lists as $key => $val) {
        								    $item_seq_id = $val['seq_id'];
        								    $item_name = $val['item_name'];
        								    $item_year = $val['item_year'];
        								    $item_type = $val['item_type'];
        								    $item_period = $val['item_period'];
        								    $item_period_label = empty($item_period) ? '' : '(' . $item_period . '년)';
        								    
        								    $product_name = $class_name . ' ' . $item_name;
							?>
										<input type="hidden" name="item_id[]" value="<?php echo $item_seq_id ?>">
										<input type="hidden" name="class_id[]" value="<?php echo $class_seq_id ?>">
										
										<div class="form-group">
											<div class="col-md-2">
												<label><?php echo $class_name ?></label>
											</div>
											<div class="col-md-3">
												<?php echo $item_name ?>
											</div>
											<div class="col-md-4 has-warning">
												<input type="text" name="product_name[]" class="form-control" placeholder="연회비 항목 이름" value="<?php echo $product_name ?>">
											</div>
											<div class="col-md-3">
												<div class="input-group has-warning">
													<input type="text" name="product_price[]" class="form-control numeric" placeholder="금액">
													<span class="input-group-addon">원</span>
												</div>
											</div>
										</div>
							<?php 
								        }
								    }
								}
							}
							?>
									</div>
									<!-- /.box-body -->
									<div class="box-footer text-center">
										<a href="item_list.php" class="btn btn-default">리스트</a> &nbsp; &nbsp;
										<button type="submit" id="btn-submit" class="btn btn-info">등록</button>
									</div>
									<!-- /.box-footer -->
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
		$(function() {
// 			$("input").prop("required", true);	// All fields required
			$(".numeric").numeric({ negative: false });

			$("#form-item-new").validate({
				rules: {
				},
				messages: {
				},
				submitHandler: function(form) {
					$.ajax({
						type : "POST",
						url : "./ajax/product-add-bulk.php",
						data : $("#form-item-new").serialize(),
						dataType : "json",
						beforeSend : function() {
							$("#btn-submit").prop("disabled", true);
						},
						success : function(res) {
							if (res.code == "0") {
								location.href = "product_list.php";
							} else {
								alert(res.msg);
							}
						}
					}).done(function() {
					}).fail(function() {
					}).always(function() {
						$("#btn-submit").prop("disabled", false);
					}); // ajax
				}
			});

			// 연도 선택
			$("#product_year").change(function() {
				var year = $(this).val();
				if (year != "") {
					location.href = "?year=" + year;
				}
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>