<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('항목을 선택해 주십시오.');
}

$product_id = $_GET['id'];

$row = if_get_product($product_id);

$class_id = $row['user_class_id'];
$product_year = $row['product_year'];
$product_period = $row['product_period'];
$product_name = $row['product_name'];
$product_type = $row['product_type'];
$product_price = $row['product_price'];
$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$prod_item_id = $jdec['item_sid'];

$class_lists = if_get_user_class_list();

$item_lists = if_get_item_list($product_year);

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회비 항목 편집</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
                				<input type="hidden" name="product_id" value="<?php echo $product_id ?>">
                				<input type="hidden" name="product_period" value="<?php echo $product_period ?>">
    							<div class="box box-info">
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">연도</label>
                							<div class="col-md-8">
                								<input type="number" name="product_year" class="form-control numeric" id="product_year" value="<?php echo $product_year ?>" readonly>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">회원 등급</label>
                							<div class="col-md-8">
            						<?php 
            						if (!empty($class_lists)) {
            						    foreach ($class_lists as $k => $v) {
            						        $class_seq_id = $v['seq_id'];
            						        $class_name = $v['class_name'];
            						        $checked = $class_id != $class_seq_id ? '' : 'checked';
    								?>
    											<div class="radio">
													<label>
														<input type="radio" name="class_id" class="class_id" value="<?php echo $class_seq_id ?>" <?php echo $checked ?>>
														<span><?php echo $class_name ?></span>
													</label>
												</div>
    								<?php
            						    }
    								}
    								?>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">비용 목록</label>
                							<div class="col-md-8">
                							
            					<?php 
            					if (!empty($item_lists)) {
            					    foreach ($item_lists as $key => $val) {
            					        $seq_id = $val['seq_id'];
            					        $item_name = $val['item_name'];
            					        $checked = $prod_item_id != $seq_id ? '' : 'checked';
            					?>
            									<div class="radio">
													<label>
														<input type="radio" name="item_id" class="item_id" value="<?php echo $seq_id ?>" <?php echo $checked ?>>
														<span><?php echo $item_name ?></span>
													</label>
												</div>
            					<?php 
            					    }
            					}
            					?>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">회비 이름</label>
                							<div class="col-md-8">
                								<input type="text" name="product_name" class="form-control" id="product_name" placeholder="연회비" value="<?php echo $product_name ?>">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">금액</label>
                							<div class="col-md-8">
                								<input type="text" name="product_price" class="form-control numeric" id="product_price" value="<?php echo $product_price ?>">
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<a href="product_list.php" class="btn btn-default">리스트</a> &nbsp; &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
                					</div>
                					<!-- /.box-footer -->
        						</div>
        					</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
    		$("input, select").prop("required", true);	// All fields required
    		$(".numeric").numeric({ negative: false });

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/product-edit.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.href = "product_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>