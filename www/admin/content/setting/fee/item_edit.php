<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('항목을 선택해 주십시오.');
}

$seq_id = $_GET['id'];

$row = if_get_item($seq_id);

$item_year = $row['item_year'];
$item_name = $row['item_name'];
$item_type = $row['item_type'];
$item_period = $row['item_period'];
$item_comment = $row['item_comment'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>비용 항목 편집</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
                				<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
    							<div class="box box-info">
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">연도</label>
                							<div class="col-md-8">
                								<input type="number" name="item_year" class="form-control numeric" id="item_year" value="<?php echo $item_year ?>" maxlength="4"> 	
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">비용 이름</label>
                							<div class="col-md-8">
                								<input type="text" name="item_name" class="form-control" id="item_name" placeholder="연회비" value="<?php echo $item_name ?>">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">종류</label>
                							<div class="col-md-8">
                				<?php 
                				foreach ($if_fee_type as $key => $val) {
                				    $checked = strcmp($item_type, $key) ? '' : 'checked';
            					?>
            								<div class="radio">
                								<label>
                									<input type="radio" name="item_type" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
                								</label>
                							</div>
            					<?php 
            					}
            					?>
            								<label id="item_type-error" class="error" for="item_type"></label>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="item_comment" class="form-control"><?php echo $item_comment ?></textarea>
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<a href="item_list.php" class="btn btn-default">리스트</a> &nbsp; &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
                					</div>
                					<!-- /.box-footer -->
        						</div>
        					</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
    		$("input").prop("required", true);	// All fields required
    		$(".numeric").numeric({ negative: false });

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/item-edit.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.href = "item_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>