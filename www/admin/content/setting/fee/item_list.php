<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$year = empty($_GET['year']) ? '' : $_GET['year'];
$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 50;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 연도
if (!empty($year)) {
    $sql .= " AND item_year = ?";
    array_push($sparam, $year);
}

// 키워드 검색
if (!empty($qs1)) {
    $sql .= " AND item_year LIKE ? OR item_name LIKE ? OR item_comment LIKE ?";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_items'] . "
		WHERE
			1
			$sql
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();

$item_years = if_get_item_all_year(); // 연도별

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>비용 항목 관리</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-8">
        					<div class="box box-info">
        						<div class="box-header">
        							<form id="search-form">
                						<div class="box-tools">
                							<div class="pull-left">
                    							<select name="year" id="year" class="form-control">
													<option value="">- 선 택 -</option>
									<?php 
									if (!empty($item_years)) {
										foreach ($item_years as $key => $val) {
											$item_year = $val['item_year'];
											$selected = $item_year == $year ? 'selected' : '';
									?>
													<option value="<?php echo $item_year ?>" <?php echo $selected ?>><?php echo $item_year ?></option>
									<?php 
										}
									}
									?>
												</select>
                    						</div>
                						</div>
                					</form>
                					
        						</div>
        						<!-- /.box-header -->
            					<div class="box-body table-responsive no-padding">
        							<table class="table table-hover">
        								<tbody>
        									<tr class="active">
            									<th style="width: 10px">#</th>
                								<th>연도</th>
                								<th>비용 이름</th>
                								<th>종류</th>
                								<th>메모</th>
                								<th>-</th>
            								</tr>
            					<?php 
                				if (!empty($item_results)) {
                				    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
                				    
                					foreach ($item_results as $key => $val) {
                						$seq_id = $val['seq_id'];
                						$item_year = $val['item_year'];
                						$item_name = $val['item_name'];
                						$item_type = $val['item_type'];
                						$item_period = $val['item_period'];
                						$item_comment = $val['item_comment'];
                				?>
            								<tr id="item-<?php echo $seq_id ?>">
            									<td><?php echo $list_no ?></td>
            									<td><?php echo $item_year ?></td>
            									<td><?php echo $item_name ?></td>
            									<td><?php echo $if_fee_type[$item_type] ?></td>
            									<td><?php echo $item_comment ?></td>
            									<td>
                									<a href="item_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">수정</a>
                									<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
                								</td>
            								</tr>
            					<?php 
            					       $list_no--;
                					}
                				}
            					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
            					<div class="box-footer text-center">
                		<?php
                		echo $paginator->if_paginator();
                		?>
                				</div>
            					<!-- /.box-footer -->
        					</div>
        				</div>
        				<div class="col-md-4">
							<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
    							<div class="box box-primary">
    								<div class="box-header with-border">
                    					<h3 class="box-title">비용 항목 생성</h3>
                    				</div>
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">연도</label>
                							<div class="col-md-8">
                								<input type="text" name="item_year" class="form-control numeric" id="item_year" placeholder="<?php echo date('Y') ?>" maxlength="4">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">비용 이름</label>
                							<div class="col-md-8">
                								<input type="text" name="item_name" class="form-control" id="item_name" placeholder="연회비">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">종류</label>
                							<div class="col-md-8">
                				<?php 
                				foreach ($if_fee_type as $key => $val) {
            					?>
            								<div class="radio">
                								<label>
                									<input type="radio" name="item_type" value="<?php echo $key ?>"> <?php echo $val ?>
                								</label>
                							</div>
            					<?php 
            					}
            					?>
            								<label id="item_type-error" class="error" for="item_type"></label>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="item_comment" class="form-control"></textarea>
                							</div>
                						</div>
                						<div class="text-center">
            								<button type="submit" id="btn-submit" class="btn btn-primary">등록</button>
                    					</div>
                					</div>
                					<!-- /.box-body -->
        						</div>
        					</form>
						</div>
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		
		<script>
    	$(function() {
    		$("input").prop("required", true);	// All fields required
    		$(".numeric").numeric({ negative: false });

    		// 연도
			$("#item_year").datepicker({
				format: "yyyy",
				viewMode: "years", 
				minViewMode: "years",
				autoclose: true
			});

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/item-add.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.href = "item_list.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    		
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/item-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		// 연도 선택
			$("#year").change(function() {
				var year = $(this).val();
				location.href = "?year=" + year;
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>