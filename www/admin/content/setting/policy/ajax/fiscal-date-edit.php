<?php
/*
 * Desc: 연회비 부과 기준 날짜 설정
 *
 */
require_once '../../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['fiscal_type'])) {
    $code = 101;
    $msg = '연회비 부과 기준을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$fiscal_type = $_POST['fiscal_type'];
$fiscal_md = '';
$fiscal_comment = empty($_POST['fiscal_comment']) ? '' : $_POST['fiscal_comment'];
$update_dt = date('Y-m-d H:i:s');

if (!strcmp($fiscal_type, 'A')) {
    if (empty($_POST['fiscal_md'])) {
        $code = 102;
        $msg = '월과 일을 네자리 숫자로 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
    $fiscal_md = $_POST['fiscal_md'];
}

$compact = compact('fiscal_type', 'fiscal_md', 'fiscal_comment', 'update_dt');
$json = json_encode($compact);

$result = if_update_option_value('if_fiscal_data', $json);

if (empty($result)) {
    $code = 201;
    $msg = '저장할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>