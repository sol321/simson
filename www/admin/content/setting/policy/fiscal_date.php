<?php
/*
 * Desc: 특정 월일 혹은 회원 가입날짜를 연회비 부과 기준 날짜로 처리하기 위함.
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

if_authenticate_admin();

$fiscal_data = if_get_option('if_fiscal_data');

if (!empty($fiscal_data)) {
    $jdec = json_decode($fiscal_data, true);
    $fiscal_type = $jdec['fiscal_type'];
    $fiscal_md = $jdec['fiscal_md'];
    $fiscal_comment = $jdec['fiscal_comment'];
} else {
    $fiscal_type = '';
    $fiscal_md = '';
    $fiscal_comment = '';
}

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>연회비 부과 기준 날짜 설정</h1>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<!-- form start -->
                			<form id="form-item-new" class="form-horizontal">
    							<div class="box box-info">
    								<div class="box-header with-border">
                    					<h3 class="box-title">회원의 연회비를 부과하는 기준 날짜를 설정합니다.</h3>
                    				</div>
                					<div class="box-body">
                						<div class="form-group">
                							<label class="col-md-4 control-label">기준</label>
                							<div class="col-md-8">
                								<div class="radio">
                    								<label>
                    									<input type="radio" name="fiscal_type" value="A" <?php echo !strcmp($fiscal_type, 'A') ? 'checked' : ''; ?>> 월일
                    								</label>
                    								<input type="number" name="fiscal_md" class="form-control numeric <?php echo !strcmp($fiscal_type, 'A') ? '' : 'hide'; ?>" id="fiscal_md" placeholder="0101 (네자리 숫자)" maxlength="4" value="<?php echo $fiscal_md ?>">
                    							</div>
                								<div class="radio">
                    								<label>
                    									<input type="radio" name="fiscal_type" value="B" <?php echo !strcmp($fiscal_type, 'B') ? 'checked' : ''; ?>> 회원 가입(등록)날짜
                    								</label>
                    							</div>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-4 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="fiscal_comment" class="form-control"><?php echo $fiscal_comment ?></textarea>
                							</div>
                						</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<button type="submit" id="btn-submit" class="btn btn-info">저장</button>
                					</div>
                					<!-- /.box-footer -->
        						</div>
        					</form>
						</div>
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		
		<script>
    	$(function() {
//     		$("input").prop("required", true);	// All fields required
    		$(".numeric").numeric({ negative: false });

    		$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/fiscal-date-edit.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						alert("저장했습니다.");
        						location.reload();
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});

    		// 연회비 부과 기준
    		$('input[name="fiscal_type"]').click(function() {
        		var val = $(this).val();
        		if (val == "A") {
        			$("#fiscal_md").removeClass("hide");
        		} else {
        			$("#fiscal_md").addClass("hide");
        		}
        	});
        	
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>