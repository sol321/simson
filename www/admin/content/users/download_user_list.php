<?php
/*
 * Desc : 회원 리스트 다운로드
 *      1. 전체 다운로드
 *      2. 검색결과 다운로드 (검색 조건에 따른 sql)
 *      
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once INC_PATH . '/classes/PHPExcel.php';

if_authenticate_admin();

$dt = empty($_GET['dt']) ? '' : $_GET['dt'];

$filename = '회원 리스트.xlsx';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Inforang")
->setLastModifiedBy("Softsyw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Abstract");

$active_sheet = $objPHPExcel->setActiveSheetIndex(0);

$column_array = array(
    '아이디', '성명', '회원 등급', '회원 상태', '이메일', '휴대전화', '자택 주소',
    '자격 구분', '소속 분류', '기타', '근무처', '직위', '근무처 전화번호', '근무처 주소',
    '성별', '생년월일', '이메일 수신', '문자메시지 수신',
    '가입날짜', '최종 로그인 날짜'
);

$active_sheet->fromArray($column_array);

$sql = '';

// 회원 구분
$uclasses = if_get_all_user_class();
$if_user_class = if_get_user_class_array($uclasses);

/*
 * Desc: 검색 조건에 따른 sql 만들기
 */
if (!empty($dt)) {
    $query_str = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
    parse_str($query_str, $output);
    
    foreach ($output as $key => $val) {
        ${"$key"} = $val;
    }
    
    // 키워드 검색
    if (!empty($qa1) && !empty($qs1)) {
        $sql .= " AND $qa1 LIKE '%$qs1%'";
    }
    
    // 날짜별 검색
    if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
        $period_from_attach = $period_from . ' 00:00:00';
        $period_to_attach = $period_to . ' 23:59:59';
        $sql .= " AND $qa2 BETWEEN '$period_from_attach' AND '$period_to_attach'";
    }
    
    // 회원 등급 조회
    if (!empty($qa3)) {
        $sql .= " AND u.user_class = '$qa3'";
    }
    
    // 학회 조회
    if (!empty($qa4)) {
        $sql .= " AND p.item_type_secondary = '$qa4'";
    }
    
    // 회원 상태 조회
    if (!empty($qa5)) {
        $sql .= " AND u.user_state = '$qa5'";
    }
}

$query = "
        SELECT
            u.user_login,
            u.name_ko,
            u.user_class,
            u.user_state,
            u.user_email,
            u.user_mobile,
            CONCAT(JSON_UNQUOTE(u.meta_data->'$.home_address1'), ' ', JSON_UNQUOTE(u.meta_data->'$.home_address2')) AS home_address,
            JSON_UNQUOTE(u.meta_data->'$.user_qualification') AS user_qualification,
            JSON_UNQUOTE(u.meta_data->'$.org_category') AS org_category,
            CONCAT(JSON_UNQUOTE(u.meta_data->'$.license_no'), ' ', JSON_UNQUOTE(u.meta_data->'$.user_qualification_etc')) AS category_comment,
            u.org_name,
            JSON_UNQUOTE(u.meta_data->'$.org_position') AS org_position,
            JSON_UNQUOTE(u.meta_data->'$.org_phone') AS org_phone,
            CONCAT(JSON_UNQUOTE(u.meta_data->'$.org_address1'), ' ', JSON_UNQUOTE(u.meta_data->'$.org_address2')) AS org_address,
            JSON_UNQUOTE(u.meta_data->'$.gender') AS gender,            
            u.user_birth,
            JSON_UNQUOTE(u.meta_data->'$.email_agree') AS email_agree,
            JSON_UNQUOTE(u.meta_data->'$.sms_agree') AS sms_agree,
            u.create_dt,
            JSON_UNQUOTE(u.meta_data->'$.last_login_dt') AS last_login_dt
        FROM
            " . $GLOBALS['if_tbl_users'] . " AS u
        LEFT JOIN
            " . $GLOBALS['if_tbl_user_payment'] . " AS p
        ON
            u.seq_id = p.user_id AND
            p.pay_state = '9000' AND
            p.item_type <> 'ENT'
        WHERE
            u.show_hide = 'show'
            $sql
        GROUP BY 
            u.seq_id
        ORDER BY
			u.seq_id DESC
";
$stmt = $ifdb->prepare($query);
$stmt->execute();
$item_results = $ifdb->get_results($stmt);

foreach ($item_results as $key => $val) {
    $item_results[$key]['user_class'] = @$if_user_class[$val['user_class']];
    $item_results[$key]['user_state'] = @$if_user_state[$val['user_state']];
    $item_results[$key]['user_qualification'] = @$if_user_qualification[$val['user_qualification']];
    $item_results[$key]['org_category'] = @$if_org_category[$val['org_category']];
}

$active_sheet->fromArray($item_results, null, "A2");

$active_sheet->getDefaultColumnDimension()->setWidth(20);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('회원 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '. gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>