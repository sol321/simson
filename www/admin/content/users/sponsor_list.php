<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qa3 = empty($_GET['qa3']) ? '' : trim($_GET['qa3']);

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// 날짜별 검색
if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
    $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

// 후원 방식 조회
if (!empty($qa3)) {
    $sql .= " AND sponsorship_type = '$qa3'";
}

// mobileal placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

// 확회 검색을 위해 if_user_payment 테이블을 조인
$query = "
        SELECT
            *
        FROM
            " . $GLOBALS['if_tbl_sponsors'] . "
        WHERE
            1
            $sql
        ORDER BY
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
// $total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						후원 관리 
						<a href="sponsor_add.php" class="btn btn-info btn-sm">
							후원자 등록
						</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">

								<div class="row">
									<div class="col-md-2">
										<select name="qa3" class="form-control">
    										<option value="">- 후원 방식 -</option>
    							<?php
    							foreach ($if_sponsorship_type as $key => $val) {
    								$selected = $qa3 == $key ? 'selected' : '';
    							?>
    										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
    							<?php
    							}
    							?>
    									</select>
    								</div>
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="name" <?php echo strcmp($qa1, 'name') ? '' : 'selected'; ?>>성명</option>
											<option value="org_name" <?php echo strcmp($qa1, 'org_name') ? '' : 'selected'; ?>>근무처</option>
											<option value="email" <?php echo strcmp($qa1, 'email') ? '' : 'selected'; ?>>이메일</option>
											<option value="mobile" <?php echo strcmp($qa1, 'mobile') ? '' : 'selected'; ?>>휴대전화</option>
										</select>
									</div>
									<div class="col-md-2">
    									<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    								</div>
    								<div class="col-md-6">
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
								</div>
								
								<div class="row add-top">
    								<div class="col-md-2">
    									<select name="qa2" class="form-control">
    										<option value="">- 기간 선택 -</option>
    										<option value="apply_dt" <?php echo !strcmp($qa2, 'apply_dt') ? 'selected' : ''; ?>>후원 신청 날짜</option>
    									</select>
    								</div>
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
    									</div>
    								</div>
    							</div>

							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
    						<div class="pull-right">
								<a href="download_sponsor_list.php?dt=1" class="btn btn-success btn-sm">검색결과 엑셀 파일 저장</a> &nbsp;
								<a href="download_sponsor_list.php" class="btn btn-success btn-sm">전체 엑셀 파일 저장</a>
							</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>성명</th>
										<th>근무처</th>
										<th>이메일</th>
										<th>휴대전화번호</th>
        								<th>후원 신청 날짜</th>
										<th>후원 금액</th>
        								<th>후원 방식</th>
        								<th>-</th>
    								</tr>
    				<?php
    				$curdate = date('Y-m-d');
    				
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$mobile = $val['mobile'];
							$name = $val['name'];
							$email = $val['email'];
							$org_name = $val['org_name'];
							$apply_dt = $val['apply_dt'];
							$sponsorship_type = $val['sponsorship_type'];
							$sponsorship_amount = $val['sponsorship_amount'];
						?>
									<tr id="id-<?php echo $seq_id ?>">
										<td><?php echo $list_no ?></td>
										<td><?php echo $name ?></td>
										<td><?php echo $org_name ?></td>
										<td><?php echo $email ?></td>
										<td><?php echo $mobile ?></td>
										<td><?php echo $apply_dt ?></td>
										<td><?php echo number_format($sponsorship_amount) ?></td>
										<td><?php echo $if_sponsorship_type[$sponsorship_type] ?></td>
										<td>
											<a href="sponsor_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">편집</a>
                							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까? 복구되지 않으니 주의해 주십시오.")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/sponsor-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>