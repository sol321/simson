<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once FUNC_PATH . '/functions-product.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qa3 = empty($_GET['qa3']) ? '' : trim($_GET['qa3']);
$qa4 = empty($_GET['qa4']) ? '' : trim($_GET['qa4']);
$qa5 = empty($_GET['qa5']) ? '' : trim($_GET['qa5']);

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// 날짜별 검색
if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
    $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

// 회원 등급 조회
if (!empty($qa3)) {
    $sql .= " AND u.user_class = '$qa3'";
}

// 회원 상태 조회
if (!empty($qa5)) {
    $sql .= " AND u.user_state = '$qa5'";
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

// 확회 검색을 위해 if_user_payment 테이블을 조인
$query = "
        SELECT
            u.*
        FROM
            " . $GLOBALS['if_tbl_users'] . " AS u
        LEFT JOIN
            " . $GLOBALS['if_tbl_user_payment'] . " AS p
        ON
            u.seq_id = p.user_id AND
            p.pay_state = '9000' AND
            p.item_type <> 'ENT'
        WHERE
            u.show_hide = 'show'
            $sql
        GROUP BY 
            u.seq_id
        ORDER BY
			u.seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();

// 회원 구분
$uclasses = if_get_all_user_class();
$if_user_class = if_get_user_class_array($uclasses);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회원 리스트</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">

								<div class="row">
									<div class="col-md-2">
										<select name="qa3" class="form-control">
    										<option value="">- 회원 등급 -</option>
    							<?php
    							foreach ($if_user_class as $key => $val) {
    								$selected = $qa3 == $key ? 'selected' : '';
    							?>
    										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
    							<?php
    							}
    							?>
    									</select>
    								</div>
									<div class="col-md-2">
										<select name="qa5" class="form-control">
    										<option value="">- 회원 상태 -</option>
    							<?php
    							foreach ($if_user_state as $key => $val) {
    								$selected = $qa5 == $key ? 'selected' : '';
    							?>
    										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
    							<?php
    							}
    							?>
    									</select>
    								</div>
									<div class="col-md-3">
    								</div>
    							</div>
    							
    							<div class="row add-top">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="u.user_login" <?php echo strcmp($qa1, 'u.user_login') ? '' : 'selected'; ?>>아이디</option>
											<option value="u.name_ko" <?php echo strcmp($qa1, 'u.name_ko') ? '' : 'selected'; ?>>성명</option>
											<option value="u.user_email" <?php echo strcmp($qa1, 'u.user_email') ? '' : 'selected'; ?>>이메일</option>
											<option value="u.user_mobile" <?php echo strcmp($qa1, 'u.user_mobile') ? '' : 'selected'; ?>>휴대전화</option>
										</select>
									</div>
									<div class="col-md-6">
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    									<div class="col-md-6">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        								</div>
    								</div>
								</div>
								
								<div class="row add-top">
    								<div class="col-md-2">
    									<select name="qa2" class="form-control">
    										<option value="">- 기간 선택 -</option>
    										<option value="u.create_dt" <?php echo !strcmp($qa2, 'u.create_dt') ? 'selected' : ''; ?>>가입 날짜</option>
    										<option value="u.update_dt" <?php echo !strcmp($qa2, 'u.update_dt') ? 'selected' : ''; ?>>최종 수정일</option>
    									</select>
    								</div>
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
    									</div>
    								</div>
    							</div>

							</form>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
    						<div class="pull-right">
								<a href="download_user_list.php?dt=1" class="btn btn-success btn-sm">검색결과 엑셀 파일 저장</a> &nbsp;
								<a href="download_user_list.php" class="btn btn-success btn-sm">전체 엑셀 파일 저장</a>
							</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>회원 구분/상태</th>
										<th>아이디</th>
										<th>성명</th>
										<!-- <th>회비 결제 </th> -->
										<th>직장전화</th>
										<th>휴대전화</th>
										<th>최근 로그인 날짜</th>
										<th>등록날짜</th>
        								<th>-</th>
    								</tr>
    				<?php
    				$curdate = date('Y-m-d');
    				
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$user_login = $val['user_login'];
							$user_class = $val['user_class'];
							$name_ko = $val['name_ko'];
// 							$user_email = $val['user_email'];
							$user_mobile = $val['user_mobile'];
							$user_state = $val['user_state'];
							$user_class = $val['user_class'];
							$create_dt = $val['create_dt'];
							$meta_data = $val['meta_data'];
							$jdec = json_decode($meta_data, true);
							$org_phone = $jdec['org_phone'];
							$last_login_dt = @$jdec['last_login_dt'];
							
							$user_pmt = if_get_user_payment_by_userid($seq_id);  // 회원의 가입 학회 정보를 조회
							
							$bg_class = $user_state == '41' ? 'bg-yellow' : '';

						?>
									<tr id="id-<?php echo $seq_id ?>" class="<?php echo $bg_class ?>">
										<td><?php echo $list_no ?></td>
										<td>
											<?php echo $if_user_class[$user_class] ?> /
											<?php echo $if_user_state[$user_state] ?>
										</td>
										<td><?php echo $user_login ?></td>
										<td><?php echo $name_ko ?></td>
										<!-- <td>
							<?php 
							/*
							if (!empty($user_pmt)) {
							    foreach ($user_pmt as $k => $v) {
							        $item_type = $v['item_type'];
							        $item_year = $v['item_year'];
							        $expiry = strcmp($item_type, 'ENT') ? $v['expiry_date'] : '-';
							        
							        $expiry_label = $expiry < $curdate ? '<span class="label label-default">' . $expiry .'</span>' : '<span class="label label-success">' . $expiry .'</span>';
							        
							        $fee_type = $if_fee_type[$item_type];
							?>
											<?php echo $item_year ?> <?php echo $fee_type ?> <?php echo $expiry_label ?> <br>
							<?php 
							    }
							}
							*/
							?>
										</td> -->
										<td><?php echo $org_phone ?></td>
										<td><?php echo $user_mobile ?></td>
										<td><?php echo $last_login_dt ?></td>
										<td><?php echo substr($create_dt, 0, 10) ?></td>
										<td>
											<a href="user_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">편집</a>
									<?php 
									if (empty($bg_class)) {        // 차단회원은 '차단' 버튼 필요없음. 
									?>
                							<a href="javascript:;" class="badge bg-yellow action-block">차단</a>
                					<?php 
									}
                					?>
                							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="10">
											<a href="user_hide_list.php" class="btn btn-info btn-xs">삭제된 회원</a>
										</td>
									</tr>
								</tfoot>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 차단
    		$(".action-block").click(function(e) {
    			e.preventDefault();

    			if (!confirm("차단하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/user-block.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    		
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/user-hide.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>