<?php
/*
 * Desc : 임원 리스트 다운로드
 *      1. 전체 다운로드
 *      2. 검색결과 다운로드 (검색 조건에 따른 sql)
 *
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/PHPExcel.php';

if_authenticate_admin();

$dt = empty($_GET['dt']) ? '' : $_GET['dt'];

$filename = '임원 리스트.xlsx';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Inforang")
->setLastModifiedBy("Softsyw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Abstract");

$active_sheet = $objPHPExcel->setActiveSheetIndex(0);

$column_array = array(
    '직위', '성명', '근무처', '이메일'
);

$active_sheet->fromArray($column_array);

$sql = '';

/*
 * Desc: 검색 조건에 따른 sql 만들기
 */
if (!empty($dt)) {
    $query_str = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
    parse_str($query_str, $output);
    
    foreach ($output as $key => $val) {
        ${"$key"} = $val;
    }
    
    // 키워드 검색
    if (!empty($qa1) && !empty($qs1)) {
        $sql .= " AND $qa1 LIKE '%$qs1%'";
    }
}

$query = "
        SELECT
            position,
            name,
            org_name,
            email
        FROM
            " . $GLOBALS['if_tbl_executives'] . "
        WHERE
            1
            $sql
        ORDER BY
			seq_id DESC
";
$stmt = $ifdb->prepare($query);
$stmt->execute();
$item_results = $ifdb->get_results($stmt);

$active_sheet->fromArray($item_results, null, "A2");

$active_sheet->getDefaultColumnDimension()->setWidth(20);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('임원 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '. gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>