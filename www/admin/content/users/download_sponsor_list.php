<?php
/*
 * Desc : 후원자 리스트 다운로드
 *      1. 전체 다운로드
 *      2. 검색결과 다운로드 (검색 조건에 따른 sql)
 *
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/PHPExcel.php';

if_authenticate_admin();

$dt = empty($_GET['dt']) ? '' : $_GET['dt'];

$filename = '후원자 리스트.xlsx';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Inforang")
->setLastModifiedBy("Softsyw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Abstract");

$active_sheet = $objPHPExcel->setActiveSheetIndex(0);

$column_array = array(
    '후원 신청 날짜', '성명', '생년월일', '소속기관명', '휴대전화번호', '이메일', '후원방식', '후원금액', 
    '구좌', '은행명', '계좌번호', '예금주', '납부 시작 연월', '용도', '용도-기타',
    '성명 (기부금)' ,'주민등록번호', '주소' 
);

$active_sheet->fromArray($column_array);

$sql = '';

/*
 * Desc: 검색 조건에 따른 sql 만들기
 */
if (!empty($dt)) {
    $query_str = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
    parse_str($query_str, $output);
    
    foreach ($output as $key => $val) {
        ${"$key"} = $val;
    }
    
    // 키워드 검색
    if (!empty($qa1) && !empty($qs1)) {
        $sql .= " AND $qa1 LIKE '%$qs1%'";
    }
    // 날짜별 검색
    if (!empty($qa2) && !empty($period_from) && !empty($period_to)) {
        $period_from_attach = $period_from . ' 00:00:00';
        $period_to_attach = $period_to . ' 23:59:59';
        $sql .= " AND $qa2 BETWEEN '$period_from_attach' AND '$period_to_attach'";
    }
    
    // 후원 방식 조회
    if (!empty($qa3)) {
        $sql .= " AND sponsorship_type = '$qa3'";
    }
}

$query = "
        SELECT
            apply_dt,
            name,
            birth_dt,
            org_name,
            mobile,
            email,
            sponsorship_type,
            sponsorship_amount,
            JSON_UNQUOTE(meta_data->'$.prd_amt_account') AS prd_amt_account,
            JSON_UNQUOTE(meta_data->'$.prd_bank_name') AS prd_bank_name,
            JSON_UNQUOTE(meta_data->'$.prd_account_no') AS prd_account_no,
            JSON_UNQUOTE(meta_data->'$.prd_account_holder') AS prd_account_holder,
            JSON_UNQUOTE(meta_data->'$.prd_start_ym') AS prd_start_ym,
            JSON_UNQUOTE(meta_data->'$.prd_usage') AS prd_usage,
            JSON_UNQUOTE(meta_data->'$.prd_etc_content') AS prd_etc_content,
            JSON_UNQUOTE(meta_data->'$.don_name') AS don_name,
            JSON_UNQUOTE(meta_data->'$.don_reg_no') AS don_reg_no,
            JSON_UNQUOTE(meta_data->'$.don_address') AS don_address
        FROM
            " . $GLOBALS['if_tbl_sponsors'] . "
        WHERE
            1
            $sql
        ORDER BY
			seq_id DESC
";
$stmt = $ifdb->prepare($query);
$stmt->execute();
$item_results = $ifdb->get_results($stmt);

foreach ($item_results as $key => $val) {
    $item_results[$key]['sponsorship_type'] = @$if_sponsorship_type[$val['sponsorship_type']];
    $item_results[$key]['prd_usage'] = @$if_sponsorship_usage[$val['prd_usage']];
}

$active_sheet->fromArray($item_results, null, "A2");

$active_sheet->getDefaultColumnDimension()->setWidth(20);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('후원자 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '. gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>