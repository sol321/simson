<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

// 확회 검색을 위해 if_user_payment 테이블을 조인
$query = "
        SELECT
            *
        FROM
            " . $GLOBALS['if_tbl_executives'] . "
        WHERE
            1
            $sql
        ORDER BY
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
// $total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						임원 리스트
						<a href="executives_add.php" class="btn btn-info btn-sm">
							임원 등록
						</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
    							<div class="row">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="name" <?php echo strcmp($qa1, 'name') ? '' : 'selected'; ?>>성명</option>
											<option value="position" <?php echo strcmp($qa1, 'position') ? '' : 'selected'; ?>>직위</option>
											<option value="email" <?php echo strcmp($qa1, 'email') ? '' : 'selected'; ?>>이메일</option>
											<option value="org_name" <?php echo strcmp($qa1, 'org_name') ? '' : 'selected'; ?>>근무처</option>
										</select>
									</div>
									<div class="col-md-6">
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    									<div class="col-md-6">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        								</div>
    								</div>
								</div>
							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
    						<div class="pull-right">
								<a href="download_executives_list.php?dt=1" class="btn btn-success btn-sm">검색결과 엑셀 파일 저장</a> &nbsp;
								<a href="download_executives_list.php" class="btn btn-success btn-sm">전체 엑셀 파일 저장</a>
							</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>직위</th>
										<th>성명</th>
										<th>근무처</th>
										<th>이메일</th>
        								<th>-</th>
    								</tr>
    				<?php
    				$curdate = date('Y-m-d');
    				
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$position = $val['position'];
							$name = $val['name'];
							$email = $val['email'];
							$org_name = $val['org_name'];
							
							$meta_data = $val['meta_data'];
							$jdec = json_decode($meta_data, true);
							$create_dt = $jdec['create_dt'];

						?>
									<tr id="id-<?php echo $seq_id ?>">
										<td><?php echo $list_no ?></td>
										<td><?php echo $position ?></td>
										<td><?php echo $name ?></td>
										<td><?php echo $org_name ?></td>
										<td><?php echo $email ?></td>
										<td>
											<a href="executives_edit.php?id=<?php echo $seq_id ?>" class="badge bg-aqua">편집</a>
                							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 차단
    		$(".action-block").click(function(e) {
    			e.preventDefault();

    			if (!confirm("차단하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/user-block.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    		
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/executives-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>