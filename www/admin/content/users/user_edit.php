<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-product.php';
require_once FUNC_PATH . '/functions-payment.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('회원을 선택해 주십시오.');
}

$user_id = $_GET['id'];

$user_row = if_get_user_by_id($user_id);

$user_login = $user_row['user_login'];
$name_ko = $user_row['name_ko'];
$user_email = $user_row['user_email'];
$user_mobile = $user_row['user_mobile'];
$user_birth = $user_row['user_birth'];
$org_name = $user_row['org_name'];
$show_hide = $user_row['show_hide'];
$user_state = $user_row['user_state'];
$user_class = $user_row['user_class'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);
$gender = $jdec['gender'];
$home_zipcode = $jdec['home_zipcode'];
$home_address1 = $jdec['home_address1'];
$home_address2 = $jdec['home_address2'];
$org_zipcode = $jdec['org_zipcode'];
$org_address1 = $jdec['org_address1'];
$org_address2 = $jdec['org_address2'];
$org_phone = $jdec['org_phone'];
$org_position = $jdec['org_position'];
$org_category = $jdec['org_category'];
$license_no = $jdec['license_no'];
$email_agree = empty($jdec['email_agree']) ? '' : $jdec['email_agree'];
$sms_agree = empty($jdec['sms_agree']) ? '' : $jdec['sms_agree'];
$user_qualification = $jdec['user_qualification'];
$user_qualification_etc = $jdec['user_qualification_etc'];
$admin_memo = $jdec['admin_memo'];

// 회원 구분
$uclasses = if_get_all_user_class();
$if_user_class = if_get_user_class_array($uclasses);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회원정보 수정</h1>
				</div>
				<div class="content">
					<div class="row">
                    	<!-- form start -->
                        <form id="form-item-new" class="form-horizontal">
                        	<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                        	
                        	<div class="col-md-6">
                        		<div class="box box-danger">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-red">기본 정보</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group text-danger">
                        					<label class="col-md-3 control-label">회원 등급</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        			<?php 
                        			foreach ($if_user_class as $key => $val) {
                        			    $checked = $key == $user_class ? 'checked' : '';
                        			?>
                        							<label>
                        								<input type="radio" name="user_class" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
                        							</label> &nbsp; &nbsp;
                        			<?php 
                        			}
                        			?>
                        						</div>
                        					</div>
                        				</div>
                        				<div class="form-group text-danger">
                        					<label class="col-md-3 control-label">회원  상태</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        			<?php 
                        			foreach ($if_user_state as $key => $val) {
                        			    $checked = $key == $user_state ? 'checked' : '';
                        			?>
                        							<label>
                        								<input type="radio" name="user_state" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
                        							</label> &nbsp; &nbsp;
                        			<?php 
                        			}
                        			?>
                        						</div>
                        					</div>
                        				</div>
                        			
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">아이디</label>
                        					<div class="col-md-8">
                        						<?php echo $user_login ?>
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성명</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="name_ko" id="name_ko" value="<?php echo $name_ko ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">비밀번호</label>
                        					<div class="col-md-8">
                        						<input type="password" class="form-control" name="user_pass" id="user_pass" value="">
                        						<div class="help-block"><small>변경시에만 입력해 주십시오.</small></div>
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">이메일</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="user_email" id="user_email" value="<?php echo $user_email ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">휴대전화</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="user_mobile" id="user_mobile" value="<?php echo $user_mobile ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">자택주소</label>
                        					<div class="col-md-3">
                        						<input type="text" class="form-control" name="home_zipcode" id="home_zipcode" value="<?php echo $home_zipcode ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-8">
                        						<input type="text" class="form-control" name="home_address1" id="home_address1" value="<?php echo $home_address1 ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-8">
                        						<input type="text" class="form-control" name="home_address2" id="home_address2" placeholder="상세주소" value="<?php echo $home_address2 ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">관리자 메모</label>
                        					<div class="col-md-8">
                        						<textarea name="admin_memo" id="admin_memo" class="form-control" rows="5"><?php echo $admin_memo ?></textarea>
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        		<div class="text-center">
    								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
    								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
                          		</div>
                        	</div>
                        	
                        	<div class="col-md-6">
                        		<div class="box box-info">
                        			<div class="box-header with-border">
                        				<h3 class="box-title text-info">상세 정보</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">자격 구분</label>
                        					<div class="col-md-8">
                        						<div class="radio">
        							<?php 
        							foreach($if_user_qualification as $key => $val) {
        							    $checked = $user_qualification == $key ? 'checked' : '';
    								?>
    												<label><input type="radio" name="user_qualification" class="user_qualification" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php
    								}
    								?>
    											</div>
    											<input type="text" name="user_qualification_etc" id="user_qualification_etc" class="form-control hide" placeholder="기타 선택 시 입력해 주세요" value="<?php echo $user_qualification_etc ?>">
    											<input type="text" name="license_no" id="license_no" class="form-control hide" placeholder="의사 면허번호" value="<?php echo $license_no ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">소속 분류</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        			<?php 
    								foreach( $if_org_category as $key => $val) {
    								    $checked = $key == $org_category ? 'checked' : '';
    								?>
    												<label><input type="radio" name="org_category" class="org_category" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php
    								}
    								?>
    											 </div>
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">근무처</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">직위</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="org_position" id="org_position" value="<?php echo $org_position ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">근무처 전화번호</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="org_phone" id="org_phone" value="<?php echo $org_phone ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">근무처 주소</label>
                        					<div class="col-md-3">
                        						<input type="text" class="form-control" name="org_zipcode" id="org_zipcode" value="<?php echo $org_zipcode ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-8">
                        						<input type="text" class="form-control" name="org_address1" id="org_address1" value="<?php echo $org_address1 ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-8">
                        						<input type="text" class="form-control" name="org_address2" id="org_address2" placeholder="상세주소" value="<?php echo $org_address2 ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성별</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        							<label><input type="radio" name="gender" class="gender" value="M" <?php echo strcmp($gender, 'M') ? '' : 'checked'; ?>> 남</label> &nbsp; &nbsp;
                        							<label><input type="radio" name="gender" class="gender" value="F" <?php echo strcmp($gender, 'F') ? '' : 'checked'; ?>> 여</label>
                        						</div>
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">생년월일</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="user_birth" id="user_birth" value="<?php echo $user_birth ?>">
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        		
                        		<div class="box box-info">
                        			<div class="box-header with-border">
                        				<h3 class="box-title text-info">부가 정보</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">이메일 수신</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        			<?php 
    								foreach ($if_agree_yn as $key => $val) {
    								    $checked = strcmp($key, $email_agree) ? '' : 'checked';
    								?>
    												<label><input type="radio" name="email_agree" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php 
    								}
    								?>
    											</div>
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">문자메시지 수신</label>
                        					<div class="col-md-8">
                        						<div class="radio">
                        			<?php 
    								foreach ($if_agree_yn as $key => $val) {
    								    $checked = strcmp($key, $sms_agree) ? '' : 'checked';
    								?>
    												<label><input type="radio" name="sms_agree" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?></label> &nbsp; &nbsp;
    								<?php 
    								}
    								?>
    											</div>
                        					</div>
                        				</div>
                        			</div>
                        		</div>
                        	</div>
                      	</form>
					</div>
					<!-- /.row -->
					
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		
		<script>
		$(function () {
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/user-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "user_list.php";
							} else {
								alert("정보를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});
		});

		// 우편번호 찾기
		$("#home_zipcode, #home_address1").click(function() {
			execDaumPostcode('home');
		});
		// 우편번호 찾기
		$("#org_zipcode, #org_address1").click(function() {
			execDaumPostcode('dept');
		});

		// Daum 우편번호
		function execDaumPostcode(spot) {
    		new daum.Postcode({
    			oncomplete: function(data) {
    				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
    
    				// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
    				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
    				var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
    				var extraRoadAddr = ''; // 도로명 조합형 주소 변수
    
    				// 법정동명이 있을 경우 추가한다. (법정리는 제외)
    				// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
    				if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
    					extraRoadAddr += data.bname;
    				}
    				// 건물명이 있고, 공동주택일 경우 추가한다.
    				if(data.buildingName !== '' && data.apartment === 'Y'){
    				   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
    				}
    				// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
    				if(extraRoadAddr !== ''){
    					extraRoadAddr = ' (' + extraRoadAddr + ')';
    				}
    				// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
    				if(fullRoadAddr !== ''){
    					fullRoadAddr += extraRoadAddr;
    				}
    
    				// 우편번호와 주소 정보를 해당 필드에 넣는다.
    				if (spot == "home") {
        				document.getElementById('home_zipcode').value = data.zonecode; //5자리 새우편번호 사용
        				document.getElementById('home_address1').value = fullRoadAddr + " ";		// 도로명 주소
        				document.getElementById('home_address2').focus();
        			} else if (spot == "dept") {
        				document.getElementById('org_zipcode').value = data.zonecode; //5자리 새우편번호 사용
        				document.getElementById('org_address1').value = fullRoadAddr + " ";		// 도로명 주소
        				document.getElementById('org_address2').focus();
        			}
    			}
    		}).open();
    	}
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>