<?php
/*
 * Desc: 임원 완전 삭제 (delete)
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '임원을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['id'];

$user_row = if_get_executives_by_id($seq_id);

// 사진 삭제
$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);
$file_attachment = $jdec['file_attachment'];
@unlink($file_attachment[0]['file_path']);
@unlink($file_attachment[0]['thumb_path']);

$result = if_delete_executives($seq_id);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>