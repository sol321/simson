<?php
/*
 * Desc: 회원 정보 수정
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_id'])) {
    $code = 101;
    $msg = '회원을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['name_ko'])) {
    $code = 103;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_email'])) {
    $code = 104;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_mobile'])) {
    $code = 105;
    $msg = '휴대전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_class'])) {
    $code = 106;
    $msg = '회원 등급을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['user_state'])) {
    $code = 107;
    $msg = '회원 상태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_email = empty($_POST['user_email']) ? '' : trim($_POST['user_email']);

if (!empty($user_email)) {
    if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$user_id = $_POST['user_id'];

// 변경 전 회원정보 저장 : json
$user_row = if_get_user_by_id($user_id);
$meta_key = 'update_' . time();
$meta_value = json_encode($user_row);
if_update_user_meta($user_id, $meta_key, $meta_value);

$result = if_update_user_by_cms($user_id);

if (empty($result)) {
    $code = 201;
    $msg = '정보 수정을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>