<?php
/*
 * Desc: 회원 완전 삭제 (delete)
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '회원을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$user_id = $_POST['id'];

if_delete_user_session($user_id);   // 회원의 세션 삭제(강제 로그아웃)

$result = if_delete_user($user_id);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>