<?php
/*
 * Desc: 임원 등록
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once INC_PATH . '/classes/Thumbnail.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['position'])) {
    $code = 101;
    $msg = '직위를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['name'])) {
    $code = 103;
    $msg = '이름을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['org_name'])) {
    $code = 105;
    $msg = '근무처를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['email'])) {
    $code = 104;
    $msg = '이메일을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['file_path'])) {
    $code = 107;
    $msg = '사진을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$email = empty($_POST['email']) ? '' : trim($_POST['email']);

if (!empty($email)) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_add_executives();

if (empty($result)) {
    $code = 201;
    $msg = '임원 등록을 할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>