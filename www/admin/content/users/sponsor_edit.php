<?php
/*
 * Desc: 후원자 편집
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('임원을 선택해 주십시오.');
}

$seq_id = $_GET['id'];

$user_row = if_get_sponsor_by_id($seq_id);

$apply_dt    = $user_row['apply_dt'];
$name        = $user_row['name'];
$birth_dt    = $user_row['birth_dt'];
$org_name    = $user_row['org_name'];
$mobile      = $user_row['mobile'];
$email       = $user_row['email'];
$sponsorship_type  = $user_row['sponsorship_type'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);

$man_input = $jdec['man_input'];
$temp_amount = $jdec['temp_amount'];
$prd_amount = $jdec['prd_amount'];
$prd_amt_account = $jdec['prd_amt_account'];
$prd_usage = $jdec['prd_usage'];
$prd_start_ym = $jdec['prd_start_ym'];
$prd_bank_name = $jdec['prd_bank_name'];
$prd_account_no = $jdec['prd_account_no'];
$prd_etc_content = $jdec['prd_etc_content'];
$prd_account_holder = $jdec['prd_account_holder'];

$don_name = $jdec['don_name'];
$don_reg_no = $jdec['don_reg_no'];
$don_address = $jdec['don_address'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content">
					<div class="row">
						<!-- form start -->
                        <form id="form-item-new" class="form-horizontal">
                        	<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
                    		<div class="col-md-6">
                        		<div class="box box-danger">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-red">후원자 수정</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">후원 신청 날짜</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="apply_dt" id="apply_dt" value="<?php echo $apply_dt ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성명</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="name" id="name" value="<?php echo $name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">생년월일</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="birth_dt" id="birth_dt" value="<?php echo $birth_dt ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">소속기관명</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">휴대전화번호</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="mobile" id="mobile" placeholder="010-xxxx-xxxx" value="<?php echo $mobile ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">이메일</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="email" id="email" value="<?php echo $email ?>">
                        					</div>
                        				</div>
                        				
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">후원 방식</label>
                        					<div class="col-md-8">
                        			<?php 
                        			foreach ($if_sponsorship_type as $key => $val) {
                        			    $checked = strcmp($sponsorship_type, $key) ? '' : 'checked';
                        			?>
                        						<div class="radio">
                        							<label>
                        								<input type="radio" name="sponsorship_type" class="sps-type" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
                        							</label>
                        						</div>
                        			<?php 
                        			}
                        			?>
                        					</div>
                        				</div>
                        				
                        				<div class="form-group" id="wrap_temp">
                        					<label class="col-md-3 control-label">후원 금액</label>
                        					<div class="col-md-5">
                        						<div class="input-group">
            										<input type="text" class="form-control numeric" name="temp_amount" id="temp_amount" value="<?php echo $temp_amount ?>">
            										<span class="input-group-addon">원</span>
            									</div>
                        					</div>
                        				</div>
                        				
                        				<div class="form-group" id="wrap_period">
                        					<label class="col-md-1 control-label"></label>
                        					<div class="col-md-10">
                        						<table class="table">
                        							<tr>
                        								<th class="text-green text-right">후원 금액</th>
                        								<td>
                        									<div class="input-group">
                        										<input type="text" class="form-control numeric" name="prd_amt_account" id="prd_amt_account" value="<?php echo $prd_amt_account ?>">
                        										<span class="input-group-addon">구좌</span>
                        									</div>
                        									
                        									<div class="checkbox pull-left">
                    											<label>
                    												<input type="checkbox" name="prd_manual_input" id="prd_manual_input" value="1" <?php echo $man_input == '1' ? 'checked' : ''; ?>> 직접 입력
                    											</label>
                    										</div>
                    										<div class="text-right help-block">(1구좌 = 10,000원)</div>
                        									
                        									<div class="input-group" id="wrap-prd-amount2">
                        										<input type="text" class="form-control numeric" name="prd_amount" id="prd_amount" value="<?php echo $prd_amount ?>">
                        										<span class="input-group-addon">원</span>
                        									</div>
                        								</td>
                        							</tr>
                        							<tr>
                        								<th class="text-green text-right">자동이체계좌</th>
                        								<td>
                                    						<div class="input-group">
                        										<span class="input-group-addon">은행</span>
                        										<input type="text" class="form-control" name="prd_bank_name" id="prd_bank_name" value="<?php echo $prd_bank_name ?>">
                        									</div>
                                    						<div class="input-group">
                        										<span class="input-group-addon">계좌번호</span>
                        										<input type="text" class="form-control" name="prd_account_no" id="prd_account_no" value="<?php echo $prd_account_no ?>">
                        									</div>
                                    						<div class="input-group">
                        										<span class="input-group-addon">예금주</span>
                        										<input type="text" class="form-control" name="prd_account_holder" id="prd_account_holder" value="<?php echo $prd_account_holder ?>">
                        									</div>
                        								</td>
                        							</tr>
                        							<tr>
                        								<th class="text-green text-right">납부 시작 연월</th>
                        								<td>
                        									<div class="input-group">
                        										<input type="text" class="form-control" name="prd_start_ym" id="prd_start_ym" value="<?php echo $prd_start_ym ?>">
                        									</div>
                        								</td>
                        							</tr>
                        							<tr>
                        								<th class="text-green text-right">용도</th>
                        								<td>
                        									<div class="form-group">
                                            					<div class="col-md-8">
                                            			<?php 
                                            			foreach ($if_sponsorship_usage as $key => $val) {
                                            			    $checked = strcmp($prd_usage, $key) ? '' : 'checked';
                                            			?>
                                            						<div class="radio">
                                            							<label>
                                            								<input type="radio" name="prd_usage" value="<?php echo $key ?>" <?php echo $checked ?>> <?php echo $val ?>
                                            							</label>
                                            						</div>
                                            			<?php 
                                            			}
                                            			?>
                                            						<input type="text" name="prd_etc_content" id="prd_etc_content" class="form-control" value="<?php echo $prd_etc_content ?>">
                                            					</div>
                                            				</div>
                        								</td>
                        							</tr>
                        						</table>
                        					</div>
                        				</div>
                        			</div>
                        			<div class="box-footer text-center">
        								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
                              		</div>
                        		</div>
                        	</div>
    						<div class="col-md-6">
    							<div class="box box-warning">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-yellow">기부금 영수증 정보</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성명</label>
                        					<div class="col-md-4">
                        						<input type="text" class="form-control" name="don_name" id="don_name" value="<?php echo $don_name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">주민등록번호</label>
                        					<div class="col-md-4">
                        						<input type="text" class="form-control" name="don_reg_no" id="don_reg_no" data-inputmask='"mask": "999999-9999999"' data-mask value="<?php echo $don_reg_no ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">주 소</label>
                        					<div class="col-md-9">
                        						<input type="text" class="form-control" name="don_address" id="don_address" value="<?php echo $don_address ?>">
                        					</div>
                        				</div>
                        			</div>
                        		</div>
    						</div>
    					</form>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

		<!-- Inputmask -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script>
		$(function () {
			$('#birth_dt').inputmask('yyyy-mm-dd');
			$('[data-mask]').inputmask();
			$(".numeric").numeric({ negative: false });

			// 초기화
			if ($(".sps-type:checked").val() == "period") {
				$("#wrap_temp").addClass("hide");
			} else {
				$("#wrap_period").addClass("hide");
			}

			if (!$("#prd_manual_input").prop("checked")) {
				$("#wrap-prd-amount2").addClass("hide");
			} 

			// 후원 방식
			$(".sps-type").click(function() {
				var val = $(this).val();
				if (val == "period") {
					$("#wrap_period").removeClass("hide");
					$("#wrap_temp").addClass("hide");
				} else {
					$("#wrap_period").addClass("hide");
					$("#wrap_temp").removeClass("hide");
				}
			});

			// 직접 입력
			$("#prd_manual_input").click(function() {
				var checked = $(this).prop("checked");

				if (checked) {
					$("#wrap-prd-amount2").removeClass("hide");
					$("#prd_amt_account").val("");
				} else {
					$("#wrap-prd-amount2").addClass("hide");
					$("#prd_amount").val("");
				}
			});
			
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/sponsor-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "sponsor_list.php";
							} else {
								alert("후원자를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// jqueryUI
// 			$("#apply_dt").datepicker({
// 				 dateFormat : "yy-mm-dd",
// 				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
// 				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
// 				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
// 				 showMonthAfterYear : true,
// 				 changeYear : true,
// 				 changeMonth : true,
// 				 defaultDate: "0",
// 				 numberOfMonths: 1
// 			});

			// bootstrap datepicker
			// https://bootstrap-datepicker.readthedocs.io/en/latest/i18n.html
			$.fn.datepicker.dates['en'] = {
    			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    			daysMin: ["일", "월", "화", "수", "목", "금", "토"],
    			months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    			monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    			today: "Today",
    			clear: "Clear",
    			format: "mm/dd/yyyy",
    			titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    			weekStart: 0
			};

			// 연월일 선택
			$('#apply_dt').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true
		    });

			// 연월 선택
			$('#prd_start_ym').datepicker({
				format: "yyyy-mm",
				viewMode: "months", 
				minViewMode: "months",
				autoclose: true
		    });
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>