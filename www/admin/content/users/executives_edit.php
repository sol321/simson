<?php
/*
 * Desc: 임원 편집
 *      사진 올리기 참조 : https://codepen.io/shubhamc_007/pen/zNJWPM
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-user.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('임원을 선택해 주십시오.');
}

$seq_id = $_GET['id'];

$user_row = if_get_executives_by_id($seq_id);

$position = $user_row['position'];
$name = $user_row['name'];
$email = $user_row['email'];
$org_name = $user_row['org_name'];

$meta_data = $user_row['meta_data'];
$jdec = json_decode($meta_data, true);

$file_attachment = $jdec['file_attachment'];

$thumb_url = $file_attachment[0]['thumb_url'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
		<style>
		
		</style>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content">
					<div class="row">
                    	<div class="col-md-6">
                        	<!-- form start -->
                            <form id="form-item-new" class="form-horizontal">
                            	<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
                            	
                        		<div class="box box-danger">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-red">임원 편집</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">직위*</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="position" id="position" value="<?php echo $position ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성명*</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="name" id="name" value="<?php echo $name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">근무처*</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="org_name" id="org_name" value="<?php echo $org_name ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">이메일*</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="email" id="email" value="<?php echo $email ?>">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">사진*</label>
                        					<div class="col-md-8">
                        						<br>
                        						<div class="container">
                                            		<div class="row">
                                            			<div class="col-md-2 imgUp">
                                            				<div class="image-preview" style="background-image: url(<?php echo $thumb_url ?>);"></div>
                                            				<label class="btn btn-info btn-upload">
                                            					사진 찾기
                                            					<input type="file" name="attachment[]" id="upload-photo" value="Upload Photo" style="width: 0px; height: 0px; overflow: hidden;">
                                            				</label>
                                            				<i class="fa fa-times image-del hide"></i>
                                            			</div><!-- col-2 -->
                                            		</div><!-- row -->
                                            	</div><!-- container -->
                        					</div>
                        				</div>
                        			</div>
                        			<div class="box-footer text-center">
        								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
                              		</div>
                        		</div>
                      		</form>
                    	</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		
		<script>
		$(function () {
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/executives-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "executives_list.php";
							} else {
								alert("임원을 수정할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 사진 이미지 Upload
			$("#upload-photo").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var ftype = this.files[0].type;
// 				var fext = fname.split('.').pop().toLowerCase();
				var max_file_size = 20971520;	// 20M

				if (!/^image/.test(ftype)) {
					alert("이미지 파일(png, jpg, gif)만 사용하실 수 있습니다.");
					return;
				}
				
				if (fsize > max_file_size) {
					alert("최대 20MB 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {

							$('.image-preview').css("background-image", "url("+ xhr.file_url[0] +")");
							
							var	fileLists = '<input type="hidden" name="file_path[]" value="' + xhr.file_path[0] + '">' +
    										'<input type="hidden" name="file_url[]" value="' + xhr.file_url[0] + '">' +
    										'<input type="hidden" name="file_name[]" value="' + xhr.file_name[0] + '">'
    										;
							$(".image-preview").append(fileLists);
							$("i.image-del").removeClass("hide");

						} else {
							alert(xhr.msg);
						}
						
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 사진 이미지 Upload 시작

			// 사진 이미지 삭제
			$(document).on("click", "i.image-del" , function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$(".image-preview").html("");
						$('.image-preview').css("background-image", "url(<?php echo $thumb_url ?>)");
						$("i.image-del").addClass("hide");
						
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 사진 이미지 삭제
			//-------------------- 사진 이미지 끝
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>