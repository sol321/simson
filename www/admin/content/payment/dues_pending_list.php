<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qs2 = !isset($_GET['qs2']) ? '' : trim($_GET['qs2']);

$ccd = empty($_GET['ccd']) ? '' : $_GET['ccd'];     // country code

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// 날짜별 검색
if (!empty($period_from) && !empty($period_to)) {
    $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

if (!empty($ccd)) {
    $sql .= " AND country_code = ?";
    array_push($sparam, $ccd);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			o.order_code,
            o.item_year,
            o.create_dt,
            u.user_login,
            u.name_ko,
            COUNT(*) AS tcnt,
            SUM(o.order_amount) AS tsum
		FROM
			" . $GLOBALS['if_tbl_item_order'] . " AS o
		LEFT JOIN
			" . $GLOBALS['if_tbl_users'] . " AS u
		ON
		    o.user_id = u.seq_id
		WHERE
            o.order_state = '1000' AND
            JSON_UNQUOTE(o.meta_data->'$.payment_method') = 'BANK_TR'
			$sql
        GROUP BY
            o.order_code,
            o.item_year,
            o.create_dt,
            u.user_login,
            u.name_ko
        ORDER BY
			o.create_dt DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
			
require_once ADMIN_PATH . '/include/cms-header.php';

?>
		
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>무통장 입금대기 내역</h1>
				</div>
				<div class="content">
				
					<div class="box box-default">
						<div class="box-body">
							<div class="row">
    							<form id="search-form">
    								<div class="col-md-6">
    									<div class="col-md-6">
    										<select name="qa1" id="qa1" class="form-control">
    											<optgroup label="키워드 검색">
    												<option value="name_ko" <?php echo strcmp($qa1, 'name_ko') ? '' : 'selected'; ?>>회원 이름</option>
    												<option value="user_login" <?php echo strcmp($qa1, 'user_login') ? '' : 'selected'; ?>>회원 아이디</option>
    												<option value="mobile" <?php echo strcmp($qa1, 'mobile') ? '' : 'selected'; ?>>휴대전화번호</option>
    												<option value="user_email" <?php echo strcmp($qa1, 'user_email') ? '' : 'selected'; ?>>이메일</option>
    											</optgroup>
    										</select>
    									</div>
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    								</div>
    
    								<div class="col-md-6">
    									<div class="add-top">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default hide"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    									</div>
    								</div>
    							</form>
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
    					<div class="box-body table-responsive">
    						<form id="form-item-list">
    							<table class="table table-hover">
    								<thead>
    									<tr>
    										<th>#</th>
    										<th>신청날짜</th>
    										<th>회원</th>
    										<th>내용</th>
    										<th>금액</th>
    										<th>입금인</th>
    										<th>입금은행</th>
    										<th>입금예정일</th>
    										<th></th>
    									</tr>
    								</thead>
    								<tbody>
    						<?php
    						if (!empty($item_results)) {
    						    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    
    							foreach ($item_results as $key => $val) {
    								$user_login = $val['user_login'];
    								$user_name = $val['name_ko'];
    								$order_code = $val['order_code'];
    								$tsum = $val['tsum'];
    								$create_dt = $val['create_dt'];
    								
    								$pmt_results = if_get_user_payment_by_code($order_code);
    								
    								$meta_data = $pmt_results[0]['meta_data'];
    								$jdec = json_decode($meta_data, true);
    								$remitter = $jdec['remitter'];
    								$bank_name = $jdec['bank_name'];
    								$due_date = $jdec['due_date'];
    						?>
    									<tr id="oc-<?php echo $order_code ?>">
    										<td><?php echo $list_no ?></td>
    										<td><?php echo substr($create_dt, 0, 10) ?></td>
    										<td>
    											<?php echo $user_name ?>
    											<div class="text-green"><?php echo $user_login ?></div>
    										</td>
    										<td>
    											<ol>
    								<?php
    							    foreach ($pmt_results as $k => $v) {
//     							        $i_type_secondary = $v['item_type_secondary'];
    							        $i_order_name = $v['order_name'];
    							        $i_order_amount = $v['order_amount'];
    								?>
    												<li>
            											<!-- <span class="text-orange"><?php echo $i_type_secondary ?></span> -->
            											<?php echo $i_order_name ?>
            											<span class="label label-success"><?php echo number_format($i_order_amount) ?></span>
            										</li>
    								<?php 
    								}
    								?>
    											</ol>
    										</td>
    										<td><b><?php echo number_format($tsum) ?></b></td>
    										<td><?php echo $remitter ?></td>
    										<td><?php echo str_replace(' ', '<br>', $bank_name) ?></td>
    										<td><?php echo $due_date ?></td>
    										<td>
    											<a href="dues_pending_edit.php?code=<?php echo $order_code ?>" class="btn btn-xs btn-info pps-edit">변경하기</a> &nbsp;
    											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
    										</td>
    									</tr>
    						<?php
    						      $list_no--;
    							}
    						}
    						?>
    								</tbody>
    							</table>
							</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
	
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");

    			$.ajax({
    				type : "POST",
    				url : "./ajax/dues-pending-list-delete.php",
    				data : {
    					"code" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>