<?php
/*
 * Desc: 학회별 회비 통계
 */
require_once '../../../if-config.php';

if_authenticate_admin();

$tyear = date('Y');

// search
$qa1 = empty($_GET['qa1']) ? $tyear : trim($_GET['qa1']);

$query = "
        SELECT
        	item_year,
            item_type_secondary,
        	SUM(pay_amount) AS total,
        	COUNT(*) AS cnt
        FROM
			" . $GLOBALS['if_tbl_user_payment'] . " 
        WHERE
        	item_year = ? AND
        	pay_state = '9000' AND
            pay_amount > 0
        GROUP BY
        	item_year,
            item_type_secondary
";
$stmt = $ifdb->prepare($query);
$stmt->bind_param('s', $qa1);
$stmt->execute();
$item_results =  $ifdb->get_results($stmt);

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="dues_statistics.php"><b>연도별 회비 통계</b></a></li>
							<li class="active"><a href="dues_statistics_inst.php"><b>학회별 회비 통계</b></a></li>
						</ul>
					</div>
				</section>

				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-default">
								<div class="box-body">
									<form id="search-form">
										<div class="col-md-6">
											<select name="qa1" id="qa1" class="form-control">
												<option value="">- 연도 -</option>
									<?php 
									for ($i = $tyear; $i > 1990; $i--) {
										$selected = $qa1 == $i ? 'selected' : '';
									?>
												<option value="<?php echo $i ?>" <?php echo $selected ?>><?php echo $i ?></option>
									<?php
									}
									?>
											</select>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header">
									<h4><?php echo $qa1 ?></h4>
								</div>
								<div class="box-body">
									<table class="table table-hover">
										<tbody>
											<tr class="bg-info">
												<th>학회</th>
												<th>인원 수</th>
												<th>합계</th>
											</tr>
							<?php
							$total_user = 0;
							$total_sum = 0;
							
							if (!empty($item_results)) {
								foreach ($item_results as $key => $val) {
								    $item_type_secondary = $val['item_type_secondary'];
								    $institute = empty($item_type_secondary) ? '<span class="text-danger">입회비</span>' : $item_type_secondary;  
									$total = $val['total'];
									$cnt = $val['cnt'];
									
									$total_user += $cnt;
									$total_sum += $total;
							?>
											<tr>
												<td><?php echo $institute ?></td>
												<td><?php echo $cnt ?></td>
												<td><?php echo number_format($total) ?></td>
											</tr>
							<?php
    							}
							}
							?>
										</tbody>
										<tfoot>
											<tr class="bg-info">
												<th>Total</th>
												<th><?php echo number_format($total_user) ?></th>
												<th><?php echo number_format($total_sum) ?></th>
											</tr>
										</tfoot>
									</table>
								</div><!-- /.box-body -->
							</div><!-- / .box -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function() {
			$("#qa1").change(function() {
				var year = $(this).val();

				location.href = "?qa1=" + year;
			});
		});
		</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
