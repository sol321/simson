<?php
/*
 * Desc: 무통장 입금대기 내역 삭제
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['code'])) {
    $code = 105;
    $msg = '내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$order_code = $_POST['code'];

$result = if_delete_item_order_by_code($order_code);

if (empty($result)) {
    $code = 201;
    $msg = '삭제하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>