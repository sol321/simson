<?php
/*
 * Desc: 구독 신청자 & 구독 신청 정보 업데이트
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['seq_id'])) {      // if_journal_user seq id
    $code = 101;
    $msg = '업데이트할 내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['subscription_id'])) {     // if_journal_subscription seq id
    $code = 102;
    $msg = '업데이트할 내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];
$subscription_id = $_POST['subscription_id'];

if_update_journal_subscription_user($seq_id);

$result = if_update_journal_subscription($subscription_id);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>