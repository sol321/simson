<?php
/*
 * Desc: 구독료 납부 내역 삭제
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 105;
    $msg = '내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$subscription_id = $_POST['id'];

// if_journal_user의 seq_id를 공유하는 다른 subscription 내역이 있는지 조회
$sub_row = if_get_journal_subscription($subscription_id);
$journal_user_id = $sub_row['journal_user_id'];

$result = if_delete_journal_subscription($subscription_id);

$slibing_count = if_get_journal_subscription_slibing_count($journal_user_id);
// subscription 레코드가 없으면 , if_journal_user 정보도  필요가 없으므로 삭제
if ($slibing_count < 1) {
    if_delete_journal_subscription_user($journal_user_id);
}

if (empty($result)) {
    $code = 201;
    $msg = '삭제하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>