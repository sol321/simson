<?php
/*
 * Desc: 무통장 입금대기 내역을 if_user_payment 로 이동함. 하나만.
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';
require_once FUNC_PATH . '/functions-user.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['order_code'])) {
    $code = 105;
    $msg = '업데이트할 내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['pay_date'])) {
    $code = 106;
    $msg = '결제날짜를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$order_code = $_POST['order_code'];

$result = if_add_user_payment_by_code($order_code);

if ($result > 0) {
    if_delete_item_order_by_code($order_code); // 카트에서 삭제
}

$json = compact('code', 'msg');
echo json_encode($json);

?>