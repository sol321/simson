<?php
/*
 * Desc: 편집 완료
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['pay_id'])) {
    $code = 105;
    $msg = '업데이트할 내역을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$pay_id = $_POST['pay_id'];

$pmt_history = if_get_user_payment($pay_id);    // 히스토리

$result = if_update_user_payment();     // 업데이트

if ($result) {
    if_record_user_payment($pay_id, $pmt_history);
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>