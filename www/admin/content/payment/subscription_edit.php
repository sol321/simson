<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('입금내역을 선택해 주십시오.');
}

$subscription_id = $_GET['id'];

// 구독 신청정보 조회
$sub_row = if_get_journal_subscription($subscription_id);

$journal_name = $sub_row['journal_name'];
$journal_fee = $sub_row['journal_fee'];
$journal_issue_month = $sub_row['journal_issue_month'];
$pay_date = $sub_row['pay_date'];
$pay_state = $sub_row['pay_state'];
$expiry_date = $sub_row['expiry_date'];
$journal_user_id = $sub_row['journal_user_id'];

// 구독 신청자 조회
$jn_urow = if_get_journal_subscription_user($journal_user_id);

$jn_user_seq = $jn_urow['seq_id'];
$user_name = $jn_urow['user_name'];
$user_email = $jn_urow['user_email'];
$org_phone = $jn_urow['org_phone'];
$user_mobile = $jn_urow['user_mobile'];
$create_dt = $jn_urow['create_dt'];
$meta_data = $jn_urow['meta_data'];
$jdec = json_decode($meta_data, TRUE);
$zipcode = $jdec['zipcode'];
$address1 = $jdec['address1'];
$address2 = $jdec['address2'];
$admin_memo = @$jdec['admin_memo'];

$jn_results = if_get_all_journal_items();

require_once ADMIN_PATH . '/include/cms-header.php';
?>

	<!-- jQueryUI -->
	<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						구독료 납부 내역 변경
					</h1>
				</section>

				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="seq_id" value="<?php echo $jn_user_seq ?>">
							<input type="hidden" name="subscription_id" value="<?php echo $subscription_id ?>">
							<div class="col-md-6">
								<div class="box box-warning">
									<div class="box-body">
										<div class="form-group">
                							<label class="col-md-3 control-label">등록 날짜</label>
                							<div class="col-md-9">
                								<p class="text-primary"><?php echo $create_dt ?></p>
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">신청자명</label>
                							<div class="col-md-9">
                								<input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo $user_name ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">이메일</label>
                							<div class="col-md-9">
                								<input type="text" name="user_email" id="user_email" class="form-control" value="<?php echo $user_email ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">휴대전화</label>
                							<div class="col-md-9">
                								<input type="text" name="user_mobile" id="user_mobile" class="form-control" value="<?php echo $user_mobile ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">직장 전화번호</label>
                							<div class="col-md-9">
                								<input type="text" name="org_phone" id="org_phone" class="form-control" value="<?php echo $org_phone ?>">
                							</div>
                						</div><div class="form-group">
                        					<label class="col-md-3 control-label">배송지 주소</label>
                        					<div class="col-md-3">
                        						<input type="text" class="form-control" name="zipcode" id="zipcode" value="<?php echo $zipcode ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-9">
                        						<input type="text" class="form-control" name="address1" id="address1" value="<?php echo $address1 ?>" readonly>
                        					</div>
                        					<div class="col-md-offset-3 col-md-9">
                        						<input type="text" class="form-control" name="address2" id="address2" placeholder="상세주소" value="<?php echo $address2 ?>">
                        					</div>
                        				</div>
                						<div class="form-group">
                							<label class="col-md-3 control-label">신청 학회지</label>
                							<div class="col-md-9">
                								<?php echo $journal_name ?> (<?php echo $journal_issue_month ?>월)
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 금액</label>
                							<div class="col-md-9">
                								<input type="text" name="journal_fee" id="journal_fee" class="form-control numeric" value="<?php echo $journal_fee ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 상태</label>
                							<div class="col-md-9">
                					<?php 
                					foreach ($if_payment_state as $key => $val) {
                					    $checked = $key == $pay_state ? 'checked' : '';
                					?>
                								<div class="radio">
                									<label>
                										<input type="radio" name="pay_state" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
                									</label>
                								</div>
                					<?php 
                					}
                					?>
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 날짜</label>
                							<div class="col-md-9">
                								<input type="text" name="pay_date" id="pay_date" class="form-control" value="<?php echo $pay_date ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">구독 만료 날짜</label>
                							<div class="col-md-9">
                								<!-- 결제날짜 && 결제완료(결제 상태) : 구독 만료 날짜 생성  -->
                								<?php echo $expiry_date ?>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-3 control-label">메모</label>
                							<div class="col-md-9">
                								<textarea name="admin_memo" class="form-control"><?php echo $admin_memo ?></textarea>
                							</div>
                						</div>
									</div>
									<div class="box-footer text-center">
            							<button type="button" class="btn btn-default" id="btn-cancel">취소</button> &nbsp; &nbsp;
        								<button type="submit" class="btn btn-success" id="btn-submit">저장</button>
            						</div><!-- /.box-footer -->
								</div>
							</div>
							
						</form>
						<div class="col-md-4">
						<!-- Contents -->
						</div>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		
    	<script>
    	$(function () {
    		$(".numeric").numeric({ negative: false });
    		
    		$("#form-item-new").submit(function(e) {
        		e.preventDefault();
    
        		var pay_date = $.trim($("#pay_date").val());
    
        		if (pay_date.length != 10) {
            		alert("결제날짜를 YYYY-mm-dd 형식으로 입력해 주십시오.");
            		$("#pay_date").focus();
            		return false;
            	}
    
        		$.ajax({
    				type : "POST",
    				url : "./ajax/subscription-edit.php",
    				data : $(this).serialize(),
    				dataType : "json",
    				beforeSend : function() {
    
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.href = "dues_list.php";
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    				$("#btn-submit").prop("disabled", false);
    			}); // ajax
            });
    
        	
    		$("#btn-cancel").click(function() {
        		history.back();
        	});
    
    		$("#pay_date, #expiry_date").datepicker({
      			 dateFormat : "yy-mm-dd",
      			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
      		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
      			 changeMonth : true,
      			 changeYear : true
      		});

      		$(".journal_name").click(function() {
          		var issueMon = $(this).closest("label").find(".jn-issue-desc").html();
          		$("#journal_issue_month").val(issueMon);
          	});
    	});

    	// 우편번호 찾기
		$("#zipcode, #address1").click(function() {
			execDaumPostcode('home');
		});

    	// Daum 우편번호
		function execDaumPostcode(spot) {
    		new daum.Postcode({
    			oncomplete: function(data) {
    				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
    
    				// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
    				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
    				var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
    				var extraRoadAddr = ''; // 도로명 조합형 주소 변수
    
    				// 법정동명이 있을 경우 추가한다. (법정리는 제외)
    				// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
    				if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
    					extraRoadAddr += data.bname;
    				}
    				// 건물명이 있고, 공동주택일 경우 추가한다.
    				if(data.buildingName !== '' && data.apartment === 'Y'){
    				   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
    				}
    				// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
    				if(extraRoadAddr !== ''){
    					extraRoadAddr = ' (' + extraRoadAddr + ')';
    				}
    				// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
    				if(fullRoadAddr !== ''){
    					fullRoadAddr += extraRoadAddr;
    				}
    
    				// 우편번호와 주소 정보를 해당 필드에 넣는다.
    				if (spot == "home") {
        				document.getElementById('zipcode').value = data.zonecode; //5자리 새우편번호 사용
        				document.getElementById('address1').value = fullRoadAddr + " ";		// 도로명 주소
        				document.getElementById('address2').focus();
    				}
    			}
    		}).open();
    	}
    	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
