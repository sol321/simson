<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('입금내역을 선택해 주십시오.');
}

$pay_id = $_GET['id'];

$pay_row = if_get_user_payment($pay_id);

$pay_content = $pay_row['pay_content'];
$pay_amount = $pay_row['pay_amount'];
$pay_date = $pay_row['pay_date'];
$pay_method = $pay_row['pay_method'];
$pay_state = $pay_row['pay_state'];
$item_type = $pay_row['item_type'];
$item_type_secondary = $pay_row['item_type_secondary'];
$item_year = $pay_row['item_year'];
$create_dt = $pay_row['create_dt'];
$expiry_date = $pay_row['expiry_date'];
$meta_data = $pay_row['meta_data'];
$jdec = json_decode($meta_data, true);

$admin_memo = @$jdec['admin_memo'];

$user_id = $pay_row['user_id'];

$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);

require_once ADMIN_PATH . '/include/cms-header.php';
?>

	<!-- jQueryUI -->
	<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						회비 납부 내역 변경
					</h1>
				</section>

				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="pay_id" value="<?php echo $pay_id ?>">
							<div class="col-md-6">
								<div class="box box-warning">
									<div class="box-body">
										<div class="form-group">
                							<label class="col-md-3 control-label">등록 날짜</label>
                							<div class="col-md-9">
                								<p class="text-primary"><?php echo $create_dt ?></p>
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-3 control-label">연도</label>
                							<div class="col-md-9">
                								<div class="input-group">
                									<input type="text" name="item_year" id="item_year" class="form-control numeric" value="<?php echo $item_year ?>" maxlength="4">
                									<div class="input-group-addon">년</div>
                								</div>
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 날짜</label>
                							<div class="col-md-9">
                								<input type="text" name="pay_date" id="pay_date" class="form-control" value="<?php echo $pay_date ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 금액</label>
                							<div class="col-md-9">
                								<input type="text" name="pay_amount" id="pay_amount" class="form-control numeric" value="<?php echo $pay_amount ?>">
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 방법</label>
                							<div class="col-md-9">
                					<?php 
                					foreach ($if_payment_method as $key => $val) {
                					    $checked = $key == $pay_method ? 'checked' : '';
                					?>
                								<div class="radio">
                									<label>
                										<input type="radio" name="pay_method" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
                									</label>
                								</div>
                					<?php 
                					}
                					?>
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">결제 상태</label>
                							<div class="col-md-9">
                					<?php 
                					foreach ($if_payment_state as $key => $val) {
                					    $checked = $key == $pay_state ? 'checked' : '';
                					?>
                								<div class="radio">
                									<label>
                										<input type="radio" name="pay_state" value="<?php echo $key ?>" <?php echo $checked ?>><?php echo $val ?>
                									</label>
                								</div>
                					<?php 
                					}
                					?>
                							</div>
                						</div>
										<div class="form-group">
                							<label class="col-md-3 control-label">회비 만료 날짜</label>
                							<div class="col-md-9">
                								<input type="text" name="expiry_date" id="expiry_date" class="form-control" value="<?php echo $expiry_date ?>">
                							</div>
                						</div>
                						<div class="form-group">
                							<label class="col-md-3 control-label">회비 이름</label>
                							<div class="col-md-9">
                								<input type="text" name="pay_content" id="pay_content" class="form-control" value="<?php echo $pay_content ?>">
                							</div>
                						</div>
                			<?php 
                			if (strcmp($item_type, 'ENT')) { // 입회비는 학회가 필요 없음.
                			?>
                						<div class="form-group">
                							<label class="col-md-3 control-label">학회</label>
                							<div class="col-md-9">
                					<?php 
            						if ( !empty($unit_items) ) {
            							foreach ($unit_items as $key => $val ) {
            							    $checked = strcmp($val, $item_type_secondary) ? '' : 'checked';
            						?>
                								<div class="radio">
                									<label>
                										<input type="radio" name="item_type_secondary" value="<?php echo $val ?>" <?php echo $checked ?>><?php echo $val ?>
                									</label>
                								</div>
                					<?php 
            							}
            						}
                					?>
                							</div>
                						</div>
                			<?php 
                			}
                			?>
                						<div class="form-group">
                							<label class="col-md-3 control-label">메모</label>
                							<div class="col-md-9">
                								<textarea name="admin_memo" class="form-control"><?php echo $admin_memo ?></textarea>
                							</div>
                						</div>
									</div>
									<div class="box-footer text-center">
            							<button type="button" class="btn btn-default" id="btn-cancel">
            								취소
            							</button> &nbsp; &nbsp;
        								<button type="submit" class="btn btn-success" id="btn-submit">
        									저장
        								</button>
            						</div><!-- /.box-footer -->
								</div>
							</div>
							
						</form>
						<div class="col-md-4">
						<!-- Contents -->
						</div>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
    	<script>
    	$(function () {
    		$(".numeric").numeric({ negative: false });
    		
    		$("#form-item-new").submit(function(e) {
        		e.preventDefault();
    
        		var pay_date = $.trim($("#pay_date").val());
    
        		if (pay_date.length != 10) {
            		alert("결제날짜를 YYYY-mm-dd 형식으로 입력해 주십시오.");
            		$("#pay_date").focus();
            		return false;
            	}
    
        		$.ajax({
    				type : "POST",
    				url : "./ajax/dues-edit.php",
    				data : $(this).serialize(),
    				dataType : "json",
    				beforeSend : function() {
    
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.href = "dues_list.php";
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    				$("#btn-submit").prop("disabled", false);
    			}); // ajax
            });
    
        	
    		$("#btn-cancel").click(function() {
        		history.back();
        	});
    
    		$("#pay_date, #expiry_date").datepicker({
      			 dateFormat : "yy-mm-dd",
      			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
      		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
      			 changeMonth : true,
      			 changeYear : true
      		});
    
    	});
    	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
