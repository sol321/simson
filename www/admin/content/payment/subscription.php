<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-product.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);

$ccd = empty($_GET['ccd']) ? '' : $_GET['ccd'];     // country code

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

if (!empty($qa2)) {
    $sql .= " AND s.journal_name = ?";
    array_push($sparam, $qa2);
}

// 날짜별 검색
if (!empty($period_from) && !empty($period_to)) {
    $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

if (!empty($ccd)) {
    $sql .= " AND country_code = ?";
    array_push($sparam, $ccd);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			s.*,
            ju.user_name,
            ju.create_dt,
            u.user_login
		FROM
			" . $GLOBALS['if_tbl_journal_subscription'] . " AS s
		INNER JOIN
			" . $GLOBALS['if_tbl_journal_user'] . " AS ju
		LEFT JOIN
			" . $GLOBALS['if_tbl_users'] . " AS u
        ON
            u.seq_id = ju.user_id
		WHERE
            s.pay_state <> '1000' AND
			ju.seq_id = s.journal_user_id
			$sql
		ORDER BY
			ju.seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

$jn_results = if_get_all_journal_items();  // 학회지 정보

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>구독료 납부 현황</h1>
				</div>
				<div class="content">
				
					<div class="box box-default">
						<div class="box-body">
							<div class="row">
    							<form id="search-form">
    								<div class="col-md-8">
    								
    									<div class="col-md-4">
    										<select name="qa2" class="form-control">
        										<option value="">- 학회지 선택 -</option>
        							<?php
        							if (!empty($jn_results)) {
        							    foreach ($jn_results as $key => $val ) {
        							        $jn_name = $val['journal_name'];
        							        $selected = strcmp($qa2, $jn_name) ? '' : 'selected';
        							?>
        										<option value="<?php echo $jn_name ?>" <?php echo $selected ?>><?php echo $jn_name ?></option>
        							<?php
            							}
        							}
        							?>
        									</select>
        								</div>
    									<div class="col-md-4">
    										<select name="qa1" id="qa1" class="form-control">
    											<option value="">- 선택 -</option>
												<option value="ju.user_name" <?php echo strcmp($qa1, 'ju.user_name') ? '' : 'selected'; ?>>신청자명</option>
												<option value="u.name_ko" <?php echo strcmp($qa1, 'u.name_ko') ? '' : 'selected'; ?>>회원 이름</option>
												<option value="u.user_login" <?php echo strcmp($qa1, 'u.user_login') ? '' : 'selected'; ?>>회원 아이디</option>
												<option value="ju.user_mobile" <?php echo strcmp($qa1, 'ju.user_mobile') ? '' : 'selected'; ?>>휴대전화번호</option>
												<option value="ju.user_email" <?php echo strcmp($qa1, 'ju.user_email') ? '' : 'selected'; ?>>이메일</option>
    										</select>
    									</div>
    									<div class="col-md-4">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    								</div>
    
    								<div class="col-md-4">
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
										<button type="button" id="reset-btn" class="btn btn-default hide"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
    							</form>
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
    					<div class="box-body table-responsive">
    						<form id="form-item-list">
    							<table class="table table-hover">
    								<thead>
    									<tr>
    										<th>#</th>
    										<th>아이디</th>
    										<th>신청자명</th>
    										<th>학회지</th>
    										<th>결제금액</th>
    										<th>신청날짜</th>
    										<th>등록날짜</th>
    										<th>만료날짜</th>
    										<th>상태</th>
    										<th></th>
    									</tr>
    								</thead>
    								<tbody>
    						<?php
    						if (!empty($item_results)) {
    						    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    
    							foreach ($item_results as $key => $val) {
    								$subscription_id = $val['subscription_id'];
    								$user_login = $val['user_login'];
    								$user_name = $val['user_name'];
    								$journal_name = $val['journal_name'];
    								$journal_fee = $val['journal_fee'];
    								$pay_state = $val['pay_state'];
    								$create_dt = $val['create_dt'];
    								$pay_date = $val['pay_date'];
    								$expiry_date = $val['expiry_date'];

//     								$meta_data = $val['meta_data'];
//     								$jdec = json_decode($meta_data, true);
    						?>
    									<tr id="id-<?php echo $subscription_id ?>">
        									<td><?php echo $list_no ?></td>
        									<td><?php echo $user_login ?></td>
        									<td><?php echo $user_name ?></td>
    										<td><?php echo $journal_name ?></td>
    										<td><?php echo number_format($journal_fee) ?></td>
    										<td><?php echo substr($create_dt, 0, 10) ?>
    										<td><?php echo $pay_date ?></td>
    										<td><?php echo $expiry_date ?></td>
    										<td><?php echo $if_payment_state[$pay_state] ?></td>
    										<td>
    											<a href="subscription_edit.php?id=<?php echo $subscription_id ?>" class="btn btn-xs btn-info pps-edit">변경하기</a> &nbsp;
    											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
    										</td>
    									</tr>
    						<?php
    						        $list_no--;
    							}
    						}
    						?>
    								</tbody>
    							</table>
							</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
	
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/subscription-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>