<?php
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 20;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qa3 = empty($_GET['qa3']) ? '' : trim($_GET['qa3']);

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

if (!empty($qa3)) {
    $sql .= " AND p.item_year = ?";
    array_push($sparam, $qa3);
}

// 날짜별 검색
if (!empty($period_from) && !empty($period_to)) {
    $sql .= " AND $qa2 BETWEEN ? AND ?";
    $period_from_attach = $period_from . ' 00:00:00';
    $period_to_attach = $period_to . ' 23:59:59';
    array_push($sparam, $period_from_attach, $period_to_attach);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			p.*,
            u.user_login,
            u.name_ko
		FROM
			" . $GLOBALS['if_tbl_user_payment'] . " AS p
		LEFT JOIN
			" . $GLOBALS['if_tbl_users'] . " AS u
		ON
		    p.user_id = u.seq_id
		WHERE
			1
			$sql
		ORDER BY
			p.pay_id DESC
";

$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();


require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>회비 납부 현황</h1>
				</div>
				<div class="content">
				
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<div class="row">
									<div class="col-md-3">
										<select name="qa3" class="form-control">
    										<option value="">- 연도 -</option>
    							<?php
    							for ($year = date('Y'); $year > 2000; $year--) {
    							    $selected = $qa3 == $year ? 'selected' : '';
    							?>
    										<option value="<?php echo $year ?>" <?php echo $selected ?>><?php echo $year ?></option>
    							<?php
    							}
    							?>
    									</select>
    								</div>
    							</div>
    							
    							<div class="row add-top">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="u.user_login" <?php echo strcmp($qa1, 'u.user_login') ? '' : 'selected'; ?>>아이디</option>
											<option value="u.name_ko" <?php echo strcmp($qa1, 'u.name_ko') ? '' : 'selected'; ?>>성명</option>
											<option value="u.user_email" <?php echo strcmp($qa1, 'u.user_email') ? '' : 'selected'; ?>>이메일</option>
											<option value="u.user_mobile" <?php echo strcmp($qa1, 'u.user_mobile') ? '' : 'selected'; ?>>휴대전화</option>
										</select>
									</div>
									<div class="col-md-6">
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    									<div class="col-md-6">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        								</div>
    								</div>
								</div>
								
								<div class="row add-top">
    								<div class="col-md-2">
    									<select name="qa2" class="form-control">
    										<option value="">- 기간 선택 -</option>
    										<option value="p.pay_date" <?php echo !strcmp($qa2, 'p.pay_date') ? 'selected' : ''; ?>>결제 날짜</option>
    										<option value="p.expiry_date" <?php echo !strcmp($qa2, 'p.expiry_date') ? 'selected' : ''; ?>>만료날짜</option>
    									</select>
    								</div>
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
    									</div>
    								</div>
    							</div>
							</form>
							
						</div><!-- /.box-body -->
						<div class="box-footer">
						</div>
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
    					<div class="box-body table-responsive">
    						<form id="form-item-list">
    							<table class="table table-hover">
    								<thead>
    									<tr>
    										<th>#</th>
    										<th>연도</th>
    										<th>회원</th>
    										<th>내용</th>
    										<th>금액</th>
    										<th>결제방법</th>
    										<th>결제날짜</th>
    										<th>만료날짜</th>
    										<th>상태</th>
    										<th></th>
    									</tr>
    								</thead>
    								<tbody>
    						<?php
    						if (!empty($item_results)) {
    						    $list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    
    							foreach ($item_results as $key => $val) {
    								$pay_id = $val['pay_id'];
    								$user_login = $val['user_login'];
    								$user_name = $val['name_ko'];
    								$pay_state = $val['pay_state'];
    								$pay_content = $val['pay_content'];
    								$pay_amount = $val['pay_amount'];
    								$pay_date = $val['pay_date'];
    								$pay_method = $val['pay_method'];
    								$item_type = $val['item_type'];
    								$expiry_date = strcmp($item_type, 'ANN') ? '-' : $val['expiry_date'];
    								$item_year = $val['item_year'];
    								$item_type_secondary = $val['item_type_secondary'];
    						?>
    									<tr id="pmt-<?php echo $pay_id ?>">
        									<td><?php echo $list_no ?></td>
    										<td><?php echo $item_year ?></td>
    										<td>
    											<?php echo $user_name ?>
    											<div class="text-green"><?php echo $user_login ?></div>
    										</td>
    										<td>
    											<?php echo $pay_content ?>
    										</td>
    										<td><?php echo number_format($pay_amount) ?></td>
    										<td><?php echo $if_payment_method[$pay_method] ?></td>
    										<td><?php echo str_replace('-', '.', $pay_date) ?></td>
    										<td><?php echo $expiry_date ?></td>
    										<td><?php echo $if_payment_state[$pay_state] ?></td>
    										
    										<td>
    											<a href="dues_edit.php?id=<?php echo $pay_id ?>" class="btn btn-xs btn-info pps-edit">변경하기</a> &nbsp;
    											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
    										</td>
    									</tr>
    						<?php
    						        $list_no--;
    							}
    						}
    						?>
    								</tbody>
    							</table>
							</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
	
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
		$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}
    
    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/dues-list-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>