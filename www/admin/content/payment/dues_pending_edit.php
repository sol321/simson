<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-payment.php';

if_authenticate_admin();

if (empty($_GET['code'])) {
    if_js_alert_back('입금내역을 선택해 주십시오.');
}

$order_code = $_GET['code'];

$order_result = if_get_item_order_by_code($order_code);

require_once ADMIN_PATH . '/include/cms-header.php';
?>

	<!-- jQueryUI -->
	<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						무통장 입금대기 승인
					</h1>
				</section>

				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<form id="form-item-new" class="form-horizontal">
							<input type="hidden" name="order_code" value="<?php echo $order_code ?>">
							<div class="col-md-6">
								<div class="box box-info">
            						<div class="box-body">
            							<table class="table">
            								<tbody>
                								<tr class="active">
                									<th>회비명</th>
                									<th>금액(원)</th>
                								</tr>
                					<?php 
                					if (!empty($order_result)) {
                					    $total_amount = 0;
                					    
                					    foreach ($order_result as $key => $val) {
                					        $order_name = $val['order_name'];
                					        $amount = $val['order_amount'];
//                 					        $item_period = $val['item_period'];
                					        
                					        $meta_data = $val['meta_data'];
                					        $jdec = json_decode($meta_data, true);
                					        $remitter = $jdec['remitter'];
                					        $bank_name = $jdec['bank_name'];
                					        $due_date = $jdec['due_date'];
                					        
                					        $total_amount += $amount;
                					?>
                								<tr>
                									<td><?php echo $order_name ?></td>
                									<td><?php echo number_format($amount) ?></td>
                								</tr>
                					<?php 
                					    }
                					}
                					?>
                							</tbody>
                							<tfoot>
                								<tr class="bg-info">
                    								<td>합계</td>
                    								<td>
                    									<b><?php echo number_format($total_amount) ?></b>
                    								</td>
                    							</tr>
                							</tfoot>
                							
            							</table>
            						
            							<table class="table">
                        					<tbody>
                        						<tr>
                        							<th class="active">입금인</th>
                        							<td>
                        								<?php echo $remitter ?>
                        							</td>
                        						</tr>
                        						<tr>
                        							<th class="active">입금 예정일</th>
                        							<td>
                        								<?php echo $due_date ?>
                        							</td>
                        						</tr>
                        						<tr>
                        							<th class="active">입금 은행</th>
                        							<td>
                        								<?php echo $bank_name ?>
                        							</td>
                        						</tr>
                        					</tbody>
                        				</table>
                                    	
                						
            						</div><!-- /.box-body -->
            					</div><!-- / .box -->
							</div><!-- /.col -->
							
							<div class="col-md-6">
								<div class="box box-success">
									<div class="box-body">
										<div class="form-group has-success">
                							<label class="col-md-3 control-label">입금 날짜</label>
                							<div class="col-md-9">
                								<input type="text" name="pay_date" id="pay_date" class="form-control" value="<?php echo $due_date ?>">
                								<div class="help-block">
                									입금날짜를 기준으로 연회비 유효 기간이 정해집니다. (입금일 + 1년)
                								</div>
                							</div>
                						</div>
									</div>
									<div class="box-footer text-center">
            							<button type="button" class="btn btn-default" id="btn-cancel">
            								<i class="fa fa-mail-reply"></i> 뒤로가기
            							</button> &nbsp; &nbsp;
        								<button type="submit" class="btn btn-success" id="btn-submit">
        									입금완료로 변경
        								</button>
            						</div><!-- /.box-footer -->
								</div>
							</div>
							
						</form>
						<div class="col-md-4">
						<!-- Contents -->
						</div>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
	<script>
	$(function () {
		$("#form-item-new").submit(function(e) {
    		e.preventDefault();

    		var pay_date = $.trim($("#pay_date").val());

    		if (pay_date.length != 10) {
        		alert("결제날짜를 YYYY-mm-dd 형식으로 입력해 주십시오.");
        		$("#pay_date").focus();
        		return false;
        	}

    		$.ajax({
				type : "POST",
				url : "./ajax/dues-pending-edit.php",
				data : $(this).serialize(),
				dataType : "json",
				beforeSend : function() {

				},
				success : function(res) {
					if (res.code == "0") {
						location.href = "dues_pending_list.php";
					} else {
						alert(res.msg);
					}
				}
			}).done(function() {
			}).fail(function() {
			}).always(function() {
				$("#btn-submit").prop("disabled", false);
			}); // ajax
        });

    	
		$("#btn-cancel").click(function() {
    		history.back();
    	});

		$("#pay_date").datepicker({
  			 dateFormat : "yy-mm-dd",
  			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
  		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
  			 changeMonth : true,
  			 changeYear : true
  		});

	});
	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
