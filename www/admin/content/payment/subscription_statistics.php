<?php
/*
 * Desc: 구독료 통계
 */
require_once '../../../if-config.php';

if_authenticate_admin();

$tyear = date('Y');

// search
$qa1 = empty($_GET['qa1']) ? $tyear : trim($_GET['qa1']);

$from = $qa1 . '-01-01';
$to = $qa1 . '-12-31';
 
$query = "
        SELECT
        	journal_name,
        	SUM(journal_fee) AS total,
        	COUNT(*) AS cnt
        FROM
        	" . $GLOBALS['if_tbl_journal_subscription'] . "
        WHERE
        	pay_state = '9000' AND
            pay_date BETWEEN ? AND ?
        GROUP BY
        	journal_name
";
$stmt = $ifdb->prepare($query);
$stmt->bind_param('ss', $from, $to);
$stmt->execute();
$item_results =  $ifdb->get_results($stmt);

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-default">
								<div class="box-body">
									<form id="search-form">
										<div class="col-md-6">
											<select name="qa1" id="qa1" class="form-control">
												<option value="">- 연도 -</option>
									<?php 
									for ($i = $tyear; $i > 1990; $i--) {
										$selected = $qa1 == $i ? 'selected' : '';
									?>
												<option value="<?php echo $i ?>" <?php echo $selected ?>><?php echo $i ?></option>
									<?php
									}
									?>
											</select>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header">
									<h4><?php echo $qa1 ?></h4>
								</div>
								<div class="box-body">
									<table class="table table-hover">
										<tbody>
											<tr class="bg-info">
												<th>신청 학회지</th>
												<th>인원 수</th>
												<th>합계</th>
											</tr>
							<?php
							$total_user = 0;
							$total_sum = 0;
							
							if (!empty($item_results)) {
								foreach ($item_results as $key => $val) {
								    $journal_name = $val['journal_name'];
									$total = $val['total'];
									$cnt = $val['cnt'];
									
									$total_user += $cnt;
									$total_sum += $total;
							?>
											<tr>
												<td><?php echo $journal_name ?></td>
												<td><?php echo $cnt ?></td>
												<td><?php echo number_format($total) ?></td>
											</tr>
							<?php
    							}
							}
							?>
										</tbody>
										<tfoot>
											<tr class="bg-info">
												<th>Total</th>
												<th><?php echo number_format($total_user) ?></th>
												<th><?php echo number_format($total_sum) ?></th>
											</tr>
										</tfoot>
									</table>
								</div><!-- /.box-body -->
							</div><!-- / .box -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function() {
			$("#qa1").change(function() {
				var year = $(this).val();

				location.href = "?qa1=" + year;
			});
		});
		</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
