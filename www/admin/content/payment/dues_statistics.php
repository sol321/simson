<?php
/*
 * Desc: 연도별 회비 통계
 */
require_once '../../../if-config.php';

if_authenticate_admin();

$tyear = date('Y');

// search
$qa1 = empty($_GET['qa1']) ? $tyear : trim($_GET['qa1']);

$query = "
		SELECT
			SUBSTR(pay_date, 1, 7) AS ym,
			SUM(pay_amount) AS total,
			COUNT(*) AS cnt
		FROM
			" . $GLOBALS['if_tbl_user_payment'] . "
		WHERE
            item_year = ? AND
			pay_state = '9000' AND
			pay_amount > 0
		GROUP BY
			SUBSTR(pay_date, 1, 7)
";	
$stmt = $ifdb->prepare($query);
$stmt->bind_param('s', $qa1);
$stmt->execute();
$item_results =  $ifdb->get_results($stmt);

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="dues_statistics.php"><b>연도별 회비 통계</b></a></li>
							<li><a href="dues_statistics_inst.php"><b>학회별 회비 통계</b></a></li>
						</ul>
					</div>
				</section>

				<!-- Main content -->
				
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-default">
								<div class="box-body">
									<form id="search-form">
										<div class="col-md-6">
											<select name="qa1" id="qa1" class="form-control">
												<option value="">- 연도 -</option>
									<?php 
									for ($i = $tyear; $i > 1990; $i--) {
										$selected = $qa1 == $i ? 'selected' : '';
									?>
												<option value="<?php echo $i ?>" <?php echo $selected ?>><?php echo $i ?></option>
									<?php
									}
									?>
											</select>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header">
									<h4><?php echo $qa1 ?></h4>
								</div>
								<div class="box-body">
									<table class="table table-hover">
										<tbody>
											<tr class="bg-info">
												<th>월</th>
												<th>인원 수</th>
												<th>합계</th>
											</tr>
							<?php
							$total_user = 0;
							$total_sum = 0;
							
							for ($i = 0; $i < 12; $i++) {
							
    							if (!empty($item_results)) {
    								foreach ($item_results as $key => $val) {
    									$ym = $val['ym'];
    									$month = intval(substr($ym, 5));
    									$total = $val['total'];
    									$cnt = $val['cnt'];
    									
    									if (($i + 1) == $month) {
    									    $total_user += $cnt;
    									    $total_sum += $total;
							?>
											<tr>
												<td><?php echo $month ?></td>
												<td><?php echo $cnt ?></td>
												<td><?php echo number_format($total) ?></td>
											</tr>
							<?php
                							$i++;
    									}
    								}
    								echo '<tr><td>' . ($i + 1) . '</td><td>0</td><td>0</td></tr>';
    							} else {
    							    echo '<tr><td>' . ($i + 1) . '</td><td>0</td><td>0</td></tr>';
    							}
							}
							?>
										</tbody>
										<tfoot>
											<tr class="bg-info">
												<th>Total</th>
												<th><?php echo number_format($total_user) ?></th>
												<th><?php echo number_format($total_sum) ?></th>
											</tr>
										</tfoot>
									</table>
								</div><!-- /.box-body -->
							</div><!-- / .box -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function() {
			$("#qa1").change(function() {
				var year = $(this).val();

				location.href = "?qa1=" + year;
			});
		});
		</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
