<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-board.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
	if_js_alert_back('글에 대한 정보가 필요합니다.');
}

/* 게시글에 대한 정보 */

$post_id = $_GET['id'];
$post_row = if_get_post($post_id);

if (empty($post_row)) {
	if_js_alert_back('게시글이 존재하지 않습니다.');
}

// $post_row -> variable 변수 할당
foreach ($post_row as $key => $val) {
	${"col_$key"} = $val;
}

// JSON
$col_meta_data = json_decode($col_meta_data, true);
$file_attachment = empty($col_meta_data['file_attachment']) ? '' : $col_meta_data['file_attachment'];
$thumb_attachment = empty($col_meta_data['thumb_attachment']) ? '' : $col_meta_data['thumb_attachment'];

// 게시판 정보
$board_row = if_get_board($col_template_id);
$tpl_id = $board_row['seq_id'];
$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];
$meta_data = $board_row['meta_data'];
$tpl_max_filesize = if_get_val_from_json($meta_data, 'tpl_max_filesize');   // MB
$max_file_size = $tpl_max_filesize * 1048576;


$comment_style = "style='display:none;'";
$meta_data = json_decode($meta_data, true);

//댓글 사용여부
$use_comment = !empty($meta_data['use_comment']) ? $meta_data['use_comment'] : 'N';
if(!strcmp($use_comment,'Y')) {
    // 댓글 조회
    $comment_results = if_get_post_comments($post_id, 'DESC');
    $comment_style = "";
} else {
    $comment_results = [];
}

// 댓글 조회
//$comment_results = if_get_post_comments($post_id, 'DESC');

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1><?php echo $tpl_name ?></h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">상세 보기</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<table class="table table-bordered">
								<colgroup>
									<col width="10%">
									<col width="*">
									<col width="10%">
								</colgroup>
								<tr>
									<th>제목</th>
									<td colspan="3"><?php echo htmlspecialchars($col_post_title) ?></td>
								</tr>
								<tr>
									<th>이름</th>
									<td><?php echo $col_post_name ?>
									<th>등록날짜</th>
									<td><?php echo $col_post_date ?>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="3"><?php echo $col_post_content ?></td>
								</tr>
								<tr>
									<th>대표 이미지</th>
									<td colspan="3">
						<?php
						if ( !empty($thumb_attachment) ) {
							foreach ( $thumb_attachment as $key => $val ) {
								$thumb_path = $val['file_path'];
								$thumb_url = $val['file_url'];
								$thumb_name = $val['file_name'];
								if (is_file($thumb_path)) {
						?>
										<img src="<?php echo $thumb_url ?>" style="max-width: 800px;">
						<?php
								}
							}
						}
						?>
									
									</td>
								</tr>
								<tr>
									<th>첨부 파일</th>
									<td colspan="3">
										<ul class="list-group" id="preview-attachment">
							<?php
							if ( !empty($file_attachment) ) {
								foreach ( $file_attachment as $key => $val ) {
									$attach_file_path = $val['file_path'];
									$attach_file_url = $val['file_url'];
									$attach_file_name = $val['file_name'];
									if (is_file($attach_file_path)) {
							?>
											<li class="list-group-item list-group-item-success">
												<input name="file_path[]" value="<?php echo $attach_file_path ?>" type="hidden">
												<input name="file_url[]" value="<?php echo $attach_file_url ?>" type="hidden">
												<input name="file_name[]" value="<?php echo $attach_file_name ?>" type="hidden">
												<a href="<?php echo INC_URL ?>/lib/download_post_attachment.php?id=<?php echo $post_id ?>&idx=<?php echo $key ?>" class="btn btn-success btn-xs">
													<?php echo $attach_file_name ?>
												</a>
											</li>
							<?php
									}
								}
							}
							?>
										</ul>
									</td>
								</tr>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer text-center">
							<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
						</div>
						<!-- /.box-footer -->
					</div>
					
					<div class="box box-warning"  <?php echo $comment_style;?>>
						<div class="box-header with-border">
							<h3 class="box-title">댓글 보기</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
						
				<?php 
				if (!empty($comment_results)) {
					foreach ($comment_results as $key => $val) {
						$comment_id = $val['comment_id'];
						$comment_author = $val['comment_author'];
						$comment_content = htmlspecialchars($val['comment_content']);
						$comment_dt = $val['comment_dt'];
						$meta_data = $val['meta_data'];
						$time_ago = if_time_ago(strtotime($comment_dt));
						
						$jdec = json_decode($meta_data, true);
						$file_attachment = $jdec['file_attachment'];
				?>
							<div class="panel panel-default article-comment" id="comment-<?php echo $comment_id ?>">
								<div class="panel-heading">
									<h3 class="panel-title">
										<button type="button" class="pull-right btn btn-danger btn-xs btn-delete">삭제</button>
										<?php echo $comment_author ?> &nbsp; &nbsp;
										<small><?php echo $time_ago ?></small>
									</h3>
								</div>
								<div class="panel-body">
									<?php echo nl2br($comment_content) ?>
								</div>
								<div class="panel-footer">
    					<?php 
                        if (!empty($file_attachment)) {
                            foreach ($file_attachment as $k => $v) {
                                $file_name = $v['file_name'];
                        ?>
                        			<a href="<?php echo INC_URL ?>/lib/download_comment_attachment.php?id=<?php echo $comment_id ?>&idx=<?php echo $k ?>" class="label label-info" style="color: white;">
                                		<?php echo $file_name ?>
                                	</a> &nbsp;
                        <?php 
                            }
                        }
                        ?>
								</div>
							</div>
				<?php 
					}
				}
				?>
						</div>
					</div>
					
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
		$(function () {
			$("#btn-cancel").click(function() {
				history.back();
			});

			// 댓글 삭제
			$(".btn-delete").click(function(e) {
				if (!confirm("삭제하시겠습니까?")) {
					return;
				}
				
				var id = $(this).closest(".article-comment").attr("id").replace(/\D/g, "");
				
				$.ajax({
					type : "POST",
					url : "./ajax/comment-delete.php",
					data : {
						"id" : id
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>