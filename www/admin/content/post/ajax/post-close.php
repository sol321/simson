<?php
/*
 * Desc: post_state > close
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '글에 대한 정보를 알 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$post_id = $_POST['id'];

$result = if_close_post($post_id);

if (empty($result)) {
    $code = 201;
    $msg = '삭제하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>