<?php
/*
 * Desc: 댓글 삭제
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 102;
    $msg = '댓글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$comment_id = $_POST['id'];

// 첨부파일 조횐
$row = if_get_post_comment_by_id($comment_id);

$meta_data = $row['meta_data'];
$jdec = json_decode($meta_data, true);
$file_attachment = $jdec['file_attachment'];

if (!empty($file_attachment)) {
    foreach ($file_attachment as $key => $val) {
        $file_path = $val['file_path'];
        @unlink($file_path);
    }
}

$result = if_delete_post_comment($comment_id);

if (empty($result)) {
    $code = 201;
    $msg = '삭제하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);
?>