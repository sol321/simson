<?php
/*
 * Desc: Post 편집
 *  if_posts
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = 'Restricted Area - Admin Only -';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_id'])) {
    $code = 102;
    $msg = '게시글에 대한 정보가 필요합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_title'])) {
    $code = 103;
    $msg = '제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_update_post();

if (empty($result)) {
    $code = 201;
    $msg = '수정하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>