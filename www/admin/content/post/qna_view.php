<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-board.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('글에 대한 정보가 필요합니다.');
}

/* 게시글에 대한 정보 */

$post_id = $_GET['id'];
$post_row = if_get_post($post_id);

if (empty($post_row)) {
    if_js_alert_back('게시글이 존재하지 않습니다.');
}

// $post_row -> variable 변수 할당
foreach ($post_row as $key => $val) {
    ${"col_$key"} = $val;
}

// JSON 
$col_meta_data = json_decode($col_meta_data, true);
$file_attachment = empty($col_meta_data['file_attachment']) ? '' : $col_meta_data['file_attachment'];
$thumb_attachment = empty($col_meta_data['thumb_attachment']) ? '' : $col_meta_data['thumb_attachment'];

// 게시판 정보
$board_row = if_get_board($col_template_id);
$tpl_id = $board_row['seq_id'];
$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1><?php echo $tpl_name ?></h1>
				</div>
				<div class="content">
    				<div class="box box-info">
        				<div class="box-header with-border">
        					<h3 class="box-title">상세 보기</h3>
        				</div>
        				<!-- /.box-header -->

    					<div class="box-body">
							<div class="form-group">
								<label>글쓴이</label>
								<div>
									<?php echo $col_post_name ?>
								</div>
							</div>
							<div class="form-group">
								<label>제목</label>
								<div>
									<?php echo $col_post_title ?>
								</div>
							</div>
							<div class="form-group">
								<label>내용</label>
								<?php echo $col_post_content ?>
							</div>
							<div class="form-group">
								<ul class="list-group" id="preview-thumb">
					<?php
					if ( !empty($thumb_attachment) ) {
						foreach ( $thumb_attachment as $key => $val ) {
							$thumb_path = $val['file_path'];
							$thumb_url = $val['file_url'];
							$thumb_name = $val['file_name'];
							if (is_file($thumb_path)) {
					?>
									<li class="list-group-item list-group-item-info">
										<input name="thumb_path[]" value="<?php echo $thumb_path ?>" type="hidden">
										<input name="thumb_url[]" value="<?php echo $thumb_url ?>" type="hidden">
										<input name="thumb_name[]" value="<?php echo $thumb_name ?>" type="hidden">
										<span class="badge"><span class="glyphicon glyphicon-remove hide-thumb-attach" style="cursor: pointer;"></span></span>
										<a href="<?php echo INC_URL ?>/lib/download_post_thumb.php?id=<?php echo $post_id ?>&idx=<?php echo $key ?>" class="btn btn-info btn-xs">
											<?php echo $thumb_name ?>
										</a>
									</li>
					<?php
							}
						}
					}
					?>
								</ul>
							</div>
    					</div>
    					<!-- /.box-body -->
    					<div class="box-footer text-center">
    						<button type="button" id="btn-cancel" class="btn btn-info">리스트로</button> &nbsp;
    					</div>
    					<!-- /.box-footer -->
        				
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
    	<script>
    	$(function () {
    		$("#btn-cancel").click(function() {
    			history.back();
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>