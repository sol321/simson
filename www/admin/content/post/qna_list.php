<?php
/*
 * Desc: Q & A 관리
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-board.php';
require_once FUNC_PATH . '/functions-post.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['bt'])) {
    if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}

/* 게시판에 대한 정보 */
$tpl_id = $_GET['bt'];
$board_row = if_get_board($tpl_id);

if (empty($board_row)) {
    if_js_alert_back('사용할 수 있는 게시판이 존재하지 않습니다.');
}

$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];
/* --게시판에 대한 정보 */

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 9;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if (!empty($q)) {
    $sql = " AND template_id = ? AND (post_content LIKE ? OR post_title LIKE ? OR post_name LIKE ?) ";
    array_push($sparam, $tpl_id, '%' . $q . '%', '%' . $q . '%', '%' . $q . '%');
} else {
    $sql = " AND template_id = ? ";
    array_push($sparam, $tpl_id);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_posts'] . "
		WHERE
			post_state = 'open' AND
            post_type = 'qna_new' AND
            post_type_secondary = 'question'
			$sql
		ORDER BY
			post_order DESC,
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();
			
require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						<?php echo $tpl_name ?>
						<a href="post_add.php?bt=<?php echo $tpl_id ?>" class="btn btn-info btn-xs">
							새 글 등록
						</a>						
					</h1>
				</div>
				<div class="content">
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
        					<div class="box-tools">
        						<form class="form-inline">
									<input type="hidden" name="bt" value="<?php echo $tpl_id ?>">
									<div class="input-group">
										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="Search">
										<div class="input-group-btn">
											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
										</div>
									</div>
								</form>
        					</div>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>제목</th>
										<th>작성자</th>
										<th>등록날짜</th>
        								<th>-</th>
    								</tr>
    				<?php
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$post_id = $val['seq_id'];
							$post_title = htmlspecialchars($val['post_title']);
							$post_date = $val['post_date'];
							$post_name = htmlspecialchars($val['post_name']);
							$post_order = $val['post_order'];
							$notice_label = empty($post_order) ? '' : '<span class="label label-danger">필독</span>';
							
							$reply_row = if_get_answer_post($post_id);
							
							if (!empty($reply_row)) {
							    $reply_id = $reply_row['seq_id'];
							    $reply_title = $reply_row['post_title'];
							    $reply_name = $reply_row['post_name'];
							    $reply_date = $reply_row['post_date'];
							    $reply_btn = '';
							} else {
							    $reply_btn = '<a href="qna_reply.php?id=' . $post_id .'" class="badge bg-yellow">답변등록</a>';
							}
						?>
									<tr id="post-id-<?php echo $post_id ?>">
										<td><?php echo $list_no ?></td>
										<td>
											<?php echo $notice_label;?> <a href="qna_view.php?id=<?php echo $post_id ?>"><?php echo $post_title ?></a>
										</td>
										<td><?php echo $post_name ?></td>
										<td><?php echo $post_date ?></td>
										<td>
											<a href="post_edit.php?id=<?php echo $post_id ?>&bt=<?php echo $tpl_id ?>" class="badge bg-aqua">편집</a>
											<?php echo $reply_btn ?>
                							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
										</td>
									</tr>
						<?php 
						if (!empty($reply_row)) {
						?>
									<tr id="post-id-<?php echo $reply_id ?>" class="active">
										<td></td>
										<td>
											└ &nbsp;
											<a href="qna_view.php?id=<?php echo $reply_id ?>"><?php echo $reply_title ?></a>
										</td>
										<td><?php echo $reply_name ?></td>
										<td><?php echo $reply_date ?></td>
										<td>
											<a href="javascript:;" class="badge bg-red action-delete">삭제</a> &nbsp;
										</td>
									</tr>
						<?php 
						}
						?>
									
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script>
    	$(function() {
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/post-close.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>