<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';
require_once FUNC_PATH . '/functions-board.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('글에 대한 정보가 필요합니다.');
}

/* 게시글에 대한 정보 */

$post_id = $_GET['id'];
$post_row = if_get_post($post_id);

if (empty($post_row)) {
    if_js_alert_back('게시글이 존재하지 않습니다.');
}

// $post_row -> variable 변수 할당
foreach ($post_row as $key => $val) {
    ${"col_$key"} = $val;
}

// JSON 
$col_meta_data = json_decode($col_meta_data, true);
$file_attachment = empty($col_meta_data['file_attachment']) ? '' : $col_meta_data['file_attachment'];
$thumb_attachment = empty($col_meta_data['thumb_attachment']) ? '' : $col_meta_data['thumb_attachment'];

// 게시판 정보
$board_row = if_get_board($col_template_id);
$tpl_id = $board_row['seq_id'];
$tpl_name = $board_row['tpl_name'];
$tpl_skin = $board_row['tpl_skin'];
$meta_data = $board_row['meta_data'];
$tpl_max_filesize = if_get_val_from_json($meta_data, 'tpl_max_filesize');   // MB
$max_file_size = $tpl_max_filesize * 1048576;

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1><?php echo $tpl_name ?></h1>
				</div>
				<div class="content">
    				<div class="box box-info">
        				<div class="box-header with-border">
        					<h3 class="box-title">답변 등록</h3>
        				</div>
        				<!-- /.box-header -->
        				<!-- form start -->
        				<form id="form-post-reply">
        					<!-- MAX_FILE_SIZE must precede the file input field -->
        					<input type="hidden" name="MAX_FILE_SIZE" id="max-file-size" value="<?php echo $max_file_size ?>">
        					<input type="hidden" name="tpl_id" id="tpl_id" value="<?php echo $tpl_id ?>">
        					<input type="hidden" name="post_parent" value="<?php echo $post_id ?>">
    						<input type="hidden" name="post_type" value="<?php echo $col_post_type ?>">
    						<input type="hidden" name="post_type_secondary" value="answer">
        					
        					<div class="box-body">
								<div class="form-group">
									<label>글쓴이</label>
									<input type="text" name="post_name" id="post_name" required class="form-control" placeholder="글쓴이" value="<?php echo if_get_current_admin_name() ?>">
								</div>
								<div class="form-group">
									<label>제목</label>
									<input type="text" id="post_title" name="post_title" class="form-control" placeholder="제목" value="[답변] <?php echo $col_post_title ?>">
								</div>
								<div class="form-group">
									<label>문의 내용</label>
									<div class="alert alert-info">
										<?php echo $col_post_content ?>
									</div>
								</div>
								<div class="form-group">
									<label>답변</label>
									<textarea id="post_content" name="post_content">
									문의하신 내용에 대한 답변을 드립니다.<hr>
										<?php echo $col_post_content ?>
									</textarea>
								</div>
								<div class="form-group">
									<div style="margin-top: 5px;">
										<input type="file" name="attachment[]" id="thumb-attach" class="hide">
									</div>
									<ul class="list-group" id="preview-thumb">
						<?php
						if ( !empty($thumb_attachment) ) {
							foreach ( $thumb_attachment as $key => $val ) {
								$thumb_path = $val['file_path'];
								$thumb_url = $val['file_url'];
								$thumb_name = $val['file_name'];
								if (is_file($thumb_path)) {
						?>
										<li class="list-group-item list-group-item-info">
											<input name="thumb_path[]" value="<?php echo $thumb_path ?>" type="hidden">
											<input name="thumb_url[]" value="<?php echo $thumb_url ?>" type="hidden">
											<input name="thumb_name[]" value="<?php echo $thumb_name ?>" type="hidden">
											<span class="badge"><span class="glyphicon glyphicon-remove hide-thumb-attach" style="cursor: pointer;"></span></span>
											<a href="<?php echo INC_URL ?>/lib/download_post_thumb.php?id=<?php echo $post_id ?>&idx=<?php echo $key ?>" class="btn btn-info btn-xs">
												<?php echo $thumb_name ?>
											</a>
										</li>
						<?php
								}
							}
						}
						?>
									</ul>
								</div>
								<div class="form-group">
									<div class="btn btn-success btn-upload-attach">
										<i class="fa fa-upload" aria-hidden="true"></i> 파일 첨부
									</div>
									<span>최대 <?php echo $tpl_max_filesize ?> MB (<?php echo number_format($max_file_size) ?> bytes)</span>
									<div style="margin-top: 5px;">
										<input type="file" name="attachment[]" id="file-attach" class="hide" multiple>
									</div>
									<ul class="list-group" id="preview-attachment">
						<?php
						if ( !empty($file_attachment) ) {
							foreach ( $file_attachment as $key => $val ) {
								$attach_file_path = $val['file_path'];
								$attach_file_url = $val['file_url'];
								$attach_file_name = $val['file_name'];
								if (is_file($attach_file_path)) {
						?>
										<li class="list-group-item list-group-item-success">
											<input name="file_path[]" value="<?php echo $attach_file_path ?>" type="hidden">
											<input name="file_url[]" value="<?php echo $attach_file_url ?>" type="hidden">
											<input name="file_name[]" value="<?php echo $attach_file_name ?>" type="hidden">
											<span class="badge"><span class="glyphicon glyphicon-remove hide-file-attach" style="cursor: pointer;"></span></span>
											<a href="<?php echo INC_URL ?>/lib/download_post_attachment.php?id=<?php echo $post_id ?>&idx=<?php echo $key ?>" class="btn btn-success btn-xs">
												<?php echo $attach_file_name ?>
											</a>
										</li>
						<?php
								}
							}
						}
						?>
									</ul>
								</div>
        					</div>
        					<!-- /.box-body -->
        					<div class="box-footer text-center">
        						<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
        					</div>
        					<!-- /.box-footer -->
        				</form>
        			</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- Numeric -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>

    	<script>
    	$(function () {
    		// CKEditor file upload
    		CKEDITOR.replace("post_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?"
    	    });

    	    //-------------------- 첨부파일 시작
    		// 첨부파일 trigger
    		$(".btn-upload-attach").click(function() {
				$("#file-attach").click();
			});

    		// 첨부파일 Upload 시작
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();

				if (fsize > 10485760) {
					alert("최대 10M 파일까지 업로드할 수 있습니다.");
					return;
				}

				$("#form-post-reply").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
													'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
													'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작

			// 첨부파일 삭제
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 첨부파일 삭제
			
			// 첨부파일 숨기기
			$(document).on("click", ".hide-file-attach", function() {
				$(this).closest("li").fadeOut("slow", function() { $(this).remove(); });
			});
			//-- 첨부파일 숨기기
			//-------------------- 첨부파일 끝
			
			//-------------------- 대표이미지 시작
    		// 대표이미지 trigger
    		$(".btn-upload-thumb").click(function() {
				$("#thumb-attach").click();
			});

    		// 대표이미지 Upload 시작
			$("#thumb-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
				var max_file_size = $("#max-file-size").val();

				if (fsize > max_file_size) {
					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
					return;
				}

				$("#form-post-reply").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists = '<li class="list-group-item list-group-item-info">' +
													'<input type="hidden" name="thumb_path[]" value="' + xhr.file_path[i] + '">' +
													'<input type="hidden" name="thumb_url[]" value="' + xhr.file_url[i] + '">' +
													'<input type="hidden" name="thumb_name[]" value="' + xhr.file_name[i] + '">' +
													'<span class="badge"><span class="glyphicon glyphicon-remove delete-thumb-attach" style="cursor: pointer;"></span></span>' +
													xhr.file_name[i] +
												'</li>';
							}
							$("#preview-thumb").html(fileLists);
						} else {
							alert(xhr.msg);
						}
						$('input[name="attachment[]"]').val("");
					}
				});
			});

			// 대표이미지 삭제
			$(document).on("click", ".list-group-item .delete-thumb-attach", function() {
				var file = $(this).parent().parent().find('input[name="thumb_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-thumb li").each(function(idx) {
							var file = $(this).find('input[name="thumb_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});
			//-- 대표이미지 삭제
			
			// 대표이미지 숨기기
			$(document).on("click", ".hide-thumb-attach", function() {
				$(this).closest("li").fadeOut("slow", function() { $(this).remove(); });
			});
			//-- 대표이미지 숨기기 */
			//-------------------- 대표이미지 끝

			// 폼 전송
    		$("#form-post-reply").submit(function(e) {
        		e.preventDefault();
        		CKEDITOR.instances.post_content.updateElement();
//         		var postContent = CKEDITOR.instances.post_content.getData();
        	});

    		$("#form-post-reply").validate({
    			rules: {
    				post_title: {
    					required: true
    				}
    			},
    			messages: {
    				post_title: {
    					required: "제목을 입력해 주십시오."
    				}
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/qna-reply.php",
    					data : $("#form-post-reply").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							location.href = "qna_list.php?bt=<?php echo $tpl_id ?>";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    
    		$("#btn-cancel").click(function() {
    			history.back();
    		});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>