<?php
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['jt'])) {
    if_js_alert_back('게시판에 대한 정보가 필요합니다.');
}

/* 학술지 종류에 대한 정보 */
$jn_type = $_GET['jt'];

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    $sql .= " AND $qa1 LIKE ?";
    array_push($sparam, '%' . $qs1 . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_journal'] . "
		WHERE
			jn_type = '$jn_type'
			$sql
		ORDER BY
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						<?php echo strtoupper($jn_type) ?>
						<a href="journal_add.php?jt=<?php echo $jn_type ?>" class="btn btn-info btn-sm">
							학회지 등록
						</a>
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<input type="hidden" name="jt" value="<?php echo $jn_type ?>">
    							<div class="row">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="author" <?php echo strcmp($qa1, 'author') ? '' : 'selected'; ?>>저자(Author)</option>
											<option value="j_title" <?php echo strcmp($qa1, 'j_title') ? '' : 'selected'; ?>>제목(Title)</option>
											<option value="j_keyword" <?php echo strcmp($qa1, 'j_keyword') ? '' : 'selected'; ?>>키워드(Keyword)</option>
											<option value="j_abstract" <?php echo strcmp($qa1, 'j_abstract') ? '' : 'selected'; ?>>초록(Abstract)</option>
										</select>
									</div>
									<div class="col-md-6">
    									<div class="col-md-6">
    										<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
    									</div>
    									<div class="col-md-6">
    										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
    										<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        								</div>
    								</div>
								</div>
							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>Vol.<br>No.</th>
										<th>논문제목 <br><small><i>Keyword</i></small></th>
										<th>게시일<br>Page</th>
										<th>작성자</th>
										<th></th>
    								</tr>
    				<?php
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$journal_id = $val['seq_id'];
							$jn_vol = $val['jn_vol'];
							$jn_no = $val['jn_no'];
							$jn_year = $val['jn_year'];
							$jn_month = $val['jn_month'];
							$author = $val['author'];
							$j_title = $val['j_title'];
							$j_keyword = $val['j_keyword'];
							
							$meta_data = $val['meta_data'];
							$jdec = json_decode($meta_data, true);
							$page_from = $jdec['page_from'];
							$page_to = $jdec['page_to'];
							
						?>
									<tr id="post-id-<?php echo $journal_id ?>">
										<td><?php echo $list_no ?></td>
										<td>
											<b><?php echo $jn_vol ?></b>권 <br>
											<b><?php echo $jn_no ?></b>호
										<td>
											<?php echo if_cut_str($j_title, 60) ?><br>
											<small><i><?php echo $j_keyword ?></i></small>
										</td>
										<td>
											<?php echo $jn_year ?>년 <?php echo $jn_month ?>월<br>
											<?php echo $page_from ?>~<?php echo $page_to ?> page
										</td>
										<td><?php echo $author ?></td>
										<td>
											<a href="journal_edit.php?id=<?php echo $journal_id ?>" class="btn btn-primary btn-xs">편집</a> &nbsp;
											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/journal-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>