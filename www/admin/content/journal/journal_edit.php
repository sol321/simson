<?php
/*
 * Desc: 학회지 편집
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('학회지에 대한 정보가 필요합니다.');
}

$seq_id = $_GET['id'];

$jn_row = if_get_journal_by_id($seq_id);

$jn_vol = $jn_row['jn_vol'];
$jn_no = $jn_row['jn_no'];
$jn_year = $jn_row['jn_year'];
$jn_month = $jn_row['jn_month'];
$jn_type = $jn_row['jn_type'];
$author = $jn_row['author'];
$j_title = $jn_row['j_title'];
$j_keyword = $jn_row['j_keyword'];
$j_abstract = $jn_row['j_abstract'];

$meta_data = $jn_row['meta_data'];
$jdec = json_decode($meta_data, true);
$page_from = $jdec['page_from'];
$page_to = $jdec['page_to'];
$file_attachment = $jdec['file_attachment'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>
	
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content">
					<div class="row">
                    	<div class="col-md-12">
                        	<!-- form start -->
                            <form id="form-item-new" class="form-horizontal">
                            	<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $seq_id ?>">
                            	<input type="hidden" name="jt" id="jt" value="<?php echo $jn_type ?>">
                        		<div class="box box-primary">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-primary"><?php echo strtoupper($jn_type) ?> 수정</h3>
                        			</div>
                        			<div class="box-body">
                        				<div class="form-group">
                        					<label class="col-md-2 control-label">게시월/권/호/page</label>
                        					<div class="col-md-2">
                        						<div class="input-group">
        											<input type="text" id="jn_year_month" name="jn_year_month" class="form-control" value="<?php echo $jn_year ?>-<?php echo $jn_month ?>">
        											<div class="input-group-addon">
        												<i class="fa fa-calendar"></i>
        											</div>
        										</div>
                        					</div>
                        					<div class="col-md-3">
                        						<div class="input-group">
            										<input type="text" id="jn_vol" name="jn_vol" class="form-control numeric" value="<?php echo $jn_vol ?>">
            										<div class="input-group-addon">권 (Vol.)</div>
            										<input type="text" id="jn_no" name="jn_no" class="form-control numeric" value="<?php echo $jn_no ?>">
            										<div class="input-group-addon">호 (No.)</div>
            									</div>
        									</div>
                        					<div class="col-md-3">
                        						<div class="input-group">
            										<input type="text" id="page_from" name="page_from" class="form-control numeric" value="<?php echo $page_from ?>">
            										<div class="input-group-addon">~</div>
            										<input type="text" id="page_to" name="page_to" class="form-control numeric" value="<?php echo $page_to ?>">
            										<div class="input-group-addon">page</div>
            									</div>
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">저자</label>
        									<div class="col-md-8">
        										<input type="text" name="author" id="author" required class="form-control" placeholder="Author" value="<?php echo $author ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">제목</label>
        									<div class="col-md-8">
        										<input type="text" id="j_title" name="j_title" class="form-control" placeholder="Title" value="<?php echo $j_title ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">키워드</label>
        									<div class="col-md-8">
        										<input type="text" id="j_keyword" name="j_keyword" class="form-control" placeholder="Keyword" value="<?php echo $j_keyword ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">초록</label>
        									<div class="col-md-8">
        										<textarea id="j_abstract" name="j_abstract" class="form-control" rows="10" placeholder="Abstract"><?php echo $j_abstract ?></textarea>
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">파일</label>
        									<div class="col-md-8">
        										<div class="btn btn-default btn-file">
            										<i class="fa fa-paperclip"></i> 첨부파일
            										<input type="file" name="attachment[]" id="file-attach" multiple>
            									</div>
            									
            									<ul class="list-group" id="preview-attachment">
                    				<?php
            						if (!empty($file_attachment)) {
            							foreach ($file_attachment as $key => $val) {
            								$file_path = $val['file_path'];
            								$file_url = $val['file_url'];
            								$file_name = $val['file_name'];
            								if (is_file($file_path)) {
            						?>
            										<li class="list-group-item list-group-item-info">
            											<input type="hidden" name="file_path[]" value="<?php echo $file_path ?>">
            											<input type="hidden" name="file_url[]" value="<?php echo $file_url ?>">
            											<input type="hidden" name="file_name[]" value="<?php echo $file_name ?>">
            											<span class="badge"><span class="glyphicon glyphicon-remove hide-file-attach" style="cursor: pointer;"></span></span>
            											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $file_name ?>">
            												<?php echo $file_name ?>
            											</a>
            										</li>
            						<?php
            								}
            							}
            						}
            						?>
                    							</ul>
        									</div>
        								</div>
                        			</div>
                        			<div class="box-footer text-center">
        								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
                              		</div>
                        		</div>
                      		</form>
                    	</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		
		<script>
		$(function () {
			$(".numeric").numeric({ negative: false });
			
			// https://bootstrap-datepicker.readthedocs.io/en/latest/i18n.html
			$.fn.datepicker.dates['en'] = {
    			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    			monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    			today: "Today",
    			clear: "Clear",
    			format: "mm/dd/yyyy",
    			titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    			weekStart: 0
			};

			// 연월 선택
			$('#jn_year_month').datepicker({
				format: "yyyy-mm",
				viewMode: "months", 
				minViewMode: "months",
				autoclose: true
		    });

			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/journal-edit.php",
					data : $("#form-item-new").serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "journal_list.php?jt=" + $("#jt").val();
							} else {
								alert("학회지를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) { 
						if (xhr.code == "0") { 
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists += '<li class="list-group-item list-group-item-success">' +
    												'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
    												'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
    												'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
    												'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
    												xhr.file_name[i] +
    											'</li>';
							}
							$("#preview-attachment").append(fileLists);
						} else {
							alert(xhr.msg);
						}
						$("#file-attach").val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작
			
			// 파일 삭제 
			$(document).on("click", ".list-group-item .delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment li").each(function(idx) {
							var file = $(this).find('input[name="file_path[]"]').val();
							if (file == res.file_path) {
								$(this).fadeOut("slow", function() { $(this).remove(); });
							}
						});
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});

			// 첨부파일 숨기기
			$(document).on("click", ".hide-file-attach", function() {
				$(this).closest("li").fadeOut("slow", function() { $(this).remove(); });
			});
			//-- 첨부파일 숨기기
			// -- 첨부 파일 끝
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>