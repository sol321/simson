<?php
/*
 * Desc: 학회지 완전 삭제 (delete)
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '학회지를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['id'];

$jn_row = if_get_journal_by_id($seq_id);

// 첨부 파일 삭제
$meta_data = $jn_row['meta_data'];
$jdec = json_decode($meta_data, true);
$file_attachment = $jdec['file_attachment'];
if (!empty($file_attachment)) {
    foreach ($file_attachment as $key => $val) {
        @unlink($val['file_path']);
    }
}

$result = if_delete_journal($seq_id);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>