<?php
/*
 * Desc: 학회지 등록
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-journal.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['jt'])) {
    $code = 101;
    $msg = '학회지 종류를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['jn_year_month'])) {
    $code = 103;
    $msg = '게시월을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['jn_vol'])) {
    $code = 104;
    $msg = '권(Vol)을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['jn_no'])) {
    $code = 105;
    $msg = '호(No)를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['page_from'])) {
    $code = 106;
    $msg = 'page(from)를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['page_to'])) {
    $code = 107;
    $msg = 'page(to)를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['author'])) {
    $code = 108;
    $msg = '저자를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['j_title'])) {
    $code = 110;
    $msg = '제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['j_keyword'])) {
    $code = 111;
    $msg = '키워드를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['j_abstract'])) {
    $code = 112;
    $msg = '초록을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_journal();

if (empty($result)) {
    $code = 201;
    $msg = '학회지를 등록할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>