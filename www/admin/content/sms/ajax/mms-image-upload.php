<?php
/*
 * Desc : MMS 포토이미지 파일 업로드
 * 		
 */
require_once '../../../../if-config.php';
require_once INC_PATH . '/classes/Thumbnail.php';

$code = 0;
$msg = '';

$ym = date('Ym');
$tmp_up_dir = UPLOAD_PATH . '/tmp/' . $ym;
$tmp_up_url = UPLOAD_URL . '/tmp/' . $ym;

if (!is_dir($tmp_up_dir)) {
    mkdir($tmp_up_dir, 0777, true);
}

if (strcmp($_FILES['attachment_photo']['type'], 'image/jpeg')) {
    $code = 101;
    $msg = '이미지 파일의 확장자는 jpg만 가능합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if ($_FILES['attachment_photo']['size'] > (500 * 1024)) {
    $code = 102;
    $msg = '500KB 이하 파일만 전송하실 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$file_ext = strtolower(if_get_file_extension($_FILES['attachment_photo']['name']));
$new_file_name = if_make_rand() . '.' . $file_ext;

$upload_path = $tmp_up_dir . '/' . $new_file_name;
$result = move_uploaded_file($_FILES['attachment_photo']['tmp_name'], $upload_path);

if ($result) {
    $thumbnail = new Thumbnail();
    
    $file_path = $tmp_up_dir . '/' . $new_file_name;    // 원본
    
    // Thumbnail
    $thumb_suffix = '-thumb';
    $thumb_width = 600;
    $thumb_name = $thumbnail->resize_image($file_path, $thumb_suffix, $thumb_width);
    $thumb_path = $tmp_up_dir . '/' . $thumb_name;
    $thumb_url = $tmp_up_url . '/' . $thumb_name;
    
    if (stripos($thumb_name, $thumb_suffix) !== false) {	// thumbnail을 생성한 경우
        @unlink($file_path);	// 업로드한 원본 이미지 삭제
    }
} else {
    $code = 505;
    $msg = '파일을 업로드할 수 없습니다. 관리자에게 문의해 주십시오.';
}

$json = compact('code', 'msg', 'thumb_url', 'thumb_path');
echo json_encode($json);

?>