<?php
/*
 * Desc : 받는 사람 휴대폰번호 엑셀에서 추출
 *
 */
require_once '../../../../if-config.php';

$code = 0;
$msg = '';

$tmp_up_dir = UPLOAD_PATH . '/tmp';
$tmp_up_url = UPLOAD_URL . '/tmp';

if (!is_dir($tmp_up_dir)) {
	mkdir($tmp_up_dir, 0777, true);
}

if (stripos($_FILES['attachment']['type'], 'application/vnd') === false) {
	$code = 101;
	$msg = '엑셀 포멧이 아닙니다. 다시 확인해 주십시오.';
	$json = compact('code', 'msg');
	exit(json_encode($json));
}

$file_ext = strtolower(if_get_file_extension($_FILES['attachment']['name']));
$new_file_name = if_make_rand() . '.' . $file_ext;

$upload_file = $tmp_up_dir . '/' . $new_file_name;
$result = move_uploaded_file($_FILES['attachment']['tmp_name'], $upload_file);

$mobile = '';

if ($result) {
	if (!strcmp($file_ext, 'xls')) {
	    require_once INC_PATH . '/classes/PHPExcel.php';
	    
	    $objPHPExcel = new PHPExcel();
	    $objReader = new PHPExcel_Reader_Excel5();
	    $objReader->setReadDataOnly(true);
	    $objPHPExcel = $objReader->load($upload_file);
	    $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
	    $sheet = $objPHPExcel->getActiveSheet();
	    $array_data = array();
	    
	    foreach ($rowIterator as $row) {
	        $rowIndex = $row->getRowIndex();
	        $array_data[$rowIndex] = array('A'=>'');
	        
	        $cell = $sheet->getCell('A' . $rowIndex);
	        $array_data[$rowIndex]['A'] = $cell->getValue();
	    }
	    
	    if (!empty($array_data)) {
	        foreach ($array_data as $key => $val) {
	            if (if_validate_mobile_number($val['A'])) {
	                $mobile .= $val['A'] . "\n";
	            }
	        } // foreach
	    }
	} else if (!strcmp($file_ext, 'xlsx')) {
	    require_once INC_PATH . '/classes/SimpleXLSX.php';
	    
	    $xlsx = new SimpleXLSX($upload_file);
	    $array_data = $xlsx->rows();
	    
	    if (!empty($array_data)) {
	        foreach ($array_data as $key => $val) {    // First row index : 0
                if (if_validate_mobile_number($val[0])) {
                   $mobile .= $val[0] . "\n";
                }
	        } // foreach
	    }
	}
	@unlink($upload_file);	// 업로드한 원본 파일 삭제
}

$json = compact('code', 'msg', 'mobile');
echo json_encode($json);

?>