<?php
require_once '../../../if-config.php';

if_authenticate_admin();

require_once ADMIN_PATH . '/include/cms-header.php';
?>

		<script src="<?php echo ADMIN_INC_URL ?>/js/sms.js"></script>
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						메시지 보내기
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<form id="form-item-new">
    					<input type="hidden" name="msg_type" id="msg_type" value="mt-sms">
    						<div class="col-md-3">
    							<div class="box box-success">
    								<div class="box-header">
    									<button type="button" id="mt-sms" class="btn btn-msg-type bg-orange btn-xs">단문(SMS)</button>
    									<button type="button" id="mt-lms" class="btn btn-msg-type bg-purple btn-xs">장문(LMS)</button>
    									<button type="button" id="mt-mms" class="btn btn-msg-type bg-purple btn-xs">포토(MMS)</button>
    								</div>
                                	<div class="box-body">
                                		<div class="form-group has-success hide" id="msg-title-wrap">
                                			<label>제목</label>
                							<input type="text" id="msg_title" name="msg_title" class="form-control">
                						</div>
                                		<div class="form-group has-success">
                                			<label>메시지</label>
                                			<textarea name="message" id="message" class="form-control" style="height: 180px;"></textarea>
                                			<span class="pull-right" id="byte-guage"><b>0</b> / 90</span>
                                		</div>
                                		<div class="form-group hide" id="mms-img-wrap">
                                			<div class="btn btn-info btn-sm btn-file">
                                            	<i class="fa fa-fw fa-image"></i> 이미지 파일(jpg)
                                            	<input type="file" name="attachment_photo" id="attachment_photo">
                                            </div>
                							<small class="help-block">이미지 크기 = 500KB 이하 </small>
                							
                							<div id="attachment_preview" style="margin-top: 10px;"></div>
                							<input type="hidden" name="mms_photo_path" id="mms_photo_path">
                						</div>
                                		<div class="form-group">
                                			<label>발신번호</label>
                                			<input type="text" name="callback" class="form-control" value="" readonly>
                                		</div>
                                	</div><!-- /.box-body -->
                                </div>
        					</div>
    						<div class="col-md-3">
    							<div class="box box-primary">
                                	<div class="box-header">
        								<div class="btn bg-olive btn-xs btn-file">
                                        	<i class="fa fa-fw fa-file-excel-o"></i> 엑셀 업로드
                                        	<input type="file" name="attachment" id="attachment_file">
                                        </div>
                                        <div class="pull-right">
        									<a href="../_files/sample_mobile.xlsx" class="btn btn-info btn-xs">샘플 양식 Download</a>
        								</div>
                                	</div>
                                	<div class="box-body">
                                		<div class="form-group has-success">
                                			<label>휴대전화번호</label>
                                			<textarea name="mobiles" id="mobiles" class="form-control" style="height: 300px;"></textarea>
                                		</div>
                                	</div><!-- /.box-body -->
                                	<div class="box-footer">
                                		<div class="text-center">
                                			<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-fw fa-paper-plane-o"></i> 발송합니다</button>
                                		</div>
                                	</div>	
                                </div>
        					</div>
        				</form>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		
    	<script>
    	$(function() {
        	
        	var sendMsgType = $("#msg_type").val();

			defaultMsgBox(sendMsgType);

			$("#message").on("click keyup keypress input propertychange", function() {
				if (sendMsgType == "mt-sms") {
					if (countBytes($("#message").val()) > 90) {
						if (confirm("장문으로 변경하시겠습니까?")) {
							sendMsgType = "mt-lms";
							$("#msg_type").val(sendMsgType);
							$(".btn-msg-type").removeClass("bg-orange").addClass("bg-purple");
							$("#" + sendMsgType).removeClass("bg-purple").addClass("bg-orange");
							$("#msg-title-wrap").removeClass("hide");
							limitBytes($("#message").val(), 2000);
						} else {
							limitBytes($("#message").val(), 90);
						}
					} else {
						limitBytes($("#message").val(), 90);
					}
				} else {
					limitBytes($("#message").val(), 2000);
				}
			});

			$(".btn-msg-type").click(function() {
				$(".btn-msg-type").removeClass("bg-orange").addClass("bg-purple");
				$(this).removeClass("bg-purple").addClass("bg-orange");
				
				sendMsgType = $(this).attr("id");
    			$("#msg_type").val(sendMsgType);

    			defaultMsgBox(sendMsgType);
			});

			// MMS 이미지 업로드
			$("#attachment_photo").change(function() {
				var size = parseInt(this.files[0].size);
				var fext = this.files[0].name.split('.').pop().toLowerCase();

				if (fext != "jpg" && fext != "jpeg") {
					alert("포토이미지의 파일 확장자는 jpg만 사용해 주십시오.");
					$(this).val("");
					return;
				}

				if (size > (500 * 1024)) {
					alert("포토이미지의 파일 용량은 500KB 미만입니다.");
					$(this).val("");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "./ajax/mms-image-upload.php",
					dataType : "json",
					success: function(xhr) {
						if (xhr.code == "0") {
							var uploadedImages =	'<div class="list-group-item">' +
														'<input type="hidden" name="thumb_path[]" id="mms-file-path" value="' + xhr.thumb_path + '">' +
														'<input type="hidden" name="thumb_url[]" id="mms-file-url" value="' + xhr.thumb_url + '">' +
														'<span class="badge bg-red delete-tmp" style="cursor: pointer;">삭제</span>' +
														'<img src="' + xhr.thumb_url + '" class="preivew-attachment" ' +
															'style="border: 1px solid #eeeeee; max-width: 100%; cursor: pointer;">' +
													'</div>';

							$("#attachment_preview").html(uploadedImages);
							$("#mms_photo_path").val(xhr.thumb_path);
						} else {
							$("#attachment_preview").html("");
							$("#mms_photo_path").val("");
							alert(xhr.msg);
						}
					}
				});
			});

			// File Deletion
			$(document).on("click", ".delete-tmp", function(e) {
				if (confirm("파일을 삭제하시겠습니까?")) {
					$.ajax({
						type : "POST",
						url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
						data : {
							"filePath" : $("#mms-file-path").val()
						},
						dataType : "json",
						success : function(res) {
							if (res.code == "0") {
								$("#attachment_preview").html("");
								$("#mms_photo_path").val("");
							} else {
								alert(res.msg);
							}
						}
					});
				}
			});
			// MMS 이미지 업로드

			// Excel 업로드
			$("#attachment_file").change(function() {
				var fext = this.files[0].name.split('.').pop().toLowerCase();

				if (fext != "xls" && fext != "xlsx") {
					alert("파일 포멧은 xls와 xlsx만 가능합니다.");
					$(this).val("");
					return;
				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "./ajax/add-mobile-from-file.php",
					dataType : "json",
					beforeSend: function() {
						$("#mobiles").val("");
					},
					success: function(res) {
						if (res.code == "0") {
							$("#mobiles").val(res.mobile);
						}
					}
				});
			});
			// Excel 업로드

			// Submit
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/send-msg.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("발송했습니다.");
// 								location.reload();
							} else {
								alert("발송하지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
					$("#btn-submit").prop("disabled", false);
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});
        });

    	function defaultMsgBox(sendMsgType) {
			if (sendMsgType != "mt-sms") {	// LMS, MMS
				$("#msg-title-wrap").removeClass("hide");
				limitBytes($("#message").val(), 2000);
			} else {
				$("#msg-title-wrap").addClass("hide");
				limitBytes($("#message").val(), 90);
			}

			if	(sendMsgType == "mt-mms") {	// MMS
				$("#mms-img-wrap").removeClass("hide");
			} else {
				$("#mms-img-wrap").addClass("hide");
			}
		}
    	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
