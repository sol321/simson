<?php
/*
 * Desc: if_options 에 등록된 내용 조회, 약관 등과 같은 하드 코딩 정보를 다룰 때 사용.
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-term.php';

if_authenticate_admin();

$option_name = 'site_email_from';
$json = if_get_option($option_name);
$option_value = json_decode($json, true);

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1>사이트 이메일 주소</h1>
				</section>
				<section class="content">
					<div class="col-md-6">
    					<form id="form-item-new">
    						<input type="hidden" name="option_name" value="<?php echo $option_name ?>">
    						<div class="box box-success">
    							<div class="box-body">
    								<div class="form-group">
    									<label>이메일</label>
    									<input type="email" name="site_email_from" id="site_email_from" class="form-control" value="<?php echo $option_value ?>">
    								</div>
    							</div><!-- /.box-body -->
    							<div class="box-footer">
    								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> 저장합니다</button>
    							</div>
    						</div><!-- /. box -->
    					</form>
    				</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
    		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    
        	<script>
        	$(function() {
        		$("#form-item-new").validate({
        			rules: {
            			"site_email_from" : {
                			required: true,
                			email: true
                		}
        			},
        			messages: {
        			},
        			submitHandler: function(form) {
        				$.ajax({
        					type : "POST",
        					url : "./ajax/site-email.php",
        					data : $("#form-item-new").serialize(),
        					dataType : "json",
        					beforeSend : function() {
        						$("#btn-submit").prop("disabled", true);
        					},
        					success : function(res) {
        						if (res.code == "0") {
        							if (res.result) {
        								alert("저장했습니다.");
        							} else {
        								alert("저장하지 못했습니다.");
        							}
        						} else {
        							alert(res.msg);
        						}
        					}
        				}).done(function() {
        					$("#btn-submit").prop("disabled", false);
        				}).fail(function() {
        				}).always(function() {
        				}); // ajax
            		}
            	});
            });
        	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>