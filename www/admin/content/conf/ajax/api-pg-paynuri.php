<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_is_admin()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['store_id'])) {
    $code = 101;
    $msg = '가맹점번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['store_name'])) {
    $code = 102;
    $msg = '가맹점명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}
/*
 if (empty($_POST['store_url'])) {
 $code = 103;
 $msg = '가맹점 URL을 입력해 주십시오.';
 $json = compact('code', 'msg');
 exit(json_encode($json));
 }
 */
if (empty($_POST['biz_no'])) {
    $code = 104;
    $msg = '사업자등록번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['crypto_key'])) {
    $code = 105;
    $msg = '가맹점 KEY를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$store_id = $_POST['store_id'];
$store_name = $_POST['store_name'];
$store_url = $_POST['store_url'];
$biz_no = $_POST['biz_no'];
$crypto_key = $_POST['crypto_key'];
$option_name = $_POST['option_name'];

$compact = compact('store_id', 'store_name', 'store_url', 'biz_no', 'crypto_key');
$json = json_encode($compact);
$result = if_update_option_value($option_name, $json);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>