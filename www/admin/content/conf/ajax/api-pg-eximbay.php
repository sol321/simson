<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_is_admin()) {
    $code = 510;
    $msg = '관리자만 사용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mid'])) {
    $code = 101;
    $msg = '가맹점 아이디를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['store_name'])) {
    $code = 102;
    $msg = '가맹점명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['secret_key'])) {
    $code = 105;
    $msg = '가맹점 KEY를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$mid = $_POST['mid'];
$store_name = $_POST['store_name'];
$secret_key = $_POST['secret_key'];
$option_name = $_POST['option_name'];
$request_url = $_POST['request_url'];

$compact = compact('mid', 'store_name', 'secret_key', 'request_url');
$json = json_encode($compact);
$result = if_update_option_value($option_name, $json);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>