<?php
/*
 * Desc: 무통장입금계좌 정보 삭제
 *
 */
require_once '../../../../if-config.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!isset($_POST['id'])) {
    $code = 101;
    $msg = '삭제할 은행을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$idx = $_POST['id'];

$result = if_remove_json_option('if_bank_account_data', 'bank_data', $idx);

if (empty($result)) {
    $code = 201;
    $msg = '삭제할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>