<?php
/*
 * Desc: 무통장입금계좌 정보 저장
 *
 */
require_once '../../../../if-config.php';
// require_once FUNC_PATH . '/functions-payment.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['bank_name'])) {
    $code = 101;
    $msg = '은행명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['bank_account'])) {
    $code = 102;
    $msg = '계좌번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['account_holder'])) {
    $code = 103;
    $msg = '예금주를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$bank_data[0] = $_POST['bank_name'] . "\t" . $_POST['bank_account'] . "\t" . $_POST['account_holder'] . "\t" . date('Y-m-d H:i:s');
$compact = compact('bank_data');
$json = json_encode($compact);

$result = if_preserve_option_value('if_bank_account_data', $json);

if (empty($result)) {
    $code = 201;
    $msg = '저장할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg');
echo json_encode($json);

?>