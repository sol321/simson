<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

foreach ($_POST as $key => $val) {
    $option_value = json_encode(trim($val));
    if_update_option_value($key, $option_value);
}

$json = compact('code', 'msg');
echo json_encode($json);

?>