<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['option_name'])) {
    $code = 101;
    $msg = '아이템 형태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_content'])) {
    $code = 102;
    $msg = '내용을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$option_name = $_POST['option_name'];
$option_value = json_encode($_POST['post_content']);

$result = if_update_option_value($option_name, $option_value);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>