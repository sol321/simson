<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_host']))) {
    $code = 101;
    $msg = '메일 서버 호스트명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_userid']))) {
    $code = 102;
    $msg = '메일 서버 사용자를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['mail_passwd'])) {
    $code = 103;
    $msg = '메일 서버 비밀번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['mail_db_name']))) {
    $code = 104;
    $msg = '메일 데이터베이스명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['hcode']))) {
    $code = 105;
    $msg = 'HCode를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$mail_host = trim($_POST['mail_host']);
$mail_userid = trim($_POST['mail_userid']);
$mail_passwd = $_POST['mail_passwd'];
$mail_db_name = trim($_POST['mail_db_name']);
$hcode = trim($_POST['hcode']);

$compact = compact('mail_host', 'mail_userid', 'mail_passwd', 'mail_db_name', 'hcode');
$json = json_encode($compact);

if_update_option_value('if_mail_svr', $json);

$json = compact('code', 'msg');
echo json_encode($json);

?>