<?php
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['option_name'])) {
    $code = 101;
    $msg = '아이템 형태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['site_email_from'])) {
    $code = 102;
    $msg = '이미일 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$option_name = $_POST['option_name'];
$site_email_from = trim($_POST['site_email_from']);
$option_value = json_encode($site_email_from);

if (!empty($site_email_from)) {
    if (!filter_var($site_email_from, FILTER_VALIDATE_EMAIL)) {
        $code = 117;
        $msg = '유효한 이메일주소가 아닙니다.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_update_option_value($option_name, $option_value);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>