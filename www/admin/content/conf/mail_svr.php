<?php
/*
 * Desc: 메일 서버 설정 
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-term.php';

if_authenticate_admin();

$mail_svr = json_decode(if_get_option('if_mail_svr'), true);

$option_mail_host = @$mail_svr['mail_host'];
$option_mail_userid = @$mail_svr['mail_userid'];
$option_mail_passwd = @$mail_svr['mail_passwd'];
$option_mail_db_name = @$mail_svr['mail_db_name'];
$option_hcode = @$mail_svr['hcode'];

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1>메일 서버 정보</h1>
				</section>
				<section class="content">
					<div class="col-md-6">
    					<form id="form-item-new">
    						<div class="box box-success">
    							<div class="box-body">
    								<div class="form-group">
    									<label>호스트</label>
    									<input type="text" name="mail_host" id="mail_host" class="form-control" value="<?php echo $option_mail_host ?>">
    								</div>
    								<div class="form-group">
    									<label>사용자</label>
    									<input type="text" name="mail_userid" id="mail_userid" class="form-control" value="<?php echo $option_mail_userid ?>">
    								</div>
    								<div class="form-group">
    									<label>비밀번호</label>
    									<input type="text" name="mail_passwd" id="mail_passwd" class="form-control" value="<?php echo $option_mail_passwd ?>">
    								</div>
    								<div class="form-group">
    									<label>DB명</label>
    									<input type="text" name="mail_db_name" id="mail_db_name" class="form-control" value="<?php echo $option_mail_db_name ?>">
    								</div>
    								<div class="form-group">
    									<label>HCode</label>
    									<input type="text" name="hcode" id="hcode" class="form-control" value="<?php echo $option_hcode ?>">
    								</div>
    							</div><!-- /.box-body -->
    							<div class="box-footer">
    								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> 저장합니다</button>
    							</div>
    						</div><!-- /. box -->
    					</form>
    				</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
        	<script>
        	$(function() {
        		$("#form-item-new").submit(function(e) {
    				e.preventDefault();

    				$.ajax({
    					type : "POST",
    					url : "./ajax/mail-svr.php",
    					data : $(this).serialize(),
    					dataType : "json",
    					beforeSend : function() {
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							alert("저장했습니다.");
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    				}); // ajax
    			});
            });
        	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>