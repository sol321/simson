<?php
/*
 * Desc: if_options 에 등록된 내용 조회
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-term.php';

if_authenticate_admin();

$option_site_name = json_decode(if_get_option('site_name'), true);      // 사이트명
$option_site_email_from = json_decode(if_get_option('site_email_from'), true);      // 발신자 이메일주소
$option_site_email_return = json_decode(if_get_option('site_email_return'), true);      // 회신 이메일주소
$option_site_admin_email = json_decode(if_get_option('site_admin_email'), true);    // 관리자 이메일 주소
$option_site_biz_no = json_decode(if_get_option('site_biz_no'), true);    // 사업자등록번호

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1>사이트 기본 정보</h1>
				</section>
				<section class="content">
					<div class="col-md-6">
    					<form id="form-item-new">
    						<div class="box box-success">
    							<div class="box-body">
    								<div class="form-group">
    									<label>사이트 이름</label>
    									<input type="text" name="site_name" id="site_name" class="form-control" value="<?php echo $option_site_name ?>">
    								</div>
    								<div class="form-group">
    									<label>발신자 이메일</label>
    									<input type="email" name="site_email_from" id="site_email_from" class="form-control" value="<?php echo $option_site_email_from ?>">
    									<div class="help-block">메일 발송 시 사용하는 보내는 사람 메일 주소입니다.</div>
    								</div>
    								<div class="form-group">
    									<label>회신 이메일</label>
    									<input type="email" name="site_email_return" id="site_email_return" class="form-control" value="<?php echo $option_site_email_return ?>">
    									<div class="help-block">메일 발송 후, 수신 서버에서 참조하는 회신 메일 주소입니다.</div>
    								</div>
    								<div class="form-group">
    									<label>관리자 이메일</label>
    									<input type="email" name="site_admin_email" id="site_admin_email" class="form-control" value="<?php echo $option_site_admin_email ?>">
    								</div>
    								<div class="form-group">
    									<label>사업자등록번호</label>
    									<input type="text" name="site_biz_no" id="site_biz_no" class="form-control" value="<?php echo $option_site_biz_no ?>">
    								</div>
    							</div><!-- /.box-body -->
    							<div class="box-footer">
    								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> 저장합니다</button>
    							</div>
    						</div><!-- /. box -->
    					</form>
    				</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
    		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    
        	<script>
    		$("#form-item-new").validate({
    			rules: {
    				"site_email_from" : {
            			email: true
            		},
    				"site_admin_email" : {
            			email: true
            		}
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/site-info.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							alert("저장했습니다.");
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    					$("#btn-submit").prop("disabled", false);
    				}).fail(function() {
    				}).always(function() {
    				}); // ajax
        		}
        	});
        	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>