<?php
/*
 * Desc: 엑심베이(Eximbay) 정보 설정
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

if_authenticate_admin();

$option_name = 'if_api_pg_eximbay';
$json = if_get_option($option_name);
$option_value = json_decode($json, true);

if (!empty($option_value)) {
    $mid = $option_value['mid'];
    $store_name = $option_value['store_name'];
    $secret_key = $option_value['secret_key'];
    $request_url = $option_value['request_url'];
} else {
    $mid = '';
    $store_name = '';
    $secret_key = '';
    $request_url = '';
}

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1>
						Eximbay PG 정보 설정 <small>Eximbay로부터 받은 정보를 설정합니다.</small>
					</h1>
				</section>
				<section class="content">
					<div class="row">
    					<form id="form-item-new">
							<input type="hidden" name="option_name" value="<?php echo $option_name ?>">
							<div class="col-md-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">Eximbay</h3>
										<a href="https://merchant.eximbay.com" target="_blank">상점 관리자 웹사이트</a>
									</div>
									<div class="box-body">
										<div class="form-group">
											<label>가맹점명</label>
											<input type="text" name="store_name" class="form-control" value="<?php echo $store_name ?>">
										</div>
										<div class="form-group">
											<label>가맹점 MID</label>
											<input type="text" name="mid" id="mid" class="form-control" value="<?php echo $mid ?>">
										</div>
										<div class="form-group">
											<label>가맹점 Secret Key</label>
											<input type="text" name="secret_key" class="form-control" value="<?php echo $secret_key ?>">
										</div>
										<div class="form-group">
											<label>Eximbay Request URL</label>
											<input type="text" name="request_url" class="form-control" value="<?php echo $request_url ?>">
											<div class-"help-block">
												<span class="label label-info">TEST</span>https://secureapi.test.eximbay.com/Gateway/BasicProcessor.krp<br>
												<span class="label label-success">REAL</span>https://secureapi.eximbay.com/Gateway/BasicProcessor.krp<br>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-footer">
										<button type="submit" class="btn btn-success" id="btn-submit">
											<i class="fa fa-check"></i> 저장합니다 <span class="hide"><i class="fa fa-refresh fa-spin"></i></span>
										</button>
									</div>
								</div><!-- /. box -->
							</div><!-- /.col -->
						</form>
    				</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
        	<script>
        	$(function() {
    			$("#form-item-new").submit(function(e) {
    				e.preventDefault();

    				$.ajax({
    					type : "POST",
    					url : "./ajax/api-pg-eximbay.php",
    					data : $(this).serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							alert("저정했습니다.");
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}); // form
    		});
        	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>