<?php
/*
 * Desc: if_options 에 등록된 내용 조회, 약관 등과 같은 하드 코딩 정보를 다룰 때 사용.
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';
require_once FUNC_PATH . '/functions-term.php';

if_authenticate_admin();

if (empty($_GET['ct'])) {
    if_js_alert_back('아이템에 대한 정보가 필요합니다.');
}

$content_type = $_GET['ct'];
$json = if_get_option($content_type);
$option_value = json_decode($json, true);

$content_title = if_get_term_name_by_url();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1><?php echo $content_title ?></h1>
				</section>
				<section class="content">
					<form id="form-item-new">
						<input type="hidden" name="option_name" value="<?php echo $content_type ?>">
						
						<div class="box box-success">
							<div class="box-body">
								<div class="form-group">
									<textarea id="post_content" name="post_content"><?php echo $option_value ?></textarea>
								</div>
								<button type="submit" class="btn btn-success" id="btn-submit">
									<i class="fa fa-check"></i> 저장합니다 <span class="hide"><i class="fa fa-refresh fa-spin"></i></span>
								</button>
							</div>
						</div><!-- /. box -->
					</form>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
    	<!-- jQuery Validate Plugin -->
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		
    	<script>
    	$(function () {
    		// CKEditor file upload
    		CKEDITOR.replace("post_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?",
    	    	height: "400px"
        	    	
    	    });

    		$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances.post_content.updateElement();

				$.ajax({
					type : "POST",
					url : "./ajax/content-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("저장했습니다.");
// 								location.reload();
							} else {
								alert("저장하지 못했습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
					$("#btn-submit").prop("disabled", false);
				}).fail(function() {
				}).always(function() {
				}); // ajax
			}); // form
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>