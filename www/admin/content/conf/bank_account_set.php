<?php
require_once '../../../if-config.php';

if_authenticate_admin();

$if_bank_account_data = if_get_option('if_bank_account_data');

$jdec = json_decode($if_bank_account_data, true);

require_once ADMIN_PATH . '/include/cms-header.php';
?>

		</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						무통장입금 계좌
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-4">
							<form id="form-item-new">
    							<div class="box box-success">
    								<div class="box-body">
    									<div class="form-group">
    										<label>은행명</label>
    										<input type="text" name="bank_name" id="bank_name" class="form-control">
    									</div>
    									<div class="form-group">
    										<label>계좌번호</label>
    										<input type="text" name="bank_account" id="bank_account" class="form-control">
    									</div>
    									<div class="form-group">
    										<label>예금주</label>
    										<input type="text" name="account_holder" id="account_holder" class="form-control">
    									</div>
    								</div><!-- /.box-body -->
    								<div class="box-footer">
    									<button type="submit" class="btn btn-success" id="btn-submit">
    										<i class="fa fa-check"></i> 저장합니다 
    									</button>
    								</div>
    							</div><!-- /. box -->
    						</form>
						</div>
							
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-body">
									<ul class="list-group">
							<?php 
							if (!empty($jdec)) {
							    foreach ($jdec as $key => $val) {
							        foreach ($val as $k => $v) {
							            $exp = explode("\t", $v);
							?>
										<li class="list-group-item bank-list" id="it-<?php echo $k ?>">
											<span class="badge" style="cursor: pointer;">삭제</span>
											<?php echo $exp[0] ?>
											<?php echo $exp[1] ?>
											(예금주: <?php echo $exp[2] ?>)
										</li>
							<?php 
							        }
							    }
							}
							?>
									</ul>
								</div><!-- /.box-body -->
							</div><!-- /. box -->
						</div><!-- /.col -->
					</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
    	<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    	
		<script>
		$(function() {
			$("input").prop("required", true);	// All fields required

			$("#form-item-new").validate({
    			rules: {
    			},
    			messages: {
    			},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/bank-account-set.php",
    					data : $("#form-item-new").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
        						location.reload();
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});

    		// 삭제
    		$(".bank-list").click(function() {
        		if (!confirm("삭제하시겠습니까?")) {
            		return;
            	}
            	
       			var id = $(this).attr("id").replace(/\D/g, "");

       			$.ajax({
       				type : "POST",
					url : "./ajax/bank-account-delete.php",
					data : {
						"id" : id
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
           	});
		});
		</script>
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
	</body>
</html>