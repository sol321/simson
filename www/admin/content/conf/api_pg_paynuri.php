<?php
/*
 * Desc: 페이누리 정보 설정
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-option.php';

if_authenticate_admin();

$option_name = 'if_api_pg_paynuri';
$json = if_get_option($option_name);
$option_value = json_decode($json, true);

if (!empty($option_value)) {
    $store_id = $option_value['store_id'];
    $store_name = $option_value['store_name'];
    $store_url = $option_value['store_url'];
    $biz_no = $option_value['biz_no'];
    $crypto_key = $option_value['crypto_key'];
} else {
    $store_id = '';
    $store_name = '';
    $store_url = '';
    $biz_no = '';
    $crypto_key = '';
}

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			
				<section class="content-header">
					<h1>
						페이누리 PG 정보 설정 <small>페이누리로부터 받은 정보를 설정합니다.</small>
					</h1>
				</section>
				<section class="content">
					<div class="row">
    					<form id="form-item-new">
							<input type="hidden" name="option_name" value="<?php echo $option_name ?>">
							<div class="col-md-6">
								<div class="box box-success">
									<div class="box-header with-border">
										<h3 class="box-title">페이누리</h3>
										<a href="https://store.paynuri.com" target="_blank">상점 관리자 웹사이트</a>
									</div>
									<div class="box-body">
										<div class="form-group">
											<label>가맹점 번호</label>
											<input type="text" name="store_id" id="store_id" class="form-control" value="<?php echo $store_id ?>" maxlength="10" placeholder="10자리 숫자">
										</div>
										<div class="form-group">
											<label>가맹점명</label>
											<input type="text" name="store_name" class="form-control" value="<?php echo $store_name ?>">
										</div>
										<div class="form-group">
											<label>가맹점 URL</label>
											<input type="text" name="store_url" class="form-control" value="<?php echo $store_url ?>" placeholder="<?php echo $_SERVER['HTTP_HOST'] ?>">
										</div>
										<div class="form-group">
											<label>사업자등록번호</label>
											<input type="text" name="biz_no" id="biz_no" class="form-control" value="<?php echo $biz_no ?>" placeholder="숫자만 입력해 주십시오">
										</div>
										<div class="form-group">
											<label>가맹점 KEY</label>
											<input type="text" name="crypto_key" class="form-control" value="<?php echo $crypto_key ?>">
										</div>
									</div><!-- /.box-body -->
									<div class="box-footer">
										<button type="submit" class="btn btn-success" id="btn-submit">
											<i class="fa fa-check"></i> 저장합니다 
										</button>
									</div>
								</div><!-- /. box -->
							</div><!-- /.col -->
						</form>
    				</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
			<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
    		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
    
        	<script>
        	$(function() {
    			$("#store_id, #biz_no").numeric();

    			$("#form-item-new").submit(function(e) {
    				e.preventDefault();

    				$.ajax({
    					type : "POST",
    					url : "./ajax/api-pg-paynuri.php",
    					data : $(this).serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							alert("저장했습니다.");
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}); // form
    		});
        	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>
    	</div>
    	<!-- ./wrapper -->

	</body>
</html>