<?php
/*
 * Desc: 배너 리스트
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 999;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$banner_from = empty($_GET['banner_from']) ? '' : $_GET['banner_from'];
$banner_to = empty($_GET['banner_to']) ? '' : $_GET['banner_to'];

$sql = '';
$pph = '';
$sparam = [];

// 제목 검색
if (!empty($qs1)) {
    $sql .= " AND (post_title LIKE ? OR post_content LIKE ?)";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// 배너 노출 기간 검색
if (!empty($banner_from) && !empty($banner_to)) {
    $sql .= " AND (period_from BETWEEN ? AND ?) OR (period_to BETWEEN ? AND ?)";
    $banner_from_attach = $banner_from . ' 00:00:00';
    $banner_to_attach = $banner_to . ' 23:59:59';
    array_push($sparam, $banner_from_attach, $banner_to_attach, $banner_from_attach, $banner_to_attach);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_service_posts'] . "
		WHERE
			post_type = 'banner_new'
			$sql
		ORDER BY
			post_order ASC,
            seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						배너 리스트
						<a href="banner_add.php" class="btn btn-info btn-sm">배너 등록</a> &nbsp;
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<div class="row">
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="banner_from" name="banner_from" value="<?php echo $banner_from ?>" class="form-control" placeholder="배너 노출 기간">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="banner_to" name="banner_to" value="<?php echo $banner_to ?>" class="form-control">
    									</div>
    								</div>
    								<div class="col-md-3">
    									<div class="input-group">
    										<input type="text" name="qs1" value="" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
    										<div class="input-group-btn">
    											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
    										</div>
    									</div>
    								</div>
    								<div class="col-md-2">
    									<button type="button" id="reset-btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
    								
    							</div>
							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<form id="form-item-list">
    							<table class="table table-hover">
    								<thead>
    									<tr class="active">
        									<th>#</th>
    										<th>이미지 </th>
    										<th>내용</th>
    										<th>사용 여부</th>
    										<th>배너 노출 기간</th>
    										<th>등록날짜</th>
    										<th></th>
        								</tr>
        							</thead>
        							<tbody id="sortable">
        				<?php
    					if (!empty($item_results)) {
    						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
    						foreach ($item_results as $key => $val) {
    							$seq_id = $val['seq_id'];
    							
    							$post_title = $val['post_title'];
    							$post_state = $val['post_state'];
    							$period_from = $val['period_from'];
    							$period_to = $val['period_to'];
    							$create_dt = $val['create_dt'];
    							
    							$meta_data = $val['meta_data'];
    							$jdec = json_decode($meta_data, true);
    							$banner_url = $jdec['banner_url'];
    							$file_attachment = $jdec['file_attachment'];
    							$file_url = $file_attachment[0]['file_url'];
    							$img_url = empty($file_url) ? '' : '<img src="' . $file_url . '" style="height: 50px;">';
    						?>
    									<tr id="post-id-<?php echo $seq_id ?>">
    										<td>
    											<?php echo $list_no ?>
    											<input type="hidden" name="banner_id[]" value="<?php echo $seq_id ?>">
    										</td>
    										<td>
    											<?php echo $img_url ?>
    										</td>
    										<td>
    											<span class="label label-info">제목</span> <?php echo $post_title ?><br>
    											<span class="label label-info">URL</span> <?php echo $banner_url ?>
    										</td>
    										<td><?php echo $post_state ?></td>
    										<td>
    											<?php echo $period_from ?> ~ <?php echo $period_to ?>
    										</td>
    										<td><?php echo $create_dt ?></td>
    										<td>
    											<a href="banner_edit.php?id=<?php echo $seq_id ?>" class="btn btn-primary btn-xs">편집</a> &nbsp;
    											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
    										</td>
    									</tr>
    					<?php
    							$list_no--;
    						}
    					}
    					?>
    								</tbody>
    							</table>
							</form>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
//         		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/banner-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			$("#banner_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#banner_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#banner_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#banner_from").datepicker("option", "maxDate", selectedDate);
				 }
			});

			$("#sortable").sortable({
		 		stop : function(event, ui) {
		 			reorderBanner();
		 		}
		 	});
			$("#sortable").disableSelection();
    	});

    	function reorderBanner() {
			$.ajax({
				type : "POST",
				url : "./ajax/banner-reorder.php",
				data : $("#form-item-list").serialize(),
				dataType : "json",
				success : function(res) {
					if (res.code == "0") {
						location.reload();
					} else {
						alert(res.msg);
					}
				}
			});
		}
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>