<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('배너에 대한 정보가 필요합니다.');
}

$seq_id = $_GET['id'];

$pw_row = if_get_banner_by_id($seq_id);

$post_title = $pw_row['post_title'];
$post_state = $pw_row['post_state'];
$period_from = $pw_row['period_from'];
$period_to = $pw_row['period_to'];

$meta_data = $pw_row['meta_data'];
$jdec = json_decode($meta_data, true);
$banner_url = $jdec['banner_url'];
$file_attachment = $jdec['file_attachment'];

require_once ADMIN_PATH . '/include/cms-header.php';
?>
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						배너 편집
					</h1>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<form id="form-item-new">
							<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
							<div class="col-md-8">
								<div class="box box-solid">
									<div class="box-body">
										<div class="form-group">
											<label>배너 제목</label>
											<input id="post_title" name="post_title" class="form-control" placeholder="배너 제목을 입력해 주십시오." value="<?php echo $post_title ?>">
										</div>
										<div class="form-group">
											<label>URL</label>
											<input id="banner_url" name="banner_url" class="form-control" value="<?php echo $banner_url ?>">
										</div>
										<div class="form-group">
											<label>배너 노출 기간</label>
											<input type="text" id="banner-period" class="form-control" readonly value="<?php echo $period_from ?> - <?php echo $period_to ?>">
											<input type="hidden" name="period_from" id="period_from" value="<?php echo $period_from ?>">
											<input type="hidden" name="period_to" id="period_to" value="<?php echo $period_to ?>">
										</div>
										<div class="form-group">
											<label>사용 여부</label>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="post_state" value="close" <?php echo strcmp($post_state, 'close') ? '' : 'checked'; ?>>사용하지 않습니다
												</label>
											</div>
										</div>
										<div class="form-group">
        									<label>배너 이미지 파일</label>
        									<div>
        										<div class="btn btn-default btn-file">
            										<i class="fa fa-paperclip"></i> 파일 선택
            										<input type="file" name="attachment[]" id="file-attach">
            									</div>
            									<ul class="list-group" id="preview-attachment">
            						<?php
            						if (!empty($file_attachment)) {
            							foreach ($file_attachment as $key => $val) {
            								$file_path = $val['file_path'];
            								$file_url = $val['file_url'];
            								$file_name = $val['file_name'];
            								if (is_file($file_path)) {
            						?>
            										<li class="list-group-item list-group-item-success">
            											<img src="<?php echo $file_url ?>" style="width: 100%;">
            											<input type="hidden" name="file_path[]" value="<?php echo $file_path ?>">
            											<input type="hidden" name="file_url[]" value="<?php echo $file_url ?>">
            											<input type="hidden" name="file_name[]" value="<?php echo $file_name ?>">
            											<span class="badge"><span class="glyphicon glyphicon-remove hide-file-attach" style="cursor: pointer;"></span></span>
            											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $file_name ?>">
            												<?php echo $file_name ?>
            											</a>
            										</li>
            						<?php
            								}
            							}
            						}
            						?>
            									</ul>
        									</div>
        								</div>
									</div><!-- /.box-body -->
									<div class="box-footer text-center">
										<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
									</div><!-- /.box-footer -->
								</div><!-- /. box -->
							</div><!-- /.col -->
						</form>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<!-- date-range-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/moment/moment.min.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<!-- bootstrap time picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
		
    	<script>
    	$(function () {
    		// 배너 노출 기간
			$("#banner-period").daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				locale: {
					format: "YYYY-MM-DD HH:mm:ss",
					applyLabel: "적용",
					cancelLabel: "취소",
					daysOfWeek: [
						"<span style='color: red;'>일</span>", 
						"월", "화", "수", "목", "금",
						"<span style='color: blue;'>토</span>"
					],
					monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				}
			}, function(start, end) {
				$("#period_from").val(start.format("YYYY-MM-DD HH:mm:ss"));
				$("#period_to").val(end.format("YYYY-MM-DD HH:mm:ss"));
			});

			// Default
    		var bannerPeriod = $("#banner-period").val().split(" - ");
			$("#period_from").val(bannerPeriod[0]);
			$("#period_to").val(bannerPeriod[1]);
    	    
    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/banner-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "banner_list.php";
							} else {
								alert("배너를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 첨부파일 Upload
			$("#file-attach").change(function() {
				var fsize = this.files[0].size;
				var fname = this.files[0].name;
				var fext = fname.split('.').pop().toLowerCase();
// 				var max_file_size = $("#max-file-size").val();

// 				if (fsize > max_file_size) {
// 					alert("최대 '" + max_file_size + "' bytes 파일을 업로드할 수 있습니다.\n현재 선택된 파일은 '" + fsize + "' bytes입니다.");
// 					return;
// 				}

				$("#form-item-new").ajaxSubmit({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_upload.php",
					dataType : "json",
					success: function(xhr) { 
						if (xhr.code == "0") { 
							var fileLists = '';
							for (var i = 0; i < xhr.file_url.length; i++) {
								fileLists = '<li class="list-group-item list-group-item-success">' +
                								'<img src="' + xhr.file_url[i] + '" style="width: 100%;">' +
                								'<input type="hidden" name="file_path[]" value="' + xhr.file_path[i] + '">' +
                								'<input type="hidden" name="file_name[]" value="' + xhr.file_name[i] + '">' +
                								'<input type="hidden" name="file_url[]" value="' + xhr.file_url[i] + '">' +
                								'<span class="badge"><span class="glyphicon glyphicon-remove delete-file-attach" style="cursor: pointer;"></span></span>' +
                								xhr.file_name[i] +
                							'</li>';
							}
							$("#preview-attachment").html(fileLists);
						} else {
							alert(xhr.msg);
						}
						$("#file-attach").val("");
					}
				});
			});
			//-- 첨부파일 Upload 시작
			
			// 파일 삭제 
			$(document).on("click", ".delete-file-attach", function() {
				var file = $(this).parent().parent().find('input[name="file_path[]"]').val();

				$.ajax({
					type : "POST",
					url : "<?php echo INC_URL ?>/lib/attachment_delete.php",
					data : {
						"filePath" : file
					},
					dataType : "json",
					success : function(res) {
						$("#preview-attachment").html("");
						if (res.code != "0") {
							alert(res.msg);
						}
					}
				});
			});

			// 첨부파일 숨기기
			$(document).on("click", ".hide-file-attach", function() {
				$(this).closest("li").fadeOut("slow", function() { $(this).remove(); });
			});
			//-- 첨부파일 숨기기
			// -- 첨부 파일 끝
    	});
    	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
