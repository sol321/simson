<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('팝업에 대한 정보가 필요합니다.');
}

$seq_id = $_GET['id'];

$pw_row = if_get_popup_window($seq_id);

$post_title = $pw_row['post_title'];
$post_content = $pw_row['post_content'];
$post_state = $pw_row['post_state'];
$period_from = $pw_row['period_from'];
$period_to = $pw_row['period_to'];

$meta_data = $pw_row['meta_data'];
$jdec = json_decode($meta_data, true);
$popup_width = $jdec['popup_width'];
$popup_height = $jdec['popup_height'];
$popup_top = $jdec['popup_top'];
$popup_left = $jdec['popup_left'];

require_once ADMIN_PATH . '/include/cms-header.php';
?>
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						팝업 편집
					</h1>
				</section>
				<!-- Main content -->
				<section class="content">
					<div class="row">
						<form id="form-item-new">
							<input type="hidden" name="seq_id" value="<?php echo $seq_id ?>">
							<div class="col-md-4">
								<div class="box box-solid">
									<div class="box-body">
										<div class="form-group">
											<label>팝업 제목</label>
											<input id="post_title" name="post_title" class="form-control" placeholder="팝업 제목을 입력해 주십시오." value="<?php echo $post_title ?>">
										</div>
										<div class="form-group">
											<label>팝업 크기</label> 
											<div class="input-group">
												<span class="input-group-addon">가로 (너비)</span>
												<input type="text" name="popup_width" id="popup_width" class="form-control numeric" value="<?php echo $popup_width ?>">
												<span class="input-group-addon">px</span>
											</div>
											<div class="input-group">
												<span class="input-group-addon">세로 (높이)</span>
												<input type="text" name="popup_height" id="popup_height" class="form-control numeric" value="<?php echo $popup_height ?>">
												<span class="input-group-addon">px</span>
											</div>
											<div class="help-block">※ 팝업 크기는 팝업에 들어가는 내용에 맞춰 설정해주세요.</div>
										</div>
										<div class="form-group">
											<label>팝업 위치</label>
											<div class="input-group">
												<span class="input-group-addon">Top (위로부터)</span>
												<input type="text" name="popup_top" id="popup_top" class="form-control numeric" value="<?php echo $popup_top ?>">
												<span class="input-group-addon">px</span>
											</div>
											<div class="input-group">
												<span class="input-group-addon">Left (좌로부터)</span>
												<input type="text" name="popup_left" id="popup_left" class="form-control numeric" value="<?php echo $popup_left ?>">
												<span class="input-group-addon">px</span>
											</div>
										</div>
										<div class="form-group">
											<label>노출 기간</label>
											<input type="text" id="popup-period" class="form-control" readonly value="<?php echo $period_from ?> - <?php echo $period_to ?>">
											<input type="hidden" name="period_from" id="period_from" value="<?php echo $period_from ?>">
											<input type="hidden" name="period_to" id="period_to" value="<?php echo $period_to ?>">
										</div>
										<div class="form-group">
											<label>사용 여부</label>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="post_state" value="close" <?php echo strcmp($post_state, 'close') ? '' : 'checked'; ?>>사용하지 않습니다
												</label>
											</div>
										</div>
									</div><!-- /.box-body -->
								</div><!-- /. box -->
							</div><!-- /.col -->
							<div class="col-md-8">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">팝업 내용</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group">
											<textarea id="post_content" name="post_content" class="form-control"><?php echo $post_content ?></textarea>
										</div>
									</div><!-- /.box-body -->
									<div class="box-footer text-center">
										<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
									</div><!-- /.box-footer -->
								</div><!-- /. box -->
							</div>
						</form>
					</div><!-- /.row -->
				</section><!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

		<!-- CKEditor -->
		<script src="<?php echo INC_URL ?>/js/ckeditor/ckeditor.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		<!-- date-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<!-- date-range-picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/moment/moment.min.js"></script>
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<!-- bootstrap time picker -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/components/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
		
    	<script>
    	$(function () {
    		$(".numeric").numeric({ negative: false });
    		
        	// CKEditor toolbar custom
    		CKEDITOR.config.toolbar = [
        		['Font','FontSize','-','TextColor','BGColor','-','Bold','Italic','Underline','StrikeThrough','-','Image','Maximize','Source']
			];
			
    		// CKEditor file upload
    		CKEDITOR.replace("post_content", {
    	    	filebrowserUploadUrl: "<?php echo INC_URL ?>/lib/ckeditor_upload.php?",
    	    	height: "300px"
    	    });

    		// 팝업 노출 기간
			$("#popup-period").daterangepicker({
				timePicker: true,
				timePicker24Hour: true,
				locale: {
					format: "YYYY-MM-DD HH:mm:ss",
					applyLabel: "적용",
					cancelLabel: "취소",
					daysOfWeek: [
						"<span style='color: red;'>일</span>", 
						"월", "화", "수", "목", "금",
						"<span style='color: blue;'>토</span>"
					],
					monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				}
			}, function(start, end) {
				$("#period_from").val(start.format("YYYY-MM-DD HH:mm:ss"));
				$("#period_to").val(end.format("YYYY-MM-DD HH:mm:ss"));
			});

			// Default
    		var popupPeriod = $("#popup-period").val().split(" - ");
			$("#period_from").val(popupPeriod[0]);
			$("#period_to").val(popupPeriod[1]);
    	    
    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();
				CKEDITOR.instances.post_content.updateElement();

				$.ajax({
					type : "POST",
					url : "./ajax/popup-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "popup_list.php";
							} else {
								alert("팝업을 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});
    	});
    	</script>
	
<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>
