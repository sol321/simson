<?php
/*
 * Desc: 팝업 리스트
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$popup_from = empty($_GET['popup_from']) ? '' : $_GET['popup_from'];
$popup_to = empty($_GET['popup_to']) ? '' : $_GET['popup_to'];

$sql = '';
$pph = '';
$sparam = [];

// 제목 검색
if (!empty($qs1)) {
    $sql .= " AND (post_title LIKE ? OR post_content LIKE ?)";
    array_push($sparam, '%' . $qs1 . '%', '%' . $qs1 . '%');
}

// 팝업 노출 기간 검색
if (!empty($popup_from) && !empty($popup_to)) {
    
    $sql .= " AND (period_from BETWEEN ? AND ?) OR (period_to BETWEEN ? AND ?)";
    $popup_from_attach = $popup_from . ' 00:00:00';
    $popup_to_attach = $popup_to . ' 23:59:59';
    array_push($sparam, $popup_from_attach, $popup_to_attach, $popup_from_attach, $popup_to_attach);
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
		SELECT
			*
		FROM
			" . $GLOBALS['if_tbl_service_posts'] . "
		WHERE
			post_type = 'popup_new'
			$sql
		ORDER BY
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<div class="pull-right">
						<a href="popup_preview.php" class="btn btn-success btn-sm" target="_blank">팝업 미리보기</a>
					</div>
					<h1>
						팝업 리스트
						<a href="popup_add.php" class="btn btn-info btn-sm">팝업 등록</a> &nbsp;
					</h1>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">
								<div class="row">
    								<div class="col-md-4">
    									<div class="input-group datepicker input-daterange">
    										<input type="text" id="popup_from" name="popup_from" value="<?php echo $popup_from ?>" class="form-control" placeholder="팝업 노출 기간">
    										<span class="input-group-addon">~</span>
    										<input type="text" id="popup_to" name="popup_to" value="<?php echo $popup_to ?>" class="form-control">
    									</div>
    								</div>
    								<div class="col-md-3">
    									<div class="input-group">
    										<input type="text" name="qs1" value="" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $qs1 ?>">
    										<div class="input-group-btn">
    											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
    										</div>
    									</div>
    								</div>
    								<div class="col-md-2">
    									<button type="button" id="reset-btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
    								</div>
    								
    							</div>
							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
    									<th>#</th>
										<th>제목</th>
										<th>사용 여부</th>
										<th>팝업 노출 기간</th>
										<th>크기(px)</th>
										<th>위치(px)</th>
										<th>등록날짜</th>
										<th></th>
    								</tr>
    				<?php
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$post_title = $val['post_title'];
							$post_state = $val['post_state'];
							$period_from = $val['period_from'];
							$period_to = $val['period_to'];
							$create_dt = $val['create_dt'];
							
							$meta_data = $val['meta_data'];
							$jdec = json_decode($meta_data, true);
							$popup_width = $jdec['popup_width'];
							$popup_height = $jdec['popup_height'];
							$popup_top = $jdec['popup_top'];
							$popup_left = $jdec['popup_left'];
						?>
									<tr id="post-id-<?php echo $seq_id ?>">
										<td><?php echo $list_no ?></td>
										<td>
											<?php echo $post_title ?>
										<td>
											<?php echo $post_state ?>
										</td>
										<td>
											<?php echo $period_from ?> ~ <?php echo $period_to ?>
										</td>
										<td>
											<?php echo $popup_width ?> X <?php echo $popup_height ?>
										</td>
										<td>
											Top: <?php echo $popup_top ?><br>
											Left: <?php echo $popup_left ?>
										</td>
										<td><?php echo $create_dt ?></td>
										<td>
											<a href="popup_edit.php?id=<?php echo $seq_id ?>" class="btn btn-primary btn-xs">편집</a> &nbsp;
											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
    					<div class="box-footer text-center">
        		<?php
        		echo $paginator->if_paginator();
        		?>
        				</div>
    					<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
    	$(function() {
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/popup-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			$("#popup_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#popup_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#popup_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 1,
				 onClose: function(selectedDate) {
					  $("#popup_from").datepicker("option", "maxDate", selectedDate);
				 }
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>