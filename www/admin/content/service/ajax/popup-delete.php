<?php
/*
 * Desc: 팝업 완전 삭제 (delete)
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['id'])) {
    $code = 101;
    $msg = '팝업을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['id'];

$result = if_delete_popup_window($seq_id);

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>