<?php
/*
 * Desc: 팝업 등록
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_title'])) {
    $code = 101;
    $msg = '제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['popup_width'])) {
    $code = 103;
    $msg = '가로 크기을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['popup_height'])) {
    $code = 104;
    $msg = '세로 크기를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_popup_window();

if (empty($result)) {
    $code = 201;
    $msg = '팝업을 등록할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>