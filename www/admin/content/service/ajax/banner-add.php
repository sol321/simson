<?php
/*
 * Desc: 배너 등록
 *
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['post_title'])) {
    $code = 101;
    $msg = '배너 제목을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['file_path'])) {
    $code = 103;
    $msg = '배너를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_banner();

if (empty($result)) {
    $code = 201;
    $msg = '배너를 등록할 수 없습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>