<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-service-post.php';

$pops= if_get_live_popup_windows();

require_once INC_PATH . '/front-header.php';
?>
	</head>

	<body>

	<?php 
	if ($pops) {
	    foreach ($pops as $key => $val) {
	        $popup_id = $val['seq_id'];
	        $post_content = $val['post_content'];
	        
	        $meta_data = $val['meta_data'];
	        $jdec = json_decode($meta_data, true);
	        $popup_width = $jdec['popup_width'] + 2;
	        $popup_height = $jdec['popup_height'] + 0;
	        $popup_top = $jdec['popup_top'];
	        $popup_left = $jdec['popup_left'];
	?>
		<div id="popup-<?php echo $popup_id ?>" class="popup_dialog" style="display: none; width: <?php echo $popup_width ?>px; height: <?php echo $popup_height ?>px; position: absolute; top: <?php echo $popup_top ?>px; left: <?php echo $popup_left ?>px;">
			<div class="modal-dialog modal-lg" style="width: <?php echo $popup_width  ?>px; height: <?php echo $popup_height ?>px;">
				<div class="modal-content">
					<div class="modal-body" style="padding: 0px;">
						<div style="width: <?php echo $popup_width - 2 ?>px; height: <?php echo $popup_height ?>px;">
							<?php echo $post_content ?>
						</div>
						<div style="padding: 8px; border: 0px solid #A0A0A0; border-top: 0; background-color: #eee;">
							<div style="float: left;">
								<div style="cursor: pointer; font-size: 12px;" class="pop_close">닫기</div>
							</div>
							<div style="float: right;">
								<div style="cursor: pointer; font-size: 12px;" class="pop_disable">오늘 하루 더 이상 보지 않기</div>
							</div>
							<div style="clear: both;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	    }
	} else {
	    echo '<h1>현재 사용 가능한 팝업이 없습니다.</h1>';
	}
	?>		
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
        <script>
        function setCookie(name, value, expiredays) {
        	var todayDate = new Date();
        	todayDate.setDate(todayDate.getDate() + expiredays);
        	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
        }
        
        function getCookie(name) {
        	var value = "; " + document.cookie;
        	var parts = value.split("; " + name + "=");
        	if (parts.length == 2) return parts.pop().split(";").shift();
        }
        
        function closeCookie(name) {
           setCookie(name, "pop_disable" , 1); // 1일간
        }
        
        $(function() {
        	$(".popup_dialog").draggable();
        	
        	var w = $(document).width();
        	var h = screen.height;
        
        	if ($(".popup_dialog").length == 0) {
        		return;
        	} else if ($(".popup_dialog").length == 1) {	// 1개의 팝업일 경우 가운데 출력
        		var popupName = $(".popup_dialog").attr("id");
        
        		if (getCookie(popupName) != "pop_disable") {
        			var pw = $("#" + popupName).css("width").replace(/\D/g, "");
        			var ph = $("#" + popupName).css("height").replace(/\D/g, "");
        			var top = parseInt((h - ph) / 2);
        			var left = parseInt((w - pw) / 2);
        			var zIndex = 1000;
        			$("#" + popupName).css({
        // 				"position" : "absolute",
        // 				"top" : top,
        // 				"left" : left,
        				"z-index" : zIndex
        			}).fadeIn();
        		}
        	} else {
        		$(".popup_dialog").each(function(index) {
        			var popupName =  $(this).attr("id");
        			var top = 100;
        			var left = index * 300;
        			var zIndex = index + 1000;
        
        			if (getCookie(popupName) != "pop_disable") {
        				$("#" + popupName).css({
        // 					"position" : "absolute",
        // 					"top" : top,
        // 					"left" : left,
        					"z-index" : zIndex
        				}).fadeIn();
        			}
        		});
        	}
        
        	$(".pop_close").click(function() {
        		var popupName = $(this).closest(".popup_dialog").attr("id");
        		$("#" + popupName).fadeOut("slow");
        	});
        
        	$(".pop_disable").click(function() {
        		var popupName = $(this).closest(".popup_dialog").attr("id");
        		closeCookie(popupName);
        		$("#" + popupName).fadeOut("slow");
        	});
        
        });
        </script>
	
	</body>
	
</html>
