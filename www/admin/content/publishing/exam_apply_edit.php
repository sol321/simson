<?php
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('내역을 선택해 주십시오.');
}

$apply_id = $_GET['id'];

$exam_row = if_get_mock_exam_application($apply_id);
$univ_name = $exam_row['univ_name'];
$univ_code = $exam_row['univ_code'];
$apply_name = $exam_row['apply_name'];
$apply_email = $exam_row['apply_email'];
$apply_phone = $exam_row['apply_phone'];
$apply_dt = $exam_row['apply_dt'];

$meta_data = $exam_row['meta_data'];
$jdec = json_decode($meta_data, TRUE);

$apply_fax = $jdec['apply_fax'];
$org_zipcode = $jdec['org_zipcode'];
$org_address1 = $jdec['org_address1'];
$org_address2 = $jdec['org_address2'];
$admin_memo = @$jdec['admin_memo'];

$file = $jdec['file_attachment'];
$file_path = $file[0]['file_path'];
$file_url = $file[0]['file_url'];
$file_name = $file[0]['file_name'];

require_once ADMIN_PATH . '/include/cms-header.php';
?>

	<!-- jQueryUI -->
	<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
	
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">

<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content">
					<div class="row">
                    	<div class="col-md-8">
                        	<!-- form start -->
                            <form id="form-item-new" class="form-horizontal">
                            	<input type="hidden" name="seq_id" id="seq_id" value="<?php echo $apply_id ?>">
                        		<div class="box box-primary">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-primary">모의고사 신청 정보 수정</h3>
                       					<div class="pull-right"><?php echo $apply_dt ?></div>
                        			</div>
                        			<div class="box-body">
        								<div class="form-group">
        									<label class="col-md-2 control-label">신청대학교</label>
        									<div class="col-md-8">
        										<?php echo $univ_name ?> (<?php echo $univ_code ?>)
        									</div>
        								</div>
        								<div class="form-group">
                        					<label class="col-md-2 control-label">받을 주소</label>
                        					<div class="col-md-3">
                        						<input type="text" class="form-control" name="org_zipcode" id="org_zipcode" value="<?php echo $org_zipcode ?>">
                        					</div>
                        					<div class="col-md-offset-2 col-md-8">
                        						<input type="text" class="form-control" name="org_address1" id="org_address1" value="<?php echo $org_address1 ?>">
                        					</div>
                        					<div class="col-md-offset-2 col-md-8">
                        						<input type="text" class="form-control" name="org_address2" id="org_address2" value="<?php echo $org_address2 ?>">
                        					</div>
                        				</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">신청자명</label>
        									<div class="col-md-8">
        										<input type="text" id="apply_name" name="apply_name" class="form-control" value="<?php echo $apply_name ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">전화번호</label>
        									<div class="col-md-8">
        										<input type="text" id="apply_phone" name="apply_phone" class="form-control" value="<?php echo $apply_phone ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">팩스번호</label>
        									<div class="col-md-8">
        										<input type="text" id="apply_fax" name="apply_fax" class="form-control" value="<?php echo $apply_fax ?>">
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">이메일</label>
        									<div class="col-md-8">
        										<input type="text" id="apply_email" name="apply_email" class="form-control" value="<?php echo $apply_email ?>">
        									</div>
        								</div>
        								<div class="form-group">
                							<label class="col-md-2 control-label">메모</label>
                							<div class="col-md-8">
                								<textarea name="admin_memo" class="form-control"><?php echo $admin_memo ?></textarea>
                							</div>
                						</div>
        								<div class="form-group">
        									<label class="col-md-2 control-label">학생코드표</label>
        									<div class="col-md-8">
            									<ul class="list-group" id="preview-attachment">
                    				<?php
            						if (!empty($file_path)) {
        								if (is_file($file_path)) {
            						?>
            										<li class="list-group-item list-group-item-info">
            											<input type="hidden" name="file_path[]" value="<?php echo $file_path ?>">
            											<input type="hidden" name="file_url[]" value="<?php echo $file_url ?>">
            											<input type="hidden" name="file_name[]" value="<?php echo $file_name ?>">
            											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn btn-info btn-xs" title="<?php echo $file_name ?>">
            												<?php echo $file_name ?>
            											</a>
            										</li>
            						<?php
            							}
            						}
            						?>
                    							</ul>
        									</div>
        								</div>
                        			</div>
                        			<div class="box-footer text-center">
        								<button type="button" id="btn-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
                              		</div>
                        		</div>
                      		</form>
                    	</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		
		<script>
		$(function () {
			// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/exam-apply-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								location.href = "exam_apply_list.php";
							} else {
								alert("수정할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			$("#btn-cancel").click(function() {
				history.back();
			});

			// 근무처 우편번호 찾기
			$("#org_zipcode, #org_address1").click(function() {
				execDaumPostcode('dept');
			});
		});

		function execDaumPostcode(spot) {
			new daum.Postcode({
				oncomplete: function(data) {
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

					// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
					// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
					var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
					var extraRoadAddr = ''; // 도로명 조합형 주소 변수

					// 법정동명이 있을 경우 추가한다. (법정리는 제외)
					// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
					if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
						extraRoadAddr += data.bname;
					}
					// 건물명이 있고, 공동주택일 경우 추가한다.
					if(data.buildingName !== '' && data.apartment === 'Y'){
					   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
					if(extraRoadAddr !== ''){
						extraRoadAddr = ' (' + extraRoadAddr + ')';
					}
					// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
					if(fullRoadAddr !== ''){
						fullRoadAddr += extraRoadAddr;
					}

					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					if (spot == "dept") {
						document.getElementById('org_zipcode').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('org_address1').value = fullRoadAddr + " ";		// 도로명 주소
						document.getElementById('org_address2').focus();
					} else if (spot == "mail") {
						document.getElementById('mail_zipcode').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('mail_address1').value = fullRoadAddr + " ";		// 도로명 주소
						document.getElementById('mail_address2').focus();
					}
//	 				document.getElementById('sample4_jibunAddress').value = data.jibunAddress;		// 지번 주소
				}
			}).open();
		}
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>