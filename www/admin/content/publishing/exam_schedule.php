<?php
/*
 * Desc: 모의고사 > 시험 일정
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if (!empty($q)) {
	$sql = " AND exam_year = ? ";
	array_push($sparam, $q);
}

// Positional placeholder ?
if (!empty($sql)) {
	$pph_count = substr_count($sql, '?');
	for ($i = 0; $i < $pph_count; $i++) {
		$pph .= 's';
	}
}

if (!empty($pph)) {
	array_unshift($sparam, $pph);
}

$query = "
		SELECT
            exam_year,
			COUNT(*) AS cnt
		FROM
			" . $GLOBALS['if_tbl_mock_exam'] . "
        WHERE
            1
            $sql
		GROUP BY
			exam_year
		ORDER BY
			1 DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
		
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="exam_apply_list.php"><b>신청 접수</b></a></li>
							<li class="active"><a href="exam_schedule.php"><b>시험 일정</b></a></li>
							<li><a href="univ_code.php"><b>학교 코드</b></a></li>
							<li><a href="#"><b>공지사항</b></a></li>
							<li><a href="#"><b>질문게시판</b></a></li>
							<li><a href="#"><b>FAQ</b></a></li>
						</ul>
					</div>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-4">
    						<form id="form-time-new" class="form-horizontal">
    							<input type="hidden" name="action_type" id="action_type" value="ADD">
    							<div class="box box-default">
    								<div class="box-header with-border">
    									<div class="col-md-12">
    										<div class="input-group">
    											<div class="input-group-addon">
    												<b>연도</b>
    											</div>
    											<input type="text" class="form-control numeric" name="exam_year" id="exam_year" maxlength="4">
    										</div>
    									</div>
    								</div>
    								<!-- /.box-header -->
    								<div class="box-body" id="wrap-st-date">
    						<?php 
    						for ($i = 1; $i < 6; $i++) {
    						?>
    									<div class="form-group box-st-date">
    										<div class="col-md-12">
    											<div class="input-group">
    												<div class="input-group-addon">
    													<b class="st-order"><?php echo $i ?></b> 차
    												</div>
    												<div class="has-success">
    													<input type="text" class="form-control st-date" name="exam_order[]" value="" maxlength="10">
    												</div>
    												<span class="input-group-btn">
    													<button type="button" class="btn bg-red btn-flat st-act-delete"><i class="fa fa-times"></i></button>
    												</span>
    											</div>
    										</div>
    									</div>
    						<?php 
    						}
    						?>
    								</div>
    								<!-- /.box-body -->
    								<div class="box-footer">
    									<div class="form-group">
    										<div class="col-md-12">
    											<div class="pull-right">
    												<button type="button" class="btn btn-info btn-sm" id="btn-add-sch-time"><i class="fa fa-plus"></i> 추가</button>
    											</div>
    											<div class="text-center">
            										<button type="submit" id="btn-submit" class="btn btn-primary">저장하기</button>
            									</div>
    										</div>
    									</div>
    								</div>
    								<!-- /.box-footer -->
    							</div>
    						</form>
						</div>
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header">
									<h4 class="box-title">
		   								Total : <?php echo number_format($total_count) ?> &nbsp;
									</h4>
									
									<div class="box-tools">
                						<form class="form-inline">
        									<div class="input-group">
        										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="연도 검색">
        										<div class="input-group-btn">
        											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        										</div>
        									</div>
        								</form>
                					</div>
									
								</div><!-- /.box-header -->
								<div class="box-body">
									<table class="table table-hover">
										<thead>
											<tr class="active">
												<th>#</th>
												<th>연도</th>
												<th>차수</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
							<?php
							if (!empty($item_results)) {
								$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
								foreach ($item_results as $key => $val) {
									$exam_year = $val['exam_year'];
									$exam_total = $val['cnt'];
								?>
											<tr id="item-id-<?php echo $exam_year ?>">
												<td><?php echo $list_no ?></td>
												<td>
													<div style="font-size: 17px; font-weight: 800;"><?php echo $exam_year ?></div>
												</td>
												<td>
													<b><?php echo $exam_total ?></b>차
												</td>
												<td>
													<button class="btn btn-warning btn-xs action-edit">수정</button> &nbsp;
													<button class="btn btn-danger btn-xs action-delete">삭제</button> &nbsp;
												</td>
											</tr>
							<?php
									$list_no--;
								}
							}
							?>
										</tbody>
									</table>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<div class="text-center">
						<?php 
						echo $paginator->if_paginator();
						?>
									</div>
								</div>
							</div><!-- /.box -->
						
						</div>
						
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
		<script>
		$(function() {
			$(".numeric").numeric({ negative: false });
			
			// 폼 전송 ( 등록, 수정 )
			$("#form-time-new").submit(function(e) {
				e.preventDefault();

				var atype = $("#action_type").val();
				var aurl = atype == "ADD" ? "exam-schedule-add.php" : "exam-schedule-edit.php";

				$.ajax({
					type : "POST",
					url : "./ajax/" + aurl,
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("차수 정보를 저장했습니다.");
								location.reload();
							} else {
								alert("차수 정보를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 조회 (수정)
			$(".action-edit").click(function() {
				var year = $(this).closest("tr").attr("id").replace(/\D/g, "");

				$.ajax({
					type : "POST",
					url : "./ajax/exam-schedule-get.php",
					data : {
						"year" : year
					},
					dataType : "json",
					success : function(res) {
						if (res.code == "0") {
							$("#exam_year").val(res.exam_year);
							$("#wrap-st-date").html(res.exam_html);
							$("#action_type").val("EDIT");

							$(".st-date").datepicker({
				     			 dateFormat : "yy-mm-dd",
				     			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				     		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
				     			 changeMonth : true,
				     			 changeYear : true
				     		});
						}
					}
				});
			});

			// 삭제
			$(".action-delete").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return false;
				}

				var year = $(this).closest("tr").attr("id").replace(/\D/g, "");
	
				$.ajax({
					type : "POST",
					url : "./ajax/exam-schedule-delete.php",
					data : {
						"year" : year
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							alert("삭제했습니다.");
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});

			$(".st-date").datepicker({
     			 dateFormat : "yy-mm-dd",
     			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
     		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
     			 changeMonth : true,
     			 changeYear : true
     		});

     		// 차수 UI 추가
     		$("#btn-add-sch-time").click(function() {
     			var num = $("#wrap-st-date div.box-st-date").length;
     			var nextNum = num + 1;

         		// 마지막 box-st-date 내용 복사 후 붙여넣기
       			var copy = $("#wrap-st-date div.box-st-date:last").clone();
       			var schTime = $('<div>').append(copy).html();

       			$("#wrap-st-date").append(schTime);

       			$("#wrap-st-date div.box-st-date:last").find(".st-order").html(nextNum); 
       			$("#wrap-st-date div.box-st-date:last").find(".st-date").val(""); 
         	});

         	// 차수 삭제
         	$(document).on("click", ".st-act-delete", function() {
         		$(this).closest(".box-st-date").remove();
            });

            function numberST() {
            	$("#wrap-st-date div.box-st-date").each(function(idx) {
           			$(this).find(".st-order").html(idx + 1);
           		});
            }
            
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>