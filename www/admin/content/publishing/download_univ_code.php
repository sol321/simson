<?php
/*
 * Desc : 대학교 코드 다운로드
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/PHPExcel.php';

if_authenticate_admin();

$filename = '대학교 코드.xlsx';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Inforang")
->setLastModifiedBy("Softsyw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Code");

$active_sheet = $objPHPExcel->setActiveSheetIndex(0);

$column_array = array('대학 코드', '대학명');

$active_sheet->fromArray($column_array);

$query = "
		SELECT
            univ_code,
            univ_name
		FROM
			" . $GLOBALS['if_tbl_univ_code'] . "
		ORDER BY
			univ_name ASC
";
$stmt = $ifdb->prepare($query);
$stmt->execute();
$item_results = $ifdb->get_results($stmt);

$active_sheet->fromArray($item_results, null, "A2");

$active_sheet->getDefaultColumnDimension()->setWidth(20);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('대학교 코드');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '. gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>