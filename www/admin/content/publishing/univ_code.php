<?php
/*
 * Desc: 모의고사 > 학교코드 
 */
require_once '../../../if-config.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$q = empty($_GET['q']) ? '' : trim($_GET['q']);
$sql = '';
$pph = '';
$sparam = [];

// 검색어
if (!empty($q)) {
    $sql = " AND univ_name LIKE ? ";
    array_push($sparam, '%' . $q . '%');
}

// Positional placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

$query = "
        SELECT
            *
        FROM
            " . $GLOBALS['if_tbl_univ_code'] . "
        WHERE
            1
            $sql
        ORDER BY
			seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>

	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="exam_apply_list.php"><b>신청 접수</b></a></li>
							<li><a href="exam_schedule.php"><b>시험 일정</b></a></li>
							<li class="active"><a href="univ_code.php"><b>학교 코드</b></a></li>
							<li><a href="#"><b>공지사항</b></a></li>
    						<li><a href="#"><b>질문게시판</b></a></li>
    						<li><a href="#"><b>FAQ</b></a></li>
						</ul>
					</div>
				</div>
				<div class="content">
					<div class="row">
                    	<div class="col-md-4">
        					<div class="box box-info" id="box-univ-add">
        						<!-- form start -->
                        		<form id="form-item-new">
                					<div class="box-body">
										<h4>새 학교 등록</h4>
										<div class="form-group">
											<input type="text" class="form-control numeric" name="univ_code" id="univ_code" placeholder="코드" maxlength="3">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="univ_name" id="univ_name" placeholder="대학명">
										</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
        								<button type="submit" id="btn-submit" class="btn btn-primary">추가하기</button>
                					</div>
                					<!-- /.box-footer -->
                				</form>
        					</div>
        					
        					<!-- Edit -->
        					<div class="box box-warning hide" id="box-univ-edit">
        						<!-- form start -->
                        		<form id="form-item-edit">
                        			<input type="hidden" name="seq_id" id="seq_id">	
                					<div class="box-body">
										<h4>코드 변경</h4>
										<div class="form-group">
											<input type="text" class="form-control" name="univ_code" id="univ_code_edit" placeholder="이름" required>
										</div>
										<div class="form-group">
											<input type="text" class="form-control" name="univ_name" id="univ_name_edit" placeholder="휴대전화번호">
										</div>
                					</div>
                					<!-- /.box-body -->
                					<div class="box-footer text-center">
                						<button type="button" id="btn-contact-edit-cancel" class="btn btn-default">취소</button> &nbsp;
        								<button type="submit" id="btn-submit-edit" class="btn btn-success">저장합니다</button>
                					</div>
                					<!-- /.box-footer -->
                				</form>
        					</div>
        					<!-- /. Edit -->
            			</div>
            			<div class="col-md-8">
							<div class="box box-success">
        						<div class="box-header">
        							<h3 class="box-title">
        								<form id="form-excel-uplod">
            								Total : <?php echo number_format($total_count) ?> &nbsp;
            								<button type="button" class="btn btn-success btn-xs" id="btn-bulk-upload">
            									<span class="glyphicon glyphicon-upload"></span> Excel 업로드
            								</button> &nbsp; &nbsp;
            								<a href="download_univ_code.php" class="btn btn-info btn-xs">엑셀 파일 저장</a>
            								<input type="file" name="attachment" id="attachment_file" class="hide">
            							</form>
        							</h3>
        							<div class="box-tools">
        								<form class="form-inline">
        									<div class="input-group">
        										<input type="text" name="q" value="<?php echo $q ?>" class="form-control input-sm pull-right" placeholder="Search">
        										<div class="input-group-btn">
        											<button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        										</div>
        									</div>
        								</form>
        							</div>
        						</div><!-- /.box-header -->
        						<div class="box-body">
        							<table class="table table-hover">
        								<thead>
        									<tr>
        										<th>#</th>
        										<th>대학 코드</th>
												<th>대학교명</th>
        										<th></th>
        									</tr>
        								</thead>
        								<tbody>
        					<?php
        					if (!empty($item_results)) {
        						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        						foreach ($item_results as $key => $val) {
        						    $sid = $val['seq_id'];
        						    $ucode = $val['univ_code'];
        						    $uname = $val['univ_name'];
        						?>
        									<tr id="item-id-<?php echo $sid ?>">
        										<td><?php echo $list_no ?></td>
        										<td><?php echo $ucode ?></td>
        										<td><?php echo $uname ?></td>
        										<td>
        											<button class="btn btn-warning btn-xs action-edit">수정</button> &nbsp;
        											<button class="btn btn-danger btn-xs action-delete">삭제</button> &nbsp;
        										</td>
        									</tr>
        					<?php
        							$list_no--;
        						}
        					}
        					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
        						<div class="box-footer">
        							<div class="text-center">
        				<?php 
        				echo $paginator->if_paginator();
        				?>
        							</div>
        						</div>
        					</div><!-- /.box -->
						
						</div>
            		</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- Form -->
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.form.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.serializeObject.min.js"></script>
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
		<script>
    	$(function() {
    		$(".numeric").numeric({ negative: false });
    		
    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/univ-code-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("정보를 등록했습니다.");
								location.reload();
							} else {
								alert("정보를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});

			// 수정 클릭 시, 수정 form 노출
			$(".action-edit").click(function() {
				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
				var ucode = $(this).closest("tr").find("td").eq(1).html();
				var uname = $(this).closest("tr").find("td").eq(2).html();
				
				$("#seq_id").val(id);
				$("#univ_code_edit").val(ucode);
				$("#univ_name_edit").val(uname);

				$("#box-univ-add").addClass("hide");
				$("#box-univ-edit").removeClass("hide");
			});

			// 수정 변경 form 취소
			$("#btn-contact-edit-cancel").click(function() {
				$("#box-univ-edit").addClass("hide");
				$("#box-univ-add").removeClass("hide");
			});

			// 변경 저장
			$("#form-item-edit").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/univ-code-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit-edit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("정보를 변경했습니다.");
								location.reload();
							} else {
								alert("정보를 변경할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit-edit").prop("disabled", false);
				}); // ajax
			});
			
			// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/univ-code-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
        					alert("삭제했습니다.");
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		// Excel Upload
			$("#btn-bulk-upload").click(function() {
				if (confirm("기존의 자료를 완전 삭제한 후, 새로운 자료를 등록합니다. 계속하시겠습니까?")) {
					$("#attachment_file").click();
				}
			});
			// File Upload
			$("#attachment_file").change(function() {
				var fext = this.files[0].name.split('.').pop().toLowerCase();

				if ( fext !=  'xls' && fext != 'xlsx' ) {
					alert('확장자가 xls, xlsx인 엑셀파일만 업로드해 주십시오.');
					$(this).val("");
					return;
				}

				$("#btn-bulk-upload").html("업로드중입니다...");
				$("#btn-bulk-upload").prop("disabled", true);

				$("#form-excel-uplod").ajaxSubmit({
					type : "POST",
					url : "./ajax/add-upload-excel-file.php",
					data : {
					},
					dataType : "json",
					success: function(res) {
						if (res.code == "0") {
							alert(res.result + "건이 등록되었습니다.");
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				});
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>