<?php
/*
 * Desc: 차수 수정
 *  
 *  idx 파라미터 : 신청구분이 추가일 경우엔, 날짜를 변경할 수 있다. 정규 응시일보다 이전을 선택할 수 없도록 처리하기 위해, 정규날짜를 가져오기 위해 필요함.
 * 
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_close('내역을 선택해 주십시오.');
}

$idx = $_GET['idx'];

$order_id = $_GET['id'];
$row = if_get_mock_exam_order($order_id);

$apply_id = $row['apply_id'];
$apply_type = $row['apply_type'];
$exam_order = $row['exam_order'];
$exam_state = $row['exam_state'];
$pay_state = $row['pay_state'];
$apply_type = $row['apply_type'];
$exam_date = $row['exam_date'];

$app_row = if_get_mock_exam_application($apply_id);
$univ_name = $app_row['univ_name'];
$univ_code = $app_row['univ_code'];
$apply_name = $app_row['apply_name'];
$apply_dt = $app_row['apply_dt'];

// 추가 신청일 경우 날짜 선택 시, 정규 차시 날짜 이후를 조회하기 위함.
$meta_data = $app_row['meta_data'];
$jdec = json_decode($meta_data, TRUE);
$exam_regular_dt = $jdec['exam_regular_dt'];
$min_date = $exam_regular_dt[$idx];     // 정규 신청에 해당하는 날짜
$min_next_dt = date('Y-m-d', strtotime($min_date) + 86400); // 1일 이후

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>
	
	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
			<div class="content-wrapper">
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<!-- form start -->
							<form id="form-item-edit" class="form-horizontal">
								<input type="hidden" name="order_id" value="<?php echo $order_id ?>">
								<input type="hidden" name="apply_type" value="<?php echo $apply_type ?>">
                        	
                        		<div class="box box-primary">
                        			<div class="box-header with-border">
                       					<h3 class="box-title text-primary"><?php echo $univ_name ?>(<?php echo $univ_code ?>) - <?php echo $apply_name ?></h3>
                       					<div class="pull-right"><?php echo $apply_dt ?></div>
                        			</div>
                        			<div class="box-body">
                        	
                        				<div class="form-group">
        									<label class="col-xs-2 control-label">신청 구분</label>
        									<div class="col-xs-8">
        										<?php echo $exam_order ?>차 
        										<?php echo $if_exam_type[$apply_type] ?>
        									</div>
        								</div>
                        				<div class="form-group">
        									<label class="col-xs-2 control-label">시험 날짜</label>
        									<div class="col-xs-8">
            						<?php 
            						if (!strcmp($apply_type, 'A')) {
            						?>
        										<input type="text" name="exam_date" class="form-control st-date" value="<?php echo $exam_date ?>">
        										<input type="hidden" id="regular-exam-date" value="<?php echo $min_next_dt ?>">
        							<?php 
            						} else {
        							?>
        										<?php echo $exam_date ?>
        										<input type="hidden" name="exam_date" value="<?php echo $exam_date ?>">
        							<?php 
            						}
        							?>
        									</div>
        								</div>
                        				<div class="form-group">
        									<label class="col-xs-2 control-label">상태</label>
        									<div class="col-xs-8">
        										<select name="exam_state" id="exam_state" class="form-control">
            						<?php 
            						foreach ($if_exam_state as $key => $val) {
            						    $selected = $exam_state == $key ? 'selected' : '';
            						?>
            										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
            						<?php 
            						}
            						?>
        										</select>
        									</div>
        								</div>
                        				<div class="form-group">
        									<label class="col-xs-2 control-label">입금 상태</label>
        									<div class="col-xs-8">
        										<select name="pay_state" id="pay_state" class="form-control">
            						<?php 
            						foreach ($if_payment_state as $key => $val) {
            						    $selected = $pay_state == $key ? 'selected' : '';
            						?>
            										<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
            						<?php 
            						}
            						?>
        										</select>
        									</div>
        								</div>
        								<div class="form-group">
        									<label class="col-xs-2 control-label">관리자 메모</label>
        									<div class="col-xs-8">
        										<textarea name="admin_memo" class="form-control"></textarea>
        									</div>
        								</div>
        							</div>
        							<div class="box-footer text-center">
        								<button type="button" id="btn-close" class="btn btn-default">닫기</button> &nbsp;
        								<button type="submit" id="btn-submit" class="btn btn-primary">저장</button>
        							</div>
        						</div>
                        	</form>
						</div>
					</div>
				</div>
			</div>	
		</div>
		
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		<script>
		$(function() {
			// 폼 전송
			$("#form-item-edit").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/exam-order-edit.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
						$("#btn-submit").prop("disabled", true);
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("저장했습니다.");
								opener.window.location.reload();
							} else {
								alert("저장할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
					$("#btn-submit").prop("disabled", false);
				}); // ajax
			});
			
			$("#btn-close").click(function() {
				window.close();
			});

			$(".st-date").datepicker({
    			 dateFormat : "yy-mm-dd",
    			 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
    		     monthNamesShort : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
    			 changeMonth : true,
    			 changeYear : true,
    			 minDate : $("#regular-exam-date").val()
    		});
		});
		</script>
	</body>
</html>