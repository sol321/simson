<?php
/*
 * Desc: 모의고사 신청내역 견적서 출력
 *
 *  합계 견적서와 특정 차시 개별 견적서를 출력.
 *
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

if (empty($_GET['id'])) {
	if_js_alert_back('신청에 대한 정보가 필요합니다.');
}

$apply_id = $_GET['id'];    // 신청 아이디
$pr_oid = empty($_GET['oid']) ? 0 : $_GET['oid'];       // 차시 아이디

// 신청대학교 정보
$exam_row = if_get_mock_exam_application($apply_id);
$univ_name = $exam_row['univ_name'];

// 은행 정보
$if_bank_account_data = if_get_option('if_bank_account_data');
$jdec = json_decode($if_bank_account_data, true);
if (!empty($jdec)) {
	$bank = explode("\t", $jdec['bank_data'][0]);   // 무통장입금계좌 첫번째 정보
	$bank_txt = $bank[0] . '  <strong style="padding-right: 10px;">' . $bank[1] . '</strong> ' . $bank[2];
} else {
	$bank_txt = '수협은행  <strong style="padding-right: 10px;">1010-1181-1046</strong> 사)인포랑학회';
}

// 응시자 수
$taker_number = if_get_exam_taker_count($apply_id);

// 차수 정보
$order_results = if_get_mock_exam_orders_of_application($apply_id);

/*
 * Desc: 금액 설정
 *
 *  2회차 까지는 8,000원
 *  3회자 부터는 4,000원
 */
$until_second = 8000;
$from_third = 4000;

?>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no" />
<link href="https://fonts.googleapis.com/css?family=Nanum+Myeongjo:400,700&display=swap" rel="stylesheet">
<title>Estimate</title>
<style>
*{margin: 0;padding: 0;box-sizing: border-box;-moz-box-sizing: border-box;font-family: 'Nanum Myeongjo', serif;}
body{font-family: 'Nanum';}
.page{width: 21cm;padding: 1cm;margin: 1cm auto;}
.wrap{border: 2px solid #000;}
table th, table td{padding: 10px;border-spacing: 0;border: 1px solid #e8e8e8;}
table th{border-bottom: 0;font-size: 16px;}
table th:first-child{border-left: 0;}
table th:last-child{border-right: 0;}
table td{height: 35px;font-size: 15px;}
table td:first-child{border-left: 0;}
table td:last-child{border-right: 0;}
table tbody tr:last-child td{border-bottom: none;}
@page{size: A4;margin: 0;}
@media print {
	body * {
		visibility: hidden;
	}
	#section-to-print, #section-to-print * {
		visibility: visible;
	}
	#section-to-print {
		position: absolute;
		top: 10;
	}
}
</style>

<script src="<?php echo ADMIN_INC_URL ?>/js/util.js"></script>

</head>
<body>
	<div class="page">

		<div style="text-align: right; margin-bottom: 10px;">
			<a href="javascript:window.print();" style="background-color: #366AF2; color: #fff; padding: 5px; text-decoration: none;">인쇄하기</a>
			<a href="javascript:close();" style="background-color: #5F5F5F; color: #fff; padding: 5px; text-decoration: none;">닫기</a>
		</div>
		
		<div id="section-to-print">
    		<div class="wrap">
    			<h1 style="display: block;padding: 10px 0;text-decoration: underline;text-align: center;">견&nbsp;&nbsp;&nbsp;적&nbsp;&nbsp;&nbsp;서</h1>
    			<table style="width: 99%;margin: 0 auto;border-collapse: collapse;">
    				<caption style="overflow: hidden;position: absolute;width: 1px;height: 1px;clip: rect(0 0 0 0);margin: -1px;">견적서</caption>
    				<colgroup>
    					<col style="width: 30%;">
    					<col style="width: 5%;">
    					<col style="width: 15%;">
    					<col style="width: 20%;">
    					<col style="width: 10%;">
    					<col style="width: 20%;">
    				</colgroup>
    				<tbody>
    					<tr>
    						<td rowspan="5">
    							<span style="display: block;text-decoration: underline;"><?php echo date('Y년 m월 d일') ?></span>
    							<span style="display: block;padding: 10px 0;text-decoration: underline;"><strong style="font-size: 20px;letter-spacing: 1px;"><?php echo $univ_name ?></strong> 귀중</span>
    							아래와 같이 견적합니다.
    						</td>
    						<td rowspan="5">
    							공<br>
    							급<br>
    							자
    						</td>
    						<td>등록번호</td>
    						<td colspan="3">105-86-61529</td>
    					</tr>
    					<tr>
    						<td>상호<br>(법인명)</td>
    						<td>사)인포랑학회</td>
    						<td>성명</td>
    						<td style="text-align: center;">김용로 <span style="position: relative;display: inline-block;width: 63px;height: 60px;vertical-align: middle;"><span style="display: block;width: 20px;height: 20px;position: absolute;top: 50%;left: 50%;z-index: 2;margin-top: -10px;margin-left: -14px;">(인)</span> <img src="<?php echo ADMIN_INC_URL ?>/img/stamp.png" alt="" style="position: absolute;top: 0;left: 0;z-index: 1;"></span></td>
    					</tr>
    					<tr>
    						<td>사업장 주소</td>
    						<td colspan="3">
    							서울특별시 광진구 광나루로 56길 85, 테크노-마트21 31층
    						</td>
    					</tr>
    					<tr>
    						<td>업태</td>
    						<td>비영리법인</td>
    						<td>종목</td>
    						<td>학술, 교육, 출판</td>
    					</tr>
    					<tr>
    						<td>전화번호</td>
    						<td colspan="3">
    							Tel: 02-337-3337<br>
    							Fax: 02-325-7667
    						</td>
    					</tr>
    				</tbody>
    			</table>
    			<div style="width: 99%;margin: 0 auto;padding: 10px 20px;border-top: 2px solid #666;border-bottom: 2px solid #222;">
    				<span style="display: block;padding-bottom: 20px;font-size: 20px;">합계금액</span>
    				<span style="font-size: 14px;">(공급가액+세액)</span> 
    				<strong style="padding-left: 20px;font-size: 20px;letter-spacing: 1px;"><span id="fee-korean"></span> 원 整 (<span id="fee-number"></span>)</strong>
    			</div>
    			<table style="width: 99%;margin: 0 auto;border-collapse: collapse;">
    				<caption style="overflow: hidden;position: absolute;width: 1px;height: 1px;clip: rect(0 0 0 0);margin: -1px;">견적내용</caption>
    				<colgroup>
    					<col style="20%">
    					<col style="10%">
    					<col style="10%">
    					<col style="15%">
    					<col style="15%">
    					<col style="15%">
    					<col style="15%">
    				</colgroup>
    				<thead>
    					<tr>
    						<th scope="col">품&nbsp;&nbsp;명</th>
    						<th scope="col">회&nbsp;&nbsp;차</th>
    						<th scope="col">인&nbsp;&nbsp;원</th>
    						<th scope="col">단&nbsp;&nbsp;가</th>
    						<th scope="col">공급가액</th>
    						<th scope="col">세&nbsp;&nbsp;액</th>
    						<th scope="col">비&nbsp;&nbsp;고</th>
    					</tr>
    				</thead>
    				<tbody>
    		<?php
    		$total_sum = 0;
    		
    		
    		if (!empty($order_results)) {
    			
    			if (empty($pr_oid)) {
    			    $order_total = count($order_results);
    			
    				foreach ($order_results as $key => $val) {
    					$exam_order = $val['exam_order'];
    					$exam_fee = $key < 2 ? $until_second : $from_third;
    					$total_sum += $taker_number * $exam_fee;
    		?>
    					<tr>
    						<td>대학생 학습역량<br>평가 모의고사</td>
    						<td style="text-align: center;"><?php echo $exam_order ?>회차</td>
    						<td style="text-align: center;"><?php echo number_format($taker_number)?></td>
    						<td style="text-align: center;"><?php echo number_format($exam_fee) ?></td>
    						<td style="text-align: center;"><?php echo number_format($taker_number * $exam_fee) ?></td>
    						<td style="text-align: center;">0</td>
    						<td></td>
    					</tr>
    		<?php 
    				}
    			} else {
    			    $order_total = 1;
    			    
    			    foreach ($order_results as $key => $val) {
    			        $order_id = $val['order_id'];
    			        $exam_order = $val['exam_order'];
    			        $exam_fee = $key < 2 ? $until_second : $from_third;
    			        $total_sum = $taker_number * $exam_fee;
    			        
    			        $hide_row = $order_id == $pr_oid ? '' : 'style="display: none;"';
    		?>
    					<tr <?php echo $hide_row ?>>
    						<td>대학생 학습역량<br>평가 모의고사</td>
    						<td style="text-align: center;"><?php echo $exam_order ?>회차</td>
    						<td style="text-align: center;"><?php echo number_format($taker_number)?></td>
    						<td style="text-align: center;"><?php echo number_format($exam_fee) ?></td>
    						<td style="text-align: center;"><?php echo number_format($taker_number * $exam_fee) ?></td>
    						<td style="text-align: center;">0</td>
    						<td></td>
    					</tr>
    		<?php
    			    }
    			}
    		}
    		?>
    			<?php 
    			for ($i = 10; $i > $order_total; $i--) {     // 빈 칸
    			?>
    					<tr>
    						<td></td>
    						<td style="text-align: center;"></td>
    						<td style="text-align: center;"></td>
    						<td style="text-align: center;"></td>
    						<td style="text-align: center;"></td>
    						<td style="text-align: center;"></td>
    						<td></td>
    					</tr>
    			<?php 
    			}
    			?>
    				</tbody>
    				<tfoot>
    					<tr>
    						<td style="border-top: 1px solid #000;text-align: center;"><strong>계</strong></td>
    						<td style="border-top: 1px solid #000;text-align: center;"></td>
    						<td style="border-top: 1px solid #000;text-align: center;"></td>
    						<td style="border-top: 1px solid #000;text-align: center;"><strong></strong></td>
    						<td style="border-top: 1px solid #000;text-align: center;"><strong><?php echo number_format($total_sum) ?></strong></td>
    						<td style="border-top: 1px solid #000;text-align: center;"><strong>0</strong></td>
    						<td style="border-top: 1px solid #000;"></td>
    					</tr>
    				</tfoot>
    			</table>
    		</div>
    		<span style="display: block;padding-top: 5px;">
    			입금계좌 : <?php echo $bank_txt ?>
    		</span>
    	</div>
	</div>
	
	<input type="hidden" id="total_sum" value="<?php echo $total_sum ?>">
	
	<script>
    var totalSum = document.getElementById("total_sum").value;
    var feeK = switchKo(totalSum);
    var feeN = new Intl.NumberFormat('ko-KR', { style: 'currency', currency: 'KRW' }).format(totalSum);
    document.getElementById("fee-korean").innerHTML = feeK;
    document.getElementById("fee-number").innerHTML = feeN;

//  	window.print();
    </script>
	
</body>
</html>
