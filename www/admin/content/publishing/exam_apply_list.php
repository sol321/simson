<?php
/*
 * Desc: 모의고사 메인
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);
$qa2 = empty($_GET['qa2']) ? '' : trim($_GET['qa2']);
$qa3 = empty($_GET['qa3']) ? '' : trim($_GET['qa3']);

$period_from = empty($_GET['period_from']) ? '' : $_GET['period_from'];
$period_to = empty($_GET['period_to']) ? '' : $_GET['period_to'];

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
	$sql .= " AND $qa1 LIKE ?";
	array_push($sparam, '%' . $qs1 . '%');
}

// 응시 날짜 검색
if (!empty($period_from) && !empty($period_to)) {
	$sql .= " AND (o.exam_date BETWEEN ? AND ?)";
	array_push($sparam, $period_from, $period_to);
}

// Positional placeholder ?
if (!empty($sql)) {
	$pph_count = substr_count($sql, '?');
	for ($i = 0; $i < $pph_count; $i++) {
		$pph .= 's';
	}
}

if (!empty($pph)) {
	array_unshift($sparam, $pph);
}

$query = "
		SELECT
			COUNT(*),
			a.*
		FROM
			" . $GLOBALS['if_tbl_mock_exam_apply'] . " AS a
		INNER JOIN
			" . $GLOBALS['if_tbl_mock_exam_order'] . " AS o
		WHERE
			a.seq_id = o.apply_id
			$sql
		GROUP BY
			a.seq_id
		ORDER BY
			a.seq_id DESC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();
$total_page = $paginator->if_get_total_page();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="exam_apply_list.php"><b>신청 접수</b></a></li>
							<li><a href="exam_schedule.php"><b>시험 일정</b></a></li>
							<li><a href="univ_code.php"><b>학교 코드</b></a></li>
							<li><a href="#"><b>공지사항</b></a></li>
							<li><a href="#"><b>질문게시판</b></a></li>
							<li><a href="#"><b>FAQ</b></a></li>
						</ul>
					</div>
				</div>
				<div class="content">
					<div class="box box-default">
						<div class="box-body">
							<form id="search-form">

								<div class="row">
									<div class="col-md-2">
										<select name="qa1" id="qa1" class="form-control">
											<option value="">- 키워드 -</option>
											<option value="univ_name" <?php echo strcmp($qa1, 'univ_name') ? '' : 'selected'; ?>>대학교</option>
											<option value="apply_name" <?php echo strcmp($qa1, 'apply_name') ? '' : 'selected'; ?>>신청자명</option>
										</select>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-6">
												<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 검색 (<?php echo number_format($total_count) ?>)</button> &nbsp;
												<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row add-top">
									<div class="col-md-4">
										<div class="input-group datepicker input-daterange">
											<input type="text" id="period_from" name="period_from" value="<?php echo $period_from ?>" class="form-control">
											<span class="input-group-addon">~</span>
											<input type="text" id="period_to" name="period_to" value="<?php echo $period_to ?>" class="form-control">
										</div>
									</div>
								</div>

							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				
					<div class="box box-info">
						<div class="box-header">
							<h3 class="box-title">
								Total : <?php echo number_format($total_count) ?> &nbsp;
							</h3>
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr class="active">
										<th>#</th>
										<th>신청대학교</th>
										<th>신청자</th>
										<th>차수(응시일)</th>
										<th>응시자 수</th>
										<th>견적서</th>
										<th>학생리스트</th>
										<th>신청날짜</th>
										<th></th>
									</tr>
					<?php
					if (!empty($item_results)) {
						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
						foreach ($item_results as $key => $val) {
							$seq_id = $val['seq_id'];
							
							$univ_name = $val['univ_name'];
							$apply_name = $val['apply_name'];
							$apply_dt = $val['apply_dt'];
							
							$meta_data = $val['meta_data'];
							$file = if_get_val_from_json($meta_data, 'file_attachment');
							$file_path = $file[0]['file_path'];
							$file_name = $file[0]['file_name'];
							
							$cnt = if_get_exam_taker_count($seq_id);
							$exam_orders = if_get_mock_exam_orders_of_application($seq_id);
						?>
									<tr id="post-id-<?php echo $seq_id ?>">
										<td><?php echo $list_no ?></td>
										<td><?php echo $univ_name ?></td>
										<td><?php echo $apply_name ?></td>
										<td>
											<ul class="list-group">
    							<?php
    							if (!empty($exam_orders)) {
    								foreach ($exam_orders as $k => $v) {
    								    $order_id = $v['order_id'];
    								    $exam_order = $v['exam_order'];
    								    $exam_date = $v['exam_date'];
    								    $exam_state = $v['exam_state'];
    								    $pay_state = $v['pay_state'];
    								    $apply_type = $v['apply_type'];
    							?>
    											<li class="list-group-item" id="item-<?php echo $order_id ?>">
    												<span class="label label-default"> <?php echo $k + 1 ?></span>
    												<?php echo $exam_order ?>차 
    												(<?php echo $if_exam_type[$apply_type] ?> : <?php echo $exam_date ?>)
    												
    												<?php echo $if_exam_state[$exam_state] ?>
    												<?php echo $if_payment_state[$pay_state] ?>
    												
    												<a href="exam_order_edit.php?id=<?php echo $order_id ?>&idx=<?php echo $k ?>" class="btn btn-warning btn-xs edit-order-win">수정</a>
    												<a href="print_estimate.php?id=<?php echo $seq_id ?>&oid=<?php echo $order_id ?>" class="btn btn-info btn-xs print-estimate">견적서</a>
    												
    											</li>
    							<?php 
    								}
    							}
    							?>
											</ul>
										</td>
										<td><?php echo number_format($cnt) ?>명</td>
										<td>
											<a href="print_estimate.php?id=<?php echo $seq_id ?>" class="btn btn-info btn-xs print-estimate">합계 견적서</a>
										</td>
										<td>
											<a href="exam_applicant_list.php?id=<?php echo $seq_id ?>" class="btn btn-info btn-xs">
												리스트(<?php echo number_format($cnt) ?>)
											</a>
											<a href="<?php echo ADMIN_INC_URL ?>/lib/download_filepath.php?fp=<?php echo base64_encode($file_path) ?>&fn=<?php echo base64_encode($file_name) ?>" class="btn btn-success btn-xs" title="<?php echo $file_name ?>">
												Download
											</a>
										</td>
										<td><?php echo substr($apply_dt, 0, 10) ?></td>
										<td>
											<a href="exam_apply_edit.php?id=<?php echo $seq_id ?>" class="btn btn-info btn-xs">편집</a> &nbsp;
											<a href="javascript:;" class="btn btn-danger btn-xs action-delete">삭제</a> &nbsp;
										</td>
									</tr>
					<?php
							$list_no--;
						}
					}
					?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer text-center">
				<?php
				echo $paginator->if_paginator();
				?>
						</div>
						<!-- /.box-footer -->
					</div>
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script>
		$(function() {
			// 삭제
			$(".action-delete").click(function(e) {
				e.preventDefault();

				if (!confirm("삭제하시겠습니까?")) {
					return false;
				}

				var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
	
				$.ajax({
					type : "POST",
					url : "./ajax/exam-apply-delete.php",
					data : {
						"id" : id
					},
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							location.reload();
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});

			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});

			$("#period_from").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 3,
				 onClose: function(selectedDate) {
					  $("#period_to").datepicker("option", "minDate", selectedDate);
				 }
			});
			$("#period_to").datepicker({
				 dateFormat : "yy-mm-dd",
				 dayNamesMin : ["일", "월", "화", "수", "목", "금", "토"],
				 monthNamesShort : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 monthNames : ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
				 showMonthAfterYear : true,
				 changeYear : true,
				 changeMonth : true,
				 defaultDate: "0",
				 numberOfMonths: 3,
				 onClose: function(selectedDate) {
					  $("#period_from").datepicker("option", "maxDate", selectedDate);
				 }
			});

			// 차수 수정
			$(".edit-order-win").click(function(e) {
				e.preventDefault();
				var url = $(this).attr("href");
				window.open(url, "examorder", "width=650px,height=500px,scrollbars=1");
			});
			
			// 견적서 출력
			$(".print-estimate").click(function(e) {
				e.preventDefault();
				var url = $(this).attr("href");
				window.open(url, "print", "width=850px,height=1000px,scrollbars=1");
			});
		});
		</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>