<?php
/*
 * Desc: 모의고사 응시생 리스트
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';
require_once INC_PATH . '/classes/Paginator.php';

if_authenticate_admin();

if (empty($_GET['id'])) {
    if_js_alert_back('신청 대학에  대한 정보가 필요합니다.');
}

$apply_id = intval($_GET['id']);

$exam_row = if_get_mock_exam_application($apply_id);
$univ_name = $exam_row['univ_name'];
$univ_code = $exam_row['univ_code'];

$page = empty($_GET['page']) ? 1 : $_GET['page'];		// page number
$list_count = 10;		// 리스트 개수

// search
$qa1 = empty($_GET['qa1']) ? '' : trim($_GET['qa1']);
$qs1 = !isset($_GET['qs1']) ? '' : trim($_GET['qs1']);

$sql = '';
$pph = '';
$sparam = [];

// 키워드 검색
if (!empty($qa1) && !empty($qs1)) {
    if (!strcmp($qa1, 'student_id')) {   // 학번
        $sql = " AND student_id = ? ";
        array_push($sparam, $qs1);
    } else {
        $sql = " AND student_name LIKE ? ";
        array_push($sparam, '%' . $qs1 . '%');
    }
}

// student_idal placeholder ?
if (!empty($sql)) {
    $pph_count = substr_count($sql, '?');
    for ($i = 0; $i < $pph_count; $i++) {
        $pph .= 's';
    }
}

if (!empty($pph)) {
    array_unshift($sparam, $pph);
}

// 확회 검색을 위해 if_user_payment 테이블을 조인
$query = "
        SELECT
            *
        FROM
            " . $GLOBALS['if_tbl_mock_exam_taker'] . "
        WHERE
            apply_id = '$apply_id'
            $sql
        ORDER BY
			student_name ASC
";
$paginator = new Paginator($ifdb, $page, $list_count);
$item_results = $paginator->if_init_pagination($query, $sparam);
$total_count = $paginator->if_get_total_rows();

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<!-- jQueryUI -->
		<link rel="stylesheet" href="<?php echo INC_URL ?>/css/jquery-ui.min.css">
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="content-header">
					<h1>
						모의고사 응시자 리스트
						<small><?php echo $univ_name ?>(<?php echo $univ_code ?>)</small>
					</h1>
				</div>
				<div class="content">
					<div class="row">
    					<div class="col-md-6">
        					<div class="box box-info">
        						<div class="box-header">
                					<h3 class="box-title">
        								Total : <?php echo number_format($total_count) ?> &nbsp;
        							</h3>
        							<div class="pull-right">
    									<a href="download_exam_taker.php?id=<?php echo $apply_id ?>" class="btn btn-success btn-sm">엑셀 파일 저장</a>
    								</div>
        						</div>
        						<!-- /.box-header -->
        						
        						<div class="box-body">
    								<div class="form-group">
                						<form id="search-form" class="form-inline">
                							<input type="hidden" name="id" value="<?php echo $apply_id ?>">
        									
        									<div class="col-md-12">
        										<select name="qa1" id="qa1" class="form-control">
        											<option value="">- 선택 -</option>
        											<option value="student_id" <?php echo strcmp($qa1, 'student_id') ? '' : 'selected'; ?>>학번</option>
        											<option value="student_name" <?php echo strcmp($qa1, 'student_name') ? '' : 'selected'; ?>>성명</option>
        										</select>
        										<div class="input-group">
        											<input type="text" name="qs1" value="<?php echo $qs1 ?>" class="form-control">
        											<div class="input-group-btn">
        												<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        											</div>
        										</div>
        										<div class="input-group">
        											<button type="button" id="reset-btn" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> 초기화</button>
        										</div>
        									</div>
        									
        								</form>
                					</div>
                					<br><br>
        						
        							<table class="table table-hover">
        								<tbody>
        									<tr class="active">
            									<th>#</th>
        										<th>학번</th>
        										<th>성명</th>
                								<th>-</th>
            								</tr>
            				<?php
            				$curdate = date('Y-m-d');
            				
        					if (!empty($item_results)) {
        						$list_no = $page == 1 ? $total_count : $total_count - (($page - 1) * $paginator->rows_per_page);
        						foreach ($item_results as $key => $val) {
        							$taker_id = $val['taker_id'];
        							
        							$student_id = $val['student_id'];
        							$student_name = $val['student_name'];
        						?>
        									<tr id="id-<?php echo $taker_id ?>">
        										<td><?php echo $list_no ?></td>
        										<td><?php echo $student_id ?></td>
        										<td><?php echo $student_name ?></td>
        										<td>
                        							<a href="javascript:;" class="badge bg-red action-delete">삭제</a>
        										</td>
        									</tr>
        					<?php
        							$list_no--;
        						}
        					}
        					?>
        								</tbody>
        							</table>
        						</div><!-- /.box-body -->
            					<div class="box-footer text-center">
                		<?php
                		echo $paginator->if_paginator();
                		?>
                				</div>
            					<!-- /.box-footer -->
        					</div>
        				</div>
        				<div class="col-md-4">
            				<div class="box box-default">
            					<div class="box-header">
            						<h4>응시자 추가 - <?php echo $univ_name ?></h4>
            					</div>
            					<div class="box-body">
            						<form id="form-item-new" class="form-horizontal">
            							<input type="hidden" name="apply_id" value="<?php echo $apply_id ?>">
            							<div class="form-group">
                        					<label class="col-md-3 control-label">학번</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control numeric" name="student_id" id="student_id">
                        					</div>
                        				</div>
                        				<div class="form-group">
                        					<label class="col-md-3 control-label">성명</label>
                        					<div class="col-md-8">
                        						<input type="text" class="form-control" name="student_name" id="student_name">
                        					</div>
                        				</div>
                        				<div class="text-center">
                        					<button type="submit" id="btn-submit" class="btn btn-primary">추가</button>
                        				</div>
            						</form>
            					</div>
            				</div>
            			</div>
        			</div>
				
				</div>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->
		
		<!-- jQueryUI -->
		<script src="<?php echo INC_URL ?>/js/jquery-ui.min.js"></script>
		
		<script src="<?php echo INC_URL ?>/js/jquery/jquery.numeric-min.js"></script>
		
		<script>
    	$(function() {
    		$(".numeric").numeric({ negative: false });
    		
        	// 삭제
    		$(".action-delete").click(function(e) {
    			e.preventDefault();

    			if (!confirm("삭제하시겠습니까?")) {
        			return false;
    			}

    			var id = $(this).closest("tr").attr("id").replace(/\D/g, "");
    
    			$.ajax({
    				type : "POST",
    				url : "./ajax/exam-applicant-delete.php",
    				data : {
    					"id" : id
    				},
    				dataType : "json",
    				beforeSend : function() {
    				},
    				success : function(res) {
    					if (res.code == "0") {
    						location.reload();
    					} else {
    						alert(res.msg);
    					}
    				}
    			}).done(function() {
    			}).fail(function() {
    			}).always(function() {
    			}); // ajax
    		});

    		// 폼 전송
			$("#form-item-new").submit(function(e) {
				e.preventDefault();

				$.ajax({
					type : "POST",
					url : "./ajax/exam-applicant-add.php",
					data : $(this).serialize(),
					dataType : "json",
					beforeSend : function() {
					},
					success : function(res) {
						if (res.code == "0") {
							if (res.result) {
								alert("추가했습니다.");
								location.reload();
							} else {
								alert("응시자를 등록할 수 없습니다.");
							}
						} else {
							alert(res.msg);
						}
					}
				}).done(function() {
				}).fail(function() {
				}).always(function() {
				}); // ajax
			});

    		// 검색 초기화
			$("#reset-btn").click(function() {
				$("#search-form :input[type='text']").val("");
				$("select").val("");
			});
    	});
    	</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>