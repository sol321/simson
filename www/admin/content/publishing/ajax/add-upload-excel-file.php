<?php
/*
 * Desc : 엑셀 대량 등록(학교 코드)
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_FILES['attachment']['tmp_name'])) {
    $code = 101;
    $msg = '주소록 파일(xls, xlsx, csv)을 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$extension = strtolower(if_get_file_extension($_FILES['attachment']['name']));

if ($extension != 'xls' && $extension != 'xlsx') {
    $code = 102;
    $msg = '주소록 파일 확장자는 xls, xlsx만 가능합니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$upload_path = UPLOAD_PATH . '/tmp';
if (!is_dir($upload_path)) {
    mkdir($upload_path, 0777, true);
}
$new_file_name = if_make_rand() . '.' . $extension;
$new_file_path = $upload_path . '/' . $new_file_name;
if (!move_uploaded_file($_FILES['attachment']['tmp_name'], $new_file_path)) {
    $code = 201;
    $msg = '엑셀 파일을 처리하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if_delete_univ_code_all();

$result = if_add_excel_file_to_univ_code($new_file_path);

if (empty($result)) {
    $code = 202;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>