<?php
/*
 * Desc : 모의고사 차수 변경 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['order_id'])) {
    $code = 111;
    $msg = '차수 신청 내역을  선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['exam_state'])) {
    $code = 103;
    $msg = '모의고사 시험 상태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['pay_state'])) {
    $code = 104;
    $msg = '입금 상태를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// if (empty($_POST['exam_date'])) {
//     $code = 105;
//     $msg = '응시날짜를 입력해 주십시오.';
//     $json = compact('code', 'msg');
//     exit(json_encode($json));
// }

$order_id = $_POST['order_id'];
$result = if_update_mock_exam_order($order_id);

if (empty($result)) {
    $code = 501;
    $msg = '저장하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>