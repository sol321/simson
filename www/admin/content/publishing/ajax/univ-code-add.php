<?php
/*
 * Desc : 학교 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['univ_code'])) {
    $code = 101;
    $msg = '대학 코드를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty(trim($_POST['univ_name']))) {
    $code = 102;
    $msg = '대학교 이믈을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$check = if_exists_univ_code($_POST['univ_code']);

if ($check) {
    $code = 105;
    $msg = '이미 사용중인 대학교 코드입니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$result = if_add_univ_code();

if (empty($result)) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>