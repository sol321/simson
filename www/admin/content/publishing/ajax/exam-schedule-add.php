<?php
/*
 * Desc : 차수 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['exam_year'])) {
    $code = 101;
    $msg = '연도를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$exam_year = intval($_POST['exam_year']);

if (strlen($exam_year) != 4) {
    $code = 103;
    $msg = '4자리 숫자로 연도룰 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$check = if_exists_exam_year($exam_year);

if ($check) {
    $code = 105;
    $msg = '해당 연도는 이미 등록되어 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

// 차수 입력 확인
foreach ($_POST['exam_order'] as $key => $val) {
    if (empty($val)) {
        $code = 108;
        $msg = $key + 1 . '차수 응시날짜를 입력해 주십시오.';
        $json = compact('code', 'msg');
        exit(json_encode($json));
    }
}

$result = if_add_mock_exam();

if (empty($result)) {
    $code = 501;
    $msg = '등록하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>