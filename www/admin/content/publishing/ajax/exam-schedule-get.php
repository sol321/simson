<?php
/*
 * Desc : 모의고사 차수 정보 조회
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['year'])) {
    $code = 101;
    $msg = '연도를 선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$exam_year = $_POST['year'];

$result = if_get_mock_exam_by_year($exam_year);

if (empty($result)) {
    $code = 501;
    $msg = '해당 정보를 조회하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$exam_html = '';

foreach ($result as $key => $val) {
    
    $exam_order = $val['exam_order'];
    $exam_date = $val['exam_date']; 
    
    $exam_html .=<<<EOD
            <div class="form-group box-st-date">
				<div class="col-md-12">
					<div class="input-group">
						<div class="input-group-addon">
							<b class="st-order">$exam_order</b> 차
						</div>
						<div class="has-success">
							<input type="text" class="form-control st-date" name="exam_order[]" value="$exam_date" maxlength="10">
						</div>
						<span class="input-group-btn">
							<button type="button" class="btn bg-red btn-flat st-act-delete"><i class="fa fa-times"></i></button>
						</span>
					</div>
				</div>
			</div>
EOD;
}

$json = compact('code', 'msg', 'exam_year', 'exam_html');
echo json_encode($json);

?>