<?php
/*
 * Desc : 모의고사 변경 등록
 */
require_once '../../../../if-config.php';
require_once FUNC_PATH . '/functions-mock-exam.php';

$code = 0;
$msg = '';

if (!if_get_current_admin_id()) {
    $code = 510;
    $msg = '관리자만 이용할 수 있습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['seq_id'])) {
    $code = 111;
    $msg = '신청 내역을  선택해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['org_address1']) || empty($_POST['org_address2'])) {
    $code = 102;
    $msg = '주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_name'])) {
    $code = 103;
    $msg = '신청자명을 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_phone'])) {
    $code = 104;
    $msg = '전화번호를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (empty($_POST['apply_email'])) {
    $code = 105;
    $msg = '이메일 주소를 입력해 주십시오.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

if (!filter_var($_POST['apply_email'], FILTER_VALIDATE_EMAIL)) {
    $code = 117;
    $msg = '유효한 이메일주소가 아닙니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$seq_id = $_POST['seq_id'];
$result = if_update_mock_exam_application($seq_id);

if (empty($result)) {
    $code = 501;
    $msg = '저장하지 못했습니다.';
    $json = compact('code', 'msg');
    exit(json_encode($json));
}

$json = compact('code', 'msg', 'result');
echo json_encode($json);

?>