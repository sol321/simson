<?php
require_once '../if-config.php';

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo json_decode(if_get_option('site_name')) ?></title>

		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/bootstrap.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/css/custom.css">

		<!-- jQuery 3 -->
		<script src="<?php echo INC_URL ?>/js/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap.min.js"></script>
		<!-- jQuery Validate Plugin -->
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
    	
    <body class="hold-transition login-page">
        <div class="login-box">
        	<div class="login-logo">
        		<b>Admin</b> Login
        	</div>
        	<!-- /.login-logo -->
        	<div class="login-box-body">
        		<p class="login-box-msg">
        		</p>
        
        		<form id="form-item-login">
        			<div class="form-group has-feedback">
        				<input type="text" name="login" id="login" class="form-control" placeholder="아이디">
        			</div>
        			<div class="form-group has-feedback">
        				<input type="password" name="password" id="password" class="form-control" placeholder="비밀번호" autocomplete="new-password">
        			</div>
        			<div class="row">
        				<!-- /.col -->
        				<div class="col-xs-12">
        					<button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
        				</div>
        				<!-- /.col -->
        			</div>
        		</form>
        
        	</div>
        	<!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        
        <script>
    	$(function () {
    		$("input").prop("required", true);	// All fields required
    		
    		$("#form-item-login").validate({
    			rules: {
					login: {
						required: true,
						minlength: 6
					},
					password: {
						required: true,
						minlength: 6
					}
				},
				messages: {
					login: {
						required: "아이디를 입력해 주십시오.",
						minlength: "아이디는 최소 6자 이상입니다."
					},
					password: {
						required: "비밀번호를 입력해 주십시오.",
						minlength: "비밀번호는 최소 6자 이상입니다."
					}
				},
    			submitHandler: function(form) {
    				$.ajax({
    					type : "POST",
    					url : "./ajax/login.php",
    					data : $("#form-item-login").serialize(),
    					dataType : "json",
    					beforeSend : function() {
    						$("#btn-submit").prop("disabled", true);
    					},
    					success : function(res) {
    						if (res.code == "0") {
    							location.href = "dashboard.php";
    						} else {
    							alert(res.msg);
    						}
    					}
    				}).done(function() {
    				}).fail(function() {
    				}).always(function() {
    					$("#btn-submit").prop("disabled", false);
    				}); // ajax
    			}
    		});
    	});
        </script>
        
    </body>
</html>

