<?php
/*
 * Desc : 관리자 로그아웃
 */
require_once '../if-config.php';

unset($_SESSION['cms']);

if_redirect(ADMIN_URL . '/login.php');

?>