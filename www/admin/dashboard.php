<?php
require_once '../if-config.php';
require_once FUNC_PATH . '/functions-user.php';
require_once FUNC_PATH . '/functions-payment.php';

if_authenticate_admin();

$user_total = if_get_user_total_count();
$executives_total = if_get_total_executives();
$sponsor_total = if_get_total_sponsors();

// 학회
$unit_json = if_get_option('academy');
$unit_items = json_decode($unit_json, true);
$unit_total_user = []; 
if (!empty($unit_items)) {
	foreach ($unit_items as $key => $val) {
	    $number = if_get_number_by_academy($val);
	    array_push($unit_total_user, $number);
	}
}
$label_academy = empty($unit_items) ? '' : "'" . implode("','", $unit_items) . "'";
$label_number = empty($unit_total_user) ? '' : implode(",", $unit_total_user);

require_once ADMIN_PATH . '/include/cms-header.php';

?>
		<script src="<?php echo ADMIN_INC_URL ?>/js/chartjs.min.js"></script>
	</head>

	<body class="hold-transition sidebar-mini <?php echo ALTE_SKIN ?>">

		<div class="wrapper">
		
<?php 
require_once ADMIN_PATH . '/include/cms-gnb.php';
?>
		
<?php 
require_once ADMIN_PATH . '/include/cms-lnb.php';
?>
			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Dashboard</h1>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-3 col-md-6 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-users"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">회원</span>
									<span class="info-box-number"><?php echo number_format($user_total) ?></span>
								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.col -->
						<div class="col-md-3 col-md-6 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-green"><i class="fa fa-fw fa-tags"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">임원</span>
									<span class="info-box-number"><?php echo number_format($executives_total) ?></span>
								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.col -->
						<div class="col-md-3 col-md-6 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-thumbs-o-up"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">후원</span>
									<span class="info-box-number"><?php echo number_format($sponsor_total) ?></span>
								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.col -->
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">납부 완료 학회 이력</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="col-md-8">
											<canvas id="canvas"></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!-- /.content-wrapper -->
			
			<script>
			// ChartJS
			var barChartData = {
					labels: [<?php echo $label_academy ?>],
					datasets: [{
						label: '',
						backgroundColor: "#F3584E",
						data: [<?php echo $label_number ?>]
					}]

				};

				window.onload = function() {
					var ctx = document.getElementById('canvas').getContext('2d');
					window.myBar = new Chart(ctx, {
						type: 'bar',
						data: barChartData,
						options: {
							responsive: true,
							legend: {
								position: 'top',
							},
							title: {
								display: true,
								text: 'Bar Chart'
							}
						}
					});
				};
			</script>

<?php
require_once ADMIN_PATH . '/include/cms-footer.php';
?>

	</body>
</html>