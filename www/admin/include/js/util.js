/*
 * 숫자를 한글로 표현, number2koreaan
 * https://gist.github.com/jihunleekr/d175222e68f296470898
 */
function switchKo(num) {
	var namesInSeat = ['', '일', '이', '삼', '사', '오', '육', '칠', '팔', '구'];
	var namesInSeats = ['', '십', '백', '천'];
	var namesInFourSeat = ['', '만', '억', '조'];
	var numArr = num.toString().split('').reverse();
	var numStr = '';

	for (var i = numArr.length - 1; i >= 0; i--) {		
		if (numArr[i] > 0) {
			numStr += namesInSeat[numArr[i]];
			numStr += namesInSeats[i % 4];
		}		
		if (i % 4 == 0) {
			numStr += namesInFourSeat[parseInt(i / 4)];
		}
	}
	return numStr;
}