function countBytes(sendMsg) {
	var chr = "", chrLength = 0;
	var extra = $("#unsubscribe").prop("checked") ? 23 : 0;
	
	for (i = 0; i < sendMsg.length; i++) {
		chr = sendMsg.charAt(i);
		if (escape(chr).length > 4) {
			chrLength += 2;
		} else if (chr != "\r") {		// %0D%0A
			chrLength++;
		}
	}
	chrLength += extra;
	return chrLength;
}

function limitBytes(sendMsg, maxByte) {
	var chr = "", chrLength = 0, validMsgLength = 0, validChrLength = 0, validMsg = "", bytesVal = "";
	var maxBytes = $("#unsubscribe").prop("checked") ? maxByte - 23 : maxByte;
	
	for (i = 0; i < sendMsg.length; i++) {
		chr = sendMsg.charAt(i);
		if (escape(chr).length > 4) {
			chrLength += 2;
		} else if (chr != "\r") {		// %0D%0A
			chrLength++;
		}
		if (chrLength <= maxBytes) {
			validMsgLength = i + 1;
			validChrLength = chrLength;
		}
	}
	if (chrLength > maxBytes) {
		alert(maxBytes +"바이트 이상의 메세지는 전송하실 수 없습니다.");
		validMsg = sendMsg.substr(0, validMsgLength);
		$("#message").val(validMsg);
		bytesVal = "<b>" + validChrLength + "</b> / "+ maxBytes;
	} else {
		bytesVal = "<b>" + chrLength + "</b> / "+ maxBytes;
	}
	
	if ($("#byte-guage").length > 0) {
		$("#byte-guage").html(bytesVal);
	}
}

function limitBytesBatch(msgObj, maxByte) {
	var chr = "", chrLength = 0, validMsgLength = 0, validChrLength = 0, validMsg = "", bytesVal = "";
	var maxBytes = maxByte;
	var sendMsg = msgObj.val();
	
	for (i = 0; i < sendMsg.length; i++) {
		chr = sendMsg.charAt(i);
		if (escape(chr).length > 4) {
			chrLength += 2;
		} else if (chr != "\r") {		// %0D%0A
			chrLength++;
		}
		if (chrLength <= maxBytes) {
			validMsgLength = i + 1;
			validChrLength = chrLength;
		}
	}
	if (chrLength > maxBytes) {
		alert(maxBytes +"바이트 이상의 메세지는 전송하실 수 없습니다.");
		validMsg = sendMsg.substr(0, validMsgLength);
		msgObj.val(validMsg);
		bytesVal = "<b>" + validChrLength + "</b> / "+ maxBytes;
	} else {
		bytesVal = "<b>" + chrLength + "</b> / "+ maxBytes;
	}
	
	msgObj.parent().find("span").html(bytesVal);
}