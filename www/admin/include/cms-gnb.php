			<header class="main-header">
				<!-- Logo -->
        		<a href="<?php echo ADMIN_URL ?>/dashboard.php" class="logo">
        			<!-- mini logo for sidebar mini 50x50 pixels -->
        			<span class="logo-mini"><b>I</b>F</span>
        			<!-- logo for regular state and mobile devices -->
        			<span class="logo-lg">CMS</span>
        		</a>
        
        		<!-- Header Navbar: style can be found in header.less -->
        		<nav class="navbar navbar-static-top">
        			<!-- Sidebar toggle button-->
        			<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        				<span class="sr-only">Toggle navigation</span>
        			</a>
        			
        			<div class="fav-item-shortcut pull-left hide"><!-- 자주쓰는 메뉴 바로 가기-->
       					<a href="" class="btn btn-info">회원관리</a>
       				</div>
        			
        			<!-- Navbar Right Menu -->
        			<div class="navbar-custom-menu">
        				<ul class="nav navbar-nav">
        					<!-- User Account: style can be found in dropdown.less -->
        					<li class="dropdown user user-menu">
        						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
        							<span class="hidden-xs">관리자</span>
        						</a>
        						<ul class="dropdown-menu">
        							<!-- Menu Footer-->
        							<li class="user-footer">
        								<div class="pull-left">
        									<a href="<?php echo ADMIN_URL ?>/content/setting/password.php" class="btn btn-default btn-flat">비밀번호 변경</a>
        								</div>
        								<div class="pull-right">
        									<a href="<?php echo ADMIN_URL ?>/logout.php" class="btn btn-danger btn-flat">로그아웃</a>
        								</div>
        							</li>
        						</ul>
        					</li>
        		<?php 
        		if (if_get_current_admin_level() == '9000') {
        		?>
        					<!-- Control Sidebar Toggle Button -->
        					<li>
        						<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        					</li>
        		<?php 
        		}
        		?>
        				</ul>
        			</div>
        		</nav>
			</header>