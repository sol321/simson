<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo json_decode(if_get_option('site_name')) ?></title>

		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<link rel="stylesheet" href="<?php echo INC_URL ?>/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/components/font-awesome/css/font-awesome.min.css">
	
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/adminlte/css/skins/_all-skins.min.css">
	
		<!-- Custom css -->
		<link rel="stylesheet" href="<?php echo ADMIN_INC_URL ?>/css/custom.css">

		<!-- jQuery 3 -->
		<script src="<?php echo INC_URL ?>/js/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo INC_URL ?>/bootstrap/js/bootstrap.min.js"></script>
		<!-- jQuery Validate Plugin -->
		<script src="<?php echo INC_URL ?>/js/jquery/validation/jquery.validate.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo ADMIN_INC_URL ?>/adminlte/js/adminlte.min.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Google Font -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">