<?php
require_once FUNC_PATH . '/functions-term.php';

// Board 리스트 조회
// require_once FUNC_PATH . '/functions-board.php';
// $board_lists = if_get_board_lists();
?>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
        		<section class="sidebar">
        			<!-- sidebar menu: : style can be found in sidebar.less -->
        			<ul class="sidebar-menu" data-widget="tree">
        				<li class="treeview">
        					<a style="display: inline-block;">
        						<span class="btn btn-info btn-xs" id="lnb-expand"><i class="fa fa-fw fa-plus-square"></i>전체 확장</span>
        					</a>
        					<a style="display: inline-block;">
        						<span class="btn btn-success btn-xs" id="lnb-reduce"><i class="fa fa-fw fa-minus-square"></i>전체 축소</span>
        					</a>
        				</li>
        				
        	<?php 
        	if_print_terms(0, 'cms_lnb_menu', 0);
        	?>
        				
        				
        			</ul>
        		</section>
        		<!-- /.sidebar -->
        	</aside>
