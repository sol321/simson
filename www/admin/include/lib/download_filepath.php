<?php
/*
 * Desc : 파일 다운로드 , 각 파라미터는 base64_encoding
 * 		fp : 물리 파일 경로, fn : 파일명.
 */
require_once '../../../if-config.php';
require_once FUNC_PATH . '/functions-post.php';

if (empty($_GET['fp'])) {
    if_js_alert_back('다운로드 파일에 대한 정보가 필요합니다.');
}

if (empty($_GET['fn'])) {
    if_js_alert_back('파일명에 대한 정보가 필요합니다.');
}

$file_path = base64_decode($_GET['fp']);
$file_name = base64_decode($_GET['fn']);

if (!is_file($file_path)) {
    if_js_alert_back('파일이 존재하지 않습니다.');
}

$length = filesize($file_path);
$file_name = rawurlencode($file_name);		// firefox에서는 한글 인코딩됨.

// Start Download
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="'. $file_name . '"');
header('Content-Type: application/zip');
header('Content-Transfer-Encoding: binary');
if ($length > 0) {
    header('Content-Length: ' . $length);
}

set_time_limit(0);

$file = fopen($file_path, "rb");

if ($file) {
    while(!feof($file)) {
        print(fread($file, 1024 * 8));
        flush();
        usleep(20000);		// *** 남은 다운로드 시간을 보여준다.
    }
    @fclose($file);
}

?>